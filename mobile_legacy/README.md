# EWARS Mobile Application (react-native)

Currently only supported on android, iOS coming shortly.

## TODO

- Move this to YARN

## Release 

```
appcenter codepush release-react -a ewars/EWARS-Mobile -d Production --mandatory --target-binary-version "^33.0.0"
```