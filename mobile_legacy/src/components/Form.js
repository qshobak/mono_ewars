import React, {Component} from "react";
import {
    View,
    UIManager,
    Animated,
    ToolbarAndroid,
    Keyboard,
    Button,
    StyleSheet,
    ScrollView,
    Text,
    Image,
    TouchableHighlight,
    AsyncStorage,
    Alert,
    ToastAndroid,
    Modal,
    NetInfo,
    KeyboardAvoidingView
} from "react-native";
import Icon from "react-native-fontawesome-pro";


import Utils from "./utils";

import AssignmentField from "./fields/AssignmentField";

import SelectField from "./fields/SelectField";
import TextField from "./fields/TextField";
import TextArea from "./fields/TextAreaField";
import NumberField from "./fields/NumberField";
import DateField from "./fields/DateField";
import Location from "./fields/Location";
import LatLngField from './fields/LatLngField';
import TimeField from "./fields/f.time";
import API from "./api/api";
import Storage from "./lib/storage";
import CalcDisplayField from "./fields/CalcDisplayField";

import styles from "./STYLES";

import {
    ActionButton,
    LineSpacer
} from "./common";

import moment from "moment";

const style = StyleSheet.create({
    BASE: {
        backgroundColor: "#F2F2F2"
    },
    BASE_PADDED: {
        backgroundColor: "#F2F2F2",
        paddingBottom: 50
    },
    DETAILS: {
        flexDirection: "column",
        backgroundColor: "#FFFFFF",
        marginTop: 8,
        marginBottom: 16,
        marginLeft: 8,
        marginRight: 8,
        paddingBottom: 16
    },
    BUTTON_BAR: {
        flexDirection: "row",
        justifyContent: "flex-end",
        backgroundColor: "#333333",
        height: 40
    },
    LABEL: {
        fontSize: 12
    },
    HEADER: {
        fontWeight: "bold",
        fontSize: 14,
        marginBottom: 8,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8
    },
    FIELD: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        marginLeft: 8,
        marginTop: 8,
        marginRight: 8
    },
    SUB_LABEL: {
        fontSize: 12,
        marginLeft: 8,
        paddingLeft: 8,
        borderLeftWidth: 1,
        fontWeight: "bold",
        marginBottom: 8,
        marginTop: 14
    },
    SUB_SUB_LABEL: {
        fontSize: 12,
        marginLeft: 5,
        paddingLeft: 8,
        marginBottom: 8,
        marginTop: 14
    },
    IMAGE_BTN: {
        width: 40,
        height: 40,
        padding: 14,
        marginRight: 8,
        tintColor: "white"
    },
    MAIN: {
        margin: 0
    },
    ERROR: {
        backgroundColor: "#cc5254",
        padding: 4,
        marginTop: 3,
        marginBottom: 3
    },
    ERROR_TEXT: {
        fontSize: 12,
        color: "#FFFFFF"
    },
    BUTTON_BAR_FILL: {
        flex: 2
    }
})

const FIELDS = {
    matrix: null,
    text: TextField,
    textarea: TextArea,
    date: DateField,
    select: SelectField,
    number: NumberField,
    time: TimeField,
    age: null,
    assignment: null,
    location: null,
    display: null,
    lat_long: LatLngField,
    calculated: CalcDisplayField
};

const FIELD_SORT = (a, b) => {
    if (a.order > b.order) return 1;
    if (a.order < b.order) return -1;
    return 0;
};

// Flatten the form definition
const flattenForm = (definition) => {
    let fields = [];

    let rootFields = [];
    for (let i in definition) {
        rootFields.push({
            ...definition[i],
            name: `${definition[i].name}`
        });
    }

    rootFields = rootFields.sort(FIELD_SORT);

    rootFields.forEach((node, index) => {
        if (node.type == "matrix") {
            fields.push(node);

            let rows = [];
            for (let i in node.fields) {
                let row = node.fields[i];

                rows.push({
                    ...node.fields[i],
                    name: `${node.name}.${row.name}`
                })
            }

            rows = rows.sort(FIELD_SORT);

            rows.forEach(row => {
                fields.push(row);

                let cells = [];
                for (let p in row.fields) {
                    let cell = row.fields[p];

                    cells.push({
                        ...cell,
                        name: `${row.name}.${cell.name}`
                    })
                }

                cells = cells.sort(FIELD_SORT);
                cells.forEach(cell => {
                    fields.push(cell);
                })
            })

        } else {
            fields.push(node);
        }
    })

    return fields;
};

// Get the next focusable field after the supplied index
const getNextFocusableField = (definition, curIndex) => {

};

class Display extends Component {
    render() {
        return (
            <View style={styles.DISPLAY_FIELD}>
                <Text style={styles.DISPLAY_TEXT}>{Utils.i18n(this.props.data.label)}</Text>
            </View>
        )
    }
}

class Header extends Component {
    render() {
        return (
            <View>
                <Text style={style.HEADER}>{Utils.i18n(this.props.data.label)}</Text>
            </View>
        )
    }
}


class Field extends Component {
    static defaultProps = {
        enabled: true
    };

    _onChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let FieldType = null;
        if (this.props.data.show_mobile === false) {
            return null;
        }

        if (this.props.data.type == "display") {
            return null;
            // return <Display data={this.props.data}/>
        }

        if (this.props.data.type == "header") {
            return <Header data={this.props.data}/>
        }

        if (this.props.data.type == "matrix") {
            return (
                <View>
                    <Text style={style.SUB_LABEL}>{Utils.i18n(this.props.data.label)}</Text>
                </View>
            )
        }

        if (this.props.data.type == "row") {
            return (
                <View>
                    <Text style={style.SUB_SUB_LABEL}>{Utils.i18n(this.props.data.label)}</Text>
                </View>
            )
        }


        if (FIELDS[this.props.data.type]) {
            FieldType = FIELDS[this.props.data.type]
        }

        let label;
        if (this.props.data.label) {
            label = Utils.i18n(this.props.data.label);
        } else {
            label = "";
        }

        if (this.props.data.required) label += " *";

        let error;
        if (this.props.errors[this.props.data.name]) {
            error = (
                <View style={style.ERROR}>
                    <Text style={style.ERROR_TEXT}>{this.props.errors[this.props.data.name]}</Text>
                </View>
            )
        }

        return (
            <View style={style.FIELD}>
                <Text style={style.LABEL}>{label}</Text>
                {error}
                {FieldType ?
                    <FieldType
                        enabled={this.props.enabled}
                        errors={this.props.errors}
                        ref={(el) => this._node = el}
                        onNextField={this.props.onNextField}
                        nextField={this.props.nextField}
                        focused={this.props.focused}
                        onChange={this._onChange}
                        name={this.props.data.name}
                        sourceData={this.props.sourceData}
                        data={this.props.data}/>
                    : null}
            </View>
        )
    }
}

class Form extends Component {
    static navigationOptions = () => {
        return {
            title: global._("FORM"),
            headerRight: (
                <TouchableHighlight style={styles.NAV_BTN} onPress={() => {
                    Utils.emit("SHOW_FORM_ACTIONS")
                }}>
                    <Icon
                        containerStyle={{
                            marginTop: 8,
                            marginRight: 8
                        }}
                        name="share-square" type="light" size={30}/>
                </TouchableHighlight>
            )
        }
    };

    static defaultProps = {
        draft: false,
        data: null,
        form: null,
        assignment: null
    };

    constructor(props) {
        super(props);

        const data = props.navigation.getParam("data", {});
        const form = props.navigation.getParam("form", {});
        const assignment = props.navigation.getParam("assignment", {});

        this.state = {
            data: data.data,
            data_date: data.data_date,
            location_id: data.location_id,
            uuid: null,
            errors: {},
            dirty: false,
            status: "DRAFT",
            actionsVisible: false,
            form: form,
            definition: flattenForm(form.definition),
            assignment: assignment,
            processing: false,
            focusedField: null,
            elFields: {}
        }

        this._fields = {};
    }

    componentWillMount() {
        Utils.listen("SHOW_FORM_ACTIONS", () => {
            this.setState({
                actionsVisible: true
            })
        })

        const data = this.props.navigation.getParam("data", {});
        const form = this.props.navigation.getParam("form", {});
        const assign = this.props.navigation.getParam("assignment", {});
        if (data) {
            this.setState({
                data: data.data,
                location_id: data.location_id || null,
                data_date: data.data_date || null,
                status: data.status,
                uuid: data.uuid || null,
                assignment: assign,
                form: form

            })
        }
    }

    componentWillUnmount() {
        Utils.unListen("SHOW_FORM_ACTIONS");
    }

    keyboardWillShow = (event) => {
        this.setState({
            keyboardOpen: true
        });
    };

    keyboardWillHide = (event) => {
        this.setState({
            keyboardOpen: false
        })
    };

    componentWillReceiveProps(nextProps) {
        const data = this.props.navigation.getParam("data", {});
        for (var i in data) {
            if (i != "data") this.state[i] = data[i];
        }
    }

    _submit = () => {
        let errors = this._validate();
        if (errors) {
            this.setState({
                errors: errors,
                actionsVisible: false
            })
            ToastAndroid.show(global._("FORM_ERRORS"), ToastAndroid.SHORT);
            return;
        }

        if (global.USER.role == "USER") {
            if (global.IS_CONNECTED) {
                this._submitDo(true);
            } else {
                this._submitDo(false);
            }
        } else {
            this._submitDo(true);
        }
    };

    _queue = (data) => {
        Storage.addToQueue(data)
            .then(() => {
                ToastAndroid.show(global._("RECORD_QUEUED"), ToastAndroid.SHORT);
                this.props.navigation.pop(2);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    _submitDo = (immediate) => {

        let loc_name = null;
        if (global.USER.role == 'USER') {
            const assignment = this.props.navigation.getParam("assignment", null);
            assignment.locations.forEach((item) => {
                if (item.location_id == this.state.location_id) {
                    loc_name = item.location_name;
                }
            })
        } else {
            if (this.state.location) {
                loc_name = this.state.location.name;
            }
        }


        let data = {
            uuid: this.state.uuid || Utils.uuid(),
            location_id: this.state.location_id,
            data_date: this.state.data_date,
            data: this.state.data,
            form_id: this.state.form.id,
            version_id: this.state.form.version_id,
            created_date: new Date(),
            last_modified: new Date(),
            status: "SUBMITTED",
            meta: {
                form_name: this.state.form.name,
                location_name: loc_name,
                interval: this.state.form.features.INTERVAL_REPORTING ? this.state.form.features.INTERVAL_REPORTING.interval : 'DAY'
            }
        };

        if (immediate) {

            // Test if remote is reachable
            this._debugEndpoint(() => {
                this.setState({
                    processing: true
                }, () => {
                    API.submitForm([
                        this.state.form.id,
                        this.state.location_id,
                        this.state.data_date,
                        this.state.data
                    ]).then((resp) => {
                        if (resp.data.err) {
                            if (resp.data.code == "DUPLICATE_REPORT") {
                                ToastAndroid.show(global._("SUBMISSION_FAILED_DATE"), ToastAndroid.LONG);
                                this.setState({actionsVisible: false, processing: false});
                            }
                        } else {
                            Storage.addToSent(data).then(() => {
                                this.setState({actionsVisible: false, processing: false});
                                this.props.navigation.goBack();
                                ToastAndroid.show(global._("SUBMISSION_SUCCESS"), ToastAndroid.SHORT);
                            }).catch(err => {
                                console.log(err)
                            });
                        }
                    }).catch((err) => {
                        this.setState({processing: false}, () => {
                            if (global.USER.role == "USER") {
                                this._queue(data);
                            } else {
                                Alert.alert(
                                    global._("ALERT_SUBMIT_FAILED"),
                                    global._("ALERT_SUBMIT_FAILED_MESSAGE"),
                                    [
                                        {text: global._("CANCEL"), onPress: () => this._cancelSubmit()},
                                        {text: global._("TRY_AGAING"), onPress: () => { this._submitDo(true); }}
                                    ]
                                );
                            }
                        }); // end of innert setState
                    }); // end of catch
                }); // end of main setstate

            }, () => {
                // error reaching the remote EWARS Server
                if (global.USER.role == "USER") {
                    ToastAndroid.show(global._("NO_REMOTE_CONNECT"), ToastAndroid.SHORT);
                    this._queue(data);
                } else {
                    Alert.alert(
                        global._("ALERT_SUBMIT_FAILED"),
                        global._("ALERT_SUBMIT_FAILED_MESSAGE"),
                        [
                            {text: global._("CANCEL"), onPress: () => this._cancelSubmit()},
                            {text: global._("TRY_AGAIN"), onPress: () => { this._submitDo(true) }}
                        ]
                    );
                }
            });

        } else {
            Alert.alert(
                global._("ALERT_QUEUE_TITLE"),
                global._("ALERT_QUEUE_MESSAGE"),
                [
                    {
                        text: global._("OK"),
                        onPress: () => {
                            this._queue(data);
                        }
                    }
                ]
            )
        }
    };

    _cancelSubmit = () => {
        this.setState({
            processing: false,
            actionsVisible: false
        })
    };

    _cancel = () => {
        this.setState({
            processing: false,
            actionsVisible: false
        })
    };

    _onChange = (prop, value, data) => {
        let errors = this.state.errors;
        if (this.state.errors[prop]) {
            delete errors[prop];
        }

        this.setState({
            ...this.state,
            dirty: true,
            data: {
                ...this.state.data,
                [prop]: value
            },
            errors: errors
        })
    };

    _onRootChange = (prop, value) => {
        this.setState({
            ...this.state,
            dirty: true,
            [prop]: value
        })
    };

    _validate = () => {
        let errors = {};
        let hasErrors = false;

        if (this.state.form.features.LOCATION_REPORTING) {
            if (!this.state.location_id || this.state.location_id == "") {
                hasErrors = true;
                errors.location_id = global._("ERR_NO_LOCATION");
            }
        }

        if (!Utils.isSet(this.state.data_date)) {
            hasErrors = true;
            errors.data_date = global._("ERR_NO_DATE");
        }

        if (Utils.isSet(this.state.data_data)) {
            // TODO: Remove layer
            if (this.state.form.id != 4 && global.USER.account_name != 'Bangladesh') {
                if (Utils.isFuture(this.state.data_date)) {
                    hasErrors = true;
                    errors.data_date = global._("ERR_NO_FUTURE");
                }
            }
        }

        if (global.USER.role == "USER" && this.props.assignment) {
            if (this.state.form.features.INTERVAL_REPORTING && this.state.form.features.LOCATION_REPORTING) {
                let dates;

                if (Utils.isSet(this.state.location_id)) {
                    // Check if it's a duplicated
                    if (this.state.assignment) {
                        let assign;
                        this.state.assignment.locations.forEach(item => {
                            if (item.location_id = this.state.location_id) {
                                assign = item;
                            }
                        });

                        if (assign) dates = assign.existing;
                    }
                }

                if (dates && Utils.isSet(this.state.data_date)) {
                    if (dates.indexOf(this.state.data_date) >= 0) {
                        errors.data_date = global._("ERR_DUPLICATE");
                        hasErrors = true;
                    }
                }
            }
        } else {
            // Check a report doesn't already exist
        }

        let fields = Utils.getVisibleFields(this.state.definition, this.state.data);

        fields.forEach(field => {

            if (['header', 'display', 'matrix', 'row', 'calculated'].indexOf(field.type) < 0) {

                if (field.required) {
                    if (!Utils.isSet(this.state.data[field.name])) {
                        errors[field.name] = global._("ERR_REQUIRED");
                        hasErrors = true;
                    }
                }
            }
        });

        if (hasErrors) return errors;
        return false;
    };

    _save = () => {

        const _data = this.props.navigation.getParam("data", {});
        const form = this.props.navigation.getParam("form", {});
        const assign = this.props.navigation.getParam("assignment", null);
        let loc_name = null;

        if (assign) {
            assign.locations.forEach(item => {
                if (item.location_id == this.state.location_id) {
                    loc_name = item.location_name;
                }
            })
        } else {
            if (this.state.location) {
                loc_name = this.state.location.name;
            }
        }

        let data = {
            uuid: this.state.uuid || Utils.uuid(),
            location_id: this.state.location_id,
            data_date: this.state.data_date,
            data: this.state.data,
            form_id: form.id,
            version_id: form.version_id,
            created_date: new Date(),
            last_modified: new Date(),
            status: "DRAFT",
            meta: {
                form_name: form.name,
                location_name: loc_name,
                interval: form.features.INTERVAL_REPORTING ? form.features.INTERVAL_REPORTING.interval : "DAY"
            }
        };

        Storage.updateDraft(data)
            .then(() => {
                ToastAndroid.show(global._("DRAFT_SAVED"), ToastAndroid.SHORT);
                this.props.navigation.pop(2);
            })
            .catch((err) => {
                console.log(err);
            })

    };

    _delete = () => {
        Alert.alert(
            global._("ALERT_DELETE_DRAFT"),
            global._("ALERT_DELETE_DRAFT_MESSAGE"),

            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                    }
                },
                {
                    text: global._("DELETE"),
                    onPress: () => this._processDelete()
                }
            ]
        )

    };

    _processDelete = () => {
        Storage.deleteReport(this.state.uuid)
            .then(() => {
                ToastAndroid.show(global._("RECORD_DELETED"), ToastAndroid.SHORT);
                this.setState({
                    actionsVisible: false
                })
                this.props.navigation.pop(2);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    // Called by Utils.signal
    _showModal = () => {
        this.setState({
            actionsVisible: true
        })
    };

    _closeModal = () => {
        this.setState({
            actionsVisible: false
        })
    };

    _amend = () => {

    };

    _retract = () => {

    };

    _onLocationChange = (location) => {
        this.setState({
            location_id: location.uuid,
            location: location
        })

    };

    _onNextField = (fieldName) => {
        let fields = Utils.getVisibleFields(this.state.definition, this.state.data);

        let nextFieldName;
        let curFieldIndex;
        for (let i in fields) {
            if (fields[i].name == fieldName) {
                curFieldIndex = parseInt(i);
                break;
            }
        }

        if (parseInt(curFieldIndex) >= fields.length - 1) {
            nextFieldName = "END";
        } else {
            let curIndex = parseInt(curFieldIndex) + 1;
            while (nextFieldName == null) {
                console.log(nextFieldName);
                if (curIndex >= fields.length - 1) {
                    nextFieldName = "END";
                } else {
                    if (['text', 'number', 'textarea', 'numeric'].indexOf(fields[curIndex].type) >= 0) {
                        nextFieldName = fields[curIndex].name;
                    } else {
                        curIndex++;
                    }
                }
            }

            console.log("NEXT", nextFieldName);

            if (nextFieldName && nextFieldName != "END") {
                console.log("HERE", nextFieldName);
            }
        }

        this.setState({
            focusedField: nextFieldName
        })
    };

    renderFields = () => {
        let fields = [];

        let enabled = true;

        if (this.state.status === "DRAFT") enabled = true;
        if (this.state.status === "SUBMITTED") enabled = false;
        let visibleFields = Utils.getVisibleFields(this.state.definition, this.state.data);

        visibleFields.forEach((item, index) => {
            let field = (
                <Field
                    enabled={enabled}
                    errors={this.state.errors}
                    focused={this.state.focusedField == item.name}
                    ref={(el) => {
                        this._fields[item.name] = el;
                    }}
                    onNextField={this._onNextField}
                    onChange={this._onChange}
                    sourceData={this.state.data}
                    key={item.name}
                    data={item}/>
            );
            fields.push(field);
        });

        return fields;
    };

    _debugEndpoint = (cb, errCallback) => {
        API.testEndpoint()
            .then(res => {
                console.log("HERE: ", res);
                if (res.data.pong && res.data.pong == "OK") {
                    cb()
                } else {
                    errCallback();
                }
            }).catch(err => {
                console.log("HERE ERR: err");
                errCallback();
            });
    };

    render() {
        const assignment = this.props.navigation.getParam("assignment", null);
        const form = this.props.navigation.getParam("form", {});

        let fields = [];
        for (let i in this.state.form.definition) {
            fields.push({
                ...this.state.form.definition[i],
                name: i
            })
        }

        fields = fields.sort(function (a, b) {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        let enabled = true;

        if (this.state.status === "DRAFT") enabled = true;
        if (this.state.status === "SUBMITTED") enabled = false;

        let locationSelector;
        if (form.features.LOCATION_REPORTING) {
            if (global.USER.role === "USER" && assignment) {
                locationSelector = (
                    <View>
                        {this.state.errors.location_id ?
                            <View style={style.ERROR}>
                                <Text style={style.ERROR_TEXT}>{this.state.errors.location_id}</Text>
                            </View>
                            : null}
                        <AssignmentField
                            enabled={enabled}
                            onChange={this._onRootChange}
                            name="location_id"
                            value={this.state.location_id}
                            data={assignment}/>
                    </View>
                )
            } else {
                locationSelector = (
                    <View>
                        {this.state.errors.location_id ?
                            <View style={style.ERROR}>
                                <Text style={style.ERROR_TEXT}>{this.state.errors.location_id}</Text>
                            </View>
                            : null}
                        <Location
                            enabled={enabled}
                            onChange={this._onLocationChange}
                            value={this.state.location_id}
                            site_type_id={this.state.form.features.LOCATION_REPORTING.site_type_id}/>
                    </View>
                )
            }

        }

        let interval = "DAY",
            offsetAvailability = false;
        if (this.state.form.features.INTERVAL_REPORTING) {
            interval = this.state.form.features.INTERVAL_REPORTING.interval;
        }

        let dateLabel = global._("RECORD_DATE") + "*";
        if (this.state.form.id == 11 && global.USER.account_name == 'Bangladesh') dateLabel = 'Date of admission*';
        if (this.state.form.id == 4 && global.USER.account_name == 'Bangladesh') offsetAvailability = true;
        if (this.state.form.id == 102 && global.USER.account_name == 'Valhalla') offsetAvailability = true;

        let deletable = false;
        if (this.state.uuid) deletable = true;
        if (this.state.status == 'SUBMITTED') deletable = true;

        let containerStyle = this.state.keyboardOpen ? style.BASE_PADDED : style.BASE;

        return (
            <KeyboardAvoidingView style={containerStyle}>
                <Modal
                    onRequestClose={() => {
                        console.log("CLOSE");
                    }}
                    transparent={false}
                    visible={this.state.processing}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <View style={{width: 100, height: 80, justifyContent: "center", alignItems: "center"}}>
                            <Icon name="share" size={40} type="light"/>
                        </View>
                        <View style={{width: 200, height: 50}}>
                            <Text
                                style={{textAlign: "center"}}>{global._("SUBMITTING_RECORD")}</Text>
                        </View>

                    </View>
                </Modal>
                <View style={style.MAIN}>
                    <ScrollView
                        keyboardDismissMode={"none"}
                        ref={(el) => this._scroller = el}
                        keyboardShouldPersistTaps={"always"}>
                        <Text style={styles.FORM_TITLE}>{Utils.i18n(this.state.form.name)}</Text>

                        <View style={style.DETAILS}>
                            <Text style={style.HEADER}>{global._("RECORD_DETAILS")}*</Text>

                            {locationSelector}

                            <View style={style.FIELD}>
                                <Text style={style.LABEL}>{dateLabel}</Text>
                                {this.state.errors.data_date ?
                                    <View style={style.ERROR}>
                                        <Text style={style.ERROR_TEXT}>{this.state.errors.data_date}</Text>
                                    </View>
                                    : null}
                                <DateField
                                    enabled={enabled}
                                    name="data_date"
                                    data={{date_type: interval}}
                                    value={this.state.data_date}
                                    offsetAvailability={offsetAvailability}
                                    onChange={this._onRootChange}/>
                            </View>
                        </View>
                        <View style={style.DETAILS}>
                            {this.renderFields()}
                        </View>
                        <View>
                            <Text value={JSON.stringify(this.state.form)} multiline={true}/>
                        </View>
                        <View style={styles.FORM_BUFFER}></View>

                    </ScrollView>
                </View>
                <Modal
                    transparent={false}
                    style={styles.MODAL_STYLE}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.actionsVisible}>
                    <View style={styles.ACTIONS_MODAL}>
                        {this.state.status == "DRAFT" ?
                            <ActionButton
                                title={global._("SUBMIT")}
                                color="green"
                                onPress={this._submit}/>
                            : null}
                        {this.state.status == "DRAFT" ?
                            <ActionButton
                                title={global._("SAVE_AS_DRAFT")}
                                color="orange"
                                onPress={this._save}/>
                            : null}
                        {deletable ?
                            <LineSpacer/>
                            : null}
                        {deletable ?
                            <ActionButton
                                title={global._("DELETE")}
                                color="red"
                                onPress={this._delete}/>
                            : null}
                        <LineSpacer/>
                        <ActionButton
                            title={global._("CANCEL")}
                            color="grey"
                            onPress={this._cancel}/>
                    </View>

                </Modal>
            </KeyboardAvoidingView>
        )
    }
}

export default Form;
