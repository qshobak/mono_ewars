import React, {Component} from "react";
import codePush, {InstallMode} from "react-native-code-push";

import {
    View,
    Text,
    Image,
    StyleSheet,
    Button,
    ScrollView,
    Linking,
    ToastAndroid,
    NetInfo
} from "react-native";

import API from "./api";
import DeviceInfo from "react-native-device-info";

import DialogAndroid from "react-native-dialogs";

import styles from "./STYLES";

const style = StyleSheet.create({
    scroller: {
        padding: 10
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 10,
        marginTop: 10
    },
    par: {
        marginBottom: 5
    },
    button: {
        marginBottom: 8
    },
    image: {
        justifyContent: "center",
        alignItems: "center"
    }
})

class About extends Component {
    static navigationOptions = () => {
        return {
            title: global._("ABOUT_EWARS")
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            cl_ver: {}
        }

    }

    componentWillMount() {
        codePush.getUpdateMetadata().then(data => {
            console.log(data);
            this.setState({cl_ver: data || {}})
        })
    }

    _onVisitClick = () => {
        Linking.openURL("http://ewars-project.org").catch((err) => {
        })
    };

    _onSyncStatusChange = (SyncStatus) => {
        if (!this._dg) {
        }

        switch(SyncStatus) {
            case codePush.SyncStatus.CHECKING_FOR_UPDATE:
                // Show "Checking for update" notification
                ToastAndroid.show("Checking for updates", ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.DOWNLOADING_PACKAGE:
                ToastAndroid.show("Downloading updates", ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.INSTALLING_UPDATE:
                ToastAndroid.show("Installing updates", ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.UP_TO_DATE:
                ToastAndroid.show("EWARS is up to date", ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.UPDATE_INSTALLED:
                ToastAndroid.show("Update Installed", ToastAndroid.SHORT);
                break;
        }
    };

    _onUpdateCheck = () => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                let updateDialogOptions = {
                    updateTitle: global._("UPDATE_TITLE"),
                    optionalUpdateMessage: global._("UPDATE_MESSAGE"),
                    optionalIgnoreButtonLabel: global._("UPDATE_CANCEL"),
                    optionalInstallButtonLabel: global._("UPDATE_INSTALL")
                };

                codePush.sync({
                    updateDialog: updateDialogOptions,
                    installMode: InstallMode.IMMEDIATE,
                    mandatoryInstallMode: InstallMode.IMMEDIATE
                }, this._onSyncStatusChange);

                API.checkVersion((result) => {
                    if (parseInt(result) > 32) {
                        Alert.alert(
                            "New Version",
                            "A new version of EWARS Mobile is available",
                            [
                                {
                                    text: "Ignore",
                                    onPress: () => {
                                    }
                                },
                                {
                                    text: "Download", onPress: () => {
                                    this._download(result)
                                }
                                }
                            ]
                        );
                    } else {
                        ToastAndroid.show("You are currently using the latest version", ToastAndroid.SHORT);
                    }
                })
            } else {
                ToastAndroid.show(global._("NO_INTERNET_CONNECTION"), ToastAndroid.SHORT);
            }
        });
    };

    _download = () => {
        Linking.openURL("http://cdn.ewars.ws/apk/app-release.apk").catch((err) => {
        })
    };

    render() {
        let version = DeviceInfo.getVersion();
        let clientVersion = codePush.getUpdateMetadata();
        return (
            <ScrollView style={styles.VIEW_MAIN}>
                <View style={style.image}>
                    <Image source={require("../img/ic_launcher.png")}/>
                </View>

                <View style={styles.ABOUT_MAIN}>
                    <Text style={style.title}>{global._("ABOUT_EWARS")}</Text>
                    <Text style={style.par}>{global._("APP_VERSION")}: {version} [{this.state.cl_ver.label || "Base"}]</Text>
                    <Text style={style.par}>{global._("ABOUT_1")}</Text>
                    <Text style={style.par}>{global._("ABOUT_2")}</Text>
                    <Text style={style.par}>{global._("ABOUT_3")}</Text>
                    <Text style={style.par}>{global._("ABOUT_4")}</Text>

                    <View style={style.button}>
                        <Button
                            style={style.button}
                            onPress={this._onVisitClick}
                            title={global._("BUTTON_VISIT_EWARS")}/>
                    </View>
                    <View style={style.button}>
                        <Button
                            style={style.button}
                            onPress={this._onUpdateCheck}
                            title={global._("BUTTON_CHECK_UPDATES")}/>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

export default About;