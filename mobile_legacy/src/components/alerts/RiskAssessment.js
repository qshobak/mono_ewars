import React, {Component} from "react";

import {
    View,
    ScrollView,
    Button,
    Text
} from "react-native";
import DialogAndroid from "react-native-dialogs";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

import {
    FormLabel,
    TextAreaField,
    Error
} from "../common";


class RiskAssessment extends Component {
    static navigationOptions = {
        title: global._("RISK_ASSESS")
    };

    constructor(props) {
        super(props);

        this.state = {
            hazard: "",
            exposure: "",
            context: "",
            data: null,
            error: null
        }
    }

    componentWillMount() {
        this._query();
    }

    componentWillReceiveProps() {
        this._query();
    }

    _query = () => {
        const alert = this.props.navigation.getParam("alert", {});
        API.getAlertStage([alert.uuid, "RISK_ASSESS"])
            .then((resp) => {
                if (resp.data) {
                    this.setState({
                        hazard: resp.data.data.hazard_assessment,
                        exposure: resp.data.data.exposure_assessment,
                        context: resp.data.data.context_assessment,
                        data: resp.data
                    })
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _onChange = (prop, text) => {
        this.setState({
            error: null,
            [prop]: text
        });
    };

    _submit = () => {
        if (!this.state.hazard || this.state.hazard == "") {
            this.setState({
                error: global._("ERR_HAZARD")
            })
            return;
        }

        if (!this.state.exposure || this.state.exposure == "") {
            this.setState({
                error: global._("ERR_EXPOSURE")
            })
            return;
        }

        if (!this.state.context || this.state.context == "") {
            this.setState({
                error: global._("ERR_CONTEXT")
            })
            return;
        }

        let id;
        if (this.state.data) id = this.state.data.id || null;

        const alert = this.props.navigation.getParam("alert", {});
        API.doAlertAction([
            alert.uuid,
            id,
            {
                alert_id: alert.uuid,
                type: "RISK_ASSESS",
                status: "SUBMITTED",
                data: {
                    hazard_assessment: this.state.hazard,
                    exposure_assessment: this.state.exposure,
                    context_assessment: this.state.context
                }
            }
        ])
            .then((resp) => {

                global.RELOAD_ALERT = true;
                this.props.navigation.goBack();
            })
            .catch((err) => {
                this.setState({
                    errors: [
                        global._("ERR_UNKNOWN")
                    ]
                })
            })

    };

    render() {
        const alert = this.props.navigation.getParam("alert", {});
        if (alert.stage == "VERIFICATION") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>{global._("PLEASE_VERIFICATION")}</Text>
                </View>
            )
        }
        let readOnly = false;

        if (this.state.data) {
            if (this.state.data.status == "SUBMITTED") {
                readOnly = true;
            } else {
                readOnly = false;
            }
        }
        return (
            <ScrollView>
                <View style={styles.CARD}>
                    {this.state.error ?
                        <Error text={this.state.error}/>
                    : null}

                    <FormLabel text={global._("HAZARD_ASSESSMENT")}/>

                    <TextAreaField
                        name="hazard"
                        disabled={readOnly}
                        value={this.state.hazard}
                        onChange={(prop, text) => {
                            this.setState({
                                hazard: text
                            })
                        }}/>

                    <FormLabel text={global._("EXPOSURE_ASSESSMENT")}/>

                    <TextAreaField
                        name="exposure"
                        disabled={readOnly}
                        value={this.state.exposure}
                        onChange={(prop, text) => {
                            this.setState({
                                exposure: text
                            })
                        }}/>


                    <FormLabel text={global._("CONTEXT_ASSESSMENT")}/>
                    <TextAreaField
                        name="context"
                        disabled={readOnly}
                        value={this.state.context}
                        onChange={(prop, text) => {
                            this.setState({
                                context: text
                            })
                        }}/>

                    {!readOnly ?
                        <Button
                            title={global._("BUTTON_SUBMIT_ASSESSMENT")}
                            onPress={this._submit}/>
                        : null}
                </View>

            </ScrollView>
        )
    }
}

export default RiskAssessment;