import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TextInput,
    TouchableHighlight,
    Image,
    StyleSheet,
    Picker
} from "react-native";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

const ROLES = {
    USER: "Reporting User",
    ACCOUNT_ADMIN: "Account Adminstrator",
    REGIONAL_ADMIN: "Geographic Administrator"
};

class AlertUser extends Component {
    render() {
        return (
            <View style={styles.ROW}>
                <Text>{this.props.data.name} ({this.props.data.email})</Text>
                <Text>{ROLES[this.props.data.role]}</Text>
            </View>
        )
    }
}


class AlertUsers extends Component {
    static navigationOptions = {
        title: global._("ALERT_USERS")
    };

    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentWillMount() {
        this._query();
    }

    _query = () => {
        API.queryAlertUsers([this.props.alert.uuid])
            .then((resp) => {
                this.setState({
                    users: resp.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };

    render() {
        return (
            <View>
                <ScrollView>
                    {this.state.users.map((item) => {
                        return (
                            <AlertUser
                                data={item}
                                key={"USER_" + item.id}/>
                        )
                    })}

                </ScrollView>
            </View>
        )
    }
}

export default AlertUsers;