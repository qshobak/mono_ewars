import React, {Component} from "react";
import {
    View, TextInput
} from "react-native";

class CalcDisplayField extends Component {
     constructor(props) {
         super(props);
     }

     render() {
         let value = 0;

         (this.props.data.calc_def || []).forEach(item => {
             let subVal = (this.props.sourceData || {})[item] || 0;
             value += parseFloat(subVal);
         })
         value = String(value);

         return (
             <View>
                 <TextInput
                     editable={false}
                     keyboardType="default"
                     value={value}/>
             </View>
         )
     }
}

export default CalcDisplayField;