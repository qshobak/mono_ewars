import React, {Component} from "react";
import {
    View,
    Picker,
    Text
} from "react-native";

import MultipleChoice from "rn-multiple-choice";

import Utils from "../utils";

class SelectMultiple extends Component {
    _select = (option) => {
        let actValue;

        this.props.data.options.forEach(item => {
            if (item[1] == option) actValue = item[0];
        });

        let data = this.props.value || [];
        if (data.indexOf(actValue) >= 0) {
            let idx = data.indexOf(actValue);
            data.splice(idx, 1);
        } else {
            data.push.apply(data, [actValue]);
        }
        this.props.onChange(data);
    };

    render() {
        let visOptions = this.props.data.options.map(item => {
            return item[1];
        })

        let selectedOptions = [];

        this.props.data.options.forEach(item => {
            if (this.props.value.indexOf(item[0]) >= 0) {
                selectedOptions.push(item[1])
            }
        })

        return (
            <View>
                <MultipleChoice
                    disabled={!this.props.enabled}
                    options={visOptions}
                    selectedOptions={selectedOptions}
                    onSelection={this._select}/>
            </View>
        )
    }

}

class Select extends Component {
    static defaultProps = {
        enabled: true
    };

    constructor(props) {
        super(props);
    }

    _onChange = (value) => {
        this.props.onChange(this.props.name, value);
    };



    render() {
        let value = Utils.getFieldValue(
            this.props.sourceData,
            this.props.name
        );

        if (value == null) value = "NULL";

        if (this.props.data.multiple) {
            if (!value || value == "NULL") value = [];
            return (
                <SelectMultiple
                    enabled={this.props.enabled}
                    value={value}
                    onChange={this._onChange}
                    data={this.props.data}/>
            )
        }

        let items = [];

        (this.props.data.options || []).forEach(item => {
            let label = item[1];
            if (item[1].en != null)  {
                label = item[1].en;
            }
            items.push(
                <Picker.Item
                    key={item[0]}
                    label={label}
                    value={item[0]}/>
            )
        })

        if (this.props.optionsSource) {
            if (this.props.optionsSource.prefixed) {
                this.props.optionsSource.prefixed.forEach(item => {
                    items.push(
                        <Picker.Item
                            key={item[0]}
                            label={item[1].en || item[1]}
                            value={key[0]}/>
                    )
                })
            }
        }

        items.unshift(
            <Picker.Item
                key="NONE"
                label={global._("NONE_SELECTED")}
                value={"NULL"}/>
        )

        return (
            <View>
                <Picker
                    ref={(el) => this._el = el}
                    enabled={this.props.enabled}
                    mode={Picker.MODE_DROPDOWN}
                    selectedValue={value}
                    onValueChange={this._onChange}>
                    {items}
                </Picker>

            </View>
        )
    }
}

export default Select;