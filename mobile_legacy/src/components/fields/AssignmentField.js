import React, {Component} from "react";
import {
    View,
    Text,
    Picker,
    StyleSheet
} from "react-native";

import Utils from "../utils";


const style = StyleSheet.create({
    BASE: {
        backgroundColor: "#F2F2F2",
        flexDirection: "column"
    },
    DETAILS: {
        flexDirection: "column",
        backgroundColor: "#FFFFFF",
        marginTop: 8,
        marginBottom: 16,
        marginLeft: 8,
        marginRight: 8,
        paddingBottom: 16
    },
    BUTTON_BAR: {
        flexDirection: "row",
        justifyContent: "flex-end",
        backgroundColor: "#333333",
        height: 30
    },
    LABEL: {
        fontSize: 12
    },
    HEADER: {
        fontWeight: "bold",
        fontSize: 14,
        marginBottom: 8,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8
    },
    FIELD: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        marginLeft: 8,
        marginTop: 8,
        marginRight: 8
    },
    SUB_FIELD: {},
    IMAGE_BTN: {
        width: 20,
        height: 20,
        padding: 14,
        marginRight: 8
    },
    MAIN: {}
})

class AssignmentField extends Component {
    static defaultProps = {
        enabled: true
    }

    _onChange = (value) => {
        this.props.onChange(this.props.name, value);
    };

    render() {

        let items = [
            <Picker.Item
                key={""}
                label={global._("NONE_SELECTED")}
                value=""/>
        ];

        let locations = this.props.data.locations.sort((a, b) => {
            return (a.location_name.en || a.location_name).localeCompare(b.location_name.en || b.location_name);
        })


        locations.forEach(item => {
            items.push(
                <Picker.Item
                    key={item.location_id}
                    label={Utils.i18n(item.location_name)}
                    value={item.location_id}/>
            )
        })

        return (
            <View style={style.FIELD}>
                <Text style={style.LABEL}>{global._("ASSIGNMENT_LOCATION")}*</Text>
                <Picker
                    enabled={this.props.enabled}
                    mode={Picker.MODE_DROPDOWN}
                    selectedValue={this.props.value}
                    onValueChange={this._onChange}>
                    {items}
                </Picker>
            </View>
        )
    }
}

export default AssignmentField;