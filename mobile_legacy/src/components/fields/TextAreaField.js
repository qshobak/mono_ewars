import React, {Component} from "react";
import {
    View,
    TextInput
} from "react-native";
import Utils from "../utils";

class TextArea extends Component {
    static defaultProps = {
        enabled: true
    };

    _onChange = (e) => {
        this.props.onChange(this.props.name, e);
    };

    componentDidUpdate(prevProps) {
        if (this.props.focused && !prevProps.focused) {
            this._el.focus();
        }
    }

    _nextField = () => {
        this.props.onNextField(this.props.name);
    };

    render() {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);
        return (
            <View>
                <TextInput
                    ref={(el) => this._el = el}
                    editable={this.props.enabled}
                    blurOnSubmit={false}
                    onSubmitEditing={this._nextField}
                    onChangeText={this._onChange}
                    multiline={true}
                    value={value || ""}/>
            </View>
        )
    }
}

export default TextArea;
