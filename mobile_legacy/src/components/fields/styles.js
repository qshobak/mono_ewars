import {StyleSheet} from "react-native";

export default STYLES = StyleSheet.create({
    FIELD: {
        padding: 8,
        background: "#F2F2F2",
        marginBottom: 8
    }
})