import React, {Component} from "react";
import {
    View,
    TextInput
} from "react-native";

import Utils from "../utils";

class NumberField extends Component {
    static defaultProps = {
        enabled: true
    };

    constructor(props) {
        super(props)
    }

    onChange = (value) => {
        if (value.indexOf("-") >= 0) value = value.replace("-", "");
        if (value.indexOf(",") >= 0) value = value.replace(",", "");
        if (value.indexOf("_") >= 0) value = value.replace("_", "");
        let strippedValue = value.replace(/[^\d.-]/g, '');
        let counter = 0;
        let validatedStr = strippedValue.replace(/[^0-9.]|\./g, function ($0) {
            if ($0 == "." && !(counter++)) // dot found and counter is not incremented
                return "."; // that means we met first dot and we want to keep it
            return ""; // if we find anything else, let's erase it
        });

        this.props.onChange(this.props.name, validatedStr);
    };

    componentDidUpdate(prevProps) {
        if (this.props.focused && !prevProps.focused) {
            this._el.focus();
        }
    }

    _nextField = () => {
        this.props.onNextField(this.props.name);
    };

    render() {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);

        if (value == undefined || value == null) {
            value = "";
        } else {
            value = String(value);
        }

        return (
            <View>
                <TextInput
                    returnKeyType="next"
                    editable={this.props.enabled}
                    ref={(el) => this._el = el}
                    keyboardType="numeric"
                    blurOnSubmit={false}
                    onChangeText={this.onChange}
                    onSubmitEditing={this._nextField}
                    value={value}/>
            </View>
        )
    }
}

export default NumberField;
