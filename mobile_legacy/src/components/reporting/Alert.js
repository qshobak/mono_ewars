import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TouchableHighlight,
    Image,
    StyleSheet,
    Modal
} from "react-native";
import DialogAndroid from "react-native-dialogs";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "row",
        padding: 8,
        borderBottomWidth: 1,
        backgroundColor: "#FFFFFF",
        borderColor: "#F2F2F2"
    },
    HEADER: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        backgroundColor: "#FFFFFF",
        borderColor: "#F2F2F2"
    },
    ROW_ALERT: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        backgroundColor: "#FFFFFF",
        borderColor: "#F2F2F2"
    },
    ICON: {
        flex: 1,
        flexBasis: 0
    },
    FLEX_TOUCH: {
        flexDirection: "row"
    },
    STAGE_TEXT: {
        flex: 1
    }
});

const STAGES = ["VERIFICATION", "RISK_ASSESS", "RISK_CHAR", "OUTCOME"];


class StageLink extends Component {
    _onSelect = () => {
        this.props.onClick(this.props.stage);
    };

    render() {
        let status = 0;

        if (this.props.stage == "VERIFICATION") {
            if (this.props.alert.outcome == "MONITOR" && this.props.alert.stage == "VERIFICATION") {
                status = 1;
            }
        }

        if (this.props.stage == this.props.alert.stage) {


        } else {
            let stageIndex = STAGES.indexOf(this.props.stage);
            let curIndex = STAGES.indexOf(this.props.alert.stage);

            if (stageIndex < curIndex) {
                // assumption is that it's completed
                status = 1;
            }
        }

        let img = (
            <Image
                style={styles.LIST_ITEM_CARET}
                source={require("../../img/ic_schedule_black_24dp.png")}/>
        );

        if (status) img = (
            <Image
                style={styles.LIST_ITEM_CARET}
                source={require("../../img/ic_check_circle_black_24dp.png")}/>
        )

        return (
            <TouchableHighlight onPress={this._onSelect}>
                <View style={style.ROW}>
                    <View style={styles.LIST_ITEM_ICON}>
                        {img}
                    </View>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text>{this.props.name}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Image
                            style={styles.LIST_ITEM_CARET}
                            source={require("../../img/ic_keyboard_arrow_right_black_24dp.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class AlertSection extends Component {
    _onSelect = () => {
        this.props.onClick(this.props.section);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._onSelect}>
                <View style={style.ROW}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text>{this.props.name}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Image
                            style={styles.LIST_ITEM_CARET}
                            source={require("../../img/ic_keyboard_arrow_right_black_24dp.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class AlertView extends Component {
    static navigationOptions = () => {
        return {
            title: global._("ALERT")
        }
    };

    constructor(props) {
        super(props);

        const alert = this.props.navigation.getParam("alert", {});
        const alarm = this.props.navigation.getParam("alarm", {});
        this.state = {
            alert: alert,
            alarm: alarm,
            loading: false
        };

        this._sub = this.props.navigation.addListener(
            'didFocus',
            this._getAlert
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    componentWillMount() {
        this._getAlert(this.state.alert.uuid);
    }

    _getAlert = (uuid) => {
        // Used to reload the alert when it changes
        const alert = this.props.navigation.getParam("alert", {});
        const alarm = this.props.navigation.getParam("alarm", {});
        API.getAlert([alert.uuid])
            .then((resp) => {
                global.RELOAD_ALERT = false;
                this.setState({
                    alert: resp.data,
                    loading: false
                })
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    loading: false
                })
            })
    };

    componentWillReceiveProps(nextProps) {
        this._getAlert(this.state.alert.uuid);
    }

    _selectStage = (stage) => {
        this.props.navigation.navigate(stage, {
            alert: this.state.alert,
            alarm: this.props.alarm
        })
    };

    _showReport = () => {

    };


    _showActivity = () => {
        this.props.navigation.navigate("ALERT_ACTIVITY", {
            alert: this.state.alert,
            alarm: this.props.alarm
        })
    };

    _showUsers = () => {
        this.props.navigation.navigate("ALERT_USERS", {
            alert: this.state.alert,
            alarm: this.props.alarm
        })
    };

    _showTriggeringReport = () => {
        API.getTriggeringReport([this.state.alert.triggering_report_id])
            .then((resp) => {
                let data = resp.data;
                this.props.navigation.navigate("FORM", {
                    form: {
                        id: data.form_id,
                        name: data.form_name,
                        features: data.features,
                        definition: data.form_definition
                    },
                    data: {
                        uuid: data.uuid,
                        data_date: data.data_date,
                        data: data.data,
                        location_id: data.location_id,
                        status: data.status,
                        form_id: data.form_id
                    }
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };


    render() {
        let locationName = Utils.i18n(this.state.alert.location_name);
        let risk = global._(this.state.alert.risk),
            stage = global._(this.state.alert.stage),
            stage_state = global._(this.state.alert.stage_state);
        let triggerDate = Utils.DATE(this.state.alert.trigger_end, this.state.alarm.ds_interval);

        return (
            <View style={styles.VIEW_MAIN}>
                <Modal
                    visible={this.state.loading}
                    onRequestClose={() => {
                        console.log("CLOSE")
                    }}>
                    <Text>{global._("LOADING_ALERT")}</Text>
                </Modal>
                <ScrollView>
                    <View style={style.HEADER}>
                        <Text style={{}}>{Utils.i18n(this.state.alarm.name)}</Text>
                        <Text>{locationName}</Text>
                        <Text>{triggerDate}</Text>
                    </View>

                    <View style={styles.ROW}>
                        <Text style={styles.HEADER}>{global._("ALERT_WORKFLOW")}</Text>
                    </View>

                    <StageLink
                        name={global._("VERIFICATION")}
                        onClick={this._selectStage}
                        alert={this.state.alert}
                        stage="VERIFICATION"/>
                    <StageLink
                        name={global._("RISK_ASSESS")}
                        onClick={this._selectStage}
                        alert={this.state.alert}
                        stage="RISK_ASSESS"/>
                    <StageLink
                        name={global._("RISK_CHAR")}
                        alert={this.state.alert}
                        onClick={this._selectStage}
                        stage="RISK_CHAR"/>
                    <StageLink
                        name={global._("OUTCOME")}
                        onClick={this._selectStage}
                        alert={this.state.alert}
                        stage="OUTCOME"/>

                    <View style={styles.ROW}>
                        <Text style={styles.HEADER}>{global._("ALERT_DATE")}</Text>
                    </View>

                    <AlertSection
                        name={global._("ACTIVITY")}
                        onClick={this._showActivity}
                        view="ALERT_ACTIVITY"/>
                    <AlertSection
                        name={global._("TRIGGERING_RECORD")}
                        onClick={this._showTriggeringReport}
                        view="ALERT_REPORT"/>
                    {/*<AlertSection*/}
                        {/*name={global._("USERS")}*/}
                        {/*onClick={this._showUsers}*/}
                        {/*view="ALERT_USERS"/>*/}


                </ScrollView>
            </View>
        )
    }
}

export default AlertView;