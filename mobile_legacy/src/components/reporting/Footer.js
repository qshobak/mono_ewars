import React, {Component} from "react";
import {URI} from "../constants";

import {
    View,
    NetInfo,
    Image,
    Text,
    Switch,
    ToastAndroid
} from "react-native";

import styles from "../STYLES";
import Utils from "../utils";

// const HEALTH_CHECK_URI = URI + "/health";
const HEALTH_CHECK_URI = "https://ewars.ws/health";
export default class Footer extends Component {
    static defaultProps = {
        isConnected: false
    }

    constructor(props) {
        super(props);

        this.state = {
            connected: null
        };

        this._timerCheck = null;

        this._isChecking = false;

        this._connChange();
    }

    componentWillMount() {
        NetInfo.addEventListener('connectionChange', this._connChange);
    }

    componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this._connChange);
    }

    _connChange = (connectionInfo) => {
        // Check if this isn't the first connection change
        if (connectionInfo) console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType)

        if (connectionInfo) {
            if (connectionInfo.type == "none") {
                global.IS_CONNECTED = false;
                this.setState({
                    connected: false
                }, () => {
                    Utils.emit("CONNECTION_CHANGE", false);
                })
                return;
            }
        }

        if (!this._isChecking) {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this._isChecking = true;
                    fetch(HEALTH_CHECK_URI)
                        .then((res) => {
                            this._isChecking = false;
                            global.IS_CONNECTED = true;
                            this.setState({
                                connected: true
                            }, () => {
                                Utils.emit("CONNECTION_CHANGE", true)
                            })
                        })
                        .catch((error) => {
                            this._isChecking = false;
                            global.IS_CONNECTED = false;
                            this.setState({
                                connected: false
                            }, () => {
                                Utils.emit("CONNECTION_CHANGE", false);
                            })
                        });
                } else {
                    this._isChecking = false;
                    global.IS_CONNECTED = false;
                    this.setState({
                        connected: false
                    }, () => {
                        Utils.emit("CONNECTION_CHANGE", false);
                    })
                }
            });
        }
    };

    render() {
        let label = global._("NO_CONNECTION");
        if (this.state.connected) label = global._("CONNECTED");
        if (this.state.connected == null) label = global._("CHECKING_CONNECTION");

        return (
            <View style={styles.FOOTER}>
                <Text style={styles.CONNECTION_TEXT}>{label}</Text>
            </View>
        )
    }
}
