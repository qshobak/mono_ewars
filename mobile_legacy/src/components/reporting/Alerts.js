import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TouchableHighlight,
    Image,
    Modal,
    StyleSheet
} from "react-native";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    },
    ROW_ALERT: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        backgroundColor: "#FFFFFF",
        borderColor: "#F2F2F2"
    },
    BUTTON_BAR: {
        flexDirection: "row",
        height: 40,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2",
        alignItems: "flex-start",
        paddingRight: 8
    },
    SYNC: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        borderWidth: 1,
        borderColor: "#333"
    },
    QUEUE_TITLE: {
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 8,
        backgroundColor: "#333",
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderWidth: 1,
        borderColor: "#333",
        marginLeft: 8
    },
    QUEUE_TEXT: {
        color: "#FFFFFF"
    }
})


class Item extends Component {
    render() {
        return (
            <View style={style.ROW}>
                <Text>{Utils.i18n(this.props.data.meta.form_name)}</Text>
                {this.props.data.meta.location_name ?
                    <Text>{Utils.i18n(this.props.data.meta.location_name)}</Text>
                    : null}
                {this.props.data.data_date ?
                    <Text>{Utils.DATE(this.props.data.data_date, this.props.data.meta.interval)}</Text>
                    : null}
            </View>
        )
    }
}

class Alert extends Component {
    _showAlert = () => {
        this.props.onClick(
            this.props.data,
            this.props.alarm
        )
    };

    render() {
        let triggeredDate = Utils.DATE(this.props.data.trigger_end, this.props.alarm.date_spec_interval_type);
        return (
            <TouchableHighlight onPress={this._showAlert}>
                <View style={style.ROW_ALERT}>
                    <Text>{Utils.i18n(this.props.data.location_name)}</Text>
                    <Text>{triggeredDate}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}

class AlarmSection extends Component {
    render() {
        return (
            <View style={styles.HEADER_WRAPPER}>
                <Text style={styles.HEADER_TEXT}>{Utils.i18n(this.props.data.name)}</Text>
            </View>
        )
    }
}

class Alerts extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: global._("ALERTS")
        }
    };

    static defaultProps = {
        open: true
    };

    constructor(props) {
        super(props);

        const isOpen = this.props.navigation.getParam("open", true);

        this.state = {
            alerts: [],
            alarms: [],
            _isLoaded: false,
            open: isOpen
        }

        this._sub = this.props.navigation.addListener(
            'didFocus',
            this._getAlerts
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    componentWillMount() {
        this._getAlerts(this.props.open);
    }

    componentWillReceiveProps(nextProps) {
        if (global.RELOAD_ALERTS) {
            global.RELOAD_ALERTS = false;
            this._getAlerts(nextProps.open);
        }
    }

    _getAlerts = () => {

        const isOpen = this.props.navigation.getParam("open", true);
        console.log("HER", isOpen);
        let method = API.getOpenAlerts;
        if (!isOpen) method = API.getMonitoredAlerts;

        method()
            .then((resp) => {
                this.setState({
                    alerts: resp.data.alerts,
                    alarms: resp.data.alarms,
                    _isLoaded: true
                })
            })
            .catch((resp) => {
            });
    }

    _showAlert = (alert, alarm) => {
        this.props.navigation.navigate("ALERT", {
            title: Utils.i18n(alarm.name),
            alert: alert,
            alarm: alarm
        })
    };

    render() {
        let items = [];

        this.state.alarms.forEach((alarm) => {
            items.push(
                <AlarmSection data={alarm} key={alarm.uuid}/>
            );

            this.state.alerts.forEach((alert) => {
                if (alert.alarm_id === alarm.uuid) {
                    items.push(
                        <Alert
                            data={alert}
                            key={alert.uuid}
                            onClick={this._showAlert}
                            alarm={alarm}/>
                    )
                }
            })
        });

        return (
            <View style={styles.VIEW_MAIN}>
                <Modal
                    visible={!this.state._isLoaded}
                    transparent={false}
                    onRequestClose={() => {
                        console.log("HERE")
                    }}>
                    <Text style={{textAlign: "center", paddingTop: 8}}>{global._("LOADING_ALERTS")}</Text>
                </Modal>
                <ScrollView>
                    {items}
                </ScrollView>


            </View>
        )
    }
}

export default Alerts;