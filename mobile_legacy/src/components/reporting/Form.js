import React, {Component} from "react";
import {
    View,
    ToolbarAndroid,
    Button,
    StyleSheet,
    ScrollView,
    Text,
    Image,
    TouchableHighlight,
    AsyncStorage,
    Alert,
    ToastAndroid
} from "react-native";

import Utils from "../utils";

import AssignmentField from "../fields/AssignmentField";

import SelectField from "../fields/SelectField";
import TextField from "../fields/TextField";
import TextArea from "../fields/TextAreaField";
import NumberField from "../fields/NumberField";
import DateField from "../fields/DateField";
import CalcDisplayField from "../fields/CalcDisplayField";
import TimeField from "../fields/f.time";

import moment from "moment";

const style = StyleSheet.create({
    BASE: {
        backgroundColor: "#F2F2F2",
        flexDirection: "column"
    },
    DETAILS: {
        flexDirection: "column",
        backgroundColor: "#FFFFFF",
        marginTop: 8,
        marginBottom: 16,
        marginLeft: 8,
        marginRight: 8,
        paddingBottom: 16
    },
    BUTTON_BAR: {
        flexDirection: "row",
        justifyContent: "flex-end",
        backgroundColor: "#333333",
        height: 40
    },
    LABEL: {
        fontSize: 12
    },
    HEADER: {
        fontWeight: "bold",
        fontSize: 14,
        marginBottom: 8,
        marginTop: 10,
        marginLeft: 8,
        marginRight: 8
    },
    FIELD: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        marginLeft: 8,
        marginTop: 8,
        marginRight: 8
    },
    SUB_LABEL: {
        fontSize: 12,
        marginLeft: 8,
        paddingLeft: 8,
        borderLeftWidth: 1,
        fontWeight: "bold",
        marginBottom: 8,
        marginTop: 14
    },
    IMAGE_BTN: {
        width: 40,
        height: 40,
        padding: 14,
        marginRight: 8
    },
    MAIN: {},
    ERROR: {
        backgroundColor: "#cc5254",
        padding: 4,
        marginTop: 3,
        marginBottom: 3
    },
    ERROR_TEXT: {
        fontSize: 12,
        color: "#FFFFFF"
    },
    BUTTON_BAR_FILL: {
        flex: 2
    }
})

const FIELDS = {
    matrix: null,
    text: TextField,
    textarea: TextArea,
    date: DateField,
    select: SelectField,
    number: NumberField,
    age: null,
    assignment: null,
    location: null,
    display: null,
    calculated: CalcDisplayField,
    time: TimeField
}

class Display extends Component {
    render() {
        return (
            <View>
                <Text style={style.LABEL}>{Utils.i18n(this.props.data.label)}</Text>

            </View>
        )
    }
}

class Header extends Component {
    render() {
        return (
            <View>
                <Text style={style.HEADER}>{Utils.i18n(this.props.data.label)}</Text>
            </View>
        )
    }
}


class Field extends Component {
    _onChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let FieldType = null;

        if (this.props.data.type == "display") {
            return <Display data={this.props.data}/>
        }

        if (this.props.data.type == "header") {
            return <Header data={this.props.data}/>
        }

        if (this.props.data.type == "matrix" || this.props.data.type == "row") {
            let fields = [];
            for (var i in this.props.data.fields) {
                fields.push({
                    ...this.props.data.fields[i],
                    name: `${this.props.data.name}.${i}`
                })
            }
            fields = fields.sort((a, b) => {
                if (a.order > b.order) return 1;
                if (a.order < b.order) return -1;
                return 0;
            });

            return (
                <View>
                    <Text style={style.SUB_LABEL}>{Utils.i18n(this.props.data.label)}</Text>
                    <View>
                        {fields.map(item => {
                            return <Field
                                data={item}
                                key={item.name}
                                sourceData={this.props.sourceData}
                                errors={this.props.errors}
                                onChange={this._onChange}/>
                        })}
                    </View>
                </View>
            )
        }

        if (FIELDS[this.props.data.type]) {
            FieldType = FIELDS[this.props.data.type]
        }

        let label = Utils.i18n(this.props.data.label);
        if (this.props.data.required) label += " *";

        let error;
        if (this.props.errors[this.props.data.name]) {
            error = (
                <View style={style.ERROR}>
                    <Text style={style.ERROR_TEXT}>{this.props.errors[this.props.data.name]}</Text>
                </View>
            )
        }

        let shouldVis = true;
        if (this.props.data.conditional_bool) {
            // Check to see if this field should be visible
            shouldVis = Utils.shouldBeVisible(this.props.sourceData, this.props.data);
        }

        if (!shouldVis) {
            return <View></View>
        }

        return (
            <View style={style.FIELD}>
                <Text style={style.LABEL}>{label}</Text>
                {error}
                {FieldType ?
                    <FieldType
                        onNextField={this.props.onNextField}
                        nextField={this.props.nextField}
                        errors={this.props.errors}
                        onChange={this._onChange}
                        name={this.props.data.name}
                        sourceData={this.props.sourceData}
                        data={this.props.data}/>
                    : null}
            </View>
        )
    }
}

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            data_date: null,
            location_id: null,
            errors: {},
            activeField: null
        }
    }

    _submit = () => {
        Alert.alert(
            "Submit Report",
            "Are you sure you want to submit this report? you will not be able to edit this report after submission.",
            [
                {
                    text: "Cancel", onPress: () => {
                    }
                },
                {text: "Submit", onPress: () => this._submitDo()}
            ]
        );
    }

    _submitDo = () => {
        let errors = this._validate();
        console.log(errors);
        if (errors) {
            console.log("ERRORS", errors);
            this.setState({
                errors: errors
            })
            ToastAndroid.show("There are errors in the form", ToastAndroid.SHORT);
            return;
        }

        let loc_name = null;

        this.props.assignment.assignments.forEach(item => {
            if (item.location_id == this.state.location_id) {
                loc_name = item.location_name;
            }
        })

        let data = {
            uuid: Utils.uuid(),
            location_id: this.state.location_id,
            data_date: this.state.data_date,
            data: this.state.data,
            form_id: this.props.assignment.form_id,
            version_id: this.props.assignment.version_id,
            created_date: new Date(),
            last_modified: new Date(),
            status: "SUBMITTED",
            meta: {
                form_name: this.props.assignment.form_name,
                location_name: loc_name,
                interval: this.props.assignment.interval
            }
        };

        try {
            AsyncStorage.getItem("@DATA:queue").then((resp) => {
                let storage;
                if (resp == null) storage = [];
                if (resp) storage = JSON.parse(resp);
                storage.push(data);
                AsyncStorage.setItem("@DATA:queue", JSON.stringify(storage))
                    .then((err) => {
                        ToastAndroid.show("Report submitted for syncing", ToastAndroid.SHORT);
                        this.props.cancel();
                    });
            })
        } catch (err) {
            console.log(err);
        }
    };

    _cancel = () => {
        this.props.cancel()
    };

    _onChange = (prop, value) => {
        let errors = this.state.errors;
        if (this.state.errors[prop]) {
            delete errors[prop];
        }

        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            },
            errors: errors
        })
    };

    _onRootChange = (prop, value) => {
        this.setState({
            [prop]: value
        })
    };

    _validate = () => {
        let errors = {};
        let hasErrors = false;

        if (this.props.assignment.location_aware) {
            if (!this.state.location_id || this.state.location_id == "") {
                hasErrors = true;
                errors.location_id = "Please specify a location for the report";
            }
        }

        if (!Utils.isSet(this.state.data_date)) {
            hasErrors = true;
            errors.data_date = "Please specify a valid report date";
        }

        if (Utils.isSet(this.state.data_data)) {
            if (Utils.isFuture(this.state.data_date)) {
                hasErrors = true;
                errors.data_date = "You can not submit a report in the future"
            }
        }

        if (this.props.assignment.single_report_context) {
            let dates;

            if (Utils.isSet(this.state.location_id)) {
                // Check if it's a duplicated
                let assign;
                this.props.assignment.assignments.forEach(item => {
                    if (item.location_id = this.state.location_id) {
                        assign = item;
                    }
                })

                if (assign) dates = assign.existing;
            }

            if (dates && Utils.isSet(this.state.data_date)) {
                if (dates.indexOf(this.state.data_date) >= 0) {
                    errors.data_date = "You can not submit a report with a future date";
                    hasErrors = true;
                }
            }
        }

        for (let key in this.props.assignment.definition) {
            let field = this.props.assignment.definition[key];

            if (field.required && !field.fields) {
                if (!Utils.isSet(this.state.data[key]) && Utils.shouldBeVisible(this.state.data, field)) {
                    errors[key] = "Please provide a valid value";
                    hasErrors = true;
                }
            }

            if (field.fields) {
                let required = [];
                for (let i in field.fields) {
                    if (field.fields) {
                        for (var t in field.fields[i].fields) {
                            let fL = field.fields[i].fields[t];
                            if (fL.required) {
                                required.push([`${key}.${i}.${t}`, fL])
                            }
                        }
                    }
                }

                required.forEach(req => {
                    if (!Utils.isSet(this.state.data[req[0]]) && Utils.shouldBeVisible(this.state.data, req[1])) {
                        errors[req[0]] = "Please provide a valid value";
                        hasErrors = true;
                    }
                })
            }
        }

        console.warn(errors);

        if (hasErrors) return errors;
        return false;

    };

    _save = () => {
        let loc_name = null;

        this.props.assignment.assignments.forEach(item => {
            if (item.location_id == this.state.location_id) {
                loc_name = item.location_name;
            }
        });

        let data = {
            location_id: this.state.location_id,
            data_date: this.state.data_date,
            data: this.state.data,
            form_id: this.props.assignment.form_id,
            version_id: this.props.assignment.version_id,
            created_date: new Date(),
            last_modified: new Date(),
            meta: {
                form_name: this.props.assignment.form_name,
                location_name: loc_name
            }
        };

        try {
            AsyncStorage.getItem("@DATA:drafts").then((resp) => {
                let storage;
                if (resp == null) storage = [];
                if (resp) storage = JSON.parse(resp);
                storage.push(data);
                AsyncStorage.setItem("@DATA:drafts", JSON.stringify(storage))
                    .then((err) => {
                        ToastAndroid.show("Report saved to drafts", ToastAndroid.SHORT);
                        this.props.cancel();
                    });
            })
        } catch (err) {
            console.log(err);
        }

    };

    _onNextField = (nextField) => {
        this.setState({
            activeField: nextField
        })
    };

    render() {
        let fields = [];
        for (let i in this.props.assignment.definition) {
            fields.push({
                ...this.props.assignment.definition[i],
                name: i
            })
        }

        fields = fields.sort(function (a, b) {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        let dispFields = fields.map((item, index) => {
            let nextField,
                focused = false;
            if (this.state.activeField == item.name) focused = true;
            if (fields[index + 1]) nextField = fields[index + 1].name;
            return <Field
                errors={this.state.errors}
                onChange={this._onChange}
                onNextField={this._onNextField}
                nextField={nextField}
                focused={focused}
                sourceData={this.state.data}
                key={item.name}
                data={item}/>
        });

        return (
            <View style={style.BASE}>
                <View style={style.MAIN}>
                    <ScrollView>
                        <View style={style.DETAILS}>
                            <Text style={style.HEADER}>Report Details*</Text>

                            {this.props.assignment.location_aware ?
                                <View>
                                    {this.state.errors.location_id ?
                                        <View style={style.ERROR}>
                                            <Text style={style.ERROR_TEXT}>{this.state.errors.location_id}</Text>
                                        </View>
                                        : null}
                                    <AssignmentField
                                        onChange={this._onRootChange}
                                        name="location_id"
                                        value={this.state.location_id}
                                        data={this.props.assignment}/>
                                </View>
                                : null}

                            <View style={style.FIELD}>
                                <Text style={style.LABEL}>Report Date *</Text>
                                {this.state.errors.data_date ?
                                    <View style={style.ERROR}>
                                        <Text style={style.ERROR_TEXT}>{this.state.errors.data_date}</Text>
                                    </View>
                                    : null}
                                <DateField
                                    name="data_date"
                                    data={{date_type: this.props.assignment.interval || "DAY"}}
                                    value={this.state.data_date}
                                    onChange={this._onRootChange}/>
                            </View>
                        </View>
                        <View style={style.DETAILS}>
                            {dispFields}
                        </View>
                        <View>
                            <Text value={JSON.stringify(this.props.assignment)} multiline={true}/>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

export default Form;
