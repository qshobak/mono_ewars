import CONSTANTS from "../constants";

const FR = {
    FORMS: "Formulaire",
    ALERT_LOG: "Journal d'alerte",
    SETTINGS_UPDATED: "Paramètres mis à jour",
    AN_ERROR_OCCURRED: "Une erreur est survenue",
    MOBILE_NO: "Téléphone portable #",
    LANGUAGE: "Langue",
    SAVE_CHANGES: "Enregistrer la(les) modification(s)",

    UPDATE_TITLE: "Mise à jour disponible",
    UPDATE_MESSAGE: "Une nouvelle mise à jour est disponible. Installer?",
    UPDATE_CANCEL: "Annuler",
    UPDATE_INSTALL: "Installer",
    ABOUT_EWARS: "À propos de EWARS",
    APP_VERSION: "Version de l'application",

    ABOUT_1: "EWARS est un outil simple, basé sur le terrain, conçu pour aider à la surveillance des maladies, l'alerte et la réponse en cas d'urgence.",
    ABOUT_2: "Conçu pour les utilisateurs de terrain et fonctionner dans des environnements distants et difficiles d'accès.",
    ABOUT_3: "Peut être rapidement configuré et déployé dans les 48 heures suivant la déclaration d'une urgence.",
    ABOUT_4: "Pour plus d'informations, veuillez contacter info@ewars.ws",
    BUTTON_VISIT_EWARS: "Visitez EWARS-project.org",
    BUTTON_CHECK_UPDATES: "Rechercher les mises à jour",
    NO_INTERNET_CONNECTION: "Pas de connexion Internet",

    PROFILE: "Profile",

    NAME: "Nom",
    EMAIL: "Email",
    ORGANIZATION: "Organisation",
    ACCOUNT: "Compte",
    RECORD_MANAGEMENT: "Gestion des enregistrements",
    OTHER: "Autre",
    ABOUT: "À propos",
    SIGN_OUT: "Déconnexion",
    SYNCING_WAIT: "Synchronisation: Veuillez patienter pendant la mise à jour de vos données...",
    PROBLEM_RECORDS: "Problème d'enregistrements",
    SENT: "Envoyé",
    DRAFTS: "Brouillons",
    QUEUE: "File d'attente",
    OPEN_ALERTS: "Ouvrir les alertes",
    MONITORED_ALERTS: "Alertes surveillées",
    SIGN_OUT_MESSAGE: "Êtes-vous certain de vouloir vous déconnecter? Vous aurez besoin d'une connexion de données pour vous reconnecter à l'application.",
    CANCEL: "Annuler",
    SYNC_ERROR_RECORDS: "Certains rapports n'ont pas pu être synchronisés, consultez la section Enregistrements de problèmes pour plus d'informations.",
    SYNC_COMPLETE: "Synchronisation terminée",
    OK: "Ok",
    MISSING_GPS: "Certains de vos emplacements assignés manquent des coordonnées GPS, appuyez ici pour les mettre à jour.",
    LOADING_FORMS: "Chargement des formulaires...",
    SETTINGS: "Paramètres",
    ERR_DUPLICATE_RECORD: "Un enregistrement existe déjà pour cet emplacement et à cette date",
    ERR_LOCATION_REQUIRED: "Un emplacement est requis",
    ERR_DATA_DATE_REQUIRED: "Une date d'enregistrement est requise",
    ERR_FORM_CHANGE: "Le formulaire pour cet enregistrement a changé, veuillez le mettre à jour et le soumettre à nouveau",
    LOADING: "Chargement...",
    NO_DATA_CONNECTION: "Aucune connexion de données disponible",
    SYNC_ERR_1: "Une erreur est survenue lors de la synchronisation, veuillez réessayer plus tard",
    CLEAR_QUEUE: "Effacer la file?",
    CLEAR_QUEUE_MESSAGE: "Êtes-vous sûr de vouloir effacer la file d'attente? Tous les articles seront supprimés et ne seront pas synchronisés avec EWARS",
    CLEAR: "Effacer",
    DELETE_RECORD: "Supprimer l'enregistrement?",
    DELETE_RECORD_MESSAGE: "Supprimer cet enregistrement le supprimera complètement de votre téléphone.",
    BUTTON_DELETE_RECORD: "Supprimer l'enregistrement",
    DELETING_RECORD: "Suppression d'un enregistrement...",
    RECORD_DELETED: "Enregistrement supprimé",
    DEQUEUE_RECORD: "L'enregistrement de la file d'attente?",
    DEQUEUE_RECORD_MESSAGE: "En désactivant cet enregistrement, vous le déplacerez dans le dossier Brouillons et l'enregistrement ne sera pas envoyé lors de la prochaine synchronisation.",
    BUTTON_DEQUEUE: "Enregistrement désactivé",
    RECORD_DEQUEUED: "Record dequeued",
    QUEUE_NO_RECORDS: "Aucun enregistrement en attente pour le moment",
    SYNC: "Synchronisation",
    CLEAR_QUEUED: "Effacer la file",
    MOVE_TO_DRAFTS: "Déplacer vers les brouillons",
    ASSIGNMENT: "Affectation",
    ACTIONS: "Actions",
    CREATE_NEW: "Créer Nouveau",
    BROWSE: "Parcourir",
    QUEUED: "En file d'attente",
    ALERTS: "Alertes",
    ALERT: "Alerte",
    LOADING_ALERTS: "Chargement des alertes...",
    HIGH: "Risque élevé",
    LOW: "Faible risque",
    MODERATE: "Risque modéré",
    SEVERE: "Risque grave",
    PENDING: "En attente",
    INCOMPLETE: "Pas terminé",
    COMPLETED: "Terminé",
    COMPLETE: "Achevée",
    VERIFICATION: "Verification",
    RISK_ASSESS: "L'évaluation des risques",
    RISK_CHAR: "Caractérisation du risque",
    OUTCOME: "Résultat",
    LOADING_ALERT: "Chargement de l'alerte...",
    ALERT_DATE: "Date d'alerte",
    ACTIVITY: "Activité",
    TRIGGERING_RECORD: "Enregistrement déclencheur",
    USERS: "Utilisateurs",
    FORM_ERRORS: "Il y a des erreurs dans le formulaire",
    SUBMIT_NOW: "Soumettre maintenant?",
    SUBMIT_MESSAGE: "Nous avons détecté que vous disposez d'une connexion Internet active. Souhaitez-vous envoyer ce document maintenant?",
    BUTTON_SUBMIT_DEFER: "Non, file d'attente pour plus tard",
    BUTTON_SUBMIT_NOW: "Soumettre maintenant",
    RECORD_QUEUED: "Enregistrement ajouté à la file d'attente, veuillez synchroniser pour soumettre votre enregistrement.",
    SUBMISSION_FAILED_DATE: "La soumission a échoué, un enregistrement avec cette date existe déjà",
    SUBMISSION_SUCCESS: "Enregistrement soumis avec succès",
    ALERT_SUBMIT_FAILED: "La soumission d'enregistrement a échoué",
    ALERT_SUBMIT_FAILED_MESSAGE: "L'enregistrement n'a pas pu être soumis immédiatement.",
    BUTTON_TRY_AGAIN: "Réessayer",
    ERR_NO_LOCATION: "Veuillez spécifier un emplacement pour l'enregistrement",
    ERR_NO_DATE: "Veuillez spécifier une date d'enregistrement valide",
    ERR_NO_FUTURE: "Vous ne pouvez pas soumettre d'enregistrement à l'avenir",
    ERR_DUPLICATE: "Un enregistrement pour cet emplacement/cette date existe déjà",
    ERR_REQUIRED: "S'il vous plaît fournir une valeur valide",
    DELETE_DRAFT: "supprimer le brouillon?",
    ALERT_DELETE_DRAFT: "Projet enregistré",
    ALERT_DELETE_DRAFT_MESSAGE: "Êtes-vous sûr de vouloir supprimer ce brouillon?",
    DELETE: "Supprimer",
    RECORD_DETAILS: "Détails de l'enregistrement",
    SUBMIT: "Soumettre",
    SAVE_AS_DRAFT: "Enregistrer comme brouillon",
    NONE_SELECTED: "Aucune sélection",
    ASSIGNMENT_LOCATION: "Lieu d'affectation",
    LOCATION: "Lieu",
    CLEAR_LOG: "Effacer le journal?",
    CLEAR_LOG_MESSAGE: "L'enregistrement de ces éléments envoyés ne sera plus disponible sur ce téléphone.",
    BUTTON_CLEAR_SENT: "Effacer le journal envoyé",
    ALERT_WORKFLOW: "Flux d'alerte",
    CAMERA_TITLE: "Permission d'utiliser une caméra",
    CAMERA_MESSAGE: "Nous avons besoin de votre permission pour utiliser la caméra de votre téléphone pour scanner le code à barres de la demande",
    BARCODE_SCANNER: "Scanner de codes-barres",
    ALERT_USERS: "Lanceurs d'alerte",
    ERR_OUTCOME: "Veuillez spécifier un résultat pour l'alerte",
    ERR_UNKNOWN: "Une erreur non gérée s'est produite, veuillez réessayer plus tard",
    PLEASE_VERIFICATION: "Veuillez finaliser la vérification",
    PLEASE_RISK_ASSESS: "Veuillez finaliser l'évaluation des risques",
    PLEASE_RISK_CHAR: "Veuillez finaliser la caractérisation des risques",
    PLEASE_OUTCOME: "Veillez finaliser le résultat",
    DISCARD: "Ne pas tenir compte",
    MONITOR: "Moniteur",
    RESPOND: "Répondre",
    COMMENTS: "Commentaires",
    BUTTON_SUBMIT_OUTCOME: "Soumettre le résultat",
    ERR_HAZARD: "Veuillez fournir une évaluation des dangers",
    ERR_EXPOSURE: "Veuillez fournir une évaluation de l'exposition",
    ERR_CONTEXT: "Veuillez fournir une évaluation du contexte",
    HAZARD_ASSESSMENT: "Évaluation des dangers",
    EXPOSURE_ASSESSMENT: "Évaluation de l'exposition",
    CONTEXT_ASSESSMENT: "Évaluation du contexte",
    BUTTON_SUBMIT_ASSESSMENT: "Soumettre une évaluation",
    LIKELIHOOD: [
        "Est prévu de se produire dans la plupart des circonstances (ex. probabilité of 95% ou plus) ",
        "Se produira probablement dans la plupart des circonstances(e.g. une probabilité entre 70% et 94%) ",
        "Se produira une partie du temps (ex. une probabilité entre 30% et 69%) ",
        "Pourrait se produire une partie du temps (ex. une probabilité entre 5% et 29%) ",
        "Pourrait se produire dans des circonstances exceptionnelles (ex. a probabilité moins que 5%)"
    ],
    CONSEQUENCES: [
        [
            "- Impact limité sur la population affectée",
            "- Peu de perturbation des activités et des services normaux",
            "- Les réponses de routine sont adéquates et il n'est pas nécessaire de mettre en œuvre des mesures de contrôle supplémentaires",
            "- Peu de coûts supplémentaires pour les autorités et les parties prenantes"
        ],
        [
            "- Impact mineur pour une petite population ou un groupe à risque",
            "- Perturbation limitée des activités et services habituels",
            "- Un petit nombre de mesures de contrôle supplémentaires seront nécessaires qui nécessitent des ressources minimales",
            "- Une augmentation des coûts pour les autorités et les parties prenantes"
        ],
        [
            "- Impact modéré en tant que grande population ou groupe à risque est affecté",
            "- Perturbation modérée des activités et services normaux",
            "- Certaines mesures de contrôle supplémentaires seront nécessaires et certaines d'entre elles nécessitent des ressources modérées à mettre en œuvre",
            "- Augmentation modérée des coûts pour les autorités et les parties prenantes"
        ],
        [
            "- Impact majeur pour une petite population ou un groupe à risque",
            "- Perturbation majeure des activités et services habituels",
            "- Un grand nombre de mesures de contrôle supplémentaires seront nécessaires et certaines d'entre elles nécessitent des ressources importantes pour être mises en œuvre",
            "- Augmentation significative des coûts pour les autorités et les parties prenantes"
        ],
        [
            "- Impact important pour un grand groupe de population ou à risque",
            "- Perturbation grave des activités et services habituels",
            "- Un grand nombre de mesures de contrôle supplémentaires seront nécessaires et la plupart d'entre elles nécessitent des ressources importantes pour être mises en œuvre",
            "- Augmentation importante des coûts pour les autorités et les parties prenantes"
        ]
    ],
    RISK_LOW_MESSAGE: "Géré selon les protocoles de réponse standard, les programmes de contrôle de routine et la réglementation (ex. Suivi par des systèmes de surveillance de routine) ",
    RISK_MODERATE_MESSAGE: "Les rôles et la responsabilité pour la réponse doivent être spécifiés. Suivis spécifiques ou contrôle des mésures requises (ex. surveillance renforcée, campagnes de vaccination supplémentaires) ",
    RISK_HIGH_MESSAGE: "L'attention des hautes autorités nécessaire: il peut être nécessaire d'établir des structures de commandement et de contrôle; une série de mesures de contrôle supplémentaires seront nécessaires dont certaines peuvent avoir des conséquences importantes ",
    RISK_SEVERE_MESSAGE: "Réponse immédiate requise même si l'événement est signalé en dehors des heures normales de travail. L'attention immédiate de la haute direction est nécessaire (par exemple, la structure de commandement et de contrôle devrait être établie en quelques heures); the implementation of control measures with serious consequences is highly likely ",
    LOW_RISK: "Faible risque",
    MODERATE_RISK: "Risque modéré",
    HIGH_RISK: "Risque élevé",
    SEVERE_RISK: "Risque grave",
    ERR_LIKELIHOOD: "Veuillez spécifier une probabilité",
    ERR_CONSEQUENCES: "Veuillez préciser les conséquences",
    ALMOST_CERTAIN: "Presque certain",
    HIGHLY_LIKELY: "Hautement probable",
    LIKELY: "Probable",
    UNLIKELY: "Improbable",
    VERY_UNLIKELY: "Très improbable",
    MINIMAL: "Minimal",
    MINOR: "Mineur",
    MAJOR: "Majeur",
    RISK: "Risque",
    BUTTON_SUBMIT_CHAR: "Soumettre la caractérisation",
    ERR_VERIFICATION_OUTCOME: "Veuillez spécifier un résultat",
    ERR_TRIAGE: "Veuillez entrer une évaluation de triage",
    VERIFICATION_COMMENTS: "Commentaires de vérification",
    VERIFICATION_OUTCOME: "Résultat de la vérification",
    TRIAGE: "Triage",
    START_RISK_ASSESSMENT: "Commencer l'évaluation des risques",
    BUTTON_SUBMIT_VERIFICATION: "Soumettre la vérification",
    LOCATION_MANAGEMENT: "Gestion de la localisation",
    LOCATION_EDITOR: "Éditeur de la localisation",
    LATITUDE: "Latitude",
    LONGITUDE: "Longitude",
    BUTTON_USE_LOCATION: "Utiliser l'emplacement actuel",
    ERR_LOGIN: "email/mot de passe incorrecte",
    ERR_MULTI_ACCOUNT: "Les utilisateurs multi-comptes ne sont actuellement pas pris en charge, ils seront pris en charge dans une prochaine version.",
    PASSWORD: "Mot de passe",
    LOGIN: "S'identifier",
    ATTEMPTING_LOGIN: "Tentative de connexion...",
    DATA_UPGRADE_COMPLETE: "Mise à niveau des données terminée",
    ERR_UNK_SYNC: "Une erreur est survenue, veuillez essayer la synchronisation",
    DOWNLOADING_UPDATE: "Téléchargement de la mise à jour...",
    INSTALLING_UPDATE: "Installation de mise à jour...",
    EWARS_UPDATED: "EWARS est à jour",
    UPDATE_INSTALLED: "Mise à jour installée",
    NO_CONNECTION: "Pas de connection",
    CONNECTED: "Connecté",
    BUTTON_SCAN_BARCODE: "Scanner le code à barres",
    RECORD_DATE: "Date d'enregistrement",

    BUTTON_MOVE_TO_DRAFTS: "Déplacer vers les brouillons",
    PERMISSION_GPS_TITLE: "Permis d'accès au site",
    PERMISSION_GPS_MESSAGE: "EWARS Mobile a besoin d'accéder à votre position GPS pour effectuer cette tâche",
    ERR_GPS_PERMISSIONS: "EWARS n'a pas pu obtenir l'autorisation d'accéder à votre position GPS",
    GETTING_LOCATION: "Obtenir l'emplacement ...",
    ERR_LOCATION_RETRIEVAL: "Une erreur s'est produite lors de la récupération de votre position. Veuillez réessayer.",
    PERMISSION_CAMERA_TITLE: "Autorisation d'accès à la caméra",
    PERMISSION_CAMERA_MESSAGE: "EWARS doit avoir accès à votre caméra pour effectuer cette tâche",
    ERR_CAMERA_PERMISSION: "EWARS n'a pas pu obtenir l'autorisation d'accéder à votre caméra",
    ERR_CAMERA: "Une erreur s'est produite lors de l'utilisation de l'appareil photo pour la lecture de codes à barres",

    ALERT_QUEUE_TITLE: "Dossier de file d'attente",
    ALERT_QUEUE_MESSAGE: "Vous n'avez actuellement pas de connexion active à EWARS, cette soumission a été ajoutée à la file d'attente, allez dans la file d'attente et appuyez sur 'Sync' lorsque vous avez une connexion Internet active pour soumettre cet enregistrement.",
    SELECT_DATE: "Sélectionner une date",
    NO_SENT_RECORDS: "Aucun enregistrement envoyé",
    FORM: "Forme",
    SUBMITTING_RECORD: "Soumettre un enregistrement ...",
    NO_DRAFTS_AVAILABLE: "Pas de brouillons disponibles",

    DRAFT_SAVED: "Projet enregistré",
    CHECKING_CONNECTION: "Vérification de la connexion"


};

export default FR;
