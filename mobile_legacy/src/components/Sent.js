import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    TouchableHighlight,
    NetInfo,
    ToastAndroid,
    Alert,
    Modal
} from "react-native";

import Utils from "./utils";
import API from "./api";
import Storage from "./lib/storage";

import {
    ActionButton,
    LineSpacer
} from "./common";

import styles from "./STYLES";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    },
    BUTTON_BAR: {
        flexDirection: "row",
        height: 48,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2",
        alignItems: "flex-start",
        paddingRight: 8,
        paddingLeft: 8,
        paddingTop: 8
    },
    SYNC: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        borderWidth: 1,
        borderColor: "#333"
    },
    QUEUE_TITLE: {
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 8,
        backgroundColor: "#333",
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderWidth: 1,
        borderColor: "#333",
        marginLeft: 8
    },
    QUEUE_TEXT: {
        color: "#FFFFFF"
    }
})

class Item extends Component {
    constructor(props) {
        super(props);
    }

    _onPress = () => {
        this.props.onSelectQueueItem(this.props.data);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._onPress}>
            <View style={styles.ROW}>
                <Text>{Utils.i18n(this.props.data.meta.form_name)}</Text>
                {this.props.data.meta.location_name ?
                    <Text>{Utils.i18n(this.props.data.meta.location_name)}</Text>
                    : null}
                {this.props.data.data_date ?
                    <Text>{Utils.DATE(this.props.data.data_date, this.props.data.meta.interval)}</Text>
                    : null}
            </View>
            </TouchableHighlight>
        )
    }
}

class Sent extends Component {
    static navigationOptions = () => {
        return {
            title: global._("SENT")
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            showControls: false
        };
        this._load();
    }


    _load = () => {
        const data = this.props.navigation.getParam("data", {});
        Storage.getSent(data.form ? (data.form.id || null) : null)
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _clear = () => {
        Alert.alert(
            global._("CLEAR_LOG"),
            global._("CLEAR_LOG_MESSAGE"),
            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                    }
                },
                {
                    text: global._("CLEAR"),
                    onPress: this._clearData}
            ]
        )
    };

    _clearData = () => {
        const data = this.props.navigation.getParam("data", {});
        Storage.clearSent(data.form ? (data.form.id || null) : null)
            .then(() => {
                this._load();
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _closeModal = () => {
        this.setState({
            showControls: false
        })
    };

    _onSelectQueueItem = (data) => {
        this.setState({
            showControls: true,
            selectedItem: data
        })
    };

    render() {
        let view;

        if (this.state.data.length > 0) {
            view = this.state.data.map((item, idx) => {
                return <Item
                    onSelectQueueItem={this._onSelectQueueItem}
                    key={idx}
                    data={item}/>
            });
        } else {
            view = (
                <Text style={styles.PLACEHOLDER}>{global._("NO_SENT_RECORDS")}</Text>
            )
        }

        return (
            <View>
                <View style={styles.ACTION_WRAP}>
                    <ActionButton
                        title={global._("BUTTON_CLEAR_SENT")}
                        onPress={this._clear}/>
                </View>
                <View>
                    <ScrollView>
                        {view}
                    </ScrollView>
                </View>
                <Modal
                    transparent={false}
                    style={styles.MODAL_STYLE}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.showControls}>
                    <View style={styles.ACTIONS_MODAL}>
                        <ActionButton
                            title={global._("BUTTON_MOVE_TO_DRAFTS")}
                            color="grey"
                            onPress={this._moveRecordToDrafts}/>
                        <ActionButton
                            title={global._("DELETE_RECORD")}
                            color="red"
                            onPress={this._deleteRecordFromQueue}/>
                        <LineSpacer/>
                        <ActionButton
                            title={global._("CANCEL")}
                            color="grey"
                            onPress={this._closeModal}/>
                    </View>

                </Modal>
            </View>
        )
    }
}

export default Sent;