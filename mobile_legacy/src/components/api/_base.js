import {URI} from "../constants";
import Storage from "../lib/storage";

class BaseAPI {

    static _call = (method, args, kwargs) => {
        return new Promise((resolve, reject) => {
            Storage.getUser()
                .then((resp) => {


                    let request = new XMLHttpRequest();
                    request.open("POST", URI + "/api/_m", true);

                    request.onload = function () {
                        if (this.status >= 200 && this.status < 300) {
                            let res = JSON.parse(request.responseText);
                            if (res.recover) {
                                Storage.setToken(res.token)
                                    .then((result) => {
                                        global.TOKEN = res.token;
                                        resolve(res.data);
                                    })
                            } else {
                                resolve(JSON.parse(request.responseText));
                            }
                        } else {
                            reject({
                                status: this.status,
                                statusText: request.statusText
                            })
                        }
                    };

                    request.onerror = function () {
                        reject({
                            status: this.status,
                            statusText: request.statusText
                        })
                    };

                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("Accept", "application/json");
                    request.setRequestHeader("E_C_TYPE", "M");
                    // Submit user token along with the request

                    request.setRequestHeader("EC_TOKEN", global.TOKEN);
                    if ([null, undefined, ""].indexOf(global.TOKEN) >= 0) {
                        if ([null, undefined, ""].indexOf(resp["@SETTINGS:id"]) < 0) {
                            request.setRequestHeader("E_C_RECOVERY_ID", resp['@SETTINGS:id']);
                            request.setRequestHeader("E_C_RECOVERY_EMAIL", resp['@SETTINGS:email']);
                        }
                    }

                    request.send(JSON.stringify([
                        method,
                        args,
                        kwargs
                    ]));
                })
        })
    };
}

export default BaseAPI;