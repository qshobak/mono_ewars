let state;

class ApplicationState {
    constructor() {
        state = this;

        this.assignments = [];
        this.manifests = [];
        this.forms = [];
        this.form_versions = [];
        this.locations = [];

        this.isConnected = false;

        this.lastSync;
    }
}

export default ApplicationState;
export {state, ApplicationState};