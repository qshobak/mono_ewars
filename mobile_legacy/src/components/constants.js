//export const URI = "http://10.0.2.2:9010";
//export const URI = "http://192.168.1.18x4:9000";
export const URI = "https://ewars.ws";
export default CONSTANTS = {
    // User Types
    SUPER_ADMIN: "SUPER_ADMIN",
    INSTANCE_ADMIN: "INSTANCE_ADMIN",
    GLOBAL_ADMIN: "GLOBAL_ADMIN",
    ACCOUNT_ADMIN: "ACCOUNT_ADMIN",
    USER: "USER",
    ORG_ADMIN: "ORG_ADMIN",
    LAB_USER: "LAB_USER",
    REGIONAL_ADMIN: "REGIONAL_ADMIN",
    COUNTRY_SUPERVISOR: "ACCOUNT_ADMIN",
    BOT: "BOT",
    API_USER: "API_USER",

    // General states
    DISABLED: "DISABLED",

    // Reporting intervals
    NONE: "NONE",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR",

    // Filterable time periods
    ONEM: "1M",
    THREEM: "3M",
    SIXM: "6M",
    ONEY: "1Y",
    YTD: "YTD",
    ALL: "ALL",

    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    DRAFT: "DRAFT",
    DELETED: "DELETED",

    DEFAULT: "DEFAULT",

    DESC: "DESC",
    ASC: "ASC",

    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD",

    // Icons
    ICO_DASHBOARD: "fa fa-dashboard",
    ICO_USERS: "fa fa-users",
    ICO_USER: "fa fa-user",
    ICO_REPORT: "fa fa-file",
    ICO_REPORTS: "fa fa-clipboard",
    ICO_GRAPH: "fa fa-bar-chart",
    ICO_INVESTIGATION: "fa fa-search",
    ICO_RELATED: "fa fa-link",
    ICO_GEAR: "fa fa-gear",
    ICO_CARET_DOWN: "fa fa-caret-down",
    ICO_SEND: "fa fa-envelope-o",
    ICO_SUBMIT: "fa-send",
    ICO_SAVE: "fa-save",
    ICO_CANCEL: "fa-close",

    // Chart Types
    SERIES: "SERIES",
    BAR: "BAR",
    MAP: "MAP",
    PIE: "PIE",
    SCATTER: "SCATTER",

    // Indicator types
    CUSTOM: "CUSTOM",

    OPEN: "OPEN",
    CLOSED: "CLOSED",

    // Form types
    PRIMARY: "PRIMARY",
    INVESTIGATIVE: "INVESTIGATIVE",
    LAB: "LAB",
    SURVEY: "SURVEY",

    // Alert stages
    VERIFICATION: "VERIFICATION",
    RISK_ASSESS: "RISK_ASSESS",
    RISK_CHAR: "RISK_CHAR",
    OUTCOME: "OUTCOME",

    // OUtcomes

    // Stage states
    DISCARD: "DISCARD",
    MONITOR: "MONITOR",
    ADVANCE: "ADVANCE",
    COMPLETED: "COMPLETED",
    PENDING: "PENDING",
    RESPOND: "RESPOND",
    DISCARDED: "DISCARDED",
    AUTODISCARDED: "AUTODISCARDED",

    // Risk levels
    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",

    // Colors
    RED: "red",
    GREEN: "green",
    ORANGE: "orange",
    YELLOW: "yellow",

    // Default Formats
    DEFAULT_DATE: "YYYY-MM-DD",

    // Dates
    MONTHS_LONG: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    MONTHS_SHORT: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    CAL_DAYS_LABELS: ["Sun", "Mon", "Tue", "Wed", "Thurs", "Fri", "Sat"],
    CAL_DAYS_IN_MONTH: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
}
