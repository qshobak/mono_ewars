import {
    View,
    StyleSheet,
    Text,
    ToolbarAndroid
} from "react-native";
import React, {Component} from 'react';
import {RNCamera} from 'react-native-camera';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    header: {
        height: 30,
        backgroundColor: "white"
    },
    capture: {
        flex: 2,
    }
});

export default class BarcodeScanner extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    _read = (data) => {
        console.log(data);
        this.props.onChange(this.props.name, data.data);
    };

    _action = () => {
        this.props.onClose();
    };

    render() {
        let actions = [
            {
                title: global._("CANCEL"),
                show: "always"

            }
        ];

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <ToolbarAndroid
                        height={30}
                        onActionSelected={this._action}
                        title={global._("Barcode Scanner")}
                        actions={actions}/>
                </View>
                <View style={styles.capture}>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        type={RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        onBarCodeRead={this._read}
                        permissionDialogTitle={global._("CAMERA_TITLE")}
                        permissionDialogMessage={global._("CAMERA_MESSAGE")}
                    />
                </View>
            </View>
        )

    }
}