import {StyleSheet} from "react-native";

export default StyleSheet.create({
    HEADER_WRAPPER: {
        paddingTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#CCCCCC",
        display: "flex",
        flexDirection: "row",
        flex: 1
    },
    HEADER_TEXT: {
        fontSize: 12,
        fontWeight: "bold"

    },
    LOCATION_ICON_LEFT: {
        width: 30,
        flex: 1,
        maxWidth: 30
    },
    LOCATION_ICON_RIGHT: {
        width: 30,
        flex: 1,
        maxWidth: 30
    },
    LOCATION_TEXT: {
        flex: 2
    },
    LIST_ITEM: {
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        borderBottomColor: "#CCCCCC",
        borderBottomWidth: 1,
        paddingTop: 12,
        paddingBottom: 12
    },
    ACT_ITEM: {
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        borderBottomColor: "#CCCCCC",
        borderBottomWidth: 1,
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 8,
        paddingRight: 8
    },
    LIST_ITEM_GEOM: {
        backgroundColor: "#ebcb8b",
        flexDirection: "row",
        borderBottomColor: "#CCCCCC",
        borderBottomWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        marginLeft: 8,
        marginRight: 8,
        marginTop: 8,
        borderRadius: 6
    },
    LIST_ITEM_LEFT: {
        flex: 4,
        paddingLeft: 8
    },
    LIST_ITEM_ICON: {
        flex: 1,
        paddingLeft: 8,
        maxWidth: 20,
        paddingRight: 8
    },
    LIST_ITEM_RIGHT: {
        flex: 1,
        paddingRight: 8,
        maxWidth: 20
    },
    LIST_ITEM_CARET: {
        width: 20,
        height: 20
    },
    LIST_ITEM_TEXT: {
        fontSize: 12
    },
    LIST_ITEM_TEXT_GEOM: {
        fontSize: 12,
        color: "#333333"
    },
    NAV_HEADER: {
        backgroundColor: "rgb(49,54,148)",
        height: 50
    },
    NAV_BTN: {
        backgroundColor: "transparent",
        height: 50,
        width: 45
    },
    NAV_BTN_RIGHT: {
        backgroundColor: "transparent",
        height: 50,
        width: 45
    },
    NAV_IMAGE: {
        height: 30,
        width: 30,
        marginTop: 8,
        marginLeft: 8,
        tintColor: "black"
    },
    NAV_IMAGE_RIGHT: {
        height: 30,
        width: 30,
        marginTop: 8,
        marginRight: 8
    },
    HEADER_BTN: {
        height: 40,
        width: 40,
        paddingTop: 8,
        paddingLeft: 4
    },
    NAV_TEXT: {
        textAlign: "left",
        color: "white",
        paddingTop: 20,
        fontWeight: "bold"
    },
    VIEW_MAIN: {
        backgroundColor: "#F2F2F2",
        flex: 1
    },
    ABOUT_MAIN: {
        padding: 8
    },
    FOOT: {
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 8,
        paddingRight: 8
    },
    FOOT_TEXT: {
        fontSize: 10,
        color: "#CCCCCC",
        textAlign: "center"
    },
    ASSIGN_HEADER: {
        backgroundColor: "#FFFFFF",
        borderBottomWidth: 1,
        borderBottomColor: "#CCCCCC",
        paddingLeft: 8,
        paddingTop: 2,
        paddingBottom: 4
    },
    ASSIGN_HEADER_TEXT: {
        fontSize: 12
    },
    BUTTON_BAR_WRAP: {
        flex: 1
    },
    SYNC_BUTTON: {
        textAlign: "center"
    },
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    },
    BUTTON_BAR: {
        flexDirection: "row",
        height: 40,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2",
        alignItems: "flex-start"
    },
    SYNC: {
        backgroundColor: "#F2F2F2",
        padding: 5,
        marginTop: 3,
        marginBottom: 3,
        marginLeft: 8
    },
    PLACEHOLDER: {
        textAlign: "center",
        paddingTop: 10,
        paddingLeft: 16,
        paddingRight: 16
    },
    BOLD: {
        fontWeight: "bold"
    },
    COMMENT_BUBBLE: {
        backgroundColor: "#F2F2F2",
        padding: 8
    },
    SCROLLVIEW: {
        padding: 8,
        flex: 2,
        flexGrow: 1,
        backgroundColor: "#F2F2F2"
    },
    SCROLLVIEW_NOPAD: {
        backgroundColor: "#F2F2F2",
        flex: 1
    },
    SCROLL_WRAPPER: {
        flex: 2
    },
    ALERT_TITLE: {
        fontSize: 18,
        fontWeight: "bold"
    },
    CARD: {
        backgroundColor: "#FFFFFF",
        marginTop: 8,
        marginRight: 8,
        marginBottom: 8,
        marginLeft: 8,
        padding: 8
    },
    INPUT_LABEL_WRAPPER: {
        borderBottomWidth: 1,
        borderBottomColor: "#333333"
    },
    INPUT_LABEL: {
        fontWeight: "bold",
        fontSize: 15
    },
    PAGE: {
        backgroundColor: "#F2F2F2"
    },

    // Form submission modal
    ACTIONS_MODAL: {
        marginTop: 16,
        marginRight: 16,
        marginBottom: 16,
        marginLeft: 16,
        backgroundColor: "#FFFFFF",
        padding: 8
    },
    MODAL_STYLE: {
        backgroundColor: "rgba(0,0,0,0.5)"
    },
    ACTION_BUTTON_WRAPPER: {
        marginBottom: 8
    },
    ACTION_BUTTON: {
    },
    LINE_SPACER: {
        borderBottomWidth: 1,
        borderBottomColor: "#CCCCCC",
        marginTop: 8,
        marginBottom: 16
    },
    BROWSE_HEADER: {
        height: 30
    },
    FIELD: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        marginLeft: 8,
        marginTop: 8,
        marginRight: 8
    },
    SUB_LABEL: {
        fontSize: 12,
        marginLeft: 8,
        paddingLeft: 8,
        borderLeftWidth: 1,
        fontWeight: "bold",
        marginBottom: 8,
        marginTop: 14
    },
    IMAGE_BTN: {
        width: 40,
        height: 40,
        padding: 14,
        marginRight: 8,
        tintColor: "white"
    },
    LABEL: {
        fontSize: 12
    },
    LOCATION_FIELD: {
        borderBottomWidth: 1,
        paddingBottom: 5,
        paddingLeft: 16,
        paddingTop: 8,
        borderBottomColor: "#CCCCCC"
    },
    LOCATION_FIELD_TEXT: {
        fontSize: 16,
        color: "#000000"
    },
    FOOTER: {
        flex: 0,
        height: 30,
        paddingTop: 5,
        backgroundColor: "#333333"
    },
    MAIN: {
        flex: 1,
        backgroundColor: "#F2F2F2"
    },
    WRAPPER: {
        flexDirection: "column",
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#FFFFFF"
    },
    CONNECTION_TEXT: {
        color: "#F2F2F2",
        textAlign: "center"
    },
    FORM_TITLE: {
        fontWeight: "bold",
        marginTop: 8,
        marginLeft: 8,
        marginBottom: 8,
        fontSize: 16
    },
    OFFLINE: {
        padding: 16
    },
    OFFLINE_TEXT: {
        color: "#333333",
        paddingLeft: 8,
        paddingRight: 8
    },
    TEXT_BLOCK: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        paddingRight: 8
    },
    RISK_TITLE: {
        fontWeight: "bold",
        fontSize: 14,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 8
    },
    ACTION_WRAP: {
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 8
    },
    TEXT_AREA: {
        textAlignVertical: "top"
    },
    DISPLAY_FIELD: {
        marginLeft: 8,
        marginRight: 8
    },
    DISPLAY_TEXT: {},
    FORM_BUFFER: {
        marginTop: 60
    },
    ERROR: {
        marginTop: 8,
        marginBottom: 8,
        flexDirection: "row",
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: "#ff000e"
    },
    ERROR_TEXT: {
        color: "#F2F2F2"
    }
})