import {AsyncStorage} from "react-native";
import when from "whenjs";
import moment from "moment";
import {URI} from "./constants";

import DeviceInfo from "react-native-device-info";

import Utils from "./utils";

class SyncAPI {

    static checkVersion = (cb, errCb) => {
        let request = new XMLHttpRequest();
        request.onreadystatechange = (e) => {
            if (request.readyState !== 4) {
                return;
            }

            if (request.status == 200) {
                let data = request.responseText;
                cb(data);
            } else {
                if (errCb) errCb();
            }
        };

        request.open("GET", "http://cdn.ewars.ws/apk/ANDROID_VERSION_TEST", true);


        request.send();
    };

    static _syncUser = (token, manifest, cb, errCb) => {

        manifest.action = "SYNC_USER";

        let request = new XMLHttpRequest();
        request.onreadystatechange = (e) => {
            if (request.readyState !== 4) {
                return;
            }

            if (request.status == 200) {
                let data = JSON.parse(request.responseText);
                let orgName = data.data.organization_name;
                if (orgName) {
                    if (orgName.en) orgName = orgName.en;
                }

                try {
                    AsyncStorage.multiSet([
                        ["@SETTINGS:account_name", data.data.account_name],
                        ["@SETTINGS:id", String(data.data.id)],
                        ["@SETTINGS:name", data.data.name],
                        ["@SETTINGS:organization_name", orgName || ""],
                        ["@SETTINGS:email", data.data.email],
                        ["@SETTINGS:role", data.data.role],
                        ["@SETTINGS:location_id", data.data.location_id || ""],
                        ["@SETTINGS:token", token]
                    ], (err) => {
                        if (!err) {
                            cb();
                        } else {
                            errCb();
                        }
                    })
                } catch (err) {
                    console.log("TTTT", err);
                }
            } else {
                console.log("ERROR", request.status)
            }
        };

        request.open("POST", URI + "/api/sync/android/v2", true);

        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader("Accept", "application/json");

        request.send(JSON.stringify({
            manifest: manifest,
            token: token
        }));


    };

    static _syncAssignments = (token, manifest, cb, cbErr) => {
        manifest.action = "SYNC_ASSIGNMENTS";

        let request = new XMLHttpRequest();
        request.onreadystatechange = (e) => {
            if (request.readyState !== 4) {
                return;
            }

            if (request.status == 200) {
                let data = JSON.parse(request.responseText);
                try {
                    AsyncStorage.setItem("@SETTINGS:assignments", JSON.stringify(data.data))
                        .then((err) => {
                            cb();
                            console.log(err);
                        })
                } catch (err) {
                    cbErr();
                    console.log(err);
                }
            } else {
                cbErr();
                console.log("ERROR", request.status)
            }
        };

        request.open("POST", URI + "/api/sync/android/v2", true);

        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader("Accept", "application/json");

        request.send(JSON.stringify({
            manifest: manifest,
            token: token
        }));
    };

    static _syncReports = (token, manifest, cb, cbErr) => {
        manifest.action = "SYNC_REPORTS";

        let reports = [];

        try {
            AsyncStorage.getItem("@DATA:queue").then((data) => {
                let dt = JSON.parse(data || "[]");

                if (dt.length <= 0) {
                    cb();
                    return;
                }

                dt.forEach((item, index) => {
                    dt[index].origData = dt[index].data;
                    dt[index].data = Utils.unflatten(dt[index].data);
                })

                manifest.data = dt;

                let request = new XMLHttpRequest();
                request.onreadystatechange = (e) => {
                    if (request.readyState !== 4) {
                        return;
                    }

                    if (request.status == 200) {
                        let data = JSON.parse(request.responseText);
                        AsyncStorage.removeItem("@DATA:queue")
                            .then((err) => {
                                AsyncStorage.getItem("@DATA:sent").then((sent) => {
                                    let newd = JSON.parse(sent || "[]");

                                    dt.forEach(dat => {
                                        dat.data = dat.origData;
                                        delete dat.data;
                                    })

                                    newd.push.apply(newd, dt);
                                    AsyncStorage.setItem("@DATA:sent", JSON.stringify(newd))
                                        .then(resp => {
                                            cb();
                                        })
                                        .catch(err => {
                                            cbErr();
                                        })
                                })
                            })
                    } else {
                        cbErr();
                        console.log("ERROR", request.status)
                    }
                };

                request.open("POST", URI + "/api/sync/android/v2", true);

                request.setRequestHeader("Content-Type", "application/json");
                request.setRequestHeader("Accept", "application/json");

                request.send(JSON.stringify({
                    manifest: manifest,
                    token: token
                }));
            })
        } catch (err) {
            console.log(err);
        }

    };

    static sync = (store, token) => {
        let state;
        if (store) {
            state = store.getState();
        } else {
            state = {settings: {userToken: token}}
        }

        return new Promise((resolve, reject) => {
            let completion = {
                USER: false,
                REPORTS: false,
                ASSIGNMENTS: false
            };

            function testDone() {
                let isDone = false;

                if (completion.USER === true && completion.ASSIGNMENTS === true) isDone = true;

                return isDone;
            }

            let manifest = {
                platform: "ANDROID",
                sync_version: "2.1",
                device_id: DeviceInfo.getUniqueID(),
                lng: "",
                lat: "",
                phone: "",
                versionCode: DeviceInfo.getVersion(),
                versionName: DeviceInfo.getReadableVersion(),
                androidVersion: DeviceInfo.getSystemVersion(),
                device: DeviceInfo.getModel()
            };

            SyncAPI._syncUser(
                state.settings.userToken,
                manifest,
                () => {
                    completion.USER = true;

                    if (testDone()) {
                        resolve(true);
                    }
                },
                () => {
                    reject();
                });

            SyncAPI._syncAssignments(
                state.settings.userToken,
                manifest,
                () => {
                    completion.ASSIGNMENTS = true;
                    if (testDone()) {
                        resolve(true);
                    }
                },
                () => {
                    reject();
                });

            SyncAPI._syncReports(
                state.settings.userToken,
                manifest,
                () => {
                    completion.REPORTS = true;
                    if (testDone()) {
                        resolve(true);
                    }
                },
                () => {
                    reject();
                }
            )
        })
    }
}

export default SyncAPI;