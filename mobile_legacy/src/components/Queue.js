import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    TouchableHighlight,
    NetInfo,
    ToastAndroid,
    Alert,
    Modal
} from "react-native";
import Icon from "react-native-fontawesome-pro";

import {
    ActionButton,
    LineSpacer
} from "./common";

import Utils from "./utils";
import API from "./api";
import Sync from "./lib/sync";

import styles from "./STYLES";

import Storage from "./lib/storage";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    },
    BUTTON_BAR: {
        flexDirection: "row",
        height: 48,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2",
        alignItems: "flex-start",
        paddingRight: 8,
        paddingLeft: 8,
        paddingTop: 8
    },
    SYNC: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        borderWidth: 1,
        borderColor: "#333"
    },
    QUEUE_TITLE: {
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 8,
        backgroundColor: "#333",
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderWidth: 1,
        borderColor: "#333",
        marginLeft: 8
    },
    QUEUE_TEXT: {
        color: "#FFFFFF"
    }
})

class Item extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showControls: false
        }
    }

    _onPress = () => {
        this.props.onSelectQueueItem(this.props.data);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={style.ROW}>
                    <Text>{Utils.i18n(this.props.data.meta.form_name)}</Text>
                    {this.props.data.meta.location_name ?
                        <Text>{Utils.i18n(this.props.data.meta.location_name)}</Text>
                        : null}
                    {this.props.data.data_date ?
                        <Text>{Utils.DATE(this.props.data.data_date, this.props.data.meta.interval)}</Text>
                        : null}
                </View>
            </TouchableHighlight>
        )
    }
}

class Queue extends Component {
    static navigationOptions = () => {
        return {
            title: global._("QUEUE")
        }
    };

    static defaultProps = {
        form: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            editing: false,
            showControls: false,
            selectedItem: null,
            syncing: false
        }
    }

    componentWillMount() {
        this._load();
    }

    _load() {
        const data = this.props.navigation.getParam("data", {});
        Storage.getQueued(data.form ? (data.form.id || null) : null)
            .then((data) => {
                this.setState({
                    data: data,
                    syncing: false
                })
            })
            .catch((err) => {
                console.log("ERR2", err);
            });

    }

    _sync = () => {
        if (!global.IS_CONNECTED) {
            ToastAndroid.show(global._("NO_CONNECTION"), ToastAndroid.SHORT);
            return;
        }
        this.setState({
            syncing: true
        }, () => {
            let self = this;

            let sync = new Sync();
            sync.prep()
                .then((errs) => {
                    if (errs) {
                        if (errs.length > 0) {
                            // Show that errors have occurred
                            Alert.alert(
                                global._("PROBLEM_RECORDS"),
                                global._("SYNC_ERR_RECORDS"),
                                [
                                    {
                                        text: global._("OK"),
                                        onPress: () => {
                                            self._load();
                                        }
                                    }
                                ]
                            )
                        } else {
                            self._load();
                            ToastAndroid.show(global._("SYNC_COMPLETE"), ToastAndroid.SHORT);
                        }
                    } else {
                        console.log(errs);
                        self._load();
                        ToastAndroid.show(global._("SYNC_COMPLETE"), ToastAndroid.SHORT);
                    }
                })
                .catch((err) => {
                    console.log(err);
                    self._load();
                    ToastAndroid.show(global._("SYNC_ERR_1"), ToastAndroid.SHORT);
                })
        })
    };

    _clear = () => {
        Alert.alert(
            global._("CLEAR_QUEUE"),
            global._("CLEAR_QUEUE_MESSAGE"),
            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                    }
                },
                {
                    text: global._("CLEAR"),
                    onPress: () => {
                        this._clearQueue()
                    }
                }
            ]
        );
    };

    _clearQueue = () => {
        const data = this.props.navigation.getParam("data", {});
        Storage.clearQueue(data.form ? (data.form.id || null) : null)
            .then(() => {
                this._load();
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _onSelectQueueItem = (data) => {
        this.setState({
            showControls: true,
            selectedItem: data
        })
    };

    _closeModal = () => {
        this.setState({
            showControls: false,
            selectedItem: null
        })
    };

    _deleteRecordFromQueue = () => {
        Alert.alert(
            global._("DELETE_RECORD"),
            global._("DELETE_RECORD_MESSAGE"),
            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                        this._closeModal();
                    }
                },
                {
                    text: global._("BUTTON_DELETE_RECORD"),
                    onPress: () => {
                        this._performRecordDeletion();
                    }
                }
            ],
            {cancelable: false}
        )
    };

    _resetView = () => {
        const data = this.props.navigation.getParam("data", {});
        Storage.getQueued(data.form ? (data.form.id || null) : null)
            .then((data) => {
                this.setState({
                    data: data,
                    showControls: false,
                    selectedItem: null
                })
            })
            .catch((err) => {
                console.log("ERR2", err);
            });
    };

    _performRecordDeletion = () => {

        // Storage.removeRecordFromQueue(this.state.selectedItem.)
        Storage.deleteRecordFromQueue(this.state.selectedItem)
            .then(res => {
                ToastAndroid.show(global._("RECORD_DELETED"), ToastAndroid.SHORT);
                this._resetView();
            })
            .catch(err => {
                console.log(err);
            });
    };

    _moveRecordToDrafts = () => {
        Alert.alert(
            global._("DEQUEUE_RECORD"),
            global._("DEQUEUE_RECORD_MESSAGE"),
            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                        this._closeModal();
                    }
                },
                {
                    text: global._("BUTTON_DEQUEUE"),
                    onPress: () => {
                        this._performDequeue();
                    }
                }
            ],
            {cancelable: false}
        )

    };

    _performDequeue = () => {

        // Storage.removeRecordFromQueue(this.state.selectedItem.)
        Storage.dequeueRecord(this.state.selectedItem)
            .then(res => {
                ToastAndroid.show(global._("RECORD_DEQUEUED"), ToastAndroid.SHORT);
                this._resetView();
            })
            .catch(err => {
                console.log(err);
            });
    };

    _submitRecordNow = () => {
        Alert.alert(
            "Submit Record?",
            "Are you sure you want to submit this record now instead of through syncing?",
            [
                {
                    text: global._("CANCEL"),
                    onPress: () => {
                        this._closeModal();
                    }
                },
                {
                    text: global._("BUTTON_SUBMIT_RECORD"),
                    onPress: () => {
                        console.log("HERE")
                    }
                }
            ],
            {cancelable: false}
        )
    };

    render() {
        let view;
        if (this.state.data.length > 0) {
            view = this.state.data.map((item, idx) => {
                return <Item
                    onSelectQueueItem={this._onSelectQueueItem}
                    key={idx}
                    data={item}/>
            })
        } else {
            view = <Text style={styles.PLACEHOLDER}>{global._("QUEUE_NO_RECORDS")}</Text>
        }

        return (
            <View>
                <Modal
                    onRequestClose={() => {
                        console.log("CLOSE");
                    }}
                    transparent={false}
                    visible={this.state.syncing}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <View style={{width: 100, height: 80, justifyContent: "center", alignItems: "center"}}>
                            <Icon name="sync" size={40} type="light"/>
                        </View>
                        <View style={{width: 200, height: 50}}>
                            <Text
                                style={{textAlign: "center"}}>{global._("SYNCING_WAIT")}</Text>
                        </View>

                    </View>
                </Modal>
                <View style={style.BUTTON_BAR}>
                    <View style={styles.BUTTON_BAR_WRAP}>
                        <TouchableHighlight
                            onPress={this._sync}>
                            <View style={style.SYNC}>
                                <Text style={styles.SYNC_BUTTON}>{global._("SYNC")}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.BUTTON_BAR_WRAP}>
                        <TouchableHighlight
                            onPress={this._clear}>
                            <View style={style.SYNC}>
                                <Text style={styles.SYNC_BUTTON}>{global._("CLEAR_QUEUED")}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
                <ScrollView>
                    {view}
                </ScrollView>
                <Modal
                    transparent={false}
                    style={styles.MODAL_STYLE}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.showControls}>
                    <View style={styles.ACTIONS_MODAL}>
                        <ActionButton
                            title={global._("BUTTON_MOVE_TO_DRAFTS")}
                            color="grey"
                            onPress={this._moveRecordToDrafts}/>
                        <ActionButton
                            title={global._("DELETE_RECORD")}
                            color="red"
                            onPress={this._deleteRecordFromQueue}/>
                        <LineSpacer/>
                        <ActionButton
                            title={global._("CANCEL")}
                            color="grey"
                            onPress={this._closeModal}/>
                    </View>

                </Modal>
            </View>
        )
    }
}

export default Queue;