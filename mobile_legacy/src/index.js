/*
 THis is a laege comment at the top of this file in order to make this thing to stuff
 */

import {configureFontAwesomePro} from "react-native-fontawesome-pro";

configureFontAwesomePro("light");

import config from "./components/lib/lang";
import codePush, {InstallMode} from "react-native-code-push"; import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    DrawerLayoutAndroid,
    ToolbarAndroid,
    Settings,
    Linking,
    ListView,
    AsyncStorage,
    Image,
    TextInput,
    Button,
    NetInfo,
    TouchableHighlight,
    Modal,
    ToastAndroid,
    Alert,
    ActivityIndicator
} from 'react-native';

import {YellowBox} from 'react-native';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

import Reporting from "./components/Reporting";

import DeviceInfo from "react-native-device-info";

import Assignments from "./components/Assignments";

import DialogAndroid from "react-native-dialogs";

import API from "./components/api";

import LoadingView from "./components/LoadingView";
import LoginView from "./components/Login";
import Utils from "./components/utils";
import styles from "./components/STYLES";

import * as reducers from "./components/reducers";
import * as TYPES from "./components/actions/actionTypes";
import {combineReducers, createStore} from "redux";

const reduction = combineReducers(reducers);
const store = createStore(reduction);
import Sync from "./components/lib/sync";

global.TOKEN = "LOADING";
global.IS_CONNECTED = true;

class NoConnection extends Component {
    render() {
        return (
            <View style={styles.OFFLINE}>
                <Text style={styles.OFFLINE_TEXT}>Account Administrator and Geographic Administrator accounts currently
                    do not support offline operations. Please ensure you have an active data connection before using
                    EWARS mobile.</Text>
            </View>
        )
    }
}

class src extends Component {
    constructor(props) {
        super(props);

        config("en");


        this.state = {
            _loading: true,
            view: "ASSIGNMENTS",
            syncModal: false,
            settings: store.getState().settings,
            isConnected: false
        }

        this._mounted = false;
        this._firstCheck = true;
        this._checking = false;

    }

    componentDidMount() {
        this._mount();
    }

    _mount = () => {
        Utils.listen("LOGOUT", this._processLogout);
        this._load()
            .then(res => {
                if (global.USER != null) {
                    this.setState({
                        _loading: false,
                        user: global.USER || null,
                        token: global.USER.token || "NO_AUTH",
                        isConnected: global.IS_CONNECTED
                    }, () => {
                        Utils.listen("CONNECTION_CHANGE", this._changeConn);
                    })
                } else {
                    this.setState({
                        _loading: false,
                        user: null,
                        token: "NO_AUTH",
                        isConnected: global.IS_CONNECTED
                    }, () => {
                        Utils.listen("CONNECTION_CHANGE", this._changeConn);
                    })
                }
            })
            .catch(err => {
                ToastAndroid.show("Error: Could not load user details", ToastAndroid.LONG);
            })
        // this._checkConnection(() => {
        // NetInfo.isConnected.addEventListener("connectionChange", this._handleFirstConnectivityChange);
        // })
    }

    componentWillUnmount() {
        Utils.unListen("LOGOUT", this._processLogout);
        Utils.unListen("CONNECTION_CHANGE", this._changeConn);
    }

    _changeConn = (state) => {
        if (state != this.state.isConnected) {
            this.setState({isConnected: state})
        }
    };

    _load = () => {
        global.TOKEN = "LOADING";
        return new Promise((resolve, reject) => {


            try {
                AsyncStorage.multiGet([
                    "@SETTINGS:id",
                    "@SETTINGS:account_name",
                    "@SETTINGS:name",
                    "@SETTINGS:email",
                    "@SETTINGS:organization_name",
                    "@SETTINGS:role",
                    "@SETTINGS:version",
                    "@SETTINGS:language",
                    "@DATA:settings",
                    "@EWARSSettings:token"
                ]).then((data) => {
                    let user = {};
                    data.forEach(item => {
                        user[item[0].split(":")[1]] = item[1];
                    })

                    user.settings = JSON.parse(user.settings || '{}');
                    config(user.settings.language || "en");

                    global.USER = user;
                    global.TOKEN = user.token || "NO_AUTH";

                    resolve();
                });
            } catch (err) {
                reject()
                console.log(err)
            }
        })
    };

    codePushStatusDidChange(status) {
        switch (status) {
            case codePush.SyncStatus.CHECKING_FOR_UPDATE:
                break;
            case codePush.SyncStatus.DOWNLOADING_PACKAGE:
                ToastAndroid.show(global._("DOWNLOADING_UPDATE"), ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.INSTALLING_UPDATE:
                ToastAndroid.show(global._("INSTALLING_UPDATE"), ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.UP_TO_DATE:
                ToastAndroid.show(global._("EWARS_UPDATED"), ToastAndroid.SHORT);
                break;
            case codePush.SyncStatus.UPDATE_INSTALLED:
                ToastAndroid.show(global._("UPDATE_INSTALLED"), ToastAndroid.SHORT);
                break;
        }
    }

    codePushDownloadDidProgress(progress) {
        console.log(progress.receivedBytes + " of " + progress.totalBytes + " received.");
    }

    _download = (version) => {
        Linking.openURL("http://cdn.ewars.ws/apk/app-release.apk").catch(err => console.log(err));
    };

    _processLogout = () => {
        try {
            AsyncStorage.clear(() => {
                global.USER = null;
                global.TOKEN = null;
                store.dispatch({
                    type: TYPES.SET_USER_TOKEN,
                    data: false
                });
                this.setState({user: null, token: null})
            })
        } catch (err) {
            console.log(err)
        }
    };

    _logout = () => {
        Alert.alert(
            global._("SIGN_OUT"),
            global._("SIGN_OUT_MESSAGE"),
            [
                {
                    text: global._("CANCEL"), onPress: () => {
                    }
                },
                {text: global._("SIGN_OUT"), onPress: () => this._processLogout()}
            ]
        );
    };

    _goCritical = () => {
        try {
            AsyncStorage.clear(() => {
                delete global.USER;
                delete global.TOKEN;
                store.dispatch({
                    type: TYPES.SET_USER_TOKEN,
                    data: false
                });
                this.setState({user: null})
            })
        } catch (err) {
            console.log(err)
        }
    };

    _success = (token) => {
        this._mount();
    };

    render() {
        if (this.state._loading) {
            return <LoadingView/>;
        }
        if (global.TOKEN === null || global.TOKEN === "NO_AUTH" || global.TOKEN === undefined) {
            return (
                <LoginView
                    onLoginSuccess={this._success}
                    store={store}/>
            );
        }

        if (global.TOKEN === "LOADING") {
            return (
                <LoadingView cancel={this._goCritical}/>
            )
        }

        return <Reporting
            isConnected={this.state.isConnected}
            connection={this.state.isConnected}
            logout={this._logout}/>;
    }
}

let updateDialogOptions = {
    updateTitle: "Update available",
    optionalUpdateMessage: "A new update is available. Install?",
    optionalIgnoreButtonLabel: "Cancel",
    optionalInstallButtonLabel: "Install",
    appendReleaseDescription: true
};
let codePushOptions = {
    updateDialog: updateDialogOptions,
    //checkFrequency: codePush.CheckFrequency.MANUAL,
    checkFrequency: codePush.CheckFrequency.ON_APP_START,
    installMode: InstallMode.IMMEDIATE
};
src = codePush(codePushOptions)(src);

AppRegistry.registerComponent('collect', () => src);
