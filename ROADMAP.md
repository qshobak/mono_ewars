# Roadmap (2019-03-19)

This roadmap is a tentative plan for the core development team. Things can change pretty rapidly as PRs and change requests come in and priorities change. But this document gives a bit of a high-level driver for where we're going with the application and what to expect in terms of changes in the forseeable future.

## Short-term (1-2 months)

- Bugs
- Refactor of UI into easier build system
- Porting components to WebComponents where possible
- Reduction of dependencies to reduce application size
- Better dashboards and analysis tools
- More complex form fields and workflows
- Revamped cloud account management application
- Easier installation through `setup.py install`
- Refactor registration requesting to allow different levels of approval

## Mid-term (2-4 months)

- Desktop applciation support
- Refactored UI to match desktop UI
- New mobile release supporting completely offline operation for all user types
- Custom users
- New document generation engine removing dependency on chromium/electron
- Porting of perfomance-critical operations into Rust library
- Refactor of database for desktop support completion
- Binary installable for server compnents
- Ability to run workers from the core binary (AlarmWorker, NotificationsWorker, SMSWorker, etc...) to make scaling easier
- Refactored document and dashboard layout editorp
- Refactored analysis/widget configuration views
- More statistics functions
- More visualization types

## Long-term (4-8 months)

- Support ticket system
- Teams
- Desktop release

## In a distant future far far away

- 100% rust application for server

## Possible but not scoped

- Community-based surveillance tools
