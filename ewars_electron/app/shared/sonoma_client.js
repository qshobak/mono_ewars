const net = require("net");
const EventEmitter = require("events");

const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

module.exports = class SonomaClient {
    constructor() {
        this.socket = null;
        this.emitter = new EventEmitter();
        this.commands = {};
        this.currentMessageFragments = [];
    }

    start (socketPath) {
        return new Promise((resolve, reject) => {
            this.socket = new net.Socket();
            this.socket.setNoDelay(true);
            this.socket = net.connect(9009, "127.0.0.1", (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve()
                }
            });

            this.socket.on("data", this._handleInput.bind(this));
            this.socket.on("error", reject);
        });
    }

    tx(cmd, args, callback, err) {
        let _uuid = uuid();

        this.commands[_uuid] = [callback, err, true];

        this.sendMessage({
            type: "Command",
            cmd,
            args,
            id: _uuid
        });
    }

    cx(data, callback, err) {
        let _uuid = uuid();

        this.commands[_uuid] = [callback, err];

        this.sendMessage({...data, id: _uuid});
    }

    sendMessage (message) {
        this.socket.write(JSON.stringify(message));
        this.socket.write("\n");
    }

    addMessageListener (callback) {
        this.emitter.on("message", callback);
    }

    removeMessageListener (callback) {
        this.emitter.removeListener("message", callback);
    }

    _handleInput (input) {
        let searchStartIndex = 0;
        while (searchStartIndex < input.length) {
            const newlineIndex = input.indexOf("\n", searchStartIndex);
            if (newlineIndex !== -1) {
                this.currentMessageFragments.push(input.slice(searchStartIndex, newlineIndex));
                let data = JSON.parse(Buffer.concat(this.currentMessageFragments));
                this.emitter.emit("message", data);
                this.currentMessageFragments.length = 0;
                searchStartIndex = newlineIndex + 1;
                if (data.id) {
                    let res = this.commands[data.id] || null
                    if (res) {
                        if (res[2]) {
                            res[0](data.d);
                        } else {
                            res[0](data);
                        }
                    }
                }
            } else {
                this.currentMessageFragments.push(input.slice(searchStartIndex));
                break;
            }
        }
    }
}
