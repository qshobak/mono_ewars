#!/bin/sh
set -e
WORKING_DIR=`pwd`
THIS_PATH=`readlink -f $0`
cd `dirname ${THIS_PATH}`
FULL_PATH=`pwd`
cd ${WORKING_DIR}
cat <<EOS > EWARS.desktop
[Desktop Entry]
Name=EWARS
Comment=EWARS Desktop application for Linux
Exec="${FULL_PATH}/ewars-desktop"
Terminal=false
Type=Application
Icon=${FULL_PATH}/icon.svg
Categories=Network;InstantMessaging;
EOS
chmod +x EWARS.desktop
