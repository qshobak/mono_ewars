class WidgetCard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {};
        if (this.props.marginRight) style.marginRight = this.props.marginRight;
        if (this.props.marginBottom) style.marginBottom = this.props.marginBottom;
        return (
            <div className="widget-card" style={style}>
                <div className="header">

                </div>
                <div className="body">

                </div>
            </div>
        )
    }
}

export default WidgetCard;
