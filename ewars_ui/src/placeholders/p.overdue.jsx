import React from "react";

class OverduePlaceholder extends React.Component {
    render() {
        return (
            <div className="column placeholder dark" style={{position: "relative"}}>
                <div className="placeholder-icon">
                    <i className="fal fa-stopwatch"></i>
                </div>
                <div className="placeholder-title">
                    No overdue records
                </div>
                <div className="placeholder-text">
                    <p>Well done! Looks like you're all caught up on your reporting!</p>
                </div>
            </div>
        )
    }
}

export default OverduePlaceholder;