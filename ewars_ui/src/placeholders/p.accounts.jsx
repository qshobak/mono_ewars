import React from "react";

class AccountsPlaceholder extends React.Component {
    render() {
        return (
            <div className="column placeholder dark" style={{marginTop: "40px", position: "relative"}}>
                <div className="placeholder-title">
                    No active accounts
                </div>
                <div className="placeholder-text">
                    <p>Looks like you haven't signed into any accounts yet, login at the left to get access to your
                        EWARS accounts.</p>
                </div>
            </div>
        )
    }
}

export default AccountsPlaceholder;
