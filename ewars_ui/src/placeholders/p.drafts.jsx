import React from "react";

import IMAGES from "../constants/images";

class DraftsPlaceholder extends React.Component {
    render() {
        return (
            <div className="column placeholder bgd-ps-neutral" style={{position: "relative"}}>
                <div className="placeholder-title">
                    No drafts
                </div>
                <div className="placeholder-icon">
                    <div className="em-drafts-icon"></div>
                </div>
                <div className="placeholder-text">
                    <p>Looks like you don't have any drafts at the moment, to create one navigate to a form or location, create a record and save it. After that it will show up here.</p>
                </div>
            </div>
        )
    }
}

export default DraftsPlaceholder;
