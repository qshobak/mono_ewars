const PropTypes = require("prop-types");
const React = require("react");
const { Client: StyletronClient } = require("styletron-engine-atomic");
const { Provider: StyletronProvider } = require("styletron-react");
import AppContext from "./context_provider";

import "./css/fontawesome/css/fontawesome.min.css";
import "./css/fontawesome/css/light.min.css";
import "./css/theme.less";

import ViewMain from "./views/view.main.jsx";
import ViewLogin from "./views/view.login.jsx";
import ViewLoader from "./views/view.loader.jsx";

const theme = {
    editor: {
        fontFamily: "Menlo",
        backgroundColor: "white",
        baseTextColor: "black",
        fontSize: 13,
        lineHeight: 1.5
    },
    useColors: [
        { r: 31, g: 150, b: 255, a: 1 },
        { r: 64, g: 181, b: 87, a: 1 },
        { r: 206, g: 157, b: 59, a: 1 },
        { r: 216, g: 49, b: 176, a: 1 },
        { r: 235, g: 221, b: 91, a: 1 }
    ]
}

const styletronInstance = new StyletronClient();
class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            account: null
        }

    }

    componentDidMount() {
        window.addEventListener("SET_USER", this._setUser, false);
    }

    _setUser = (e) => {
        this.props.cx({
            type: "SetUser",
            uid: e.detail.user.uuid
        }, (res) => {
            this.setState({
                user: e.detail.user,
                account: e.detail.account
            });
        }, (err) => {
            console.log(err)
        });
    };

    getChildContext() {
        return {
            cx: this.props.cx,
            tx: this.props.tx,
            user: this.state.user,
            account: this.state.account
        }
    }

    render() {
        let view;

        if (!this.state.user) {
            view = (
                <ViewLogin/>
            )
        }

        if (this.state.user) {
            if (!this.state.account.seeded) {
                view = <ViewLoader/>;
            } else {
                view = <ViewMain/>;
            }
        }

        window.tx = this.props.sonoma.tx;
        window.__ = (val) => {
            if (val == undefined) return val;
            if (typeof val == "object") {
                return val.en || val;
            }
            return val
        }

        return (
            <StyletronProvider value={styletronInstance}>
                <AppContext.Provider value={{
                    tx: (cmd, data, cb, err) => this.props.tx(cmd, data, cb, err),
                    cx: this.props.cx,
                    growl: this.props.growl,
                    Menu: this.props.Menu,
                    MenuItem: this.props.MenuItem,
                    __: () => {},
                    prompt: () => {},
                    dialog: this.props.dialog,
                    context_menu: this.props.context_menu,
                    account: this.state.account,
                    user: this.state.user
                    }}>
                    {view}
                </AppContext.Provider>
            </StyletronProvider>
        )
    }
}

App.childContextTypes = {
    cx: PropTypes.func,
    tx: PropTypes.func,
    user: PropTypes.object,
    account: PropTypes.object
};

module.exports = App;
