import React from "react";

import GridCell from "../c.gridcell.jsx";


const GRID = {
    display: "grid",
    height: "100%",
    maring: "8px",
    gridColumnGap: "8px",
    grodRowGap: "8px",
    justifyItems: "stretch",
    alignItems: "streatch",
    gridTemplateRows: "16px 60px auto",
    gridTemplateColumns: "1fr 1fr 1fr 1fr"

}

class AlertsSynopsisWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alerts: []
        }
    }

    render() {
        return (
            <div className="dash-card">
                <div className="card-header">
                    <div className="card-title">Alerts</div>
                    <div className="card-link">Browse</div>
                </div>
                <div className="card-body">
                    <div style={GRID}>
                        <GridCell row={2} rowEnd={3} col={2} colEnd={3}>
                            <div style={{width: "100%", width: "100%"}} ref={(el) => this._el = el}></div>
                        </GridCell>
                        <GridCell row={2} rowEnd={3} col={3} colEnd={4}>
                            <div style={{width: "100%", width: "100%"}} ref={(el) => this._el2 = el}></div>
                        </GridCell>
                        <GridCell row={3} rowEnd={4} col={1} colEnd={5}>
                            <p>No items to display</p>
                        </GridCell>
                    </div>
                </div>
            </div>
        )
    }
}

export default AlertsSynopsisWidget;
