import React from "react";

class ActivitySynopsisWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    compnonentDidMount() {
        this._query(this.state.page || 0);
    }

    _query = () => {
        window.sonomaClient.tx("com.sonoma.activity")
            .then(res => {
                console.log(res);
            }).catch(err => {
                console.log(err);
            });
    }

    render() {
        let items;

        if (this.state.data.length <= 0) {
            items = <p>Nothings happened yet!</p>;
        }
        return (
            <div className="dash-card">
                <div className="card-header">
                    <div className="card-title">Activity</div>
                    <div className="card-link">Brows</div>
                </div>
                <div className="card-body">
                    <div className="nd-list">
                        {items}

                    </div>
                </div>
            </div>
        )
    }
}

export default ActivitySynopsisWidget;
