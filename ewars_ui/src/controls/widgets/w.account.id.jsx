import React from "react";


const STYLES = {
    ACC_NAME: {
        fontWeight: "bold",
        fontSize: "1.2rem"
    }
}

class AccountId extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let account_name = window.state.account.account_name || "";

        return (
            <div className="dash-card">
                <div className="card-body">
                    <h3 style={STYLES.ACC_NAME}>{account_name}</h3>
                </div>
            </div>
        )
    }
}

export default AccountId;
