import React from "react";

class ForecastItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="forecast-item">
                <div className="column">
                    <div className="row bb forecast-title" style={{maxHeight: "35px"}}>
                        {this.props.data.form_name.en || this.props.data.form_name}
                    </div>
                    <div className="row" style={{flex: 2, marginBottom: "8px", marginTop: '8px', minHeight: "58px"}}>
                        <div className="column">
                            <div className="row box-label">Past</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Today</div>
                            <div className="row box-val blue">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Tomorrow</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Thur</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Fri</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Sat</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">Sun</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                        <div className="column">
                            <div className="row box-label">7+ days</div>
                            <div className="row box-val">{this.props.data.forecast[this.props.data.days[0]] || 0}</div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

class WidgetAdminReporting extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow">
                {(this.props.data.forecasts || []).map(item => {
                    return <ForecastItem data={item}/>
                })}

            </div>
        )
    }
}

export default WidgetAdminReporting;
