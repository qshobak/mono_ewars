import React from "react";
import Highcharts from "highcharts";

//import {remote} from "electron";

//const {Menu, MenuItem} = remote;

import GridCell from "../c.gridcell.jsx";

const DUMMY_DATA = [
    0.75
]

const GRID = {
    display: "grid",
    height: "100%",
    maring: "8px",
    gridColumnGap: "8px",
    grodRowGap: "8px",
    justifyItems: "stretch",
    alignItems: "streatch",
    gridTemplateRows: "16px 60px auto",
    gridTemplateColumns: "1fr 1fr 1fr 1fr"

}

class ReportingSynopsisWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            forms: [],
            fid: null,
            data: []
        }
    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.self.forms", [], (res) => {
            this.setState({
                forms: res,
                fid: res[0].uuid || null
            })
        }, (err) => {
            console.log(err)
        });

        Highcharts.chart(this._el, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '75%',
                align: 'center',
                verticalAlign: 'middle'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                }
            },
            series: [{
                type: 'pie',
                data: [
                    0.75
                ]
            }]
        });

        Highcharts.chart(this._el2, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '75%',
                align: 'center',
                verticalAlign: 'middle'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                }
            },
            series: [{
                type: 'pie',
                data: [
                    0.75
                ]
            }]
        });
    }

    _showMenu = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new Menu();

        this.state.forms.forEach(item => {
            menu.append(new MenuItem({
                label: __(item.name),
                click: () => {
                    this.setState({
                        fid: item.fid
                    })
                }
            }));
        });

        menu.popup({window: remote.getCurrentWindow()});
    };

    render() {
        let label = "Loading...";

        if (this.state.fid) {
            let form = this.state.forms.filter(item => {
                return item.uuid = this.state.fid;
            })[0] || null;
            if (form) label = __(form.name);
        }

        return (
            <div className="dash-card">
                <div className="card-header">
                    <div className="card-title">Reporting</div>
                    <div className="card-link">Browse</div>
                </div>
                <div className="card-body">
                    <div style={GRID}>
                        <GridCell row={1} rowEnd={2} col={1} colEnd={5}>
                            <a href="#"
                               onClick={this._showMenu}
                               className="quick-menu">
                            &nbsp;{label}&nbsp;<i
                                className="fal fa-caret-down"></i>
                            </a>
                        </GridCell>
                        <GridCell row={2} rowEnd={3} col={2} colEnd={3}>
                            <div style={{width: "100%", width: "100%"}} ref={(el) => this._el = el}></div>
                        </GridCell>
                        <GridCell row={2} rowEnd={3} col={3} colEnd={4}>
                            <div style={{width: "100%", width: "100%"}} ref={(el) => this._el2 = el}></div>
                        </GridCell>
                        <GridCell row={3} rowEnd={4} col={1} colEnd={5}>
                            <p>No items to display</p>
                        </GridCell>
                    </div>
                </div>
            </div>
        )

    }
}

export default ReportingSynopsisWidget;
