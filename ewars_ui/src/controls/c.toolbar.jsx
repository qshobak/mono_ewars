import React from "react";

class Toolbar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="ux-toolbar">
        {this.props.children}
      </div>
    )
  }
}

export default Toolbar;
