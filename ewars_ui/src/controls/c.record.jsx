import React from "react";

class ReportingItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="reporting-item">
                <div className="column">
                    <div className="row">
                        <strong>Routine Surveillance Form</strong> - Alpha Facility
                    </div>
                    <div className="row" style={{maxHeight: "13px"}}>
                        <div className="column" style={{flex: 2}}>2018-W1</div>
                        <div className="column block" style={{textAlign: "right", paddingRight: "8px"}}>Due: 2018-12-31</div>

                    </div>
                </div>
                <div className="reporting-circle">

                </div>

            </div>
        )
    }
}

export default ReportingItem;
