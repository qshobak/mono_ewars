import React from "react";

import Moment from "moment";

class AlertOpen extends React.Component {
    render() {
        return (
            <div className="node">
                <i className="fal fa-bell"></i>
                <div className="date">
                    {this.props.date.format("YYYY-MM-DD")}
                </div>
            </div>
        )
    }
}

class AlertClose extends React.Component {
    render() {
        return (
            <div className="node">
                <i className="fal fa-ban"></i>
                <div className="date">{this.props.date.format("YYYY-MM-DD")}</div>
            </div>
        )
    }
}

class AlertToday extends React.Component {
    render() {
        return (
            <div className="node">
                <i className="fal fa-calendar"></i>
                <div className="date">{this.props.date.format("YYYY-MM-DD")}</div>
            </div>
        )
    }
}

class AlertNode extends React.Component {
    render() {
        return (
            <div className="node">
                <i className="fal fa-tag"></i>
                <div className="date">{Moment.utc(this.props.date).local().format("YYYY-MM-DD")}</div>
            </div>
        )
    }
}

class AlertTimeline extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.data.events);

        let raised = Moment.utc(this.props.data.raised).local();

        let end_date,
            end_item;
        if (this.props.data.status == "CLOSED") {
            end_date = Moment.utc(this.props.data.closed).local();
            end_item = <AlertClose date={end_date}/>;
        } else {
            end_date = Moment();
            end_item = <AlertToday date={end_date}/>;
        }

        let items = [];
        if (Object.keys(this.props.data.alarm_workflow).length <= 0) {
            // This is the normal workflow
            if (this.props.data.events.VERIFICATION) {
                if (this.props.data.events.VERIFICATION.status == "SUBMITTED") {
                    items.push(
                        <td><AlertNode name="Verification" date={this.props.data.events.VERIFICATION.submitted}/></td>
                    )
                }
            }

            if (this.props.data.events.RISK_ASSESS) {
                if (this.props.data.events.RISK_ASSESS.status == "SUBMITTED") {
                    items.push(
                        <td><AlertNode name="Risk Assessment" date={this.props.data.events.RISK_ASSESS.submitted}/></td>
                    )
                }
            }

            if (this.props.data.events.RISK_CHAR) {
                if (this.props.data.events.RISK_CHAR.status == "SUBMITTED") {
                    items.push(
                        <td><AlertNode name="Risk Characterization" date={this.props.data.events.RISK_CHAR.submitted}/></td>
                    )
                }
            }

            if (this.props.data.events.OUTCOME) {
                if (this.props.data.events.OUTCOME.status == "SUBMITTED") {
                    items.push(
                        <td><AlertNode name="Outcome" date={this.props.data.events.OUTCOME.submitted}/></td>
                    )
                }
            }
        }

        return (
            <div className="timeline-wrapper">
                <div className="timeline-title"><i className="fal fa-calendar"></i>&nbsp;Alert Timeline</div>
                <div className="alert-timeline">
                    <div className="alert-line"></div>
                    <div className="table">

                        <table width="100%">
                            <tbody>
                            <tr>
                                <td className="first norm">
                                    <AlertOpen date={raised}/>
                                </td>
                                {items}
                                <td></td>
                                <td className="last norm">
                                    {end_item}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default AlertTimeline;
