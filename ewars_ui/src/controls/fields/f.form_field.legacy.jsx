/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

function _getActiveField(definition, fieldName) {
    var path = fieldName.split(".");
    var fqp = path.join(".fields.");

    var field = ewars.getKeyPath(fqp, definition);

    return field;
}

var TreeNode = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            isExpanded: false
        }
    },

    onCaretClick: function () {
        if (this.props.field.fields) {
            this.setState({
                isExpanded: this.state.isExpanded ? false : true
            })
        } else {
            var rootPath = this.props.name;
            if (this.props.parentPath) rootPath = this.props.parentPath + "." + this.props.name;

            var rootName = ewars.formatters.I18N_FORMATTER(this.props.field.label);
            if (this.props.parentName) rootName = this.props.parentName + " > " + ewars.formatters.I18N_FORMATTER(this.props.field.label);

            this.props.onNodeSelect(rootPath, rootName, this.props.field);
        }
    },

    _onLabelClick: function () {
        if (!this.props.field.fields) {
            var rootPath = this.props.name;
            if (this.props.parentPath) rootPath = this.props.parentPath + "." + this.props.name;

            var rootName = ewars.formatters.I18N_FORMATTER(this.props.field.label);
            if (this.props.parentName) rootName = this.props.parentName + " > " + ewars.formatters.I18N_FORMATTER(this.props.field.label);
            this.props.onNodeSelect(rootPath, rootName, this.props.field);
        } else {
            this.setState({
                isExpanded: this.state.isExpanded ? false : true
            })
        }
    },

    _processChildren: function () {
        var rootPath = this.props.name;
        if (this.props.parentPath) rootPath = this.props.parentPath + "." + this.props.name;

        var rootName = ewars.formatters.I18N_FORMATTER(this.props.field.label);
        if (this.props.parentName) rootName = this.props.parentName + " > " + ewars.formatters.I18N_FORMATTER(this.props.field.label);

        var arrayed = _.map(this.props.field.fields, function (field, fieldName) {
            return _.extend({}, field, {name: fieldName});
        }, this);

        var arrayed = _.sortBy(arrayed, function (item) {
            return parseFloat(item.order);
        }, this);

        return _.map(arrayed, function (field) {
            return (
                <TreeNode
                    field={field}
                    parentPath={rootPath}
                    parentName={rootName}
                    name={field.name}
                    onNodeSelect={this.props.onNodeSelect}/>
            )
        }, this)
    },

    render: function () {
        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.field.label);

        var hasChilds = false;
        if (this.props.field.fields) hasChilds = true;

        var childs;
        if (hasChilds) {
            childs = this._processChildren();
        }

        var iconClass = "fal fa-cube";
        if (hasChilds) iconClass = "fal fa-caret-right";
        if (hasChilds && this._hasLoaded && this.state.isExpanded) iconClass = "fal fa-caret-down";

        return (
            <li>
                <table>
                    <tr>
                        <td>
                            <div className="node-control" onClick={this.onCaretClick}>
                                <i className={iconClass}></i>
                            </div>
                        </td>
                        <td>
                            <div className="label" onClick={this._onLabelClick}>{nodeName}</div>
                        </td>
                    </tr>
                </table>

                {this.state.isExpanded ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});


var FormFieldSelector = React.createClass({
    getInitialState: function () {
        return {
            showTree: false
        }
    },

    onChange: function (e) {
        this.props.onUpdate(this.props.name, e.target.value);
    },

    _toggleTree: function () {
        this.setState({
            showTree: this.state.showTree ? false : true
        })
    },

    _rootNodes: function () {
        var arrayed = _.map(this.props.form.version.definition, function (field, fieldName) {
            return _.extend({}, field, {name: fieldName});
        }, this);

        var arrayed = _.sortBy(arrayed, function (item) {
            return parseFloat(item.order);
        }, this);

        return _.map(arrayed, function (def) {
            if (['header'].indexOf(def.type) < 0) {
                return (
                    <TreeNode
                        field={def}
                        parentPath={null}
                        parentName={null}
                        name={def.name}
                        onNodeSelect={this._onNodeSelect}/>
                )
            }
        }, this);
    },

    _onNodeSelect: function (path, name, field) {
        this.setState({
            selectedName: name,
            showTree: false
        });

        this.props.onUpdate(this.props.name, path, name, field);
    },

    render: function () {
        var handleIcon = "fal fa-globe";
        if (this.state.showTree) handleIcon = "fal fa-times";

        var selectedName = "No Item Selected";
        if (this.props.value) {
            var field = _getActiveField(this.props.form.version.definition, this.props.value);
            if (!field) {
                // Field might be an additional item
                _.each(this.props.addonFields, function (addField) {
                    if (this.props.value == addField.name) {
                        field = addField;
                    }
                }, this)
            }
            selectedName = ewars.formatters.I18N_FORMATTER(field.label);
        }

        var nodes = this._rootNodes();

        if (this.props.addonFields) {
            _.each(this.props.addonFields, function (field) {

                nodes.unshift(
                    <TreeNode
                        field={field}
                        parentPath={null}
                        parentNode={null}
                        name={field.name}
                        onNodeSelect={this._onNodeSelect}/>
                )
            }, this);
        }

        return (
            <div className="location-selector">
                <div className="handle">
                    <div className="current">
                        <div className="current-location">
                            {selectedName}
                        </div>
                    </div>
                    <div className="grip" rel="tooltip" title="Select Location" onClick={this._toggleTree}>
                        <i className={handleIcon}></i>
                    </div>
                </div>
                {this.state.showTree ?
                    <div className="location-tree-wrapper">
                        <div className="location-tree">

                            <ul>
                                {nodes}
                            </ul>

                        </div>
                    </div>
                    : null}

            </div>
        )
    }
});

module.exports = FormFieldSelector;