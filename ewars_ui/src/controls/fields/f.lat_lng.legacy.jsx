import React from "react";
var Modal = require("../../ModalComponent.react");

const MODAL_ACTIONS = [
    {icon: 'fa-times', action: 'CLOSE', label: 'Close'}
]

const WARN_STYLE = {
    position: 'absolute',
    top: '10px',
    right: '10px',
    height: '20px',
    background: '#F2F2F2',
    color: '#333333',
    padding: '5px',
    zIndex: 1
};

class LatLng extends React.Component {
    static defaultProps = {
        parent_id: null
    };

    constructor(props) {
        super(props);

        this.state = {
            isMapEnabled: false,
            parentGeom: null
        }

        this._map = null;
        this._layer = null;
        this._source = {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        };

        this._sourceRaw = {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        }

        this._sourceParentGeom = {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        }
    }

    /**
     * Get the users current location from the browser
     * @private
     */
    _getLocation = () => {
        ewars.prompt("fa-globe", "Location Detection", "Would you like EWARS to try and use your current location?", function () {
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    this._setValue(position.coords.latitude, position.coords.longitude);
                }.bind(this),
                function (error) {
                    if (error.code == error.PERMISSION_DENIED) {
                        ewars.growl("Browser geolocation is currently disabled, please enable it to use this feature");
                    }
                }.bind(this));
        }.bind(this))
    };

    _setValue = (lat, lng) => {
        var value = {
            type: "Point",
            coordinates: [lng, lat]
        };
        this.props.onUpdate(this.props.name, JSON.stringify(value));
    };

    _showMap = () => {
        if (this.props.parent_id) {
            ewars.tx('com.ewars.location.geometry', [this.props.parent_id])
                .then(res => {
                    this.setState({
                        isMapEnabled: true
                    }, () => {
                        if (res.features != null) {
                            this._renderMap(res);
                        } else {
                            this._renderMap();
                        }
                    })
                })
        } else {
            this.setState({
                isMapEnabled: true
            }, () => {
                this._renderMap();
            })
        }
    };


    /**
     * Render the map
     * @param parentGeom
     * @private
     */
    _renderMap(parentGeom) {
        if (this.state.isMapEnabled) {
            if (!this._map) {

                mapboxgl.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
                this._map = new mapboxgl.Map({
                    container: this._el,
                    minZoom: 0,
                    style: 'mapbox://styles/jduren/cj95cps6g154x2rmnu6xphlya',
                    maxZoom: 20,
                    zoom: 1,
                    width: "100%",
                    height: '100%',
                    attributionControl: false,
                    doubleClickZoom: false,
                    scrollWheelZoom: false
                });

                this._map.addControl(new mapboxgl.NavigationControl(), 'top-left');

                this._map.on("style.load", () => {
                    this._map.addSource("markers", this._sourceRaw);

                    if (parentGeom) {
                        this._map.addSource('parent', this._sourceParentGeom);
                    }

                    this._source = this._map.getSource('markers');

                    if (this.props.value) {
                        let val;
                        if (typeof this.props.value == 'string') {
                            if (this.props.value.indexOf('{') >= 0) val = JSON.parse(this.props.value);
                        } else {
                            val = this.props.value || {};
                        }

                        let newData = {
                            type: "FeatureCollection",
                            features: [{
                                type: "Feature",
                                properties: {},
                                geometry: val
                            }]
                        };

                        this._source.setData(newData);

                        if (val.coordinates[0] != 0 && val.coordinates[1] != 0) {
                            this._map.jumpTo({center: val.coordinates, zoom: 11.15});
                        }
                    } else {
                        this._map.jumpTo({center: [1, 1]});
                        this._map.setZoom(1);
                    }

                    if (parentGeom) {
                        this._sourceParentGeom = this._map.getSource('parent');
                        this._parentLayer = this._map.addLayer({
                            id: 'parent',
                            type: 'fill',
                            source: 'parent',
                            paint: {
                                'fill-color': '#000000',
                                'fill-opacity': 0.4
                            },
                            'filter': ['==', '$type', 'Polygon']
                        })

                        this._sourceParentGeom.setData(parentGeom);
                    }

                    this._layer = this._map.addLayer({
                        id: "markers",
                        interactive: true,
                        type: "symbol",
                        source: "markers",
                        layout: {
                            "icon-image": "circle-15"
                        }
                    })

                    this._layer = this._map.on("dblclick", (e) => {
                        var newData = {
                            type: "FeatureCollection",
                            features: [{
                                type: "Feature",
                                geometry: {
                                    type: "Point",
                                    coordinates: [
                                        e.lngLat.lng,
                                        e.lngLat.lat
                                    ]
                                }
                            }]
                        };
                        this._source.setData(newData);

                        this._setValue(e.lngLat.lat, e.lngLat.lng);
                    })


                });


            } else {
                // map exists, we may need to update parent geometry
                console.log(this.state.parentGeom);
            }
        }
    };

    componentWillReceiveProps(nextProps) {
        if (this.state.isMapEnabled) {

            if (this._map) {
                var newData = {
                    type: "FeatureCollection",
                    features: [{
                        type: "Feature",
                        geometry: nextProps.value
                    }]
                };
                this._source.setData(newData);

            }
        }
    };

    _setPos = (e) => {
        var val;
        if (this.props.value) {
            val = this.props.value;
        } else {
            val = {
                type: "Point",
                coordinates: [0, 0]
            }
        }
        if (e.target.name == "lat") val.coordinates[1] = e.target.value;
        if (e.target.name == 'lng') val.coordinates[0] = e.target.value;
        this.props.onUpdate(this.props.name, JSON.stringify(val));
    };

    _closeModal = () => {
        this.state.isMapEnabled = false;
        this.forceUpdate();
    };

    _onAction = (action) => {
        if (action == 'CLOSE') {
            this._map = null;
            this._layer = null;
            this._source = {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: []
                }
            };
            this._sourceRaw = {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: []
                }
            }

            this._sourceParentGeom = {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: []
                }

            }
            this.setState({
                isMapEnabled: false
            })
        }
    };

    render = () => {
        var lat, lng;

        if (this.props.value) {
            var val = this.props.value;
            if (val.coordinates) {
                lat = val.coordinates[1];
                lng = val.coordinates[0];
            }
        }

        var hasGeoLocate = true;
        if (!navigator.geolocation) hasGeoLocate = false;

        var modalButtons = [
            {label: "Close", onClick: this._closeModal}
        ];


        return (
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <input
                                name="lat"
                                disabled={this.props.readOnly}
                                type="text"
                                value={lat}
                                onChange={this._setPos}/>
                        </td>
                        <td>
                            <input
                                name="lng"
                                disabled={this.props.readOnly}
                                type="text"
                                value={lng}
                                onChange={this._setPos}/>
                        </td>
                    </tr>
                    {!this.props.readOnly ?
                        <tr>
                            <td colSpan="2" width="50px">
                                {hasGeoLocate ?
                                    <ewars.d.Button
                                        onClick={this._getLocation}
                                        ref="geoBtn"
                                        label="Current Location"
                                        icon="fa-compass"/>
                                    : null}
                                <ewars.d.Button
                                    onClick={this._showMap}
                                    label="Map Location"
                                    icon="fa-map"/>
                            </td>
                        </tr>
                        : null}
                    </tbody>
                </table>
                <ewars.d.Shade
                    buttons={modalButtons}
                    actions={MODAL_ACTIONS}
                    onAction={this._onAction}
                    shown={this.state.isMapEnabled}>
                    <div style={WARN_STYLE}>Double-click to place marker</div>
                    <div
                        className="point-selector"
                        style={{height: '100%'}}
                        ref={el => {
                            this._el = el
                        }}>
                    </div>
                </ewars.d.Shade>

            </div>
        )
    }

}


export default LatLng;
