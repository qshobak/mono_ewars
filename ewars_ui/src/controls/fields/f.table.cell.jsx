/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MatrixCell = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {
        var label = ewars.formatters.I18N_FORMATTER(this.props.field.label);
        if (this.props.field.required) label += " *";

        let errors;
        if (this.props.errors) {
            if (this.props.errors[this.props.field.path]) {
                errors = this.props.errors[this.props.field.path];
            }
        }

        return (
            <td className="cell">
                <div className="cell-label">
                    <div className="ide-row">
                        {errors ?
                            <div className="ide-col" style={{maxWidth: 22}} onHover={this._onHover}>
                                <i className="fal fa-exclamation-triangle red"></i>
                            </div>
                            : null}
                        <div className="ide-col">
                            {label}
                        </div>
                    </div>

                </div>
                <div className="cell-input">
                    <div className="ide-row">
                        <div className="ide-col">
                            {this.props.children}
                        </div>
                        {this.props.field.help ?
                            <div className="ide-col" style={{maxWidth: 25}}>
                                <a href="#" className="field-control"><i className="fal fa-question-circle"></i></a>
                            </div>
                            : null}
                    </div>
                    {errors ?
                        <div className="ide-row">
                            <div className="ide-col errors">
                                {errors.map(err => {
                                    return <div>{err}</div>
                                })}
                            </div>
                        </div>
                        : null}

                </div>

                {this.state.showErrors ?
                    <div className="field-errors">

                    </div>
                    : null}

            </td>
        )
    }
});

module.exports = MatrixCell;