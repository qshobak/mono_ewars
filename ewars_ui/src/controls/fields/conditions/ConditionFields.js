/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var BooleanField = require('../BooleanField.react');
var CheckboxField = require('../CheckboxField.react');
var DateField = require('../DateField.react');
var SelectField = require('../SelectField.react');
var TaxonomyField = require('../../taxonomy/TaxonomySelectField.react');
var TextAreaField = require('../TextAreaField.react');
var TextInputField = require('../TextInputField.react');
var AgeField = require("../AgeField.react");
var LocationSelectField = require("../LocationSelectField.react");
var NumberField = require("../NumberField.react");
var SwitchField = require("../SwitchField.react");
var IndicatorSelector = require("../../indicator/IndicatorSelectorComponent.react");

module.exports = {
    select: SelectField,
    text: TextInputField,
    textarea: TextAreaField,
    boolean: BooleanField,
    checkbox: CheckboxField,
    date: DateField,
    number: NumberField,
    taxonomy: TaxonomyField,
    age: AgeField,
    location: LocationSelectField,
    switch: SwitchField,
    indicator: IndicatorSelector
};