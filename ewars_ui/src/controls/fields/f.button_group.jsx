class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = () => {
        this.props.onClick(this.props.data.[1]);
    };

    render() {
        let icon,
            label,
            items = [];

        if (this.props.data[0]) icon = <i className={"fal " + this.props.data[0]}></i>;
        items.push(icon);
        if (icon) items.push(&nbsp;);
        if (this.props.data[2]) item.push(this.props.data[2]);

        return (
            <button
                onClick={this._click}>
                {items}
            </button>
        )
    }
}

class ButtonGroup extends React.Component {
    static defaultProps = {
        context_help: null,
        value: null,
        config: {
            context_help: null
        }
    };

    _onClick = (data) => {
        this.props.onChange(this.props.name, data);
    };

    render() {
        let contextHelp;
        if (this.props.config.context_help) {
            if (this.props.value != null) {
                contextHelp = __(this.props.config.context_help[this.props.value])
            }
        }
        return (
            <div className="btn-group formed">
                {this.props.config.options.map((button, index) => {
                    let key = `BUTTON_${index}`;
                    return (
                        <Button
                            data={button}
                            onClick={this._onClick}/>
                    )
                })}
                <div className="clearer" style={{display: "block", paddingBottom: "5px"}}></div>
            </div>
        )
    }
}

export default ButtonGroup;
