import React from "react";


class TextAreaField extends React.Component {
    static defaultProps = {
        value: null,
        name: null,
        label: null
    };

    _change = (e) => {
        this.props.onChange(this.props.name, e.target.value);
    };

    render() {
        if (this.props.readOnly) {
            return (
                <p style={{padding: "8px"}}>{this.props.value || "None provided"}</p>
            )
        }

        return (
            <textarea
                name=""
                id=""
                cols="30"
                rows="10"
                onChange={this._change}
                value={this.props.value || ""}>
            </textarea>
        )
    }
}

export default TextAreaField;
