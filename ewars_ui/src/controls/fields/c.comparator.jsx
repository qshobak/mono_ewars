import React from 'react';

const TYPES = {
    "EQ": "=",
    "NEQ": "!=",
    "GT": ">",
    "GTE": ">=",
    "LT": "<",
    "LTE": "<=",
    "NULL": "Null",
    "NNULL": "Not null",
    "BETWEEN": "Between",
    "NBETWEEN": "Not between"
};

class Child extends React.Component {
    render() {
        return (
            <div className="column">

            </div>
        )
    }
}

class ComparatorField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showChilds: false
        }
    }

    _toggle = () => {
        this.setState({
            showChilds: !this.state.showChilds
        }, () => {
            if (this._el) {
                this._el.style.transform = "scale(1)";
                this._el.style.opacity = 1;
            }
        })
    };

    componentDidMount() {
        window.addEventListener("click", this._handleBodyClick);
    }

    componentWillUnmount() {
        window.removeEventListener("click", this._handleBodyClick);
    }

    _handleBodyClick = (e) => {

    };

    render() {

        let items = [];
        if (this.state.showChilds) {
            let counter = 0;

            let row = [];
            for (let i in TYPES) {
                row.push(
                    <Child label={TYPES[i]} data={i}/>
                );
                counter++;

                if (counter == 2) {
                    items.push(
                        <div className="row">
                            {row}
                        </div>
                    );
                    counter = 0;
                    row = [];
                }
            }

        }

        console.log(items);

        return (
            <div className="cmp-field">
                <div className="row cmp-field-inner"
                     onClick={this._toggle}
                     style={{height: "100%"}}>
                    <div className="column">?</div>
                    <div className="column  bl" style={{maxWidth: "15px", minWidth: "15px"}}>
                        <i className="fal fa-caret-down"></i>
                    </div>
                </div>
                {this.state.showChilds ?
                    <div
                        ref={(el) => this._el = el}
                        style={{transform: "scale(0)", opacity: 0}}
                        className="cmp-childs">
                        <div className="columns" style={{height: "100%"}}>
                            {items}
                        </div>
                    </div>
                    : null}
            </div>

        )
    }
}

export default ComparatorField;