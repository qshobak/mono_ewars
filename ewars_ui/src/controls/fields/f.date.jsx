import React from "react";

const Moment = require("moment");
import DayPicker from "./date/f.date.day.jsx";
import WeekPicker from "./date/f.date.isoweek.jsx";
import MonthPicker from "./date/f.date.month.jsx";
import YearPicker from "./date/f.date.year.jsx";
import DisplayPicker from "./date/f.date.display.jsx";

const PICKERS = {
    NONE: DayPicker,
    DAY: DayPicker,
    WEEK: WeekPicker,
    MONTH: MonthPicker,
    YEAR: YearPicker,
    DISPLAY: DisplayPicker
};

class Handle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let name = "No date selected";

        if (this.props.value) {
            switch (this.props.date_type) {
                case "DAY":
                    name = this.props.value;
                    break;
                case "WEEK":
                    name = Moment.utc(this.props.value).local().format("GGGG-[W]WW");
                    break;
                case "YEAR":
                    name = Moment.utc(this.props.value).local().format("YYYY");
                    break;
                case "MONTH":
                    name = Moment.utc(this.props.value).local().format("YYYY-MM");
                    break;
                default:
                    name = Moment.utc(this.props.value).local().format("YYYY-MM-DD");
                    break;
            }
        }

        return (
            <div className="handle" onClick={this.props.onClick}>
                <div className="row">
                    <div className="column">{name}</div>
                    <div className="column icon" style={{maxWidth: 31}}>
                        <i className="fal fa-calendar"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class DateField extends React.Component {
    static defaultProps = {
        date_type: "DAY",
        value: ""
    };

    constructor(props) {
        super(props);

        this.state = {
            showOptions: false,
            value: this.props.value || Moment()
        }
    }

    static defaultProps = {
        value: null,
        dispValue: null,
        name: null,
        config: {
            block_future: false,
            date_type: "DAY"
        }
    };

    componentWillMount() {
        if (!this.props.value) {
            this.state.value = Date.now()
        } else {
            this.state.value = this.props.value;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.value) {
            this.state.value = Date.now()
        } else {
            this.state.value = nextProps.value;
        }
    }

    componentWillUnmount() {
    }

    _handleBodyClick = (evt) => {
        if (this.refs.selector) {
            const area = this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    };

    _change = (e) => {
        let date_type = this.props.date_type || this.props.interval;
        if (!date_type) date_type == "DAY";
        let result;
        switch (date_type) {
            case "WEEK":
                result = Moment.utc(e.target.value).endOf("week").format("YYYY-MM-DD");
                this.props.onChange(this.props.name, result);
                break;
            case "DAY":
                this.props.onChange(this.props.name, e.target.value);
                break;
            case "MONTH":
                result = Moment.utc(e.target.value).endOf("month").format("YYYY-MM-DD");
                this.props.onChange(this.props.name, result);
                break;
            case "YEAR":
                result = `${e.target.value}-12-31`;
                this.props.onChange(this.props.name, result);
                break;
            default:
                this.props.onChange(this.props.name, e.target.value);
                break;
        }
    };


    _toggle = () => {
        this.setState({
            showOptions: !this.state.showOptions
        })
    };

    _clearValue = () => {
        this.props.onChange(this.props.name, "");
    };

    render() {
        let view;

        let date_type = this.props.date_type || this.props.interval;

        if (this.props.readOnly) {
            let name = "None selected";
            if (this.props.value) {
                switch (this.props.date_type) {
                    case "DAY":
                        name = this.props.value;
                        break;
                    case "WEEK":
                        name = Moment.utc(this.props.value).local().format("GGGG-[W]WW");
                        break;
                    case "YEAR":
                        name = Moment.utc(this.props.value).local().format("YYYY");
                        break;
                    case "MONTH":
                        name = Moment.utc(this.props.value).local().format("YYYY-MM");
                        break;
                    default:
                        name = Moment.utc(this.props.value).local().format("YYYY-MM-DD");
                        break;
                }
            }
            return (
                <input type="text" disabled={true} value={name}/>
            )
        }

        let hasValue = true;
        if (this.props.value == null) hasValue = false;
        if (this.props.value == undefined) hasValue = false;
        if (this.props.value == "") hasValue = false;

        if (date_type == "DAY" || !date_type) {
            return (
                <div className="date-picker">
                    <div className="column br icon">
                        <i className="fal fa-calendar"></i>
                    </div>
                    <div className="column">
                        <input
                            onChange={this._change}
                            pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                            value={this.props.value || ""}
                            type="date"/>
                    </div>
                    {hasValue ?
                        <div onClick={this._clearValue} className="column clear-date bl" style={{maxWidth: "20px"}}>
                            <i className="fal fa-times"></i>
                        </div>
                        : null}
                </div>
            )
        }

        if (date_type == "MONTH") {
            return (
                <div className="date-picker">
                    <div className="column br icon">
                        <i className="fal fa-calendar"></i>
                    </div>
                    <div className="column">
                        <input
                            onChange={this._change}
                            type="month"
                            value={this.props.value || ""}/>
                    </div>
                    {hasValue ?
                        <div onClick={this._clearValue} className="column clear-date bl" style={{maxWidth: "20px"}}>
                            <i className="fal fa-times"></i>
                        </div>
                        : null}
                </div>
            )
        }

        if (date_type == "WEEK" || date_type == "ISOWEEK") {
            let value = "";
            if (this.props.value) {
                value = Moment.utc(this.props.value).format("GGGG-[W]WW");
            }
            return (
               <div className="date-picker">
                    <div className="column br icon">
                        <i className="fal fa-calendar"></i>
                    </div>
                    <div className="column">
                        <input
                            onChange={this._change}
                            type="week"
                            value={value || ""}/>
                    </div>
                    {hasValue ?
                        <div onClick={this._clearValue} className="column clear-date bl" style={{maxWidth: "20px"}}>
                            <i className="fal fa-times"></i>
                        </div>
                        : null}
                </div>
            )
        }


        return (
            <div ref="selector" className="ew-select" onClick={this._handleBodyClick}>
                <Handle
                    onClick={this._toggle}
                    value={this.props.value}
                    format={date_type}/>
                {this.state.showOptions ?
                    <div className="ew-select-data" style={{maxWidth: 300, right: 0, left: "initial"}}>
                        {view}
                    </div>
                    : null}
            </div>
        )
    }
}

export default DateField;
