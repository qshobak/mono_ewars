import React form "react';

const TYPE_SELECT = {
    options: [
        ["STATIC", "STATIC"],
        ["DYNAMIC", "DYNAMIC"]
    ]
};

const ACTIONS = [
    ["fa-plus", "ADD"]
];

const ACTIONS_OPTION = [
    ["fa-caret-up", "UP"],
    ["fa-caret-down", "DOWN"],
    ["fa-wrench", "CONFIG"],
    ["fa-trash", "REMOVE"]
];

const CONFIG_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CLOSE"}
];

class OptionConfig extends React.Component {
    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>

                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

class StaticOption extends React.Component {
    state = {
        showOptions: false
    };

    _value = (prop, value) => {
        this.props.onChange(this.props.index, 0, value);
    };

    _display = (prop, value) => {
        this.props.onChange(this.props.index, 1, value);
    };

    _action = (action) => {
        if (action == "REMOVE") {
            this.props.onRemove(this.props.index);
        }

        if (action == "UP") {
            if (this.props.index > 0) {
                this.props.onUp(this.props.index);
            }
        }

        if (action == "DOWN") {
            this.props.onDown(this.props.index);
        }

        if (action == "CONFIG") {
            this.setState({
                showOptions: true
            })
        }

        if (action == "CLOSE") {
            this.setState({
                showOptions: false
            })
        }
    };

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{flex: 2}}>
                            <TextField
                                config={{n: "VALUE"}}
                                onChange={this._value}
                                value={this.props.data[0]}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell width="20px">
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{flex: 2}}>
                            <TextField
                                data={{i18: this.props.i18, n: "LABEL"}}
                                onChange={this._display}
                                value={this.props.data[1]}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: "block"}} width="110px">
                            <ewars.d.ActionGroup
                                height="31px"
                                actions={ACTIONS_OPTION}
                                onAction={this._action}
                                right={true}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                <ewars.d.Shade
                    title="Options"
                    icon="fa-wrench"
                    actions={CONFIG_ACTIONS}
                    onAction={this._action}
                    shown={this.state.showOptions}>
                    {this.state.showOptions ?
                        <OptionConfig
                            onChange={this._onConfigChange}
                            data={this.props.data}/>
                        : null}

                </ewars.d.Shade>
            </div>
        )
    }
}

export default class OptionsSet extends React.Component {
    static defaultProps = {
        showTypes: true,
        title: "Options Editor",
        data: {
            i18: true
        },
        onChange: function () {
        }
    };

    _action = (action, index) => {
        if (action == "ADD") {
            let cp = ewars.copy(this.props.value || []);
            if (this.props.data.i18) {
                cp.push(["", {en: ""}])
            } else {
                cp.push(["", ""])
            }

            this.props.onChange(this.props.data.n, cp);
        }

        if (action == "UP") {
            let cp = ewars.copy(this.props.value || []);
            if (index > 0) {
                let tmp = cp[index - 1];
                cp[index - 1] = cp[index];
                cp[index] = tmp;
                this.props.onChange(this.props.data.n, cp);
            }
        }

        if (action == "DOWN") {
            let cp = ewars.copy(this.props.value || []);
            if (index < cp.length - 1) {
                let tmp = cp[index + 1];
                cp[index + 1] = cp[index];
                cp[index] = tmp;
                this.props.onChange(this.props.data.n, cp);
            }

        }

    };

    _onValueChange = (index, prop, value) => {
        let cp = ewars.copy(this.props.value || []);
        cp[index][prop] = value;

        this.props.onChange(this.props.data.n, cp);
    };

    _remove = (index) => {
        let cp = ewars.copy(this.props.value || []);
        cp.splice(index, 1);
        this.props.onChange(this.props.data.n, cp);

    };

    render() {
        let type_value = "STATIC";

        let options = (this.props.value || []).map((item, index) => {
            return <StaticOption
                onChange={this._onValueChange}
                index={index}
                i18={this.props.data.i18}
                onRemove={this._remove}
                onUp={(index) => {
                    this._action("UP", index)
                }}
                onDown={(index) => {
                    this._action("DOWN", index)
                }}
                data={item}/>;
        });

        return (
            <div>
                <ewars.d.Layout style={{border: "1px solid #262626"}}>
                    <ewars.d.Toolbar label={this.props.title}>
                        <ewars.d.Row>
                            <ewars.d.Cell style={{display: "block"}}>
                                <ewars.d.ActionGroup
                                    right={true}
                                    onAction={this._action}
                                    actions={ACTIONS}/>

                                {this.props.showTypes ?
                                    <div className="pull-right">
                                        <ewars.d.ButtonGroup
                                            config={TYPE_SELECT}
                                            name="TYPE"
                                            value={type_value}
                                            onChange={this._onTypeChange}/>
                                    </div>
                                    : null}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <div className="block-tree"
                                 style={{position: "relative", height: "auto", overflowY: "show"}}>
                                <div className="block">
                                    <div className="block-content">
                                        <ewars.d.Row>
                                            <ewars.d.Cell style={{flex: 2}}>Stored Value</ewars.d.Cell>
                                            <ewars.d.Cell width="20px">
                                            </ewars.d.Cell>
                                            <ewars.d.Cell style={{flex: 2}}>Display Value</ewars.d.Cell>
                                            <ewars.d.Cell style={{display: "block"}} width="110px">
                                            </ewars.d.Cell>
                                        </ewars.d.Row>
                                    </div>
                                </div>
                                {options}
                            </div>

                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}
