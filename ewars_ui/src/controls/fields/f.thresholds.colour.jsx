import React from "react";

import EditableList from "./f.editablelist.jsx";
import Colour from "./f.colour.jsx";
import TextField from "./f.text.jsx";
import NumberField from "./f.numeric.jsx";

const ACTIONS = [
    {icon: "fa-plus", action: "ADD"},
    {icon: "fa-minus", action: "REMOVE"},
    {icon: "fa-caret-up", action: "MOVE_UP"},
    {icon: "fa-caret-down", action: "MOVE_DOWN"}
];

const SEP_STYLE = {
    textAlign: "center",
    lineHeight: "30px"
};

let INF_STYLE = {
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    borderTop: "1px solid #505152",
    borderRight: "1px solid #505152",
    borderBottom: "1px solid #505152",
    textAlign: "center",
    lineHeight: "30px",
    width: "37px",
    height: "31px"
};

class ThresholdColour extends React.Component {
    onClick = () => {
        this.props.onClick(this.props.index);
    };

    _onChange = (prop, value) => {
        this.props.onChange(this.props.index, prop, value);
    };

    _setMaxInf = () => {
        let newValue = "INF";
        if (this.props.data[1] == "INF") newValue = 0;
        this.props.onChange(this.props.index, "max", newValue);
    };

    render() {
        let readOnly = false;
        if (this.props.data[1] == "INF") readOnly = true;
        let value = this.props.data[1];
        if (this.props.data[1] == "INF") value = "Infinity";

        let MAX_STYLE = {
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0
        };

        if (this.props.data[1] == "INF") {
            INF_STYLE.background = "#505152";
            INF_STYLE.color = "#F2F2F2";

            MAX_STYLE.background = "#505152";
            MAX_STYLE.color = "#F2F2F2";
        }

        let className = "iw-list-edit-item";
        if (this.props.active) className += " active";

        return (
            <div className={className} onClick={this.onClick} style={{padding: 2}}>
                <ewars.d.Row>

                    <ewars.d.Cell width="50%">
                        <NumberField
                            name="min"
                            onUpdate={this._onChange}
                            value={this.props.data[0]}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell width="20px" style={SEP_STYLE}>to</ewars.d.Cell>
                    <ewars.d.Cell wudth="50%" style={{flexDirection: "row"}}>
                        <ewars.d.Cell style={{flexBasis: 0, flex: 2}}>
                            <NumberField
                                name="max"
                                style={MAX_STYLE}
                                readOnly={readOnly}
                                onUpdate={this._onChange}
                                value={value}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{flexBasis: 0, flexGrow: 0}} onClick={this._setMaxInf}>
                            <div style={INF_STYLE}>Inf</div>
                        </ewars.d.Cell>


                    </ewars.d.Cell>
                    <ewars.d.Cell width={20} style={SEP_STYLE}>=</ewars.d.Cell>
                    <ewars.d.Cell width={35} style={{paddingRight: 5}}>
                        <Colour
                            name="colour"
                            value={this.props.data[2] || "#333"}
                            onUpdate={this._onChange}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class ThresholdValue extends React.Component {
    onClick = () => {
        this.props.onClick(this.props.index);
    };

    _onChange = (prop, value) => {
        this.props.onChange(this.props.index, prop, value);
    };

    _setMaxInf = () => {
        let newValue = "INF";
        if (this.props.data[1] == "INF") newValue = 0;
        this.props.onChange(this.props.index, "max", newValue);
    };

    render() {
        let className = "iw-list-edit-item";
        if (this.props.active) className += " active";

        let readOnly = false;
        if (this.props.data[1] == "INF") readOnly = true;
        let value = this.props.data[1];
        if (this.props.data[1] == "INF") value = "Infinity";

        let MAX_STYLE = {
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0
        };

        if (this.props.data[1] == "INF") {
            INF_STYLE.background = "#505152";
            INF_STYLE.color = "#F2F2F2";

            MAX_STYLE.background = "#505152";
            MAX_STYLE.color = "#F2F2F2";
        }

        return (
            <div className={className} onClick={this.onClick} style={{padding: 2}}>
                <ewars.d.Row>

                    <ewars.d.Cell width="50%">
                        <NumberField
                            name="min"
                            onUpdate={this._onChange}
                            value={this.props.data[0]}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell width="20px" style={SEP_STYLE}>to</ewars.d.Cell>
                    <ewars.d.Cell wudth="33.3%" style={{flexDirection: "row"}}>
                        <ewars.d.Cell style={{flexBasis: 0, flex: 2}}>
                            <NumberField
                                name="max"
                                style={MAX_STYLE}
                                readOnly={readOnly}
                                onUpdate={this._onChange}
                                value={value}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{flexBasis: 0, flexGrow: 0}} onClick={this._setMaxInf}>
                            <div style={INF_STYLE}>Inf</div>
                        </ewars.d.Cell>


                    </ewars.d.Cell>
                    <ewars.d.Cell width={20} style={SEP_STYLE}>=</ewars.d.Cell>
                    <ewars.d.Cell width="33.3%" style={{paddingRight: 5}}>
                        <TextField
                            name="output"
                            value={this.props.data[2] || ""}
                            onUpdate={this._onChange}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class ColourThresholds extends React.Component {
    static defaultProps = {
        output_type: "COLOUR",
        config: {
            output_type: "COLOUR"
        }
    };

    constructor(props) {
        super(props);
    }

    _onListAction = (action, selectedIndex) => {
        if (action === "ADD") {
            let thresholds = this.props.value || [];
            thresholds.push([0, 0, ""]);
            this.props.onUpdate(this.props.name, thresholds);
        }

        if (action === "REMOVE") {
            let thresholds = this.props.value || [];
            thresholds.splice(selectedIndex, 1);
            this.props.onUpdate(this.props.name, thresholds);
        }

        if (action === "MOVE_UP") {
            if (selectedIndex > 0) {
                let thresholds = this.props.value || [];
                let tmp = thresholds[selectedIndex - 1];

                thresholds[selectedIndex - 1] = thresholds[selectedIndex];
                thresholds[selectedIndex] = tmp;

                this.props.onUpdate(this.props.name, thresholds);
            }
        }

        if (action === "MOVE_DOWN") {
            let thresholds = this.props.value || [];
            if (selectedIndex < thresholds.length) {
                let tmp = thresholds[selectedIndex + 1];
                thresholds[selectedIndex + 1] = thresholds[selectedIndex];
                thresholds[selectedIndex] = tmp;

                this.props.onUpdate(this.props.name, thresholds);
            }
        }

    };

    _onPropChange = (index, prop, value) => {
        let threshes = this.props.value;
        if (prop == "colour") threshes[index][2] = value;
        if (prop == "output") threshes[index][2] = value;
        if (prop == "min") threshes[index][0] = value;
        if (prop == "max") threshes[index][1] = value;

        this.props.onUpdate(this.props.name, threshes);
    };

    render() {
        let Template = ThresholdColour;
        if (this.props.config.output_type == "VALUE") Template = ThresholdValue;

        return (
            <EditableList
                onAction={this._onListAction}
                Template={Template}
                onChange={this._onPropChange}
                items={this.props.value || []}
                actions={ACTIONS}/>
        )
    }
}

export default ColourThresholds;
