import React from "react";

class FormFieldField extends React.Component {
    static defaultProps = {
        required_type: null
    };

    constructor(props) {
        super(props);

        this.state = {
            forms: ewars.g.forms || null
        };

        if (!ewars.g.forms) {
            ewars.tx("com.ewars.mapping.layer_options", [])
                .then(res => {
                    ewars.g.forms = res.forms;
                    this.setState({
                        forms: res.forms
                    })
                })
                .catch(err => {
                    ewars.error("error loading forms");
                })
        }
    }

    _onChange = (e) => {
        let value = this.props.value || [null, null];
        if (e.target.name == "fid") value[0] = e.target.value;
        if (e.target.name == "field") value[1] = e.target.value;
        this.props.onUpdate(this.props.name, value);
    };

    render() {

        if (!this.state.forms) {
            return (
                <div className="placeholder">Loading...</div>
            )
        }

        let fields = [];
        let form_id = (this.props.value || [null, null])[0] || null;
        if (form_id != null) {
            let form = this.state.forms.filter(item => {
                return item.id == form_id
            })[0] || null;

            if (form) {
                for (let i in form.definition) {
                    fields.push([i, __(form.definition[i].label), form.definition[i].type]);
                }
            }

            // If we're only looking for a specific type of field,
            // fulter the fields available to that type
            if (this.props.config.required_type) {
                fields = fields.filter(item => {
                    return item[2] === this.props.config.required_type;
                })
            }
        }

        return (
            <div>
                <label htmlFor="">Form</label>
                <div className="select-custom">
                    <select
                        value={(this.props.value || [null, null])[0]}
                        name="fid"
                        onChange={this._onChange}>
                        <option value="">No form selected</option>
                        {this.state.forms.map(item => {
                            return (
                                <option value={item.id}>{item.name.en || item.name}</option>
                            )
                        })}
                    </select>
                </div>

                <div style={{display: "block", height: "8px"}}></div>

                <label htmlFor="">Field</label>
                <div className="select-custom">
                    <select
                        value={(this.props.value || [null, null])[1]}
                        onChange={this._onChange}
                        name="field" id="">
                        <option value="">No field selected</option>
                        {fields.map(item => {
                            return <option value={item[0]}>{item[1]}</option>
                        })}
                    </select>
                </div>
            </div>
        )
    }
}

export default FormFieldField;
