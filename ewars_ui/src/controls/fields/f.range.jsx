import React from "react";
import Moment from "moment";

import RangeUtils from "../../utils/RangeUtils";

import NumberField from "./f.number.jsx";
import TextField from "./f.text.jsx";

import DayPicker from "./f.date.jsx";

const QUICK_RANGES = [
    [
        ["-30D", "Last 30 days"],
        ["-60D", "Last 60 days"],
        ["-6M", "Last 6 months"],
        ["-1Y", "Last 1 year"],
        ["-2Y", "Last 2 years"],
        ["-5Y", "Last 5 years"]
    ],
    [
        ["-1D", "Yesterday"],
        ["-2D", "Day before yesterday"],
        ["-7D", "This day last week"],
        ["PREV_WEEK", "Previous week"],
        ["PREV_MONTH", "Previous month"],
        ["PREV_YEAR", "Previous year"]
    ],
    [
        ["NOW", "Today"],
        ["CUR_WEEK", "This week"],
        ["CUR_MONTH", "This month"],
        ["CUR_YEAR", "This year"]
    ]
];

const QUICK_RANGES_TEMPLATE = [
    [
        ["DD-30D", "Previous 30 days"],
        ["DD-60D", "Previous 60 days"],
        ["DD-6M", "Previous 6 months"],
        ["DD-1Y", "Previous 1 year"],
        ["DD-2Y", "Previous 2 years"],
        ["DD-5Y", "Previous 5 years"]
    ],
    [
        ["DD-1D", "Previous Day"],
        ["DD-2D", "Day before previous day"],
        ["DD-7D", "Same day previous week"],
        ["D_PREV_WEEK", "Previous week"],
        ["D_PREV_MONTH", "Previous month"],
        ["D_PREV_YEAR", "Previous year"]
    ],
    [
        ["D_DATE", "Document date"],
        ["D_CUR_WEEK", "Document week"],
        ["D_CUR_MONTH", "Document month"],
        ["D_CUR_YEAR", "Document year"]
    ]
];

const PRESETS = [
    ["NOW", "Today"],
    ["END", "End date"],
    ["N_W_S", "This week start"],
    ["N_W_E", "This week end"],
    ["N_M_S", "This month start"],
    ["N_M_E", "This month end"],
    ["N_Y_S", "This year start"],
    ["N_Y_E", "This year end"]
];

const PRESETS_TEMPLATE = [
    ["D_DATE", "Document date"],
    ["NOW", "Today"],
    ["END", "End date"],
    ["D_W_S", "Document week start"],
    ["D_W_E", "Document week end"],
    ["D_M_S", "Document month start"],
    ["D_M_E", "Document month end"],
    ["D_Y_S", "Document year start"],
    ["D_Y_E", "Document year end"],
    ["N_W_S", "This week start"],
    ["N_W_E", "This week end"],
    ["N_M_S", "This month start"],
    ["N_M_E", "This month end"],
    ["N_Y_S", "This year start"],
    ["N_Y_E", "This year end"]
];

class QuickRange extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="iw-qk-range" onClick={this._onClick}>{this.props.data[1]}</div>
        )
    }
}

class Offset extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        this.props.onChange(value);
    };

    render() {
        return (
            <div className="dr-offset">
                <label htmlFor="">Offset</label>
                <div className="clearer"></div>
                <TextField
                    name=""
                    value={this.props.value}
                    onUpdate={this._onChange}/>
            </div>
        )
    }
}

class DateSelect extends React.Component {
    static defaultProps = {
        showEndDate: false,
        showStartDate: false
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            mode: "CUSTOM"
        }
    }

    componentDidMount() {
        window.__hack__.addEventListener("click", this._handleBodyClick);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._handleBodyClick);
    }

    _handleBodyClick = (evt) => {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.setState({
                    shown: false
                })
            }
        }
    };

    _trigger = () => {
        this.setState({
            shown: !this.state.shown
        })

    };

    _setMode = (mode) => {
        this.setState({mode})
    };

    _onPickerChange = (value) => {
        this.props.onChange(this.props.prop, Moment.utc(value).format("YYYY-MM-DD"));
        this.setState({
            shown: false
        })
    };

    _selectpreset = (preset) => {
        this.props.onChange(this.props.prop, "{" + preset[0] + "}");
    };

    _onOffsetChange = (value) => {
        let newValue = RangeUtils.applyOffset(this.props.value, value);
        this.props.onChange(this.props.prop, newValue);
    };

    render() {

        let label;

        let pickerValue;
        if (RangeUtils.isDate(this.props.value)) {
            pickerValue = RangeUtils.getDate(this.props.value);
        }
        label = RangeUtils.formatDateSpec(this.props.value);

        let code;
        if (RangeUtils.hasCode(this.props.value)) {
            code = RangeUtils.getCode(this.props.value);
        }

        let offset = RangeUtils.getOffset(this.props.value);

        let presets = PRESETS;
        if (this.props.mode == "TEMPLATE") presets = PRESETS_TEMPLATE;
        if (ewars.TEMPLATE_MODE == "TEMPLATE") presets = PRESETS_TEMPLATE;

        return (
            <div className="ew-select" onClick={this._handleBodyClick} ref="selector">
                <div className="handle" onClick={this._trigger}>
                    <Row>
                        <Cell>{label}</Cell>
                        <Cell width="31">
                            <i className="fal fa-calendar"></i>
                        </Cell>
                    </Row>
                </div>
                {this.state.shown ?
                    <div className="ew-select-data" style={{minWidth: 500}}>
                        <Layout>
                            <Row>
                                <Cell width={300} borderRight={true}>
                                    <DayPicker
                                        value={pickerValue}
                                        name="picker"
                                        onChange={this._onPickerChange}/>
                                </Cell>
                                <Cell width={200}>
                                    <Layout>
                                        <Row>
                                            <Cell borderBottom={true}>
                                                <div className="ide-panel ide-panel-absolute ide-scroll">
                                                    <div className="drpresets-list">
                                                        {presets.map(preset => {
                                                            return <PresetItem
                                                                code={code}
                                                                onClick={this._selectpreset}
                                                                data={preset}/>;
                                                        })}
                                                    </div>
                                                </div>
                                            </Cell>
                                        </Row>
                                        <Row height={66}>
                                            <Cell style={{padding: 8}}>
                                                <Offset
                                                    value={offset}
                                                    onChange={this._onOffsetChange}/>
                                            </Cell>
                                        </Row>
                                    </Layout>
                                </Cell>
                            </Row>
                        </Layout>
                    </div>
                    : null}
            </div>
        )
    }
}

class DatePop extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return <div className="dummy"></div>
    }
}

class PresetItem extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        let className = "dr-preset";
        if (this.props.code == this.props.data[0]) className += " active";

        return (
            <div className={className} onClick={this._onClick}>
                <Row>
                    <Cell width={20}>
                        <div className="indicator"></div>
                    </Cell>
                    <Cell>{this.props.data[1]}</Cell>
                </Row>
            </div>
        )

    }
}

class RangeSelector extends React.Component {
    static defaultProps = {
        mode: "DEFAULT", // or TEMPLATE,
        value: []
    };

    constructor(props) {
        super(props);

        this.state = {}
    }

    _onQkSelect = (qk) => {
        let tmp = RangeUtils.processPreset(qk[0]);
        this.props.onUpdate(this.props.name, [tmp[0], tmp[1]])
    };

    _onPropChange = (prop, value) => {
        let data = this.props.value || [];

        if (prop == "start") data[0] = value;
        if (prop == "end") data[1] = value;

        this.props.onUpdate(this.props.name, data);
    };

    render() {

        let startDate = this.props.value[0] || Moment.utc().format("YYYY-MM-DD");
        let endDate = this.props.value[1] || Moment.utc().format("YYYY-MM-DD");
        let RANGES = QUICK_RANGES;
        if (ewars.TEMPLATE_MODE == "TEMPLATE") RANGES = QUICK_RANGES_TEMPLATE;

        return (
            <div className="iw-range">
                <Layout>
                    <Row>
                        <Cell addClass="iw-range-values" borderRight={true} style={{paddingRight: 10}}>
                            <label htmlFor="">From</label>
                            <DateSelect
                                prop="start"
                                value={startDate}
                                showEndDate={true}
                                mode={this.props.mode}
                                onChange={this._onPropChange}/>
                            <div style={{height: 10}}></div>

                            <label htmlFor="">To:</label>
                            <DateSelect
                                prop="end"
                                value={endDate}
                                mode={this.props.mode}
                                showEndDate={true}
                                onChange={this._onPropChange}/>
                        </Cell>
                        <Cell addClass="iw-quick-ranges">
                            <Layout>
                                <Toolbar label="Quick Ranges"></Toolbar>
                                <Row style={{padding: 8}}>{RANGES.map(item => {
                                    return (
                                        <Cell>
                                            {item.map(qk => {
                                                return <QuickRange data={qk} onClick={this._onQkSelect}/>
                                            })}
                                        </Cell>
                                    )
                                })}
                                </Row>
                            </Layout>
                        </Cell>

                    </Row>
                </Layout>
            </div>
        )
    }
}

export default RangeSelector;
