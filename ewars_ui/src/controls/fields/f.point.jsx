import React from "react";

class PointSelectField extends React.Component {
    _map = null;
    _marker = null;

    state = {};

    static defaultProps = {
        showTiles: true,
        height: 345,
        pointEditMode: false,
        defaultLat: 51.505,
        defaultLng: -0.09
    }

    componentDidMoun () {
        if (!this._map) this.renderEditor();
    }

    componentDidUpdate() {
        if (!this._map) this.renderEditor();

        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            //this._marker = L.marker(value.coordinates).addTo(this._map);
            this._marker.setLatLng(value.coordinates);
            this._map.panTo(new L.LatLng(value.coordinates[0], value.coordinates[1]));
        }
    }

    componentWillUnmount() {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    }

    _useUsersLocation = () => {
        var startPos;
        var geoSuccess = function (position) {
            startPos = position;

            this._handleMapLocationChange(startPos.coords.latitude, startPos.coords.longitude);
        }.bind(this);

        var geoError = function (error) {

            if (error.code == 1) {
                ewars.notifications.notification("fa-bell", "Geocoding Error", "You have previously selected that you do not want to allow this site to use your location, please clear your browsers location settings in order to use this function.");
            }

            if (error.code == 0) {
                ewars.notifications.notifications("fa-bell", "Unknown Error", "An unknown error occurred while trying to determing your location, please try again later.");
            }

            if (error.code == 2) {
                ewars.notifications.notification("fa-bell", "Location Unkown", "We could not reliably determine your current location.");
            }

            if (error.code == 3) {
                ewars.notifications.notification("fa-bell", "Geolocation Timeout", "Geolocation took too long and has been cancelled, please enter your location manually.");
            }
        };

        navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
    };

    _handleInputChange = (e) => {
        var prop = e.target.name;
        var value = e.target.value;

        var data = JSON.parse(this.props.value);
        if (prop == "lat") data.coordinates[0] = value;
        if (prop == "lng") data.coordinates[1] = value;

        this.props.onUpdate(this.props.name, JSON.stringify(data));
    };

    render() {
        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            var lat = value.coordinates[0];
            var lng = value.coordinates[1];
        }

        return (
            <div className="administration-field" style={{height: 450}}>
                <div className="map" ref="mapControl"></div>

                <div className="help">Double Click on the map to set the location or manually enter lat/lng below</div>
                <div className="point-controls">
                    <table>
                        <tr>
                            <td><span>Lat</span><input type="text" name="lat" onChange={this._handleInputChange}
                                                       value={lat}/></td>
                            <td><span>Lng</span><input type="text" name="lng" onChange={this._handleInputChange}
                                                       value={lng}/></td>
                            <td>
                                <button onClick={this._useUsersLocation}>Use my Location</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    };

    _createGeoJSON = (lat, lng) => {
        return JSON.stringify({"type": "Point", "coordinates": [lat, lng]});
    };

    _handleMapLocationChange = (lat, lng) => {
        this.props.onUpdate(this.props.name, this._createGeoJSON(lat, lng));
    };

    renderEditor = () => {
        this._map = mapboxgl.Map(this.refs.mapControl.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            doubleClickZoom: false,
            worldCopyJump: true,
            attributionControl: false,
            scrollWheelZoom: false
        });


        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            this._marker = L.marker(value.coordinates).addTo(this._map);
            this._map.setView(new L.LatLng(value.coordinates[0], value.coordinates[1]), 8);
        } else {
            this._marker = L.marker([46.232727, 6.134357]).addTo(this._map);
            this._map.setView(new L.LatLng(46.232727, 6.134357), 5);
        }

        this._map.on("dblclick", function (e) {
            this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
        }.bind(this))

    }
}

export default PointSelectField;
