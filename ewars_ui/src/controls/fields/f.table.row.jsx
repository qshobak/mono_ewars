/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MatrixRow = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {

        var label;

        if (this.props.data.show_row_label) {
            var labelContent = ewars.I18N(this.props.data.label);
            label = (
                <th>{labelContent}</th>
            )
        }

        var children = this.props.children;
        if (!Array.isArray(children)) {
            children = [children];
        }

        return (
            <tr>
                {label}
                {children}
            </tr>
        )
    }
});

module.exports = MatrixRow;