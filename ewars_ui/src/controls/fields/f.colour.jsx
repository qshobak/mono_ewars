import React from "react";

import TextField from "./f.text.jsx";

const COLOURS = [
    ["#000000", "#191919", "#333333", "#4C4C4C", "#666666"],
    ["#7F7F7F", "#999999", "#B3B3B3", "#E6E6E6", "#FFFFFF"],
    ["#004080", "#000080", "#0000FF", "#0080FF", "#66CCFF"],
    ["#66FFFF", "#00FFFF", "#008080", "#008040", "#408000"],
    ["#008000", "#00FF00", "#80FF00", "#66FF66", "#00FF80"],
    ["#66FFCC", "#804000", "#800000", "#FF0000", "#FF6666"],
    ["#FF8000", "#FFFF00", "#FFFF66", "#FFCC66", "#400080"],
    ["#800080", "#800040", "#8000FF", "#FF00FF", "#FF0080"],
    ["#CC66FF", "#FF66FF"]
];

class ColorCell extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <Cell style={{backgroundColor: this.props.data, height: 20}} onClick={this._onClick}>

            </Cell>
        )
    }
}

class Colour extends React.Component {
    state = {
        shown: false,
        showLeft: false
    };

    componentWillMount() {
        document.body.addEventListener("click", this._onBodyClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._onBodyClick);
    }

    _onBodyClick = (evt) => {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.setState({
                    shown: false
                })
            }
        }
    };

    _trigger = () => {
        let boundingRect = this.refs.selector.getBoundingClientRect();
        let screenWidth = window.innerWidth;

        let showLeft = false;
        if (screenWidth - boundingRect.right < 250) {
            showLeft = true;
        }
        
        this.setState({
            shown: !this.state.shown,
            showLeft: showLeft
        })
    };

    _onColourSelect = (colour) => {
        this.setState({shown: false});
        this.props.onUpdate(this.props.name, colour);
    };

    _onCustomChange = (prop, value) => {
        this.props.onUpdate(this.props.name, value);
    };

    render() {

        let STYLE = {
            height: 30,
            backgroundColor: "#FFFFFF",
            padding: 5,
            textAlign: "center"
         };
        if (this.props.value) STYLE.backgroundColor = this.props.value;

        let dropStyle = {
            padding: 5,
            width: 250
        };

        if (this.state.showLeft) {
            dropStyle.left = "-271px"
        }

        return (
            <div className="ew-select" ref="selector" style={{minWidth: 0}}>
                <div className="handle" onClick={this._trigger} style={{minWidth: 0}}>
                    <div className="row">
                        <i className="fal fa-eyedropper"></i>
                    </div>
                </div>
                {this.state.shown ?
                    <div className="ew-select-data" style={dropStyle}>
                        <div className="column">
                            {COLOURS.map(row => {
                                return (
                                    <div className="row">
                                        {row.map(cell => {
                                            return (
                                                <ColorCell onClick={this._onColourSelect} data={cell}/>
                                            )
                                        })}
                                    </div>
                                )
                            })}
                            <div className="row">
                                <div style={{height: "10px"}}></div>
                                <label htmlFor="">Custom</label>
                                <TextField
                                    name="colour"
                                    onChange={this._onCustomChange}
                                    value={this.props.value}/>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

export default Colour;
