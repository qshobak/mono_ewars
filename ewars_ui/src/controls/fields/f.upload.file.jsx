const ALLOWED_TYPES = [
    "image/png",
    "image/jpg",
    "image/jpeg",
    "application/pdf",
    "text/csv",
    "image/gif"
];

class FileUploadField extends React.Component {
    static defaultProps = {
        filePath: "/{account_id}/assets/",
        allowed_types: ALLOWED_TYPES,
        label: "Choose file",
        file_size: 2,
        local: false,
        return_data: false,
        onData: null,
        binary: false,
        showInput: true,
        showPreview: true
    };

    constructor(props) {
        super(props);

        this.state = {
            uploading: false,
            file: null,
            imagePreviewUrl: null
        }
    }

    componentWillMount() {
        this._uuid = ewars.utils.uuid();
        if (this.props.value) this.state.imagePreviewUrl = this.props.value;
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.value != nextProps.value) this.state.imagePreviewUrl = nextProps.value;
    }

    _onFileChange = (event) => {
        event.preventDefault();

        let reader = new FileReader();
        let file = event.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
                uploading: true
            });

            if (this.props.return_data) {
                this.props.onData(reader.result);
            } else {
               this._upload(reader.result);
            }
        };

        if (this.props.binary) {
            reader.readAsBinaryString(file);
        } else {
            reader.readAsDataURL(file);
        }

    };

    _upload = (fileData) => {
        // console.log(fileData);
        let bl = new ewars.Blocker(null, "Uploading file...");
        let formData = new FormData();
        formData.append("file", this.refs.filer.files[0]);
        formData.append("fileName", this.state.file.name);
        formData.append("fileType", this.state.file.type);
        formData.append("tki", window.user.tki);
        if (this.props.local) {
            formData.append("local", true);
        }
        let oReq = new XMLHttpRequest();

        oReq.onreadystatechange = () => {
            let DONE = 4;
            let OK = 200;

            if (oReq.readyState == DONE) {
                bl.destroy();
                if (oReq.status == OK) {
                    let resp = JSON.parse(window.decodeURI(oReq.responseText));
                    this.props.onUpdate(this.props.name, resp.fn);
                } else {
                    if (oReq.status == 500) {
                    }

                    if (oReq.status == 101) {

                    }
                }
            }
        };

        oReq.open("POST", "/upload", true);
        oReq.send(formData);
    };

    render() {
        let imagePreviewUrl = this.state.imagePreviewUrl;
        let imagePreview;
        if (imagePreviewUrl) {
            imagePreview = (<img width="100px" src={this.props.value}/>);
        }

        let inputStyle = {};
        if (!this.props.showInput) inputStyle.display = "none";

        return (
            <div className="form-field">
                <input style={inputStyle} onChange={this._onFileChange} ref="filer" type="file" name="file" id={this._uuid}/>
                <label className="fileLabel" htmlFor={this._uuid}>{this.props.label}</label>
                {this.props.showPreview && imagePreview ?
                    <div>{imagePreview}</div>
                : null}
            </div>
        )
    }
}

export default FileUploadField;