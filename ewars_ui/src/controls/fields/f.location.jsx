import React from 'react';

import AppContext from "../../context_provider";

import ActionGroup from "../c.actiongroup.jsx";

class LocationFieldNode extends React.Component {
    static defaultProps = {
        data: {},
        sti: null
    };

    state = {
        data: [],
        showChilds: false
    };

    constructor(props) {
        super(props);
    }

    _toggle = () => {
        if (!this._loaded) {
            window.sonomaClient.tx('com.sonoma.location.children', this.props.data.uuid, (res) => {
                this._loaded = true;
                this.setState({
                    data: res,
                    showChilds: true
                })
            }, (err) => console.log(err));
        } else {
            this.setState({
                showChilds: !this.state.showChilds
            })
        }
    };

    _onSelect = (e) => {
        this.props.onSelect(this.props.data.uuid);
    };

    render() {
        let icon;
        if (this.props.data.child_count > 0) {
            icon = (
                <div className="column node-icon" style={{maxWidth: "12px"}} onClick={this._toggle}>
                    <i className={"fal " + (this.state.showChilds ? "fa-caret-down" : "fa-caret-right")}></i>
                </div>
            )
        }

        let selectClass = "column node-name";
        let selectFn;
        if (this.props.data.lti == this.props.lti) {
            selectClass += " selectable";
            selectFn = this._onSelect;
        }

        return (
            <div className="location-tree-node">
                <div className="node" onClick={selectFn}>
                    <div className="row">
                        {icon}
                        <div className={selectClass}>{this.props.data.name.en || this.props.data.name} [loc_type]</div>
                    </div>
                </div>
                {this.state.showChilds ?
                    <div className="children">
                        {this.state.data.map(item => {
                            return <LocationFieldNode
                                lti={this.props.lti}
                                onSelect={this.props.onSelect}
                                key={item.uuid}
                                data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class LocationFieldHandle extends React.Component {
    state = {
        location: null
    };

    constructor(props) {
        super(props);

        if (this.props.value) {
            window.sonomaClient.tx("com.sonoma.location", this.props.value, (res) => {
                this.setState({
                    location: res
                })
            })
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.value != this.props.value) {
            window.sonomaClient.tx('com.sonoma.location', nextProps.value, (res) => {
                this.setState({
                    location: res
                })
            })
        }
    }

    render() {
        let label = "No location selected";
        if (this.state.location) {
            label = this.state.location.name.en || this.state.location.name;
        }

        return (
            <div className="handle row" onClick={this.props.toggle}>
                <div className="column icon br">
                    <i className="fal fa-map-marker"></i>
                </div>
                <div className="column handle-label">{label}</div>
                <div className="column caret"><i className="fal fa-caret-down"></i></div>
            </div>
        )
    }
}


class LocationField extends React.Component {
    static defaultProps = {
        lti: null,
        status: 'ACTIVE',
        showInactive: false
    };

    state = {
        showChilds: false,
        roots: [],
        term: ""
    };

    _loaded = false;

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.locations.root", [], (res) => {
            this.setState({
                roots: res
            })
        }, (err) => console.log(err));
    }


    _searchLocations = () => {
    };

    _updateSearchTerm = (e) => {
        this.setState({
            term: e.target.value
        })
    };

    componentWillMount() {
        document.body.addEventListener("click", this._handleBodyClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleBodyClick);
    }

    _toggle = () => {
        let newState = !this.state.showChilds;
        this.setState({
            showChilds: !this.state.showChilds
        }, () => {
            if (newState) {
                document.body.addEventListener("click", this._handleBodyClick);
            }
        })
    };

    _handleBodyClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                this.setState({
                    showChilds: false
                })
            }
        }
    };

    _selectNode = (uuid) => {
        this.setState({
            showChilds: false
        }, () => {
            this.props.onChange(this.props.name, uuid);
        })
    };

    _closeModal = () => {
        this.setState({
            showChilds: false
        })
    };

    render() {
        let childStyle = {
            display: this.state.showChilds ? "block" : "none"
        };

        return (
            <div className="ew-select"
                 style={{background: "#FFFFFF", position: "unset"}}
                 ref={(el) => this._el = el}>
                <LocationFieldHandle
                    toggle={this._toggle}
                    value={this.props.value}/>
                {this.state.showChilds ?
                    <LocationPopover
                        onClose={this._closeModal}
                        onSelect={this._selectNode}
                        {...this.props}/>
                    : null}
            </div>
        )
    }
}

const ICON_STYLE = {maxWidth: "12px"}

class AssignedLocation extends React.Component {
    constructor(props) {
        super(props);
    }

    _select = () => {
        this.props.onSelect(this.props.data.lid);
    };

    render() {
        console.log(this.props);
        return (
            <div className="location-tree-node" onClick={this._select}>
                <div className="node">
                    <div className="row">
                        <div className="column node-icon" style={ICON_STYLE}>
                            <i className="fal fa-map-marker"></i>
                        </div>
                        <div className="column node-name selectable">
                            {__(this.props.data.location_name)}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class LocationPopover extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            searchTerm: "",
            roots: [],
            view: "ALL"
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = "1";
        }, 10);


        let defaultView = "MINE";
        if (["ACCOUNT_ADMIN", "REGIONAL_ADMIN"].indexOf(this.context.user.role) >= 0) {
            defaultView = "ALL"
        }

        if (this.context.user.role == "USER") {
            window.sonomaClient.tx('com.sonoma.self.assignments', null, (res) => {
                this.setState({
                    roots: res.filter(item => {
                        return item.fid == this.props.fid;
                    })
                })
            }, err => {
                window.state.error("ERROR_LOADING_ASSIGNMENTS", err);
            })
        } else {
            window.sonomaClient.tx("com.sonoma.locations.root", [], (res) => {
                this.setState({
                    roots: res
                })
            }, (err) => console.log(err));
        }
    }

    _close = () => {
        this._el.removeEventListener("transitionend", this._close);
        this.props.onClose();
    };

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this._el.addEventListener("transitionend", this._close);
                this._el.style.transform = "scale(0)";
                this._el.style.opacity = "0";
                break;
            default:
                break;
        }
    };

    _searchChange = (e) => {
        this.setState({
            searchTerm: e.target.value
        })
    };

    _selectNode = (uuid) => {
        this.props.onSelect(uuid);
    };

    _clearSearch = () => {
        this.setState({
            searchTerm: ""
        })
    };

    render() {
        let view;

        if (this.context.user.role == "USER") {
            view = this.state.roots.map(item => {
                return (
                    <AssignedLocation key={item.uuid} {...this.props} onSelect={this._selectNode} data={item}/>);
            });
        } else {
            view = this.state.roots.map(item => {
                return (
                    <LocationFieldNode
                        key={item.uuid}
                        {...this.props}
                        onSelect={this._selectNode}
                        data={item}/>
                )
            });
        }

        return (
            <div className="user-card-outer">
                <div
                    style={{transform: "scale(0)", opacity: "0"}}
                    ref={(el) => this._el = el}
                    className="modal-inner">
                    <div className="columns">
                        <div className="row bb gen-toolbar">
                            {this.context.user.role == "USER" ?
                                <div onClick={() => this._view("MINE")}
                                     className={"clicker" + (this.state.view == "MINE" ? " active" : "")}>
                                    <i className="fal fa-clipboard"></i>&nbsp;Assigned Locations
                                </div>
                                : null}
                            {this.context.user.role != 'USER' ?
                                <div onClick={() => this._view("ALL")}
                                     className={"clicker" + (this.state.view == "ALL" ? " active" : "")}>
                                    <i className="fal fa-globe"></i>&nbsp;All Locations
                                </div>
                                : null}
                            <div className="column"></div>
                            <ActionGroup
                                actions={[["fa-times", 'CLOSE', "CLOSE"]]}
                                onAction={this._action}/>
                        </div>
                        <div className="row bb gen-toolbar" style={{flexBasis: 0, flexGrow: 0, paddingLeft: "8px"}}>
                            <div className="dg-search">
                                <div className="column search-icon" style={{maxWidth: "20px"}}>
                                    <i className="fal fa-search"></i>
                                </div>
                                <div className="column">
                                    <input type="text"
                                           onChange={this._searchChange}
                                           value={this.props.term || ""}/>
                                </div>
                                <div className="column" style={{maxWidth: "20px"}} onClick={this._clearSearch}>
                                    <i className="fal fa-close"></i>
                                </div>
                            </div>
                        </div>
                        <div className="row block block-overflow" style={{padding: "8px", maxHeight: "300px"}}>
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default LocationField;
