import React from "react";

class TraceTable extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="trace-table">

            </div>
        )
    }
}

class TraceMap extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="trace-map">

            </div>
        )
    }

}


class TraceField extends React.Component {
    state = {
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="trace-map-field">
                <div className="row">
                    <div className="column block br"
                         style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs">
                            <div
                                onClick={() => this._view("DEFAULT")}
                                className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                                <i className="fal fa-table"></i>
                            </div>
                            <div
                                onClick={() => this._view("MAP")}
                                className={"ide-tab" + (this.state.view == "MAP" ? " ide-tab-down" : "")}>
                                <i className="fal fa-map"></i>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default TraceField;