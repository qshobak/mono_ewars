import React from "react";

import Moment from "moment";
import DateUtils from "../../utils/date_utils";

class CustomIntervalField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class DayPicker extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="picker">
                <div className="column">
                    <input type="date" value={this.props.value} onChange={this.props.onChange}/>
                </div>
            </div>
        )
    }
}

class YearPicker extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="picker">
                <div className="column">
                    <input type="year" value={this.props.value} onChange={this.props.onChange}/>
                </div>
            </div>
        )
    }
}

class MonthPicker extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="picker">
                <div className="column">
                    <input type="month" value={this.props.value} onChange={this.props.onChange}/>
                </div>
            </div>

        )
    }
}

class ISOWeek extends React.Component {
    render() {
        return (
            <div className="date-node" onClick={() => this.props.onSelect(this.props.date)}>
                {this.props.data}
            </div>
        )
    }
}

class ISOWeekPicker extends React.Component {
    state = {
        year: Moment().year()
    };

    constructor(props) {
        super(props);
    }

    _yearChange = (e) => {
        this.setState({
            year: e.target.value
        })
    };

    _onChange = () => {

    };

    _upYear = () => {
        this.setState({
            year: this.state.year + 1
        })
    };

    _dropYear = () => {
        this.setState({
            year: this.state.year - 1
        })
    };

    render() {
        let now = `${this.state.year}-01-01`;
        let today = Moment.utc(now).format("YYYY-MM-DD");
        let dates = DateUtils.getWeeksInPeriod(today, null);

        dates = dates.reverse();

        let cornerStyle = {
            maxWidth: "25px",
            minWidth: "25px",
            justifyContent: "center",
            alignItems: "center"
        };

        return (
            <div className="picker">
                <div className="column">
                    <div className="row bb bt" style={{maxHeight: "35px", minWidth: "35px"}}>
                        <div className="column br"
                             onClick={this._dropYear}
                             style={cornerStyle}>
                            <i className="fal fa-caret-left"></i>
                        </div>
                        <div className="column">
                            <div className="select-wrapper">
                                <select value={this.state.year} onChange={this._yearChange}>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                </select>
                            </div>
                        </div>
                        <div className="column bl"
                             onClick={this._upYear}
                             style={cornerStyle}>
                            <i className="fal fa-caret-right"></i>
                        </div>
                    </div>
                    <div className="row block block-overflow"
                         style={{maxHeight: "150px", minHeight: "150px", background: "rgba(0,0,0,0.05)", padding: "0"}}>
                        {dates.map(item => {
                            return <ISOWeek
                                date={item}
                                onSelect={this.props.onChange}
                                data={Moment.utc(item).local().format("[W]WW GGGG (YYYY-MM-DD)")}/>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

class Handle extends React.Component {
    static defaultProps = {
        interval: null,
        value: null
    };

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.value != this.props.value) return true;
        return false;
    }

    render() {
        let label = "No selection";

        if (this.props.value) {
            switch (this.props.interval) {
                case "DAY":
                    label = Moment.utc(this.props.value).format("YYYY-MM-DD");
                    break;
                case "MONTH":
                    label = Moment.utc(this.props.value).format("YYYY-MM");
                    break;
                case "WEEK":
                    label = Moment.utc(this.props.value).format("[W]WW GGGG (YYYY-MM-DD)");
                    break;
                case "YEAR":
                    label = Moment.utc(this.props.value).format("YYYY");
                    break;
                default:
                    label = Moment.utc(this.props.value).local().format("YYYY-MM-DD");
                    break;
            }
        }

        return (
            <div className="handle row" onClick={this.props.onClick}>
                <div className="column icon br">
                    <i className="fal fa-calendar"></i>
                </div>
                <div className="column handle-label">{label}</div>
                <div className="column caret">
                    <i className="fal fa-caret-down"></i>
                </div>
            </div>

        )
    }
}

class IntervalField extends React.Component {
    static defaultProps = {
        interval: null
    };

    constructor(props) {
        super(props);

        this.state = {
            showOptions: false
        }
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleBodyClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleBodyClick);
    }

    _toggle = () => {
        this.setState({
            showOptions: !this.state.showOptions
        })
    };

    _handleBodyClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                this.setState({
                    showOptions: false
                })
            }
        }
    };

    _onChange = (val) => {
        this.setState({
            showOptions: false
        }, () => {
            this.props.onChange(this.props.name, val);
        })
    };

    render() {
        let Cmp = DayPicker;
        switch (this.props.interval) {
            case "DAY":
            default:
                Cmp = DayPicker;
                break;
            case "WEEK":
                Cmp = ISOWeekPicker;
                break;
            case "MONTH":
                Cmp = MonthPicker;
                break;
            case "YEAR":
                Cmp = YearPicker;
                break;
        }

        return (
            <div className="ew-select" ref={(el) => this._el = el}>
                <Handle
                    onClick={this._toggle}
                    value={this.props.value}
                    interval={this.props.interval}/>
                {this.state.showOptions ?
                    <Cmp value={this.props.value} onChange={this._onChange}/>
                    : null}
            </div>
        )
    }
}

export default IntervalField;