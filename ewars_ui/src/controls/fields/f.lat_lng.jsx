import React from "react";

const styles = {
    wrapper: {
        border: '1px solid #CCCCCC'
    },
    field: {
        padding: "8px"
    },
    label: {
        padding: '8px 8px 0 8px',
        width: "25%",
        textAlign: "right",
        background: "rgba(0,0,0,0.1)",
        marginRight: "5px"
    },
    input: {
        width: "auto"
    }
};

class LatLngField extends React.Component {
    constructor(props) {
        super(props);
    }

    _getLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (pos) => {
                    this.props.onChange(this.props.name, [
                        pos.coords.latitude,
                        pos.coords.longitude
                    ])
                },
                (err) => {
                    ewars.growl("Could not access your current coordinates");
                    console.log(err);
                })
        }
    };

    _onPropChange = (e) => {
        let value = this.props.value || [];
        if (e.target.name == 'latitude') value[0] = e.target.value;
        if (e.target.name == 'longitude') value[1] = e.target.value;

        this.props.onUpdate(this.props.name, value, this.props.config.path || null);
    };

    render() {
        let showGeoButton = navigator.geolocation != undefined ? true : false;

        if (this.props.readOnly) {
            if (this.props.value) {
                return (
                    <span
                        style={{padding: "8px 0 8px 9"}}>{this.props.value[0] || "No lat"}, {this.props.value[1] || "No Long"}</span>
                )
            } else {
                return (
                    <span style={{padding: "8px 0 8px 0"}}>Coordinates not provided</span>
                )
            }
        }

        return (
            <div className="column">
                <div className="row">
                    <label htmlFor="Latitude">Latitude</label>
                    <input
                        style={styles.input}
                        value={this.props.value[0] || ""}
                        onChange={this._onPropChange}
                        name="latitude"
                        type="text"/>
                </div>
                <div className="row">
                    <label htmlFor="longitude">Longitude</label>
                    <input
                        style={styles.input}
                        value={this.props.value[1] || ""}
                        onChange={this._onPropChange}
                        name="longitude"
                        type="text"/>
                </div>
                {showGeoButton ?
                    <div className="row">
                        <button onClick={this._getLocation}>
                            <i className="fal fa-bullseye"></i>&nbsp;Use current location
                        </button>
                    </div>
                    : null}
            </div>

        )
    }
}

export default LatLngField;
