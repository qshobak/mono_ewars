/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    render: function () {
        var className = "language";
        if (this.props.active) className += " active";

        return (
            <div className={className}>{this.props.value}</div>
        )
    }
});

var LanguageStringField = React.createClass({
    _defaultLang: "en",

    getInitialState: function () {
        return {
            curLang: "en",
            showTree: false
        };
    },

    componentWillMount: function () {
        // A little clean up, fix labels which are not dicts to dicts
        this.state.value = this.props.value;
        if (!this.props.value) {
            this.state.value = {en: ""}
        }

        if (typeof this.props.value == "string") {
            this.state.value = {en: this.props.value};
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.value = nextProps.value;
        if (!nextProps.value) {
            this.state.value = {en: ""}
        }

        if (typeof nextProps.value == "string") {
            this.state.value = {en: nextProps.value}
        }
    },

    onChange: function (e) {
        var copy = this.state.value;
        copy[this.state.curLang] = e.target.value;
        this.props.onUpdate(this.props.name, copy);
    },

    _handleLanguageChange: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var language = e.target.dataset.lang;
        this.setState({
            showTree: false,
            curLang: language
        })
    },

    _hideTree: function () {
        this.setState({
            showTree: false
        })
    },

    _toggleTree: function (e) {
        this.setState({
            showTree: this.state.showTree == true ? false : true
        });

    },

    render: function () {
        var curValue = this.state.value[this.state.curLang];

        var languages = _languages.map(function (language) {
            var active = false;
            if (this.state.curLang == language) active = true;
            return (
                <LanguageNode
                    key={language}
                    value={language}
                    active={active}/>
            )
        }.bind(this));

        var nodes = [];
        for (var i in _languages) {
            var language = _languages[i];
            nodes.push(
                <li>
                    <div className="label" onClick={this._handleLanguageChange} data-lang={language}>{language}</div>
                </li>
            )
        }

        var disabled = false;
        if (this.props.readOnly) disabled = true;

        return (
            <div className="location-selector">
                <div className="handle">
                    <div className="current no-pad">
                        <input type="text" disabled={disabled} value={curValue} name="lang_string" onChange={this.onChange}/>
                    </div>
                    <div className="grip" onClick={this._toggleTree}>
                        <div className="languages">
                            {languages}
                        </div>
                        <i className="fal fa-language"></i>
                    </div>
                </div>

                {this.state.showTree ?
                    <div className="location-tree-wrapper">
                        <div className="location-tree">
                            <h4>Available Languages</h4>
                            <ul className="languages-options">
                                {nodes}
                            </ul>

                        </div>
                    </div>
                    : null}

            </div>
        )
    }
});

module.exports = LanguageStringField;