const Button = require("../ButtonComponent.react");

const Spinner = require("../Spinner.react");

var SelectField = require("../fields/SelectField.react");
const TextField = require("../fields/TextInputField.react");

var CONSTANTS = require("../../constants");

var TreeNode = require("./TreeNode.react");

import IndicatorTreeComponent from "../../../indicators/components/IndicatorTreeComponent.react";

import USERS from "../../forms/indicators/USERS";
import OLD_ALERTS from "../../forms/indicators/OLD_ALERTS";
import ALERTS from "../../forms/indicators/ALERTS";
import ASSIGNMENTS from "../../forms/indicators/ASSIGNMENTS";
import DEVICES from "../../forms/indicators/DEVICES";
import FORM_SUBMISSIONS from "../../forms/indicators/FORM_SUBMISSIONS";
import LOCATIONS from "../../forms/indicators/LOCATIONS";
import TASKS from "../../forms/indicators/TASKS";
import FORMS from "../../forms/indicators/FORMS";


const UUID_REG = /^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}/g;

const Form = require("../Form.react");


const SYSTEM_INDS = {
    USERS: USERS,
    ALERTS: ALERTS,
    OLD_ALERTS: OLD_ALERTS,
    ASSIGNMENTS: ASSIGNMENTS,
    DEVICES: DEVICES,
    FORM_SUBMISSIONS: FORM_SUBMISSIONS,
    LOCATIONS: LOCATIONS,
    TASKS: TASKS,
    FORMS: FORMS
};

var SYSTEM_TYPES = {
    ALARM: null,
    FORM: null,
    ORGANIZATION: null,
    ALERT: null,
    TASK: null
};

const resourceId = {
    "form": "id",
    alarm: "uuid",
    organization: "uuid"
};

const LABELS = {
    form: _l("FORM"),
    organization: _l("ORGANIZATION"),
    metric: _l("METRIC"),
    submitter_type: _l("SUBMITTER_TYPE"),
    form_id: _l("FORM"),
    organization_id: _l("ORGANIZATION"),
    source: _l("SOURCE")
};

const queryDef = {
    form: {
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            },
            orderby: {"name.en": "ASC"}
        }
    },
    alarm: {
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: {"name.en": "ASC"}
        }
    },
    organization: {
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: {"name.en": "ASC"}
        }
    }

};

const itemStyle = {
    marginTop: 8
};

var ALARM_CONFIG = {
    optionsSource: {
        resource: "alarm",
        valSource: "uuid",
        labelSource: "name",
        query: (function () {
            var q = {
                status: {eq: true}
            };

            if (window.user) {
                if ([CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN, CONSTANTS.INSTANCE_ADMIN].indexOf(window.user.role) < 0) {
                    q.account_id = window.user.account_id;
                }
            }

            return q;
        })()
    }
};

const COMPARATORS = [
    ["EQ", "Equal to"],
    ["NEQ", "Not equal to"]
];

class FilterControl extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (prop, value) => {
        let settings = ewars.copy(this.props.settings);
        if (value) {
            settings[prop] = value;
        } else {
            if (settings[prop]) delete settings[prop];
        }
        this.props.onChange(settings);
    };

    render() {
        let label = LABELS[this.props.data[0]];
        let valOpts = {};

        if (this.props.data[1]) {
            if (this.props.data[1].resource) {
                valOpts = queryDef[this.props.data[1].resource]
            } else {
                let options = this.props.data[1].map(item => {
                    return [item, _l(item)]
                });
                valOpts.options = options;
            }
        }

        let value = this.props.settings[this.props.data[0]] || null;

        return (
            <div style={itemStyle}>
                <div className="ide-row">
                    <div className="ide-col" style={{padding: "5px 5px 5px 0"}}>{label}</div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <SelectField
                            name={this.props.data[0]}
                            onUpdate={this._onChange}
                            value={value}
                            config={valOpts}/>
                    </div>
                </div>
            </div>
        )
    }
}

class IndicatorControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: []
        }
    }

    _filterChange = (data, prop, value) => {
        let settings = ewars.copy(this.props.data);
        if (!_.isObject(settings)) {
            settings = {};
        }
        settings[prop] = value;
        this.props.onChange(settings);
    };

    render() {

        let FORM_DEF = SYSTEM_INDS[this.props.itype];

        if (!FORM_DEF) {
            return <div className="ind-options">
                <div className="placeholder">Loading...</div>
            </div>
        }

        let IndForm = this.props.Form || Form;

        return (
            <IndForm
                definition={FORM_DEF}
                data={this.props.data}
                readOnly={false}
                updateAction={this._filterChange}/>
        )
    }
}

var Handle = React.createClass({
    render: function () {
        var name = "No Selection";

        if (this.props.value) {
            name = ewars.I18N(this.props.value.name);
        }
        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{name}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

class IndicatorNode extends React.Component {
    _isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            show: false
        }
    }

    _onClick = () => {
        if (!this._isLoaded && this.props.data.context == "FOLDER") {
            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        show: !this.state.show
                    })
                }.bind(this));
            return;
        }

        this.setState({
            show: !this.state.show
        })
    };

    render() {
        var className = "item locale-select";
        if (this.props.value == this.props.data.uuid) className += " active";

        var name = ewars.I18N(this.props.data.name);

        var children;
        if (this.state.show && this._isLoaded) {
            children = this.state.data.map(function (item) {
                return <IndicatorNode
                    onSelect={this.props.onSelect}
                    hideInactive={true}
                    key={item.uuid || item.id}
                    data={item}/>
            }.bind(this))
        } else {
            children = <Spinner/>;
        }

        let iconClass;
        if (this.props.data.context == "FOLDER") iconClass = "fal fa-folder";
        if (this.props.data.context == "FOLDER" && this.state.show) iconClass = "fal fa-folder-open";

        var handleClass = "locale-tree-node";

        var showSelect = false;
        if (this.props.data.context == "INDICATOR") showSelect = true;
        if (showSelect) handleClass += " has-button";

        let handler = this._onClick;
        if (this.props.data.context == "INDICATOR") handler = () => {
            this.props.onSelect(this.props.data)
        };

        return (
            <div className={className}>
                <div className={handleClass} onClick={handler}>
                    {iconClass ?
                        <div className="expander"><i className={iconClass}></i></div>
                        : null}
                    <div className="labeler no-pad">{name}</div>
                </div>
                {this.state.show ?
                    <div className="locale-tree-node-children" style={{display: "block"}}>
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
}


function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

class SearchInput extends React.Component {
    static defaultProps = {
        value: ''
    };

    constructor(props) {
        super(props);

        this.sendForSearch = debounce(this.sendForSearch, 250);
    }

    componentDidMount() {
        this._el.focus();
    }

    sendForUpdate = (e) => {
        this.props.onChange(e.target.value);

        if (e.target.value.length > 2) {
            this.sendForSearch(e.target.value);
        }

        if (e.target.value == "") this.sendForSearch(null);
    };

    sendForSearch = (searchTerm) => {
        this.props.onSearch(searchTerm);
    };

    render() {
        return (
            <input
                type="text"
                ref={(el) => {this._el = el;}}
                onChange={this.sendForUpdate}
                value={this.props.value}/>
        )
    }
}

var IndicatorSelectorComponent = React.createClass({
    getDefaultProps: function () {
        return {
            config: {
                showSystem: true
            },
            name: null,
            showSystem: true
        }
    },

    _isLoaded: false,


    getInitialState: function () {
        return {
            showTree: false,
            data: [],
            search: "",
            nodeName: "Select Indicator",
            nodeUUID: null,
            multiple: false,
            indicator: null
        };
    },

    _clearSearch: function () {
        ewars.tx("com.ewars.indicators", [null])
            .then(function (resp) {
                this._isLoaded = true;
                this.setState({
                    search: "",
                    data: resp
                })
            }.bind(this));
    },

    _onSearchChange: function(val) {
        this.setState({
            search: val
        })
    },

    _onSearch: function (term) {
        if (term == null || term == '') {
            ewars.tx("com.ewars.indicators", [null])
                .then((resp) => {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        search: ''
                    })
                });
        } else {

            let filter = {
                "name.en": {LIKE: term}
            };
            if (!this.props.showSystem) filter.itype = {eq: "DEFAULT"};
            ewars.tx("com.ewars.query", ["indicator", ["uuid", "name", "itype", "definition", "status"], filter, null, null, null, null])
                .then(resp => {
                    this.setState({
                        data: resp.map(item => {
                            return {
                                ...item,
                                context: "INDICATOR"
                            }
                        })
                    })
                })
        }
    },

    _toggle: function () {
        if (!this._isLoaded) {
            ewars.tx("com.ewars.indicators", [null])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        showTree: !this.state.showTree
                    })
                }.bind(this));
            return;
        }

        this.setState({
            showTree: !this.state.showTree
        })
    },

    componentWillMount: function () {
        if (_.isObject(this.props.value) && this.props.value.uuid) {
            this._init(this.props.value.uuid);
        } else {
            this._init(this.props.value);
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (this.state.node) {
            if (_.isObject(nextProps.value)) {
                // This is a dict based indicator
                if (nextProps.value.uuid != this.state.node.uuid) this._init(nextProps.value.uuid);
            } else {
                if (this.state.node.uuid != nextProps.value) this._init(nextProps.value);
            }
        } else {
            if (_.isObject(nextProps.value)) this._init(nextProps.value.uuid);
            if (!_.isObject(nextProps.value)) this._init(nextProps.value);
        }
    },

    _init: function (value) {
        if (value && value.context) {
            // This is a legacy indicator
            this.setState({
                nodeName: null,
                nodeUUID: null,
                node: null
            });
            return;
        }
        if (value && !_.isObject(value)) {
            ewars.tx("com.ewars.resource", ["indicator", value, null, null])
                .then((resp) => {
                    this.setState({
                        nodeName: ewars.I18N(resp.name),
                        nodeUUID: resp.uuid,
                        node: resp
                    })
                })
        } else if (value && _.isObject(value)) {
            if (value.itype == "CUSTOM") {
                this.state.nodeName = value.name;
                this.state.nodeUUID = null;
                this.state.node = value;
            } else {
                ewars.tx("com.ewars.resource", ["indicator", value.uuid, null, null])
                    .then(function (resp) {
                        if (resp != null) {
                            this.state.nodeName = ewars.I18N(resp.name);
                            this.state.nodeUUID = resp.uuid;
                            this.state.node = resp
                        }
                    }.bind(this))
            }
        }
    },

    _onNodeSelection: function (node) {
        if (node.context != "INDICATOR") return;

        this.setState({
            nodeName: ewars.formatters.I18N_FORMATTER(node.name),
            nodeUUID: node.uuid || null,
            node: node,
            showTree: false
        });

        let name = this.props.config.nameOverride || this.props.name;

        var path = this.props.config ? this.props.config.path : null;
        if (node.itype == "DEFAULT") {
            this.props.onUpdate(name, node.uuid, path, node);
        } else {
            this.props.onUpdate(name, {uuid: node.uuid}, path, node);
        }
    },

    _systemIndUpdate: function (data) {
        let name = this.props.config.nameOverride || this.props.name;
        this.props.onUpdate(name, data, this.props.config ? this.props.config.path : null, {itype: "SYSTEM"});
    },

    render: function () {
        var nodeName = "Select Indicator";
        if (this.state.node) {
            nodeName = ewars.formatters.I18N_FORMATTER(this.state.node.name);
            if (this.state.node.itype == "CUSTOM") nodeName += " [legacy]";
        }

        var isConfigurable = false;
        if (this.state.node) {
            if (["STATIC", "DEFAULT"].indexOf(this.state.node.itype) < 0) {
                isConfigurable = true
            }
        }

        let rootNodes;
        if (this._isLoaded && this.state.showTree) {
            rootNodes = this.state.data.map(function (item) {
                return <IndicatorNode data={item} onSelect={this._onNodeSelection}/>
            }.bind(this))
        }

        return (
            <div className="depth">
                <div ref="selector" className="ew-select locale-selector">
                    <Handle
                        onClick={this._toggle}
                        readOnly={this.props.readOnly}
                        value={this.state.node}/>
                    {this.state.showTree ?
                        <div className="ew-select-data">
                            <div className="locale-search">
                                <div className="input">
                                    <SearchInput
                                        value={this.state.search}
                                        onChange={this._onSearchChange}
                                        placeholder="Search indicators..."
                                        onSearch={this._onSearch}/>
                                    {this.state.search.length > 0 ?
                                        <div className="search-button" onClick={this._clearSearch}><i
                                            className="fal fa-times-circle"></i></div>
                                        : null}
                                </div>
                            </div>
                            {rootNodes}
                        </div>
                        : null}


                </div>
                {isConfigurable ?
                    <div className="add-on">
                        <IndicatorControl
                            data={this.props.value}
                            readOnly={false}
                            Form={this.props.Form}
                            itype={this.state.node.itype || null}
                            onChange={this._systemIndUpdate}
                            definition={this.state.node.definition}/>
                    </div>
                    : null}
            </div>
        )
    }
});

module.exports = IndicatorSelectorComponent;
