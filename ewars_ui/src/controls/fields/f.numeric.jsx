import React from "react";

var reg = /^\d+$/;

class NumericField extends React.Component {
    state = {};

    static defaultProps = {
        placeholder: "Enter number",
        name: null,
        readOnly: false,
        path: null,
        config: {
            allow_negative: true,
            decimal_allowed: true,
            style: {}
        }
    };

    componentDidMount(){
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.value != this.props.value) return true;
        return false;
    }

    _onChange = (e) => {

        var value = e.target.value;

        if (!this.props.config.decimal_allowed) value = value.split(".")[0];
        if (!this.props.config.allow_negative) value = value.replace("-", "");

        value = value.replace(/[^\d.-]/g, '');

        this.props.onChange(this.props.name, value);
    };

    render() {
        var value = this.props.value || "";

        let style = {paddingRight: "5px"};
        if (this.props.style) Object.assign(style, this.props.style);

        return (
            <input
                   disabled={this.props.readOnly}
                   className="form-control number-field"
                   style={style}
                   onChange={this._onChange}
                   type="text"
                   value={value}
                   placeholder={this.props.placeholder || 0}
                   name={this.props.name}/>
        )

    }

}

export default NumericField;
