import React from 'react'

import ActionGroup from "./c.actiongroup.jsx";

const ACTIONS = [
    ['fa-times', 'CANCEL', 'CANCEL']
];

class LocationRoot extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    _toggle = () => {
        if (!this.state.data) {
            this._el.setAttribute("class", "fal fa-cog fa-spin");
            window.sonomaClient.tx('com.sonoma.locations.root', null, (res) => {
                this._el.setAttribute("class", "fal fa-caret-down");
                this.setState({
                    data: res,
                    show: true
                })
            }, (err) => {
                console.log(err);
            })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    render() {
        let iconClass = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
        return (
            <div className="location-tree-node">
                <div className="node">
                    <div className="row" onClick={this._toggle}>
                        <div className="column node-icon" style={{maxWidth: "12px"}}>
                            <i ref={(el) => this._el = el} className={iconClass}></i>
                        </div>
                        <div className="column node-name">Locations</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="children">
                        <LocationGroupsNode onSelect={this.props.onSelect}/>
                        {this.state.data.map(item => {
                            return (
                                <LocationTreeNode
                                    onSelect={this.props.onSelect}
                                    data={item}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class LocationGroupNode extends React.Component {
    render() {
        return (
            <div className="location-tree-node">
                <div className="node">
                    <div className="row">
                        <div className="column node-name">{this.props.data}</div>
                    </div>
                </div>
            </div>
        )
    }
}

class LocationGroupsNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    _toggle = () => {
        if (!this.state.data) {
            this._el.setAttribute("class", "fal fa-cog fa-spin");
            window.sonomaClient.tx('com.sonoma.location_groups', null, (res) => {
                this.setState({
                    data: res,
                    show: true
                })
            }, (err) => {
                window.state.error("ERROR", err);
            })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    render() {
        let iconClass = "fal fa-caret-right";
        if (this.state.show) iconClass = "fal fa-caret-down";

        return (
            <div className="location-tree-node">
                <div className="node" onClick={this._toggle}>
                    <div className="row">
                        <div className="column node-icon" style={{maxWidth: "12px"}}>
                            <i ref={(el) => this._el = el} className={iconClass}></i>
                        </div>
                        <div className="column node-name">Location Groups</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="children">
                        {this.state.data.map(item => {
                            return (
                                <LocationGroupNode data={item}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class LocationTreeNode extends React.Component {
    static defaultProps = {
        data: {},
        sti: null
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    _toggle = () => {
        if (!this.state.data) {
            this._el.setAttribute("class", "fal fa-cog fa-spin");
            window.sonomaClient.tx("com.sonoma.location.children", this.props.data.uuid, (res) => {
                this._el.setAttribute("class", "fal fa-caret-down");
                console.log(res);
                this.setState({
                    data: res,
                    show: true
                })
            }, (err) => {
                console.log(err);
            })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let icon;
        if (this.props.data.child_count > 0) {
            let iconClass = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
            icon = (
                <div className="column node-icon"
                     style={{maxWidth: "12px"}} onClick={this._toggle}>
                    <i ref={(el) => this._el = el}
                       className={iconClass}></i>
                </div>
            )
        }

        return (
            <div className="location-tree-node">
                <div className="node">
                    <div className="row">
                        {icon}
                        <div onClick={this._select}
                             className="column node-name">{this.props.data.name.en || this.props.data.name}</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="children">
                        {this.state.data.map(item => {
                            return (
                                <LocationTreeNode
                                    onSelect={this.props.onSelect}
                                    key={item.uuid}
                                    data={item}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class FormNode extends React.Component {
    render() {
        return (
            <div className="location-tree-node">
                <div className="node">
                    <div className="row">
                        <div className="column node-name">
                            {this.props.data.name.en || this.props.data.name}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class FormsRoot extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    _toggle = () => {
        if (!this.state.data) {
            this._el.setAttribute("class", "fal fa-cog fa-spin");
            window.sonomaClient.tx('com.sonoma.forms.available', null, (res) => {
                this._el.setAttribute("class", "fal fa-caret-down");
                this.setState({
                    data: res,
                    show: true
                })
            }, err => {
                console.log(err);
            })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    render() {
        let iconClass = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
        return (
            <div className="location-tree-node">
                <div className="node" onClick={this._toggle}>
                    <div className="row">
                        <div className="column node-icon" style={{maxWidth: "12px"}}>
                            <i ref={(el) => this._el = el} className={iconClass}></i>
                        </div>
                        <div className="column node-name">
                            Forms
                        </div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="children">
                        {this.state.data.map(item => {
                            return (
                                <FormNode data={item}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class StartPanel extends React.Component {
    render() {
        return (
            <div className="column placeholder" style={{position: "relative"}}>
                <div className="placeholder-icon">
                    <i className="fal fa-clipboard"></i>
                </div>
                <div className="placeholder-title"></div>
                <div className="placeholder-text">
                    <p>To get started requesting a new assignment, either select the location, form or location group
                        that you would like to have an assignment for.</p>
                    <p>Assignemnt requests must be approved by an administrator before you can start reporting them,
                        once an adminsitrator has approved your request you will be able to see and submit data for the
                        request forms and/or locations.</p>
                </div>
            </div>
        )
    }
}

class SelectionPanelLocation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.location.forms.available', this.props.data.uuid, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_LOCATION", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid) {
            window.sonomaClient.tx("com.sonoma.location.forms.available", nextProps.data.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("ERROR_LOADING_LOCATION", err);
            })
        }
    }

    _showFormSynopsis = (data) => {
        console.log(data);
    };

    render() {
        console.log(this.state, this.props);

        if (this.state.data) {
            if (this.state.data.length <= 0) {
                return (
                    <div className="column block block-overflow">
                        <div className="location-title">{this.props.data.name.en || this.props.data.name}</div>
                        <div className="location-type">{this.props.data.lti_name.en || this.props.data.lti_name}</div>

                        <p>There are currently no forms being reported for this type of location.</p>
                    </div>
                )
            }
        }

        return (
            <div className="column block block-overflow">
                <div className="location-title">{this.props.data.name.en || this.props.data.name}</div>
                <div className="location-type">{this.props.data.lti_name.en || this.props.data.lti_name}</div>

                <div className="dash-header">
                    <i className="fal fa-clipboard"></i>&nbsp;Forms
                </div>

                <div className="inline-selector">
                    {(this.state.data || []).map(item => {
                        let hasAssign = item.assign_uuid != null;
                        let toggle;
                        if (!hasAssign) {
                            toggle = () => {
                                this.props.toggle(item.uuid);
                            }
                        }
                        let className = "fal " + (this.props.forms.indexOf(item.form_id) >= 0 ? " fa-check-square" : " fa-square");
                        return (
                            <div className={"form-item row" + (hasAssign ? " assigned" : "")}>
                                <div className="column selector" onClick={toggle}>
                                    {!hasAssign ?
                                        <i className={className}></i>
                                        : null}
                                </div>
                                <div className="column label" onClick={toggle}>
                                    <span>{item.name.en || item.name}</span>
                                </div>
                                <div className="column selector bl" onClick={() => this._showFormSynopsis(item)}>
                                    <i className="fal fa-eye"></i>
                                </div>
                            </div>
                        )
                    })}
                </div>

                <button style={{marginLeft: "8px"}}
                        onClick={this._submit}>Update assignments
                </button>


            </div>
        )
    }
}

class SelectionPanelForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow">

            </div>
        )
    }
}

class AssignmentSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            location: null,
            form: null,
            forms: [],
            locations: []
        }
    }

    componentDidMount() {
        let height = this._outerEl.offsetHeight;
        height = height - 80;
        this._el.style.height = height + "px";
        this._el.style.top = "40px";
        this._el.style.bottom = "40px";
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = 1;
        }, 10);
    }

    _close = () => {
        this._el.removeEventListener("transitionend", this._close);
        window.dispatchEvent(new CustomEvent("HIDE_ASSIGNMENT_SELECT", {}));
    };

    _action = (action) => {
        switch (action) {
            case "CANCEL":
                this._el.addEventListener("transitionend", this._close);
                this._el.style.transition = "all 0.1s ease-out";
                this._el.style.transform = "scale(0)";
                this._el.style.opacity = "0";
                break;
            default:
                break;
        }
    };

    _selectLocation = (location) => {
        this.setState({
            location: location,
            view: "LOCATION"
        })
    };

    _toggleForm = (form_id) => {
        let forms = this.state.forms;
        if (forms.indexOf(form_id) >= 0) {
            forms.splice(forms.indexOf(form_id), 1);
        } else {
            forms.push(form_id);
        }
        this.setState({
            forms: forms
        })
    };

    render() {
        let initialStyle = {
            transition: "all 0.3s ease-out",
            opacity: "0",
            transform: "scale(0)",
            position: "absolute",
            top: "20px",
            left: "20px",
            right: "20px",
            bottom: "20px",
            margin: "0",
            width: "auto"
        };

        let view = <StartPanel/>;
        if (this.state.view == "LOCATION") view =
            <SelectionPanelLocation
                toggle={this._toggleForm}
                data={this.state.location}
                forms={this.state.forms}/>;
        if (this.state.view == "FORM") view =
            <SelectionPanelLocation data={this.state.form} locations={this.state.locations}/>;

        return (
            <div className="user-card-outer" ref={(el) => this._outerEl = el}>
                <div
                    style={initialStyle}
                    ref={(el) => this._el = el}
                    className="modal-inner">
                    <div className="column" style={{height: "100%", width: "100%"}}>
                        <div className="row bb gen-toolbar">
                            <div className="column grid-title">Assignment Request</div>
                            <ActionGroup actions={ACTIONS} onAction={this._action}/>
                        </div>
                        <div className="row">
                            <div className="column br block block-overflow"
                                 style={{maxWidth: "30%", minWidth: "30%", padding: "8px"}}>
                                <LocationRoot onSelect={this._selectLocation}/>
                                <FormsRoot onSelect={this._selectForm}/>
                            </div>
                            <div className="column">
                                {view}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AssignmentSelector;
