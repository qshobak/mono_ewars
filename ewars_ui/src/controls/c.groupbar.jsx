import React from "react";

class GroupBar extends React.Component {
    constructor(props) {
        super(props);
    }

    _toggle = () => {
        window.state.toggleMenu()
    };

    render() {

        let _state = window.state.getState("CORE");

        return (
            <div className="groupbar">
                <div
                    onClick={() => _state.setState({menu: "DEFAULT"})}
                    className={"bar-item " + (this.props.menu == "DEFAULT" ? " active" : "")}>
                    <i className="fal fa-tachometer"></i>
                </div>
                <div
                    onClick={() => _state.setState({menu: "REPORTING"})}
                    className={"bar-item" + (this.props.menu == "REPORTING" ? " active" : "")}>
                    <i className="fal fa-clipboard"></i>
                </div>
                <div
                    onClick={() => _state.setState({menu: "ALERTS"})}
                    className={"bar-item" + (this.props.menu == "ALERTS" ? " active" : "")}>
                    <i className="fal fa-bell"></i>
                </div>
                {!window.DESKTOP ?
                    <div
                        onClick={() => _state.setState({menu: "DOCUMENTS"})}
                        className={"bar-item" + (this.props.menu == "DOCUMENTS" ? " active" : "")}>
                        <i className="fal fa-book"></i>
                    </div>
                    : null}
                {!window.DESKTOP ?
                    <div
                        onClick={() => _state.setState({menu: "ANALYSIS"})}
                        className={"bar-item" + (this.props.menu == "ANALYSIS" ? " active" : "")}>
                        <i className="fal fa-chart-line"></i>
                    </div>
                    : null}
                {!window.DESKTOP ?
                    <div
                        onClick={() => _state.setState({menu: "ADMIN"})}
                        className={"bar-item" + (this.props.menu == "ADMIN" ? " active" : "")}>
                        <i className="fal fa-cogs"></i>
                    </div>
                    : null}

                <button onClick={this._toggle}>
                    <i className="fal fa-caret-left"></i>
                </button>
            </div>
        )
    }
}

export default GroupBar;
