/// #if DESKTOP
//const {dialog} = require("electron").remote;
/// #endif

import React from 'react';
import Moment from "moment";

import AppContext from "../context_provider";

import ActionGroup from "./c.actiongroup.jsx";
let state = window.state;


const CONTROL_ACTIONS = [
    // ['fa-users', 'PEERS', 'Show raw peers'],
    //['fa-code-merge', "REBASE", "Rebase from remote"],
    ['fa-tag', 'LOG', 'Event Log'],
    //['fa-info-circle', "HELP", "HELP"],
    //['fa-wifi', 'CONNECTION', "Connection Status"],
    //['fa-building', 'ACCOUNTS', 'ACCOUNTS']
];


class FootBar extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        window.addEventListener("online", this._alertStatusOnline);
        window.addEventListener("offline", this._alertStatusOffline);
        window.addEventListener("tooltipshown", this._handleTooltip);
        window.addEventListener("tooltiphide", this._handleToolTipHide);

    }

    componentWillUnmount() {
        window.removeEventListener("offline", this._alertStatusOffline);
        window.removeEventListener("online", this._alertStatusOnline);
        window.removeEventListener("tooltipshown", this._handleTooltip);
        window.removeEventListener("tooltiphide", this._handleToolTipHide);
    }

    _alertStatusOnline = () => {

    };

    _alertStatusOffline = () => {

    };

    _handleTooltip = (value) => {
        this._el.innerText = value.detail;

    };

    _handleToolTipHide = () => {
        this._el.innerText = "";
    };

    _action = (action) => {
        switch(action) {
            case 'CONNECTION':
                window.state.showConnectionCard();
                break;
            case "HELP":
                window.state.addTab("KNOWLEDGEBASE", {name: "Guidance"});
                break;
            case "PEERS":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "PEERS",
                        name: __("PEERS"),
                        icon: "fa-users"
                    }
                }));
                break;
            case "REBASE":
                this._rebase();
                break;
            case "LOG":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "LOG",
                        name: "Event Log",
                        icon: "fa-clock"
                    }
                }));
                break;
            case "BLE":
                window.state.getBlueToothDevices();
                break;
            case "ACCOUNTS":
                window.state.showAccountsCard();
                break;
            default:
                break;
        }
    };

    _rebase = () => {
        window.dialog.showMessageBox({
            type: "question",
            buttons: ["Perform Rebase", "Cancel"],
            title: "Rebase?",
            message: "Are you sure you want rebase off of the remote, any unsynced local data will be lost and you will need to re-download any record data.",
            cancelId: 1
        }, (ind) => {
            if (ind == 0) {
            }
        })
    };

    render() {
        return (
            <div className="row bt footbar" style={{maxHeight: "30px", padding: "0 16px 0 8px"}}>
                <div className="column grid-title">{this.context.user.name} [{this.context.user.email}] - 0.0.1-develop</div>
                <div className="column"
                     style={{textAlign: "right", justifyContent: "center", paddingRight: "8px"}}
                     ref={(el) => this._el = el}>&nbsp;</div>

                <ActionGroup
                    actions={CONTROL_ACTIONS}
                    onAction={this._action}/>

            </div>
        )
    }
}

export default FootBar;
