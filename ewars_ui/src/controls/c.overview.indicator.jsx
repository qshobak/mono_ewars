import React from "react";

class OverviewIndicator extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row header-node header-node-text"><span>{this.props.value || 0}</span></div>
                <div className="row block" style={{textAlign: "center"}}>
                    <i className={this.props.icon}></i>&nbsp;{this.props.name}
                </div>
            </div>
        )
    }
}

export default OverviewIndicator;