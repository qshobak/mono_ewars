import React from "react";
import Moment from "moment";

/// #if DESKTOP
//const {dialog} = require('electron').remote;
/// #endif

let state = window.state;

import ActionGroup from "./c.actiongroup.jsx";
import ws from "../libs/api";

import DateUtils from "../utils/date_utils";

const ACTIONS = [
    ['fa-download', 'DOWNLOAD_ALL', "Download all"],
    ['fa-times', 'CLOSE', "Close"]
];

const ITEM_ACTIONS_SEEDED = [
    ['fa-sync', 'SYNC', 'Rebase']
];

const ITEM_ACTIONS_BASE = [
    ['fa-download', "DOWNLOAD", "Download"]
];

const ITEM_ACTIONS_SEEDING = [
    ['fa-cog fa-spin', "SEEDING", "Seeding..."]
];

const ITEM_ACTIONS_QUEUED = [
    ['fa-clock', 'QUEUE', "Queued"]
];

class ListItem extends React.Component {
    constructor(props) {
        super(props);

        let split = props.data.split('-');
        let start = `${split[0]}-${split[1]}-1`;

        if (this.props.interval == "WEEK") {
            start = Moment.utc(this.props.data).subtract(7, 'd').format("YYYY-MM-DD");
        } else if (this.props.interval == "YEAR") {
            start = `${split[0]}-01-01`
        }

        let id = `${this.props.form.id}_${start}_${this.props.data}`;
        this.state = {
            start_date: start,
            state: state.getDownloadState(id),
            id: id
        }
    }

    componentWillMount() {
        state.on("JOBS_CHANGED", this._jobsChanged);
    }

    componentWillUnmount() {
        state.off("JOBS_CHANGED", this._jobsChanged);
    }

    _jobsChanged = () => {
        this.setState({
            state: state.getDownloadState(this.state.id)
        })
    };

    _action = (action) => {
        switch (action) {
            case "DOWNLOAD":
                state.addDownload(
                    this.props.form.id,
                    [this.state.start_date, this.props.data]
                );
                break;
            case "SYNC":
                dialog.showMessageBox({
                    type: "question",
                    buttons: [
                        "Sync",
                        "Cancel"
                    ],
                    defaultId: 0,
                    title: "Re-seed data set",
                    message: "Are you sure you want to re-seed this dataset, any local changes will be overwritten",
                }, (result) => {
                    if (result == 0) {
                        state.addDownload(
                            this.props.form.id,
                            [this.state.start_date, this.props.data]
                        )
                    }
                });
                break;
            default:
                break
        }
    };

    render() {
        let label = "";
        let actions = ITEM_ACTIONS_BASE;

        if (this.state.state == "PROCESSING") {
            label = " - Seeding";
            actions = ITEM_ACTIONS_SEEDING;
        }
        if (this.state.state == "SEEDED") {
            label = " - Seeded";
            actions = ITEM_ACTIONS_SEEDED;
        }

        if (this.state.state == "QUEUED") {
            label = " - Queued";
            actions = ITEM_ACTIONS_QUEUED;
        }

        let dateLabel = `${this.state.start_date} to ${this.props.data}`;
        if (this.props.interval == "WEEK") {
            dateLabel = Moment.utc(this.props.data).format('GGGG-[W]WW');
        }

        if (this.props.interval == "YEAR") {
            dateLabel = Moment.utc(this.props.data).format("YYYY");
        }

        return (
            <div className="download-block row">
                <div className="column name">
                    {dateLabel}{label}
                </div>
                <ActionGroup
                    actions={actions}
                    onAction={this._action}/>
            </div>
        )
    }

}

class SourceData extends React.Component {
    static defaultProps = {
        form: null,
        type: "RECORD"
    };

    constructor(props) {
        super(props);

        this.state = {
            oldest: null
        };

        if (this.props.type == "RECORD") {
            ws("com.sonoma.seed.records.oldest", [this.props.form.id])
                .then(res => {
                    this.setState({
                        oldest: res
                    })
                })
                .catch(err => {
                    console.log(err);
                })
        } else {
            ws('com.sonoma.seed.alerts.oldest', [this.props.alarm.uuid])
                .then(res => {
                    this.setState({
                        oldest: res
                    })
                })
                .catch(err => {
                    console.log(err);
                })
        }
    }

    render() {
        if (!this.state.oldest) {
            return (
                <div className="row">
                    <p>Loading...</p>
                </div>
            )
        }

        let items = [];
        let intervalDownload = "MONTH";
        let startDate = this.state.oldest || '2017-01-01';
        if (this.props.form.features.INTERVAL_REPORTING != null) {
            let interval = this.props.form.features.INTERVAL_REPORTING.interval;
            switch (interval) {
                case "DAY":
                    items = DateUtils.getMonthsInPeriod(startDate, null);
                    break;
                case "WEEK":
                    items = DateUtils.getMonthsInPeriod(startDate, null);
                    break;
                case "MONTH":
                    items = DateUtils.getMonthsInPeriod(startDate, null);
                    break;
                case "YEAR":
                    intervalDownload = "YEAR";
                    items = DateUtils.getYearsInPeriod(startDate, null);
                    break;
                default:
                    break;
            }
        } else {
            intervalDownload = "WEEK";
            items = DateUtils.getWeeksInPeriod(startDate, null);
            items = items.reverse();
        }

        console.log(items);
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "35px", height: "35px"}}>
                    <div className="column" style={{maxWidth: "20px", justifyContent: "center", textAlign: "center"}}>
                       <i className="fal fa-database"></i>
                    </div>
                    <div className="column grid-title">Data</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={() => {
                            this.props.onClose()
                        }}/>
                </div>
                <div className="row block block-overflow" style={{padding: "8px"}}>
                    {items.map(item => {
                        return (
                            <ListItem
                                interval={intervalDownload}
                                form={this.props.form}
                                data={item}/>
                        )
                    })}
                </div>
            </div>
        )

    }
}

export default SourceData;
