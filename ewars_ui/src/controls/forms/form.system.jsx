import React from "react";

import FIELD_TYPES from "../../constants/const.fields";
import VALIDATIONS from "../../constants/const.validations";
import FormUtils from "../../utils/form_utils";

const NO_PADS = [
    "NUMBER",
    "NUMERIC",
    "TEXT",
    "SELECT",
    "DATE",
    "LOCATION"
];

class Unimplemented extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>Field type not implemented</div>
        )
    }
}

class HeaderField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-header">
                {this.props.data.label.en || this.props.data.label}
            </div>
        )
    }
}

class DisplayField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <input type="text" disabled={true} value={this.props.value}/>
        )

    }
}


class SectionHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-section-header">
                <div className="form-section-inner">
                    <div className="title">{this.props.data.title.en || this.props.data.title}</div>
                    <div className="description">
                        {this.props.data.description.en || this.props.data.description}
                    </div>
                </div>
            </div>
        )
    }
}


class FormField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.field.type == 'header') {
            return (
                <HeaderField data={this.props.field}/>
            )
        }

        if (this.props.field.type == 'section') {
            return (
                <SectionHeader data={this.props.field}/>
            )
        }

        let Ctrl;

        if (FIELD_TYPES[this.props.field.type.toUpperCase()]) {
            Ctrl = FIELD_TYPES[this.props.field.type.toUpperCase()];
        } else {
            Ctrl = Unimplemented;
        }

        if (this.props.field.type == 'display') {
            Ctrl = DisplayField;
        }

        let value = "";
        if (this.props.data) {
            value = this.props.data[this.props.field.name] || "";
        }

        let showLabel = true;
        if (this.props.field.label === false) showLabel = false;

        if (this.props.field.vertical) {
            return (
                <div className="form-field">
                    <div className="column">
                        {showLabel ?
                            <div className="row label">{this.props.field.label.en || this.props.field.label}</div>
                            : null}
                        <div className="row control">
                            <Ctrl
                                errors={this.props.errors}
                                onChange={this.props.onChange}
                                value={value}
                                {...this.props.field}/>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="form-field">
                <div className="row">
                    {showLabel ?
                        <div className="column label">{this.props.field.label.en || this.props.field.label}</div>
                        : null}
                    <div className="column control">
                        <Ctrl
                            errors={this.props.errors}
                            onChange={this.props.onChange}
                            value={value}
                            {...this.props.field}/>
                    </div>
                </div>
            </div>
        )
    }
}

class SystemForm extends React.Component {
    static defaultProps = {
        data: {}
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="system-form">
                {(this.props.definition || []).map(item => {
                    return (
                        <FormField data={this.props.data || {}} field={item}/>
                    )
                })}

            </div>
        )
    }
}

export default SystemForm;
