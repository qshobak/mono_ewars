var FormField = require("./FormField.react");
let FormUtils = require("../utils/FormUtils");

import DateField from "./fields/DateField.react";

var MatrixField = require("./fields/MatrixField.react");
var SelectField = require('./fields/SelectField.react');
var TextAreaField = require('./fields/TextAreaField.react');
var TextInputField = require('./fields/TextInputField.react');
var DisplayField = require("./fields/DisplayField.react");
var ConditionsField = require("./fields/ConditionsField.react");
var RowField = require("./fields/RowField.react");
var TopoJSONField = require("./fields/TopoJSONField.react");
var LocationInputField = require("./fields/LocationInputField");
var LocationSelectField = require("./fields/LocationSelectField.react");
var LanguageStringField = require("./fields/LanguageStringInput.react");
var NumberField = require("./fields/NumberField.react");
var SlugField = require("./fields/SlugField.react");
var SwitchField = require("./fields/SwitchField.react");
var IndicatorSelector = require("../components/indicator/IndicatorSelectorComponent.react");
var PointField = require("./fields/point/PointSelectionField.react");
var MapGeometryField = require("./fields/MapGeometryField.react");
var SelectFieldOptionsField = require("./fields/system/SelectFieldOptionsField.react");
var HeaderField = require("./fields/HeaderField.react");
var AssignmentLocationField = require("./fields/report_fields/AssignmentLocationField.react");
var IndicatorDefinition = require("./fields/system/IndicatorDefinition.react");
// Only used in widget editors
var ComplexSourcesDefinition = require("./widget_editors/_shared/ComplexSourcesManager.react");

import AssignmentSelector from "../cmp/fields/AssignmentSelector";

import AgeField from "./fields/AgeField";
import RangeSelector from "../cmp/fields/RangeSelector";
import ColourThresholds from "../cmp/fields/ColourThresholds";
import ButtonGroup from "../cmp/fields/ButtonGroup";
import DOMSizeField from "../cmp/fields/DOMSizeField";
import Colour from "../cmp/fields/Colour";
import FileUploadField from "../cmp/fields/FileUploadField";
import LocationGroupField from "../cmp/fields/LocationGroupField";
import LatLngField from './fields/LatLngField';
import GeoEditor from "../cmp/fields/BoundingBox";
import FormFieldField from './fields/system/field.form_field';

class NullComponent extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (<div></div>)
    }
}

const FIELDS = {
    select: SelectField,
    text: TextInputField,
    string: TextInputField,
    textarea: TextAreaField,
    date: DateField,
    email: TextInputField,
    matrix: MatrixField,
    number: NumberField,
    point: PointField,
    display: DisplayField,
    conditions: ConditionsField,
    row: RowField,
    topojson: TopoJSONField,
    location: LocationSelectField,
    language_string: LanguageStringField,
    slug: SlugField,
    switch: SwitchField,
    indicator: IndicatorSelector,
    geometry: MapGeometryField,
    select_options: SelectFieldOptionsField,
    header: HeaderField,
    assignment_location: AssignmentSelector,
    indicator_definition: IndicatorDefinition,
    _complex_sources: ComplexSourcesDefinition,
    assignment: AssignmentSelector,
    age: AgeField,
    date_range: RangeSelector,
    colour_thresholds: ColourThresholds,
    button_group: ButtonGroup,
    dom_size: DOMSizeField,
    color: Colour,
    file: FileUploadField,
    location_group: LocationGroupField,
    content: NullComponent,
    lat_long: LatLngField,
    form_field: FormFieldField,
    bounds: GeoEditor
};

var NON_FIELDS = ["display", "grid"];

/**
 * Comparator used for sorting the fields in the definition by
 * their order property
 * @param a
 * @param b
 * @returns {number}
 * @private
 */
function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    var fields = _.map(sourceFields, function (token, name) {
        var field = JSON.parse(JSON.stringify(token));
        field.name = name;

        if (field.fields) {
            var children = _getFieldsAsArray(field.fields);
            field.fields = children;
        }

        return field;
    });

    return _.sortBy(fields, function (item) {
        return parseInt(item.order);
    });
}

function _recursiveSetValue(definition, data, path, errors) {
    for (var i in definition) {
        var field = definition[i];
        if (path) {
            definition[i].path = `${path}.${i}`;
        } else {
            definition[i].path = i;
        }

        if (field.fields) {
            // This field doesn't have a value, it has children which have values
            field.fields = _recursiveSetValue(field.fields, data[i] || {}, field.path, errors);
        }

        let key = i;
        if (field.nameOverride) key = field.nameOverride;

        if (key.indexOf(".") >= 0) {
            field.value = ewars.getKeyPath(key, data);
        } else {
            field.value = data[key] || field.defaultValue || "";
        }

        // Check for errors for this field
        // Only if it's a certain type of field
        if (NON_FIELDS.indexOf(field.type) < 0 && errors) {
            var fieldErrors = ewars.getKeyPath(definition[i].path, errors);
            if (fieldErrors) definition[i].error = fieldErrors;
        }

    }
    return definition;
}

/**
 * Check if a value isn't set, used for required fields
 * @param value
 * @returns {boolean}
 */
function isNull(value) {
    if (value == "") return true;
    if (value == undefined) return true;
    if (value == "null") return true;
    if (value == "None") return true;
    if (value == null) return true;
    if (!value) return true;
    return false;
}

/**
 * Form class - Used to render forms throughout the application*
 */
var Form = React.createClass({

    _fields: null,

    /**
     * Retrieve the initial state of the form*
     * @returns {{fields: {}, dirty: boolean, tabs: null}}
     */

    getInitialState: function () {
        return {
            fields: {},
            dirty: false,
            tabs: null,
            errors: {},
            disabled: false
        }
    },

    componentDidMount() {
        // console.log("REF", this.refs);
    },

    componentWillMount: function () {
    },

    enable: function () {
        this.state.disabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    disable: function () {
        this.state.disabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    getDefaultProps: function () {
        return {
            readOnly: true,
            light: true
        }
    },

    /**
     * Handles any changes to fields in the form and bubbles them up to the parent component*
     * @param property
     * @param value
     * @param path FQP for where the proeprty should be updated
     * @param * Any additional data that the field type has bubbled up (location, indicator, etc...)
     * @private
     * @param path
     */
    _handleChange: function (property, value, path, additionalData) {
        this.props.updateAction(this.props.data, property, value, path, additionalData);

        // TODO: Need to handle when the form is returned to it;s initial state by the users
        // track history of changes in client before a save call
        this.setState({
            dirty: true
        })
    },

    validateField: function (field_name, value) {
        let def = FormUtils.field(this.props.definition, field_name);
        this.state.errors[field_name] = [];

        if (def) {
            // Check if required
            if (def.required) {
                if (isNull(value)) {
                    this.state.errors[field_name].push(ewars.I18N(def.label) + " is a required field");
                }
            }

            if (def.type == "number") {
                if (def.min) {
                    if (value < def.min) {
                        this.state.errors[field_name].push("Can not be below " + def.min);
                    }
                }

                if (def.max) {
                    if (value > def.max) {
                        this.state.errors[field_name].push("Can not be greate than " + def.max);
                    }
                }
            }
        }

    },

    /**
     * Public functions called via ref to check if a forms data is valid
     */
    isValid: function () {
        return true;
    },

    /**
     * Processes and creates any grids defined for the form*
     * @param field
     * @returns {XML}
     * @private
     */
    _processGrid: function (field) {
        var grid;

        var columns = [];
        for (var idx in field.fields) {
            var ff = field.fields[idx];
            var key = _.uniqueId("COLUMN_");
            columns.push(
                <GridColumn
                    key={key}
                    name={idx}
                    label={ff.label}/>
            )
        }

        var data = null;
        if (this.props.data[field.name]) data = this.props.data[field.name];

        grid = (
            <div className="form-grid">
                <GridComponent
                    editable={true}
                    data={data}>
                    {columns}
                </GridComponent>
            </div>
        );

        return grid;
    },

    _getRuleResult: function (rule, data) {
        var result;

        var sourceValue = ewars.getKeyPath(rule[0], data);
        if (sourceValue == null || sourceValue == undefined) return false;

        const isNumber = (val) => {
            try {
                parseFloat(val);
                return true;
            } catch (e) {
                return false;
            }
        };

        switch (rule[1]) {
            case "eq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) >= 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue == rule[2];
                }
                break;
            case "ne":
            case "neq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) < 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue != rule[2];
                }
                break;
            case "gt":
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) > parseFloat(rule[2]);
                }
                break;
            case "lt":
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) < parseFloat(rule[2]);
                }
                break;
            case 'gte':
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) >= parseFloat(rule[2]);
                }
                break;
            case 'lte':
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) <= parseFloat(rule[2]);
                }
                break;
            default:
                result = false;
                break
        }

        return result;
    },

    _checkFieldVisible: function (field, data) {
        if (field.conditional_bool == true) {
            var result = false;

            if (!field.conditions) return true;
            if (!field.conditions.rules) return true;

            if (["ANY", "any"].indexOf(field.conditions.application) >= 0) {
                // Only one of the rules has to pass

                for (var conIdx in field.conditions.rules) {
                    var rule = field.conditions.rules[conIdx];

                    var tmpResult = this._getRuleResult(rule, data);
                    if (result != true && tmpResult == true) result = true;
                }

            } else {
                var ruleCount = field.conditions.rules.length;
                var rulePassCount = 0;

                for (var ruleIdx in field.conditions.rules) {
                    var rule = field.conditions.rules[ruleIdx];
                    var ruleResult = this._getRuleResult(rule, data);
                    if (ruleResult) rulePassCount++;
                }

                if (ruleCount == rulePassCount) result = true;

            }
            return result;
        } else {
            return true;
        }
    },

    /**
     * Renders the form into the parent container*
     * @returns {XML}
     */
    render: function () {
        var tabs = [];

        var readOnly = true;
        if (this.props.readOnly == false) readOnly = false;
        if (this.state.disabled == true) readOnly = true;

        var cancelText = "Close";
        if (this.state.dirty == true) {
            cancelText = "Cancel";
        }

        var definition;
        var defCopy = JSON.parse(JSON.stringify(this.props.definition));
        if (this.props.data) definition = _recursiveSetValue(defCopy, this.props.data, null, this.props.errors);

        var nodes = [];

        // Sort and set up the fields
        this._fields = _getFieldsAsArray(definition);

        // Instantiate defaults
        _.each(this._fields, function (field) {


            if (FIELDS[field.type]) {
                var FieldControl = FIELDS[field.type];
                var options = (field.options ? field.options : null);


                // Handle default definition
                if (field.value == null || field.value == undefined) {
                    if (field.defaultValue != null && field.defaultValue != undefined) {
                        field.value = field.defaultValue;
                    }
                }

                var visible = true;
                if (field.conditions) {
                    visible = this._checkFieldVisible(field, this.props.data);
                }

                var fields;
                if (this.props.conditionalFieldOptions) {
                    fields = this.props.conditionalFieldOptions;
                }

                if (visible) {
                    if (field.type == 'content') {
                        nodes.push(
                            <div className="form-field">
                                <div className="label"></div>
                                <div className="field-wrapper">
                                    <div className="ide-row">
                                        <div className="ide-col">
                                            <p>{field.content}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    } else {
                        var children;

                        if (field.fields) {
                            children = [];

                            _.each(field.fields, function (subField, fieldKey) {
                                var childOptions = (field.options ? field.options : null);
                                var SubFieldControl = FIELDS[field.type];

                                var wrapperKey = "WRAPPER_" + subField.type + "_" + fieldKey;
                                var controlKey = "CONTROL_" + subField.type + "_" + fieldKey;

                                let isVisible = true;
                                if (subField.conditional_bool) {
                                    isVisible = this._checkFieldVisible(subField, this.props.data);
                                }

                                if (isVisible) {
                                    children.push(
                                        <FormField
                                            key={wrapperKey}
                                            ref={fieldKey}
                                            field={field}
                                            errors={this.props.errors}
                                            readOnly={readOnly}>
                                            <SubFieldControl
                                                data={this.props.data}
                                                readOnly={readOnly}
                                                fieldOptions={fields}
                                                form={this.props.definition}
                                                map={FIELDS}
                                                errors={this.props.errors}
                                                name={fieldKey}
                                                definition={definition}
                                                value={field.value}
                                                options={childOptions}
                                                Form={Form}
                                                config={field}
                                                key={controlKey}
                                                onUpdate={this._handleChange}/>
                                        </FormField>
                                    )
                                }
                            }, this)
                        }

                        var wrapperKey = "WRAPPER_" + field.type + "_" + field.name;
                        var controlKey = "CONTROL_" + field.type + "_" + field.name;

                        nodes.push(
                            <FormField
                                key={wrapperKey}
                                field={field}
                                ref={field.name}
                                errors={this.props.errors}
                                readOnly={readOnly}>
                                <FieldControl
                                    data={this.props.data}
                                    readOnly={readOnly}
                                    key={controlKey}
                                    fieldOptions={fields}
                                    form={this.props.definition}
                                    map={FIELDS}
                                    errors={this.props.errors}
                                    name={field.name}
                                    value={field.value}
                                    definition={definition}
                                    options={options}
                                    config={field}
                                    Form={Form}
                                    onUpdate={this._handleChange}>
                                    {children}
                                </FieldControl>
                            </FormField>
                        );
                    }
                }

            }
        }, this);

        var formClass = "form";
        if (this.props.readOnly) formClass += " read-only";
        if (this.props.light) formClass += " light";

        return (
            <div className={formClass}>
                {nodes}
            </div>

        )
    }
});

module.exports = Form;