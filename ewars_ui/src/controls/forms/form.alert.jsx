import React from "react";

import FIELD_TYPES from "../../constants/const.fields";
import VALIDATIONS from "../../constants/const.validations";
import FormUtils from "../../utils/form_utils";

const NO_PADS = [
    "NUMBER",
    "NUMERIC",
    "TEXT",
    "SELECT",
    "DATE",
    "LOCATION"
]

class HeaderField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-header">
                {this.props.data.label.en || this.props.data.label}
            </div>
        )
    }
}

class Missing extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                This field type is not currently supported
            </div>
        )
    }
}

class Field extends React.Component {
    static defaultProps = {
        record: {data: {}}
    }

    constructor(props) {
        super(props);
    }

    _change = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let isVisible = FormUtils.isFieldVisibleLegacy(this.props.data, this.props.record);

        if (!isVisible) return null;

        if (this.props.data.type == 'header') {
            return <HeaderField data={this.props.data}/>
        }

        if (this.props.data.type == "matrix") {
            let rows = [];

            let f_rows = FormUtils.flatten(this.props.data.fields, this.props.data.name);

            f_rows.forEach(row => {
                if (!FormUtils.isFieldVisibleLegacy(row, this.props.record)) return null;

                let f_cells = FormUtils.flatten(row.fields, row.name);

                let cells = [];

                if (row.show_row_label) {
                    cells.push(
                        <div className="column br matrix-row-label">{row.label.en || row.label}</div>
                    )
                }

                f_cells.forEach(cell => {
                    if (!FormUtils.isFieldVisibleLegacy(cell, this.props.record)) return null;
                    cells.push(
                        <div className="column matrix-row-cell">
                            <Field
                                readOnly={this.props.readOnly}
                                record={this.props.record}
                                onChange={this.props.onChange}
                                data={cell}/>
                        </div>
                    );
                })

                rows.push(
                    <div className="matrix-row row" style={{flex: 2}}>
                        {cells}
                    </div>
                )
            })
            return (
                <div className="field-matrix">
                    <div className="label">{this.props.data.label.en || this.props.data.label}</div>
                    <div className="matrix-wrapper">
                        {rows}
                    </div>
                </div>
            )
        }


        let Cmp = FIELD_TYPES[this.props.data.type.toUpperCase()] || Missing;

        let style = {display: "block"};
        if (NO_PADS.indexOf(this.props.data.type.toUpperCase()) >= 0) style.padding = "0px";

        let label = this.props.data.label.en || this.props.data.label;
        if (this.props.data.required) label += " *";

        let value = this.props.record[this.props.name] || "";


        return (
            <div className="field">
                <div className="label">{label}</div>
                <div className="clearer"></div>
                <div className="control" style={style}>
                    <div style={style}>
                        <Cmp
                            readOnly={this.props.readOnly}
                            value={value}
                            onChange={this._change}
                            {...this.props.data}/>
                    </div>
                </div>
                {this.props.data.help ?
                    <div className="guidance">{this.props.data.help}</div>
                    : null}
            </div>
        )
    }
}

class WorkflowStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let fields = this.props.definition.filter(item => {
            return (item.stage == this.props.stage.uuid);
        });
        return(
            <div className="form-stage">
                <div className="form-stage-title">
                    <div className="row">
                        <div className="stage-line"></div>
                        <div className="column whiteout" style={{maxWidth: "20px"}}>
                            <div className="stage-state">
                                <i className="fal fa-check-circle"></i>
                            </div>
                        </div>
                        <div className="column" style={{display: "block"}}>
                            <div className="stage-name">{this.props.stage.name}</div>
                        </div>
                        <div className="column">

                        </div>
                    </div>
                </div>
                <div className="form-stage-fields">
                    {fields.map(item => {
                        return (
                        <Field
                        readOnly={this.props.readOnly}
                        record={this.props.data || {data: {}}}
                        onChange={this.props.onChange}
                        data={item}/>
                        )
                    })}
                </div>
                <div className="form-stage-controls">
                    <button>Submit</button>
                    <button>Go back</button>
                    <button>Save</button>

                </div>
            </div>
        )
    }
}

class WorkflowForm extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let stages = this.props.workflow.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        return (
            <div className="row">
                <div className="reporting-form">
                    {stages.map(item => {
                        return (
                            <WorkflowStage
                                definition={this.props.definition}
                                data={this.props.data}
                                onChange={this.props.onChange}
                                stage={item}/>
                        )
                    })}

                </div>
            </div>
        )
    }
}

class AlertForm extends React.Component {
    static defaultProps = {
        data: {data: {}},
        definition: {},
        workflow: null,
        readOnly: false
    }

    constructor(props) {
        super(props);
    }

    _onRootChange = (prop, value) => {
        this.props.onChange(prop, value, true);
    };

    render() {
        console.log(this.props);
        if (this.props.workflow) {
            return (
                <WorkflowForm
                    data={this.props.data}
                    definition={this.props.definition || []}
                    workflow={this.props.workflow || []}
                    onChange={this.props.onChange}
                    readOnly={this.props.readOnly}/>
            )
        }

        return (
            <div className="row">
                <div className="reporting-form">
                    <p className="form-required">Fields marked with a * are required</p>
                    {this.props.definition.map(item => {
                        return (
                            <Field
                                readOnly={this.props.readOnly}
                                record={this.props.data || {data: {}}}
                                onChange={this.props.onChange}
                                data={item}/>
                        )
                    })}

                </div>
            </div>
        )
    }
}

export default AlertForm;
