import React from "react";

import FIELD_TYPES from "../../constants/const.fields";
import VALIDATIONS from "../../constants/const.validations";
import FormUtils from "../../utils/form_utils";
import ActionGroup from "../../controls/c.actiongroup.jsx";

const NO_PADS = [
    "NUMBER",
    "NUMERIC",
    "TEXT",
    "SELECT",
    "DATE",
    "LOCATION",
    "BUTTONSET",
    "ASSOCIATED",
    "REPEATER",
    "ORGANIZATION",
    "ROLE",
    "INTERVAL",
    "TEXTAREA"
];

class HeaderField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-header">
                {this.props.data.label.en || this.props.data.label}
            </div>
        )
    }
}

class Missing extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                This field type is not currently supported
            </div>
        )
    }
}


class Field extends React.Component {
    static defaultProps = {
        root: false,
        record: {data: {}},
        errors: {},
        stage: null,
        fid: null
    }

    constructor(props) {
        super(props);
    }

    _change = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let isVisible = FormUtils.isFieldVisibleLegacy(this.props.data, this.props.record, this.props.stage);

        if (!isVisible) return null;

        if (this.props.data.type == "header") {
            if (isVisible) {
                return <HeaderField data={this.props.data}/>
            } else {
                return null;
            }
        }

        if (this.props.data.type == "matrix") {

            let rows = [];

            let f_rows = FormUtils.flatten(this.props.data.fields, this.props.data.name);

            f_rows.forEach(row => {
                if (!FormUtils.isFieldVisibleLegacy(row, this.props.record, this.props.stage)) return null;

                let f_cells = FormUtils.flatten(row.fields, row.name);

                let cells = [];

                if (row.show_row_label != false || row.show_row_labe == null) {
                    cells.push(
                        <div className="column br matrix-row-label">{row.label.en || row.label}</div>
                    )
                }

                f_cells.forEach(cell => {
                    if (!FormUtils.isFieldVisibleLegacy(cell, this.props.record, this.props.stage)) return null;
                    cells.push(
                        <div className="column matrix-row-cell">
                            <Field
                                errors={this.props.errors}
                                readOnly={this.props.readOnly}
                                record={this.props.record}
                                onChange={this.props.onChange}
                                fid={this.props.fid}
                                data={cell}/>
                        </div>
                    );
                })

                rows.push(
                    <div className="matrix-row row" style={{flex: 2}}>
                        {cells}
                    </div>
                )
            })
            return (
                <div className="field-matrix">
                    <div className="label">{this.props.data.label.en || this.props.data.label}</div>
                    <div className="matrix-wrapper">
                        {rows}
                    </div>
                </div>
            )
        }


        let Cmp = FIELD_TYPES[this.props.data.type.toUpperCase()] || Missing;

        let style = {display: "block"};
        if (NO_PADS.indexOf(this.props.data.type.toUpperCase()) >= 0) style.padding = "0px";
        if (this.props.data.type == "BUTTONSET") {
            style.border = "none";
        }

        let label;
        if (this.props.data.label != false) {
            let required = this.props.data.required ? " *" : "";
            label = (
                <div className="label">{this.props.data.label.en || this.props.data.label}{required}</div>
            )
        }

        let value;
        if (this.props.allRoot) {
            value = this.props.record[this.props.data.name] || "";
        } else {
            if (this.props.root) {
                value = this.props.record[this.props.data.name] || "";
            } else {
                value = this.props.record.data[this.props.data.name] || "";
            }
        }

        let hasErrors = this.props.errors[this.props.data.name] || null;
        let errorCmp;
        if (hasErrors) {
            switch (hasErrors) {
                case "NONE":
                    errorCmp = <div className="error">Please provide a valid value</div>;
                    break;
                default:
                    errorCmp = <div className="error">An unknown error occurred</div>;
                    break;
            }

        }

        let className = 'field';
        if (this.props.data.vertical) className += " vertical-field";

        return (
            <div>
                <div className={className}>
                    {label}
                    <div className="clearer"></div>
                    <div className="control" style={style}>
                        <div style={style}>
                            <Cmp
                                readOnly={this.props.readOnly}
                                value={value}
                                fid={this.props.fid}
                                onChange={this._change}
                                {...this.props.data}/>
                        </div>
                    </div>
                    {errorCmp}
                </div>
                {this.props.data.help ?
                    <div className="guidance">{this.props.data.help}</div>
                    : null}
            </div>
        )
    }
}

class ErrorCell extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let cellHeight = "30px";
        return (
            <div className="error-cell" style={{maxHeight: cellHeight}}>

            </div>
        )
    }
}

class WorkflowMenuItem extends React.Component {
    render() {
        let markerStyle = {maxWidth: "8px"};
        if (this.props.stage == this.props.data.uuid) markerStyle.background = "lightgreen";
        return (
            <div className="son-list-item" style={{padding: "0", cursor: "pointer"}}>
                <div className="row">
                    <div className="column" style={markerStyle}></div>
                    <div className="column" style={{padding: "3px", color: "#333333"}}>{this.props.data.name}</div>
                    <div className="column" style={{maxWidth: "20px", padding: "3px"}}>
                        <i className="fal fa-check-square"></i>
                    </div>
                </div>
            </div>
        )
    }
}

const FORM_ACTIONS = [
    ['fa-share', 'SUBMIT', 'Submit stage'],
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-pencil', 'EDIT', 'Edit stage']
];

const FORM_ACTIONS_COMPLETED = [
    ['fa-pencil', 'EDIT', 'Edit stage']
];

const FORM_ACTIONS_AMEND = [
    ['fa-send', 'SUBMIT', 'Submit amendment'],
    ['fa-times', 'CANCEL', 'Cancel amendment']
];

class WorkflowForm extends React.Component {
    constructor(props) {
        super(props)
    }

    _action = (action) => {
        switch (action) {
            case "SUBMIT":
                this.props.onSubmit();
                break;
            default:
                break;
        }
    };

    render() {
        let stages = this.props.workflow.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        return (
            <div className="row">
                <div className="column br panel block block-overflow">
                    {stages.map(item => {
                        return <WorkflowMenuItem
                            stage={this.props.stage}
                            data={item}/>
                    })}
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={FORM_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="reporting-form">
                        {this.props.definition.map(item => {
                            let root = item.root;
                            if (this.props.allRoot) root = true;
                            return (
                                <Field
                                    stage={this.props.stage}
                                    root={root}
                                    readOnly={this.props.readOnly}
                                    record={this.props.data || {data: {}}}
                                    onChange={this.props.onChange}
                                    data={item}/>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}


class Form extends React.Component {
    static defaultProps = {
        data: {data: {}},
        definition: {},
        errors: {},
        stage: null,
        fid: null,
        readOnly: false,
        onSubmitStage: () => {
        },
        onRetractStage: () => {
        },
        allRoot: false,
        showErrorBar: true
    };

    constructor(props) {
        super(props);
    }

    _onRootChange = (prop, value) => {
        this.props.onChange(prop, value, true);
    };

    render() {
        let flat = FormUtils.flatten(this.props.definition);

        if (this.props.workflow && this.props.workflow.length > 0) {
            return (
                <WorkflowForm
                    stage={this.props.stage}
                    data={this.props.data}
                    fid={this.props.fid}
                    onSubmit={this.props.onSubmit}
                    definition={flat || []}
                    workflow={this.props.workflow || []}
                    onChange={this.props.onChange}
                    readOnly={this.props.readOnly}/>
            )
        }

        return (
            <div className="row">
                <div className="reporting-form">
                    <p className="form-required">Fields marked with a * are required</p>
                    {flat.map(item => {
                        let root = item.root;
                        if (this.props.allRoot) root = true;
                        return (
                            <Field
                                root={root}
                                fid={this.props.fid}
                                allRoot={this.props.allRoot}
                                errors={this.props.errors || {}}
                                readOnly={this.props.readOnly}
                                record={this.props.data || {data: {}}}
                                onChange={this.props.onChange}
                                data={item}/>
                        )
                    })}

                </div>
                {this.props.showErrorBar ?
                    <div className="column block err-bar">

                    </div>
                    : null}
            </div>
        )
    }
}

export default Form;
