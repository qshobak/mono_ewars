import React from "react";

import ReactMde, {ReactMdeType} from "react-mde";
import Showdown from "showdown";


class MarkdownEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mdeState: {
                markdown: (this.props.value || "")
            }
        }

        this.converter = new Showdown.Converter({
            tables: true,
            simplifiedAutoLink: true,
            strikethrough: true,
            tasklists: true
        });
    }

    handleValueChange = (e) => {
        this.setState({mdeState: e});
    };

    render() {
        return (
            <ReactMde
                layout="tabbed"
                buttonContentOptions={{
                    iconProvider: (name) => {
                        return <i className={`fal fa-${name}`}/>;
                    }
                }}
                onChange={this.handleValueChange}
                editorState={this.state.mdeState}
                generateMarkdownPreview={(markdown) => {
                    Promise.resolve(this.converter.makeHtml(markdown));
                }}/>
        )
    }
}

export default MarkdownEditor;