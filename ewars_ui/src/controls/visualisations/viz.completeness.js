import React from "react";
import AppContext from "../../context_provider";

import Highcharts from "highcharts";

Highcharts.setOptions(
    animation: false,
    chart: {
        animation: false
    }
});

const _f = (val, alternate) => {
    if (val == undefined || val == '' || val == null) return alternate;
    if (isNum(val)) return parseInt(val);
    return val;
}

class VizCompleteness extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let options = {
            chart: {
                renderTo: this._el,
                backgroundColor: _f(props.backgroundColor, undefined),
                borderColor: _f(props.borderColor, undefined),
                borderWidth: _f(props.borderWidth, 0),
                borderRadius: _f(props.borderRadius, 0),
                markerRadius: 0
            },
            title: {text: undefined},
            subtitle: {text: undefined},
            legend: {enabled: false},
            credits: {enabled: false},
            yAxis: [],
            xAxis: [],
            series: [],
            plotOptions: {
                column: {
                    groupPadding: 0.1,
                    pointPadding: 0,
                    borderWidth: 1
                }
            }
        };

        this._chart = Highcharts.chart(this.options);
    }

    render() {
        return (
            <div ref={(el) => this._el = el} className="viz-submissions">

            </div>
        )
    }
}

export default VizCompleteness;
