import React from "react";

class GridItem extends React.Component {
    static defaultProps = {
        children: [],
        cols: 0,
        containerWidth: null,
        rowHeight: null,
        margin: null,
        maxRows: null,
        containerPadding: null

        x: null,
        y: null,
        w: null,
        h: null
    };

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="grid-cell">

            </div>
        )
    }
}

export default GridCell;
