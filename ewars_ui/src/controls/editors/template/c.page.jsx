import React from "react";

import Widget from "./c.widget.jsx";

import {PLOT_TYPES} from '../../../constants/const.types.jsx';
import PlotEditor from "../_plot/plot_screen.jsx";

import {Responsive, WidthProvider} from 'react-grid-layout';
import utils from "../../../utils/utils";

const ResponsiveGridLayout = WidthProvider(Responsive);

class Page extends React.Component {
    static defaultProps = {
        page: {
            uuid: null,
            orientation: "PORTRAIT",
            layout: [],
        },
        data: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            widget: null
        }
    }

    _onClosePlot = () => {
        this.setState({
            widget: null
        })
    };

    _onPlotSave = (data) => {
        let widgets = this.props.data;
        widgets[this.state.widget._] = data;
        this.setState({
            widget: null
        }, () => {
            this.props.onPageChange(this.props.page.layout, widgets);
        });
    };

    _onLayoutChange = (layout) => {
        // this.props.onLayoutChange(layout);
        this.props.onPageChange(layout, this.props.data)
    };

    onBreakpointChange = (breakpoint, cols) => {
    };

    _onEdit = (data) => {
        this.setState({
            widget: utils.copy(data)
        })
    };

    _onRootDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("e", '{}'));
        let layout = this.props.page.layout || [];
        let uuid = utils.uuid();
        layout.push({
            i: uuid,
            x: 0,
            y: 0,
            h: 5,
            w: 5,
            minW: 2,
            minH: 2
        });

        let widgets = this.props.data || {};
        widgets[uuid] = {
            _: uuid,
            type: data.t
        };

        this.props.onPageChange(layout, widgets);
    };

    _onRemoveItem = (data) => {
        let rIndex;
        let layout = this.state.data.definition;
        let widgets = this.state.data.widgets;
        delete widgets[data._];
        layout.forEach((item, index) => {
            if (item.i == data._) {
                rIndex = index;
            }
        })
        layout.splice(rIndex, 1);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout,
                widgets: widgets
            }
        })
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    render() {
        let outerClass = "column block block-overflow";
        let innerClass = "page-portrait";

        if (this.props.data.orientation == "LANDSCAPE") {
            outerClass = "column";
            innerClass = "page-landscape";
        }

        let view;
        if (this.state.widget) {
            if (PLOT_TYPES.indexOf(this.state.widget.type) >= 0) {
                view = <PlotEditor
                    onSave={this._onPlotSave}
                    onClose={this._onClosePlot}
                    data={this.state.widget}/>

            } else {
                view = (
                    <WidgetEditor
                        onSave={this._onPlotSave}
                        onClose={this._onClosePlot}
                        data={this.state.widget}/>
                )
            }
        }

        return (
            <div className={outerClass}
                 onDragOver={this._onDragOver}
                 onDrop={this._onRootDrop}>
                <div className={innerClass}>
                    <ResponsiveGridLayout
                        className="layout"
                        layouts={{
                            lg: this.props.page.layout || [],
                            md: this.props.page.layout || [],
                            sm: this.props.page.layout || []
                        }}
                        rowHeight={30}
                        autoSize={false}
                        verticalCompact={true}
                        onBreakPointChange={this.onBreakpointChange}
                        onLayoutChange={this._onLayoutChange}
                        breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                        cols={{lg: 36, md: 36, sm: 36, xs: 4, xxs: 2}}>
                        {(this.props.page.layout || []).map(item => {
                            return (
                                <div key={item.i}>
                                    <Widget
                                        key={"WIDG_" + item.i}
                                        onEdit={this._onEdit}
                                        onDelete={this._onRemoveItem}
                                        data={this.props.data[item.i]}/>
                                </div>
                            )
                        })}
                    </ResponsiveGridLayout>
                </div>
                {view}
            </div>
        );
    }
}

export default Page;