import React from "react";

import ActionGroup from "../../c.actiongroup.jsx";

class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let className = "tmpl-page";
        if (this.props.data.orientation == "LANDSCAPE") className += " landscape";

        return (
            <div className={className} draggable={true} onClick={() => {this.props.onSelect(this.props.data.uuid)}}>
                <div className="tmp-page-inner">
                    <div className="numeral">{this.props.index + 1}</div>
                </div>
            </div>
        )
    }
}

class Pages extends React.Component {
    static defaultProps = {
        pages: []
    };

    constructor(props) {
        super(props);

        this.props.onAddPage();
    }

    render() {
        return (
            <div className="column br panel">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={[['fa-plus', 'ADD', 'ADD_PAGE']]}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    {this.props.pages.map(item => {
                        return <Page data={item} onSelect={this.props.onPageSelect}/>
                    })}
                </div>
            </div>
        )
    }
}

export default Pages;