import React from "react";

import PlotUtils from "../plot_utils";
import ActionGroup from "../../../c.actiongroup.jsx";
import Card from "./c.card.jsx";

import TextField from "../../../fields/f.text.jsx";

import utils from "../../../../utils/utils";

const GROUP_ACTIONS = [
    ['fa-calculator', 'CALC', 'Add calculation'],
    ['fa-trash', 'DELETE', 'Delete group']
];


const GROUP_STYLE = {
    minHeight: "300px",
    background: "rgba(255,255,255,0.05",
    overflowX: "scroll",
    overflowY: "hidden"
}

const INNER_STYLE = {
    display: "flex",
    flexWrap: "nowrap"
};


class DataGroup extends React.Component {
    static defaultProps = {
        type: "DEFAULT",
        nodes: [],
        group: null
    };

    constructor(props) {
        super(props);

        window.addEventListener("resize", this._handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._handleResize);
    }

    _handleResize = () => {
        this._el.style.width = this._el.parentNode.clientWidth + "px";
    };

    componentDidMount() {
        this._el.style.width = this._el.parentNode.clientWidth + "px";
    }

    _action = (action) => {
        switch (action) {
            case "CALC":
                this._addCalc();
                break;
            default:
                break;
        }
    };

    _addCalc = () => {
        let newItem = {
            t: "C",
            fx: "",
            _: utils.uuid(),
            agg: "SUM",
            g: this.props.group._
        }

        this.props.onAddNode(newItem);
    };

    _dragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("n"));

        let newItem = utils.copy(data);
        newItem.g = this.props.group._;
        newItem._ = utils.uuid();

        if (data.label) {
            newItem.lbl = data.label;
        }

        this.props.onAddNode(newItem);
    };

    _removeNode = (id) => {
        this.props.onRemoveNode(id);
    };

    render() {

        let group_id = this.props.group._;
        let dimensions = PlotUtils.getGroupDimensions(group_id, this.props.nodes),
            measures = PlotUtils.getGroupMeasures(group_id, this.props.nodes),
            axes = PlotUtils.getAxes(this.props.nodes);

        let onDragOver = this._dragOver;
        let onDrop = this._onDrop;

        let headerStyle = {
            maxHeight: "35px",
            minHeight: "35px",
            flex: 1
        };

        let actions = GROUP_ACTIONS;

        if (this.props.type == "CALCULATION") {
            actions = [
                ['fa-trash', 'DELETE', 'Delete calculation']
            ];
            headerStyle.background = 'rgb(58, 32, 1)';
        }


        return (
            <div className="data-group">
                <div className="column" onDrop={onDrop} onDragOver={onDragOver}>
                    <div
                        className="row bb gen-toolbar"
                        style={headerStyle}>
                        <div className="column grid-title">{this.props.group.title}</div>
                        <ActionGroup
                            naked={true}
                            actions={actions}
                            onAction={this._action}/>
                    </div>
                    {this.props.type == "CALCULATION" ?
                        <div className="row bb gen-toolbar">
                            <div className="column"
                                 style={{maxWidth: "50px", background: "black", textAlign: "center"}}>
                                <i>fx</i>
                            </div>
                            <div className="column">
                                <TextField/>
                            </div>
                        </div>
                        : null}
                    <div className="row"
                         ref={(el) => this._el = el}
                         style={GROUP_STYLE}>
                        <div style={INNER_STYLE}>

                            {dimensions.map(item => {
                                return (
                                    <Card
                                        onRemoveNode={this._removeNode}
                                        definition={this.props.data} data={item}/>
                                )
                            })}
                            {measures.map(item => {
                                return (
                                    <Card
                                        onRemoveNode={this._removeNode}
                                        definition={this.props.data} data={item}/>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataGroup;