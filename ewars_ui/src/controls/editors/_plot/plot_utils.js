let PlotUtils = {};

const NODE_SORT = (a, b) => {
    if (a.order > b.order) return 1;
    if (a.order < b.order) return -1;
    return 0;
};

/**
 * Get root-level measures
 * @param data
 * @returns {*}
 */
PlotUtils.getGroupMeasures = (group, data) => {
    let items = data.filter(item => {
        return ['M', 'C'].indexOf(item.t) >= 0 && !item._t && item.g == group;
    });


    return items.sort(NODE_SORT);
};

/**
 * Get root-level dimensions
 * @param data
 * @returns {*}
 */
PlotUtils.getGroupDimensions = (group, data) => {
    let items = data.filter(item => {
        return item.t == "D" && !item._t && item.g == group;
    })

    return items.sort(NODE_SORT);
};

/**
 * Get nodes that modify a specific parent node
 * @param nid
 * @param data
 * @returns {*}
 */
PlotUtils.getNodeModifiers = (nid, data) => {
    let items = data.filter(item => {
        return item._p == nid;
    });

    return items;
};

/**
 * Get axes from definition
 * @param data
 * @returns {*}
 */
PlotUtils.getAxes = (data) => {
    return data.filter(item => {
        return item.t == 'A';
    });
};

/**
 * Get Calculations in the definition
 */
PlotUtils.getCalcs = (data) => {
    return data.filter(item => {
        return item.t == "C";
    });
};

export default PlotUtils;