import React from "react";

import PlotUtils from "./plot_utils";
import utils from "../../../utils/utils";
import DimensionTree from "../../c.tree.dimensions.jsx";
import MeasureTree from "../../c.tree.measures.jsx"
import ControlsList from "./controls/c.controls.jsx";
import ActionGroup from "../../c.actiongroup.jsx";
import Group from "./controls/c.group.jsx";


const OVERLAY_STYLE = {
    position: "fixed",
    top: "8px",
    left: "8px",
    right: "8px",
    bottom: "8px",
    zIndex: 999999,
    background: "rgba(0,0,0,0.5)",
    display: "flex",
    flexDirection: "column"
};

const INNER_STYLE = {
    border: "1px solid #505152",
    borderRadius: "3px",
    background: "#333333"
}

const DEFAULTS = {
    n: [],
    g: []
};


const PLOT_ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CLOSE', 'Close']
]


class PlotScreen extends React.Component {
    static defaultProps = {
        onClose: null,
        onSave: null,
        type: "DEFAULT",
        data: {
            _: utils.uuid(),
            n: [],
            title: "Untitled Plot",
            g: {_: utils.uuid(), title: "New Group"},
            style: {}
        }
    };

    state = {
        sideView: "MEASURES"
    };

    constructor(props) {
        super(props);

        let data = utils.copy(this.props.data);
        if (!data.n) data.n = [];
        if (!data._) data._ = utils.uuid();
        if (!data.g) data.g = [{_: utils.uuid(), title: "New Group"}];
        if (!data.style) data.style = {};
        this.state.data = data;
    }

    _view = (view) => {
        this.setState({sideView: view})
    };

    _addNode = (node) => {
        let items = this.state.data.n || [];
        items.push(node);

        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                n: items
            }
        })
    };

    _removeNode = (id) => {
        let items = this.state.data.n || [];

        let result = items.filter(item => {
            return item._ != id && item.p != id;
        });

        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                n: result
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this.props.onClose();
                break;
            case "SAVE":
                this.props.onSave(this.state.data._, this.state.data);
                break;
            default:
                break;
        }
    };

    render() {
        console.log(this.state.data);

        let calculations = PlotUtils.getCalcs(this.state.data.n);
        let axes = PlotUtils.getAxes(this.state.data.n);
        console.log(axes);

        return (
            <div style={OVERLAY_STYLE}>
                <div className="row" style={INNER_STYLE}>
                    <div className="column br" style={{maxWidth: "25%"}}>
                        <div className="row bb gen-toolbar"
                             style={{maxHeight: "35px"}}
                             onClick={() => this._view("MEASURES")}>
                            <div className="column grid-title">Measures</div>
                            <ActionGroup
                                naked={true}
                                actions={[['fa-plus', 'ADD_MEASURE', 'Create Measure']]}
                                onAction={this._action}/>
                        </div>
                        <div className="row block block-overflow">
                            <MeasureTree
                                draggable={true}/>
                        </div>

                        <div className="row bb bt gen-toolbar"
                             style={{maxHeight: "35px"}}
                             onClick={() => this._view("DIMENSIONS")}>
                            <div className="column grid-title">Dimensions</div>
                            <ActionGroup
                                naked={true}
                                actions={[['fa-plus', 'ADD_DIMENSION', 'Create dimension']]}
                                onAction={this._action}/>
                        </div>
                        <div className="row block block-overflow">
                            <DimensionTree
                                draggable={true}/>
                        </div>

                        <div className="row bt bb gen-toolbar"
                             style={{maxHeight: "35px"}}
                             onClick={() => this._view("CONTROLS")}>
                            <div className="column grid-title">Controls</div>
                        </div>
                        <ControlsList
                            show={this.state.sideView == "CONTROLS"}/>

                    </div>
                    <div className="column">
                        <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                            <div className="column"></div>
                            <ActionGroup
                                actions={PLOT_ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div className="row block block-overflow">
                            {this.state.data.g.map(item => {
                                return (
                                    <Group group={item}
                                           onRemoveNode={this._removeNode}
                                           onAddNode={this._addNode}
                                           onChange={this._onGroupChange}
                                           nodes={this.state.data.n || []}/>
                                )
                            })}

                            {calculations.map(item => {
                                return (
                                    <Group

                                        group={{_: item.g}}
                                        type="CALCULATION"
                                        data={item}
                                        onChange={this._onCalcChange}
                                        nodes={this.state.data.n || []}/>
                                )
                            })}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PlotScreen;