import React from "react";
import utils from "../../../utils/utils";

const TYPES = [
    {n: "SERIES", l: "Time Series"},
    {n: "CATEGORY", l: "Category"},
    {n: "RAW", l: "Raw"},
    {n: "MAP", l: "Map"}
]

import TYPE_DEFINITIONS from "../../../constants/const.types.jsx";

const STYLE = {
    ICON_CIRCLE: {
        borderRadius: "60px",
        background: "#333",
        textAlign: "center",
        position: "absolute",
        top: "50%",
        left: "50%",
        width: "30px",
        height: "30px",
        lineHeight: "30px",
        marginLeft: "-15px",
        marginTop: "-15px",
        color: "#F2F2F2"

    }
}

class Droppable extends React.Component {
    static defaultProps = {
        vAllowed: false,
        hAllowed: false,
        wAllowed: false,
        centroid: false
    };

    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        let target = "dp-area";

        e.dataTransfer.setData("e", JSON.stringify(this.props.data));

        if (this.props.data.t == "H") {
            window.dispatchEvent(new CustomEvent("showdrops", {detail: "H"}))
        } else if (this.props.data.t == "V") {
            window.dispatchEvent(new CustomEvent("showdrops", {detail: "V"}));
        } else {
            window.dispatchEvent(new CustomEvent("showdrops", {detail: "W"}));
        }

    };

    _onDragEnd = (e) => {
        e.preventDefault();

        window.dispatchEvent(new CustomEvent("hidedrops", {}));
    };

    render() {
        let icon;
        if (this.props.data.i) icon = <i className={"fal " + this.props.data.i}></i>;

        let style = {
            display: "flex",
            flexDirection: "row",
            textAlign: "left"
        };
        if (this.props.data.bgd) style.backround = this.props.data.bgd;

        return (
            <div className="son-list-item"
                 style={style}
                 draggable={true}
                 onDragStart={this._onDragStart}
                 onDragEnd={this._onDragEnd}>
                <div className="column" style={{maxWidth: "16px", textAlign: "center"}}>{icon}</div>
                <div className="column">{this.props.data.t}</div>
            </div>
        )
    }
}

class Droppables extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD"
    };

    constructor(props) {
        super(props)
    }

    render() {
        let items = [];
        for (let i in TYPE_DEFINITIONS) {
            let t = TYPE_DEFINITIONS[i];
            if (t.modes) {
                if (t.modes == "*" || t.modes.indexOf(this.props.mode) >= 0) {
                    items.push(t);
                }
            }
        }

        return (
            <div className="column block block-overflow" style={{paddingTop: "8px"}}>
                {items.map((item) => {
                    return <Droppable data={item}/>
                })}
            </div>
        )
    }
}

export default Droppables;
