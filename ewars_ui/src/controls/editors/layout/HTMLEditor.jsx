import React from "react";

import TextField from "../../fields/f.text.jsx";

const config = {
    ml: true
}

class HTMLEditor extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (prop, value) => {
        ewars.z.dispatch("E", "WIDGET_PROP_CHANGE", [this.props.id, "html", value])
    };

    render() {
        return (
            <ewars.d.Panel>
                <TextField
                    data={config}
                    value={this.props.data.html}
                    onChange={this._onChange}/>

            </ewars.d.Panel>
        )
    }
}

export default HTMLEditor;
