import React from 'react';

import DropArea from "./DropArea.jsx";
import HBox from "./HBox.jsx";

class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    _onRootDrop = (data) => {
        ewars.z.dispatch("E", "PAGE_ADD_ITEM", [this.props.data._, data]);
    };

    render() {
        return (
            <div className="lide-page" style={{background: "#F2F2F2", height: "1024px", margin: "16px"}}>
                <ewars.d.Layout>
                    <ewars.d.Row height="60px">
                        <ewars.d.Cell borderBottom={true}>
                            [header]
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <ewars.d.Layout style={{height: "100%"}}>
                                {this.props.data.i.map((item) => {
                                    return (
                                        <HBox
                                            key={item._}
                                            resizable={true}
                                            data={item}/>
                                    )
                                })}
                                {this.props.data.i.length <= 0 ?
                                    <div style={{height: "30px", position: "relative"}}>
                                        <ewars.d.Row height="30px">
                                            <ewars.d.Cell
                                                style={{position: "relative", display: "block", height: "30px"}}>
                                                <DropArea type="V" onDropped={this._onRootDrop}/>
                                            </ewars.d.Cell>
                                        </ewars.d.Row>
                                    </div>
                                    : null}
                            </ewars.d.Layout>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row height="60px">
                        <ewars.d.Cell borderTop={true}>
                            [footer]
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

export default Page;
