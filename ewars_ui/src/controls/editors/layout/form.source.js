const FORM_SOURCE = [
    {
        _o: 0,
        n: "var",
        l: "Variable Name",
        t: "TEXT",
        r: true
    },
    {
        _o: 1,
        n: "reduction",
        t: "BUTTONSET",
        l: "Reduction",
        o: [
            ["SUM", "Sum"],
            ["AVG", "Mean"]
        ]
    }
];


export default FORM_SOURCE;