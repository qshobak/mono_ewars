import React from "react";

class WidgetSettings extends React.Component {
    static defaultProps = {
        definition: [],
        data: {}
    }
    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        ewars.z.dispatch("E", "WIDGET_PROP_CHANGE", [this.props.id, prop, value])
    };

    render() {
        return (
            <ewars.d.Panel style={{overflow: "visible"}}>
                <ewars.d.SystemForm
                    definition={this.props.definition}
                    vertical={true}
                    data={this.props.data}
                    onChange={this._onChange}
                    enabled={true}/>

            </ewars.d.Panel>
        )
    }
}

export default WidgetSettings;
