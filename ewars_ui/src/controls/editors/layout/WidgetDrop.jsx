import React from "react";

class WidgetDrop extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDragOver = (e) => {
        e.preventDefault();
        this._el.style.transform = "scale(2)";
    };

    _onDragLeave = (e) => {
        e.preventDefault();
        this._el.style.transform = "none";
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("e"));
        if (data.c) {
            this.props.onDropped(data.c);
        } else {
            this.props.onDropped({
                c: {
                    type: data.t
                }
            })
        }
    };


    render() {
        return (
            <div className="widget-drop"
                 ref={(el) => {
                     this._rEl = el;
                 }}
                 onDrop={this._onDrop}
                 onDragOver={this._onDragOver}
                 onDragLeave={this._onDragLeave}>
                <div className="inner wd-inner" ref={(el) => {
                    this._el = el
                }}>
                    <i className="fal fa-download"></i>
                </div>
            </div>
        )
    }
}

export default WidgetDrop;
