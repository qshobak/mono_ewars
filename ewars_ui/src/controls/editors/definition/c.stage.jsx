import React from "react";

class EditorStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="editor-stage">
                    [stage description]
                    [stage fields]
                    [stage actions] - [what happens when this stage is complete]
                    [stage assignment] - [who gets assigned to this stage]
                </div>
            </div>
        )
    }
}

export default EditorStage;
