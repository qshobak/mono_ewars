import React from "react";

import ActionGroup from "../../c.actiongroup.jsx";
import RuleEditor from "../../../controls/editors/editor.rules.jsx";

const ACTIONS = [
    ['fa-plus', 'ADD', 'Add stage']
];

const RULE_ACTIONS = [
    ['fa-copy', 'DUPLICATE', 'Copy'],
    ['fa-trash', 'DELETE', 'Delete']
];

class ViewActions extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (view) => {

    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row">

                </div>
            </div>
        )
    }
}

class Actions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}


const EDITOR_ACTIONS = [
    ['fa-plus', 'ADD', 'Add item']
];

class EditorStage extends React.Component {
    state = {
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        return (
            <div className="row">
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={[]}
                            onAction={this._action}/>
                    </div>
                    <RuleEditor/>
                </div>
            </div>
        )
    }
}

class StageItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        return (
            <div className="dash-handle" onClick={this._onClick}>
                {this.props.data.name}
            </div>
        )
    }
}

const STAGES = [
    {
        name: "Verification",
        uuid: "VERIFICATION",
        description: "No description",
        task: false,
        actions: [
            {
                type: "GENERATE_RECORD",
                fid: null,
                mapping: {}
            }
        ]
    },
    {
        name: "Risk Assessment",
        uuid: "RISK_ASSESS",
        description: "No description",
        task: false,
        actions: [
            {
                type: "GENERATE_RECORD",
                fid: null,
                mapping: {}
            }
        ]
    },
    {
        name: "Risk Characterization",
        uuid: "RISK_CHAR",
        task: false,
        description: "no description",
        actions: []
    },
    {
        name: "Outcome",
        uuid: "OUTCOME",
        task: false,
        description: "no description",
        actions: []
    }
];

class ViewStages extends React.Component {
    state = {
        data: STAGES,
        item: null
    };

    constructor(props) {
        super(props);
    }

    _select = (data) => {
        this.setState({
            item: data
        })
    };

    render() {
        let view;

        if (this.state.item) {
            view = <EditorStage data={this.state.item}/>;
        }

        return (
            <div className="row">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow">
                        {this.state.data.map(item => {
                            return (
                                <StageItem data={item} onSelect={this._select}/>
                            )
                        })}
                    </div>
                </div>
                <div className="column">
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewStages;
