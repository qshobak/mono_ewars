import React from "react";

import FormUtils from "../../utils/form_utils";
import utils from "../../utils/utils";
import FieldDefinition from "./definition/c.field.jsx";
import ViewStages from "./definition/view.stages.jsx";
import ReportingForm from "../../controls/forms/form.reporting.jsx";
import RulesEditor from "../../controls/editors/editor.rules.jsx";

import ICONS from "./definition/const.icons";
import DROPPABLES from "./definition/const.droppables";

const PREVIEW_ACTIONS = [
    ['fa-eye-closed', 'PREVIEW', 'Close preview']
];

class Droppable extends React.Component {
    constructor(props) {
        super(props);
    }

    _dragStart = (e) => {
        e.dataTransfer.setData("e", JSON.stringify({
            new: true,
            type: this.props.data[0]
        }));

        setTimeout(() => {
            var evt = new CustomEvent("showdrops", {});
            window.dispatchEvent(evt);
        })
    };

    _dragEnd = (e) => {
        var evt = new CustomEvent("hidedrops", {});
        window.dispatchEvent(evt);
    };

    render() {
        if (this.props.data.length <= 1) {
            return (
                <div className="dash-section">{this.props.data[0]}</div>
            )
        }
        return (
            <div onDragStart={this._dragStart}
                 onDragEnd={this._dragEnd}
                 draggable={true}
                 className="son-list-item"
                 style={{display: "flex"}}>
                <div className="column block" style={{maxWidth: "20px"}}>
                    <i className={"fal " + ICONS[this.props.data[0]] || "fa-list"}></i>
                </div>
                <div className="column">
                    {this.props.data[1]}
                </div>
            </div>
        )
    }
}


class DefaultView extends React.Component {
    state = {
        preview: false,
        definition: {},
        dummyFormData: {
            data: {}
        },
        view: "DEFAULT"
    };

    _view = (view) => {
        this.setState({view});
    };

    constructor(props) {
        super(props);
    }

    _onFieldChange = (field_id, prop, value) => {
        this.props.onChange(FormUtils.updateField(this.props.definition, field_id, prop, value, "name"));
    };

    onAddChild = (uuid) => {
        this.props.onChange(FormUtils.addChild(this.props.definition, uuid));
    };

    _onDeleteField = (uuid) => {
        this.props.onChange(FormUtils.removeField(this.props.definition, uuid));
    };

    _onDuplicateField = (uuid) => {
        this.props.onChange(FormUtils.duplicateField(this.props.definition, uuid));
    };

    _onMoveField = (moving, target) => {
        this.props.onChange(FormUtils.moveField(this.props.definition, moving, target));
    };

    _onDropNew = (data, target) => {
        this.props.onChange(FormUtils.dropField(this.props.definition, data, target));
    };

    _onMoveFieldUp = (field) => {
        this.props.onChange(FormUtils.moveFieldUp(this.props.definition, field));
    };

    _onMoveFieldDown = (field) => {
        this.props.onChange(FormUtils.moveFieldDown(this.props.definition, field));
    };

    _dummyFormChange = (prop, value) => {
        this.setState({
            dummyFormData: {
                data: {
                    ...this.state.dummyFormData,
                    [prop]: value
                }
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "PREVIEW":
                this.setState({
                    preview: !this.state.preview
                })
                break;
            default:
                break;
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = e.dataTransfer.getData("e");
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New Field",
                required: false,
                uuid: utils.uuid()
            }
            this.props.onChange(FormUtils.dropField(this.props.definition || {}, newField));
        }
    };

    _onStageChange = (data) => {
        console.log(data);
    };

    render() {
        let formFields = FormUtils.asOptions(this.props.definition);

        let fields = [],
            fieldSet = [];

        for (let i in this.props.definition) {
            fieldSet.push({
                ...this.props.definition[i],
                name: i
            })
        }
        fieldSet = fieldSet.sort(FormUtils.ORDER_SORT);

        let _dragOver, _onDrop;
        if (fieldSet.length > 0) {
            fields = fieldSet.map((field, index) => {
                return (
                    <FieldDefinition
                        definition={this.props.definition}
                        moveFieldUp={this._onMoveFieldUp}
                        moveFieldDown={this._onMoveFieldDown}
                        onDuplicateField={this._onDuplicateField}
                        onDeleteField={this._onDeleteField}
                        onAddChild={this._onAddChild}
                        onMoveField={this._onMoveField}
                        onDropNew={this._onDropNew}
                        onChange={this._onFieldChange}
                        fieldOptions={formFields}
                        data={field}
                        order={index + 1}
                        isRoot={true}
                        key={field.uuid}/>
                )
            })
        } else {
            _dragOver = this._onDragOver;
            _onDrop = this._onDrop;
            fields = (
                <div style={{textAlign: "center", marginTop: "100px", display: "block", color: "#F2F2F2"}}>
                    <i style={{fontSize: "90px"}} className="fal fa-caret-circle-down"></i>
                    <div style={{fontSize: "14px", padding: "20px 0"}}>Drag a field here to start</div>
                </div>
            )
        }

        return (
            <div className="row">
                <div className="column panel block block-overflow br">
                    {DROPPABLES.map(item => {
                        return <Droppable data={item}/>;
                    })}
                </div>
                <div className="column">
                    <div
                        onDragOver={_dragOver}
                        onDrop={_onDrop}
                        style={{paddingRight: "8px", paddingBottom: "16px"}}
                        className="column block block-overflow">
                        {fields}
                    </div>
                </div>
            </div>
        )
    }
}


class DefinitionEditor extends React.Component {
    state = {
        view: "DEFAULT"
    }

    _view = (view) => {
        this.setState({view});
    };

    _onDefinitionChange = (data) => {
        if (this.props.version === 1) {
            this.props.onChange(data);
        } else {
            let definition = utils.copy(this.props.definition);
            definition.definition = data;
            this.props.onChange(definition);
        }
    };

    _stagesChange = (data) => {
        let definition = utils.copy(this.props.definition);
        definition.stages = data;
        this.props.onChange(definition);
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") {
            let definition = this.props.definition.definition || {};
            view = (
                <DefaultView
                    definition={definition}
                    onChange={this._onDefinitionChange}/>
            )
        }
        if (this.state.view == "STAGES") {
            let stages = this.props.definition.stages;
            view = (
                <ViewStages onChange={this._stagesChange}
                            data={stages}/>
            )
        }
        if (this.state.view == "PREVIEW") view = (
            <ReportingForm
                definition={this.props.definition}
                allRoot={true}
                data={{data: {}}}/>
        );

        return (
            <div className="row definition-editor">
                <div className="column br" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-list"></i>&nbsp;Fields
                        </div>
                        <div
                            onClick={() => this._view("STAGES")}
                            className={"ide-tab" + (this.state.view == "STAGES" ? " ide-tab-down" : "")}>
                            <i className="fal fa-tag"></i>&nbsp;Stages
                        </div>
                        <div
                            onClick={() => this._view("PREVIEW")}
                            className={"ide-tab" + (this.state.view == "PREVIEW" ? " ide-tab-down" : "")}>
                            <i className="fal fa-eye"></i>&nbsp;Preview
                        </div>
                    </div>
                </div>
                {view}
            </div>
        )
    }
}

export default DefinitionEditor;
