export default {
    name: 'Completeness',
    icon: 'fa-hashtag',
    type: 'PERCENTAGE',
    aggs: []
}