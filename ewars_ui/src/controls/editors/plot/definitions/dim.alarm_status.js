export default {
    name: 'Alarm status',
    icon: 'fa-exclamation-triangle',
    f: ['alarm_status'],
    m: ['ALERTS', 'ALARMS']
}