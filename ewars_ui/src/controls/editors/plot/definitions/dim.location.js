export default {
    name: 'Location',
    i: 'fa-map-marker',
    f: ['LOCATION', 'LOCATION_STATUS', 'LOCATION_TYPE', 'LOCATION_GROUP'],
    m: ['FIELD', 'RECORDS', 'ALERTS'],
    t: 'location'
}