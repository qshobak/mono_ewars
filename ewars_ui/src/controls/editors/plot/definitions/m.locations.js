export default {
    name: 'Number of Locations',
    icon: 'fa-hashtag',
    description: "Return number of locations",
    t: 'NUMERIC',
    aggs: [
        'COUNT,Count'
    ]
}