export default {
    name: 'Alert Raised Date',
    icon: 'fa-calendar',
    f: ['ALERT_RAISED_DATE'],
    m: ['ALERTS'],
    t: 'DATE'
}