export default {
    name: 'Form Status',
    i: 'fa-form',
    f: ['FORM_STATUS'],
    m: ['FIELD', 'RECORDS'],
    t: 'select',
    o: [
        'ACTIVE,Active',
        'DRAFT,Draft',
        'INACTIVE,Inactive'
    ]
}