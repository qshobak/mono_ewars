export default {
    name: 'User Role',
    icon: 'fa-user',
    f: ['USER_TYPE'],
    m: ['RECORDS'],
    t: 'SELECT',
    o: [
        'ACCOUNT_ADMIN,Account Administrator',
        'REGIONAL_ADMIN,Geographic Administrator',
        'USER,User'
    ]
}