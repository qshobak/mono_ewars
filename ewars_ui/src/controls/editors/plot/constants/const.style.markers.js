const MARKERS = [
    ['square', 'Square'],
    ['circle', 'Circle'],
    ['diamond', 'Diamond'],
    ['triangle', 'Triangle'],
    ['triangle-down', 'Triangle (down)']
];

export default MARKERS;
