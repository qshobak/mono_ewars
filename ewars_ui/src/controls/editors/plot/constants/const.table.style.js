export default {
    t_header: {
        t: 'H',
        l: 'Header formatting'
    },
    showHeaders: {
        t: 'bool',
        l: 'Show header row'
    },
    headerBgd: {
        t: 'color',
        l: 'Bgd. color'
    },
    headerFontSize: {
        t: 'number',
        l: 'Font size'
    },
    headerFontWeight: {
        t: 'select',
        l: 'Font weight',
        o: [
            ['normal', 'Normal'],
            ['bold', 'Bold']
        ]
    },
    headerFontColor: {
        t: 'color',
        l: 'Color'
    },
    headerCellPadding: {
        t: 'number',
        l: 'Padding'
    },
    headerTextAlign: {
        t: 'select',
        l: 'Text-Align',
        o: [
            ['left', 'Left'],
            ['center', 'Center'],
            ['right', 'Right']
        ]
    },
    t_cell: {
        t: 'H',
        l: 'Cell formatting'
    },
    cellFontSize: {
        t: 'number',
        l: 'Font size'
    },
    cellFontColor: {
        t: 'color',
        l: 'Text color'
    },
    cellFontWeight: {
        t: 'select',
        l: 'Cell font weight',
        o: [
            ['normal', 'Normal'],
            ['bold', 'Bold']
        ]
    },
    cellPadding: {
        t: 'number',
        l: 'Padding'
    },
    borderColor: {
        t: 'color',
        l: 'Border color'
    },
    valueFormat: {
        t: 'select',
        l: 'Formatting',
        o: [
            ['0', '0'],
            ['0.0', '0.0'],
            ['0.00', '0.00'],
            ['0,0.0', '0,0.0'],
            ['0,0.00', '0,0.00'],
            ['0,0', '0,0'],
            ['0%', '0%'],
            ['0.0%', '0.0%'],
            ['0.00%', '0.00%']
        ]
    },
    t_rows: {
        t: 'H',
        l: 'Row formatting'
    },
    rowBgd: {
        t: 'color',
        l: 'Row Color'
    },
    rowAlternateColor: {
        t: 'color',
        l: 'Row alternate color'
    },
    t_reload: {
        t: 'H',
        l: 'Reload'
    },
    reload: {
        t: 'bool',
        l: 'Enabled'
    },
    reloadInterval: {
        t: 'number',
        l: 'Interval (seconds)'
    }

};