import React from "react";
import {instance} from '../plot_instance';

class NavigatorEditor extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (e) => {
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        return (
            <div className="control-editor">
                <div className="control-field">
                    <div className="row">
                        <div className="column" style={{maxWidth: "30%"}}>Height</div>
                        <div className="column">
                            <input
                                type="number"
                                name="height"
                                onChange={this._onChange}
                                value={node.height || '0'}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavigatorEditor;
