import React from "react";
import ControlField from '../components/ui.control_field.jsx';
import BoolButton from '../components/ui.boolbutton.jsx';

import {instance} from '../plot_instance';

const DEFAULTS = {
    type: 'LEGEND',
    _: null,
    c: {
        target: '', // group to target with the legend
        style: '', // style of the legend,
        pos: 'TL', // position of the legend
    }
};

const POSITIONS = [
    ['TL', 'Top-left'],
    ['TC', 'Top-center'],
    ['TR', 'Top-right'],
    ['ML', 'Mid-left'],
    ['MR', 'Mid-right'],
    ['BL', 'Bottom-left'],
    ['BC', 'Bottom-center'],
    ['BR', 'Bottom-right']
]

class LegendEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nid);

        let symbolHeight = 12;
        if (node.node.fontSize) symbolHeight = node.node.fontSize;
        if (node.node.symbolHeight) symbolHeight = node.node.symbolHeight;

        return (
            <div className="control-editor">
                <ControlField label="Position">
                    <select
                        onChange={this._onChange}
                        name="position"
                        value={node.node.position || 'ML'}>
                        {POSITIONS.map(item => {
                            return <option value={item[0]}>{item[1]}</option>
                        })}
                    </select>
                </ControlField>
                <ControlField label="Orientation">
                    <select
                        onChange={this._onChange}
                        value={node.node.layout || null}
                        name="layout">
                        <option value="horizontal">Horizontal</option>
                        <option value="vertical">Vertical</option>
                    </select>
                </ControlField>
                <ControlField label="Border">
                    <BoolButton
                        value={node.node.bBorder}
                        onChange={this._onChange}
                        name="bBorder"/>
                </ControlField>
                <ControlField label="Border width">
                    <input
                        name="borderWidth"
                        onChange={this._onChange}
                        value={node.node.borderWidth || 1}
                        type="number"/>
                </ControlField>
                <ControlField label="Border color">
                    <input
                        name="borderColor"
                        onChange={this._onChange}
                        value={node.node.borderColor || '#333333'}
                        type="color"/>
                </ControlField>
                <ControlField label="Title">
                    <input
                        onChange={this._onChange}
                        value={node.node.title || ''}
                        name="title"
                        placeholder="Legend title"
                        type="text"/>
                </ControlField>
                <ControlField label="Symbol height">
                    <input
                        onChange={this._onChange}
                        value={symbolHeight}
                        name="symbolHeight"
                        type="number"/>
                </ControlField>
                <ControlField label="Bgd Colour">
                    <input
                        onChange={this._onChange}
                        name="bgdColor"
                        value={node.node.bgdColor || 'transparent'}
                        type="color"/>
                </ControlField>
                <ControlField label="Font size (px)">
                    <input
                        onChange={this._onChange}
                        name="fontSize"
                        value={node.node.fontSize || 12}
                        type="number"/>
                </ControlField>
                <ControlField label="Color">
                    <input
                        onChange={this._onChange}
                        value={node.node.color || '#333333'}
                        name="color"
                        type="color"/>
                </ControlField>
                <ControlField label="Weight">
                    <select
                        name="weight"
                        value={node.node.weight || 'normal'}
                        onChange={this._onChange}>
                        <option value="normal">Normal</option>
                        <option value="bold">Bold</option>
                    </select>
                </ControlField>
            </div>
        )
    }
}

export default LegendEditor;
