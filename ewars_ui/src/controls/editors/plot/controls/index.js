import AnnotationsEditor from './editor.annotation.jsx';
import AxisEditor from './editor.axis.jsx';
import BandEditor from './editor.band.jsx';
import LineEditor from './editor.line.jsx';
import LegendEditor from './editor.legend.jsx';
import NavigatorEditor from './editor.navigator.jsx';
import PolygonEditor from './editor.polygon.jsx';
import TextEditor from './editor.text.jsx';
import TrendEditor from './editor.trend.jsx';
import ScaleEditor from './editor.scale.jsx';
import MapLayerEditor from './editor.map.layer.jsx';
import MapPointsEditor from './editor.map.points.jsx';
import MapPolygonEditor from './editor.map.polygon.jsx';
import RangeEditor from './editor.val.map.jsx';
import StackEditor from './editor.stack.jsx';

export {
    AnnotationsEditor,
    AxisEditor,
    BandEditor,
    LineEditor,
    LegendEditor,
    NavigatorEditor,
    PolygonEditor,
    TextEditor,
    TrendEditor,
    ScaleEditor,
    MapPointsEditor,
    MapLayerEditor,
    MapPolygonEditor,
    RangeEditor,
    StackEditor
}
