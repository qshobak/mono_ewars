import React from "react";

import ActionGroup from "../../../c.actiongroup.jsx";
/**
 *
 * Annotations show points on a chart with some additional information added to them
 * Annotations can also be alternately shown from a source like alerts, teams, etc...
 *
 */

const DEFAULT = {
    type: 'ANNOTATIONS',
    _: null,
    c: {
        icon: null, // Which icon to use drop|circle|bell|square
        plot: 'v|h', // Vertical versus horizontal
        colour: null, // background colour of the nodes
        lineColour: null, // The colour of the line drawn from the axis to the node
        indexing: 'numeric/alphabet', // How each node on the chart has its id set (i.e. a number or a alpha character)
        axis: null, // Which axis to add items along
        items: [
            {
                pos: null, // the location on the target axis to set at
                content: null
            }
        ]
    }
}

const ACTIONS = [
    ['fa-plus', 'ADD']
];

const FIELDS = [
    ['source', 'select', 'Annotations Source']
];

class AnnotationsEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {

    };

    render() {
        return (
            <div className="control-editor">
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <ActionGroup
                            actions={ACTIONS}
                            right={true}
                            height="28px"
                            onAction={this._action}/>
                    </div>
                    <div className="row" style={{maxHeight: "200px"}}>

                    </div>
                </div>
            </div>
        )
    }
}

export default AnnotationsEditor;
