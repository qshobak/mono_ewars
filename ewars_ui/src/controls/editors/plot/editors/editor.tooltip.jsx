import React from "react";

class TooltipEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Tooltip">
                    <div className="bnt-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"/>
                        <ewars.d.Button
                            icon="fa-close"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default TooltipEditor;
