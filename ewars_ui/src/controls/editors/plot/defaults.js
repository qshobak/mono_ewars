import {generateId} from './utils';
import utils from "../../../utils/utils";


const DEFAULT_DEFAULTS = {
    _: null,
    g: [
        {
            _: null,
            label: 'New Group'
        }
    ],
    n: [
        {_: null, x: true, t: 'A', label: 'Period', _t: 'date'},
        {_: null, x: false, t: 'A', label: 'Value', _t: 'numeric'}
    ],
    title: '',
    style: {}
};

const DEFAULT = {
    _: null,
    g: [
        {
            _: null,
            label: 'New Group'
        }
    ],
    n: [],
    title: '',
    style: {}
};


const getDefaults = (type) => {
    let data;
    if (['DEFAULT'].indexOf(type) >= 0) {
        data = utils.copy(DEFAULT_DEFAULTS);
        data._ = generateId();
        data.g[0]._ = generateId();
        data.n.forEach(item => {
            item._ = generateId();
            item.g = data.g[0];
        })
        data.type = type;
    } else {
        data = utils.copy(DEFAULT);
        data._ = generateId();
        data.g[0]._ = generateId();
        data.type = type;
    }

    return data;
}

export {
    getDefaults
};