import React from "react";

import ContextMenu from "./ContextMenu.jsx";

import {PRESET_NAMES, PRESET} from './constants/';
import * as CTX_MENUS from './menus';

import ModalDialog from './editors/editor.filters.jsx';

import {instance} from './plot_instance';
import FilterEditor from './editors/editor.filter.jsx';
import LocationFilter from './editors/editor.filter.location.jsx';
import DateFilter from './editors/editor.filter.date.jsx';

const STYLE = {
    WRAPPER: {
        display: "block",
        background: "steelblue",
        borderRadius: '3px',
        color: "#F2F2F2",
        height: "inherit",
        paddingLeft: "8px",
        paddingRight: "8px",
        paddingTop: "8px",
        paddingBottom: "8px",
        cursor: "pointer",
        position: "relative"
    },
    WRAPPER_FILTER: {
        position: "relative",
        display: "block",
        background: "steelblue",
        borderRadius: "16px",
        color: "#F2F2F2",
        height: "25px",
        padding: "8px",
        cursor: "pointer",
        margin: "5px 8px"
    }

}

const DEFAULT_MENU = [
    ["FILTER", "Filter...", "fa-filter", null],
    "-",
    ["REMOVE", "Remove", "fa-trash", null]
];

const CTX_MAP = {
    LOCATION: CTX_MENUS.CTX_DIM_LOCATION,
    DATE: CTX_MENUS.CTX_DIM_RECORD_DATE,
    DEFAULT: DEFAULT_MENU,
    M: DEFAULT_MENU
};

class Dimension extends React.Component {
    static defaultProps = {
        filter: false,
        gid: null,
        nid: null,
        filters: [],
        data: []
    };

    constructor(props) {
        super(props)

        this.state = {
            show: false,
            editor: false
        }
    }

    _toggle = () => {
        if (this.state.editor) return;
        this.setState({
            show: !this.state.show
        })
    };

    _action = (action) => {
        this.setState({
            show: false
        })

        if (action == 'REMOVE') instance.removeNode(this.props.nid);

        if (action == "FILTER") {
            this.setState({
                editor: true
            })
        }

        if (action == "CLOSE") this.setState({editor: false});
    };

    _saveFilter = (data) => {
        this.setState({editor: false})
        instance.updateFilter(this.props.nid, data);
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let label = node.getShortLabel();

        let ctx_menu = CTX_MAP.DEFAULT;
        if (CTX_MAP[node.n]) {
            ctx_menu = CTX_MAP[node.n];
        }

        let style = STYLE.WRAPPER;
        if (node.isFilter()) style = STYLE.WRAPPER_FILTER;

        let filterView;
        if (this.state.editor) {
            if (node.isDate()) {
                filterView = <DateFilter
                    onSave={this._saveFilter}
                    onClose={() => {
                        this.setState({editor: false})
                    }}
                    nid={node._}/>
            } else if (node.isLocation()) {
                filterView = <LocationFilter
                    onSave={this._saveFilter}
                    onClose={() => {
                        this.setState({editor: false});
                    }}
                    nid={node._}/>;
            } else {
                filterView = <FilterEditor
                    onSave={this._saveFilter}
                    onClose={() => {
                        this.setState({editor: false});
                    }}
                    nid={node._}/>;
            }
        }

        if (node.isFilter()) {
            return (
                <div style={style} onClick={this._toggle}>
                    <div className="row">
                        <div className="column" style={{maxWidth: "20px"}}>
                            <i className="fal fa-filter"></i>
                        </div>
                        <div className="column">
                            {label}
                        </div>
                        <div className="column block" style={{maxWidth: "20px", textAlign: "center"}}>
                            <i className="fal fa-caret-down"></i>
                        </div>
                    </div>
                    {this.state.show ?
                        <ContextMenu
                            actions={ctx_menu}
                            onClick={this._action}/>
                        : null}
                    {filterView}
                </div>
            )
        }

        return (
            <div style={style} onClick={this._toggle}>
                {label}&nbsp;&nbsp;<i className="fal fa-caret-down"></i>
                {this.state.show ?
                    <ContextMenu
                        actions={ctx_menu}
                        onClick={this._action}/>
                    : null}

            </div>
        )
    }
}

export default Dimension;
