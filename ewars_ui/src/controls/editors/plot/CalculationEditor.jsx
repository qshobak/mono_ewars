import React from "react";

import MeasureTree from "../../c.tree.measures.jsx";
import DimensionTree from "../../c.tree.dimensions.jsx";

import PRESET_NAMES from "./const.names";
import PRESETS from "./const.presets";

const STYLES = {
    DROPZONE: {
        display: "none",
        border: "1px dashed white",
        margin: "8px",
        textAlign: "center",
        paddingTop: "20px",
        paddingBottom: "20px"
    },
    VAR_WRAPPER: {
        display: "block",
        margin: "8px"
    },
    VAR_HEADER: {
        background: "rgba(0,0,0,0.1)",
        border: "1px solid rgba(0,0,0,0.2)"
    },
    VAR_CONTROLS: {
        display: "block"
    },
    VAR_LABEL: {
        padding: "8px"
    },
    SETTINGS: {
        borderLeft: "1px solid rgba(0,0,0,0.2)",
        borderBottom: "1px solid rgba(0,0,0,0.2)",
        borderRight: "1px solid rgba(0,0,0,0.2)",
        minHeight: "120px"
    }
};

const VAR_ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-copy", "DUPE"],
    ["fa-trash", "DELETE"]
]

class Variable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    _onAction = (action) => {
        if (action == "EDIT") {
            this.setState({
                shown: !this.state.shown
            })
        }

        if (action == "DELETE") {

        }

        if (action == "DUPE") {

        }
    }

    render() {

        return (
            <div style={STYLES.VAR_WRAPPER}>
                <ewars.d.Layout>
                    <ewars.d.Row style={STYLES.VAR_HEADER}>
                        <ewars.d.Cell style={STYLES.VAR_LABEL}>Label</ewars.d.Cell>
                        <ewars.d.Cell style={STYLES.VAR_CONTROLS}>
                            <ewars.d.ActionGroup
                                height="28px"
                                actions={VAR_ACTIONS}
                                right={true}
                                onAction={this._onAction}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row style={STYLES.SETTINGS}>
                        <ewars.d.Cell borderRight={true}>

                        </ewars.d.Cell>
                        <ewars.d.Cell>

                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

class DropZone extends React.Component {
    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentWillUnmount() {
        ewars.unsubscribe("SHOW_DROPS", this._showDrops);
        ewars.unsubscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = (type) => {
        if (type == "M") {
            if (this._el) this._el.style.display = "block";
        }
    };

    _hideDrops = (e) => {
        if (this._el) this._el.style.display = "none"
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        ewars.emit("HIDE_DROPS");

        this.props.onDrop(JSON.parse(e.dataTransfer.getData("n")));
    };

    render() {
        return (
            <div onDrop={this._onDrop} onDragOver={this._onDragOver}>
                <div style={STYLES.DROPZONE} ref={el => this._el = el}>
                    <i className="fal fa-download"></i>
                </div>
            </div>
        )
    }
}

class VariableList extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ewars.d.Panel>
                <DropZone onDrop={this.props.onDrop}/>
                {this.props.data.map((item) => {
                    return <Variable
                        data={item}/>
                })}
            </ewars.d.Panel>
        )
    }
}

class CalculationEditor extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                variables: [],
                formula: ""
            }
        }
    }

    _onFormulaChange = (e) => {
        this.setState({
            data: {
                formula: e.target.value
            }
        })
    };

    _onDrop = (data) => {
        let vars = this.state.data.variables;
        vars.push(data);
        this.setState({
            data: {variables: vars}
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Calculation Editor">

                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"/>
                        <ewars.d.Button
                            onClick={this.props.onClose}
                            icon="fa-close"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row height="80px">
                    <ewars.d.Cell style={{padding: "8px"}}>
                        <input type="text" style={{minHeight: "auto", fontSize: "14px"}} value={this.state.data.formula}
                               onChange={this._onFormulaChange}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="250px" borderTop={true}>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar label="Dimensions"></ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    <DimensionTree/>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                            <ewars.d.Toolbar label="Measures"></ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    <MeasureTree/>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true}>
                        <VariableList
                            onDrop={this._onDrop}
                            data={this.state.data.variables}/>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>

        )
    }
}

export default CalculationEditor;
