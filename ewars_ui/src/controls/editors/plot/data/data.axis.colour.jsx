import React from "react";
import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';

import ActionGroup from "../../../c.actiongroup.jsx";

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <div className="row">
                    <div className="column" style={{maxWidth: "30%"}}>
                        {this.props.label}
                    </div>
                    <div className="column">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

class ValueMapping extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let node = instance.getNode(this.props.nid);

        let nth = 0;
        let items = [];

        if (this.props.displayProp) {
            items = this.props.data.map(item => {
                nth++;
                let style = {};
                if (nth % 2) style.background = 'rgba(0,0,0,0.1)';

                return (
                    <div className="thresh-row" style={style}>
                        <div className="row">
                            <div className="column" style={{padding: "5px", lineHeight: "24px"}}>
                                {item[this.props.displayProp]}
                            </div>
                            <div className="column" style={{maxWidth: "60px", padding: "5px"}}>
                                <input type="color"/>
                            </div>
                        </div>
                    </div>
                )
            });
        } else {
            items = this.props.data.map(item => {
                nth++;
                let style = {};
                if (nth % 2) style.background = 'rgba(0,0,0,0.1)';

                return (
                    <div className="thresh-row" style={style}>
                        <div className="row">
                            <div className="column" style={{padding: "5px", lineHeight: "24px"}}>
                                {item[1]}
                            </div>
                            <div className="column" style={{maxHeight: "60px", padding: "5px"}}>
                                <input type="color"/>
                            </div>
                        </div>
                    </div>
                )
            })
        }

        return (
            <div className="list-editing">
                {items}
            </div>
        )
    }
}


class NumRangeMapping extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="placeholder"></div>
        )
    }
}


const DATA_GREPS = {
    LOCATION_TYPE: {
        val: 'id',
        disp: 'name',
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.query', ['location_type', null, {}, null, null, null, null])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        }
    },
    LOCATION_GROUP: {
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.location.groups', [])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        },
        val: null,
        disp: null
    },
    ALERT_STATE: [
        ['OPEN', 'Open'],
        ['CLOSED', 'Closed']
    ],
    ALERT_STAGE: [
        ['VERIFICATION', 'Verification'],
        ['RISK_ASSESS', 'Risk Assessment'],
        ['RISK_CHAR', 'Risk Characterization'],
        ['OUTCOME', 'Outcome']
    ],
    ALERT_STAGE_STATE: [
        ['PENDING', 'Pending'],
        ['COMPLETED', 'Completed']
    ],
    ALERT_OUTCOME: [
        ['DISCARDED', 'Discarded'],
        ['RESPONSE', 'Response'],
        ['MONITOR', 'Monitor']
    ],
    LOCATION_STATUS: [
        ['ACTIVE', 'Active'],
        ['DISABLED', 'Disabled']
    ],
    ALERT_RISK: [
        ['LOW', 'Low'],
        ['MODERATE', 'Moderate'],
        ['HIGH', 'High'],
        ['SEVERE', 'Severe']
    ],
    SOURCE: [
        ['ANDROID', 'Android'],
        ['DESKTOP', 'Desktop'],
        ['SYSTEM', 'System'],
        ['IMPORT', 'Import']
    ],
    USER_TYPE: [
        ['USER', 'User'],
        ['ACCOUNT_ADMIN', 'Account Administrator'],
        ['REGIONAL_ADMIN', 'Regional Administrator']
    ]


};

class ColourAxis extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }

        //TODO: Figre out what kind of editing interface to display

        let node = instance.getNode(props.nodeId);

        if (node.n.indexOf('FIELD.') >= 0) {
            // this is a field, what kind of field is it
        } else {
            if (DATA_GREPS[node.n]) {
                this.state.grep = DATA_GREPS[node.n];
                if (DATA_GREPS[node.n].fetch == undefined) {
                    this.state.data = DATA_GREPS[node.n];
                } else {
                    DATA_GREPS[node.n].fetch()
                        .then(resp => {
                            this.setState({
                                data: resp
                            })
                        })
                }

            }
        }
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nodeId, e.target.name, e.target.value);
    };

    render() {
        let axis = instance.getNode(this.props.nodeId);

        let view;

        let dispVal, idVal;
        if (this.state.grep.disp != undefined) {
            dispVal = this.state.grep.disp;
            idVal = this.state.grep.val;
        }

        if (!this.state.data) {
            view = <div className="placeholder">Loading...</div>;
        } else {
            view = <ValueMapping
                valProp={idVal}
                displayProp={dispVal}
                nid={axis._}
                data={this.state.data}/>
        }


        return (
            <div className="column" style={{maxWidth: "33.3%", minWidth: "33.3%"}}>
                <div className="data-group">
                    <div className="row data-group-header" style={{background: '#333333'}}>
                        <div className="column grid-title">Colour Axis</div>
                        <ActionGroup
                            actions={CONTROL_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="data-group-body">
                        <div className="control-editor">
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ColourAxis;
