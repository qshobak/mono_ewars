import React from 'react';
import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';
import ActionGroup from "../../../c.actiongroup.jsx";

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <div className="row">
                    <div className="column" style={{maxWidth: "30%"}}>
                        {this.props.label}
                    </div>
                    <div className="column">{this.props.children}</div>
                </div>
            </div>
        )
    }
}

class SizeAxis extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nodeId, e.target.name, e.target.value);
    };

    render() {
        let axis = instance.getNode(this.props.nodeId);
        return (
            <div className="column" style={{maxWidth: "33.3%", minWidth: '33.3%'}}>
                <div className="data-group">
                    <div className="row data-group-header" style={{background: '#333333'}}>
                        <div className="column grid-title">Size Axis</div>
                        <ActionGroup
                            actions={CONTROL_ACTIONS}
                            right={true}
                            height="30px"
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="data-group-body">
                        <div className="control-editor">
                            <div className="control-editor">
                                <ControlField label="Label">
                                    <input type="text" value={axis.node.label} onChange={this._onChange}/>
                                </ControlField>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SizeAxis;
