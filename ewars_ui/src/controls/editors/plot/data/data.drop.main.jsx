import React from "react";
import {instance} from '../plot_instance';

class DataMainDrop extends React.Component {
    static defaultProps = {
        hasItems: false
    };

    constructor(props) {
        super(props);


        window.addEventListener("hidedrops", this._hide);
        window.addEventListener('showdrops', this._show);
    }

    componentWillUnmount() {
        window.removeEventListener("hidedrops", this._hide);
        window.removeEventListener("showdrops", this._show);
    }

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = e.dataTransfer.getData("n");

        instance.addControl(data);

        window.dispatchEvent(new CustomEvent("hidedrops"));
    };

    _hide = () => {
        if (this._el) this._el.style.display = 'none';
    };

    _show = (type) => {
        if (type == 'C') {
            if (this._el) this._el.style.display = 'block';
        }
    };

    render() {
        return (
            <div
                onDrop={this._onDrop}
                onDragOver={this._onDragOver}
                className="data-control-drop"
                ref={(el) => {
                    this._el = el
                }}>

            </div>
        )
    }
}

export default DataMainDrop;
