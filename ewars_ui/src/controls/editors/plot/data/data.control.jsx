import React from "react";
import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';
import ActionGroup from "../../../c.actiongroup.jsx";

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        let label = CONTROLS[node._t].n || "Unknown control type";

        let Cmp = CONTROLS[node._t].c || null;
        let view;
        if (Cmp) {
            if (['COL_MAP', 'VAL_MAP'].indexOf(node._t) >= 0) {
                if (node._t == 'COL_MAP') view = <Cmp nid={node._} mode='COLOUR'/>;
                if (node._t == 'VAL_MAP') view = <Cmp nid={node._} mode="VALUE"/>;
            } else {
                view = <Cmp nid={node._}/>
            }
        }


        return (
            <div className="column" style={{maxWidth: "33.3%", minWidth: "33.3%", minHeight: "250px"}}>

                <div className="data-group" style={{height: '100%'}}>
                    <div className="row data-group-header" style={{background: '#5f631d'}}>
                        <div className="column grid-title">{label}</div>
                        <ActionGroup
                            actions={CONTROL_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="control-group-body">
                        {view}
                    </div>
                </div>
            </div>
        )
    }
}

export default ControlGroup;
