import React from 'react';
import {instance} from '../plot_instance';

const STYLES = {
    CARD: {
        background: "#333333",
        margin: "8px"
    },
    CARD_HEADER: {
        padding: "5px",
        height: "30px"
    },
    CARD_BODY: {
        minHeight: "20px"
    }
}

class Card extends React.Component {
    static defaultProps = {
        isCalcVariable: false, // Whether this node is part of a calculation
        nid: null, // The node if
        gid: null, // group id that this node belongs to
        cid: null, // Calculation id
        style: {}, // Additional styles to apply to the node, mostly backgorund colour
        data: [] // The node itself
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        }
    }

    _toggle = () => {
        this.setState({
            visible: !this.state.visible
        })
    };

    _trash = () => {
        instance.removeNode(this.props.nid);
    };

    render() {
        let icon = 'fal fa-trash';
        let node = instance.getNode(this.props.nid);

        let label = this.props.title || "";
        if (label.length > 21) label = label.substr(0, 20) + "...";

        let newStyle = {
            ...STYLES.CARD,
            ...this.props.style
        };

        let titleStyle = {};
        if (node.node.color) titleStyle.color = `rgb(${node.node.color})`;


        return (
            <div style={newStyle}>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{minHeight: "30px"}}>
                        <div
                            style={{padding: "10px", whiteSpace: "nowrap"}}
                            rel={this.props.title}
                            title={this.props.title}
                            className="column">
                            <span title={this.props.title}>
                                <i style={titleStyle} className="fal fa-circle"></i>&nbsp;{label}
                            </span>
                        </div>
                        <div
                            onClick={this._trash}
                            style={{maxWidth: "30px", display: "block", padding: "10px"}}
                            className="column">
                            <i className={icon}></i>
                        </div>
                    </div>
                    <div className="row">
                        <div className="column" style={{padding: "5px"}}>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Card;
