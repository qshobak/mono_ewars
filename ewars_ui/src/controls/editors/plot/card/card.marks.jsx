import React from "react";

class CardMarker extends React.Component {
    constructor(props) {
        super(props);

        window.addEventListener("click", this.handleBodyClick);
    }

    componentWillUnmount() {
        window.addEventListener("click", this.handleBodyClick);
    };

    _onToggle = () => {
        if (this._el) {
            if (this._shown) {
                this._shown = false;
                this._el.style.display = "none";
            } else {
                this._shown = true;
                this._el.style.display = "block";
            }
        }
    };

    handleBodyClick = (evt) => {
        if (this._el) {
            const area = this._el;

            if (!area.contains(evt.target)) {
                this._shown = false;
                this._el.style.display = "none";
            }
        }
    };

    _set = (style) => {
        this._shown = false;
        if (this._el) this._el.style.display = "none";
    };

    render() {
        return (
            <div
                ref={el => this._ael = el}
                className="plot-settings">
                <div className="inner" onClick={this._onToggle}>
                    <div className="icon"><i className="fal fa-dot-circle"></i></div>
                    <div className="text">Markers</div>
                </div>
                <div
                    ref={el => this._el = el}
                    className="axis-drop-options" style={{right: "0", left: "auto"}}>
                    <div className="axis-drop-inner" style={{width: "150px", padding: "8px"}}>

                    </div>
                </div>
            </div>
        )
    }

}

export default CardMarker;
