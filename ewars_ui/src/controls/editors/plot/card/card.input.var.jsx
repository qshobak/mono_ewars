import React from "react";
import {instance} from '../plot_instance';

class VarNameTextField extends React.Component {
    static defaultProps = {
        value: "",
        cid: null,
        nid: null,
        gid: null
    };

    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        let value = e.target.value;
        value = value.replace(' ', '_');

        instance.updateNodeProp(this.props.nid, this.props.name, value);
    };

    render() {
        return (
            <div className="style-selector">
                <div className="style-handle" onClick={this._onToggle}>
                    <input
                        name={this.props.name}
                        placeholder="Var name..."
                        type="text"
                        onChange={this._onChange}
                        value={this.props.value}/>
                </div>
            </div>
        )
    }
}

export default VarNameTextField;
