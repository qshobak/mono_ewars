import React from "react";
import STYLE_BASE from './constants/const.general.style';

import {instance} from './plot_instance';

const STYLE = {
    t_container_style: {
        l: "Container Styling",
        t: "H"
    },
    border: {
        l: "Border",
        reg: "{size} {style} {color}",
        // This property can break out into other properties
        c: {
            size: ["SIZE", "0px", "Stroke"],
            style: ["OPTION", "solid", "Style", "solid|dotted|dashed"],
            color: ["COLOR", "#000000", "Color"]
        },
        __: [
            "borderTop",
            "borderRight",
            "borderBottom",
            "borderLeft"
        ]
    },
    margin: {
        l: "Margin",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "marginTop",
            "marginRight",
            "marginBottom",
            "marginLeft"
        ]
    },
    padding: {
        l: "Padding",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "paddingTop",
            "paddingRight",
            "paddingBottom",
            "paddingLeft"
        ]
    },
    boxShadow: {
        l: "Box Shadow",
        reg: "{xOffset} {yOffset} {blur} {color}",
        c: {
            xOffset: ["SIZE", "0px", "X"],
            yOffset: ["SIZE", "0px", "y"],
            blur: ["SIZE", "0px", "blur"],
            color: ["COLOR", "0px", "color"]
        }
    },
    backgroundColor: {
        l: "Bgd Colour",
        reg: "{color}",
        c: {
            color: ["COLOR", "transparent", "Color"]
        }
    }
};

const TABLE_STYLE = {
    borderBottom: "1px solid rgba(0,0,0,0.2)"
}

const LABEL_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    borderRight: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "right",
    width: "25%",
    background: "rgba(0,0,0,0.2)"
};

const INPUT_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "left"
};

const SUB_LABEL_STYLE = {
    borderLeft: "12px solid rgba(0,0,0,0.2)",
    ...LABEL_STYLE
}

const INPUT_TXT_STYLE = {
    display: "block",
    border: "none",
    padding: "0px",
    margin: "0px",
    background: "transparent",
    borderRadius: "0px"
}

const HEADER_SECTION_STYLE = {
    padding: "8px 5px 5px 5px",
    fontWeight: "bold",
    textAlign: "left",
    textTransform: "uppercase",
    background: "rgba(0,0,0,0.5)"
};

const SELECT_STYLE = {
    border: "none",
    width: "100%",
    height: "100%",
    margin: 0,
    padding: 0,
    fontSize: "12px",
    background: "transparent",
    color: "#FFFFFF"
};

class BoolButton extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = () => {
        if (this.props.value == true || this.props.value == 'true') {
            this.props.onChange({
                target: {
                    name: this.props.name || null,
                    value: false
                }
            })
        } else {
            this.props.onChange({
                target: {
                    name: this.props.name || null,
                    value: true
                }
            })
        }
    };

    render() {
        let iconClass = 'fal fa-toggle-off';
        let style = {
            fontSize: '18px',
            color: '#F2F2F2',
            paddingTop: '3px',
            cursor: 'pointer'
        }
        if (['true', true].indexOf(this.props.value) >= 0) {
            iconClass = 'fal fa-toggle-on';
            style.color = 'green';
        }
        return (
            <div
                className="bool-button"
                style={style}
                onClick={this._click}>
                <i className={iconClass}></i>
            </div>
        )
    }

}

class Styler extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            edit: false,
            value: "",
            childs: false,
            showEditor: false
        }

        window.addEventListener("closedits", this._closeEdit);
    }

    componentWillUnmount() {
        window.removeEventListener("closedit", this._closeEdit);
    }


    _edit = () => {
        window.dispatchEvent(new CustomEvent("closeedits", {}));
        this.setState({
            edit: true
        })
        // this._ref.focus();
    };

    _closeEdit = () => {
        this.setState({
            edit: false
        })
    };

    _onChange = (e) => {
        instance.updateProp(this.props.prop, e.target.value);
    };

    _expand = () => {
        this.setState({
            childs: !this.state.childs
        })
    };

    _action = (action) => {
        if (action == "CLOSE") {
            this.setState({
                showEditor: false
            })
        }
    };

    render() {
        let editor;

        switch (this.props.data.t) {
            case 'text':
            default:
                editor = (
                    <input
                        value={instance.config[this.props.prop] || null}
                        onChange={this._onChange}
                        type="text"/>
                );
                break;
            case 'select':
                editor = (
                    <select onChange={this._onChange}
                            value={instance.config[this.props.prop] || null}>
                        {this.props.data.o.map(item => {
                            return (
                                <option value={item[0]}>{item[1]}</option>
                            )
                        })}
                    </select>
                );
                break;
            case 'number':
                editor = (
                    <input type="number"
                           value={instance.config[this.props.prop] || null}
                           onChange={this._onChange}/>
                );
                break;
            case 'bool':
                editor = (
                    <BoolButton
                        value={instance.config[this.props.prop]}
                        onChange={this._onChange}/>
                );
                break;
            case 'color':
                editor = (
                    <div className="row">
                        <div className="column">
                            <input type="color"
                                   onChange={this._onChange}
                                   value={instance.config[this.props.prop] || null}/>
                        </div>
                        <div className="column"></div>
                    </div>
                );
                break;
        }


        let children = [];

        if (this.props.data.__) {
            for (let i in this.props.data.__) {
                children.push(
                    <Styler
                        isChild={true}
                        prop={i}
                        onChange={this.props.onChange}
                        data={this.props.data.__[i]}/>
                )
            }
        }

        return (
            <div style={{display: 'block'}} className="style-control">
                <div className="row">
                    {this.props.data.__ ?
                        <div className="column bb" style={{
                            maxWidth: "20px",
                            textAlign: "center",
                            padding: "5px",
                            background: "rgba(0,0,0,0.2)",
                            lineHeight: "26px"
                        }} onClick={this._expand}>
                            <i className={"fal " + (this.state.childs ? "fa-caret-down" : "fa-caret-right")}
                               style={{lineHeight: '26px'}}></i>
                        </div>
                        : null}
                    {this.props.isChild ?
                        <div className="column bb" style={{maxWidth: "5px", background: "rgba(0,0,0,0.2)"}}>
                            &nbsp;
                        </div>
                        : null}
                    <div className="column bb" style={{padding: "5px", lineHeight: "26px"}}>
                        {this.props.data.l}
                    </div>
                    <div className="column bb" style={{padding: "5px"}}>
                        {editor}
                    </div>
                </div>
                {this.state.childs ? children : null}
            </div>
        )
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={HEADER_SECTION_STYLE} colSpan="2">{this.props.data.l.en || this.props.data.l}</div>
        )
    }
}


class StyleEditor extends React.Component {
    static defaultProps = {
        data: {}, // style settings
        definition: {}, // definition for the form,
        addDefaults: true
    };

    constructor(props) {
        super(props);

        this.state = {
            search: ""
        }
    }

    _onSearchChange = (e) => {
        this.setState({
            search: e.target.value
        })
    };

    onChange = (prop, value) => {
        instance.updateProp(prop, value);
    };

    render() {
        let items = [];

        for (let prop in this.props.definition) {
            if (this.props.definition[prop].t == "H") {
                items.push(
                    <Header data={this.props.definition[prop]}/>
                )
            } else {
                items.push(
                    <Styler
                        prop={prop}
                        onChange={this.onChange}
                        data={this.props.definition[prop]}
                        style={this.props.data}/>
                )
            }
        }

        if (this.props.addDefaults) {
            for (let prop in STYLE_BASE) {
                if (STYLE_BASE[prop].t == "H") {
                    items.push(
                        <Header data={STYLE_BASE[prop]}/>
                    )
                } else {
                    items.push(
                        <Styler
                            prop={prop}
                            onChange={this.onChange}
                            data={STYLE_BASE[prop]}
                            style={this.props.data}/>
                    )
                }
            }
        }


        return items;
    }
}

export default StyleEditor;
