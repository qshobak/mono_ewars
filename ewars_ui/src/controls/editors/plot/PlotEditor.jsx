import React from "react";

import utils from "../../../utils/utils";

import ControlList from "./ControlList.jsx";
import StyleEditor from './StyleEditor.jsx'
import Button from "../../c.button.jsx";
import ActionGroup from "../../c.actiongroup.jsx";

import {PIE_STYLE, MAP_STYLE, STYLE, DP_STYLE, HEATMAP_STYLE, TABLE_STYLE} from './constants/';
import {getDefaults} from './defaults';
import {instance, PlotInstance} from './plot_instance';

import DimensionTree from "../../c.tree.dimensions.jsx";
import MeasureTree from "../../c.tree.measures.jsx";
import SourceView from './view.source.jsx';

import Preview from './Preview.jsx';

import DataManager from './DataManager.jsx';

const OVERLAY_STYLE = {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 999999,
    background: "#333333",
    display: "flex",
    flexDirection: "column"
};


const ANALYSIS_DROP = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "rgba(0,0,0,0.1)",
    display: "none",
    textAlign: "center",
    fontSize: "30px",
    color: "#FFFFFF",
    zIndex: 9
};

const PLOT_ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CLOSE', 'Close']
];

const TABS = [
    ['fa-database', "DATA", "Database"],
    ['fa-gamepad', 'CONTROLS', 'Controls'],
    ['fa-paint-brush', 'STYLE', 'Styling'],
    ['fa-angle-double-left', 'COLLAPSE', 'Collapse']
];

class AnalysisDrop extends React.Component {
    constructor(props) {
        super(props);

        window.addEventListener("showdrops", this._onShowDrop);
        window.addEventListener("hidedrops", this._hideDrops);
    }

    _onShowDrop = (type) => {
        if (type == "A") {
            if (this._el) this._el.style.display = "block";
        }
    };

    _hideDrops = () => {
        if (this._el) this._el.style.display = "none";
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        window.dispatchEvent(new CustomEvent("hidedrops", {}));
    };

    render() {
        return (
            <div
                onDragOver={this._onDragOver}
                onDrop={this._onDrop}
                ref={el => {
                    this._el = el
                }}
                style={ANALYSIS_DROP}>
                <i className="fal fa-download"></i>
            </div>
        )
    }
}

class PlotEditor extends React.Component {
    _plot = null;

    constructor(props) {
        super(props);

        let data = props.data.g ? props.data : getDefaults(props.data.type);
        let instance = new PlotInstance(data);

        this.state = {
            mainView: 'DATA',
            view: 'DATA',
            sidebar: true,
            data: instance
        };

        window.addEventListener("plotupdated", this._onChange);
    }

    componentWillUnmount() {
        window.removeEventListener("plotupdated", this._onChange);
    }

    _onChange = () => {
        console.log("HERE");
        this.setState({data: this.state.data});
    };

    _onSave = () => {
        let final = instance.getFinal();
        this.props.onSave(final);
    };

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this.props.onClose();
                break;
            case 'DATA':
                this.setState({view: "DATA"});
                break;
            case "STYLE":
                this.setState({view: "STYLE"});
                break;
            case "CONTROLS":
                this.setState({view: "CONTROLS"});
                break;
            default:
                break;

        }
        // ewars.z.dispatch("PE", "UNEDIT_FILTER");
    };

    _onStyleChange = (data) => {
        // ewars.z.dispatch('PE', 'UPDATE_CONFIG', data);
    };

    _toggleSidebar = () => {
        this.setState({
            sidebar: !this.state.sidebar
        })
    };


    render() {
        let leftView;

        if (this.state.view == "DATA") {
            leftView = (
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Dimensions</div>
                    </div>
                    <div className="row" style={{maxHeight: "50%", minHeight: "50%"}}>
                        <DimensionTree/>
                    </div>
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column">Measures</div>
                    </div>
                    <div className="row" style={{maxHeight: "50%", minHeight: "50%"}}>
                        <MeasureTree/>
                    </div>
                </div>
            )
        }
        if (this.state.view == "ANALYSIS") leftView = <AnalysisTools/>;
        if (this.state.view == "CONTROLS") leftView = <ControlList/>;
        if (this.state.view == "STYLE") {
            let styler;
            if (instance.type != 'DATAPOINT') styler = STYLE;
            if (instance.type == 'DATAPOINT') styler = DP_STYLE;
            if (instance.type == 'HEATMAP') styler = HEATMAP_STYLE;
            if (instance.type == 'MAP') styler = MAP_STYLE;
            if (instance.type == 'PIE') styler = PIE_STYLE;
            if (instance.type == 'TABLE') styler = TABLE_STYLE;
            leftView = <StyleEditor
                definition={styler}
                data={this.state.config}
                onChange={this._onStyleChange}/>
        }

        let modalView;

        let availTypes = [];
        let items = availTypes.map((item) => {
            return (
                <Button
                    label={item}
                    onClick={() => {
                        // ewars.z.dispatch("PE", "SET_TYPE", item);
                    }}/>
            )
        })

        let view;
        let render_type;

        let mainView;
        if (this.state.mainView == 'DEFAULT') mainView = <Preview data={instance.getFinal()}/>;
        if (this.state.mainView == 'DATA') mainView = <DataManager data={this.state}/>;
        if (this.state.mainView == 'SOURCE') mainView = <SourceView data={this.state}/>;

        return (
            <div style={OVERLAY_STYLE}>
                <div className="row">
                    <div className="column br panel">
                        <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                            <div className="column"></div>
                            <ActionGroup
                                naked={true}
                                actions={TABS}
                                onAction={this._action}/>
                        </div>
                        <div className="row block block-overflow">
                            {leftView}
                        </div>
                    </div>
                    <div className="column">
                        <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                            <div className="column"></div>
                            <ActionGroup
                                actions={PLOT_ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div className="row">
                            {mainView}
                        </div>
                    </div>
                </div>
                <div className="row bt gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column"></div>
                </div>
            </div>
        )
    }
}

export default PlotEditor;
