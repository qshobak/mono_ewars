import React from "react";

import ContextMenu from "./ContextMenu.jsx";
import ModalDialog from './editors/editor.filters.jsx';

import TooltipEditor from './editors/editor.tooltip.jsx';

import {PRESET_NAMES} from './constants/';

import {instance} from './plot_instance';

const STYLE = {
    WRAPPER: {
        display: "block",
        float: "left",
        background: "#333333",
        color: "#F2F2F2",
        height: "inherit",
        paddingLeft: "8px",
        paddingRight: "8px",
        paddingTop: "8px",
        paddingBottom: "8px",
        position: "relative",
        cursor: "pointer"
    },
    WRAPPER_FILTER: {
        position: "relative",
        display: "block",
        background: 'rgba(0,0,0,0.4)',
        borderRadius: "16px",
        color: "#F2F2F2",
        height: "25px",
        padding: "8px",
        cursor: "pointer",
        margin: "5px 8px"
    },
    CONTEXT: {
        position: "absolute",
        top: "100%",
        right: 0,
        background: "#333333",
        border: "1px solid #000000",
        width: "200px",
        minHeight: "100px",
        zIndex: 999
    },
    SEP: {
        display: "block",
        borderBottom: "1px solid #000000",
        height: "8px",
        marginBottom: "7px"
    },
    ITEM: {
        display: "block",
        padding: "8px"
    }
};

ewars.g.measures = {};

class Measure extends React.Component {
    static defaultProps = {
        filter: false,
        gid: null,
        nid: null
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            editor: false,
            data: null,
            edit: false,
            editMode: null
        }
    }

    _toggle = () => {
        if (this.state.editor) return;
        this.setState({
            show: !this.state.show
        })
    };

    _action = (action) => {
        this.setState({show: false});

        if (action == "REMOVE") {
            if (this.props.filter) {
                ewars.z.dispatch("PE", "REMOVE_FILTER", [this.props.gid, this.props.nid, this.props.data[0]])
            } else {
                ewars.z.dispatch("PE", "REMOVE_ITEM", this.props.data[0])
            }
        }

        if (action == "FILTER") {
            this.setState({
                editor: true
            })
        }

        if (action == "CLOSE") {
            this.setState({
                editor: false,
                edit: false
            })
        }

        if (action == "STYLE") {
            this.setState({
                edit: true,
                editMode: "STYLE"
            })
        }

        if (action == "TOOLTIP") {
            this.setState({
                edit: true,
                editMode: "TOOLTIP"
            })
        }
    };

    _onFilterSave = (data) => {
        this.setState({editor: false}, () => {
            ewars.z.dispatch('PE', 'UPDATE_FILTER', [
                this.props.gid,
                this.props.nid,
                data
            ]);
        })
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let label = "Loading...";

        if (node.isField()) {
            label = node.getShortLabel();
        }

        if (node.isFilter()) {
            return (
                <div style={STYLE.WRAPPER_FILTER} onClick={this._toggle}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="20px">
                            <i className="fal fa-filter"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>{label}</ewars.d.Cell>
                        <ewars.d.Cell width="20px" style={{textAlign: "center", display: "block"}}>
                            <i className="fal fa-caret-down"></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    {this.state.show ?
                        <ContextMenu
                            onClick={this._action}/>
                        : null}
                    {this.state.editor ?
                        <ModalDialog
                            type="FILTER"
                            onSave={this._onFilterSave}
                            nid={node._}
                            onClose={() => this._action("CLOSE")}/>
                        : null}
                </div>
            )
        }

        let editView;
        if (this.state.edit) {
            if (this.state.editMode == "STYLE") editView = <StyleEditor/>;
            if (this.state.editMode == "TOOLTIP") editView = <TooltipEditor/>;
        }

        return (
            <div style={STYLE.WRAPPER} onClick={this._toggle}>
                {label}&nbsp;&nbsp;<i className="fal fa-caret-down"></i>
                {this.state.show ?
                    <ContextMenu onClick={this._action}/>
                    : null}
                    <ewars.d.Shade
                        toolbar={false}
                        onAction={this._action}
                        shown={this.state.edit}>
                        {editView}
                    </ewars.d.Shade>
            </div>
        )
    }

}

export default Measure;
