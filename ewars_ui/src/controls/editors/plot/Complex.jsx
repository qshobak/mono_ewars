import React from "react";

const STYLE = {
    WRAPPER: {
        display: "block",
        float: "left",
        background: "#000000",
        color: "#F2F2F2",
        height: "40px"
    }
}

class Complex extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={STYLE.WRAPPER}>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        [Complex name]
                    </ewars.d.Cell>
                    <ewars.d.Cell width="15px">
                        <i className="fal fa-caret-down"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default Complex;
