import React from "react";
import * as d3 from "d3";

const STYLE = {
    border: "1px solid #000000",
    position: "absolute",
    top: 10,
    right: 10,
    bottom: 10,
    left: 10,
    background: "rgba(0,0,0,0.1)"
}

const DATA = [
    ["2017-01-01", 100],
    ["2017-02-01", 1000],
    ["2017-03-01", 2000],
    ["2017-04-01", 100]
]

class Chart extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {



        return (
            <div className="plot-chart" style={STYLE}>

            </div>
        )
    }
}

export default Chart;

