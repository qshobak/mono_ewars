import React from "react";
const d3 = require('d3');
const DateUtils = require("../../documents/utils/DateUtils");
import RangeUtils from "../../../../utils/range_utils";
import utils from "../../../../utils/utils";

function _isSet(check, fallback) {
    if (["", null, undefined].indexOf(check) >= 0) {
        return fallback;
    }

    return check;
}

class Map2 {
    constructor(el, definition, reportDate, location_uuid, isPublic, idx) {
        this.definition = definition;
        this._el = el;
        this._reportDate = reportDate;
        this._locationUUID = location_uuid;
        this._public = isPublic;
        this._idx = idx;

        this._setupLoading();
        this.query(definition);
    }

    _setupLoading = () => {
        this._el.className += " chart-loading";
        this._el.innerText = "Loading...";
    };

    _tearDownLoading = () => {
        this._el.className = "chart";
        this._el.innerText = "";
    };

    getColour = (value, ot, oth) => {
        let colour;

        let anchors = this.definition.scales || this.definition.thresholds;
        anchors.forEach(scale => {
            if (scale[1] === "INF") {
                if (value >= scale[0]) colour = scale;
            } else {
                if (value.data >= parseFloat(scale[0]) && value.data <= parseFloat(scale[1]))
                    colour = scale;
            }
        });

        if (!colour) return "rgba(0,0,0,0)";
        return colour[2];
    };

    getData = () => {
        return ["MAP", [this.definition, this._data]];
    };

    query = (config) => {
        let start_date,
            end_date;
        if (config.period) {
            end_date = RangeUtils.process(config.period[1], this._reportDate);
            start_date = RangeUtils.process(config.period[0], this._reportDate, end_date);
        } else {
            let dateRange = DateUtils.processDateSpec(config, this._reportDate);
            start_date = dateRange[0];
            end_date = dateRange[1];
        }

        let query = {
            type: config.source_type || "SLICE",
            reduction: "SUM",
            interval: "DAY",
            indicator: config.indicator,
            start_date: start_date,
            end_date: end_date,
            location: null,
            tid: this._tid,
            geometry: true,
            series: config.series || null,
            formula: config.formula || null,
            centroid: true
        };

        if (config.loc_source || config.loc_spec) {
            let loc_source = config.loc_source || config.loc_spec;

            if (loc_source == "SPECIFIC") {
                query.location = config.location;
            } else if (["TYPE", "GENERATOR"].indexOf(loc_source) >= 0) {
                query.location = {
                    parent_id: config.location,
                    site_type_id: config.site_type_id,
                    status: config.location_status || "ACTIVE"
                }
            } else if (loc_source == "GROUP") {
                query.location = {
                    groups: config.group_ids,
                    agg: "INDIVIDUAL"
                }
            }
        } else {
            query.location = {
                parent_id: config.location,
                site_type_id: config.site_type_id,
                status: config.location_status || "ACTIVE"
            }
        }

        //
        // ewars._queue.push("/arc/analysis", query)
        //     .then(resp => {
        //         this._data = resp;
        //         this.render();
        //     })
        //     .catch((err) => {
        //         let errDOM = document.createElement("i");
        //         errDOM.setAttribute("class", "raw-error fa fa-exclamation-triangle red");
        //         this._el.appendChild(errDOM);
        //         ewars.emit("WIDGET_LOADED");
        //     })
    };

    updateDefinition = (config) => {
        this.definition = config;

        if (this.svg) {
            this.svg.remove();
            this.svg = null;
        }
        this.query(config);
    };

    buildLegend = (config) => {
        if (this._legendId) {
            let original = document.getElementById(this._legendId);
            original.parentNode.removeChild(original);
        }
        var container = document.createElement("div");
        container.setAttribute("class", "map-legend");
        this._legendId = utils.uuid();
        container.setAttribute("id", this._legendId);

        let scales = config.thresholds || config.scales;
        scales.forEach(item => {
            let scaleEl = document.createElement("div");
            let dot = document.createElement("i");
            dot.setAttribute("class", "fa fa-circle");
            dot.style.color = item[2];

            let labelText;
            if (["", NaN, null, undefined].indexOf(item[1]) >= 0) {
                labelText = document.createTextNode(` ${item[0] || 0}+`);
            } else {
                labelText = document.createTextNode(` ${item[0] || 0} to ${item[1]}`);
            }

            scaleEl.appendChild(dot);
            scaleEl.appendChild(labelText);

            container.appendChild(scaleEl);
        });

        this._el.appendChild(container);
    };

    _buildKey = (locations) => {
        var container = document.createElement("div");
        container.setAttribute("class", "map-key");
        this._keyId = utils.uuid();
        container.setAttribute("id", this._keyId);

        locations.forEach((item, index) => {
            let row = document.createElement("div");
            row.setAttribute("class", "key-row");
            let rowKey = document.createElement("div");
            rowKey.setAttribute("class", "key-row-id");
            let keyText = document.createTextNode(index);
            rowKey.appendChild(keyText);
            row.appendChild(rowKey);

            let rowLabel = document.createElement("div");
            rowLabel.setAttribute("class", "key-row-label");
            let labelText = document.createTextNode(__(item.location.name));
            rowLabel.appendChild(labelText);
            row.appendChild(rowLabel);

            container.appendChild(row);
        })

        this._el.appendChild(container);

    };



    render() {
        this._tearDownLoading();
        let self = this;

        let height = this._el.parentNode.clientHeight;
        if (this.definition.height && this.definition.height != "") {
            height = this.definition.height;
            if (height.indexOf) {
                if (height.indexOf("px") >= 0) height = height.replace("px", "");
                if (height.indexOf("px") >= 0) height = height.replace("%", "");
                if (height == "") height = this._el.parentNode.clientHeight;
            }
        }

        if (height[height.length - 1] == "%") height = this._el.parentNode.clientHeight;

        let width = this._el.parentNode.clientWidth;
        if (this.definition.width && this.definition.width != "") {
            width = this.definition.width;
            if (width.indexOf) {
                if (width.indexOf("px") >= 0) width = width.replace("px", "");
                if (width.indexOf("px") >= 0) width = width.replace("%", "");
                if (width == "") width = this._el.parentNode.clientWidth;
            }
        }

        if (width[width.length - 1] == '%') width = this._el.parentNode.clientWidth;
        if (width == 0) width = this._el.parentNode.offsetWidth;

        width = parseInt(width);
        height = parseInt(height);

        this._el.style.position = 'relative';

        // append root SVG
        let svg = d3.select(this._el).append("svg")
            .attr("width", width)
            .attr("height", height);

        // Create base projection
        let projection = d3.geoMercator()
            .scale(1)
            .translate([0, 0]);

        // Create geo path
        let path = d3.geoPath()
            .projection(projection);


        this.svg = svg;

        let bgd = svg.append("g");
        bgd.append("rect")
            .attr("class", "background")
            .style("fill", _isSet(this.definition.bgd_colour, "transparent"))
            .attr("width", width)
            .attr("height", height);

        let base = svg.append("g");

        if ((this.definition.loc_source || "TYPE") == "SPECIFIC") {
            let baseG = g.append("g");
            let labelsG = g.append("g");

            let data = this._data;

            baseG.append("path")
                .attr("d", path(JSON.parse(data.d.location.geometry || '{}')))
                .style("fill", this.getColour({data: data.d.data}));

            let fontSize = "0.1em";
            if (utils.isSet(this.definition.label_font_size)) {
                fontSize = this.definition.label_font_size;
                if (fontSize.indexOf("em") < 0) fontSize += "em";
            }
            let labelColour = "#000000";
            if (utils.isSet(this.definition.label_colour)) {
                labelColour = this.definition.label_colour;
            }
            labelsG.append("text")
                .style("text-anchor", "middle")
                .style("fill", labelColour)
                .style("font-size", "12px")
                .attr("x", function (d) {
                    return path.centroid(JSON.parse(data.d.location.geometry || '{}'))[0];
                })
                .attr("y", function (d) {
                    return path.centroid(JSON.parse(data.d.location.geometry || '{}'))[1];
                })
                .text(function (d) {
                    return data.d.location.name.en || data.d.location.name;
                });

            let bounds = path.bounds(JSON.parse(this._data.d.location.geometry || '{}')),
                dx = bounds[1][0] - bounds[0][0],
                dy = bounds[1][1] - bounds[0][1],
                x = (bounds[0][0] + bounds[1][0]) / 2,
                y = (bounds[0][1] + bounds[1][1]) / 2,
                scale = .9 / Math.max(dx / width, dy / height),
                translate = [width / 2 - scale * x, height / 2 - scale * y];

            g.attr("transform", "translate(" + translate + ")scale(" + scale + ")");
        }

        if (["GROUP", "TYPE", "GENERATOR"].indexOf(this.definition.loc_source) >= 0 || this.definition.loc_spec == "GENERATOR") {
            let validLocs = [];
            this._data.d.forEach(item => {
                let geo = JSON.parse(item.location.geometry || '{}');
                if (geo.features == null || geo.features == undefined) {

                } else {
                    validLocs.push(item);
                }

            });

            // let chloroG = g.append("g");
            // let labelsG = g.append("g");
            // let lineStrings = g.append("g");




            //
            // let chloros = chloroG.selectAll("g")
            //     .data(validLocs)
            //     .enter()
            //     .append("g");
            //
            // let lG = labelsG.selectAll("g")
            //     .data(validLocs)
            //     .enter()
            //     .filter((d) => {
            //         if (this.definition.label_threshold) {
            //             if (parseFloat(d.data || 0) >= this.definition.label_threshold) return true;
            //             return false
            //         }
            //         return true;
            //     })
            //     .append("g");
            //
            // chloros.append("path")
            //     .attr("d", function (datum, i, nodes) {
            //         return path(JSON.parse(datum.location.geometry));
            //     })
            //     .attr("class", "mesh")
            //     .style("fill-opacity", _isSet(this.definition.chloro_opacity, 0.8))
            //     .style("fill", this.getColour)
            //     .style("stroke", "rgba(0,0,0,1)")
            //     .style("stroke-width", "0.5")
            //
            // let fontSize = "0.1em";
            // if (ewars.isSet(this.definition.label_font_size)) {
            //     fontSize = this.definition.label_font_size;
            //     if (fontSize.indexOf("em") < 0) fontSize += "em";
            // }
            // let labelColour = "#000000";
            // if (ewars.isSet(this.definition.label_colour)) {
            //     labelColour = this.definition.label_colour;
            // }
            //
            // lG.append("text")
            //     .style("text-anchor", "middle")
            //     .style("fill", labelColour)
            //     .style("font-size", fontSize)
            //     .attr("x", function (d) {
            //         return path.centroid(JSON.parse(d.location.geometry || "{}"))[0];
            //     })
            //     .attr("y", function (d) {
            //         return path.centroid(JSON.parse(d.location.geometry || "{}"))[1];
            //     })
            //     .text(function (d) {
            //         return ewars.I18N(d.location.name);
            //     });
            //
            //
            // lG.append("text")
            //     .style("text-anchor", "middle")
            //     .style("fill", labelColour)
            //     .style("font-size", fontSize)
            //     .attr("x", function (d) {
            //         return path.centroid(JSON.parse(d.location.geometry))[0];
            //     })
            //     .attr("y", function (d) {
            //         return path.centroid(JSON.parse(d.location.geometry))[1] + 1;
            //     })
            //     .text(function (d) {
            //         return ewars.NUM(d.data, self.definition.format || "0,0.0");
            //     });
            //
            // let links = [];
            // for (var i = 0, len = validLocs.length; i < len; i++) {
            //     let coords = path.centroid(JSON.parse(validLocs[i].location.geometry));
            //     links.push({
            //         type: "LineString",
            //         coordinates: [
            //             [coords[0], coords[1]],
            //             [coords[0] + 1, coords[1] + 1]
            //         ]
            //     })
            // }
            //
            // let pathArcs = lineStrings.selectAll(".line")
            //     .data(links);
            //
            // pathArcs.enter()
            //     .append("path")
            //     .attr("class", "line")
            //     .style("fill", "none");
            //
            // pathArcs
            //     .attr("d", path)
            //     .style("stroke", "black")
            //     .style("stroke-width", "2px")
            //
            //
            // if ((this.definition.pie_overlay || "HIDE") == "SHOW") {
            //     this.buildOverlay(this.definition);
            // }

            let root = JSON.parse(this._data.g);

            let b = path.bounds(root),
                s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height),
                center = path.centroid(JSON.parse(this._data.g)),
                t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];


            projection.scale(s)
                .translate(t);

            base.append("path")
                .datum(root)
                .style("fill", _isSet(this.definition.base_colour, "#FFFFFF"))
                .attr("d", path)

            // Set up chloropleths
            let chloros = svg.append("g");

            chloros.selectAll("path")
                .data(validLocs)
                .enter()
                .append("path")
                .attr("d", function(datum) {
                    return path(JSON.parse(datum.location.geometry))
                })
                .attr("id", function (d) {
                    return d.location.uuid;
                })
                .attr("class", "mesh")
                .style("fill-opacity", _isSet(this.definition.chloro_opacity, 0.8))
                .style("fill", this.getColour)
                .style("stroke", this.definition.s_stroke || "rgba(0,0,0,1)")
                .style("stroke-width", this.definition.s_stroke_width || 1)

            if (this.definition.b_labels) {
                let labelItems = validLocs;
                if (this.definition.label_threshold) {
                    labelItems = validLocs.filter((item) => {
                        return item.data >= parseFloat(this.definition.label_threshold);
                    })
                }

                if (this.definition.lbl_style == "DEFAULT") {
                    // Set up labels
                    let labels = svg.append("g");
                    labels.selectAll("text")
                        .data(labelItems)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this.definition.label_colour || "#000")
                        .style("font-size", this.definition.label_font_size || 12)
                        .style("text-align", "center")
                        .attr("x", function (d) {
                            return path.centroid(JSON.parse(d.location.geometry || "{}"))[0];
                        })
                        .attr("y", function (d) {
                            return path.centroid(JSON.parse(d.location.geometry || "{}"))[1];
                        })
                        .text(function (d) {
                            return __(d.location.name);
                        });
                }

                if (this.definition.lbl_style == "NUMBERED") {
                    let labels = svg.append("g");
                    labels.selectAll("text")
                        .data(labelItems)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this.definition.label_colour || "#000")
                        .style("font-size", this.definition.label_font_size || 10)
                        .style("text-align", "center")
                        .attr("x", function (d) {
                            return path.centroid(JSON.parse(d.location.geometry || "{}"))[0];
                        })
                        .attr("y", function (d) {
                            return path.centroid(JSON.parse(d.location.geometry || "{}"))[1];
                        })
                        .text(function (d, i) {
                            return i + 1;
                        });

                    this._buildKey(validLocs);
                }
            }

            // let counter = 1;
            // let numbers = svg.append("g")
            // numbers.selectAll("text")
            //     .data(validLocs)
            //     .enter()
            //     .append("text")
            //     .style("text-anchor", "middle")
            //     .style("fill", "#333")
            //     .style("font-size", "12px")
            //     .style("text-align", "center")
            //     .style("display", function (d) {
            //         console.log(path.area(JSON.parse(d.location.geometry || {})))
            //         return "block";
            //     })
            //     .attr("x", function (d) {
            //         console.log(d);
            //         return path.centroid(JSON.parse(d.location.geometry || "{}"))[0];
            //     })
            //     .attr("y", function (d) {
            //         return path.centroid(JSON.parse(d.location.geometry || "{}"))[1];
            //     })
            //     .text(function (d) {
            //         counter++;
            //         return counter -1;
            //     });


            // baseG.append("path")
            //     .attr("d", path(JSON.parse(this._data.g)))
            //     .style("fill", _isSet(this.definition.base_colour, "#FFFFFF"));


            // let bounds = path.bounds(JSON.parse(this._data.g || "{}")),
            //     dx = bounds[1][0] - bounds[0][0],
            //     dy = bounds[1][1] - bounds[0][1],
            //     x = (bounds[0][0] + bounds[1][0]) / 2,
            //     y = (bounds[0][1] + bounds[1][1]) / 2,
            //     scale = .9 / Math.max(dx / width, dy / height),
            //     translate = [width / 2 - scale * x, height / 2 - scale * y];
            //
            // g.attr("transform", "translate(" + translate + ")scale(" + scale + ")");
        }


        if ((this.definition.legend || "HIDE") == "SHOW") {
            this.buildLegend(this.definition);
        }
        window.dispatchEvent(new CustomEvent("widgetloaded"));

    }
}

export default Map2;
