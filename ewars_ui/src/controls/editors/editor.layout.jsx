import React from "react";

import LayoutEditor from "./layout/LayoutEditor.jsx";

class EditorLayout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <LayoutEditor/>
            </div>
        )
    }
}

export default EditorLayout;
