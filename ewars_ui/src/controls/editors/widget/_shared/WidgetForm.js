import {Layout, Row, Cell} from "../../../cmp/Layout";
import Toolbar from "../../../cmp/Toolbar";
import Button from "../../../cmp/Button";

const SelectField = require("../../fields/SelectField.react");
const TextAreaField = require("../../fields/TextAreaField.react");
const TextInputField = require("../../fields/TextInputField.react");
const LocationSelectField = require("../../fields/LocationSelectField.react");
const LanguageStringField = require("../../fields/LanguageStringInput.react");
const NumberField = require("../../fields/NumberField.react");
const SwitchField = require("../../fields/SwitchField.react");
const IndicatorSelector = require("../../indicator/IndicatorSelectorComponent.react");

import DateField from "../../fields/DateField.react";
import RangeSelector from "../../../cmp/fields/RangeSelector";

const FIELDS = {
    select: SelectField,
    textarea: TextAreaField,
    text: TextInputField,
    location: LocationSelectField,
    language_string: LanguageStringField,
    numeric: NumberField,
    switch: SwitchField,
    indicator: IndicatorSelector,
    date: DateField,
    date_range: RangeSelector
};

class Field extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const FieldCmp = FIELDS[this.props.data.type] || null;

        if (!FieldCmp) {
            return (
                <div className="dummy"></div>
            )
        }

        return (
            <div className="field">
                <FieldCmp/>
            </div>
        )
    }
}

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: true
        }
    }

    render() {
        return (
            <div className="form-section">
                <Layout>
                    <Toolbar>
                    </Toolbar>
                    <Row>
                        <Cell>
                            {this.props.data.c.map(field => {
                                return (
                                    <Field
                                        definition={field}
                                        data={this.props.data}
                                        logic={this.props.logic}/>
                                )
                            })}
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
}

class WidgetForm extends React.Component {
    static defaultProps = {
        definition: [],
        data: {},
        onChange: null,
        onFieldChange: null,
        logic: null
    };

    constructor(props) {
        super(props);
    }

    /**
     * Handles changes within a specific section
     * @param path - The path in the target data object to update
     * @param value - The value to update at the path
     * @private
     */
    _onSectionChange = (path, value) => {

    };

    render() {
        return (
            <div className="ide-settings-content">
                <div className="ide-settings-form">
                    <div className="form">
                        {this.props.definition.map(section => {
                            return (
                                <Section
                                    data={this.props.data}
                                    definition={section}
                                    logic={this.props.logic}
                                    onChange={this._onSectionChange}/>
                            )
                        })}

                    </div>
                </div>
            </div>
        )
    }
}

export default WidgetForm;