import SelectField from "../../fields/SelectField.react";
const Button = require("../../ButtonComponent.react");
const NumberField = require("../../fields/NumberField.react");
const TextField = require("../../fields/TextInputField.react");

const COMPARATORS = [
    ["EQ", "eq"],
    ["NEQ", "neq"],
    ["GT", "gt"],
    ["GTE", "gte"],
    ["LT", "lt"],
    ["LTE", "lte"]
];

class Threshold extends React.Component {
    constructor(props) {
        super(props)
    }

    _update = (prop, value) => {
        let data = ewars.copy(this.props.data);
        if (prop == "comparator") data[0] = value;
        if (prop == "value") data[1] = value;
        if (prop == "color") data[2] = value;

        this.props.onChange(this.props.index, data);
    };

    _remove = () => {
        this.props.onRemove(this.props.index);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content" style={{padding: 0}}>
                    <div className="ide-row">
                        <div className="ide-col border-right" style={{padding: 5}}>
                            <SelectField
                                name="comparator"
                                value={this.props.data[0]}
                                onUpdate={this._update}
                                config={{options: COMPARATORS}}/>
                        </div>
                        <div className="ide-col border-right" style={{padding: 5}}>
                            <NumberField
                                name="value"
                                value={this.props.data[1]}
                                onUpdate={this._update}/>
                        </div>
                        <div className="ide-col border-right" style={{padding: 5}}>
                            <TextField
                                onUpdate={this._update}
                                name="color"
                                value={this.props.data[2]}/>
                        </div>
                        <div className="ide-col" style={{maxWidth: 50}}>
                            <div className="btn-group">
                                <ewars.d.Button
                                    icon="fa-trash"
                                    onClick={this._remove}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Thresholds extends React.Component {
    constructor(props) {
        super(props);
    }

    _onRemove = (index) => {
        let data = ewars.copy(this.props.data || []);
        data.splice(index, 1);
        this.props.onChange(data);
    };

    _onChange = (index, threshold) => {
        let data = ewars.copy(this.props.data || []);
        data[index] = threshold;
        this.props.onChange(data);
    };

    _addThreshold = () => {
        let data = ewars.copy(this.props.data || []);
        data.push(["EQ", 0, "#333"]);
        this.props.onChange(data);
    };

    render() {
        let thresholds = this.props.data || [];
        return (
            <div className="threshold-editor">
                <div className="ide-tbar">
                    <div className="ide-tbar-text">Colour Thresholds</div>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={this._addThreshold}/>
                    </div>
                </div>

                <div className="block-tree" style={{position: "relative", maxHeight: "none"}}>
                    {thresholds.map(function (item, index) {
                        return <Threshold data={item}
                                          onRemove={this._onRemove}
                                          onChange={this._onChange}
                                          index={index}/>
                    }.bind(this))}
                </div>

            </div>
        )
    }
}

export default Thresholds;