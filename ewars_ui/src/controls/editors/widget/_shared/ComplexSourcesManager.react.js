var Button = require("../../ButtonComponent.react");

var TextField = require("../../fields/TextInputField.react");
var SelectField = require("../../fields/SelectField.react");
var IndicatorField = require("../../indicator/IndicatorSelectorComponent.react");

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

var DEFAULTS = {
    variable_name: "var_name",
    indicator: null,
    reduction: "NONE"
};

var FORM = {
    reduction: {
        type: "select",
        label: {en: "Reduction"},
        options: [
            ["NONE", "None"],
            ["SUM", "Sum"],
            ["MED", "Average/Mean"]
        ]
    }
};

var ComplexSource = React.createClass({
    getInitialState: function () {
        return {
            isExpanded: false
        }
    },

    _update: function (prop, value) {
        console.log(prop, value);
        if (prop == "variable_name") {
            value = value.replace(" ", "_")
        }
        this.props.onChange(this.props.index, prop, value);
    },

    _expand: function () {
        this.setState({
            isExpanded: this.state.isExpanded ? false : true
        })
    },

    _delete: function () {
        this.props.onDelete(this.props.index);
    },

    render: function () {
        return (
            <div className="variable-source">
                <div className="header">
                    <div className="label">{this.props.data.variable_name}</div>
                    <div className="controls">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this._expand}
                                icon="fa-pencil"/>
                            <ewars.d.Button
                                onClick={this._delete}
                                colour="red"
                                icon="fa-trash"/>
                        </div>
                    </div>
                </div>
                {this.state.isExpanded ?
                <div className="details">
                    <div className="hsplit-box">
                        <div className="ide-setting-label">Variable Name</div>
                        <div className="ide-setting-control">
                            <TextField
                                name="variable_name"
                                readOnly={false}
                                value={this.props.data.variable_name}
                                onUpdate={this._update}/>
                        </div>
                    </div>

                    <div className="hsplit-box">
                        <div className="ide-setting-label">Indicator</div>
                        <div className="ide-setting-control">
                            <IndicatorField
                                name="indicator"
                                Form={this.props.Form}
                                readOnly={false}
                                value={this.props.data.indicator}
                                onUpdate={this._update}/>
                        </div>
                    </div>

                    <div className="hsplit-box">
                        <div className="ide-setting-label">Reduction</div>
                        <div className="ide-setting-control">
                            <SelectField
                                name="reduction"
                                readOnly={false}
                                value={this.props.data.reduction}
                                onUpdate={this._update}
                                config={FORM.reduction}/>
                        </div>
                    </div>
                </div>
                    : null }
            </div>
        )
    }
});

var ComplexSourcesManager = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onChange: function (index, prop, value) {
        var copy = ewars.copy(this.props.value || []);
        copy[index][prop] = value;
        this.props.onUpdate(this.props.name, copy);
    },

    _addVariable: function () {
        var copy = ewars.copy(this.props.value || []);
        copy.push(_.extend({}, DEFAULTS));
        this.props.onUpdate(this.props.name, copy);
    },

    _delete: function (index) {
        var copy = ewars.copy(this.props.value || []);
        copy.splice(index, 1);
        this.props.onUpdate(this.props.name, copy);
    },

    render: function () {
        var variables = _.map(this.props.value, function (series, index) {
            return <ComplexSource
                onDelete={this._delete}
                data={series}
                Form={this.props.Form}
                index={index}
                onChange={this._onChange}/>
        }, this);

        return (
            <div className="variable-manager">
                <div className="ide-tbar">
                    <ewars.d.Button
                        icon="fa-plus"
                        onClick={this._addVariable}
                        label="Add Variable"/>

                </div>

                <div className="series-list">
                    {variables}
                </div>

            </div>

        )
    }
});

module.exports = ComplexSourcesManager;