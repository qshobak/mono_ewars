var PROTOTYPES = {
    SLICE: {
        type: "SLICE",
        uuid: null,
        indicator: null,
        location: null,
        start_date: null,
        end_date: null,
        reduction: null
    },
    SLICE_COMPLEX: {
        type: "SLICE_COMPLEX",
        uuid: null,
        location: null,
        start_date: null,
        end_date: null,
        formula: null,
        series: []
    },
    SERIES_COMPLEX: {
        type: "SERIES_COMPLEX",
        uuid: null,
        location: null,
        interval: null,
        start_date: null,
        end_date: null,
        formula: null,
        series: []
    },
    SERIES: {
        type: "SERIES",
        uuid: null,
        location: null,
        interval: null,
        indicator: null,
        start_date: null,
        end_date: null
    },
    MATRIX: {
        type: "TABLE",
        uuid: null,
        location: null,
        columns: []
    },
    THRESHOLD: {
        type: "THRESHOLD",
        uuid: null,
        indicator: null,
        location: null,
        start_date: null,
        end_date: null,
        interval: null
    }
};

var SOURCES_COMPLEX = {
    SLICE_COMPLEX: {
        uuid: null,
        indicator: null,
        variable_name: null,
        reduction: null
    },
    SERIES_COMPLEX: {
        uuid: null,
        indicator: null,
        variable_name: null
    }
};

var TYPE_MAPS = {
    INDICATOR: "SLICE",
    SLICE: "SLICE",
    SLICE_COMPLEX: "SLICE_COMPLEX",
    COMPLEX: "COMPLEX",
    SERIES: "SERIES",
    TABLE: "TABLE",
    SERIES_COMPLEX: "SERIES_COMPLEX",
    THRESHOLD: "THRESHOLD"
};

function _parseType(definition) {
    if (definition.type == "TABLE") return "MATRIX";
    if (definition.source_type) {
        return TYPE_MAPS[definition.source_type];
    }

    if (definition.dataType == "SERIES") return "SERIES";
    if (["line", "bar", "area"].indexOf(definition.type) >= 0 && definition.dataType == "COMPLEX") {
        return "SERIES_COMPLEX";
    }

    if (definition.dataType) {
        return TYPE_MAPS[definition.dataType];
    }

    if (definition.type == "RAW") {
        // Need to figure out if it's a SLICE_COIMPLEX
        if (definition.series[0].series && definition.series[0].series.length > 0) {
            return "SLICE_COMPLEX";
        } else {
            return "SLICE"
        }
    }
}

function _or() {
    var result = null;

    _.each(arguments, function (arg) {
        if (arg && arg != undefined) result = arg;
    });

    return result;
}

/**
 * Detects if a widget definition is a legacy definition
 */
var isLegacy = function (definition) {
    if (definition.type == "RAW" && !definition.source_type) return true;
    if (definition.type == "PIE") return true;
    if (definition.type == "SERIES" && !definition.spec) return true;
};

/**
 * Updates a legacy definition to the newer format
 */
var updateDefinition = function (definition) {
    if (definition.type == "RAW" && !definition.source_type) return _updateRawSlice(definition);
    if (definition.type == "PIE") return _updatePie(definition);
    if (definition.type == "SERIES" && !definition.spec) return _updateSeries(definition);
    return definition;
};

var _updateSeries = function (definition) {
    var def = ewars.copy(definition);

    def.spec = 2;
    def.interval = def.timeInterval;
    if (def.timeInterval) delete def.timeInterval;
    if (def.threed) delete def.threed;
    if (def.threed_alpha) delete def.threed_alpha;
    if (def.threed_beta) delete def.threed_beta;
    if (def.threed_depth) delete def.threed_depth;
    if (def.threed_view_distance) delete def.threed_view_distance;

    _.each(def.series, function (ddef, index) {
        if (ddef.type == "SERIES_COMPLEX") {
            def.series[index].source_type = "SERIES_COMPLEX";
        } else {
            def.series[index].source_type = "SERIES";
            def.series[index].indicator = _or(ddef.indicatorUUID, ddef.indicator);
            def.series[index].location = _or(ddef.locationUUID, ddef.location_id, ddef.location)
        }

    }, this);

    return def;
};

var _updatePie = function (definition) {

    var newDef = ewars.copy(definition);
    newDef.type = "CATEGORY";

    _.each(newDef.slices, function (slice, index) {
        var newSlice;
        if (slice.dataType == "SLICE") {
            newSlice = {
                title: slice.title || "New Slice",
                type: "SLICE",
                colour: slice.colour || null,
                loc_spec: slice.loc_spec || "REPORT_LOCATION",
                generator_locations_parent: slice.generator_locations_parent || null,
                generator_locations_type: slice.generator_locations_type || null,
                generator_location_status: slice.generator_location_status || null,
                generator_category_label: slice.generator_category_label || null,
                location: _or(slice.location, slice.location_id, slice.locationUUID),
                indicator: _or(slice.indicator, slice.indicator_id, slice.indicatorUUID),
                source_type: "SLICE",
                formula: slice.formula || null,
                series: slice.series || [],
                reduction: slice.reduction || "SUM"
            };
            newDef.slices[index] = newSlice;
        }
    }, this);

    return newDef;
};

var _updateRawSlice = function (definition) {
    var newDef = {
        type: "RAW",
        title: definition.title || {en: "Raw Widget"},
        format: _or(definition.format, definition.series[0].format),
        indicator_id: _or(definition.series[0].indicator, definition.series[0].indicatorUUID, definition.series[0].indicator_id),
        start_date_spec: definition.start_date_spec,
        end_date_spec: definition.end_date_spec,
        loc_spec: definition.series[0].loc_spec || "REPORT_LOCATION",
        start_date: definition.series[0].start_date || null,
        end_date: definition.series[0].end_date || null,
        source_type: "SLICE",
        reduction: definition.series[0].reduction,
        start_intervals: definition.start_intervals,
        end_intervals: definition.end_intervals,
        locationUUID: _or(definition.series[0].location, definition.series[0].location_id, definition.series[0].locationUUID)
    };

    // Need to fix location
    if (!newDef.locationUUID) {

    }

    // Need to detect if this is a complex slice
    if (definition.series[0].dataType == "SLICE_COMPLEX") {
        // This is a complex slice
        newDef.source_type = 'SLICE_COMPLEX';
        newDef.series = definition.series[0].series;

        _.each(newDef.series, function (series, index) {
            var source = ewars.copy(series);
            if (!source.reduction) source.reduction = "SUM";
            if (source.title) delete source.title;
            if (source.type) delete source.type;
            if (source.colour) delete source.colour;
            if (source.location) delete source.location;
            if (source.dataType) delete source.dataType;
            newDef.series[index] = source;
        }, this);

        newDef.formula = definition.series[0].formula;
    }

    return newDef;
};

module.exports = {
    isLegacy: isLegacy,
    updateDefinition: updateDefinition
};