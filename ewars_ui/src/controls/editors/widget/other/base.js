const ALERT_CONDITIONS = {
    application: "any",
    rules: [
        ["type", "eq", "ALERTS"],
        ["type", "eq", "ALERTS_MAP"],
        ['type', 'eq', 'ALERTS_LIST']
    ]
};

const WORKLOAD_CONDITIONS = {
    application: "any",
    rules: [
        ["type", "eq", "OVERDUE"],
        ["type", "eq", "UPCOMING"]
    ]
};

const BASE_OPTIONS = {
    t_header: {
        type: "header",
        label: "Widget Settings"
    },
    type: {
        type: "hidden"
    },
    title: {
        type: "text",
        label: "Widget Title"
    },
    icon: {
        type: "text",
        label: "Widget Icon"
    },
    headerColor: {
        type: "text",
        label: "Header Colour"
    },
    headerTextColor: {
        type: "text",
        label: "Header Text Colour"
    },

    // Metrics Widget Layout
    metric_layout: {
        type: "button_group",
        label: "Layout",
        options: [
            ["BAR", "Bar"],
            ["TABLE", "Table"]
        ],
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ["type", "eq", "METRICS"]
            ]
        }
    },
    metrics_items: {
        type: "select",
        multiple: true,
        label: "Selected Metrics",
        options: [
            ["SUBMISSIONS", "Form Submissions"],
            ["ASSIGNMENTS", "Assignments"],
            ["REPORTING_LOCATIONS", "Reporting Locations"],
            ["LOCATIONS", "Locations Total"],
            ["USERS", "Users"],
            ["ALERTS_OPEN", "Alerts Open"],
            ["ALERTS_TOTAL", "Alerts Triggered"],
            ["ALERTS_CLOSED", "Alerts Closed"],
            ["FORMS", "Forms"],
            ["DEVICES", "Devices"],
            ["TASKS_OPEN", "Tasks Open"],
            ["TASKS_UNACTIONED", "Tasks Unactioned"],
            ["TASKS_TOTAL", "Tasks Total"],
            ["COMPLETENESS", "Completeness"],
            ["TIMELINESS", "Timeliness"],
            ["DOCUMENTS", "Documents Total"],
            ["PARTNERS", "Organizations"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["type", 'eq', 'METRICS']
            ]
        }
    },

    content: {
        type: "textarea",
        label: "Content",
        i18n: false,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "TEXT"]
            ]
        }
    },

    // Alerts List and Alerts Map Options
    alarm_id: {
        type: "select",
        label: "Specific Alarm (Optional)",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSouce: "name",
            query: {}
        },
        conditional_bool: true,
        conditions: ALERT_CONDITIONS
    },
    alert_state: {
        type: "button_group",
        label: "Alert State (Optional)",
        options: [
            ["OPEN", "Active"],
            ["CLOSED", "Closed"]
        ],
        conditional_bool: true,
        conditions: ALERT_CONDITIONS
    },
    alert_stage: {
        type: "select",
        label: "Alert Stage",
        options: [
            ["VERIFICATION", "Verification"],
            ["RISK_ASSESS", "Risk Assessment"],
            ["RISK_CHAR", "Risk Characterization"],
            ["OUTCOME", "Outcome"]
        ],
        conditional_bool: true,
        conditions: ALERT_CONDITIONS
    },
    alert_stage_state: {
        type: "button_group",
        label: "Alert Stage State",
        options: [
            ["PENDING", "Pending"],
            ["ACTIVE", "Active"],
            ["COMPLETED", "Completed"]
        ],
        conditional_bool: true,
        conditions: ALERT_CONDITIONS
    },
    alert_risk: {
        type: "button_group",
        label: "Alert Risk",
        options: [
            ["LOW", "Low Risk"],
            ["MODERATE", "Moderate Risk"],
            ["HIGH", "High Risk"],
            ["SEVERE", "Very High Risk"]
        ],
        conditional_bool: true,
        conditions: ALERT_CONDITIONS
    },

    // Overdue Reports
    overdue_form_id: {
        type: "select",
        label: "Specific Form (Optional)",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            }
        },
        conditional_bool: true,
        conditions: WORKLOAD_CONDITIONS
    },
    image_url: {
        type: "text",
        label: "Image URL",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "IMAGE"]
            ]
        }
    },
    image_link: {
        type: "text",
        label: "Image link",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "IMAGE"]
            ]
        }
    }
};

export default BASE_OPTIONS;