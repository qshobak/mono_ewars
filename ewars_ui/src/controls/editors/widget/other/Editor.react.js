var Settings = require("./SettingsView.react");

var Editor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onChange: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {
        return (
            <div className="ide-layout" style={{background: "#333"}}>
                <div className="ide-row">
                    <div className="ide-col">
                        <Settings
                            definition={this.props.definition}
                            data={this.props.data}
                            onChange={this._onChange}/>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Editor;