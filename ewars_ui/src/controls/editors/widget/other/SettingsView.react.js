var Form = require("../../Form.react");

import BASE_OPTIONS from "./base";

const TYPE_OPTIONS = [
    'ACTIVITY',
    "TEXT",
    "ALERTS_LIST",
    "ALERTS_MAP",
    "METRICS_OVERALL",
    "DOCUMENTS",
    "ASSIGNMENTS",
    "METRICS",
    "OVERDUE",
    "UPCOMING",
    "IMAGE",
    "NOTIFICATIONS",
    "DELETIONS",
    "AMENDMENTS",
    "TASKS",
    "PERIOD"
];

class Option extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        let className = 'block';
        if (this.props.data == this.props.item.type) className += ' active';

        let style = {};
        if (this.props.data == this.props.item.type) style.background = 'rgba(0,0,0,0.1)';

        return (
            <div className={className} onClick={this._onClick} style={style}>
                <div className="block-content active">{_l(this.props.data)}</div>
            </div>
        )
    }
}

var SettingsView = React.createClass({
    displayName: "Raw Widget Settings",

    getInitialState: function () {
        return {
            errors: []
        }
    },

    _update: function (data, prop, value) {
        this.props.onChange(prop, value);
    },

    _onSelect: function (type) {
        this.props.onChange("type", type);
    },

    render: function () {
        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col border-right" style={{maxWidth: 300}}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">

                            <div className="block-tree">
                                {TYPE_OPTIONS.map(item => {
                                    return <Option
                                        item={this.props.data}
                                        onClick={this._onSelect}
                                        data={item}/>
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="ide-settings-content">
                            <div className="ide-settings-content">
                                <div className="ide-settings-form">
                                    <div className="chart-edit">
                                        <Form
                                            definition={BASE_OPTIONS}
                                            data={this.props.data}
                                            updateAction={this._update}
                                            readOnly={false}
                                            errors={this.state.errors}/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = SettingsView;