const Moment = require("moment");

const DEFAULTS = {
    type: "MAPPING",
    uuid: null,
    layers: [],
    annotations: []
};

export default DEFAULTS;
