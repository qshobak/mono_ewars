const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

module.exports = {
    general_header: {
        type: "header",
        label: {
            en: "General Settings"
        }
    },
    title: {
        type: "language_string",
        label: {
            en: "Chart Title"
        }
    },
    interval: {
        type: "button_group",
        label: {
            en: "Sample Interval"
        },
        options: [
            ["DAY", "Daily"],
            ["WEEK", "Weekly"],
            ["MONTH", "Monthly"],
            ["YEAR", "Annually"]
        ],
        required: true,
        help: {
            en: "The interval at which to aggregate the data in the series"
        }
    },
    period: {
        type: "date_range",
        label: "Period"
    },
    controls_header: {
        type: "header",
        label: {
            en: "Controls"
        }
    },
    show_title: {
        type: "button_group",
        options: [
            [true, "On"],
            [false, "Off"]
        ],
        label: {
            en: "Chart Title"
        }
    },
    show_legend: {
        type: "button_group",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ],
        label: {
            en: "Legend"
        }
    },
    legend_position: {
        type: "button_group",
        label: {
            en: "Legend Position"
        },
        options: [
            ["top", "Top"],
            ["right", "Right"],
            ["bottom", "Bottom"],
            ["left", "Left"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["show_legend", 'eq', true]
            ]
        }
    },
    style: {
        type: "button_group",
        label: {
            en: "Chart Style"
        },
        options: [
            ["DEFAULT", "Default"],
            ["DONUT", "Donut"],
            ["ARC", "Arc"],
            ["BAR", "Bar Graph"]
        ]
    },
    slice_ordering: {
        type: "select",
        label: {
            en: "Slice Ordering"
        },
        options: [
            ["NONE", "None"],
            ["CAT_NAME", "Category Title Ascending"],
            ["CAT_NAME_DESC", "Category Title Descending"],
            ["VALUE_ASC", "Value Ascending"],
            ["VALUE_DESC", "Value Descending"]
        ]
    },
    y_axis_label: {
        type: "text",
        label: {
            en: "Y-Axis Label"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["style", "eq", "BAR"]
            ]
        }
    },
    y_axis_format: {
        type: "select",
        label: {
            en: "Y-Axis Format"
        },
        options: [
            ["0", "0"],
            ["0.00", "0.00"],
            ["0%", "0%"],
            ["0.00%", "0.00%"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["style", "eq", "BAR"]
            ]
        }
    },
    y_axis_allow_decimals: {
        type: "button_group",
        label: {
            en: "Y axis decimals"
        },
        options: [
            [true, "Show"],
            [false, "Hide"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["style", "eq", "BAR"]
            ]
        }
    },
    t_style: {
        type: "header",
        label: "Style"
    },
    width: {
        type: "dom_size",
        label: {
            en: "Width"
        }
    },
    height: {
        type: "dom_size",
        label: {
            en: "Height"
        }
    },
    advanced_header: {
        type: "header",
        label: {
            en: "Advanced Options"
        }
    },
    hide_no_data: {
        type: "button_group",
        label: {
            en: "Slices with no data"
        },
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    t_widget: {
        type: "header",
        label: _l("DASHBOARD_SETTINGS"),
        conditional_bool: true,
        conditions: modeConditions
    },
    icon: {
        type: "text",
        label: _l("HEADER_ICON"),
        conditional_bool: true,
        conditions: modeConditions
    },
    headerColor: {
        type: "text",
        label: _l("HEADER_COLOUR"),
        conditional_bool: true,
        conditions: modeConditions
    },
    headerTextColor: {
        type: "text",
        label: _l("HEADER_TEXT_COLOUR"),
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetBackgroundColor: {
        type: "text",
        label: _l("HEADER_BACKGROUND_COLOUR"),
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetTextColor: {
        type: "text",
        label: _l("HEADER_TEXT_COLOUR"),
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "Widget footer text",
        conditional_bool: true,
        conditions: modeConditions
    }
};