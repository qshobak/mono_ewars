/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

module.exports = {
    t_general: {
        type: "header",
        label: "General"
    },
    title: {
        type: "language_string",
        label: {
            en: "Slice Label"
        }
    },
    colour: {
        type: "color",
        label: {
            en: "Colour"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "button_group",
        label: {
            en: "Location Spec"
        },
        options: [
            ["SPECIFIC", "Specific Location"],
            ["GENERATOR", "Generator"],
            ["GROUP", "Location Group(s)"]
        ]
    },
    group_ids: {
        type: "location_group",
        label: "Location Group(s)",

        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    group_output: {
        type: "button_group",
        label: "Group Output",
        help: "Aggregate: A single slice is produced aggregating the locations data together, Individual: Individual slices are produced for each location in the group",
        options: [
            ["AGGREGATE", "Aggregate"],
            ["INDIVIDUAL", "Individual"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    generator_locations_parent: {
        type: "location",
        label: {
            en: "Generator parent location"
        },
        hideInactive: true,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_locations_type: {
        type: "select",
        label: {
            en: "Generator location type"
        },
        optionsSource: {
            resource: "location_type",
            labelSource: "name",
            valSource: "id",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_location_status: {
        type: "select",
        label: {
            en: "Location Status"
        },
        options: [
            ["ANY", "Any"],
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_category_label: {
        type: "SELECT",
        label: {
            en: "Category Label Source"
        },
        options: [
            ["IND_LOC_VALUE", "Indicator - Location - Value"],
            ["IND_LOC", "Indicator - Location"],
            ["LOC", "Location"]
        ]
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    ind_spec_header: {
        type: "header",
        label: "Data Source"
    },
    source_type: {
        type: "button_group",
        label: "Source Type",
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    indicator: {
        type: "indicator",
        required: true,
        label: {
            en: "Indicator Source"
        },
        help: {
            en: "The data to use"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {
            en: "Variables"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    reduction: {
        type: "button_group",
        label: {
            en: "Reduction Type"
        },
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average/Median"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    override: {
        type: "button_group",
        label: "Period",
        options: [
            ["INHERIT", "Inherit"],
            ["OVERRIDE", "Override"]
        ]
    },
    period: {
        type: "date_range",
        label: false,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["override", "eq", "OVERRIDE"]
            ]
        }
    }
};