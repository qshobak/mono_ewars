
var Form = require("../../Form.react");
var DEFAULTS = require("./defaults_widget");
var formWidget = require("./form_widget");

var SettingsView = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _update: function (data, prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {

        this.props.data.mode = this.props.mode;

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-padded">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <div className="chart-edit">
                            <Form
                                definition={formWidget}
                                data={this.props.data}
                                updateAction={this._update}
                                readOnly={false}
                                errors={this.state.errors}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = SettingsView;