
var CONSTANTS = require("../../../constants");

var VertTab = require("../../ide/VertTab.react");

var VIEWS = {
    SETTINGS: "SETTINGS",
    DATA: "DATA",
    SOURCE: "SOURCE",
    PREVIEW: "PREVIEW"
};

var Settings = require("./SettingsView.react");
var JSONEditor = require("../_shared/JSONEditor.react");

class MapEditor extends React.Component {
    static defaultProps = {
        mode: "DEFAULT"
    };

    constructor(props) {
        super(props);

        this.state = {
            view: "SETTINGS"
        }
    }

    _onChange = (data, prop, value) => {
        console.log(data, prop, value);
        this.props.onChange(prop, value);
    };

    _changeShoulder = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        var view;
        if (this.state.view == VIEWS.SETTINGS) {
            view = <Settings
                data={this.props.data}
                mode={this.props.mode}
                onChange={this._onChange}/>;
        }
        if (this.state.view == VIEWS.SOURCE) view = <JSONEditor data={this.props.data} onSave={this.props.onJSONSave}/>;

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col ide-relative" style={{maxWidth: "34px"}}>
                        <div className="ide-panel">
                            <div className="ide-tabs">
                                <VertTab
                                    view={VIEWS.SETTINGS}
                                    label="Settings"
                                    icon="fa-globe"
                                    active={this.state.view == VIEWS.SETTINGS}
                                    onClick={this._changeShoulder}/>
                                <VertTab
                                    view={VIEWS.SOURCE}
                                    label="Source"
                                    icon="fa-code"
                                    active={this.state.view == VIEWS.SOURCE}
                                    onClick={this._changeShoulder}/>
                            </div>
                        </div>
                    </div>

                    <div className="ide-col">
                        {view}
                    </div>
                </div>
            </div>
        )
    }
}

export default MapEditor;