var Form = require("../../Form.react");
import DEFAULTS from "./defaults_widget";
var formWidget = require("./form_widget");

class Thresholds extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dummy"></div>
        )
    }
}

class SettingsView extends React.Component {
    static defaultProps = {
        mode: "DEFAULT"
    };

    constructor(props) {
        super(props);

        this.state = {
            errors:[]
        }
    }

    _update = (data, prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        if (window.user.role != "SUPER_ADMIN") {
            formWidget.location.parentId = window.user.account.location_id;
        }

        this.props.data.mode = this.props.mode;

        if (ewars.TEMPLATE_MODE == "TEMPLATE") {
            formWidget.loc_spec = {
                type: "button_group",
                label: {
                    en: "Location Spec"
                },
                options: [
                    ["SPECIFIC", "Specific Location"],
                    ["REPORT_LOCATION", "Report Location"]
                ]
            }
        }

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-padded">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <br/>
                        <div className="chart-edit">
                            <Form
                                definition={formWidget}
                                data={this.props.data}
                                updateAction={this._update}
                                readOnly={false}
                                errors={this.state.errors}/>

                            <div className="form-header">Thresholds</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


}

export default SettingsView;
