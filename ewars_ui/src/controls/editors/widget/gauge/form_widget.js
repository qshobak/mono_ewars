const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

module.exports = {
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "button_group",
        label: {
            en: "Location"
        },
        options: [
            ["SPECIFIC", "Specific Location"]
        ]
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    data_source_header: {
        type: "header",
        label: {en: "Data Source"}
    },
    source_type: {
        type: "button_group",
        label: {
            en: "Source Type"
        },
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {en: "Variables"},
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator"
        },
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    reduction: {
        type: "select",
        label: {
            en: "Reduction"
        },
        options: [
            ["NONE", "None"],
            ["SUM", "Sum"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    start_date_header: {
        type: "header",
        label: "Data Query Period"
    },
    interval: {
        type: "select",
        label: "Time Interval",
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ]
    },
    period: {
        type: "date_range",
        label: "Query Range"
    },
    t_thresholds: {
        type:"header",
        label: "Thresholds"
    },
    ranges: {
        type: "text",
        label: "Ranges"
    },
    colours: {
        type: "text",
        label: "Colours"
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    format: {
        type: "select",
        label: {
            en: "Number Formatting"
        },
        options: [
            ["0", "0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0,0.0", "0,0.0"],
            ["0,0.00", "0,0.00"],
            ["0,0", "0,0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]

        ]
    },
    t_widget: {
        type: "header",
        label: "Widget settings",
        conditions: modeConditions
    },
    widgetBaseColor: {
        type: "text",
        label: "Base colour",
        conditions: modeConditions
    },
    widgetHighlightColor: {
        type: "text",
        label: "Highlight colour",
        conditions: modeConditions
    },
    widgetIcon: {
        type: "text",
        label: "Icon",
        conditions: modeConditions
    },
    widgetSubText: {
        type: "text",
        label: "Widget sub-text",
        conditions: modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "Widget footer text"
    }
};