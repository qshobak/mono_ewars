module.exports = {
    // General
    title: {en: "New Series"},
    interval: "DAY",
    type: "SERIES",
    source_type: "SERIES",
    override: "INHERIT",
    period: [],
    style: "line",
    line_width: 0.25,
    marker_radius: 2,
    marker_symbol: "circle",
    series: [],
    loc_spec: "SPECIFIC"
};