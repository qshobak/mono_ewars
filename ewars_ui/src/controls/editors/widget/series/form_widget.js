const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

module.exports = {
    general_header: {
        type: "header",
        label: {
            en: "General settings"
        }
    },
    title: {
        type: "language_string",
        label: {
            en: "Chart title"
        },
        required: true
    },
    interval: {
        type: "button_group",
        label: {
            en: "Aggregation"
        },
        options: [
            ["DAY", "Daily"],
            ["WEEK", "Weekly"],
            ["MONTH", "Monthly"],
            ["YEAR", "Annually"]
        ],
        required: true,
        help: {
            en: "The interval at which to aggregate the data in the series"
        }
    },
    start_date_header: {
        type: "header",
        label: {
            en: "Data Period"
        }
    },
    period: {
        type: "date_range",
        label: "Period"
    },
    controls_header: {
        type: "header",
        label: {
            en: "Controls"
        }
    },
    show_title: {
        type: "button_group",
        label: "Title",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    tools: {
        type: "button_group",
        label: "Export",
        defaultValue: false,
        options: [
            [true, "Show"],
            [false, "Hide"]
        ],
        help: "Show export button for exporting images, pdf, etc..."
    },
    zoom: {
        type: "button_group",
        label: "Zoom",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ],
        defaultValue: false,
        help: "Allow zooming into the chart by click dragging"
    },
    navigator: {
        type: "button_group",
        label: "Navigator",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ],
        defaultValue: false,
        help: "Show the timeline navigator below the chart"
    },
    show_legend: {
        type: "button_group",
        label: "Legend",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    t_y_axis: {
        type: "header",
        label: {en: "Y Axis"}
    },
    hide_y_title: {
        type: "button_group",
        options: [
            [true, "Hide"],
            [false, "Show"]
        ],
        label: "Y axis title"
    },
    y_axis_label: {
        type: "text",
        label: {
            en: "Y-axis title"
        }
    },
    y_axis_format: {
        type: "select",
        label: {
            en: "Y-axis format"
        },
        options: [
            ["0", "0"],
            ["0,0", "0,0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]
        ]
    },
    y_axis_allow_decimals: {
        type: "button_group",
        label: "Decimals",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    max_y_value: {
        label: {
            en: "Max. Y value"
        },
        decimal_allowed: true,
        allow_negative: true,
        type: "number"
    },
    y_tick_interval: {
        label: {
            en: "Y axis interval"
        },
        decimal_allowed: true,
        allow_negative: true,
        type: "number"
    },
    t_x_axis: {
        type: "header",
        label: "X axis"
    },
    hide_x_labels: {
        type: "button_group",
        label: "X axis labels",
        options: [
            [true, "Hide"],
            [false, "Show"]
        ]
    },
    x_label_rotation: {
        type: "text",
        label: {
            en: "X axis label rotation"
        }
    },
    t_style: {
        type: "header",
        label: "Style"
    },
    width: {
        type: "dom_size",
        label: {
            en: "Chart width (px)"
        }
    },
    height: {
        type: "dom_size",
        label: {
            en: "Chart height (px)"
        }
    },
    stack: {
        type: "button_group",
        label: "Stacked",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ]
    },
    t_widget_style: {
        type: "header",
        label: "Widget styling",

        conditional_bool: true,
        conditions: modeConditions
    },
    icon: {
        type: "text",
        label: "Header icon",
        conditional_bool: true,
        conditions: modeConditions
    },
    headerColor: {
        type: "text",
        label: "Header colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    headerTextColor: {
        type: "text",
        label: "Header text tolour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetBackgroundColor: {
        type: "text",
        label: "Widget background colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetTextColor: {
        type: "text",
        label: "Widget text colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetSubText: {
        type: "text",
        label: "Widget sub-text",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "widget footer text",
        conditional_bool: true,
        conditions: modeConditions
    }

};