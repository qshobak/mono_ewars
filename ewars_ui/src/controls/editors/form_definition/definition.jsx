import React from "react";

import FieldDefinition from "./definition.field.jsx";

import FormUtils from "../../../utils/form_utils";
import Form from "../../forms/form.reporting.jsx";
import {utils} from "../../../utils/utils";
import ActionGroup from "../../c.actiongroup.jsx";

const ACTIONS = [
    ['fa-plus', 'ADD', 'Add field'],
    ['fa-eye', 'PREVIEW', 'Preview']
]

const ewars = {
    copy: function (data) {
        return JSON.parse(JSON.stringify(data))
    },
    uuid: function () {
        return utils.uuid();
    }
}


function _deleteField(uuid, sourceData) {
    _iterateChildren(sourceData, function (name, child, items) {
        if (child.uuid == uuid) {
            delete items[name];
        }
    })
}

function _duplicateField(uuid, sourceData) {
    let field;

    _iterateChildren(sourceData, function (name, child, items) {
        if (child.uuid == uuid) {
            let newField = JSON.parse(JSON.stringify(items[name]));
            let uuid = utils.uuid();
            let newName = "NEW_" + utils.uuid().split("-")[0];
            newField.uuid = uuid;
            newField.label = newField.label || {en: ""};
            newField.order = 99999999;
            items[newName] = newField;

            // Iterate nested cells
            if (newField.type == "row") {
                for (var i in newField.fields) {
                    newField.fields[i].uuid = utils.uuid();
                }
            }

            // iterate matrix children down to cells and update uuids
            if (newField.type == "matrix") {
                for (var n in newField.fields) {
                    let subField = newField.fields[n];
                    newField.fields[n].uuid = utils.uuid();

                    for (var t in subField.fields) {
                        subField.fields[t].uuid = utils.uuid();
                    }
                }
            }


            var arrayed = _getFieldsAsArray(items);
            arrayed.sort(_comparator);
            items = _getArrayAsObject(arrayed);
        }
    })
}

const grouper = (data) => {

}

function _findParentNode(parentName, childObj) {
    var testObj = childObj.parentNode;
    var count = 1;
    while (testObj.getAttribute('name') != parentName) {
        alert("My name is " + testObj.getAttribute("name") + ". Lets try moving up one level");
        testObj = testObj.parentNode;
        count++;
    }
}

function _normalize(sourceFields) {
    sourceFields.forEach(field => {
        if (!field.uuid) field.uuid = utils.uuid();
        if (field.fields) _normalize(field.fields);
    });
}

function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields, parentId) {
    var fields = [];
    for (let token in sourceFields) {
        let field = sourceFields[token];
        let fieldCopy = JSON.parse(JSON.stringify(field));
        fieldCopy.name = token;
        fieldCopy.parentId = parentId;
        if (!fieldCopy.uuid) fieldCopy.uuid = utils.uuid();

        if (fieldCopy.fields) {
            var children = _getFieldsAsArray(fieldCopy.fields, fieldCopy.uuid);
            fields.push.apply(fields, children);
        }

        delete fieldCopy.fields;

        fields.push(fieldCopy);
    }
    ;

    return fields.sort(_comparator);
}

function _getArrayAsObject(sourceArray) {
    var fieldDict = {};
    var resultDict = {};

    sourceArray.forEach(item => {

        if (fieldDict[item.uuid]) {
            item.uuid = utils.uuid();
        }
        fieldDict[item.uuid] = item;
    });

    for (let i in fieldDict) {
        let item = fieldDict[i];
        var parent = fieldDict[item.parentId];
        if (parent) {
            parent.fields = parent.fields || {};
            parent.fields[item.name] = item;
        } else {
            resultDict[item.name] = item;
        }
    }

    return resultDict;
}

function _resort(parentId, sourceData) {
    var toSort = [];
    var fields = _getFieldsAsArray(sourceData);

    var fieldDict = {};
    fields.forEach(item => {
        fieldDict[item.uuid] = item;
    });

    fields.forEach(item => {
        if (item.parentId == parentId) toSort.push(item);
    });

    var sortedResult = toSort.sort(_comparator);

    var count = 0;
    sortedResult.forEach(item => {
        item.order = count;
        fieldDict[item.uuid] = item;
        count++;
    });

    var result = _getArrayAsObject(_.map(fieldDict, function (item) {
        return item;
    }, this));

    return result;
}

/**
 * Fixes numbering issues
 * @param items {array}
 * @returns asArray {array}
 */
function _fixNumbering(items) {
    var asArray = _getFieldsAsArray(items);

    let groups = {};
    asArray.forEach(item => {
        if (groups[item.parentId]) {
            groups[item.parentId].push(item);
        } else {
            groups[item.parentId] = [item]
        }
    });

    let grouped = [];

    for (let i in groups) {
        grouped.push(groups[i]);
    }
    var result = [];

    grouped.forEach(group => {

        group.sort(_comparator);
        var count = 0;
        _.each(group, function (item) {
            item.order = count;
            result.push(item);
            count++;
        }, this)
    });

    return _getArrayAsObject(result)
}

const DROPPABLE_TYPES = [
    ["text", "Text field"],
    ['date', 'Date field'],
    ['location', 'Location field'],
    ['number', 'Numeric field'],
    ['header', 'Header field'],
    ['textarea', 'Textarea field'],
    ['select', 'Select field'],
    ['matrix', 'Table'],
    ['display', 'Display field'],
    ['lat_long', 'Lat/Lng field'],
    ['repeater', 'Repeater'],
    ['calculation', 'Calculation'],
    ['associated', 'Associated'],
    ['risk', 'Risk'],
    // ["repeater", "Repeater"],
    // ['progress', "Progress bar"],
    // ['image', 'Image upload'],
    // ['file', 'File upload']
];

const ICONS = {
    text: "fa-font",
    textarea: "fa-align-left",
    date: "fa-calendar",
    display: "fa-square",
    number: "fa-hashtag",
    location: 'fa-map-marker',
    lat_long: "fa-location-arrow",
    header: "fa-heading",
    matrix: "fa-th",
    row: "fa-rectangle-wide",
    select: "fa-list-alt",
    repeater: "fa-list",
    calculation: "fa-calculator",
    associated: "fa-link"
    // repeater: "fa-copy",
    // image: "fa-image",
    // progress: "fa-spinner",
    // file: "fa-file"
};


class FormDefinitionComponent extends React.Component {
    _tmpNumber = 0;

    state = {
        fields: {},
        definition: {},
        view: "edit",
        dummyForm: {
            data: {}
        }
    };

    static defaultProps = {
        definition: {},
        onChange: () => {
        }
    };

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.state.definition = _fixNumbering(ewars.copy(this.props.definition));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.definition) {
            this.state.definition = _fixNumbering(ewars.copy(nextProps.definition));
        }
    }

    /**
     * Handles adding a new field to the definition
     * @private
     */
    _addField = () => {
        var id = ewars.utils.uuid();
        var name = "untitled_" + id.split("-")[0];

        var high = -1;

        _.each(this.state.definition, function (item) {
            if (item.order > high) high = item.order;
        }, this);

        var field = {
            name: name,
            uuid: id,
            label: {en: "New Field"},
            type: "text",
            order: high + 1,
            required: false
        };

        this.state.definition[field.name] = field;
        _normalize(this.state.definition);
        this.props.onChange(this.state.definition);
    };

    _showPreview = () => {
        this.setState({
            view: "preview"
        })
    };

    _hidePreview = () => {
        this.setState({
            view: "edit"
        })
    };

    _onFieldChange = (fieldUUID, prop, value) => {
        console.log(prop, value);
        var arrFields = _getFieldsAsArray(this.state.definition);

        let collision = false;

        if (prop == "name") {
            let editing, conflict;
            // Find the field we're editing
            arrFields.forEach((field) => {
                if (field.uuid == fieldUUID) {
                    editing = field;
                    field.name = value;
                }
            });

            // Find any matches
            arrFields.forEach((field) => {
                if (field.name == value && field.uuid != fieldUUID) conflict = field;
            });

            if (conflict) {
                // there's a conflict, how do we deal with it
                // Prepend DUPE_; this is hidden in the editor
                editing.name = "DUPE_" + value;
            }
        } else {
            arrFields.forEach((field) => {
                if (field.uuid == fieldUUID) {
                    field[prop] = value;
                }
            })
        }
        var data = _getArrayAsObject(arrFields);
        _normalize(data);
        this.props.onChange(data);
    };

    _addChild = (uuid) => {
        var arrFields = _getFieldsAsArray(this.state.definition);

        var parent = arrFields.filter(item => {
            return item.uuid == uuid;
        })[0] || null;

        var newType = 'text';
        if (parent.type == "matrix") newType = "row";

        var id = ewars.utils.uuid();
        var name = "untitled_" + id.split("-")[0];

        // Get the highest ordered child
        var high = -1;
        arrField.forEach(arrField => {
            if (arrField.parentId == uuid) {
                if (parseInt(arrField.order) > high) high = parseInt(arrField.order);
            }
        });

        arrFields.push({
            name: name,
            label: {en: "New Field"},
            uuid: id,
            required: false,
            type: newType,
            order: high + 1,
            parentId: uuid
        });

        var data = _getArrayAsObject(arrFields);
        _normalize(data);
        this.props.onChange(data);
    };

    _onDeleteField = (uuid) => {
        _deleteField(uuid, this.state.definition);
        this.state.definition = _fixNumbering(this.state.definition);
        this.props.onChange(this.state.definition);
    };

    _onDuplicate = (uuid) => {
        _duplicateField(uuid, this.state.definition);
        this.state.definition = _fixNumbering(this.state.definition);
        this.props.onChange(this.state.definition);
    };

    _bigMove = (moving, target) => {
        let fields = _getFieldsAsArray(this.state.definition);
        let movingIndex,
            targetIndex;

        fields.forEach((item, index) => {
            if (item.uuid == moving) {
                movingIndex = index;
            }
            if (item.uuid == target) {
                targetIndex = index;
            }
        });

        let movingItem = fields.filter(item => {
            return item.uuid == moving;
        })[0] || null;

        fields = fields.sort(_comparator);

        let result = [];
        fields.forEach(item => {
            if (item.uuid != moving) {
                if (item.uuid == target) {
                    result.push(item);
                    result.push(movingItem);
                } else {
                    result.push(item);
                }
            }
        })

        let counter = 0;
        result.forEach(item => {
            item.order = counter;
            counter++;
        })

        this.state.definition = _getArrayAsObject(result);
        this.props.onChange(this.state.definition);
    };

    _dropNew = (data, target) => {
        let fields = _getFieldsAsArray(this.state.definition);

        fields = fields.sort(_comparator);

        data.order = 0;
        let _s = data.uuid.split("-");
        data.name = "untitled_" + _s[_s.length - 1];

        let result = [];

        if (target) {
            fields.forEach(item => {
                result.push(item);
                if (item.uuid == target) {
                    result.push(data);
                }
            });
        } else {
            result = fields;
            result.push(data);
        }

        let counter = 0;
        result.forEach(item => {
            item.order = counter;
            counter++;
        });

        this.state.definition = _getArrayAsObject(result);
        this.props.onChange(this.state.definition);
    };

    _moveFieldUp = (field, direction) => {
        var fields = _getFieldsAsArray(this.state.definition);
        let targetField = fields.filter(item => {
            return item.uuid == uuid;
        })[0] || null;
        var groups = _.groupBy(fields, "parentId");

        var targetGroup = groups[targetField.parentId];
        targetGroup.sort(_comparator);

        // Get its current indev
        var curIndex = null;
        _.each(targetGroup, function (item, index) {
            if (item.uuid == field) curIndex = index;
        });

        if (curIndex <= 0) return;

        var targetIndex = curIndex - 1;
        var tmp = targetGroup[targetIndex];
        targetGroup[targetIndex] = targetField;
        targetGroup[curIndex] = tmp;

        _.each(targetGroup, function (item, index) {
            item.order = index;

            _.each(fields, function (field) {
                if (field.uuid == item.uuid) field.order = index;
            }, this)
        }, this);

        this.state.definition = _getArrayAsObject(fields);
        this.props.onChange(this.state.definition);
    };

    _moveFieldDown = (field, direction) => {
        var fields = _getFieldsAsArray(this.state.definition);
        var targetField = _.findWhere(fields, {uuid: field});
        var groups = _.groupBy(fields, "parentId");

        var targetGroup = groups[targetField.parentId];
        targetGroup.sort(_comparator);

        // Get its current indev
        var curIndex = null;
        _.each(targetGroup, function (item, index) {
            if (item.uuid == field) curIndex = index;
        });

        if (curIndex >= targetGroup.length - 1) return;

        var targetIndex = curIndex + 1;
        var tmp = targetGroup[targetIndex];
        targetGroup[targetIndex] = targetField;
        targetGroup[curIndex] = tmp;

        _.each(targetGroup, function (item, index) {
            item.order = index;

            _.each(fields, function (field) {
                if (field.uuid == item.uuid) field.order = index;
            }, this)
        }, this);

        this.state.definition = _getArrayAsObject(fields);
        this.props.onChange(this.state.definition);
    };

    _handleDummyFormUpdate = (data, prop, value) => {
        var data = this.state.dummyForm;
        // ewars.setKeyPath(data, prop, value);
        this.setState({
            dummyForm: data
        })
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = e.dataTransfer.getData("e");
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New field",
                required: false,
                // uuid: ewars.utils.uuid()
            }
            this._dropNew(newField, null);
        }
    };

    render() {
        var formFields = FormUtils.asOptions(this.state.definition);

        let fields = [];
        let fieldSet = [];

        for (let i in this.state.definition) {
            fieldSet.push({
                ...this.state.definition[i],
                name: i
            })
        }
        fieldSet = fieldSet.sort(_comparator);

        let _dragOver, _onDrop;
        if (fieldSet.length > 0) {
            fields = fieldSet.map((field, index) => {
                return (
                    <FieldDefinition
                        fieldOptions={formFields}
                        data={field}
                        order={index + 1}
                        isRoot={true}
                        bigMove={this._bigMove}
                        dropNew={this._dropNew}
                        moveFieldUp={this._moveFieldUp}
                        moveFieldDown={this._moveFieldDown}
                        onAddChild={this._addChild}
                        onChange={this._onFieldChange}
                        onDelete={this._onDeleteField}
                        onDuplicate={this._onDuplicate}
                        key={field.uuid}/>
                )
            })
        } else {
            _dragOver = this._onDragOver;
            _onDrop = this._onDrop;
            fields = (
                <div style={{textAlign: "center", marginTop: "100px", display: "block"}}>
                    <i style={{fontSize: "90px"}} className="fal fa-caret-circle-down"></i>
                    <div style={{fontSize: "14px", padding: "20px 0"}}>Drag a field here to start</div>
                </div>
            )
        }


        if (this.state.view == "edit") {
            return (
                <div className="row">
                    <div className="column panel br">
                        {DROPPABLE_TYPES.map(item => {
                            return (
                                <div
                                    onDragStart={(e) => {
                                        e.dataTransfer.setData("e", JSON.stringify({
                                            new: true,
                                            type: item[0]
                                        }))
                                        setTimeout(() => {
                                            // ewars.emit("SHOW_DROPS", null);
                                        })
                                    }}
                                    onDragEnd={function () {
                                        // ewars.emit("HIDE_DROPS");
                                    }}
                                    draggable={true}
                                    className="son-list-item"
                                    style={{display: "flex"}}>
                                    <div className="column" style={{maxWidth: "20px"}}>
                                        <i className={"fal " + ICONS[item[0]] || "fa-list"}></i>

                                    </div>
                                    <div className="column">{item[1]}</div>
                                </div>
                            )
                        })}

                    </div>
                    <div className="column">
                        <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                            <div className="column"></div>
                            <ActionGroup
                                actions={ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div
                            onDragOver={_dragOver}
                            onDrop={_onDrop}
                            className="row block block-overflow">
                            <div className="block-tree" style={{position: "relative"}}>
                                {fields}
                            </div>

                        </div>
                    </div>
                </div>
            )
        }

        if (this.state.view == "preview") {
            return (
                <div className="column">
                    <div className="row bb" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Preview</div>
                        <div className="column">
                            <button>Close Preview</button>
                        </div>
                    </div>
                    <div className="row block block-overflow">
                        <Form
                            readOnly={false}
                            updateAction={this._handleDummyFormUpdate}
                            definition={this.props.definition}
                            data={this.state.dummyForm}
                            onCancel={this._hidePreview}/>

                    </div>
                </div>
            )
        }

        return view;
    }
}

export default FormDefinitionComponent;

