import React from "react";

const FieldMapper = {};
// import FIELD_DEFINITION from "../../../common/constants/field_definition";
const FIELD_FORM = [];
import SystemForm from "../../forms/form.system.jsx";

const _statusMap = [
    ["DISABLED", "Disabled"],
    ["ENABLED", "Enabled"]
];

const _statusDict = {
    "DISABLED": "Disabled",
    "ENABLED": "Enabled"
};

const ICONS = {
    text: "fa-font",
    textarea: "fa-align-left",
    date: "fa-calendar",
    display: "fa-square",
    number: "fa-hashtag",
    location: 'fa-map-marker',
    lat_long: "fa-location-arrow",
    header: "fa-heading",
    matrix: "fa-th",
    row: "fa-rectangle-wide",
    select: "fa-list-alt"
};


function _getArrayAsObject(sourceArray) {
    var fields = {};

    for (var index in sourceArray) {
        var field = sourceArray[index];
        fields[field.name] = field;
        fields[field.name].order = index;

        if (field.fields) {
            fields[field.name].fields = _getArrayAsObject(field.fields);
        }

        delete fields[field.name].name;
    }

    return fields;
}

function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    var fields = [];
    for (var token in sourceFields) {
        var field = JSON.parse(JSON.stringify(sourceFields[token]));
        field.name = token;

        if (field.fields) {
            var children = _getFieldsAsArray(field.fields);
            field.fields = children;
        }

        fields.push(field);
    }

    return _.sortBy(fields, function (field) {
        return parseInt(field.order);
    })
}

class FieldDefinition extends React.Component {
    state = {
        showDetails: false
    }

    constructor(props) {
        super(props)
    }

    componentWillMount () {
        if (ewars.g.activeFields) {
            if (ewars.g.activeFields.indexOf(this.props.data.uuid) >= 0) {
                this.state.showDetails = true
            }
        }

        // ewars.subscribe("SHOW_DROPS", this._showDrops);
        // ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentWillUnmount() {
        // ewars.unsubscribe("SHOW_DROPS", this._showDrops);
        // ewars.unsubscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = (uuid) => {
        if (uuid != this.props.data.uuid) {
            if (this._dropEl) {
                this._dropEl.style.display = "block";
            }
        }
    };

    _hideDrops = () => {
        if (this._dropEl) {
            this._dropEl.style.display = "none";
        }
    };

    formatLabel = (labelValue) => {
        if (!labelValue) return "Untitled Field";
        if (labelValue.length >= 70) {
            return labelValue.substring(0, 70) + "...";
        }
        return labelValue;
    };

    onPropChange = (name, value) => {
        this.props.onChange(this.props.data.uuid, name, value);
    };

    _hasChildren = () => {
        var hasChildren = false;

        if (this.props.data.type == "row" || this.props.data.type == "matrix") {
            if (this.props.data.fields) {
                return [true, this.props.data.fields];
            } else {
                return [true, this.props.data.fields];
            }
        } else {
            return [false, []]
        }

        return [false, []]
    };

    _processChildren = () => {
        var fields;

        if (!this.props.data.fields) return [];
        var subFields = _.map(this.props.data.fields, function (field, key) {
            field.name = key;
            return field;
        }, this);

        var sortedFields = _.sortBy(subFields, function (field) {
            return parseInt(field.order);
        }, this);

        if (sortedFields.length > 0) {
            fields = _.map(sortedFields, function (item, index) {
                let order = index;
                if (this.props.order) order = `${this.props.order}.${index + 1}`
                return (
                    <FieldDefinition
                        fieldOptions={this.props.fieldOptions}
                        data={item}
                        parentName={item.name}
                        onDelete={this.props.onDelete}
                        onChange={this.props.onChange}
                        moveFieldUp={this.props.moveFieldUp}
                        moveFieldDown={this.props.moveFieldDown}
                        onAddChild={this.props.onAddChild}
                        onDuplicate={this.props.onDuplicate}
                        index={item.uuid}
                        order={order}
                        key={item.uuid}/>
                )
            }, this);
        } else {
            fields = (
                <p className="box-placeholder">There are currently no fields configured,
                    <a href="#" onClick={this.addChild}>Add one</a>
                </p>
            )
        }

        return fields;
    };

    addChild = () => {
        if (this.props.data.type == "row") {
            if (this.props.data.fields) {
                if (this.props.data.fields.length >= 6) {
                    ewars.notifications.notification("fa-warning", "Maximum Six", "A row can only contain a maximum of six items");
                    return;
                }
            }
        }
        this.props.onAddChild(this.props.data.uuid);
    };

    _action = (action) => {
        switch (action) {
            case "UP":
                this.props.moveFieldUp(this.props.data.uuid, -1);
                break;
            case "DOWN":
                this.props.moveFieldDown(this.props.data.uuid, 1);
                break;
            case "DUPE":
                this.props.onDuplicate(this.props.data.uuid);
                break;
            case "TOGGLE":
                ewars.g.activeFields = ewars.g.activeFields != null ? ewars.g.activeFields : [];
                if (ewars.g.activeFields.indexOf(this.props.data.uuid) < 0) {
                    ewars.g.activeFields.push(this.props.data.uuid);
                } else {
                    let idx = ewars.g.activeFields.indexOf(this.props.data.uuid);
                    ewars.g.activeFields.splice(idx, 1);
                }

                this.setState({
                    showDetails: this.state.showDetails ? false : true
                });
                break;
            case "DELETE":
                this.props.onDelete(this.props.data.uuid);
                break;
            case "ADD":
                this.addChild();
                break;
            default:
                break;
        }

    };

    _dragStart = (e) => {
        e.dataTransfer.setData("e", this.props.data.uuid);
        setTimeout(() => {
            // ewars.emit("SHOW_DROPS", this.props.data.uuid);
        }, 500)
    };

    _dragEnd = () => {
        // ewars.emit("HIDE_DROPS");
    };

    _drop = (e) => {
        e.preventDefault();

        let data = e.dataTransfer.getData("e");
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New field",
                required: false,
                uuid: ewars.utils.uuid()
            }
            this.props.dropNew(newField, this.props.data.uuid);
        } else {
            this.props.bigMove(data, this.props.data.uuid);
        }
    };

    render() {
        var children = [];
        var hasChildren = this._hasChildren();
        if (hasChildren[0]) children = this._processChildren();

        var labelShort = this.formatLabel(ewars.I18N(this.props.data.label));

        var fieldTypeName = FieldMapper._types[this.props.data.type];

        // var order = parseInt(this.props.data.order) + 1;
        let order = this.props.order;

        let showClass = "fal fa-square-plus";
        if (this.state.showDetails) showClass = "fa fa-square-minus";

        let name = this.props.data.name || this.props.index;

        let expander = this.state.showDetails ? "fa-minus-square" : "fa-plus-square";
        const ACTIONS = [
            ["fa-angle-up", "UP"],
            ["fa-angle-down", "DOWN"],
            ["fa-copy", "DUPE"],
            ["fa-trash", "DELETE"],
            [expander, "TOGGLE"]
        ];

        if (hasChildren[0]) {
            ACTIONS.push(["fa-plus", "ADD"])
        }

        let formDefinition;
        if (this.state.showDetails) {
            formDefinition = ewars.copy(FIELD_FORM);

            if (this.props.data.type == 'select') {
                formDefinition.forEach(item => {
                    if (item.n == 'default') {
                        item.t = 'SELECT';
                        item.o = this.props.data.options;
                    }
                })
            }

            formDefinition[formDefinition.length - 1].fo = this.props.fieldOptions;
        }

        let style = {
            padding: 0,
            background: "rgba(0,0,0,0.1)",
            border: "1px solid transparent"
        };
        if (this.state.showDetails) style.border = "1px solid #F2F2F2";

        let isRoot = this.props.isRoot;

        let style_inner = {padding: 0};
        if (this.props.data.type == 'header') style_inner.background = "rgb(60, 146, 122)";

        let icon = <i className={"fal " + ICONS[this.props.data.type] || "fa-cog"}></i>;

        return (
            <div
                className="block"
                style={style}>
                <div
                    onDragStart={this._dragStart}
                    onDragEnd={this._dragEnd}
                    draggable={isRoot}
                    className="block-content"
                    style={style_inner}>
                    <div className="row" style={{maxHeight: "24px"}}>
                        <div style={{maxWidth: "30px", display: "block", padding: '8px 0px 8px 5px'}}>
                            {order}
                        </div>
                        <div style={{maxWidth: "20px", display: "block", padding: "8px 0px 8px 5px", textAlign: "center"}}>
                            {icon}
                        </div>
                        <div className="column" style={{padding: "8px", display: "block"}}>
                            <strong>{labelShort}</strong> - [{name.replace("DUPE_", "")}]
                        </div>
                            <ActionGropu
                                actions={ACTIONS}
                                onAction={this._action}/>
                    </div>
                </div>
                {this.state.showDetails ?
                    <div style={{border: "1px solid #262626"}}>
                        <SystemForm
                            definition={formDefinition}
                            enabled={true}
                            onChange={this.onPropChange}
                            data={this.props.data}/>
                    </div>
                    : null}
                {hasChildren[0] ?
                    <div style={{border: "1px solid #262626"}}>
                        <div className="block" style={{position: "relative"}}>
                            {children}
                        </div>
                    </div>
                    : null}
                {isRoot ?
                    <div
                        ref={(el) => {
                            this._dropEl = el;
                        }}
                        style={{
                            display: "none"
                        }}
                        onDragOver={(e) => {
                            e.preventDefault();
                        }}
                        className="field-drop-area"
                        onDrop={this._drop}>Drop field here</div>
                    : null}
            </div>
        );
    }
};

export default FieldDefinition;
