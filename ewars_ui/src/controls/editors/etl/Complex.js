import Set from "./Set";

class Complex extends React.Component {
    constructor(props) {
        super(props);
    }

    _remove = () => {
        ewars.store.dispatch({type: "CIRCUIT_REMOVE_ITEM", id: this.props.indicator, index: this.props.index})
    };

    render() {
        return (
            <div className="complex">
                <div className="nugget-tick"/>
                <div className="header">
                    <div className="ide-row">
                        <div className="ide-col">Complex</div>
                        <div className="ide-col icon" onClick={this._remove} style={{maxWidth: 30}}>
                            <i className="fal fa-trash"/>
                        </div>
                    </div>
                </div>
                <div className="content">
                    <Set
                        data={this.props.data}
                        path={this.props.index}
                        indicator={this.props.indicator}
                        definition={this.props.definition}/>
                </div>
            </div>
        )
    }
}

export default Complex;