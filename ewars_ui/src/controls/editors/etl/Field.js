import SelectField from "../../fields/f.select.jsx";
import FieldSelector from "../f.fieldselector.jsx";
import TextField from "../../fields/f.text.jsx";
import NumberField from "../../fields/f.numeric.jsx";
import DateField from "../../fields/f.date.jsx";

import FormUtils from "../../../utils/form_utils";


const TYPE_MAP = {
    number: NumberField,
    select: SelectField,
    text: TextField,
    date: DateField
};

const options = [
    ["EQ", "Is equal to"],
    ["NEQ", "Is not equal to"],
    ["GT", "Is greater than"],
    ["LT", "Is less than"],
    ["GTE", "Is greater-than or equal to"],
    ["LTE", "Is less-than or equal to"]
];

class NoField extends React.Component {
    render() {
        return (
            <div><p>Please select a field</p></div>
        )
    }
}

class Field extends React.Component {
    constructor(props) {
        super(props)
    }

    onRemove = () => {
        ewars.store.dispatch({type: "CIRCUIT_REMOVE_ITEM", id: this.props.indicator, index: this.props.index})
    };

    _onChange = (prop, value) => {

        let newValue = value;
        if (this.props.data.indexOf(":") >= 0) {
            // This is a complex
            let fieldKey = this.props.data.split(":");
            let fieldDef = FormUtils.field(this.props.definition, fieldKey[0]);

            if (prop == 'value') {
                if (fieldDef.multiple) {
                    newValue = value.join(",");
                }
            }

        }

        ewars.store.dispatch({
            type: "UPDATE_ROOT_ITEM",
            id: this.props.indicator,
            index: this.props.index,
            prop: prop,
            value: newValue
        })
    };


    render() {
        let fieldDef,
            fieldKey;

        let cmpValue = null;

        // A complex input
        if (this.props.data.indexOf(":") >= 0) {
            // This is a complex
            fieldKey = this.props.data.split(":");
            fieldDef = FormUtils.field(this.props.definition, fieldKey[0]);
            if (fieldDef.multiple) {
                if (fieldKey[2]) {
                    fieldKey[2] = fieldKey[2].split(",")
                } else {
                    fieldKey[2] = [];
                }
            }
        } else {
            // A straight correlary
            fieldKey = this.props.data;

            fieldDef = FormUtils.field(this.props.definition, fieldKey);
        }

        let Cmp, field;
        if (fieldDef) {
            if (TYPE_MAP[fieldDef.type]) {
                Cmp = TYPE_MAP[fieldDef.type];
            } else {
                Cmp = NoField;
            }
        }

        let endOptions = ewars.copy(options);
        if (fieldDef.multiple) {
            endOptions.push(['CNT', 'Contains']);
            endOptions.push(['NCNT', 'Does not contain'])
        }

        endOptions.push(['NULL', 'Null']);

        if (!fieldDef && fieldKey.length == 3) {
            if (fieldPath[0] != "null") {
                return (
                    <div className="field">
                        <div className="ide-row">
                            <div className="ide-col"><p className="error">Unrecognized field {fieldKey[0]}</p></div>
                            <div className="ide-col icon" style={{maxWidth: 22}} onClick={this._onRemove}>
                                <i className="fal fa-trash"></i>
                            </div>
                        </div>
                    </div>
                )
            }
        }

        if (fieldDef.type == "number" && this.props.data.indexOf(':') < 0) {
            return (
                <div className="field">
                    <div className="nugget-tick"></div>
                    <div className="ide-row">
                        <div className="ide-col">{this.props.data}</div>
                        <div className="ide-col icon" style={{maxWidth: 22}} onClick={this.onRemove}>
                            <i className="fal fa-trash"></i>
                        </div>
                    </div>
                </div>
            )
        } else {

            return (
                <div className="field">
                    <div className="ide-row">
                        <div className="ide-col" style={{padding: 0}}>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <FieldSelector
                                        name="field"
                                        onUpdate={this._onChange}
                                        definition={this.props.definition}
                                        value={this.props.data.split(":")[0]}/>

                                </div>
                                <div className="ide-col">
                                    <SelectField
                                        name="operator"
                                        onUpdate={this._onChange}
                                        value={this.props.data.split(":")[1]}
                                        config={{options: endOptions}}/>
                                </div>
                                <div className="ide-col">
                                    {fieldDef ?
                                        <Cmp
                                            config={fieldDef}
                                            name="value"
                                            onUpdate={this._onChange}
                                            value={fieldKey[2]}/>
                                        : null}

                                </div>
                            </div>
                        </div>
                        <div className="ide-col icon" style={{maxWidth: 22}} onClick={this.onRemove}>
                            <i className="fal fa-trash"></i>
                        </div>

                    </div>
                </div>
            )
        }
    }
}

export default Field;