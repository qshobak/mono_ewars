import React from "react";

var charMap = {
    // latin
    'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE',
    'Ç': 'C', 'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I',
    'Î': 'I', 'Ï': 'I', 'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O',
    'Õ': 'O', 'Ö': 'O', 'Ő': 'O', 'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U',
    'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH', 'ß': 'ss', 'à': 'a', 'á': 'a',
    'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c', 'è': 'e',
    'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
    'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o',
    'ő': 'o', 'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u',
    'ý': 'y', 'þ': 'th', 'ÿ': 'y', 'ẞ': 'SS',
    // greek
    'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
    'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
    'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
    'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
    'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',
    'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
    'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
    'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
    'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
    'Ϋ': 'Y',
    // turkish
    'ş': 's', 'Ş': 'S', 'ı': 'i', 'İ': 'I', 'ç': 'c', 'Ç': 'C', 'ü': 'u', 'Ü': 'U',
    'ö': 'o', 'Ö': 'O', 'ğ': 'g', 'Ğ': 'G',
    // russian
    'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
    'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
    'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
    'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': 'u', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
    'я': 'ya',
    'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
    'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
    'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
    'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': 'U', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
    'Я': 'Ya',
    // ukranian
    'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G', 'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',
    // czech
    'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
    'ž': 'z', 'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T',
    'Ů': 'U', 'Ž': 'Z',
    // polish
    'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
    'ż': 'z', 'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ś': 'S',
    'Ź': 'Z', 'Ż': 'Z',
    // latvian
    'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
    'š': 's', 'ū': 'u', 'ž': 'z', 'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i',
    'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N', 'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
    // currency
    '€': 'euro', '₢': 'cruzeiro', '₣': 'french franc', '£': 'pound',
    '₤': 'lira', '₥': 'mill', '₦': 'naira', '₧': 'peseta', '₨': 'rupee',
    '₩': 'won', '₪': 'new shequel', '₫': 'dong', '₭': 'kip', '₮': 'tugrik',
    '₯': 'drachma', '₰': 'penny', '₱': 'peso', '₲': 'guarani', '₳': 'austral',
    '₴': 'hryvnia', '₵': 'cedi', '¢': 'cent', '¥': 'yen', '元': 'yuan',
    '円': 'yen', '﷼': 'rial', '₠': 'ecu', '¤': 'currency', '฿': 'baht',
    '$': 'dollar',
    // symbols
    '©': '(c)', 'œ': 'oe', 'Œ': 'OE', '∑': 'sum', '®': '(r)', '†': '+',
    '“': '"', '”': '"', '‘': "'", '’': "'", '∂': 'd', 'ƒ': 'f', '™': 'tm',
    '℠': 'sm', '…': '...', '˚': 'o', 'º': 'o', 'ª': 'a', '•': '*',
    '∆': 'delta', '∞': 'infinity', '♥': 'love', '&': 'and', '|': 'or',
    '<': 'less', '>': 'greater'
}

function slugify(string, replacement) {
    return string.split('').reduce(function (result, ch) {
        if (charMap[ch]) {
            ch = charMap[ch]
        }
        // allowed
        ch = ch.replace(/[^\w\s$*_+~.()'"!\-:@]/g, '')
        result += ch
        return result
    }, '')
    // trim leading/trailing spaces
        .replace(/^\s+|\s+$/g, '')
        // convert spaces
        .replace(/[-\s]+/g, replacement || '-')
        // remove trailing separator
        .replace('#{replacement}$', '')
        .toLowerCase()
}

slugify.extend = function (customMap) {
    for (var key in customMap) {
        charMap[key] = customMap[key]
    }
}

class Indicator extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            items: [],
            _isLoaded: false
        }
    }

    _expand = () => {
        if (this.state._isLoaded) {
            this.setState({
                _show: !this.state._show
            })
        } else {
            this.refs.icon.className = "fal fa-cog fa-spin";
            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then((resp) => {
                    this.setState({
                        _isLoaded: true,
                        _show: true,
                        items: resp
                    })
                })
        }
    };

    _onDragStart = (e) => {
        ewars.emit("SHOW_DROPS", "I")
        e.dataTransfer.setData("n", JSON.stringify(["I", this.props.data.uuid, slugify(__(this.props.data.name), "_"), {}]));
    };

    _onClick = () => {
        if (this.props.onClick) {
            this.props.onClick([
                "I",
                this.props.data.uuid,
                slugify(__(this.props.data.name), "_"),
                {}
            ])
        }
    };

    render() {
        let view;
        if (this.props.data.context == "FOLDER") {
            view = (
                <div className="block-content" onClick={this._expand}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="15px" style={{display: "block"}}>
                            <i ref="icon"
                               className={"fal " + (this.state._show ? "fa-caret-down" : "fa-caret-right")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {__(this.props.data.name)}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            )
        }

        if (this.props.data.context == "INDICATOR") {
            view = (
                <div className="block-content" onDragStart={this._onDragStart} draggable={this.props.draggable}
                     onClick={this._onClick}>
                    {__(this.props.data.name)}
                </div>
            )
        }

        return (
            <div className="block">
                {view}
                {this.state._show ?
                    <div className="block-children">
                        {this.state.items.map((item) => {
                            return <Indicator
                                draggable={this.props.draggable}
                                onClick={this.props.onClick}
                                data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class IndicatorTree extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);

        this.state = {
            items: [],
            _show: false
        }

        ewars.tx('com.ewars.indicators', [null])
            .then((resp) => {
                this.setState({items: resp})
            })
    }

    _show = () => {
        this.setState({
            _show: !this.state._show
        })
    };

    render() {
        let iconClass = this.state._show ? "fa-caret-down" : "fa-caret-right";
        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i className={"fal " + iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-code-branch"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Indicators
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">
                        {this.state.items.map((item) => {
                            return <Indicator
                                onClick={this.props.onClick}
                                draggable={this.props.draggable}
                                data={item}/>
                        })}

                    </div>
                    : null}
            </div>
        )
    }
}

class FormNode extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);

        this.state = {
            fields: null,
            shown: false
        }
    }

    _toggler = () => {
        if (!this.state.fields) {
            ewars.tx("com.ewars.form.fields", [this.props.data.id])
                .then((resp) => {
                    let sorted = resp.sort((a, b) => {
                        if (a[0] > b[0]) return 1;
                        if (a[0] < b[0]) return -1;
                        return 0;
                    })
                    this.setState({
                        fields: sorted,
                        shown: true
                    })
                })
        } else {
            this.setState({
                shown: !this.state.shown
            })
        }
    };

    render() {
        return (
            <div className="block">
                <div className="block-content" onClick={this._toggler}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="15px" style={{display: "block"}}>
                            <i className={"fal " + (!this.state.shown ? "fa-caret-right" : "fa-caret-down")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {__(this.props.data.name)}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div className="block-children">
                        {this.state.fields.map((item) => {
                            return <FormField
                                data={item}
                                draggable={this.props.draggable}
                                onClick={this.props.onClick}
                                form_id={this.props.data.id}/>;
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class FormField extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        e.dataTransfer.setData("n", JSON.stringify([
            "F",
            `${this.props.form_id}.${this.props.data[1]}`,
            slugify(this.props.data[2], "_")
        ]));
    };

    _onClick = () => {
        if (this.props.onClick) {
            this.props.onClick([
                "F",
                `${this.props.form_id}.${this.props.data[1]}`,
                slugify(this.props.data[2], "_"),
                {}
            ])
        }
    }

    render() {
        return (
            <div className="block" onDragStart={this._onDragStart} draggable={this.props.draggable}
                 onClick={this._onClick}>
                <div className="block-content">{__(this.props.data[2])}</div>
            </div>
        )
    }
}

class FormTree extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            forms: [],
            _isLoaded: false
        }
    }

    _show = () => {
        if (this.state._isLoaded) {
            this.setState({
                _show: !this.state._show
            })
        } else {
            this.refs.icon.className = "fal fa-cog fa-spin";
            ewars.tx("com.ewars.query", ["form", ["id", "name"], {"status": {eq: "ACTIVE"}}, null, null, null, null])
                .then((resp) => {
                    this.setState({
                        forms: resp,
                        _isLoaded: true,
                        _show: true
                    })
                })
        }
    };

    render() {

        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i ref="icon"
                               className={"fal " + (this.state._show ? "fa-caret-down" : "fa-caret-right")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-list"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Form Fields
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">
                        {this.state.forms.map((item) => {
                            return <FormNode data={item} draggable={this.props.draggable} onClick={this.props.onClick}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class DataSetTree extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false
    };

    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            sets: []
        }
    }

    _show = () => {
        this.setState({
            _show: !this.state._shown
        })
    };

    render() {
        let iconClass = this.state._show ? "fa-caret-down" : "fa-caret-right";
        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i className={"fal " + iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-database"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Data Set
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">

                    </div>
                    : null}
            </div>
        )
    }
}

class DimensionTree extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div></div>
        )
    }
}

class DataSourceTree extends React.Component {
    static defaultPrpos = {
        draggable: true,
        onClick: null
    };

    constructor(props) {
        super(props)
    }

    _startDrag = (e) => {

    };

    _onVarDrag = (e) => {
        e.dataTransfer.setData("n", JSON.stringify(["V", null, null, {}]));
    };

    _onClick = (data) => {
        this.props.onClick(data);
    };

    _onVarClick = () => {
        this.props.onClick(["V", null, null, {}]);
    };

    render() {
        return (
            <div className="block-tree" style={{position: "relative"}}>
                <IndicatorTree draggable={this.props.draggable} onClick={this._onClick}/>
                <FormTree draggable={this.props.draggable} onClick={this._onClick}/>
                <DataSetTree draggable={this.props.draggable} onClick={this._onClick}/>
                <div className="block" draggable={this.props.draggable} onDragStart={this._onVarDrag}
                     onClick={this._onVarClick}>
                    <div className="block-content">
                        <ewars.d.Row>
                            <ewars.d.Cell width={15}>
                                <i className="fal fa-gear"></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                Variable
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataSourceTree;
