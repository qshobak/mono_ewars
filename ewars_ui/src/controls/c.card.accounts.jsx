import React from 'react';

import ActionGroup from "./c.actiongroup.jsx";

const ACTIONS = [
    ['fa-times', 'CLOSE', 'CLOSE']
];

class AccountItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card-item" onClick={this._swapAccount}>
                {this.props.data.name} - {this.props.data.role}<br />
                {this.props.data.account_name}
            </div>
        )
    }
}

class AccountsCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DETAILS"
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = "1";
        }, 10);
    }

    _close = () => {
        this._el.removeEventListener("transitionend", this._close);
        window.state.accountsCard = null;
        window.state.emit("CHANGE");
    };

    _showMenu = () => {
        let menu = new Menu();

        menu.append(
            new MenuItem({
                label: "Test",
                click: () => {
                    console.log("TEXT")
                }
            })
        );
        menu.popup({window: remote.getCurrentWindow()});
    };

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this._el.addEventListener("transitionend", this._close);
                this._el.style.transition = "all 0.1s ease-out";
                this._el.style.transform = "scale(0)";
                this._el.style.opacity = "0";
                break;
            case "MORE":
                this._showMenu();
                break;
            default:
                break;
        }
    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        return (
            <div className="user-card-outer">
                <div
                    className="modal-inner"
                    ref={(el) => this._el = el}
                    style={{transform: "scale(0)", opacity: "0"}}>
                    <div className="column" style={{height: "100%"}}>
                        <div className="row bb gen-toolbar">
                            <div className="column grid-title">Accounts</div>
                            <ActionGroup
                                actions={ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div className="row block block-overflow padded-8">
                            {window.state.accounts.map(item => {
                                return <AccountItem data={item}/>
                            })}
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default AccountsCard;
