import React from "react";

class EmmettListItem extends React.Component {
    render() {
        return (
            <div className="emmett-list-item">
                {this.props.children}
            </div>
        )
    }
}

class EmmettList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="emmett-list">
                {this.props.children}
            </div>
        )
    }
}


export default EmmettList;
