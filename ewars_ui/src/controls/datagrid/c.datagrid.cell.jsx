import React from "react";

class DataTableCell extends React.Component {
    static defaultProps = {
        editable: false,
        editor: null
    };

    constructor(props) {
        super(props);

        this.state = {
            context: false,
            edit: false,
            tempVal: null
        }
    }

    render() {
        let style = {width: this.props.width};

        let val;
        console.log(this.props.col, this.props.row)
        if (this.props.col.rootProp) {
            val = this.props.row[this.props.col.rootProp][this.props.col.name];
        } else if (this.props.col.name.indexOf(".") > 0) {
            val = ewars.getKeyPath(this.props.col.name, this.props.row);
        } else {
            val = this.props.row[this.props.col.name] || null;
        }
        let rawVal = val;


        if (this.props.col.config.type == "select" && this.props.col.config.options) {
            if (this.props.col.config.multiple) {
                if (val) {
                    let newVal = [];
                    this.props.col.config.options.forEach((item) => {
                        if (val.indexOf(item[0]) >= 0) {
                            newVal.push(item[1]);
                        }
                    });

                    val = newVal.join(", ")
                }

            } else {
                this.props.col.config.options.forEach(function (item) {
                    if (item[0] == val) val = item[1]
                })
            }
        }

        if (this.props.col.config.type == "lat_long" && val != null) val = `Lat: ${rawVal[0]}, Lng: ${rawVal[1]}`;

        if (this.props.col.fmt) val = this.props.col.fmt(val, this.props.row);

        if (val instanceof Object) val = ewars.I18N(val);

        let valueClass = "dt-cell-value";
        if (this.props.col.config.type == "number") valueClass += " rh-align";

        let EditControl;
        // if (this.state.edit) {
        //     EditControl = TYPES[this.props.col.config.type];
        // }

        let cellStyle = {
            border: "1px dashed transparent",
            position: "relative",
            height: "100%"
        };
        if (this.props.col.color) {
            let color = this.props.col.color(rawVal);
            cellStyle.borderColor = color;
            cellStyle.textShadow = `0px -2px 3px ${color}`;
        }


        return (
            <td style={style} ref="cell">
                <div style={cellStyle}>
                    {!this.state.edit ?
                        <div className={valueClass}>{val}</div>
                        : null}
                </div>
            </td>
        )
    }
}

export default DataTableCell;
