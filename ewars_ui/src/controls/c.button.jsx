import React from "react";

class Button extends React.Component {
    static defaultProps = {
        color: false,
        active: true,
        highlight: false,
        closable: false,
        label: null
    };

    constructor(props) {
        super(props);
    }

    _onClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.props.onClick(this.props.data);
    };

    onClose = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.props.onClose();
    };

    render() {

        let className = "bttn";
        if (this.props.color) className += " " + this.props.color;
        if (!this.props.active) className += " disabled";
        if (this.props.highlight) className += " active";
        let closer;
        if (this.props.closable) {
            closer = <i className="fal fa-times" onClick={this.onClose}></i>
        }

        let icon;

        if (this.props.icon) {
            if (this.props.icon.constructor == Array) {
                icon = (
                    <div className="fa-stack">
                        {this.props.icon.map((ic) => {
                            return (
                                <i className={"fal " + ic}></i>
                            )
                        })}
                    </div>
                )
            } else {
                icon = <i className={"fal " + this.props.icon}></i>
            }
        }

        if (icon && this.props.label == null) {
            className += " no-label";
        }

        let label;
        if (this.props.label) label = this.props.label.en || this.props.label;
        return (
            <div className={className} onClick={this._onClick}>
                {icon}
                {label}
                {closer}
            </div>
        )
    }
}

export default Button;