class ActionButton extends React.Component {
    static defaultProps = {
        height: "20px",
        tip: null
    };

    constructor(props) {
        super(props);

        this.state = {}
    }

    onClick = () => {
        this._hideHover();
        if (this.props.onClick) {
            this.props.onClick();
        } else {
            this.props.onAction(this.props.a);
        }
    };

    componentDidMount() {
        if (this.props.tip) {
            this.el.addEventListener('mouseover', this._showHover);
            this.el.addEventListener('mouseout', this._hideHover);
        }
    }

    componentWillUnmount() {
        if (this.props.tip) {
            this.el.removeEventListener('mouseover', this._showHover);
            this.el.removeEventListener('mouseout', this._hideHover);
        }
    }

    _showHover = () => {
        this._hover = document.createElement('div');
        this._hover.setAttribute('class', 'action-hover');
        this._hover.appendChild(document.createTextNode(this.props.tip));

        document.body.appendChild(this._hover);
    };

    _hideHover = () => {
        if (this._hover) {
            document.body.removeChild(this._hover);
        }
    };
    render() {
        let style = {
            height: this.props.height,
            lineHeight: this.props.height
        };


        return (
            <div
                ref={(el) => {this.el = el;}}
                className="action-button"
                style={style}
                onClick={this.onClick}>
                <i className={"fal " + this.props.i}></i>
            </div>
        )
    }
}

class ActionGroup extends React.Component {
    static defaultProps = {
        height: "20px"
    };

    render() {
        let className = "action-group";
        if (this.props.right) {
            className += " pull-right";
        }

        let style = {
            height: this.props.height,
            lineHeight: this.props.height
        };

        return (
            <span className={className} style={style}>
                {this.props.actions.map((action) => {
                    return (
                        <ActionButton
                            key={action[1]}
                            height={this.props.height}
                            onAction={(action) => {
                                this.props.onAction(action)
                            }}
                            tip={action[2] || null}
                            i={action[0]}
                            a={action[1]}/>
                    )
                })}
            </span>
        )
    }
}

export {
    ActionButton,
    ActionGroup
}