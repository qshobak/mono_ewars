var TreeNode = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            showChildren: false
        };
    },

    onCaretClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (["row", "group", "matrix", "form"].indexOf(this.props.data.nodeType) >= 0) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            })
        } else {
            this._onLabelClick(e);
        }

    },

    _onLabelClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (["row", "group", "matrix", "form"].indexOf(this.props.data.field.type) < 0) {
            this.props.onNodeSelect(this.props.data);
        } else {
            this.onCaretClick(e);
        }
    },

    _hasChildren: function () {
        if (!this.props.data.children) return false;
        if (this.props.data.children.length > 0) return true;
        return false;
    },

    _getIcon: function () {
        var icon = "fal ";

        if (["form", "row", "matrix"].indexOf(this.props.data.nodeType) >= 0) {
            icon += "fa-circle";
            if (this.props.data.children) {
                if (this.props.data.children.length > 0) icon = "fal fa-caret-right";
                if (this.props.data.children.length > 0 && this.state.showChildren) icon = "fal fa-caret-down";
            }
        } else {
            icon += "fa-th";
        }

        return icon;
    },

    render: function () {
        var childs;

        if (this.state.showChildren) {
            childs = _.map(this.props.data.children, function (child) {
                return <TreeNode
                    onNodeSelect={this.props.onNodeSelect}
                    data={child}/>
            }, this)
        }

        var iconClass = this._getIcon();

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.label);

        return (
            <li>
                <div className="node-control" onClick={this.onCaretClick}><i className={iconClass}></i></div>
                <div className="label" onClick={this._onLabelClick}>{nodeName}</div>
                {this.state.showChildren ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});

function _findFieldByPath(options, path) {
    var found;

    _.each(options, function (field) {
        if (field.path == path) found = field;
        if (!found && field.children) {
            var subFound = _findFieldByPath(field.children, path);
            if (subFound) found = subFound;
        }
    }, this);

    return found;
}

var FormFieldTree = React.createClass({
    getInitialState: function () {
        return {
            nodeName: "None Selected",
            nodePath: null,
            showTree: false
        }
    },

    componentWillMount: function () {
        if (this.props.value) {
            var node = _findFieldByPath(this.props.options, this.props.value);

            if (node) {
                this.state.nodeName = node.pathLabel;
                this.state.nodePath = node.path;
            }
        }
    },

    _processTree: function () {
        return _.map(this.props.options, function (option) {
            return <TreeNode
                data={option}
                onNodeSelect={this._onNodeSelection}/>;
        }, this)
    },

    _hideTree: function (e) {
        this.setState({
            showTree: false
        })
    },

    _toggleTree: function () {
        this.setState({
            showTree: this.state.showTree ? false : true
        })
    },

    _onNodeSelection: function (node) {
        this.setState({
            nodeName: node.pathLabel,
            nodePath: node.path,
            showTree: false
        });

        this.props.onUpdate(node);
    },

    render: function () {
        var rootNodes = this._processTree();

        return (
            <div className="location-selector">
                <div className="handle">
                    <div className="current">
                        <div className="current-location">{this.state.nodeName}</div>
                    </div>
                    <div className="grip" onClick={this._toggleTree}><i className="fal fa-globe"></i>
                    </div>
                </div>
                {this.state.showTree ?
                    <div className="location-tree-wrapper">
                        <div className="location-tree" style={{padding: 0, paddingTop: 0, paddingLeft: 0}}>
                            <div className="location-stack">

                                <div className="location-tree">

                                    <ul>
                                        {rootNodes}
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    : null}

            </div>
        )
    }
});

module.exports = FormFieldTree;