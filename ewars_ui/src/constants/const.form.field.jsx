export const FIELD_FORM = [
    {
        _o: 0,
        n: "label",
        i18: true,
        t: "TEXT",
        l: "FIELD_LABEL",
        d: {en: "New Field"}
    },
    {
        _o: 1,
        n: "type",
        t: "SELECT",
        l: "FIELD_TYPE",
        d: "text",
        o: [
            ["text", "TEXT_FIELD"],
            ["number", "NUMERIC_FIELD"],
            ["select", "SELECT_FIELD"],
            ["textarea", "TEXTAREA_FIELD"],
            ["switch", "SWITCH_FIELD"],
            ["header", "HEADER_FIELD"],
            ["matrix", "MATRIX_FIELD"],
            ["row", "ROW_FIELD"],
            ["display", "DISPLAY"],
            ["date", "Date Field"],
            ['location', 'Location'],
            ['lat_long', 'Lat/Lng Field']
        ]
    },
    {
        _o: 2,
        n: "name",
        t: "TEXT",
        l: "FIELD_NAME"
    },
    {
        _o: 3,
        n: "required",
        l: "REQUIRED",
        t: "BUTTONSET",
        o: [
            [false, "NO"],
            [true, "YES"]
        ],
        d: false,
        c: [
            "ANY",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location",
            "type:=:number",
            "type:=:lat_long"
        ]
    },
    {
        _: 4,
        n: "show_mobile",
        t: "BUTTONSET",
        l: "Show on mobile?",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 4,
        n: 'sti',
        t: 'SELECT',
        l: 'Location type',
        r: true,
        o: [],
        c: [
            'ALL',
            'type:=:location'
        ],
        os: ['location_type', 'id', 'name', {}]

    },
    {
        _o: 5,
        n: "show_row_label",
        t: "BUTTONSET",
        l: "Show row label",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "type:=:row"
        ]
    },
    {
        _o: 6,
        n: "date_type",
        t: "SELECT",
        l: "DATE_TYPE",
        r: true,
        d: "DAY",
        o: [
            ["DAY", _l("DAY")],
            ["WEEK", _l("WEEK")],
            ["MONTH", _l("MONTH")],
            ["YEAR", _l("YEAR")]
        ],
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    {
        _o: 7,
        n: "help",
        t: "TEXT",
        i18: true,
        l: "FIELD_INSTRUCTIONS",
        ml: true,
        c: [
            "ANY",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location",
            "type:=:number"
        ]
    },
    {
        _o: 8,
        n: "options",
        t: "OPTIONSDEF",
        l: "OPTIONS",
        i18: true,
        c: [
            "ALL",
            "type:=:select"
        ]
    },
    {
        _o: 9,
        n: "default",
        t: "TEXT",
        l: _l("DEFAULT_VALUE"),
        c: [
            "ANY",
            "type:=:number",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location"
        ]
    },
    {
        _o: 10,
        n: "redacted",
        t: "BUTTONSET",
        l: "REDACTED",
        d: false,
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ANY",
            "type:=:select",
            "type:=:number",
            "type:=:text",
            "type:=:date",
            "type:=:location",
            "type:=:textarea"
        ]
    },
    {
        _o: 11,
        n: "header_style",
        t: "SELECT",
        l: _l("HEADER_STYLE"),
        d: "TITLE",
        o: [
            ["style-title", "TITLE"],
            ["style-sub-title", "SUB_TITLE"]
        ],
        c: [
            "ALL",
            "type:=:header"
        ]
    },
    {
        _o: 12,
        n: "placeholder",
        t: "TEXT",
        l: "PLACEHOLDER",
        c: [
            "ANY",
            "type:=:text",
            "type:=:number",
            "type:=:textarea"
        ]
    },
    {
        _o: 13,
        n: "barcode",
        t: "BUTTONSET",
        l: "BARCODE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ANY",
            "type:=:text",
            "type:=:number"
        ]
    },
    {
        _o: 14,
        n: "allow_future_dates",
        t: "BUTTONSET",
        l: "ALLOW_FUTURE_DATES",
        d: false,
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    {
        _o: 15,
        n: "multiple",
        t: "BUTTONSET",
        l: "SELECT_MULTIPLE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        d: false,
        c: [
            "ALL",
            "type:=:select"
        ]
    },
    {
        _o: 16,
        n: "max",
        t: "TEXT",
        l: "MAXIMUM",
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 17,
        n: "min",
        t: "TEXT",
        l: "MINIMUM",
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 18,
        n: "negative",
        t: "BUTTONSET",
        l: "ALLOW_NEGATIVE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        d: true,
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 19,
        n: "conditional_bool",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        l: "CONDITIONAL_LOGIC"
    },
    {
        _o: 20,
        n: "conditions",
        t: "FIELDRULES",
        l: "Rules editor",
        c: [
            "ALL",
            "conditional_bool:=:true"
        ]
    }
];
