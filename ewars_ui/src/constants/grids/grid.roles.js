import Moment from "moment";

const GRID = {
    title: "Roles",
    icon: "fa-user",
    columns: [
        {
            name: "name",
            type: "text",
            label: "ROLE_NAME",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "STATUS",
            options: [
                ["SYSTEM", "System"],
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "inh",
            type: "select",
            label: "INHERITS",
            options: [
                ['ACCOUNT_ADMIN', 'ACCOUNT_ADMINISTRATOR'],
                ['REGIONAL_ADMIN', 'REGIONAL_ADMINISTRATOR'],
                ['USER', 'DEFAULT_USER']
            ],
            root: true
        },
        {
            name: "created",
            label: "CREATED",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "MODIFIED",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }

    ],
    pinnedRows: [
        {uuid: "ACCOUNT_ADMIN", name: "Account Administrator (Locked)", status: "SYSTEM", created: "2015-01-01", modified: "2015-01-01"},
        {uuid: "REGIONAL_ADMIN", name: "Regional Administrator (Locked)", status: "SYSTEM", created: "2015-01-01", modified: "2015-01-01"},
        {uuid: "USER", name: "Default User (Locked)", status: "SYSTEM", created: "2015-01-01", modified: "2015-01-01"}
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.shade({
                    title: "Role",
                    cmp: "ROLE",
                    data: {},
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "DELETE":
                if (['ACCOUNT_ADMIN', 'REGIONAL_ADMIN', 'USER'].indexOf(data.uuid) >= 0) {
                    window.state.error("ERR_DELETE_SYSTEM_ROLE");
                } else {
                    window.state.dialog({
                        title: "DELETE_ROLE",
                        body: "DELETE_ROLE_BODY",
                        buttons: [
                            'DELETE',
                            'CANCEL'
                        ]
                    }, (res) => {
                        if (res == 0) {
                            let bl = window.state.blocker("DELETING_ROLE");
                            window.sonomaClient.tx("com.sonoma.role.delete", data.uuid, (res) => {
                                bl.rm();
                                window.state.growl("fa-trash", "ROLE_DELETED");
                                window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                            }, (err) => {
                                bl.rm();
                                window.state.error("ERROR", err);
                            })
                        }
                    })
                }
                break;
            case "EDIT":
                window.state.shade({
                    title: "ROLE",
                    cmp: "ROLE",
                    data: data,
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx('com.sonoma.roles', {orders, filters, limit, offset}, (res) => {
                resolve(res)
            }, (err) => {
                window.state.error("An error occurred", rerr);
            });
        })
    }
}

export default GRID;