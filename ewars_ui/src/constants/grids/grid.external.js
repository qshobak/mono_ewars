import Moment from "moment";

const GRID = {
    title: "External Data",
    icon: "fa-database",
    columns: [
        {
            name: "name",
            type: "text",
            label: "Role Name"
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Inactive'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            name: "created",
            label: "Created",
            type: "date",
            fmt: function(Val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "Modified",
            type: "date",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'Create new']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT',' Edit'],
        ['fa-trash', 'DELETE', 'Delete']
    ],
    action: function (action, data) {
        switch(action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("AddTab", {
                    detail: {
                        type: "EDITOR",
                        resource: "EXTERNAL",
                        name: "New External"
                    }
                }));
                break;
            case "EDIT":
                window.dispatchEvent(new CustomEvent("AddTab", {
                    detail: {
                        type: "EDITOR",
                        resource: "EXTERNAL",
                        name: __(data.name),
                        uuid: data.uuid
                    }
                }));
                break;
            case "DELETE":
                window.dialog({
                    title: "DELETE_EXTERNAL",
                    body: "DELETE_EXTERNAL_BODY",
                    buttons: [
                        'Delete',
                        'Cancel'
                    ]
                }, (res) => {
                    if (res == 0) {
                        window.blocker("DELETING_EXTERNAL");
                        window.tx("com.sonoma.external.delete", data.uuid, (res) => {
                            window.removeBlocker();
                            window.growl("fa-trash", "DELETED_EXTERNAL");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            window.removeBlocker();
                            window.state.error("ERROR_DELETION", err);
                        });
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx("com.sonoma.externals", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        })
    }
}

export default GRID;
