import Moment from "moment";
import DEFAULT_STATUSES from "../const.default_statuses";

const GRID = {
    title: "Users",
    icon: "fa-users",
    columns: [
        {
            name: "name",
            label: "NAME",
            type: "str",
            root: true
        },
        {
            name: "email",
            label: "EMAIL",
            type: "str",
            root: true
        },
        {
            name: "status",
            label: "STATUS",
            type: "select",
            options: DEFAULT_STATUSES,
            root: true
        },
        {
            name: "role",
            root: true,
            type: "select",
            label: "ROLE",
            options: [
                ['ACCOUNT_ADMIN', "Account Administrator"],
                ['REGIONAL_ADMIN", "Regional Administrator'],
                ['USER', 'Reporting User']
            ]
        },
        {
            name: "created",
            label: "REGISTERED",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    search: true,
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW'],
        '-',
        ['fa-envelope', 'INVITE', "INVITE_USER"]
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-clipboard', 'ASSIGNMENTS', 'EDIT_ASSIGNMENTS'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch(action) {
            case "CREATE":
                window.state.shade({
                    cmp: "NEW_USER",
                    data: {},
                    title: "New User",
                    callback: () => {

                    }
                });
                break;
            case "EDIT":
                window.state.shade({
                    cmp: "USER",
                    data: data,
                    callback: () => {

                    }
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_USER",
                    body: "DELETE_USER_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_USER");
                        window.sonomaClient.tx("com.sonoma.user.delete", data.id, (result) => {
                            bl.rm();
                            window.state.growl("fa-trash", "DELETED_USER");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR", err);
                        })
                    }
                });
                break;
            case "INVITE":
                window.state.shade({
                    cmp: "INVITE",
                    title: "CREATE_INVITE",
                    actions: [],
                    callback: () => {

                    }
                });
                break;
            case "ASSIGNMENTS":
                window.state.shade({
                    cmp: "ASSIGNMENTS",
                    data: data.uuid,
                    title: "Assignments",
                    callback: () => {
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.users", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => console.log(err));
        })
    }
}

export default GRID;