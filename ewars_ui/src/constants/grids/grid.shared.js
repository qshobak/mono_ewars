import Moment from "moment";

const GRID = {
    title: "Shared data",
    icon: "fa-download",
    columns: [
        {
            name: "name",
            label: "NAME",
            type: "text",
            root: true
        },
        {
            name: 'status',
            label: "STATUS",
            type: "select",
            options: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive'],
                ['ARCHIVED', 'Archived']
            ],
            root: true
        },
        {
            name: "created",
            label: "CREATED",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "MODIFIED",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW'],
        ['fa-upload', 'IMPORT', 'IMPORT'],
        '-',
        ['fa-asterisk', 'HUB', "HUB"]
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-copy', 'DUPLICATE', 'DUPLICATE'],
        ['fa-download', 'EXPORT', 'EXPORT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "EDIT":
                window.state.addTab("ALARM", {uuid: data.uuid});
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_ALARM",
                    body: "DELETE_ALARM_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (result) => {
                    if (result == 0) {
                        let bl = window.state.blocker("DELETING_ALARM");
                        window.sonomaClient.tx("com.sonoma.alarm.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "DELETED_ALARM");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR_DELETION", err);
                        })
                    }
                });
                break;
            case "CREATE":
                window.state.addTab("ALARM", {uuid: data.uuid});
                break;
            default:
                break;
        }
    },
    query: function (order, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.alarms", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => reject(err));
        })
    }
}

export default GRID;