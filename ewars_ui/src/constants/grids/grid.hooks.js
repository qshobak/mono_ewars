import Moment from "moment";

const GRID = {
    title: "Hooks",
    icon: "fa-link",
    columns: [
        {
            name: "name",
            type: "text",
            label: "Hook Name",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Inactive'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "endpoint",
            type: "text",
            label: "Endpoint",
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.shade({
                    title: "HOOK",
                    data: {},
                    cmp: "HOOK",
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "EDIT":
                window.state.shade({
                    title: "HOOK",
                    data: data,
                    cmp: "HOOK",
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_HOOK",
                    body: "DELETE_HOOK_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_HOOK");
                        window.sonomaClient.tx("com.sonoma.hook.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "HOOK_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("DELETION_ERROR", err);
                        })
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.hooks", {orders, filters, limit, offset}, (res) => {
                resolve(res)
            }, (err) => {
                reject(err);
            });
        })
    }
}

export default GRID;