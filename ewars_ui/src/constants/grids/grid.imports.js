import Moment from "moment";
import dialog from "../../utils/dialog";

const STATUSES = {
    ACTIVE: "Active",
    INACTIVE: "Inactive",
    ARCHIVED: "Archived"
};

const FORMS_GRID = {
    title: "Imports",
    icon: "fa-upload",
    columns: [
        {
            name: "name",
            type: "text",
            label: "Name",
            root: true
        },
        {
            name: "form_name",
            type: "text",
            label: "Form",
            root: true,
            fmt: function (val) {
                return val.en || val;
            }
        },
        {
            name: "status",
            type: "text",
            label: "Status",
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW'],
        '-',
        ["fa-asterisk", "HUB", "HUB"]
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.addTab("IMPORT", {
                    name: "New import"
                });
                break;
            case "EDIT":
                window.state.addTab("IMPORT", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_IMPORT",
                    body: "DELETE_IMPORT_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_IMPORT");
                        window.sonomaClient.tx('com.import.delete', data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "IMPORT_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR_DELETION", err);
                        })
                    }
                });
                break;
            case "HUB":
                window.state.shade({
                    title: "HUB",
                    cmp: "HUB",
                    actions: [],
                    data: {type: "IMPORT"},
                    callback: () => {

                    }
                });
                break;
            case "IMPORT":
                break;
            case "EXPORT":
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.imports", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            })
        })
    }
}

export default FORMS_GRID;