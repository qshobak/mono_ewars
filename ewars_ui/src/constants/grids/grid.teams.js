import Moment from "moment";
import DEFAULT_STATUSES from "../const.default_statuses";

const GRID = {
    title: "TEAMS",
    icon: "fa-users",
    columns: [
        {
            name: "name",
            label: "NAME",
            type: "str",
            root: true
        },
        {
            name: "status",
            label: "STATUS",
            type: "select",
            options: DEFAULT_STATUSES
        },
        {
            name: "created",
            label: "CREATED",
            type: "date",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "CREATED",
            type: "date",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT','EDIT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch(action) {
            case "CREATE":
                window.state.shade({
                    cmp: "TEAM",
                    title: "TEAM",
                    data: {},
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "EDIT":
                window.state.shade({
                    cmp: "TEAM",
                    title: data.title.en || data.title,
                    data: data,
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_TEAM",
                    body: "DELETE_TEAM_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_TEAM");
                        window.sonomaClient.tx("com.sonoma.team.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl('fa-trash', "TEAM_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR", err);
                        })
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.teams", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        })
    }
}

export default GRID;