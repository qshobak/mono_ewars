import Moment from "moment";

const GRID = {
    title: "Reporting Periods",
    icon: "fa-calendar",
    columns: [
        {
            name: "name",
            type: "text",
            label: "NAME",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "STATUS",
            options: [
                ['ACTIVE', 'Inactive'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "type",
            type: "select",
            label: "PERIOD_TYPE",
            options: [
                ['SYSTEM', 'System'],
                ['CUSTOM', 'Custom'],
                ['MANUAL', 'Manual']
            ],
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "CREATED",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "MODIFIED",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");ß
            }

        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW'],
        '-',
        ['fa-upload', 'IMPORT', 'IMPORT'],
        '-',
        ['fa-asterisk', 'HUB', "HUB"]
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-download', 'EXPORT', 'EXPORT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.addTab("INTERVAL", {
                    name: "New interval"
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_INTERVAL",
                    body: "DELETE_INTERVAL_BODY",
                    buttons: [
                        "DELETE",
                        "CANCEL"
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_INTERVAL");
                        window.sonomaClient.tx("com.sonoma.interval.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "INTERVAL_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR", err);
                        })
                    }
                });
                break;
            case "IMPORT":
                // TODO: This needs to be different for desktop
                break;
            case "EDIT":
                window.state.addTab("INTERVAL", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx('com.sonoma.intervals', {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        })
    }
}

export default GRID;