import Moment from "moment";

import dialog from "../../utils/dialog";

const GRID = {
    title: "Alarms",
    icon: "fa-bell",
    columns: [
        {
            name: "name",
            label: "Name",
            type: "text",
            root: true
        },
        {
            name: 'status',
            label: "Status",
            type: "select",
            options: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive'],
                ['ARCHIVED', 'Archived']
            ],
            root: true
        },
        {
            name: "created",
            label: "Created",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "Modified",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'Create new alarm'],
        ['fa-upload', 'IMPORT', 'Import alarm']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'Edit alarm'],
        ['fa-copy', 'DUPLICATE', 'Duplicate alarm'],
        ['fa-download', 'EXPORT', 'Export alarm'],
        ['fa-trash', 'DELETE', 'Delete alarm']
    ],
    action: function (action, data) {
        switch (action) {
            case "EDIT":
                window.state.addTab("ALARM", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            case "DELETE":
                window.Dialog({
                    title: "Delete alarm?",
                    body: "Are you sure you want to delete this alarm? Any alerts associated with the alarm will be destroyed.",
                    buttons: [
                        'Delete',
                        'Cancel'
                    ]
                }, (result) => {
                    if (result == 0) {
                        window.tx.tx("com.sonoma.alarm.delete", data.uuid, (res) => {
                            dialog.growl("Alarm deleted");
                        }, (err) => {
                            dialog.error("An error occurred while deleting the alarm", err);
                        })
                    }
                });
                break;
            case "DUPLICATE":
                break;
            case "CREATE":
                window.stat.addTab("ALARM", {
                    name: "New Alarm",
                    uuid: null
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.alarms", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => reject(err));
        })
    }
}

export default GRID;
