import TextField from "../controls/fields/f.text.jsx";
import DateField from "../controls/fields/f.date.jsx";
import LocationField from "../controls/fields/f.location.jsx";
import SelectField from "../controls/fields/f.select.jsx";
import NumericField from "../controls/fields/f.numeric.jsx";
import PeriodField from "../controls/fields/f.period.jsx";
import IndicatorField from "../controls/fields/f.indicator.jsx";
import AssignmentField from "../controls/fields/f.assignment.jsx";
import TextAreaField from "../controls/fields/f.textarea.jsx";
import RiskField from "../controls/fields/f.risk.jsx";
import PermissionsField from "../controls/fields/f.permissions.jsx";
import AlarmField from "../controls/fields/f.alarm.jsx";
import AssociatedRecordsField from "../controls/fields/f.associated.jsx";
import DashboardField from "../controls/fields/f.dashboard.jsx";
import ButtonSet from "../controls/fields/f.buttonset.jsx";
import DictField from "../controls/fields/f.dict.jsx";
import OrganizationField from "../controls/fields/f.organization.jsx";
import LatLngField from "../controls/fields/f.lat_lng.jsx";
import DisplayField from "../controls/fields/f.display.jsx";
import GeometryField from "../controls/fields/f.geometry.jsx";
import FormField from "../controls/fields/f.form.jsx";
import FormFields from "../controls/fields/f.form_fields.jsx";
import RepeaterField from "../controls/fields/f.repeater.jsx";
import TraceField from "../controls/fields/f.trace.jsx";
import GroupsField from "../controls/fields/f.groups.jsx";
import RoleField from "../controls/fields/f.role.jsx";
import IntervalField from "../controls/fields/f.interval.jsx";

const FIELD_TYPES = {
    GEOMETRY: GeometryField,
    ROLE: RoleField,
    INTERVAL: IntervalField,
    GROUPS: GroupsField,
    ORGANIZATION: OrganizationField,
    DICT: DictField,
    BUTTONSET: ButtonSet,
    LOCATION: LocationField,
    ASSIGNMENT: AssignmentField,
    ASSOCIATED: AssociatedRecordsField,
    PERMISSIONS: PermissionsField,
    ALARM: AlarmField,
    INDICATOR: IndicatorField,
    DASHBOARD: DashboardField,
    TEXT: TextField,
    TEXTAREA: TextAreaField,
    SELECT: SelectField,
    NUMBER: NumericField,
    NUMERIC: NumericField,
    DATE: DateField,
    PERIOD: PeriodField,
    LAT_LNG: LatLngField,
    RISK: RiskField,
    FIELD_BY_UUID: null,
    VARIABLE: null,
    LOCATION_GROUP: null,
    LOCATION_TYPE: null,
    FIELD_BY_NAME: null,
    REPEATER: RepeaterField,
    GROUP: null,
    FIELDS: FormFields,
    FORM: FormField,
    DISPLAY: DisplayField,
    TRACE: TraceField
};

export default FIELD_TYPES;
