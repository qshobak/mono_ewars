const DEFAULT_ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CLOSE', 'Close']
];

export default DEFAULT_ACTIONS;