export default {
    t_general: {
        t: "H",
        l: "General Settings"
    },
    show_title: {
        t: "o",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    t_text_style: {
        t: "H",
        l: "Value Formatting"
    },
    prefix: {
        l: "Prefix"
    },
    suffix: {
        l: "Suffix"
    },
    format: {
        l: "Format",
        t: "o",
        o: [
            ["0", "0"],
            ["0,0", "0,0"],
            ["0,0.0", "0,0.0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"]
        ]
    },
    fontSize: {
        l: "Font Size"
    },
    t_mapping: {
        t: "H",
        l: "Value Mapping"
    },
    b_value_mapping: {
        t: "o",
        l: "Enabled",
        o: [
            [false, "Off"],
            [true, "On"]
        ]
    },
    value_mapping: {
        t: "vmap",
        ot: "VALUE",
        l: "Mapping"
    },
    t_cmapping:{
        t: "H",
        l: "Colour Mapping"
    },
    b_colour_mapping: {
        t: "o",
        l: "Enabled",
        o: [
            [false, "Off"],
            [true, "On"]
        ]
    },
    colour_mapping: {
        t: "vmap",
        l: "Mapping"
    }
}

