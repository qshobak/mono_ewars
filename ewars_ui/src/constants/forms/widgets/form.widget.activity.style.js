export default {
    t_general: {
        t: "H",
        l: "General Settings"
    },
    t_live_update: {
        t: "H",
        l: "Live updates"
    },
    stream: {
        t: "o",
        l: "Live update",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    stream_interval: {
        l: "Interval"
    }
}