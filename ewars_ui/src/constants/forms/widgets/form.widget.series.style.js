export default {
    t_general: {
        t: "H",
        l: "General Settings"
    },
    show_title: {
        t: "o",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    show_subtitle: {
        t: "o",
        l: "Show sub-title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    zoom: {
        t: "o",
        l: "Zoom",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    navigator: {
        t: "o",
        l: "navigator",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    t_legend: {
        t: "H",
        l: "Legend"
    },
    legend: {
        t: "o",
        l: "Show Legend",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    legend_pos: {
        t: "o",
        l: "Legend Pos.",
        o: [
            ["TL", "Top-left"],
            ["TC", "Top-center"],
            ["TR", "Top-right"],
            ["ML", "Middle-left"],
            ["MR", "Middle-right"],
            ["BL", "Bottom-left"],
            ["BC", "Bottom-center"],
            ["BR", "Bottom-right"]
        ]
    },
    legend_label: {
        l: "Label fmt."
    },
    fontSize: {
        l: "Font Size"
    },
    t_spawn: {
        t: "H",
        l: "Spawning"
    },
    spawning: {
        t: "o",
        l: "Enabled",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    spawn_target: {
        t: "o",
        l: "Spawn on",
        o: [
            ["LOCATION", "Location"],
            ["SOURCE", "Source"]
        ]
    },
    spawn_style: {
        t: "o",
        l: "Spawn style",
        o: [
            ["GRID", "Grid"],
            ["LIST", "List (Vertical)"],
            ["ROW", "Row"]
        ]
    },
    t_interactive: {
        t: "H",
        l: "Interactivity"
    },
    pt_click: {
        t: "o",
        l: "onClickPoint",
        o: [
            ["LOCATION", "Drill location"]
        ]
    },
    pt_hover: {
        t: "o",
        l: "onHoverPoint",
        o: [
            ["TABLE", "Table"]
        ]
    },
    t_dashboard: {
        t: "H",
        l: "Dashboard Settings"
    }
}

