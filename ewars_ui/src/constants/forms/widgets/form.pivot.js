export default [
    {
        _o: 0,
        t: "TEXT",
        l: "Title",
        n: "title"
    },
    {
        _o: 1,
        t: "TEXT",
        l: "Sub-Title",
        n: "subtitle",
    }
]