const FORM_SERIES_Y = [
    {
        _o: 0,
        n: "type",
        l: "Type",
        t: "SELECT",
        o: [
            ["time", "Time"],
            ["location", "Location"]
        ]
    }
]

export default FORM_SERIES_Y;