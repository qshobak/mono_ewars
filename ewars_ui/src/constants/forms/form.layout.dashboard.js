const FORM_DASHBOARD = [
    {
        _o: 0,
        n: "name",
        l: "Dashboard Name",
        t: "TEXT"
    },
    {
        _o: 1,
        n: "status",
        t: "BUTTONSET",
        l: "Status",
        o: [
            ['INACTIVE', "Inactive"],
            ["ACTIVE", "Active"]
        ]
    },
    {
        _o: 3,
        n: "description",
        l:" Description",
        t: "TEXT",
        ml: true
    }
];

export default FORM_DASHBOARD;