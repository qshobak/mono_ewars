const FORM_RAW_SOURCE = [
    {
        _o: 0,
        t: "H",
        l: "Location"
    },
    {
        _o: 1,
        t: "SELECT",
        n: "loc_spec",
        l: "Location specification",
        o: [
            ["SPECIFIC", "Specific location"],
            ["GROUPS", "Location group(s)"]
        ]
    },
    {
        _o: 2,
        n: "group_ids",
        t: "LOCGROUPS",
        l: "Location group(s)",
        c: [
            "ALL",
            "loc_spec:=:GROUPS"
        ]
    },
    {
        _o: 3,
        n: "location",
        l: "Location",
        t: "LOCATION",
        c: [
            "ALL",
            "loc_spec:=:SPECIFIC"
        ]
    },
    {
        _o: 4,
        t: "H",
        l: "Source"
    },
    {
        _o: 5,
        n: "source_type",
        l: "Source type",
        t: "BUTTONSET",
        o: [
            ["SLICE", "Source"],
            ["SLICE_COMPLEX", "Calculation"]
        ]
    },
    {
        _o: 5.1,
        n: "interval",
        l: "Complex pre-aggregation",
        t: "BUTTONSET",
        o: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ],
        c: [
            "ALL",
            "source_type:=:SLICE_COMPLEX"
        ]
    },
    {
        _o: 6,
        n: "series",
        l: "Variables",
        t: "COMPLEX",
        c: [
            "ALL",
            "source_type:=:SLICE_COMPLEX"
        ]
    },
    {
        _o: 7,
        n: "indicator",
        l: "Indicator",
        t: "INDICATOR",
        c: [
            "ALL",
            "source_type:=:INDICATOR"
        ]
    },
    {
        _o: 8,
        t: "H",
        l: "Output"
    },
    {
        _o: 10,
        t: "BUTTONSET",
        n: "reduction",
        l: "Reduction",
        o: [
            ["SUM", "Sum"],
            ["AVG", "Avg."]
        ]
    },
    {
        _o: 11,
        t: "H",
        l: "Time period"
    },
    {
        _o: 12,
        t: "PERIOD",
        n: "period",
        l: "Period"
    }
];

export default FORM_RAW_SOURCE;
