const FORM_ASSIGNMENTS = [
    {
        _o: 0,
        t: "TEXT",
        n: "title",
        l: "Title"
    },
    {
        _o: 1,
        t: "SELECT",
        l: "Form filter",
        n: "fid"
    }
]

export default FORM_ASSIGNMENTS;