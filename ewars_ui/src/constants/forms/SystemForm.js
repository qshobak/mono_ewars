import TextField from "./fields/TextField";
import SelectField from "./fields/SelectField";
import LocationGroupField from "./fields/LocationGroupField";
import ButtonSet from "./fields/ButtonSet";
import OptionsSet from "./fields/OptionSet";
import RulesEditor from "./fields/RulesEditor";
import DataSourceDrop from "./fields/DataSourceDrop";
import ColourField from "./fields/ColourField";
import DateRangeSelector from "./fields/DateRangeSelector";
import IndicatorField from "./fields/IndicatorField";

const LocationField = require("./fields/LocationField");
import HTMLEditor from "./fields/HTMLEditor";
import Sources from "./fields/Sources";


const VERT = {
    borderBottom: "1px solid rgba(0,0,0,0.9)",
    marginTop: "8px",
    marginBottom: "2px"
}

class Header extends React.Component {
    render() {
        return (
            <ewars.d.Row>
                <ewars.d.Cell>
                    {this.props.vertical ?
                        <div style={VERT}></div>
                        : null}
                    <div className="sys-header" style={{fontWeight: "bold", fontSize: "12px"}}>
                        {this.props.data.i ?
                            <i className={"fal " + this.props.data.i} style={{marginRight: "8px"}}></i>
                            : null}
                        {this.props.data.l}

                    </div>
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

const FIELDS = {
    TEXT: TextField,
    SELECT: SelectField,
    LOCATION: LocationField,
    BUTTONSET: ButtonSet,
    OPTIONSDEF: OptionsSet,
    FIELDRULES: RulesEditor,
    DATASOURCEDROP: DataSourceDrop,
    PERIOD: DateRangeSelector,
    INDICATOR: IndicatorField,
    COLOUR: ColourField,
    HTML: HTMLEditor,
    SOURCES: Sources
};

class Empty extends React.Component {
    render() {
        return (
            <div>Empty</div>
        )
    }
}


const ERRORS = {
    REQUIRED: "Required field"
}


class Field extends React.Component {
    static defaultProps = {
        errors: [],
        vertical: false,
        ro: false
    };

    _onChange = (prop, value, additional) => {
        this.props.onChange(this.props.data.n, value, additional);
    };

    render() {
        let Cmp = Empty;

        if (FIELDS[this.props.data.t] != null) {
            Cmp = FIELDS[this.props.data.t.toUpperCase()];
        }

        let help;
        if (this.props.data.h) {
            help = __(this.props.data.h);
        }

        let hasErrors = this.props.errors.length > 0;

        let style = {};
        if (hasErrors) {
            style.borderLeft = "3px solid #C62828";
        }


        if (this.props.data.t == "H") {
            return <Header
                vertical={this.props.vertical}
                data={this.props.data}/>;
        }

        let labelStyle = {};
        if (this.props.vertical) labelStyle.fontWeight = "normal";

        return (
            <ewars.d.Row addClass="sys-row">
                <ewars.d.Cell addClass="sys-label" style={labelStyle}>{__(this.props.data.l)}</ewars.d.Cell>
                <ewars.d.Cell addClass="sys-input" style={style}>
                    <Cmp
                        value={this.props.value}
                        name={this.props.data.n}
                        vertical={this.props.vertical}
                        ro={this.props.ro}
                        fields={this.props.definition}
                        data={this.props.data}
                        onChange={this._onChange}/>
                    {hasErrors ?
                        <div className="sys-field-errors">
                            {this.props.errors.map((item) => {
                                return (
                                    <div className="sys-field-error">{ERRORS[item]}</div>
                                )
                            })}
                        </div>
                        : null}
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

export default class SystemForm extends React.Component {
    static defaultProps = {
        definition: [],
        id: null,
        onChange: null,
        enabled: true,
        vertical: false,
        errors: [],
        ro: false
    };

    constructor(props) {
        super(props);

        this.state = {
            data: ewars.copy(this.props.data, {})
        }

    }

    componentWillReceiveProps = (nextProps) => {
        this.state.data = ewars.copy(nextProps.data);
    };

    _onChange = (prop, value, additional) => {
        if (this.props.onChange) {
            this.props.onChange(prop, value);
        } else {
            this.setState({
                ...this.state,
                data: {
                    ...this.state.data,
                    [prop]: value
                }
            })
        }
    };

    validate = () => {

    };

    isValid = () => {

    };

    getData = () => {

    };

    render() {
        let sorted = this.props.definition.sort((a, b) => {
            if (a._o > b._o) return 1;
            if (a._o < b._o) return -1;
            return 0;
        });
        let fields = sorted.map((field) => {
            let shown = true;
            if (field.c) {
                let cmp = field.c[0];
                let rules = field.c.slice(1);

                if (cmp == "ALL") {
                    let allTrue = true;
                    rules.forEach((item) => {
                        let [prop, comparison, value] = item.split(":");
                        let dt = ewars.getKeyPath(prop, this.props.data);
                        if (dt == "true") dt = true;
                        if (dt == "false") dt = false;
                        if (value == "true") value = true;
                        if (value == "false") value = false;
                        switch (comparison) {
                            case "=":
                                if (dt != value)
                                    allTrue = false;
                                break;
                            case "!=":
                                if (dt == value)
                                    allTrue = false;
                                break;
                            case ">":
                                if (dt < value)
                                    allTrue = false;
                                break;
                            default:
                                break;
                        }
                    });

                    if (!allTrue) shown = false;
                } else {
                    shown = false;
                    rules.forEach((item) => {
                        let [prop, comparison, value] = item.split(":");
                        let dt = ewars.getKeyPath(prop, this.props.data);
                        if (dt == "true") dt = true;
                        if (dt == "false") dt = false;
                        if (value == "true") value = true;
                        if (value == "false") value = false;
                        switch (comparison) {
                            case "=":
                                if (dt == value) shown = true;
                                break;
                            case "!=":
                                if (dt != value) shown = true;
                                break;
                            case ">":
                                if (dt > value) shown = true;
                                break;
                            case "<":
                                if (dt < value) shown = true;
                                break;
                            default:
                                break;
                        }
                    })
                }
            }

            if (shown) {
                let value,
                    errors = [];

                if (field.t != "H") {
                    if (field.n.indexOf(".") >= 0) {
                        value = ewars.getKeyPath(field.n, this.props.data);
                    } else {
                        value = this.props.data[field.n] != null ? this.props.data[field.n] : null;
                    }
                    if (value === null && field.t === "TEXT") value = "";

                    // Check for errors
                    this.props.errors.forEach((item) => {
                        if (item[0] == field.n) {
                            errors.push(item[1]);
                        }
                    });
                }

                return <Field
                    key={field.n}
                    ro={this.props.ro}
                    definition={this.props.definition}
                    data={field}
                    vertical={this.props.vertical}
                    errors={errors}
                    value={value}
                    onChange={this._onChange}/>
            } else {
                return null;
            }
        });

        let className = "system-form";
        if (this.props.vertical) className += " vertical";

        return (
            <div className={className}>
                <ewars.d.Layout>
                    {fields}
                </ewars.d.Layout>
            </div>
        )
    }
}