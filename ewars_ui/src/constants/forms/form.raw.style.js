const FORM_RAW_STYLE = [
    {
        _o: 0,
        n: "format",
        t: "SELECT",
        l: "Number formatting",
        o: [
            ["0", "0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0,0.0", "0,0.0"],
            ["0,0.00", "0,0.00"],
            ["0,0", "0,0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]
        ]
    },
    {
        _o: 1,
        n: "prefix",
        l: "Prefix",
        t: "TEXT"
    },
    {
        _o: 2,
        n: "suffix",
        l: "Suffix",
        t: "TEXT"
    },
    {
        _o: 3,
        n: "o_val_colour",
        t: "BUTTONSET",
        l: "Value colouring",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 4,
        n: "value_colouring",
        l: "Value colouring",
        t: "COLOURTHRESHOLDS",
        output_type: "COLOUR",
        c: [
            "ALL",
            "o_val_colour:eq:true"
        ]
    },
    {
        _o: 5,
        n: "o_val_mapping",
        t: "BUTTONSET",
        l: "Value mapping?",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 6,
        n: "value_mapping",
        t: "COLOURTHRESHOLDS",
        l: "Value mapping",
        output_type: "VALUE",
        c: [
            "ALL",
            "o_val_mapping:eq:true"
        ]
    },
    {
        _o: 7,
        n: "font_size",
        t: "TEXT",
        l: "Value text size"
    }
]

export default FORM_RAW_STYLE;