const FORM_ALERTS_MAP = [
    {
        _o: 0,
        t: "TEXT",
        n: "title",
        l: "Title"
    },
    {
        _o: 1,
        t: "SELECT",
        n: "aid",
        l: "Alarm",
        os: [
            "alarm",
            "uuid",
            "name",
            {}
        ]
    },
    {
        _o: 2,
        t: "TEXT",
        n: "limit",
        l: "Limit"
    },
    {
        _o: 3,
        n: "vis_level",
        t: "SELECT",
        l: "Visualization geolevel",
        os: [
            "location_type",
            "id",
            "name",
            {}
        ]
    },
    {
        _o: 4,
        n: "loc_constraint",
        t: "BUTTONSET",
        l: "Constrain location",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 5,
        n: "location",
        t: "LOCATION",
        l: "Location"
    }
]

export default FORM_ALERTS_MAP;