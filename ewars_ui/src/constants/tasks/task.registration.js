const TASK = {
    title: "Registration request",
    body: "A user has registered for an account, please review the details below and take an appropriate action:",
    form: [
        {
            name: "name",
            label: "Name",
            type: "display"
        },
        {
            name: "email",
            label: "Email",
            type: "display"
        },
        {
            name: "role",
            label: "Role",
            type: "role",
            required: true
        },
        {
            name: "location",
            label: "Location",
            type: "location",
            required: true
        },
        {
            name: "org_id",
            label: "Organization",
            required: true,
            type: "organization"

        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;