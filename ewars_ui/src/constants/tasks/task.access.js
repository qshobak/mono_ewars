const TASK = {
    title: "Access request",
    body: "A user has requested access to this account, please review the details below and take an appropriate action:",
    form: [
        {
            name: "role",
            label: "Role",
            options: [],
            required: true,
            order: 0
        },
        {
            name: "location_id",
            label: "Location",
            type: "location",
            required: true,
            order: 1
        },
        {
            name: "org_id",
            label: "Organization",
            required: true,
            type: "organization",
            order: 2
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;