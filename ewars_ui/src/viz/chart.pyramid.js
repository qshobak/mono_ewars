import Source from './source';
import Moment from 'moment';
import Base from './base';

import CONTROLS from './features';

import Highcharts from "highcharts";

Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
})


const _getData = (data, filters, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, filters, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

console.log()

const TYPE_MAP = {
    'DATE': 'datetime',
    'NUM': null,
    'PERC': null,
    'BOOl': 'category'
}

class PyramidChart extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();
    }

    _createSeries = () => {

    };

    render = () => {
        this.options.chart.type = 'pyramid';

        this.options.plotOptions = {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                center: ['50%', '50%'],
                width: '80%'
            }
        };

        let definition = this._config.n.filter(node => {
            return node.t != 'A' && node.t != 'X';
        })

        _getData(definition, [])
            .then(resp => {
                let measures = definition.filter(node => {
                    return ['M', 'C'].indexOf(node.t) && !node.p;
                })

                let dimension = definition.filter(node => {
                    return node.t == 'D' && !node.p;
                })[0]

                let graph = {
                    colorByPoint: true,
                    type: 'pyramid',
                    borderColor: null,
                    data: []
                }

                let dimValue = resp.m[dimension._].values;

                let dimIndex = resp.i[dimension._];
                let measIndex = resp.i[measures[0]._];

                resp.d.forEach(node => {
                    graph.data.push([
                        node[dimIndex],
                        parseFloat(node[measIndex])
                    ])
                })

                this.options.series = [graph];

                this._complete();


            })
            .catch(err => {
                console.log(err)
            })


    };

    _complete = () => {
        this._chart = Highcharts.chart(this.options);
    };

    _recvGroupData = (group, data) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };

    exportImage = () => {

    };
}

export default PyramidChart;
