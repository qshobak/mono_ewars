class ControlBand {
    constructor(chartOptions, props) {
        this.options = chartOptions;

        let found = false;
        this.options.xAxis.forEach(axis => {
            if (axis.id == props.axisId) {
                axis.plotBands = axis.plotBands ? axis.plotBands : [];
                found = true;
            }
        })

        if (!found) {
            this.options.yAxis.forEach(axis => {
                if (axis.id == props.axisId) {
                    axis.plotBands = axis.plotBands ? axis.plotBands : [];
                }
            })
        }
    }

    getOptions = () => {
        return this.options;
    }
}