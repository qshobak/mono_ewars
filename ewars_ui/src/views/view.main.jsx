import React from "react";
import AppContext from "../context_provider";

import GroupBar from "../controls/c.groupbar.jsx";

import ViewDefault from "./view.default.jsx";
import ViewTasks from "./view.tasks";
import ViewReporting from "./view.reporting";
import ViewDrafts from "./view.drafts";
import ViewDashboard from "./view.dashboard";
import ViewAlerts from "./view.alerts";
import ViewAlert from "./view.alert";
import ViewLocationReporting from "./view.location.reporting";
import ViewRecord from "./view.record";
import ViewDocuments from "./view.documents";
//const ViewIndicators = require("./view.indicators");
//const ViewExternalData = require("./view.external");
//const ViewTranslations = require("./view.translations");
//const ViewDocuments = require("./view.documents");
import ViewGrid from "./view.grid";
import ViewLog from "./view.log";
//const ViewNotebook = require("./view.notebook");
//const ViewConfig = require("./view.config");

import AssignmentSelector from "../controls/c.assignment.selector";
import UserCard from "../controls/c.card.user";
import LocationCard from "../controls/c.card.location";
import FormCard from "../controls/c.card.form";
import AlarmCard from "../controls/c.card.alarm";
import ConnectionCard from "../controls/c.card.connection";

//const ViewBrowser = require("./view.browser");
import Tabs from "../controls/c.tabs";
import FootBar from "../controls/c.footbar";

import Menu from "../menus/menu";

const VIEWS = {
    //DEV: DebugMainView,
    //CONFIG: ViewConfig,
    GRID: ViewGrid,
    DEFAULT: ViewDefault,
    DRAFTS: ViewDrafts,
    DASHBOARD: ViewDashboard,
    TASKS: ViewTasks,
    LOCATION_REPORTING: ViewLocationReporting,
    LOG: ViewLog,
    ALERT: ViewAlert,
    RECORD: ViewRecord,
    //NOTEBOOK: ViewNotebook,
    DOCUMENTS: ViewDocuments,
    BROWSE_ALERTS: ViewAlerts,
    BROWSE_RECORDS: ViewReporting,
    //INDICATORS: ViewIndicators,
    //EXTERNAL: ViewExternalData,
    //TRANSLATIONS: ViewTranslations,
    //BROWSER: ViewBrowser
};

class ViewMain extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            viewConfig: {},
            tabs: [{
                type: "DEFAULT",
                name: "Overview",
                icon: "fa-home",
                id: 0
            }],
            tabConfig: null,
            menu: "DEFAULT",
            showAssignmentSelect: false
        }
    }

    componentDidMount() {
        window.addEventListener("ADD_TAB", this._addTab, false);
        window.addEventListener("SET_TAB", this._setTab, false);
        window.addEventListener("REMOVE_TAB", this._removeTab, false);
        window.addEventListener("SHOW_ASSIGNMENT_SELECT", this._showAssignmentSelect, false);
        window.addEventListener("HIDE_ASSIGNMENT_SELECT", this._hideAssignmentSelect, false);
    }

    _hideAssignmentSelect = () => {
        this.setState({
            showAssignmentSelect: false
        })
    };

    _showAssignmentSelect = () => {
        this.setState({
            showAssignmentSelect: true
        })
    };

    _removeTab = (det) => {
        let rIndex = null;
        this.state.tabs.forEach((item, index) => {
            if (item.id == det.detail) rIndex = index;
        });

        let tabs = this.state.tabs;
        tabs.splice(rIndex, 1);
        let curTab = this.state.tabConfig;
        if (this.state.tabConfig.id == det.detail) {
            curTab = this.state.tabs[0];
        }
        this.setState({
            tabs: tabs,
            tabConfig: curTab
        });

    };

    _setTab = (id) => {
        let tab = this.state.tabs.filter((item) => {
            return item.id == id.detail;
        })[0] || null;
        if (tab) {
            this.setState({
                tabConfig: tab
            })
        }
    };

    _addTab = (tabConfig) => {
        let tabs = this.state.tabs;
        let id = new Date().getUTCMilliseconds();;
        let newTab = {
            ...tabConfig.detail,
            _id: id,
            id: id
        };
        tabs.push(newTab);
        this.setState({
            tabs: tabs,
            tabConfig: newTab
        });
    };

    _onChange = (data) => {
        this.setState(data);
    };

    _reset = () => {
        this.setState({});

    };

    render() {
        let view

        let ViewCmp = VIEWS[this.state.view];
        view = <ViewCmp config={this.state.viewConfig || {}}/>;
        if (this.state.tabConfig) {
            ViewCmp = VIEWS[this.state.tabConfig.type];
            view = <ViewCmp config={this.state.tabConfig || {}}/>;
        }

        let card;
        if (this.state.userCard) {
            card = <UserCard uuid={window.state.userCard}/>;
        }

        if (this.state.locationCard) {
            card = <LocationCard data={window.state.locationCard}/>;
        }

        if (this.state.showAssignmentSelect) {
            card = <AssignmentSelector/>;
        }

        if (this.state.accountsCard) {
            card = <AccountsCard/>;
        }

        if (this.state.connectionCard) {
            card = <ConnectionCard/>;
        }

        return (
            <div className="column">
                {card}
                <Tabs tabs={this.state.tabs} activeTab={this.state.tabConfig}/>
                {view}
                <FootBar/>
            </div>
        )

        return null
    }
}

export default ViewMain;
