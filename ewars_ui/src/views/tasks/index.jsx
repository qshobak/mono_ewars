import TaskAmendment from "task.amendment.jsx";
import TaskAssignment from "task.assignment.jsx";
import TaskLocationClose from "task.location.close.jsx";
import TaskLocationNew from "task.location.new.jsx";
import TaskOrganization from "task.organization.jsx";
import TaskRegistration from "task.registration.jsx";
import TaskRetraction from "task.retraction.jsx";
import TaskTeamCreate from "task.team.create.jsx";
import TaskTeamJoin from "task.team.join.jsx";
import TaskWorkflowRecord from "task.workflow.record.jsx";
import TaskWorkflowAlert from "task.workflow.alert.jsx";

export {
    TaskAmendment,
    TaskAssignment,
    TaskLocationClose,
    TaskLocationNew,
    TaskOrganization,
    TaskRegistration,
    TaskRetraction,
    TaskTeamCreate,
    TaskTeamJoin,
    TaskWorkflowAlert,
    TaskWorkflowRecord
}
