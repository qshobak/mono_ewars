import React from "react";
import Moment from "moment";

import ActionGroup from "../../controls/c.actiongroup.jsx";

import Highcharts from "highcharts";

const ACTIONS = [
    ['fa-table', 'BROWSE', 'BROWSE_ALERTS']
];

const DATE_SORT = (a, b) => {
    if (a[0] > b[0]) return 1;
    if (a[0] < b[0]) return -1;
    return 0;
};

const _convertToUtc = (val) => {
    let splat = val.split("-");
    return Date.UTC(splat[0], splat[1], splat[2]);
};

class AlarmIndicatorCell extends React.Component {
    render() {
        let style= {};
        if (this.props.color) {
            style.background = this.props.color;
            style.color = "#FFFFFF";
        }
        return (
            <div className="over-alarm-ind" 
                style={style}>
                <div className="over-alarm-ind-label">{__(this.props.label)}</div>
                <div className="over-alarm-ind-count">{this.props.value}</div>
                <button onClick={() => this.props.onClick(this.props)} className="btn btn-block"><i className="fal fa-table"></i></button>
            </div>
        )
    }
}

class AlarmIndicatorSparkline extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        this.chart = Highcharts.chart(this._el, {
            chart: {
                backgroundColor: null,
                borderWidth: 0,
                style: {
                    overflow: "visible"
                },
                skipClone: true
            },
            title: {text: null},
            subtitle: {text: null},
            xAxis: {
                type: "datetime",
                title: {text: null},
                startOnTick: false,
                endOnTick: false,
                tickPositions: [],
                labels: {
                    enabled: false
                }
            },
            credits: {
                enabled: false
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                title: {text: null},
                enabled: false,
                labels: {enabled: false},
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            series: [{
                name: "Alerts",
                marker: {
                    enabled: false,
                    radius: 0
                },
                data: []
            }]
        });
    }

    componentWillReceiveProps (nextProps) {
        for (var i = this.chart.series.length-1; i>=0; i--) {
            this.chart.series[i].remove();
        }
        this.chart.addSeries({
            name: "Alerts",
            data: nextProps.data || []
        })
    }

    render() {
        return (
            <div ref={(el) => this._el = el} className="over-alarm-spark">
                <i className="fal fa-cog fa-spin"></i>
            </div>
        )
    }
}

class AlarmListItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    componentDidMount() {
        window.tx("com.sonoma.location.alerts", {lid: this.props.data.uuid, aid: this.props.alarm.uuid}, (res) => {
            this.setState({
                data: res
            });
        }, (err) => {
            console.log(err);
        });
    }

    _typeClick = (def) => {
        console.log(def);
        switch(def.label) {
            case "Total Open Alerts":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        aid: this.props.alarm.uuid,
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        status: "OPEN",
                        lid: this.props.data.uuid || null,
                        status: "OPEN"
                    }
                }));
                break;
            case "In Verification":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        aid: this.props.alarm.uuid,
                        lid: this.props.data.uuid || null,
                        stage: "VERIFICATION",
                        status: "OPEN"
                    }
                }));
                break;
            case "Low Risk":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        aid: this.props.alarm.uuid,
                        lid: this.props.data.uuid || null,
                        risk: "HIGH",
                        status: "OPEN"
                    }
                }));
                break;
            case "Moderate Risk":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        aid: this.props.alarm.uuid,
                        lid: this.props.data.uuid || null,
                        risk: "MODERATE",
                        status: "OPEN"
                    }
                }));
                break;
            case "High Risk":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        aid: this.props.alarm.uuid,
                        lid: this.props.data.uuid || null,
                        risk: "HIGH",
                        status: "OPEN"
                    }
                }));
                break;
            case "Very High Risk":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_ALERTS",
                        icon: "fal fa-bell",
                        name: __(this.props.alarm.name),
                        aid: this.props.alarm.uuid,
                        lid: this.props.data.uuid || null,
                        risk: "SEVERE",
                        status: "OPEN"
                    }
                }));
                break;
            default:
                break;
        }

    };

    render() {
        console.log(this.state);
        return (
            <div className="over-alarm-view">
                <div className="loc-name">{__(this.props.data.name)}</div>

                <AlarmIndicatorCell 
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    label="Total Open Alerts"
                    value={this.state.data.total_open || 0}/>

                <AlarmIndicatorCell 
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    label="In Verification"
                    value={this.state.data.verified || 0}/>

                <AlarmIndicatorCell
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    color="#2C9640"
                    label="Low Risk"
                    value={this.state.data.risK_low || 0}/>
                
                <AlarmIndicatorCell 
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    color="#FABC00"
                    label="Moderate Risk"
                    value={this.state.data.risk_moderate || 0}/>

                <AlarmIndicatorCell 
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    color="#FD8400"
                    label="High Risk"
                    value={this.state.data.risk_high || 0}/>

                <AlarmIndicatorCell 
                    onClick={this._typeClick}
                    aid={this.props.alarm.uuid}
                    lid={this.props.data.uuid}
                    color="#FF2943"
                    label="Very High Risk"
                    value={this.state.data.risk_severe || 0}/>

                <AlarmIndicatorSparkline 
                    data={this.state.data.series || []}
                    aid={this.props.alarm.uuid} 
                    lid={this.props.data.uuid}/>
            </div>
        )
    }
}

class AlarmItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        window.tx("com.sonoma.self.locations", null, (res) => {
            console.log(res);
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        });
    }

    _action = () => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "BROWSE_ALERTS",
                aid: this.props.data.uuid,
                name: __(this.props.data.name),
                icon: "fa-bell"
            }
        }));
    };

    render() {
        return (
            <div className="report-overview">
                <div className="column">
                    <div className="row report-overview-inner">
                        <div className="column report-overview-info">
                            <div className="row block">
                                <span>{this.props.data.name.en || this.props.data.name}</span>
                            </div>
                        </div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                </div>
                <div className="row">
                    {this.state.data.map(item => {
                        return <AlarmListItem data={item} alarm={this.props.data}/>;
                    })}
                </div>
            </div>

        )
    }
}

class ViewOverviewAlarms extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.alarms.active', null, (res) => {
            this.setState({
                data: res
            })
        }, err => {
            window.state.error("ERROR_LOADING_ALARMS", err);
        })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        if (this.state.data.length <= 0) {
            return (
                <div className="column placeholder" style={{position: "relative", height: "100%", width: "100%"}}>
                    <div className="placeholder-icon">
                        <i className="fal fa-bell"></i>
                    </div>
                    <div className="placeholder-title">
                        No active alarms
                    </div>
                    <div className="placeholder-text">
                        <p>Looks like you don't have any active alarms set up.</p>
                    </div>
                </div>
            )
        }

        return (
            <div className="row block block-overflow bgd-ps-neutral">
                {this.state.data.map(item => {
                    return <AlarmItem data={item}/>
                })}

            </div>
        )
    }
}

export default ViewOverviewAlarms;
