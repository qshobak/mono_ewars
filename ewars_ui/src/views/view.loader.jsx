import React from "react";

import AppContext from "../context_provider";
import {H2, P, RowPanel, Panel, Button, Column, Row} from "../elements";

const STATE_LABELS = {
    DOWNLOADING: 'Downloading',
    LOADING: 'Loading',
    DONE: 'Complete',
    ERROR_DOWN: 'Error downloading',
    ERROR_LOAD: 'Error loading',
    ERROR_CONN: 'Error connection',
}


class LoaderView extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            loaded: [],
            timer: 0
        };

        this._queueLimit = 2;
        this._completed_seeds = [];

        this.loaders = {};
    }

    _seedCompleted = () => {
        this.props.onComplete();
    };

    _seedDone = (data) => {
        console.log("PACKAGE_SEEDED", data);
    };

    _seedDownloaded = () => {
        console.log("SEED_DOWNLOADED");
    };

    componentDidMount() {
        this.context.cx({
            type: "GetSeed",
            tki: this.context.account.tki
        }, (res) => {
            console.log(res);
            if (res.success) {
                window.dispatchEvent(new CustomEvent("SEEDED", {}));
            }
        }, (err) => {
            console.log(err)
        });
    }

    render() {
        return (
            <Row style={{background: "#333333"}}>
                <Panel style={{width: "30%", maxWidth:"30%", padding: "8px"}}>
                    <H2 color="#F2F2F2">Welcome to EWARS</H2>

                    <P color="#F2F2f2">Please wait while we download some base data in order to get you started. After this initial
                        load of data,
                        the application will progressively download data, alerts and records from the remote server onto
                        your
                        computer, while records are syncing, you can still browse and see records that are available as
                        long as you
                        have an active internet connection.</P>

                    <P color="#F2F2F2">Once downloading of these resources is complete, we'll get you into the application and you can
                        start
                        reporting, downloading data sets and more.</P>

                </Panel>
                <Column style={{background: "white", padding: "8px"}}>
                    <RowPanel>

                        <P>Requesting data from remote...</P>

                    </RowPanel>
                </Column>
            </Row>
        )
    }

}

export default LoaderView;


