import React from "react";

import ReportingItem from "../controls/c.record.jsx";
import WidgetAdminReporting from "../controls/widgets/w.admin.reporting.jsx";
import WidgetUserReporting from "../controls/widgets/w.user.reporting.jsx";
import ViewDashboardUser from "./view.dashboard.user.jsx";
import {FlatMenu} from "../menus/menu.jsx";
import AppContext from "../context_provider";

class ViewDefault extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            overall_forecast: [],
            overall_sparse: [],
            total_up_forecase: 0
        }

    }

    componentDidMount() {

        this.context.tx('com.sonoma.system.main', [], (res) => {
            if (res) {
                let overall_forecast = {};

                res.forecast_days.forEach(item => {
                    overall_forecast[item] = 0;
                });

                res.forecasts.forEach(forecast => {
                    for (let i in forecast.forecast) {
                        overall_forecast[i] = overall_forecast[i] + forecast.forecast[i];
                    }
                });

                let overall_sparse = [];
                res.forecast_days.forEach(item => {
                    overall_sparse.push(overall_forecast[item]);
                });


                this.setState({
                    data: res,
                    overall_forecast,
                    overall_sparse
                })
            }
        }, (err) => {
            console.log(err);
        })

    }

    render() {

        let view;

        switch (this.context.user.role) {
            case "USER":
                view = <ViewDashboardUser/>;
                break;
            case "ACCOUNT_ADMIN":
                view = <ViewDashboardUser/>;
                break;
            case "REGIONAL_ADMIN":
                view = <ViewDashboardUser/>;
                break;
        }

        return (
            <div className="row">
                <FlatMenu/>
                <div className="column">
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewDefault;
