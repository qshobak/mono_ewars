/// #if DESKTOP
//const elerem = require("electron").remote;
//const dialog = elerem.dialog;
//const fs = require('fs');
/// #endif

import React from "react";
import Moment from "moment";

import FormUtils from "../utils/form_utils";
import Form from "../controls/forms/form.reporting.jsx";
import ActionGroup from '../controls/c.actiongroup.jsx';
import Discussion from "../controls/c.discussion.jsx";
import Log from "../controls/c.log.jsx";
import Location from "../controls/c.location.jsx";
import UserList from "../controls/c.userslist.jsx";
import Guidance from "../controls/c.guidance.jsx";
import utils from "../utils/utils";

const DRAFT_RECORD_ACTIONS = [
    ["fa-share", "SUBMIT", "Submit record"],
    ['fa-save', 'SAVE', 'Save record']
];

const SUBMITTED_RECORD_ACTIONS = [
    ['fa-pencil', "AMEND", "Amend record"],
    '-',
    ['fa-trash', 'DELETE', 'Delete record'],
];

const AMEND_MODE_ACTIONS = [
    ['fa-share', 'SUBMIT_AMEND', 'Submit amendment'],
    '-',
    ["fa-times", 'CANCEL_AMEND', "Cancel amendment"]
];


class ViewRecord extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            form: null,
            data: {
                fid: this.props.config.uuid,
                data: {
                    __lid__: this.props.config.lid || undefined,
                    __dd__: this.props.config.dd || undefined
                }
            },
            mode: "DEFAULT",
            errors: {},
            view: "DEFAULT"
        }
    }

    componentDidMount() {

    }

    // This is fairly complicated as we need to fetch the state from the global register before
    // we load anything.
    componentWillReceiveProps(nextProps) {
        if (nextProps.config.record_id) {
            if (nextProps.config.record_id != this.props.config.record_id) {
                this.setState({
                    form: null,
                    data: {
                        fid: nextProps.config.uuid,
                        data: {
                            __lid__: nextProps.config.lid || undefined,
                            __dd__: nextProps.config.dd || undefined
                        }
                    },
                    mode: "DEFAULT",
                    errors: {},
                    view: "DEFAULT"
                }, () => {
                    if (nextProps.config.form_id != this.props.config.form_id) {
                        this._getForm(nextProps.config.form_id, nextProps.config.record_id || null);
                    } else if (nextProps.config.record_id) {
                        if (nextProps.config.record_id != this.props.config.record_id) {
                            this._getRecord(nextProps.config.record_id);
                        }
                    }
                });
            } else if (nextProps.config._id != this.props.config._id) {
                this.setState({
                    form: null,
                    data: {
                        fid: nextProps.config.uuid,
                        data: {
                            __lid__: nextProps.config.lid || undefined,
                            _dd__: nextProps.config.dd || undefined
                        }
                    },
                    mode: "DEFAULT",
                    errors: {},
                    view: "DEFAULT"
                }, () => {
                    if (nextProps.config.form_id != this.props.config.form_id) {
                        this._getForm(nextProps.config.form_id, nextProps.config.record_id || null);
                    } else if (nextProps.config.record_id) {
                        if (nextProps.config.record_id != this.props.config.record_id) {
                            this._getRecord(nextProps.config.record_id);
                        }
                    }
                });
            }
        }
    }

    componentWillUnmount() {
        if (!window.CACHE) window.CACHE = {};
        window.CACHE[`RECORD_${this.props.config._id}`] = this.state;
    }

    componentDidMount() {
        this._getForm(this.props.config.form_id, this.props.config.record_id || null);
    }

    _getForm = (form_id, record_id) => {
        window.sonomaClient.tx("com.sonoma.form", form_id, (form_res) => {
            if (record_id) {
                console.log(record_id);
                window.sonomaClient.tx("com.sonoma.record", record_id, (rres) => {
                    this.setState({
                        data: rres,
                        form: form_res || null
                    })
                }, (err) => console.log(err));
            } else {
                let form = form_res || null;
                this.setState({
                    form: form,
                    data: {
                        fid: form.uuid,
                        data: {
                            __lid__: this.props.config.lid || undefined,
                            __dd__: this.props.config.dd || undefined
                        }
                    }
                })
            }
        }, (err) => console.log(err));
    };

    _getRecord = (record_id) => {
        window.sonomaClient.tx("com.sonoma.record", record_id, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        })
    };

    _onChange = (prop, value, root) => {
        if (root) {
            this.setState({
                ...this.state,
                data: {
                    ...this.state.data,
                    [prop]: value
                }
            })
        } else {
            this.setState({
                ...this.state,
                data: {
                    ...this.state.data,
                    data: {
                        ...this.state.data.data,
                        [prop]: value
                    }
                }
            })
        }
    };

    _save = () => {

    };

    _deleteRecord = () => {
        window.state.dialog({
            title: "DELETE_RECORD",
            body: "DELETE_RECORD_BODY",
            buttons: [
                'DELETE',
                'CANCEL'
            ]
        }, (res) => {
            if (res == 0) {
                let bl = window.state.blocker("DELETING_RECORD");
                window.sonomaClient.tx("com.sonoma.record.retract", this.state.data.uuid, (res) => {
                    bl.rm();
                    window.state.growl("fa-trash", "RECORD_DELETED");
                }, (err) => {
                    bl.rm();
                    window.state.error("ERROR", err);
                })
            }
        });
    };

    _submit = () => {
        let errors = FormUtils.validate(this.state.form, this.state.data);

        if (Object.keys(errors).length > 0) {
            this.setState({
                errors: errors
            });
            return;
        }

        window.sonomaClient.tx("com.sonoma.record.submit", this.state.data, (res) => {
            window.state.growl('fa-save', "Record submitted");
            this.setState({
                data: res
            });
        }, (err) => {
            window.state.error("There was an error submitting this record", err);
        })

    };

    _action = (action, data) => {
        switch (action) {
            case "SAVE":
                this._saveRecord();
                break;
            case "SHARE":
                break;
            case "PRINT":
                break;
            case "EXPORT":
                this._export();
                break;
            case "SUBMIT":
                this._submit();
                break;
            case "AMEND":
                this._setAmendMode();
                break;
            case "CANCEL_AMEND":
                this._revertAmendMode();
                break;
            case "SUBMIT_AMEND":
                this._submitAmend();
                break;
            case "DELETE":
                this._deleteRecord();
                break;
            default:
                break;
        }
    };

    _saveRecord = () => {

    };

    _updateRecord = () => {

    };

    _export = () => {
        let data = JSON.stringify(this.state.data);
        dialog.showSaveDialog({
                title: "Save record",
                filters: [
                    {
                        name: "EWARS record",
                        extension: "ewars_record"
                    }
                ]
            },
            function (filePath) {
                fs.writeFileSync(filePath, data, function (err) {
                    if (err) console.error(err);
                });
            });
    };

    _revertAmendMode = () => {
        this.setState({
            tmp: null,
            data: JSON.parse(JSON.stringify(this.state.tmp)),
            mode: "DEFAULT"
        })
    };

    _submitAmend = () => {
        let errors = FormUtils.validate(this.state.form, this.state.data);

        if (Object.keys(errors).length > 0) {
            this.setState({
                errors: errors
            });
            return;
        }

        let data = {
            uuid: this.state.data.uuid,
            location_id: this.state.data.location_id || null,
            data_date: this.state.data.data_date || null,
            data: this.state.data.data || {},
            reason: this.state.reason || "No reason"
        };

        window.sonomaClient.tx("com.sonoma.record.amend", data, (res) => {
            this.setState({
                data: res,
                tmp: null,
                mode: "DEFAULT"
            }, () => {

                new Notification("Record Amended", {
                    body: "Your record amendment was saved",
                    silent: true,
                    sound: false
                })
            })
        }, (err) => {
            console.log(err);
        })
    };


    _setAmendMode = () => {
        this.setState({
            tmp: JSON.parse(JSON.stringify(this.state.data)),
            data: JSON.parse(JSON.stringify(this.state.data)),
            mode: "AMEND"
        })
    };

    _setView = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        if (!this.state.form) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        let form = utils.copy(this.state.form);

        let flattened = FormUtils.flatten(form.definition);
        let headers = []

        let readOnly = false;
        let RECORD_ACTIONS = DRAFT_RECORD_ACTIONS;
        if (this.state.data.status == 'SUBMITTED') {
            readOnly = true;
            RECORD_ACTIONS = SUBMITTED_RECORD_ACTIONS;
        }

        if (this.state.mode == "AMEND") {
            readOnly = false;
            RECORD_ACTIONS = AMEND_MODE_ACTIONS;
        }

        let view;

        if (this.state.view == "DEFAULT") {
            headers = flattened.filter(item => {
                return item.type.toUpperCase() == "HEADER";
            });
            view = (
                <div className="column">
                    <div className="row bb record-toolbar" style={{maxHeight: "34px", height: "34px"}}>
                        <div className="column grid-title" style={{flex: 2}}>
                            {this.state.form.name.en || this.state.form.name}

                            {this.state.data.uuid ?
                                <div style={{marginTop: "3px", fontSize: "0.9rem"}}>
                                    Submitted
                                    by: {this.state.data.submitter_name || ""} on {Moment.utc(this.state.data.submitted).local().format("YYYY-MM-DD HH:mm:ss")}
                                </div>
                                : null}
                        </div>
                        <ActionGroup
                            actions={RECORD_ACTIONS}
                            onAction={this._action}/>

                    </div>
                    <div className="row bgd-ps-neutral">
                        <Form
                            readOnly={readOnly}
                            data={this.state.data}
                            fid={this.state.form.uuid}
                            showErrorBar={true}
                            errors={this.state.errors}
                            features={this.state.form.features}
                            workflow={form.stages || []}
                            definition={form.definition}
                            onChange={this._onChange}/>
                    </div>
                </div>
            )
        }

        if (this.state.view == "COMMENTS") view = <Discussion data={this.state.data.comments || []}/>;
        if (this.state.view == "HISTORY") view = <Log data={this.state.data.history}/>;
        if (this.state.view == "LOCATION") view = <Location uuid={this.state.data.location_id}/>;
        if (this.state.view == "USERS") view = <UserList type="RECORD" ref_id={this.state.data.uuid}/>;
        if (this.state.view == "GUIDANCE") view = <Guidance data={this.state.form}/>;

        return (
            <div className="row">
                {false ?
                <div className="column block"
                     style={{maxWidth: "35px", width: "35px", position: 'relative', overflow: "hidden"}}>

                    <div className="ide-tabs">
                        <div
                            onClick={() => this._setView("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-clipboard"></i>&nbsp;Form
                        </div>
                        <div
                            onClick={() => this._setView("GUIDANCE")}
                            className={"ide-tab" + (this.state.view == 'GUIDANCE' ? " ide-tab-down" : "")}>
                            <i className="fal fa-book"></i>&nbsp;Guidance
                        </div>
                        {this.state.data.uuid ?
                            <div
                                onClick={() => this._setView("HISTORY")}
                                className={"ide-tab " + (this.state.view == "HISTORY" ? " ide-tab-down" : "")}>
                                <i className="fal fa-clock"></i>&nbsp;History
                            </div>
                            : null}
                        {this.state.data.uuid ?
                            <div onClick={() => this._setView("DISCUSSION")}
                                 className={"ide-tab " + (this.state.view == "DISCUSSION" ? " ide-tab-down" : "")}>
                                <i className="fal fa-comment"></i>&nbsp;Comments
                            </div>
                            : null}
                    </div>

                </div>
                : null}
                {view}
            </div>
        )
    }
}

export default ViewRecord;
