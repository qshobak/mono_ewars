import React from "react";
import Moment from "moment";
import AppContext from "../context_provider";

//const {remote} = require("electron");
//const {Menu, MenuItem} = remote;

import ActionGroup from "../controls/c.actiongroup.jsx";
import DateUtils from "../utils/date_utils";
import AssignmentSelector from "../controls/c.assignment.selector.jsx";

import ViewOverviewAssignments from "./overview/view.overview.assignments.jsx";
import ViewOverviewAlerts from "./overview/view.overview.alerts.jsx";
import ViewOverviewDue from "./overview/view.overview.due.jsx";
import ViewOverviewReporting from "./overview/view.overview.reporting.jsx";
import ViewOverviewDrafts from "./overview/view.overview.drafts.jsx";
import ViewOverviewAlarms from "./overview/view.overview.alarms.jsx";
import ViewOverviewOutbreaks from "./overview/view.overview.outbreaks.jsx";
import ViewUserMain from "./view.user.main.jsx";
import ViewAuditor from "./view.auditor.mne.jsx";

import AlertsPlaceholder from "../placeholders/p.alerts.jsx";


class PeerList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [
                {
                    uuid: null,
                    name: "Jeff Uren",
                    email: "jdurne@gmail.com",
                    role: "ACCOUNT_ADMIN"
                }
            ]
        }
    }

    _click = (uuid) => {
        window.state.showUserCard(uuid);
    };

    render() {
        return (
            <div className="row block block-overflow">
                {this.state.data.map(item => {
                    return (
                        <div className="card-item" onClick={() => this._click(item.uuid)}>
                            <span><strong>{item.name}</strong></span>
                            <span>{item.email}</span>
                            <span>{item.role}</span>
                        </div>
                    )
                })}
            </div>
        )
    }
}

class OrganizationList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row block block-overflow">

            </div>
        )
    }
}

class LocationItem extends React.Component {
    _click = () => {
        window.state.showLocationCard(this.props.data.uuid);
    };

    _context = () => {
        let menu = new Menu();

        menu.append(
            new MenuItem({
                label: "View Records",
                click: () => {
                    console.log("HERE");
                }
            })
        );
        menu.append(new MenuItem({
            label: "View Profile",
            click: () => {
                window.state.showLocationCard(this.props.data.uuid);
            }
        }));
        menu.append(
            new MenuItem({
                type: "separator"
            })
        );

        menu.append(
            new MenuItem({
                label: "Delete Assignment",
                click: () => {
                    console.log("HERE")
                }
            })
        );

        menu.popup({window: remote.getCurrentWindow()});
    };

    render() {
        let style = {borderLeftColor: "transparent"};
        if (this.props.data.status == "ACTIVE") style.borderLeftColor = "green";
        if (this.props.data.status == "INACTIVE") style.borderLeftColor = "lightslategrey";

        return (
            <div
                className="card-item" style={style}
                onContextMenu={this._context}
                onClick={this._click}>
                <span><strong>{this.props.data.name.en || this.props.data.name}</strong></span>
                <span>{this.props.data.lti_name.en || this.props.data.lti_name}</span>
            </div>
        )
    }
}

class LocationsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            showRequestor: false
        }

        window.sonomaClient.tx('com.sonoma.self.locations', [], (res) => {
            this.setState({data: res})
        }, (err) => {
            console.log(err);
        })
    }

    _request = () => {
        window.state.showAssignmentRequest();
    };

    render() {

        return (
            <div className="column" id="loc-box">
                <div className="row block block-overflow">
                    <button onClick={this._request}
                            className="block-button">
                        <i className="fal fa-plus"></i>&nbsp;Add location
                    </button>
                    <div className="block-button" onClick={this._request}>
                        <i className="fal fa-plus"></i>&nbsp;Add Location
                    </div>
                    {this.state.data.map(item => {
                        return <LocationItem data={item}/>
                    })}
                </div>
                {this.state.showRequestor ?
                    <AssignmentSelector/>
                    : null}
            </div>
        )
    }
}

class FormsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }

        window.sonomaClient.tx('com.sonoma.self.locations', [], (res) => {
            this.setState({data: res})
        }, (err) => {
            console.log(err);
        })
    }

    render() {

        return (
            <div className="column" id="loc-box">
                <div className="row block block-overflow">
                    <div className="block-button">
                        <i className="fal fa-plus"></i>&nbsp;Add Form
                    </div>
                    {this.state.data.map(item => {
                        return <LocationItem data={item}/>
                    })}
                </div>
            </div>
        )
    }
}

const STAGES = {
    WELCOME: {
        title: "Welcome",
        icon: "fa-map-signs",
        type: "content",
        next: "MENU",
        content: [
            ['h2', "Let's get started"],
            ['p', 'EWARS is a multi-platform data collection, surveillance and analysis tool intended to be used for managing humanitarian emergencies such as disease outbreaks, health crisis and natural emergencies.'],
            ['p', 'The application is made up of a variety of tools that make beginning reporting for emergencies fast and easy with minimal roadblocks in order to expedite data-based decision making in the field and at higher governmental and NGo levels.'],
            ['p', 'This tour will get you situated in the EWARS desktop application, you can retake this tour at any time by clicking Help > Tour in the main menu, or for more in-depth guidance you can open EWARS guidance by clicking Help > Guidance.'],
            ['button', 'Start tour', "ADVANCE"],
            ['button', 'Cancel', "CANCEL"]
        ]
    },
    MENU: {
        title: "Main Menu",
        icon: "fa-list",
        type: "mask",
        element: "son-main-menu",
        next: "LOCATIONS",
        pos: {
            right: "-300px",
            top: "60px"
        },
        content: [
            ['p', 'This is the main menu, it provides quick access to the main areas of the application']
        ]
    },
    LOCATIONS: {
        title: "Locations",
        icon: "fa-map-marker",
        type: "mask",
        element: "loc-box",
        next: "REPORTING",
        pos: {
            left: "-300px",
            top: "60px"
        },
        content: [
            ['p', 'Location assignments that you currently have can be seen here, to request a new one you simply click the "+" symbol and specify the location that you would like to have access to']
        ]
    },
    REPORTING: {
        title: "Reporting",
        icon: "fa-clipboard",
        type: "masK",
        pre: () => {
            return new Promise((resolve, reject) => {
                window.state.menu = "REPORTING";
                window.state.emit("CHANGE", null, () => {
                    resolve();
                });
            })
        },
        element: "menu-popout",
        next: "ALERTS",
        pos: {
            right: "-300px",
            top: "60px"
        },
        content: [
            ['p', "This is the reporting menu, frmo here you can access drafts and any quickly create/browse records for any forms you have access to"]
        ]
    },
    ALERTS: {
        title: "Alerts",
        icon: "fa-bell",
        type: "mask",
        pre: () => {
            return new Promise((resolve, reject) => {
                window.state.menu = "ALERTS";
                window.state.emit("CHANGE", null, () => {
                    resolve();
                })
            })
        },
        element: "menu-popout",
        next: "REPORTING_OVERVIEW",
        pos: {
            right: "-300px",
            top: "60px"
        },
        content: [
            ['p', 'This is the alerts menu, from here you can access current and past alerts for alert types and locations that you have access to.']
        ]
    },
    REPORTING_OVERVIEW: {
        title: "Reporting Overview",
        icon: "fa-clipboard",
        type: "mask",
        pre: () => {
            return new Promise((resolve, reject) => {
                window.state.menu = null;
                window.state.emit("CHANGE", null, () => {
                    resolve();
                })
            })
        },
        element: "reporting-overview",
        next: "END",
        pos: {
            right: "60px",
            top: "-90px"
        },
        content: [
            ['p', 'This the reporting overivew, get a top-level glance and what you need to be working on and performance metrics for each location you have access to.']
        ]
    },
    END: {
        title: "Alerts Overview",
        icon: "fa-bell",
        actions: [],
        type: "content",
        content: [
            ['h2', "That's It!"],
            ['p', "That's it for the quick introduction, here a few more tours you can take to get yourself used to how the EWARS system works"],
            ['button', 'Finish', 'CANCEL']
        ]
    }
};


class TourContentItem extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = 1;
        }, 100);
    }

    _hit = () => {
    }

    render() {
        return (
            <div className="tour-content-outer">
                <div className="tour-content"
                     ref={(el) => this._el = el}
                     style={{transition: "all 0.4s ease-out", transform: "scale(0)", opacity: "0"}}>
                    {this.props.children}
                    <button onClick={this._hit}>Start Tour</button>
                </div>
            </div>
        )
    }
}

class MaskTourItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            top: 0,
            left: 0,
            width: 0,
            height: 0
        }
    }

    componentWillMount() {
        let targetNode = document.getElementById(this.props.data.element);
        let width = targetNode.offsetWidth,
            height = targetNode.offsetHeight,
            left = targetNode.offsetLeft,
            top = targetNode.offsetTop;

        this.state.top = top + "px";
        this.state.left = left + "px";
        this.state.width = width + "px";
        this.state.height = height + "px";
    }

    componentWillReceiveProps(nextProps) {
        let targetNode = document.getElementById(nextProps.data.element);
        if (targetNode) {
            let width = targetNode.offsetWidth,
                height = targetNode.offsetHeight,
                left = targetNode.offsetLeft,
                top = targetNode.offsetTop;

            this.setState({
                top: top + "px",
                left: left + "px",
                width: width + "px",
                height: height + "px"
            }, () => {
                console.log("HERE", this._el);
                if (this._el) this._el.style.transfrom = "scale(1)";
            });
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = 1;
        }, 10);
    }

    _next = () => {
        this.props.onNext();
        this._el.removeEventListener("transitionend", this._next);
    };

    _onClick = () => {
        this._el.addEventListener("transitionend", this._next, false);
        this._el.style.transform = "scale(0)";
        this._el.style.opacity = 0;
    };

    render() {

        let items = [];

        this.props.data.content.forEach(item => {
            switch (item[0]) {
                case "p":
                    items.push(<p>{item[1]}</p>);
                    break;
                case "h2":
                    items.push(<h2>{item[1]}</h2>);
                    break;
                case "h4":
                    items.push(<h4>{item[1]}</h4>);
                    break;
                case "button":
                    items.push(<button onClick={() => this._click(item[2])}>{item[1]}</button>);
                    break;
                default:
                    break;
            }
        });

        return (
            <div className="tour-mask" style={this.state}>
                <div className="tour-mask-node"
                     ref={(el) => this._el = el}
                     style={{...this.props.data.pos, transform: "scale(0)", transition: "all 0.3s ease-out"}}
                     onClick={this._onClick}>
                    <div className="circle">
                        <div className="arrow"></div>
                        <i className={"fal " + this.props.data.icon}></i>
                    </div>
                    <div className="text">
                        {items}
                    </div>
                </div>
            </div>
        )
    }
}

class Introduction extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tour: false,
            stage: null,
            stages: [],
            dismiss: false
        };
    }

    _startTour = () => {
        this.setState({
            tour: true,
            stage: "WELCOME"
        })
    };

    _next = () => {
        let curStage = STAGES[this.state.stage];
        let nextStage = STAGES[curStage.next];

        if (nextStage.pre) {
            nextStage.pre()
                .then(() => {
                    console.log("HERE");
                    this.setState({
                        stage: curStage.next
                    })
                })
        } else {
            this.setState({
                stage: curStage.next
            });
        }
    };

    _startTutorial = () => {
        window.state.startTutorial();
    };

    _click = (action) => {
        switch (action) {
            case "CANCEL":
                this.setState({
                    stage: null,
                    tour: false
                });
                break;
            case "ADVANCE":
                let curStage = STAGES[this.state.stage];
                let nextStage = STAGES[curStage.next];

                if (nextStage.pre) {
                    nextStage.pre()
                        .then(() => {
                            this.setState({
                                stage: curStage.next
                            })
                        })
                } else {
                    this.setState({
                        stage: curStage.next
                    });
                }
                break;
            default:
                break;
        }
    };

    _guidance = () => {
        window.state.addTab("KNOWLEDGEBASE", {
            name: "Guidance"
        })
    };

    _dismissDone = () => {
        this._el.removeEventListener("transitionend", this._dismissDone);
        this.setState({
            dismiss: true
        })
    };

    _dismiss = () => {
        this._el.addEventListener("transitionend", this._dismissDone);
        this._el.style.transform = "scale(0)";
        this._el.style.opacity = "0";
        window.state.setHideTour();
    };

    render() {
        if (this.state.dismiss) return null;
        let stage;

        if (this.state.tour) {
            let stageDef = STAGES[this.state.stage];

            switch (stageDef.type.toLowerCase()) {
                case "content":
                    let items = [];
                    stageDef.content.forEach(item => {
                        switch (item[0]) {
                            case "p":
                                items.push(<p>{item[1]}</p>);
                                break;
                            case "h2":
                                items.push(<h2>{item[1]}</h2>);
                                break;
                            case "button":
                                items.push(<button onClick={() => this._click(item[2])}>{item[1]}</button>);
                                break;
                            default:
                                break;
                        }
                    });

                    stage = (
                        <TourContentItem key={this.state.stage}>
                            {items}
                        </TourContentItem>
                    );
                    break;
                case "mask":
                    stage = <MaskTourItem
                        key={this.state.stage}
                        onNext={this._next}
                        data={stageDef}/>;
                    break;
                default:
                    break;
            }
        }

        if (this.state.tour) {
            return (
                <div className="introduction">
                    <div className="tour-overlay">
                        {stage}
                    </div>
                </div>
            )
        }

        return (
            <div className="introduction"
                 ref={(el) => this._el = el}
                 style={{background: "transparent", transition: "all 0.3s ease-out"}}>
                <div className="introduction-inner">
                    <div className="row">
                        <div className="column icon">
                            <i className="fal fa-map-signs" style={{fontSize: "10.5rem"}}></i>
                        </div>
                        <div className="column block" style={{paddingTop: "26px", paddingBottom: "26px"}}>
                            <p className="title">Welcome to EWARS</p>

                            <p className="body">If this is your first time here, you can either dive in below or let us
                                take you for a tour through the application to get you started.</p>

                            <div className="row cube-buttons">
                                <div className="cube-button" onClick={this._startTour}>
                                    <i className="fal fa-map-signs"></i>
                                    <label htmlFor="Quick Tour">Quick Tour</label>
                                </div>
                                <div className="cube-button" onClick={this._guidance}>
                                    <i className="fal fa-book"></i>
                                    <label htmlFor="">Guidance</label>
                                </div>
                                <div className="cube-button" onClick={this._startTutorial}>
                                    <i className="fal fa-hands-helping"></i>
                                    <label htmlFor="Guided Tutorial">Guided Tutorial</label>
                                </div>
                            </div>

                            <button style={{float: "right", marginRight: "0"}} onClick={this._dismiss}><i
                                className="fal fa-times"></i>&nbsp;Dismiss
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


class ViewDashboardUser extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);
        this.state = {
            view: null,
            sideView: "LOCATIONS",
            data: []
        }
    }

    componentDidMount() {

        let view = "ASSIGNMENTS";
        if (this.context.user.role == "ACCOUNT_ADMIN") view = "REPORTING";
        if (this.context.user.role == "REGIONAL_ADMIN") view = "REPORTING";
        this.setState({view: view});
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    _sideView = (view) => {
        this.setState({
            sideView: view
        })
    };

    render() {
        let sideView, view;

        if (this.state.sideView == "LOCATIONS") sideView = <LocationsList/>;
        if (this.state.sideView == "FORMS") sideView = <FormsList/>;
        if (this.state.sideView == "USERS") sideView = <PeerList/>;

        if (this.state.view == "MAIN") view = <ViewUserMain/>
        if (this.state.view == "ASSIGNMENTS") view = <ViewOverviewAssignments/>;
        if (this.state.view == "REPORTING") view = <ViewOverviewReporting/>;
        if (this.state.view == "ALERTS") view = <ViewOverviewAlerts/>;
        if (this.state.view == "ALARMS") view = <ViewOverviewAlarms/>;
        if (this.state.view == "DUE") view = <ViewOverviewDue/>;
        if (this.state.view == "DRAFTS") view = <ViewOverviewDrafts/>;
        if (this.state.view == "OUTBREAKS") view = <ViewOverviewOutbreaks/>;
        if (this.state.view == "AUDITING") view = <ViewAuditor/>;

        return (
            <div className="row">
                <div className="column">
                    <div className="row block bb"
                         style={{
                             maxHeight: "30px",
                             minHeight: "30px",
                             padding: "4px 4px 0 4px",
                             background: "rgba(0,0,0,0.05)"
                         }}>
                        {["REGIONAL_ADMIN", "ACCOUNT_ADMIN"].indexOf(this.context.user.role) >= 0 ?
                            <div onClick={() => this._view("REPORTING")}
                                 className={"clicker" + (this.state.view == "REPORTING" ? " active" : "")}><i
                                className="fal fa-clipboard"></i>&nbsp;Reporting</div>
                            : null}
                        {this.context.user.role == "USER" ?
                            <div onClick={() => this._view("ASSIGNMENTS")}
                                 className={"clicker" + (this.state.view == 'ASSIGNMENTS' ? " active" : "")}><i
                                className="fal fa-clipboard"></i>&nbsp;Reporting
                            </div>
                                : null}
                        {false ?
                        <div onClick={() => this._view("OUTBREAKS")} className={"clicker" + (this.state.view == "OUTBREAKS" ? " active" : "")}>
                            <i className="fal fa-ambulance"></i>&nbsp;Outbreaks
                        </div>
                        : null}
                        {this.context.user.role == "USER" ?
                            <div onClick={() => this._view("ALERTS")}
                                 className={"clicker" + (this.state.view == "ALERTS" ? " active" : "")}><i
                                className="fal fa-bell"></i>&nbsp;Alerts
                            </div>
                            : null}
                        {['ACCOUNT_ADMIN', "REGIONAL_ADMIN"].indexOf(this.context.user.role) >= 0 ?
                            <div onClick={() => this._view("ALARMS")}
                                 className={"clicker" + (this.state.view == "ALARMS" ? " active" : "")}>
                                <i className="fal fa-bell"></i>&nbsp;Alerts
                            </div>
                                : null}
                        {false ?
                        <div onClick={() => this._view("DRAFTS")}
                             className={"clicker" + (this.state.view == "DRAFTS" ? " active" : "")}>
                            <i className="fal fa-file"></i>&nbsp;Drafts
                        </div>
                        : null}

                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewDashboardUser;
