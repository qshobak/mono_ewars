import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-plus', 'CREATE', 'Create new'],
    ['fa-upload', 'IMPORT', 'Import']
];

const ITEM_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit'],
    ['fa-download', 'EXPORT', 'Export'],
    ['fa-trash', 'DELETE', 'Delete']
]

const RESOURCES = {
    NOTEBOOKS: {
        title: "Notebooks",
        actions: function (action, data) {

        },
        onOpen: function (data) {

        },
        cmd: "com.sonoma.notebooks"
    },
    PLOTS: {
        title: "Plots",
        actions: function (action, data) {

        },
        onOpen: function (data) {

        },
        cmd: "com.sonoma.plots"
    },
    MAPS: {
        title: "Maps",
        actions: function (action, data) {

        },
        onOpen: function (data) {

        },
        cmd: "com.sonoma.maps"
    },
    PIVOTS: {
        title: "Pivots",
        actions: function (action, data) {

        },
        onOpen: function (data) {

        },
        cmd: "com.sonoma.pivots"
    },
    HUDS: {
        title: "HUDs",
        actions: function (action, data) {

        },
        onOpen: function (data) {

        },
        cmd: "com.sonoma.huds"
    }
};

class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-handle">
                <div className="row">
                    <div className="column">{this.props.data.name}</div>
                    <ActionGroup
                        actions={ITEM_ACTIONS}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class ItemBrowser extends React.Component {
    static defaultProps = {
        config: {
            type: "NOTEBOOKS"
        }
    };

    state = {
        data: []
    };

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        window.sonomaClient.tx(RESOURCES[this.props.config.type].cmd, {limit: 100}, (res) => {
            this.setState({
                data: res
            })
        }, (err) => console.log(err));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.type != this.props.config.type) {
            window.sonomaClient.tx(RESOURCES[nextProps.config.type].cmd, {limit: 100}, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => console.log(err));
        }
    }

    render() {
        let definition = RESOURCES[this.props.type];

        let view;

        return (
            <div className="row">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow">
                        {this.state.data.map(item => {
                            return (
                                <Item
                                    data={item}
                                    onSelect={this._onSelect}
                                    item_type={definition}/>
                            )
                        })}
                    </div>
                </div>
                <div className="column">
                </div>
            </div>
        )
    }
}

export default ItemBrowser;