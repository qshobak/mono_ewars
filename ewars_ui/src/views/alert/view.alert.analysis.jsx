import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ["fa-download", "DOWNLOAD", "DOWNLOAD_DATA"]
];

class ViewAlertTrendChart extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="column br bb bl bt" style={{margin: "8px", background: "white"}}>
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                        <div className="column grid-title">Trend</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block" style={{minHeight: "200px"}}>

                    </div>
                    <div className="row bt" style={{maxHeight: "30px", padding: "5px"}}>
                        <div className="column block">
                            <i className="fal fa-square"></i>&nbsp;Trend @ Trigger&nbsp;|&nbsp;
                            <i className="fal fa-square"></i>&nbsp;Current Trend
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ViewAlertDataTable extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="column br bb bl bt" style={{margin: "8px", background: "white"}}>
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                        <div className="column grid-title">Data</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow" style={{minHeight: "200px"}}>
                        <table width="100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Value @ alert time</th>
                                <th>Current value</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

class ViewAlertAnalysis extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow" style={{ background: "rgb(243, 243, 243)"}}>
                <ViewAlertTrendChart/>
                <ViewAlertDataTable/>
            </div>
        )
    }
}

export default ViewAlertAnalysis;