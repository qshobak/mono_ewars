import React from "react";
import Moment from "moment";

import Form from "../../controls/forms/form.reporting.jsx";
import ActionGroup from "../../controls/c.actiongroup.jsx";

import DEFAULT_WORKFLOW from "../../constants/forms/form.alert.default";

const ACTIONS = [
    ["fa-share", 'SUBMIT', "SUBMIT_ACTION"]
];

const DONE_ACTIONS = [
    ['fa-pencil', 'EDIT', 'EDIT_STAGE']
];


class Stage extends React.Component {
    state = {
        data: {},
        errors: {},
        completed: false
    };

    constructor(props) {
        super(props)
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "SUBMIT":
                this._submit();
                break;
            default:
                break;
        }
    };

    _submit = () => {
        // TODO: Validate

        let bl = window.state.blocker("UPDATING_ALERT");
        window.sonomaClient.tx("com.sonoma.alert.action", [this.props.data.uuid, this.state.data], (res) => {
            bl.rm();
            window.dispatchEvent(new CustomEvent("reloadalert"));
            console.log(res);
        }, (err) => {
            bl.rm();
            console.log(err);
        })
    };

    render() {
        // TODO: Figure out the form
        let form_definition = {};

        for (let i in this.props.fields) {
            let field = this.props.fields[i];
            if ((field.stages || []).indexOf(this.props.definition.uuid) >= 0) form_definition[i] = field;
        }

        return (
            <div className="alert-stage bt br bb bl shadowed" style={{background: "white"}}>
                <div className="column">
                    <div className="row gen-toolbar bb" style={{maxHeight: "34px", minHeight: "34px"}}>
                        <div className="column grid-title">{this.props.definition.name}</div>
                        {!this.props.completed ?
                            <ActionGroup
                                actions={ACTIONS}
                                onAction={this._action}/>
                            : null}
                    </div>
                    <div className="row block" style={{minHeight: "100px"}}>
                        <Form
                            showErrorBar={false}
                            readOnly={this.props.completed}
                            definition={form_definition}
                            onChange={this._onChange}
                            data={this.state.data || {}}
                            allRoot={true}
                            errors={{}}/>
                    </div>
                </div>
            </div>
        )
    }
}

class StageCompleted extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-stage bt br bb bl">
                <div className="column">
                    <div className="row gen-toolbar bb" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">[stage title]</div>
                        <ActionGroup
                            actions={DONE_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block">

                    </div>
                </div>
            </div>
        )
    }
}

class Comment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-comment"></div>
        )
    }
}

class Event extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-event">

            </div>
        )
    }
}

class CommentInput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column" style={{maxWidth: "30px"}}></div>
                <div className="column">
                    <i className="fal fa-plus"></i>
                </div>
                <div className="column">
                    <input type="text"/>
                </div>
                <div className="column" style={{maxWidth: "30px"}}></div>
            </div>
        )
    }
}

class ItemWrapper extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row alert-item">
                <div className="column" style={{position: "relative", maxWidth: "60px", textAlign: "center"}}>
                </div>
                <div className="column" style={{position: "relative"}}>
                    <div className="alert-circle">
                        <i className="fal fa-bell"></i>
                    </div>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

class AlertTriggered extends React.Component {
    render() {
        return (
            <p style={{padding: "8px"}}>Alert raised on {this.props.data.alert_date}</p>
        )
    }
}


class EntryControl extends React.Component {
    static defaultProps = {
        value: ""
    };

    _send = () => {
        window.sonomaClient.tx("com.sonoma.alert.comment", [this.props.data.uuid, this.props.value], (res) => {

        }, (err) => {

        })
    };

    _onKeyPress = (e) => {
        if (e.keyCode == 13) {
            this._send();
        }
    };

    _onPublishError = () => {
        this.props.onChange("");
    };

    render() {
        return (
            <div className="team-entry-control">
                <div className="inner">

                    <div className="column br button"
                         style={{maxWidth: "30px", justifyContent: "center", cursor: "pointer"}}>
                        <i className="fal fa-plus"></i>
                    </div>
                    <div className="column input">
                        <input
                            onKeyDown={this._onKeyPress}
                            onChange={(e) => {
                                this.props.onChange(e.target.value)
                            }}
                            value={this.props.value.content}
                            type="text"/>
                    </div>

                </div>
            </div>
        )
    }
}


class ViewAlertPipeline extends React.Component {
    state = {
        message: ""
    }

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        window.addEventListener("reloadactivity", this._reload);
    }

    componentWillUnmount() {
        window.removeEventListener("reloadactivity", this._reload);
    }

    _reload = () => {

    };

    render() {
        console.log(this.props.data);

        let states = [];
        let currentStage;

        let workflow = DEFAULT_WORKFLOW;
        if (Object.keys(this.props.data.alarm_workflow).length > 0) {
            workflow = this.props.data.alarm_workflow;
        }


        // Items consist of pairs of [Moment, DOMNode]
        // and are sorted before rendering BETWEEN the currentStage and the triggered event
        let items = this.props.data.events.sort((a, b) => {
            if (a.created > b.created) return 1;
            if (a.created < b.created) return -1;
            return 0;
        });

        let nodes = items.map(item => {
            return (
                <ItemWrapper>


                </ItemWrapper>
            );

        });

        return (
            <div className="column">
                <div className="row"
                     style={{position: "relative", overflow: "hidden", background: "rgb(243, 243, 243)"}}>
                    <div className="column block block-overflow" style={{position: "relative", paddingRight: "16px"}}>

                        {currentStage}

                        {items.map(item => {
                            return item[1];
                        })}

                        <ItemWrapper>
                            <AlertTriggered data={this.props.data}/>
                        </ItemWrapper>
                    </div>


                    <div className="alert-line"></div>
                </div>
                <div className="row block block-overflow bt" style={{position: "relative", maxHeight: "45px"}}>
                    <EntryControl
                        data={this.props.data}
                        value={this.state.message}
                        onPublish={this._onPublish}
                        onChange={this._onChange}/>
                </div>
            </div>
        )
    }
}

export default ViewAlertPipeline;
