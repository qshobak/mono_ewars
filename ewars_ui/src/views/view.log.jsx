import React from "react";

import DataGrid from "../controls/datagrid/c.datagrid.jsx";

const COLUMNS = [
    {
        name: "uuid",
        label: "UUID",
        type: "text",
        root: true
    },
    {
        name: "peer_id",
        label: "Peer ID",
        type: "text",
        root: true
    },
    {
        name: "ts_ms",
        label: "Timestamp",
        type: "number",
        root: true
    },
    {
        name: "event_type",
        label: "Event type",
        type: "text"
    },
    {
        name: "ref_id",
        label: "Reference ID",
        type: "text"
    },
    {
        name: "ref_type",
        label: "Reference type",
        type: "text"
    }
];

const GRID_ACTIONS = [];

const ROW_ACTIONS = [];

class ViewLog extends React.Component {
    constructor(props) {
        super(props);
    }

    _query = () => {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.events", [], (res) => {
                console.log(res);
                resolve({
                    records: res,
                    count: 0
                })
            }, (err) => console.log(err));
        })
    };

    render() {
        return (
            <DataGrid
                grid={[COLUMNS]}
                queryFn={this._query}
                baseFiltering={{}}
                baseOrdering={{}}
                title="Events"
                actions={[]}
                onAction={this._action}
                rowActions={[]}/>
        )
    }
}

export default ViewLog;
