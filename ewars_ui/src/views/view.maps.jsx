import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";
let state = window.state;

const ACTIONS = [
    ['fa-plus', 'CREATE', "Create notebook"],
    ['fa-chevron-left', 'HIDE', 'Hide sidebar']
];

class Notebook extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-handle">
                {this.props.data.name}
            </div>
        )
    }
}

class ViewMaps extends React.Component {
    state = {
        notebooks: []
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.notebooks", [], (res) => {
            this.setState({
                notebooks: res
            })
        }, (err) => console.log(err));
    }

    _action = (action) => {

    };

    render() {
        return (
            <div className="row">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow">
                        {this.state.notebooks.map(item => {
                            return <Notebook data={item}/>
                        })}
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title"><i className="fal fa-notebook"></i>&nbsp;Notebooks</div>
                    </div>
                    <div className="row block block-overflow">

                    </div>
                </div>
            </div>
        )
    }
}

export default ViewMaps;
