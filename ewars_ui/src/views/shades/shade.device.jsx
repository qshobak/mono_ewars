import React from 'react';

import Form from "../../controls/forms/form.reporting.jsx";
import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CLOSE', 'Close']
];

const FORM_DEFINITION = {
    device_type: {
        type: "select",
        label: "Device Type",
        options: [
            ['DESKTOP_WIN', "Desktop (Windows)"],
            ['DESKTOP_MAC', 'Desktop (Mac)'],
            ['DESKTOP_NIX', 'Desktop (Nix)'],
            ['ANDROID', 'Android'],
            ['IOS', 'iOS'],
            ['RELAY', 'Relay']
        ]
    }
};

class ShadeDevice extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar">
                    <div className="column grid-title">Device</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row system-form">
                    <Form
                        definition={FORM_DEFINITION}
                        onChange={this._change}
                        errors={{}}
                        data={this.state.data}
                        allRoot={true}/>
                </div>
            </div>
        )
    }
}

export default ShadeDevice;