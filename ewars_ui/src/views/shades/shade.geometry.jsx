import React from "react"

import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'SAVE'],
    ['fa-times', 'CANCEL', 'CANCEL']
];


class GeometryShade extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar">
                    <div className="column grid-title">Geometry Editor</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block">

                </div>
            </div>
        )
    }
}

export default GeometryShade;