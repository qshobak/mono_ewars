import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import ReportingForm from "../../controls/forms/form.reporting.jsx";

const ROLE_FORM = {
    name: {
        name: "name",
        type: "text",
        required: true,
        label: "Name"
    },
    inh: {
        name: "inherits",
        type: "Select",
        label: "Inherits",
        options: [
            ['USER', 'User'],
            ["ACCOUNT_ADMIN", "Account Administrator"],
            ['REGIONAL_ADMIN', 'Regional Administrator']
        ],
        required: true
    },
    status: {
        name: "status",
        type: "select",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive']
        ],
        required: true,
        label: "Status"
    },
    description: {
        name: "description",
        type: "textarea",
        label: "Description"
    },
    t_forms: {
        type: "header",
        label: "Forms Access"
    },
    forms: {
        type: "forms_multiple",
        label: "Forms",
        required: false,
        conditions: [
            "inh:=:USER",
            "inh:=:REGIONAL_ADMIN",
        ]
    }
};

const ACTIONS = [
    ['fa-save', 'SAVE', "Save change(s)"],
    ["fa-times", 'CLOSE", "CLOSE']
];

const MENU_ACTIONS = [
    ['fa-box-check', 'ALL', 'Select all'],
    ['fa-box', 'NONE', 'Select none']
];

const MENUS = [
    {
        id: "DASHBOARD",
        name: "Dashboard",
        icon: "fa-tachometer",
        items: []
    },
    {
        id: "REPORTING",
        name: "Reporting",
        icon: "fa-clipboard",
        items: []
    },
    {
        id: "ALERTS",
        name: "Alerts",
        icon: "fa-bell",
        items: []
    },
    {
        id: "ANALYSIS",
        name: "Analysis",
        icon: "fa-chart-line",
        items: []
    },
    {
        id: "ADMIN",
        name: "Admin",
        icon: "fa-cog",
        items: []
    }
];

class RolePermissions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column panel br block block-overflow">
                    <div className="dash-section"><i className="fal fa-clipboard"></i>&nbsp;Forms</div>

                    <div className="dash-section"><i className="fal fa-bell"></i>&nbsp;Alarms</div>

                    <div className="dash-section"><i className="fal fa-book"></i>&nbsp;Documents</div>

                    <div className="dash-section"><i className="fal fa-users"></i>&nbsp;Teams</div>
                </div>
                <div className="column">
                    <table>
                        <thead>
                        <tr>
                            <th>Context</th>
                            <th>Can View</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        )
    }
}

class MenuEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _toggle = (id) => {

    };

    render() {
        return (
            <div className="row" style={{flexWrap: "wrap"}}>
                <div className="column panel br block block-overflow">
                    {MENUS.map(item => {
                        return (
                            <div className="dash-handle">
                                <div className="row">
                                    <div className="column block grid-title">
                                        <i className={"fal " + item.icon}></i>{item.name}
                                    </div>
                                    <div className="column block"
                                         style={{maxWidth: "20px"}}
                                         onClick={() => this._toggle(item.id)}>
                                        <i className="fal fa-toggle-on"></i>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className="column">

                </div>
            </div>
        )
    }
}


class PermissionTreeNode extends React.Component {
    state = {
        showChilds: false,
        children: []
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="tree-node">

            </div>
        )
    }
}

class PermissionsTree extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow">

            </div>
        )
    }
};

class ShadeRole extends React.Component {
    state = {
        data: null,
        errors: {},
        view: "SETTINGS"
    };

    constructor(props) {
        super(props);

        if (this.props.data.uuid) {
            window.sonomaClient.tx("com.sonoma.role", this.props.data.uuid, (res) => {
                this.setState({data: res});
            }, (err) => {
                window.state.error("ERROR_LOAD_ROLE", err);
            })
        } else {
            this.state.data = {forms: []};
        }
    }

    _view = (view) => {
        this.setState({view})
    };

    _change = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "SAVE":
                this._save();
                break;
            default:
                break;
        }
    };

    _save = () => {
        let cmd = "com.sonoma.role.create",
            message = "Creating new role";
        if (this.state.data.uuid) {
            cmd = "com.sonoma.role.update";
            message = "Updating role";
        }

        let bl = window.state.blocker(message);
        window.sonomaClient.tx(cmd, this.state.data, (res) => {
            bl.rm();
            window.state.growl('fa-save', "Role updated");
            if (!this.state.data.uuid) {
                this.setState({data: res})
            }
        }, (err) => {
            bl.rm();
            window.state.error("An error occurred", err);
        })
    };

    render() {
        let view;

        if (this.state.view == "SETTINGS") {
            view = (
                <div className="column system-form">
                    <ReportingForm
                        allRoot={true}
                        definition={ROLE_FORM}
                        showErrorBar={false}
                        onChange={this._change}
                        errors={this.props.errors}
                        data={this.state.data || {}}/>
                </div>
            )
        }
        if (this.state.view == "MENUS") view = <MenuEditor/>;
        if (this.state.view == "PERMISSIONS") view = <RolePermissions/>;

        return (
            <div className="column dark">
                <div className="row gen-toolbar bb" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">
                        Role Editor
                    </div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>

                <div className="row">
                    <div className="column block" style={{maxWidth: "35px", overflow: "hidden", position: 'relative'}}>
                        <div className="ide-tabs">
                            <div
                                onClick={() => this._view("SETTINGS")}
                                className={"ide-tab" + (this.state.view == "SETTINGS" ? " ide-tab-down" : "")}>
                                <i className="fal fa-cog"></i>&nbsp;Settings
                            </div>
                            <div
                                onClick={() => this._view("PERMISSIONS")}
                                className={"ide-tab" + (this.state.view == "PERMISSIONS" ? " ide-tab-down" : "")}>
                                <i className="fal fa-lock"></i>&nbsp;Permissions
                            </div>
                        </div>

                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ShadeRole;
