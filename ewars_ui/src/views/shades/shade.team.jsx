import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'SAVE'],
    ['fa-times', 'CANCEL', 'CANCEL']
];

const FORM = {
    name: {
        type: "text",
        label: "NAME",
        required: true
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ['ACTIVE', 'ACTIVE'],
            ['INACTIVE', 'INACTIVE']
        ],
        required: true
    },
    description: {
        type: "textarea",
        label: "Description"
    }
};

class ShadeTeam extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {}
        }

    }

    _change = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch(action) {
            case "SAVE":
                this._save();
                break;
            default:
                break;
        }
    };

    _save = () => {
        let cmd = "com.sonoma.team.create";
        if (this.state.data.uuid) cmd = "com.sonoma.team.update";

        let bl = window.state.blocker("SAVING_CHANGES");
        window.sonomaClient.tx(cmd, this.state.data, (res) => {
            bl.rm();
            windwo.state.growl("fa-save", "ITEM_SAVED");
        }, (err) => {
            window.state.error("ERROR", err);
        })
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Team</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row system-form">
                    <Form
                        allRoot={true}
                        definition={FORM}
                        onChange={this._change}
                        errors={{}}
                        errorBar={false}
                        data={this.state.data}/>
                </div>
            </div>
        )
    }

}

export default ShadeTeam;