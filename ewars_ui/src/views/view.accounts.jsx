import React from "react";

import ws from "../libs/api";

let state = window.state;

const RANDO_COLORS = [
    'lightblue',
    'red',
    'orange',
    'purple',
    'cream',
    'chartreuse',
    'indigo',
    'yellow',
    'amber',
    'scarlet'
];

class AccountItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        window.state.swapToAccount(this.props.data);
    };

    render() {
        let className = "status";
        if ([window.state.account.uid && window.state.account.tki] == [this.props.data.uid, this.props.data.tki]) {
            className += " active";
        }

        let style = {
            background: RANDO_COLORS[this.props.index]
        };

        return (
            <div className="account-item" onClick={this._onClick}>
                <div className={className}>

                </div>
                <div className="logo" style={style}>

                </div>
                <div className="details" style={{justifyContent: "center"}}>
                    <div className="name">{this.props.data.account_name} ({this.props.data.role})</div>
                    <div className="domain">{this.props.data.name}</div>
                    <div className="role">{this.props.data.user_email}</div>
                </div>
            </div>
        )
    }
}

class AccountAdd extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="account-item" onClick={() => this.props.onClick()}>
                <div className="status inactive"></div>
                <div className="logo">
                    <div className="add-logo">
                        <i className="fal fa-plus"></i>
                    </div>
                </div>
                <div className="details">
                    <div className="name">Add account</div>
                </div>
            </div>
        )
    }
}

class Adder extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            domain: "ewars.ws"
        }
    }

    _auth = () => {
        if (this.state.email == "") return;
        if (this.state.email.indexOf("@") < 0) return;
        if (this.state.password == "") return;
        if (this.state.domain == "") return;
        if (this.state.domain.indexOf(".") < 0) return;

        let tenancies = [];

        let payload = {
            email: this.state.email,
            password: this.state.password,
            domain: this.state.domain
        };

        window.sonomaClient.authenticate(payload, (res) => {
            let tkis = window.state.accounts.map(item => {
                return item.tki;
            })

            tenancies = res.tenancies.map(item => {
                let seeded_match = window.state.accounts.filter(cur => {
                    return item.tki == cur.tki;
                })[0] || null;

                return {
                    ...item,
                    seeded: tkis.indexOf(item.tki) >= 0
                }
            });

            if (res.result == true) {
                window.sonomaClient.tx("com.sonoma.add_accounts", tenancies, (res) => {
                    if (res.accounts) state.setAccounts(res.accounts);
                }, (err) => {
                    console.log("ERR");
                });
            } else {
                this.setState({
                    error: res.error
                })
            }
        }, (err) => {
            console.log(err);
        });

    };

    render() {
        return (
            <div className="accounts-list adder">
                <h3>Add New Account</h3>

                <p>Sign into additional accounts to make them available in desktop.</p>

                <div className="field">
                    <label htmlFor="">Email/Username *</label>
                    <div className="control">
                        <input
                            onChange={(e) => {
                                this.setState({email: e.target.value});
                            }}
                            type="email"
                            value={this.state.email}/>
                    </div>
                </div>

                <div className="field">
                    <label htmlFor="">Password *</label>
                    <div className="control">
                        <input
                            onChange={(e) => {
                                this.setState({
                                    password: e.target.value
                                })
                            }}
                            type="password"
                            value={this.state.password}/>
                    </div>
                </div>

                <div className="field">
                    <label htmlFor="">Server address *</label>
                    <div className="control">
                        <input
                            type="text"
                            onChange={(e) => {
                                this.setState({
                                    domain: e.target.value
                                })
                            }}
                            value={this.state.domain}/>
                    </div>
                </div>

                <button onClick={this._auth}><i className="fal fa-sign-in"></i>&nbsp;Sign In</button>
                <button className="red" onClick={() => this.props.cancel()}><i className="fal fa-times"></i>&nbsp;Cancel
                </button>
            </div>
        )
    }
}


class ViewAccounts extends React.Component {
    static defaultProps = {
        visible: false
    };

    constructor(props) {
        super(props);

        this.state = {
            showAdder: false,
            showSyncing: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible && !this.state.showAdder) {
            document.body.addEventListener("click", this._hide);
        }
    }

    _hide = (e) => {
        if (this.props.visible && !this.state.showAdder) {
            if (!this._el.contains(e.target)) {
                document.body.removeEventListener("click", this._hide);
                this.props.hideAccounts();
            }
        }
    };

    _showAdder = () => {
        this.setState({
            showAdder: true
        })
    };

    _hideAdder = () => {
        this.setState({
            showAdder: false
        })
    };


    render() {
        if (!this.props.visible) return null;

        if (this.state.showAdder) {
            return <Adder cancel={this._hideAdder}/>
        }
        return (
            <div
                ref={(el) => {
                    this._el = el
                }}
                className="accounts-list">
                <AccountAdd onClick={this._showAdder}/>
                {window.state.accounts.map((item, index) => {
                    return <AccountItem data={item} index={index}/>
                })}
            </div>
        )
    }
}

export default ViewAccounts;
