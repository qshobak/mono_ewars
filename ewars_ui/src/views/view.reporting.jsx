import React from 'react';

import ViewReportingGrid from "./view.reporting.grid.jsx";
import ViewReportingCalendar from "./view.reporting.calendar.jsx";

class ViewReporting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            form: null
        }

        this._getForm(this.props.config.form_id);
    }

    _getForm = (id) => {
        window.sonomaClient.tx("com.sonoma.form", id, (res) => {
            this.setState({
                form: res || null
            })
        }, (err) => {
            console.log(err);
        })
    };

    componentWillReceiveProps(nextProps) {
        this._getForm(nextProps.config.form_id);
    }

    render() {
        return (
            <ViewReportingGrid
                lid={this.props.config.lid || null}
                form={this.state.form}/>
        )
    }
}

export default ViewReporting;
