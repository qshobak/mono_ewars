import React from "react";

const DEBUG_CANON_CALLS = [
    {
        name: "Login Test",
        action: "LOGIN_TEST",
        data: {
            cmd: "com.canon.login",
            args:
                {
                    email: "demo.admin@ewars.ws",
                    password: "ewars123"
                }
        }
    },
    {
        name: "Seed Form Test",
        data: {}
    },
    {
        name: "Alert action test",
        data: {}
    },
    {
        action: "TASK_ACTION",
        name: "Task action test",
        data: {}
    },
    {
        action: "TEST_CALL",
        name: "Basic Test Call",
        data: []
    },
    {
        action: "TEST_COMMAND",
        name: "Basic Command Test",
        data: {
            cmd: "com.sonoma.test",
            args: []
        }
    },
    {
        action: "TEST_REGISTER",
        name: "Test Endpoint Registration",
        data: {
            cmd: "com.sonoma.register",
            args: []
        }
    },
    {
        action: "TEST_COMMAND",
        name: "Test master command",
        data: {}
    },
    {
        action: "TEST_TEAM_MESSAGE",
        name: "Test Team Message",
        cmd: "com.master.team.message",
        data: {
            mid: "12343",
            tid: "1",
            tki: "_development",
            data: {
                content: "Hello World"
            }
        }
    }
];

class DebugTest extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-handle" onClick={() => this.props._onClick(this.props.data)}>
                {this.props.data.name}
            </div>
        )
    }
}

class DebugMainView extends React.Component {
    constructor(props) {
        super(props);
    }

    _testCanonCall = () => {
        window.sonomaClient.sys("com.canon.test", [], (res) => {
            console.log(res);
        }, (err) => console.log(err));
    };

    _action = (data) => {
        console.log(data);
        switch (data.action) {
            case "TEST_CALL":
                window.sonomaClient.sys("com.ewars.test", [], (res) => {
                    console.log(res);
                }, (err) => {
                    console.log(err);
                });
                break;
            case "LOGIN_TEST":
                window.sonomaClient.sys("com.canon.login", data.data.args, (res) => {
                    console.log(res);
                }, (err) => {
                    console.log(err);
                });
                break;
            case "TEST_REGISTER":
                window.sonomaClient.sys("com.canon.register", [], (res) => {
                    console.log(res);
                }, (err) => console.log(err));
                break;
            case "TEST_COMMAND":
                window.sonomaClient.mx("com.master.test", [], (res) => {
                    console.log("RESULT", res);
                }, (err) => {
                    console.log("ERR", err);
                });
                break;
            case "TEST_TEAM_MESSAGE":
                window.sonomaClient.mx(data.cmd, data.data, (res) => {
                    console.log(res);
                }, (err) => console.log(err));
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="row">
                <div className="column br panel block block-overflow">
                    {DEBUG_CANON_CALLS.map(item => {
                        return <DebugTest _onClick={this._action} data={item}/>
                    })}

                </div>
                <div className="column block block-overflow">
                    <button onClick={this._testCanonCall}>Test Canon Call</button>

                </div>

            </div>
        )
    }
}

export default DebugMainView;
