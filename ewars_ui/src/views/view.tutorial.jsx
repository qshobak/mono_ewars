import React from "react";
import OverviewIndicator from "../controls/c.overview.indicator.jsx";
import ActionGroup from "../controls/c.actiongroup.jsx";
import Moment from "moment";


const SCRIPT = [
    {}
]

// script
// Introduction and warning, how to leave tutorial
// About Assignments
//      REquest an assignment
//      Looks like it was approved
// View assignment in menu
// View assignment in overview
// Create a record
//      Click menu
//      CLick "+" on menu item
//      Enter details for report
//      Enter malaria value
//  Amend a record
//      // Browse records
//      Click amend button
//      Amend a value to a higher one
//      Uh oh, look like our change triggered an alert
//  Alerts
//      About alerts
//      Go back to overview
//      Click alert menu
//      About alerts availability
//      Browse alerts
//      Open alert
//      Alert workflow
//      Alert commenting
// Back to overview
// Alerts tab overview
// End -> Point to Knowledgebase

class AlertBrowsing extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class Alert extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class Report extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class ReportBrowsing extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class Overview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showReportingMenu: false,
            showAlertMenu: false,
            showAssignmentRequest: false,
            showMain: true
        }
    }

    _start = () => {
        this.setState({
            showMain: true
        })
    };

    render() {
        return (
            <div className="column" style={{height: "100%"}}>
                <div className="row">
                    <div className="column block main-menu block-overflow"
                         style={{maxWidth: "66px", minWidth: "66px", padding: 0}}>
                        <div className="menu-item">
                            <div className="marker"></div>
                            <div className="row menu-item-icon block"><i className="fal fa-clipboard"></i></div>
                            <div className="row menu-item-label block">Reporting</div>
                        </div>
                        <div className="menu-item">
                            <div className="marker"></div>
                            <div className="row menu-item-icon block"><i className="fal fa-bell"></i></div>
                            <div className="row menu-item-label block">Alerts</div>
                        </div>
                        <div className="menu-item">
                            <div className="marker"></div>
                            <div className="row menu-item-icon block"><i className="fal fa-check-square"></i></div>
                            <div className="row menu-item-label block">Tasks</div>
                        </div>
                    </div>

                    {this.state.showMain ?
                        <div className="column">
                            <div className="row block"
                                 style={{
                                     maxHeight: "23px",
                                     minHeight: "23px",
                                     padding: "4px",
                                     background: "rgba(0,0,0,0.05)"
                                 }}>
                                <div onClick={() => this._view("ASSIGNMENTS")}
                                     className={"clicker active"}>Assignments
                                </div>
                                <div onClick={() => this._view("DUE")}
                                     className={"clicker"}>Due Today
                                </div>
                                <div onClick={() => this._view("OVERDUE")}
                                     className={"clicker"}>Overdue
                                </div>
                                <div onClick={() => this._view("ALERTS")}
                                     className={"clicker"}>Alerts
                                </div>
                            </div>
                            <div className="row block block-overflow"
                                 id="reporting-overview"
                                 style={{background: "rgba(0,0,0,0.05)"}}>
                                <div className="dash-section">Weekly Reporting Form</div>
                                <div className="report-overview">
                                    <div className="column">
                                        <div className="row report-overview-inner">
                                            <div className="column report-overview-info">
                                                <div className="row block">
                                                    <span><strong>Alpha Health Facility</strong> - 2018</span>
                                                </div>
                                                <div className="row">
                                                    <OverviewIndicator
                                                        value={32}
                                                        name="Submitted"
                                                        icon="fal fa-clipboard"/>
                                                    <OverviewIndicator
                                                        value={2}
                                                        icon="fal fa-clock"
                                                        name="Overdue"/>
                                                    <OverviewIndicator
                                                        value={12}
                                                        icon="fal fa-question-circle"
                                                        name="Missing"/>
                                                </div>
                                            </div>
                                            <div className="column report-overview-chart">
                                                <div className={"c100 p90 orange"}>
                                                    <span>90%</span>
                                                    <div className="slice">
                                                        <div className="bar"></div>
                                                        <div className="fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ActionGroup
                                                actions={[]}
                                                vertical={true}
                                                naked={true}
                                                onAction={this._action}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : null}

                </div>
                <div className="row bt gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                    <div className="column grid-title">Mr. Field User (f.user@ewars.ws) 0.0.1-develop</div>
                </div>
            </div>
        )
    }
}

const CONTENT = {
    INITIAL: `
        <h4>Welcome to the tutorial</h4>
        <p>We're going to walk you through some of the basics of using EWARS as a reporting tool.</p>
        <p>Don't worry, we won't be creating any real data, or making any real changes to the system, none of the items you create, edit or delete in this tutorial will be effect your actual data.</p>
        <p>This tutorial covers:</p>
        <ul>
            <li>Requesting an assignment</li>
            <li>Creating records</li>
            <li>Browsing records</li>
            <li>Amending records</li>
            <li>Deleting records</li>
            <li>Alert management and verification</li>
        </ul>
        <p>At the end of the tutorial you can access some more in-depth guidance and documentation on how to use the system or dive in and get started with reporting.</p>
        <p>You can leave the tutorial at any time by clicking "Close" button below, and you can restart the tutorial at any time from the "Help" menu.</p>
    `
}


class Tutorial extends React.Component {
    constructor(props) {
        super(props);

        let screen = <Overview/>;

        this.state = {
            screen: screen,
            stage: "INITIAL"
        }
    }

    _close = () => {
        window.state.tutorial = false;
        window.state.emit("CHANGE");
    };

    render() {
        let content = CONTENT[this.state.stage];

        let buttons = [];
        if (this.state.stage == "INITIAL") {
            buttons.push(
                <button>Start tutorial</button>
            )
            buttons.push(
                <button onClick={this._close}>Close</button>
            )
        }

        return (
            <div className="tutorial">

                <div className="tutorial-view">
                    <div className="view-inner">
                        <div className="inner" dangerouslySetInnerHTML={{__html: content}}></div>
                        <div className="bottom">
                            {buttons}
                        </div>
                    </div>

                </div>

                {this.state.screen}

            </div>
        )
    }
}

export default Tutorial;