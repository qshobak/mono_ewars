import React from "react";

import AppContext from "../context_provider";

import ActionGroup from "../controls/c.actiongroup.jsx";

import Unsupported from "../placeholders/p.unsupported.jsx";

import FormReporting from "./reporting/reporting.form.jsx";
import LocationAlerts from "./reporting/reporting.alerts.jsx";

import Highcharts from "highcharts";

const _utcConvert = (val) => {
    let splat = val.split('-');
    return Date.UTC(splat[0], splat[1], splat[2]);
};

const DATE_SORT = (a, b) => {
    if (a[0] > b[0]) return 1;
    if (a[0] < b[0]) return -1;
    return 0;
}

const OTHERS = [
    // ['fa-bed', 'CAPACITY', 'Capacity'],
    // ['fa-user-md', 'STAFF', 'Staffing'],
    // ['fa-diagnoses', 'PATIENTS', 'Patients'],
    // ['fa-stopwatch', 'PERFORMANCE', 'Performance'],
    // ['fa-users', 'POPULATION', 'Population'],
    // ['fa-users', 'PEERS', 'Peers'],
    // ['fa-map', 'MAPPING', 'Mapping'],
    // ['fa-sticky-note', 'NOTES', 'Notes'],
    // ['fa-stamp', 'MOVEMENT', 'Movement'],
    // ['fa-syringe', 'CAMPAIGNS', 'Campaigns'],
    // ['fa-calendar', 'EVENTS', 'Events'],
    // ['fa-image', 'PHOTOS', 'Photos'],
    // ['fa-inventory', 'INVENTORY', 'Inventory'],
    // ['fa-comments', 'DISCUSSION', 'Discussion'],
    // ['fa-street-view', 'CHECKIN', 'Check-ins']
];

const OTHERS_ADMIN = [
    ['fa-stopwatch', 'REPORTING', 'USERS'],
    ['fa-users', 'USERS', 'USERS'],
];

const VIEWS = {};

class Alarm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="panel-node">
                <div className="column block" style={{padding: "5px"}}>
                    Alarm Name
                </div>
                <div className="column" style={{padding: "5px", maxWidth: "25px"}}>
                    <div className="pill">
                        <div className="row">12</div>
                        <div className="row">0</div>
                    </div>
                </div>
            </div>
        )
    }
}

class FormNode extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="panel-node">
                <div className="column" style={{padding: "5px"}}>
                    <strong>Form Name</strong>
                    <span>[start_date] to [end_date]</span>
                </div>
                <div className="column" style={{padding: "5px", flex: 0}}>
                    <ActionGroup
                        actions={[['fa-plus', 'CREATE', "CREATE_RECORD"]]}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class LocationForm {
    constructor(props) {
        this.props = props;
    }
}

class AlertTrend extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.location.alerts.trend', this.props.data.uuid, (res) => {
            this.setState({
                data: res
            }, () => {
                this.chart = Highcharts.chart(this._el, {
                    chart: {
                        backgroundColor: null,
                        borderWidth: 0,
                        style: {
                            overflow: "visible"
                        },
                        skipClone: true
                    },
                    title: {text: null},
                    subtitle: {text: null},
                    xAxis: {
                        type: "datetime",
                        title: {text: null},
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: [],
                        labels: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        title: {text: null},
                        enabled: false,
                        labels: {enabled: false},
                        tickPositions: [0]
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                        name: "Alerts",
                        data: res.map(item => {
                            return [_utcConvert(item[0]), item[1]]
                        }).sort(DATE_SORT)
                    }]
                })
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ALERTS", err);
        })
    }

    render() {
        return (
            <div className="row block" ref={(el) => this._el = el} style={{maxHeight: "50px", minHeight: "50px", background: "transparent"}}>

            </div>
        )
    }
}

class RecordsTrend extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.location.records.trend', this.props.data.uuid, (res) => {
            this.setState({
                data: res
            }, () => {
                this.chart = Highcharts.chart(this._el, {
                    chart: {
                        backgroundColor: null,
                        borderWidth: 0,
                        style: {
                            overflow: "visible"
                        },
                        skipClone: true
                    },
                    title: {text: null},
                    subtitle: {text: null},
                    xAxis: {
                        type: "datetime",
                        title: {text: null},
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: [],
                        labels: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        title: {text: null},
                        enabled: false,
                        labels: {enabled: false},
                        tickPositions: [0]
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                        name: "Records",
                        data: res.map(item => {
                            return [_utcConvert(item[0]), item[1]]
                        }).sort(DATE_SORT)
                    }]
                })
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ALERTS", err);
        })
    }

    render() {
        return (
            <div className="row block" ref={(el) => this._el = el} style={{maxHeight: "50px", minHeight: "50px", background: "transparent"}}>

            </div>
        )
    }
}

class DefaultView extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.location.forms.available', this.props.data.uuid, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_LOCATION", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid) {
            window.sonomaClient.tx("com.sonoma.location.forms.available", nextProps.data.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("ERROR_LOADING_LOCATION", err);
            })
        }
    }

    _showLocation = (uuid) => {
        window.state.showLocationCard(uuid);
    };

    _showUser = (uuid) => {
        window.state.showUserCard(uuid);
    };

    render() {
        let available_forms = this.state.data.filter(item => {
                return (item.status == 'ACTIVE' && !item.assign_uuid)
            }),
            active_forms = this.state.data.filter(item => {
                return (item.status == 'ACTIVE' && item.assign_uuid);
            }),
            archived_forms = this.state.data.filter(item => {
                return (item.status == 'ARCHIVED' && item.start_date != null);
            });

        if (available_forms.length <= 0) {
            if (['ACCOUNT_ADMIN', 'REGIONAL_ADMIN'].indexOf(this.context.user.role) >= 0) {
                available_forms = <p>There are currently no forms not active for reporting for this location.</p>
            } else {
                available_forms = <p>There are currently no other forms available for reporting for this location.</p>
            }
        } else {
            available_forms = available_forms.map(item => {
                return (
                    <div className="dash-handle">
                        <div className="inner">
                            <div className="icon">
                                <i className="fal fa-clipboard"></i>
                            </div>
                            <div className="label">{item.name.en || item.name}</div>
                        </div>
                    </div>
                )
            })
        }

        if (active_forms.length <= 0) {
            if (['ACCOUNT_ADMIN', "REGIONAL_ADMIN"].indexOf(this.context.user.role) >= 0) {
                active_forms = <p>There are currently no actively reported forms for this location.</p>
            } else {
                active_forms = <p>You currently do not have access to any forms for this location.</p>
            }
        } else {
            active_forms = active_forms.map(item => {
                return (
                    <div className="dash-handle">
                        <div className="inner">
                            <div className="icon">
                                <i className="fal fa-clipboard"></i>
                            </div>
                            <div className="label">{item.name.en || item.name}</div>
                        </div>
                    </div>
                )
            })
        }

        if (archived_forms.length <= 0) {
            archived_forms = <p>There are currently no archived forms for this location.</p>
        } else {
            archived_forms = archived_forms.map(item => {
                return (
                    <div className="dash-handle">
                        <div className="inner">
                            <div className="icon">
                                <i className="fal fa-clipboard"></i>
                            </div>
                            <div className="label">{item.name.en || item.name}</div>
                        </div>
                    </div>
                )
            })
        }

        return (
            <div className="row">
                <div className="column block block-overflow padded-8">
                    <div className="location-title">{this.props.data.name.en || this.props.data.name}</div>

                    <div className="critter-wrapper">
                        <div className="critter">
                            <div className="inner">
                                <div className="label">Status</div>
                                <div className="content">{this.props.data.status}</div>
                            </div>
                        </div>
                        <div className="critter">
                            <div className="inner">
                                <div className="label">
                                    Type
                                </div>
                                <div className="content">
                                    {this.props.data.lti_name.en || this.props.data.lti_name}
                                </div>
                            </div>
                        </div>
                        <div className="critter">
                            <div className="inner">
                                <div className="label">Pcode</div>
                                <div className="content">{this.props.data.pcode}</div>
                            </div>
                        </div>
                        <div className="critter">
                            <div className="inner">
                                <div className="label">UUID</div>
                                <div className="content">
                                    {this.props.data.uuid}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clearer"></div>

                    <div className="dash-header">
                        <i className="fal fa-clipboard"></i>&nbsp;Active Forms
                    </div>
                    {active_forms}

                    <div className="dash-header">
                        <i className="fal fa-clipboard"></i>&nbsp;Available Forms
                    </div>
                    {available_forms}

                    <div className="dash-header">
                        <i className="fal fa-clipboard"></i>&nbsp;Archived Forms
                    </div>
                    {archived_forms}

                </div>
                <div className="panel bl block block-overflow">

                    <div className="dash-header">
                        <i className="fal fa-bell"></i>&nbsp;Alerts Trend
                    </div>
                        <AlertTrend data={this.props.data}/>

                    <div className="dash-header">
                        <i className="fal fa-clipboard"></i>&nbsp;Records Trend
                    </div>
                    <RecordsTrend data={this.props.data}/>

                    <div className="dash-header">
                        <i className="fal fa-map-marked-alt"></i>&nbsp;Location
                    </div>

                    <div className="lineage">
                        {this.props.data.lineage.map(item => {
                            let ancestor = this.props.data.ancestors[item];
                            return (
                                <div className="location" onClick={() => this._showLocation(item)}>
                                    <strong>{ancestor.name.en || ancestor.name}</strong>
                                    {ancestor.lti_name.en || ancestor.lti_name}
                                </div>
                            )
                        })}
                    </div>

                    <div className="dash-header">
                        <i className="fal fa-info-square"></i>&nbsp;Identification
                    </div>

                    <div className="critter-wrapper">
                        <div className="critter">
                            <div className="inner">
                                <div className="label">PCode</div>
                                <div className="content">{this.props.data.pcode}</div>
                            </div>
                        </div>
                    </div>

                    <div className="dash-header">
                        <i className="fal fa-building"></i>&nbsp;Organizations
                    </div>

                    <div className="dash-header">
                        <i className="fal fa-users"></i>&nbsp;Users
                    </div>

                    <div className="lineage">
                        {this.props.data.users.map(item => {
                            return (
                                <div className="location" onClick={() => this._showUser(item.uuid)}>
                                    <strong>{item.name}</strong>
                                    {item.email}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

class ViewLocationReporting extends React.Component {
    static contextType = AppContext;
    _lastDownX = null;

    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            form: null,
            alarm: null,
            data: null
        };
    }

    componentWillUnmount() {
    }

    _view = (view) => {
        this.setState({
            view: view
        });
    };

    _showForm = (form) => {
        this.setState({
            form: form,
            view: "FORM"
        })
    };

    _showAlerts = (data) => {
        this.setState({
            alarm: data,
            view: "ALARM"
        })
    };

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.location.reporting', this.props.config.uuid, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_LOCATION", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.uuid != this.props.config.uuid) {
            this.setState(window.state.getCachedState("LOCATION_" + nextProps.config.uuid, {
                view: "DEFAULT",
                form: null,
                alarm: null,
                data: null
            }));
        }
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        let view;

        if (this.state.view == "FORM") {
            view = <FormReporting
                data={this.state.form}
                assignments={this.state.data.assignments}
                location={this.props.config.uuid}/>;
        } else if (this.state.view == "ALARM") {
            view = <LocationAlerts
                data={this.state.alarm}
                location={this.props.config.uuid}/>;
        } else if (VIEWS[this.state.view]) {

        } else {
            view = <DefaultView data={this.state.data}/>;
        }

        return (
            <div className="column">
                <div className="row">
                    <div className="column br panel block block-overflow"
                         ref={(el) => this._panelEl = el}
                         style={{padding: "8px 0 0 0"}}>

                        <div className={"dash-handle" + (this.state.view == "DEFAULT" ? " active" : "")}
                             onClick={() => this.setState({view: "DEFAULT"})}>
                            <div className="inner">
                                <div className="icon">
                                    <i className="fal fa-map-marker"></i>
                                </div>
                                <div className="label">
                                    {this.state.data.name.en || this.state.data.name}
                                </div>
                            </div>
                        </div>

                        <div className="dash-header">
                            Reporting
                        </div>

                        {this.state.data.forms.map(item => {
                            let iconStyle = {color: "green"};
                            if (!item.reporting_uuid) iconStyle.color = "#CCCCCC";
                            if (!item.assign_uuid) iconStyle.color = "orange";
                            return (
                                <div onClick={() => this._showForm(item)}
                                     className="dash-handle">
                                    <div className="inner">
                                        <div className="icon">
                                            <i className="fal fa-circle" style={iconStyle}></i>
                                        </div>
                                        <div className="label">
                                            {item.name.en || item.name}
                                        </div>
                                    </div>
                                </div>
                            )
                        })}

                        <div className="dash-header">
                            Alerts
                        </div>

                        {this.state.data.alarms.map(item => {
                            return (
                                <div className="dash-handle" onClick={() => this._showAlerts(item)}>
                                    <div className="inner">
                                        <div className="icon">
                                            <i className="fal fa-bell"></i>
                                        </div>
                                        <div className="label">{item.name}</div>
                                        <div className="icon">{item.alert_count}</div>
                                    </div>
                                </div>
                            )
                        })}

                        {false ?
                            <div className="dash-header">
                                Management
                            </div>
                            : null}
                        {OTHERS.map(item => {
                            return (
                                <div
                                    onClick={() => this._view(item[1])}
                                    className="dash-handle">
                                    <div className="inner">
                                        <div className="icon">
                                            <i className={"fal " + item[0]}></i>
                                        </div>
                                        <div className="label">{item[2]}</div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="column" style={{position: "relative"}}>
                        {view}
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewLocationReporting;
