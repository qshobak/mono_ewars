import React from "react";

import Modal from "../controls/c.modal.jsx";
import {TaskItem, LegacyTaskItem} from '../controls/c.task.jsx';

import ws from "../libs/api";

const LOCATION_REQUEST = "LOCATION_REQUEST";
const ASSIGNMENT_REQUEST = "ASSIGNMENT_REQUEST";
const REGISTRATION_REQUEST = "REGISTRATION_REQUEST";
const ACCESS_REQUEST = "ACCESS_REQUEST";
const AMENDMENT_REQUEST = "AMENDMENT_REQUEST";
const RETRACTION_REQUEST = "RETRACTION_REQUEST";
const ORGANIZATION_REQUEST = "ORGANIZATION_REQUEST";

class ViewTasks extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            filter: null,
            modal: null,
            counts: {}
        };

        if (this.props.config.task_type) this.state.filter = this.props.config.task_type;

        window.sonomaClient.tx("com.sonoma.self.tasks", [], (res) => {
            this.setState({
                items: res,
                counts: {
                    ASSIGNMENT_REQUEST: res.filter(item => {
                        return item.data.task_type == "ASSIGNMENT_REQUEST";
                    }).length,
                    RETRACTION_REQUEST: res.filter(item => {
                        return item.data.task_type == 'RETRACTION_REQUEST';
                    }).length,
                    AMENDMENT_REQUEST: res.filter(item => {
                        return item.data.task_type == "AMENDMENT_REQUEST";
                    }).length,
                    REGISTRATION_REQUEST: res.filter(item => {
                        return item.data.task_type == "REGISTRATION_REQUEST";
                    }).length,
                    ACCESS_REQUEST: res.filter(item => {
                        return item.data.task_type == "ACCESS_REQUEST";
                    }).length,
                    ORGANIZATION_REQUEST: res.filter(item => {
                        return item.data.task_type == "ORG_REQUEST"
                    }).length
                }
            })
        }, (err) => {
            console.log(err);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.task_type != this.state.filter) {
            this.setState({
                filter: nextProps.config.task_type
            })
        }
    }

    render() {

        let items = this.state.items.sort((a, b) => {
            if (a.created > b.created) return 1;
            if (a.created > b.created) return -1;
            return 0;
        });

        if (this.state.filter) {
            items = items.filter(item => {
                return item.data.task_type == this.state.filter;
            })
        }

        let placeholder;
        if (items.length <= 0) {
            placeholder = (
                <div className="column placeholder bgd-ps-neutral" style={{position: "relative"}}>
                    <div className="placeholder-icon">
                        <i className="fal fa-tasks"></i>
                    </div>
                    <div className="placeholder-title">
                        No tasks
                    </div>
                    <div className="placeholder-text">
                        <p>Looks like you don't have any tasks awaiting completion at this time.</p>
                    </div>
                </div>
            )
        }

        return (
            <div className="row">
                <div className="column panel block block-overflow br">
                    <div
                        onClick={() => this.setState({filter: null})}
                        className={"dash-handle" + (!this.state.filter ? " active" : "")}>
                        <div className="inner">
                            <i className="fal fa-tasks"></i>&nbsp;All Tasks ({this.state.items.length})
                        </div>
                    </div>

                    <div className="dash-handle">
                        <div className="inner">
                            <i className="fal fa-check-box"></i>&nbsp;Completed
                        </div>
                    </div>

                    {window.state.account.role != "USER" ?
                        <div className="dash-section-complex">
                            <div className="dash-section-inner">
                                <div className="icon">
                                    <i className="fal fa-cogs"></i>
                                </div>
                                <div className="label">Admin Tasks</div>
                            </div>
                            <div className="dash-children">
                                <div
                                    onClick={() => this.setState({filter: REGISTRATION_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Registration requests ({this.state.counts.REGISTRATION_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: ASSIGNMENT_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Assignment requests ({this.state.counts.ASSIGNMENT_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: ACCESS_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Access requests ({this.state.counts.ACCESS_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: LOCATION_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Location requests ({this.state.counts.LOCATION_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: RETRACTION_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Retraction requests ({this.state.counts.RETRACTION_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: AMENDMENT_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Amendment requests ({this.state.counts.AMENDMENT_REQUEST || 0})
                                    </div>
                                </div>
                                <div
                                    onClick={() => this.setState({filter: ORGANIZATION_REQUEST})}
                                    className="dash-handle">
                                    <div className="inner">
                                        Organization requests ({this.state.counts.ORG_REQUEST || 0})
                                    </div>
                                </div>
                            </div>
                        </div>
                        : null}


                    <div className="dash-section-complex">
                        <div className="dash-section-inner">
                            <div className="icon">
                                <i className="fal fa-clipboard"></i>
                            </div>
                            <div className="label">Reporting Tasks</div>
                        </div>
                        <div className="dash-children">

                        </div>
                    </div>

                    <div className="dash-section-complex">
                        <div className="dash-section-inner">
                            <div className="icon">
                                <i className="fal fa-bell"></i>
                            </div>
                            <div className="label">Alert Tasks</div>
                        </div>
                        <div className="dash-children">

                        </div>
                    </div>

                </div>
                <div className="column block block-overflow">

                    {placeholder}
                    {items.map(item => {
                        return <LegacyTaskItem data={item}/>
                    })}
                </div>
            </div>
        )
    }
}

export default ViewTasks;
