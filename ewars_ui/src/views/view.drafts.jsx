import React from 'react';

import DataGrid from "../controls/datagrid/c.datagrid.jsx";

const COLUMNS = [
    {
        name: "form_name",
        label: "Form",
        type: "form",
        root: true
    },
    {
        name: "location_name",
        label: "Location",
        type: "location",
        root: true
    },
    {
        name: "data_date",
        label: "Record Date",
        type: "date",
        root: true
    },
    {
        name: "created",
        type: "date",
        label: "Created",
        root: true
    },
    {
        name: "modified",
        type: "date",
        label: "Modified",
        root: true
    }
];

const GRID_ACTIONS = [];

const ROW_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit draft'],
    ['fa-trash', 'DELETE', 'Delete draft']
];

class ViewDrafts extends React.Component {
    constructor(props) {
        super(props)
    }

    _query = () => {
        return new Promise((resolve, reject) => {
            resolve({
                records: [],
                count: 0
            })
        })
    };

    _action = (action, data) => {

    };

    render() {
        return (
            <DataGrid
                grid={[COLUMNS]}
                queryFn={this._query}
                actions={[]}
                rowActions={ROW_ACTIONS}
                onAction={this._action}
                baseFilter={{}}
                baseOrdering={{}}
                title="Drafts"/>
        )
    }

}

export default ViewDrafts;
