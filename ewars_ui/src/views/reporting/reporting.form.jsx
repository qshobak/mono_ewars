import React from "react";
import Moment from "moment";

import DateUtils from "../../utils/date_utils";
import ActionGroup from "../../controls/c.actiongroup.jsx";

class DateItem extends React.Component {
    static defaultProps = {
        interval: "DAY",
        point: null,
        exists: [],
        lid: null,
        fid: null,
        record_map: {}
    };

    _click = () => {
        console.log(this.props.record_map, this.props.point.format("YYYY-MM-DD"))
        let record_id = this.props.record_map[this.props.point.format("YYYY-MM-DD")] || null;

        if (record_id) {
            window.state.addTab("RECORD", {
                name: "Record",
                record_id: record_id,
                form_id: this.props.fid
            })
        } else {
            window.state.addTab("RECORD", {
                name: "Record",
                form_id: this.props.fid,
                lid: this.props.lid || null,
                dd: this.props.point.format("YYYY-MM-DD")
            })
        }
    };


    render() {
        let label = "n/a";
        switch (this.props.interval) {
            case "DAY":
                break;
            case "WEEK":
                label = Moment.utc(this.props.point).local().format("[W]WW");
                break;
            default:
                break;
        }


        let className = "grid-cell ignore";

        if (this.props.point.isBefore(this.props.end_date, 'd') && this.props.point.isAfter(this.props.start_date, 'd')) {
            // the date is within the reporting range
            if ((this.props.exists || []).indexOf(this.props.point.format("YYYY-MM-DD")) >= 0) {
                className = "grid-cell good";
            } else {
                // id overdue
                className = "grid-cell bad";
            }
        } else {
            className = "grid-cell ignore";
        }


        return (
            <div className={className} onClick={this._click}>
                {label}
            </div>

        )
    }

}

class Node extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let formatter = "YYYY-MM-DD";
        switch (this.props.interval) {
            case "WEEK":
                formatter = "[W]WW";
                break;
            case "MONTH":
                formatter = "MM";
                break;
            case "YEAR":
                formatter = "YYYY";
                break;
            default:
                break;
        }

        return (
            <div className="report-overview">
                <div className="row report-overview-inner">
                    <div className="column report-overview-info">
                        <div className="row">
                            <div className="column block" style={{flex: 1, padding: "2px"}}>
                                <span style={{fontSize: "1.25rem"}}><strong>{this.props.year}</strong></span>
                            </div>
                        </div>
                        <div className="row ro-details block">
                            <div className="grid">
                                {this.props.dates.reverse().map(item => {
                                    return (
                                        <div className="grid-cell">{item.format(formatter)}</div>
                                    )
                                })}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const ACTIONS = [
    ['fa-calendar', 'CALENDAR', 'CALENDAR_VIEW'],
    ['fa-table', 'TABLE_VIEW', 'TABLE_VIEW']
];

class FormReporting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            view: "DEFAULT"
        }
    }

    componentDidMount() {
        if (this.props.data.reporting_uuid) {
            window.sonomaClient.tx('com.sonoma.location.submissions', {
                lid: this.props.location,
                fid: this.props.data.uuid
            }, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("ERROR_LOADING_REPORTING", err);
            })
        }
    }

    _showAssignmentRequest = () => {
        window.state.showAssignmentRequest(this.props.data.uuid, this.props.location);
    };

    _showReportingRequest = () => {
        window.state.showReportingRequest(this.props.data.uuid, this.props.location);
    };

    render() {
        if (window.state.getRole() == "USER") {
            let assign = (this.props.assignments || []).filter(item => {
                if (item.fid == this.props.data.uuid && item.lid == this.props.location) return true;
                return false;
            })[0] || null;

            if (!assign) {
                return (
                    <div className="column placeholder" style={{position: "relative"}}>
                        <div className="placeholder-icon">
                            <i className="fal fa-clipboard"></i>
                        </div>
                        <div className="placeholder-title">
                            No assignment
                        </div>
                        <div className="placeholder-text">
                            <p>You currently do not have permissions for this form type and location.</p>
                            <button onClick={this._showAssignmentRequest}>Request Assignment</button>
                        </div>
                    </div>
                )
            }
        }

        if (!this.props.data.reporting_uuid) {
            return (
                <div className="column placeholder" style={{position: "relative"}}>
                    <div className="placeholder-icon">
                        <i className="fal fa-clipboard"></i>
                    </div>
                    <div className="placeholder-title">
                        Form not actively reported
                    </div>
                    <div className="placeholder-text">
                        <p>This form is currently not actively reported for this location.</p>
                        <button onClick={this._showReportingRequest}>Start Reporting</button>
                    </div>
                </div>
            )
        }


        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                    <div className="drag"
                         ref={(el) => this._dragEl = el}></div>
                </div>
            )
        }

        let interval = this.props.data.definition.__dd__.interval || "DAY";

        let groups = {};
        let years = [];
        let record_dates = this.state.data.record_dates.map(item => Moment.utc(item));
        record_dates.forEach(item => {
            if (years.indexOf(item.year()) < 0) years.push(item.year());
            if (groups[item.year()]) {
                groups[item.year()].push(item);
            } else {
                groups[item.year()] = [item];
            }
        });

        let nodes = [];

        years.reverse().forEach(year => {
            nodes.push(
                <Node interval={interval}
                      dates={groups[year]}
                      startDate={this.state.data.start_date}
                      endDate={this.state.data.end_date}
                      exists={this.state.data.records}
                      year={year} month={null}/>
            )
        });

        return (
            <div className="column">
                <div className="row gen-toolbar" style={{maxHeight: "25px", minHeight: "35px"}}>
                    <div className="column grid-title">
                        <strong>{this.props.data.name.en || this.props.data.name}</strong>
                        <span>{this.props.data.start_date} to {this.props.data.end_date}</span>
                    </div>
                    <ActionGroup
                        dark={true}
                        active={this.state.view}
                        onClick={this._action}
                        actions={ACTIONS}/>
                </div>
                <div className="row block block-overflow">
                    {nodes}
                </div>
            </div>
        )
    }
}

export default FormReporting;