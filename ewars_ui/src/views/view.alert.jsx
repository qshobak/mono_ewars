import React from "react";

import ViewAlertDetails from "./alert/view.alert.details.jsx";
import ViewAlertPipeline from "./alert/view.alert.pipeline.jsx";
import ViewAlertGuidance from "./alert/view.alert.guidance.jsx";
import ViewAlertAnalysis from "./alert/view.alert.analysis.jsx";
import ViewAlertStakeholders from "./alert/view.alert.stakeholders.jsx";
import AlertWorkflow from "./alert/view.alert.workflow.jsx";


class AlertTabs extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row block bb"
                 style={{
                     maxHeight: "30px",
                     minHeight: "30px",
                     padding: "4px 4px 0 4px"
                 }}>
                <div onClick={() => this.props.onChange("DEFAULT")}
                     className={"clicker" + (this.props.view == "DEFAULT" ? " active" : "")}>
                    <i className="fal fa-bell"></i>&nbsp;Alert
                </div>
                {false ?
                <div onClick={() => this.props.onChange("ANALYSIS")}
                     className={"clicker" + (this.props.view == "ANALYSIS" ? " active" : "")}>
                    <i className="fal fa-chart-line"></i>&nbsp;Analysis
                </div>
                        : null}
                {false ?
                <div onClick={() => this.props.onChange("USERS")}
                     className={"clicker" + (this.props.view == "USERS" ? " active" : "")}>
                    <i className="fal fa-users"></i>&nbsp;Users
                </div>
                : null}
                <div onClick={() => this.props.onChange("RECORDS")}
                     className={"clicker" + (this.props.view == "RECORDS" ? " active" : "")}>
                    <i className="fal fa-clipboard"></i>&nbsp;Records
                </div>
                {false ?
                <div onClick={() => this.props.onChange("GUIDANCE")}
                     className={"clicker" + (this.props.view == "GUIDANCE" ? " active" : "")}>
                    <i className="fal fa-info-circle"></i>&nbsp;Guidance
                </div>
                : null}
            </div>
        )
    }
}

class ViewAlert extends React.Component {
    state = {
        view: "DEFAULT",
        alert: null
    };

    constructor(props) {
        super(props);

        this._mounted = false;

        this.state = {
            alert: null,
            view: "DEFAULT"
        }

    }

    componentDidMount() {
        window.tx("com.sonoma.alert", this.props.config.alert_id, (res) => {
            this.setState({
                alert: res
            })
        }, (err) => {
            console.log(err);
            window.state.error("ERROR", err);
        })
    }

    componentWillMount() {
        window.addEventListener("reloadalert", this._reload);
    }

    componentWillUnmount() {
        window.removeEventListener("reloadalert", this._reload);
    }

    _reload = () => {
        window.sonomaClient.tx('com.sonoma.alert', this.props.config.alert_id, (res) => {
            this.setState({
                alert: res
            });
        }, (err) => {
                window.state.error("ERROR", err);
        })
    };


    _view = (view) => {
        this.setState({
            view
        })
    };

    render() {
        if (!this.state.alert) {
            return (
                <div className="column">
                    <p>Loading...</p>
                </div>
            )
        }

        let view;
        if (this.state.view == "DEFAULT") view = <AlertWorkflow alert={this.state.alert}/>;
        if (this.state.view == "GUIDANCE") view = <ViewAlertGuidance data={this.state.alert}/>;
        if (this.state.view == "ANALYSIS") view = <ViewAlertAnalysis data={this.state.alert}/>;
        if (this.state.view == "USERS") view = <ViewAlertStakeholders data={this.state.alert}/>;

        return (
            <div className="row">
                <div className="column br">
                    <AlertTabs view={this.state.view} onChange={this._view}/>
                    {view}
                </div>
                <ViewAlertDetails data={this.state.alert}/>
            </div>
        )
    }
}

export default ViewAlert;
