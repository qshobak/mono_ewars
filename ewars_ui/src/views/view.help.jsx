import React from "react";

import Showdown from "showdown";


class ViewHelp extends React.Component {
    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.help", this.props.config.id, (res) => {
            this.setState({
                value: res
            })
        }, (err) => {
            window.state.error("An error occurred", err);
        });

        this.converter = new Showdown.Converter({
            tables: true,
            simplifiedAutoLink: true
        })
    }

    render() {
        let content = "";
        if (this.state.value) {
            content = this.converter.makeHtml(this.state.value);
        }
        return (
            <div className="column block block-overflow" dangerouslySetInnerHTML={{__html: content}}>

            </div>
        )
    }
}

export default ViewHelp;