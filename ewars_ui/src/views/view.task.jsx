import React from 'react';

import ActionGroup from "../controls/c.actiongroup.jsx";
import Form from "../controls/forms/form.reporting.jsx";

const ACTIONS = [
    ['fa-check', 'APPROVE', 'APPROVE_TASK'],
    ["fa-ban", "REJECT", "REJECT_TASK"]
];

const TASK_LABELS = {
    LOCATION_REQUEST: "Location request",
    ASSIGNMENT_REQUEST: "Assignment request",
    REGISTRATION_REQUEST: "Registration request",
    ACCESS_REQUEST: "Access request",
    AMENDMENT_REQUEST: "Amendment request",
    RETRACTION_REQUEST: "Deletion request",
    ORGANIZATION_REQUEST: "Organization request",
    CONFLICT: "Merge conflict"
};

const REGISTRATION_FORM = {
    user_name: {
        type: "display",
        label: "Name",
        order: 0
    },
    email: {
        type: "display",
        label: "Email",
        order: 1
    },
    org_id: {
        type: "organization",
        label: "Organization",
        required: true,
        order: 2
    },
    role: {
        type: "role",
        label: "Role",
        required: true,
        order: 3
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ['ACTIVE', "Active"],
            ["INACTIVE", "Inactive"]
        ],
        order: 4
    }
};

const ACCESS_FORM = {
    name: {
        type: "display",
        label: "Name",
        order: 0
    },
    email: {
        type: "display",
        label: "Email",
        order: 1
    },
    org_id: {
        type: "organization",
        label: "Organization",
        order: 2,
        required: true
    },
    role: {
        type: "role",
        label: "Role",
        required: true,
        order: 3
    }
};

const ORG_FORM = {
    name: {
        type: "text",
        label: "Organization name",
        required: true,
        order: 0
    },
    acronym: {
        type: "text",
        label: "Acronym",
        required: false,
        order: 1
    }
};

const LOCATION_REQ = {
    name: {
        type: "text",
        i18n: true,
        required: true,
        order: 0,
        label: "Location name"
    },
    status: {
        type: "select",
        label: "Status",
        required: true,
        options: [
            ['ACTIVE', 'ACTIVE'],
            ['INACTIVE', 'INACTIVE']
        ],
        order: 1
    },
    pcode: {
        type: "text",
        label: "Pcode",
        required: true,
        order: 2
    },
    location_type: {
        type: "location_type",
        label: "Location type",
        required: true,
        order: 3
    }
};

class AmendmentTask extends React.Component {

    constructor(props) {
        super(props);
    }

    _viewRecord = () => {
        window.state.addTab("RECORD", {
            uuid: this.props.data.data.report_uuid,
            name: this.props.data.data.form_name
        })
    };

    render() {
        return (
            <div className="row">
                <div className="column panel br block block-overflow">
                    <div className="dash-handle">
                        <div className="row">
                            <div className="column" style={{maxWidth: "20px"}}>
                                <i className="fal fa-clipboard"></i>
                            </div>
                            <div
                                className="column">{this.props.data.data.form_name.en || this.props.data.data.form_name}</div>
                        </div>
                    </div>

                    <div className="dash-handle">
                        <div className="row">
                            <div className="column" style={{maxWidth: "20px"}}><i className="fal view.fa-user"></i></div>
                            <div className="column">
                                {this.props.data.data.amender_name} ({this.props.data.data.amender_email})
                            </div>
                        </div>
                    </div>

                    <div className="dash-section">
                        Reason
                    </div>
                    <div style={{padding: "0px 16px"}}>
                        <p>{this.props.data.data.reason || "No reason provided"}</p>
                    </div>

                    <button onClick={this._viewRecord}>View Record</button>
                </div>
                <div className="column block block-overflow" style={{padding: "0"}}>
                    <table width="100%" className="inline-grid">
                        <thead>
                        <tr>
                            <th width="30%" style={{maxWidth: "30%"}}>Field</th>
                            <th>Original</th>
                            <th>Amended</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.data.data.amend_list.map(item => {
                            return (
                                <tr>
                                    <th>{item.long_name}</th>
                                    <td>{item.original || "NULL"}</td>
                                    <td>{item.amended}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
            </div>

        )
    }
}

class FormBasedRequest extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let form_def;

        if (this.props.data.task_type == "ASSIGNMENT_REQUEST") form_def = ASSIGN_FORM;
        if (this.props.data.task_type == "REGISTRATION_REQUEST") form_def = REGISTRATION_FORM;
        if (this.props.data.task_type == "ACCESS_REQUEST") form_def = ACCESS_FORM;
        if (this.props.data.task_type == "LOCATION_REQUEST") form_def = LOCATION_REQ;
        if (this.props.data.task_type == "ORGANIZATION_REQUEST") form_def = ORG_FORM;

        return (
            <div className="row">
                <Form
                    definition={form_def || {}}
                    data={this.props.data.data}
                    errors={{}}
                    allRoot={true}
                    onChange={this._onChange}/>
            </div>
        )
    }
}

class AssignmentRequest extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row system-form">

            </div>
        )
    }
}

const FORM_TYPES = [
    "ORGANIZATION_REQUEST",
    "REGISTRATION_REQUEST",
    "ACCESS_REQUEST"
];


class ViewTask extends React.Component {
    state = {
        data: null
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx('com.sonoma.task', this.props.config.uuid, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR", err);
        })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="column">
                    <p>Loading...</p>
                </div>
            )
        }

        console.log(this.state.data);

        let label = "Task";
        if (TASK_LABELS[this.state.data.task_type]) label += ": " + TASK_LABELS[this.state.data.task_type];


        let view;

        if (this.state.data.task_type == "AMENDMENT_REQUEST") view = <AmendmentTask data={this.state.data}/>;
        if (this.state.data.task_type == "REGISTRATION_REQUEST") view = <FormBasedRequest data={this.state.data}/>;
        if (this.state.data.task_type == "LOCATION_REQUEST") view = <FormBasedRequest data={this.state.data}/>;
        if (this.state.data.task_type == "ASSIGNMENT_REQUEST") view = <FormBasedRequest data={this.state.data}/>;

        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{minHeight: "34px", maxHeight: "34px"}}>
                    <div className="column grid-title">{label}</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                {view}
            </div>
        )
    }
}

export default ViewTask;