import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const ACTIONS = [
    ["fa-file-pdf", "PDF", "PDF_DOWNLOAD"],
    ["fa-print", "PRINT", "PRINT"]
];

class ViewGuidance extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar">
                    <div className="column grid-title">[Guidance title]</div>
                    <ActionGroup actions={ACTIONS} onAction={this._action}/>
                </div>

            </div>
        )
    }
}

export default ViewGuidance;