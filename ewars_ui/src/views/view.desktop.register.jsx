import React from 'react';

const _isEmpty = (val) => {
    if (val == "") return true;
    if (val == null) return true;
    if (val == undefined) return true;
};

const _isMatch = (val1, val2) => {
    return val1 === val2;
};

class DesktopRegisterView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            organization: "",
            email: "",
            email_c: "",
            password: "",
            password_c: "",
            domain: "",
        }
    }

    _onChange = (e) => {
        this.setState({
            data: {
                ...this.state.data,
                [e.target.name]: e.target.value
            },
            errors: []
        })
    };

    _attempt = () => {
        let errors = [];

        if (_isEmpty(this.state.name)) {
            errors.push("Name is a required field");
        }

        if (_isEmpty(this.state.email)) {
            errors.push("Email is a required field");
        }

        if (!_isMatch(this.state.email, this.state.email_c)) {
            errors.push("Emails do not match");
        }

        if (_isEmpty(this.state.password)) {
            errors.push("Password is a required field");
        }

        if (!_isMatch(this.state.password, this.state.password_c)) {
            errors.push("Passwords do not match");
        }

        if (_isEmpty(this.state.organization)) {
            errors.push("Organization is a required field");
        }

        if (_isEmpty(this.state.domain)) {
            errors.push("Account is a required field");
        }

        if (errors.length > 0) {
            this.setState({
                errors: errors,
            })
            return;
        }

        window.sonomaClient.register(this.state.data, (res) => {
            console.log(res);
        }, (err) => {
            console.log(err);
        })

    };

    render() {
        return (
            <div className="row">
                <div className="column block block-overflow panel br">
                    <h4>New User Registration</h4>

                    <p>Please provide the details right in order to request a user account in EWARS.</p>

                    <p>Registrations are subject to approval by administrators in the account that you are registering for. Until an administrator approves your account you will not be able to login with the credentials you have provided.</p>

                    <p>Once an administrator approves your access, you will receive and email at the address you provided to let you know that you are now able to log in.</p>
                </div>
                <div className="column">
                    <div className="row block block-overflow">
                        <h4>New User Registration</h4>

                        <p>Please follow the steps below to register for an account.</p>

                        <div className="form">
                            <div className="field">
                                <label htmlFor="">You Name*</label>
                                <input type="text"/>
                            </div>

                            <div className="field">
                                <div className="label">Your Email*</div>
                                <input type="email"/>
                            </div>

                            <div className="field">
                                <div className="label">Confirm email*</div>
                                <input type="email"/>
                            </div>

                            <div className="field">
                                <div className="label">Password*</div>
                                <input type="password"/>
                            </div>

                            <div className="field">
                                <div className="label">Confirm Password*</div>
                                <input type="password"/>
                            </div>

                            <div className="field">
                                <div className="label">Organization</div>
                                <input type="text"/>
                            </div>

                            <div className="field">
                                <div className="label">Account (Domain Name)*</div>
                                <input type="text"/>
                            </div>
                        </div>
                    </div>
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>

                    </div>
                </div>
            </div>
        )

    }
}

export default DesktopRegisterView;
