const React = require('react');

const $ = React.createElement;

const VIEWS = {
    CONFIG: null
}

class ViewMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            menu: "DEFAULT",
            tabs: [],
            activeTab: null,
            viewConfig: {}
        }
    }

    _onViewChange(view) {
        this.setState({view: view});
    }

    _reset() {
        this.setState({
            view: "DEFAULT",
            menu: "DEFAULT",
            tabs: [],
            activeTab: null,
            viewConfig: {}
        });
    }

    render() {
        let view;

        return $("div",
            {className: "column"}
        );
    }
}

module.exports = ViewMain;
