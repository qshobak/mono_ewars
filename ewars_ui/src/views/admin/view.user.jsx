import React from "react";


class ViewAssignments extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}


class ViewDevices extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class ViewUser extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let view;

        if (this.state.view == "ASSIGNMENTS") view = <ViewAssignments data={this.state.data}/>;
        if (this.state.view == 'DEVICES') view = <ViewDevices data={this.state.data}/>;

        if (this.state.view == "DEFAULT") {
            view = (
                <div></div>
            )
        }


        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title"></div>
                    <ActionGroup
                        actions={USER_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row">
                    <div className="column br block"
                         style={{maxWidth: "35px", overflow: "hidden", position: "relative"}}>
                        <div className="ide-tabs">
                            <div
                                onClick={() => this._view("DEFAULT")}
                                className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                                <i className="fal fa-cog"></i>
                            </div>
                            <div
                                onClick={() => this._view("ASSIGNMENTS")}
                                className={"ide-tab" + (this.state.view == "ASSIGNMETNS" ? " ide-tab-down" : "")}>
                                <i className="fal fa-clipboard"></i>&nbsp;Assignments
                            </div>
                            <div
                                onClick={() => this._view("DEVICES")}
                                className={"ide-tab" + (this.state.view == "DEVICES" ? " ide-tab-down" : "")}>
                                <i className="fal fa-mobile"></i>&nbsp;Devices
                            </div>
                        </div>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewUser;