import React from "react";

import utils from "../../utils/utils";
import Form from "../../controls/forms/form.reporting.jsx";
import ActionGroup from "../../controls/c.actiongroup.jsx";

const ITEM_ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)']
];

const GROUP_DEFINITION = {
    name: {
        type: "text",
        label: "Name",
        required: true
    },
    status: {
        type: "select",
        label: "Status",
        required: true,
        options: [
            ['ACTIVE', "ACTIVE"]
        ]
    },
    permissions: {
        type: "permissions",
        label: "Permissions",
        required: false,
        definition: {},
        vertical: true
    }
};

const IND_DEFINITION = {
    name: {
        type: "text",
        label: "Name",
        required: true
    },
    status: {
        type: "select",
        label: "Status",
        required: true,
        options: [
            ['ACTIVE', "ACTIVE"]
        ]
    },
    definition: {
        type: "definition",
        label: "Definition",
        required: true,
        vertical: true
    },
    permissions: {
        type: "permissions",
        label: "Permissions",
        required: false,
        definition: {},
        vertical: true
    }
};


class EditorView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: utils.copy(this.props.data || {}),
            view: "DEFAULT"
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = utils.copy(nextProps.data || {});
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        });
    };

    _action = (action) => {
        switch (action) {
            case "SAVE":
                if (this.props.data.ntype == "G") this._saveGroup();
                if (this.props.data.ntype == "I") this._saveIndicator();
                break;
            default:
                break;
        }
    };

    _saveGroup = () => {
        let bl = window.state.blocker("Saving changes");
        let cmd = "com.sonoma.indicator_group.update";
        if (!this.state.data.uuid) cmd = "com.sonoma.indicator_group.create";

        window.sonomaClient.tx(cmd, this.state.data, (res) => {
            blocker.rm();
            this.setState({
                data: {
                    ...this.state.data,
                    ...res
                }
            });
        }, (err) => {
            blocker.rm();
            window.state.error("An error occurred", err);
        })
    };

    _saveIndicator = () => {
        let bl = window.state.blocker("Saving changes");
        let cmd = "com.sonoma.indicator.update";
        if (!this.state.data.uuid) cmd = "com.sonoma.indicator.create";


        window.sonomaClient.tx(cmd, this.state.data, (res) => {
            bl.rm();
            this.setState({
                data: {
                    ...this.state.data,
                    ...res
                }
            })
        }, (err) => {
            window.state.error("An error occurred", err);
        })
    };

    _view = (view) => {
        this.setState({view: view})
    };

    render() {
        console.log(this.props)
        let definition = IND_DEFINITION;
        if (this.props.data.ntype == "G") definition = GROUP_DEFINITION;

        return (
            <div className="row">
                <div className="column br block" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div
                            onClick={() => this._view("DATA")}
                            className={"ide-tab" + (this.state.view == "DATA" ? " ide-tab-down" : "")}>
                            <i className="fal fa-database"></i>&nbsp;Data
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">{this.props.data.name}</div>
                        <ActionGroup
                            actions={ITEM_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row system-form">
                        <Form
                            allRoot={true}
                            errors={{}}
                            definition={GROUP_DEFINITION}
                            data={this.state.data}
                            onChange={this._onChange}/>
                    </div>
                </div>
            </div>
        )
    }
}

class IndicatorNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    onSelect = () => {

    };

    _toggle = () => {
        if (!this.state.data) {
            if (this._el) this._el.setAttribute("class", "fal fa-cog fa-spin");
            window.sonomaClient.tx("com.sonoma.indicators", [this.props.data.uuid], (res) => {
                this.setState({
                    data: res,
                    show: true
                })
            }, (err) => {
                if (this._el) this._el.setAttribute("class", "fal fa-folder");
                window.state.error("An error occurred", err);
            })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    _select = () => {
        if (this.props.data.ntype == "G") this.props.onSelectGroup(this.props.data);
        if (this.props.data.ntype == "I") this.props.onSelect(this.props.data);
    };

    _onDragStart = (e) => {
        e.dataTransfer.setData("e", JSON.stringify(this.props.data));
    };

    render() {
        let icon = true,
            iconClass = "fal fa-folder";
        if (this.state.show) iconClass = "fal fa-folder-open";

        if (this.props.data.ntype == "I") icon = false;

        let children = [];
        if (this.state.show && this.state.data) {
            children = this.state.data.map(item => {
                return <IndicatorNode data={item}
                                      onSelect={this.props.onSelect}
                                      onSelectGroup={this.props.onSelectGroup}/>
            })
        }

        return (
            <div className="tree-node">
                <div className="item row" draggable={true} onDragStart={this._onDragStart}>
                    {icon ?
                        <div className="grip" onClick={this._toggle}>
                            <i ref={(el) => this._el = el} className={iconClass}></i>
                        </div>
                        : null}
                    <div className="column label" onClick={this._select}>
                        {this.props.data.name.en || this.props.data.name}
                    </div>
                </div>

                {this.state.show ?
                    <div className="tree-children">
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
}

class IndicatorTree extends React.Component {
    static defaultProps = {
        parent: null
    };

    constructor(props) {
        super(props);

        this.state = {
            nodes: []
        };

        window.sonomaClient.tx("com.sonoma.indicators", [null], (res) => {
            this.setState({
                nodes: res
            })
        }, (err) => {
            window.state.error("Error loading indicators", err);
        })
    }

    render() {
        return (
            <div className="indicator-tree">
                {this.state.nodes.map(item => {
                    return <IndicatorNode data={item}
                                          onSelectGroup={this.props.onSelectGroup}
                                          onSelect={this.props.onSelect}/>
                })}
            </div>
        )
    }
}

class ViewIndicators extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            indicator: null,
            view: "DEFAULT"
        }
    }

    _onSelectGroup = (group) => {
        this.setState({
            indicator_group: group,
            indicator: null
        })
    };

    _onSelectIndicator = (indicator) => {
        this.setState({
            indicator_group: null,
            indicator: indicator
        })
    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        console.log(this.state);
        let view = <div></div>;
        if (this.state.indicator) view = <EditorView data={this.state.indicator}/>;
        if (this.state.indicator_group) view = <EditorView data={this.state.indicator_group}/>;

        return (
            <div className="row dark">

                <div className="column panel br" style={{maxWidth: "30%"}}>
                    <IndicatorTree
                        onSelectGroup={this._onSelectGroup}
                        onSelect={this._onSelectIndicator}/>
                </div>
                {view}
            </div>
        )
    }
}

export default ViewIndicators;
