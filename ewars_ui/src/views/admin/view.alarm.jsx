import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import MarkdownEditor from "../../controls/c.markdown.jsx";
import Form from "../../controls/forms/form.reporting.jsx";
import DefinitionEditor from "../../controls/editors/editor.definition.jsx";
import AlarmMonitoringView from "./view.alarm.monitoring.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-trash', 'DELETE', 'Delete alarm'],
];


const PERMISSIONS = [
    {
        name: "VIEW",
        label: "View alerts"
    },
    {
        name: "DELETE",
        label: "Delete alerts"
    },
    {
        name: "EXPORT",
        label: "Export alerts"
    },
    {
        name: "COMMENT",
        label: "Comment alerts"
    }
];

const FORM = {
    h_general: {
        type: "header",
        label: "General Settings",
        icon: "fa-cog",
        order: 0
    },
    name: {
        type: "text",
        label: "Name",
        required: true,
        order: 1
    },
    uuid: {
        type: "display",
        label: "UUID",
        order: 2
    },
    description: {
        type: "textarea",
        label: "Description",
        order: 3
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive'],
            ['ARCHIVED', 'Archived'],
            ['TEST', 'Test Mode']
        ],
        order: 4
    },
    eid_prefix: {
        type: "text",
        label: "EID Prefix",
        order: 5
    },
    h_ad: {
        type: "header",
        label: "Auto-Discard",
        order: 6
    },
    ad_enabled: {
        type: "buttonset",
        label: "Auto-Discard Enabled",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        order: 7
    },
    ad_period: {
        type: "duration",
        label: "Duration",
        conditions: [
            ['ad_enabled', 'eq', true]
        ],
        order: 8
    },
    h_permissions: {
        type: "header",
        label: "Permissions",
        order: 9
    },
    permissions: {
        type: "permissions",
        label: false,
        vertical: true,
        definition: PERMISSIONS,
        order: 10
    }
};

const _check = (view, _view) => {
    if (view == _view) return "ide-tab ide-tab-down";
    return "ide-tab";
};


class ViewGuidance extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block">
                <MarkdownEditor/>

            </div>
        )
    }

}

class ViewAlarm extends React.Component {
    state = {
        data: {},
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);

        if (this.props.config.uuid) {
            window.sonomaClient.tx("com.sonoma.alarm", this.props.config.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (res) => {
                window.state.error("An error occurred", err);
            })
        }
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <div className="column block block-overflow system-form">
                    <Form
                        allRoot={true}
                        onChange={this._onChange}
                        definition={FORM}
                        errors={this.state.errors || {}}
                        data={this.state.data || {}}/>
                </div>
            )
        }

        if (this.state.view == "MONITORING") view = <AlarmMonitoringView data={this.state.data} onChange={this._onMonitoringChange}/>;
        if (this.state.view == "GUIDANCE") view = <ViewGuidance data={this.state.data} onChange={this._onGuidanceChange}/>;
        if (this.state.view == "DEFINITION") view = (
            <DefinitionEditor
                onChange={this._onDefinitionChange}
                definition={this.state.data.workflow.definition || {}}
                worklfow={this.state.data.workflow.stages || []}/>
        )

        return (
            <div className="row dark">
                <div className="column br" style={{maxWidth: "34px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={_check("DEFAULT", this.state.view)}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div
                            onClick={() => this._view("MONITORING")}
                            className={_check("MONITORING", this.state.view)}>
                            <i className="fal fa-chart-line"></i>&nbsp;Monitoring
                        </div>
                        <div
                            onClick={() => this._view("DEFINITION")}
                            className={_check("DEFINITION", this.state.view)}>
                            <i className="fal fa-code-branch"></i>&nbsp;Definition
                        </div>
                        <div
                            onClick={() => this._view("GUIDANCE")}
                            className={_check("GUIDANCE", this.state.view)}>
                            <i className="fal fa-book"></i>&nbsp;Guidance
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Alarm</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewAlarm;
