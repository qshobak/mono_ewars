import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";
import utils from "../../utils/utils";
import DateField from "../../controls/fields/f.date.jsx";
import NumericField from "../../controls/fields/f.numeric.jsx";

import FORM_DEFINITION from "../../constants/definitions/form.location";

const _isArray = (data) => {
    return data.constructor === Array;
};

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save Change(s)'],
    '-',
    ['fa-trash', 'DELETE', 'Delete location']
];

const REPORTING_ACTIONS = [
    ['fa-plus', 'ADD', "Add New"]
];

const STATUSES = {
    ACTIVE: "Active",
    INACTIVE: "Inactive"
};

const PERIOD_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit period'],
    ['fa-trash', 'DELETE', 'Delete period']
];


class ReportingPeriod extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case "SAVE":
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_REPORTING_PERIOD",
                    body: "DELETE_REPORTING_PERIOD_BODY",
                    buttons: [
                        "DELETE",
                        "CANCEL"
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_REPORTING_PERIOD");
                        window.sonomaClient.tx("com.sonoma.reporting.delete", this.props.data.uuid, (res) => {
                            bl.rm();
                            window.state.growl('fa-trash', "DELETED_REPORTING_PERIOD");
                            window.dispatchEvent(new CustomEvent("reloadperiods", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("DELETE_ERROR", err);
                            window.dispatchEvent(new CustomEvent('reloadperiods', {}));
                        })
                    }
                })
        }
    };

    render() {
        let type = "Inherited";
        if (!this.props.data.pid) type = "Custom";
        return (
            <tr>
                <td>{this.props.data.form_name.en || this.props.data.form_name}</td>
                <td>{this.props.data.start_date}</td>
                <td>{this.props.data.end_date}</td>
                <td>{STATUSES[this.props.data.status]}</td>
                <td>{type}</td>
                <th>
                    <ActionGroup
                        actions={PERIOD_ACTIONS}
                        onAction={this._action}/>
                </th>
            </tr>
        )
    }
}

class ViewLocationReporting extends React.Component {
    state = {
        periods: []
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener("reloadperiods", this._reload);
    }

    componentWillUnmount() {
        window.removeEventListener("reloadperiods", this._reload);
    }

    _reload = () => {
        window.sonomaClient.tx('com.sonoam.locations.reporting_periods', this.props.location_id, (res) => {
            this.setState({
                periods: res
            })
        }, (err) => {
            window.state.error("ERROR_LOAD", err);
        })
    };

    componentWillMount() {
        window.sonomaClient.tx('com.sonoma.location.reporting_periods', this.props.location_id, (res) => {
            this.setState({
                periods: res
            })
        }, (err) => {
            window.state.error("Error retrieving reporting periods", err);
        })
    }

    _action = (action, data) => {
        switch (action) {
            case "EDIT":
                break;
            case "DELETE":
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Reporting Periods</div>
                    <ActionGroup
                        naked={true}
                        actions={REPORTING_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    <table width="100%" className="inline-grid">
                        <thead>
                        <tr>
                            <th>Form</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.periods.map(item => {
                            return <ReportingPeriod data={item} onAction={this._action}/>
                        })}
                        </tbody>
                    </table>

                </div>

            </div>
        )
    }
}

const POP_ACTIONS = [
    ['fa-plus', 'ADD', 'Add new'],
    ['fa-upload', 'IMPORT', 'Import'],
    '-',
    ['fa-download', "EXPORT", 'Export']
];

const POP_ITEM_ACTIONS = [
    ['fa-trash', "DELETE", "DELETE"]
];

class ViewPopulationItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _change = (prop, value) => {
        this.props.onChange(this.props.data.uuid, prop, value);
    };

    _action = (action) => {
        switch (action) {
            case "DELETE":
                this.props.onDelete(this.props.data.uuid);
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <tr>
                <td>
                    <DateField
                        name="start_date"
                        required={true}
                        value={this.props.data.start_date || ""}
                        onChange={this._change}
                        date_type="DAY"/>
                </td>
                <td>
                    <DateField
                        name="end_date"
                        value={this.props.data.end_date || ""}
                        onChange={this._change}
                        date_type="DAY"/>
                </td>
                <td>
                    <NumericField
                        required={true}
                        value={this.props.data.value || "0"}
                        onChange={this._change}
                        name="value"/>
                </td>
                <td>
                    <ActionGroup
                        actions={[['fa-trash', 'DELETE', 'DELETE']]}
                        onAction={this._action}/>
                </td>
            </tr>
        )
    }
}

class ViewLocationPopulation extends React.Component {
    static defaultProps = {
        data: []
    };

    constructor(props) {
        super(props);

        this.state = {editing: null};
    }

    _action = (action, data) => {
        switch (action) {
            case "ADD":
                let pops = this.props.data || [];
                let newItem = {
                    uuid: utils.uuid(),
                    start_date: null,
                    end_date: null,
                    value: 0
                };

                pops.push(newItem);
                this.setState({
                    editing: newItem.uuid
                }, () => {
                    this.props.onChange("population", pops);
                });
                break;

            default:
                break;
        }
    };

    _onPopUpdate = (uuid, prop, value) => {
        let pops = this.props.data || [];

        pops.forEach(item => {
            if (item.uuid = uuid) {
                item[prop] = value;
            }
        });

        this.props.onChange('population', pops);
    };

    _onPopDelete = (uuid) => {
        let pops = this.props.data || [];
        let rIndex;

        pops.forEach((item, index) => {
            if (item.uuid == uuid) rIndex = index;
        })

        pops.splice(rIndex, 1);
        if (uuid == this.state.editing) {
            this.setState({
                editing: null
            }, () => {
                this.props.onChange("population", pops);
            })
        } else {
            this.props.onChange("population", pops);
        }
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Population</div>
                    <ActionGroup
                        naked={true}
                        actions={POP_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    <table width="100%" className="inline-grid">
                        <thead>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Population</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.data.map(item => {
                            return <ViewPopulationItem
                                onChange={this._onPopUpdate}
                                onDelete={this._onPopDelete}
                                editing={this.state.editing == item.uuid}
                                data={item}/>
                        })}

                        </tbody>
                    </table>

                </div>
            </div>
        )
    }
}

class ViewLocationAssignments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }

        let data = {
            orders: {},
            filters: {lid: ["eq", "text", this.props.data.uuid]},
            limit: 1000,
            offset: 0
        };
        window.sonomaClient.tx("com.sonoma.assignments", data, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR", err);
        })
    }


    render() {
        return (
            <div className="column">
                <div className="row block block-overflow">
                    <table width="100%" className="inline-grid">
                        <thead>
                        <tr>
                            <th>User (Name)</th>

                        </tr>
                        </thead>
                    </table>
                </div>
            </div>

        )
    }
}

class ViewAdminLocation extends React.Component {
    state = {
        location: null,
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);

        if (this.props.data.uuid) {
            let bl = window.state.blocker("Loading location");
            window.sonomaClient.tx("com.sonoma.location.full", this.props.data.uuid, (res) => {
                bl.rm();
                this.setState({location: res});
            }, (err) => {
                bl.rm();
                console.log(err)
            });
        } else {
            this.state = {
                location: {
                    name: {en: "New Location"},
                    pcode: "",
                    pid: null,
                    groups: [],
                    org_id: null,
                    geometry_type: "POINT",
                    geojson: {},
                    status: "ACTIVE",
                    codes: {},
                    reporting: [],
                    population: []
                }
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid && nextProps.data.uuid) {
            let bl = window.state.blocker("Loading location");
            window.sonomaClient.tx("com.sonoma.location.full", nextProps.data.uuid, (res) => {
                bl.rm();
                this.setState({
                    location: res,
                    view: "DEFAULT"
                });
            }, (err) => {
                bl.rm();
                window.state.error("Error loading location", err);
            })
        }
    }


    _view = (view) => {
        this.setState({view: view});
    };

    _onChange = (prop, value) => {
        this.setState({
            location: {
                ...this.state.location,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "SAVE":
                this._save();
                break;
            case "DELETE":
                this._delete();
                break;
            default:
                break;
        }
    };

    _save = () => {
        let cmd = "com.sonoma.location.update";
        if (this.state.location.uuid) cmd = "com.sonoma.location.create";
        let bl = window.state.blocker("Updating location...");
        window.sonomaClient.tx(cmd, this.state.location, (res) => {
            bl.rm();
            window.state.growl("fa-save", "Location updated");
        }, (err) => {
            bl.rm();
            window.state.error("Error occurred", err);
        })
    };

    _delete = () => {
        window.state.dialog({
            title: "Delete Location?",
            body: "Are you sure you want to delete this location? Any records, child locations, alerts and assignments associated with this location will be deleted as well",
            buttons: [
                ['Delete'],
                ['Cancel']
            ]
        }, (res) => {
            if (res == 0) {
                let bl = window.state.blocker("Deleting location...");
                window.sonomaClient.tx("com.sonoma.location.delete", this.state.location.uuid, (res) => {
                    bl.rm();
                    window.state.growl("fa-trash", 'Location deleted');
                }, (err) => {
                    bl.rm();
                    window.state.error("An error occurred", err);
                })
            }
        })

    };

    render() {

        if (!this.state.location) {
            return (
                <div className="column dark">
                    <p>Loading...</p>
                </div>
            )
        }

        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <div className="column system-form">

                    <Form
                        definition={FORM_DEFINITION}
                        allRoot={true}
                        errors={{}}
                        data={this.state.location || {}}
                        onChange={this._onChange}/>
                </div>
            )
        }

        if (this.state.view == "POPULATION") {
            let population = this.state.location.population || [];
            if (!_isArray(population)) population = [];
            view = <ViewLocationPopulation
                onChange={this._onChange}
                data={population}
                location_id={this.state.location.uuid}/>;
        }
        if (this.state.view == "REPORTING") view =
            <ViewLocationReporting data={this.state.location} location_id={this.state.location.uuid}/>;


        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "30px"}}>
                    <div className="column grid-title">
                        {this.props.data.name.en || this.props.data.name}
                    </div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>

                <div className="row">
                    <div className="column br block"
                         style={{maxWidth: "35px", overflow: "hidden", position: "relative"}}>
                        <div className="ide-tabs">
                            <div
                                onClick={() => this._view("DEFAULT")}
                                className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                                <i className="fal fa-cog"></i>
                            </div>
                            <div
                                onClick={() => this._view("REPORTING")}
                                className={"ide-tab" + (this.state.view == "REPORTING" ? " ide-tab-down" : "")}>
                                <i className="fal fa-clipboard"></i>&nbsp;Reporting
                            </div>
                            <div
                                onClick={() => this._view("POPULATION")}
                                className={"ide-tab" + (this.state.view == "POPULATION" ? " ide-tab-down" : "")}>
                                <i className="fal fa-users"></i>&nbsp;Population
                            </div>
                            <div onClick={() => this._view("ASSIGNMENTS")}
                                 className={"ide-tab" + (this.state.view == "ASSIGNMENTS" ? " ide-tab-down" : "")}>
                                <i className="fal fa-clipboard"></i>&nbsp;Assignments
                            </div>
                        </div>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewAdminLocation;
