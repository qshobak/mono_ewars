import React from "react";

import SystemForm from "../../../controls/forms/form.system.jsx";
import FORM_DEFINITION from "../../../constants/definitions/form.location";

class ViewLocationSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SystemForm
                definition={FORM_DEFINITION}
                errors={{}}
                onChange={this._onChange}
                data={this.props.data || {}}/>
        )
    }
}

export default ViewLocationSettings;
