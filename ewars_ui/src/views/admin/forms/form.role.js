const ROLE_FORM = [
    {
        name: "name",
        type: "text",
        label: "Name",
        required: true
    },
    {
        name: "inherits",
        label: "Inherits from",
        type: "select",
        options: [
            ['USER', 'Reporting User'],
            ['ACCOUNT_ADMIN', 'Account Administrator'],
            ['REGIONAL_ADMIN', 'Regional Adminstrator']
        ],
        required: true
    },
    {
        name: "status",
        label:" Status",
        type: "select",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive']
        ],
        required: true
    },
    {
        name: "description",
        label: "Description",
        required: false,
        type: "textarea"
    }
];

export default ROLE_FORM;
