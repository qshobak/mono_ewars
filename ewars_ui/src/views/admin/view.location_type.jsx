import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";
import dialog from "../../utils/dialog";
import utils from "../../utils/utils";


const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    '-',
    ['fa-trash', 'DELETE', 'Delete location type']
];

const FORM_DEFINITION = {
    name: {
        type: "text",
        label: "Name",
        i18n: true,
        required: true,
        order: 0
    },
    description: {
        type: "text",
        label: "Description",
        order: 1
    },
    restrictions: {
        type: "lti_restrictions",
        label: "Restrictions",
        required: false,
        order: 2
    }
}


class ViewLocationTypeEditor extends React.Component {
    state = {
        lt: null,
        errors: {}
    };

    constructor(props) {
        super(props);

        this.state.lti = utils.copy(this.props.data);
    }

    _action = (action) => {
        switch(action) {
            case "SAVE":
                if (!this.state.lt.id) {
                    this._create();
                } else {
                    this._update();
                }
                break;
            case "DELETE":
                this._delete();
                break;
            default:
                break;
        }
    };

    _onChange = (prop, value) => {
        this.setState({
            lti: {
                ...this.state.lti,
                [prop]: value
            }
        })
    };

    _create = () => {
        window.sonomaClient.tx('com.sonoma.location_type.create', this.state.lti, (res) => {
            this.setState({
                lti: res
            });
            window.stage.growl("Location type created");
        }, (err) => {
            window.state.error("An error occurred", err);
        })
    };

    _update = () => {
        window.sonomaClient.tx("com.sonoma.location_type.update", this.state.lti, (res) => {
            this.setState({
                lti: res
            });
            window.state.growl("Location type update");
        }, (err) => {
            window.state.error("An error occurred", err);
        })
    };

    _delete = () => {
        new dialog.Dialog({
            title: "Delete location type?",
            body: "Are you sure you want to delete this location type? All locations with this type will be reverted to 'Uncategorized'",
            buttons: [
                'Delete',
                'Cancel'
            ]
        }, (res) => {
            if (res == 0) {
                window.sonomaClient.tx('com.sonoma.location_type.delete', this.state.lti, (res) => {
                    window.state.growl("Location type deleted");
                    this.props.onClear();
                }, (err) => {
                    window.state.error("An error occurred", err);
                })
            }
        })
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">{this.props.data.name.en || this.props.data.name}</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row system-form">
                    <Form
                        allRoot={true}
                        data={this.props.data}
                        definition={FORM_DEFINITION}
                        onChange={this._onChange}
                        errors={this.state.errors}/>
                </div>
            </div>
        )
    }
}

export default ViewLocationTypeEditor;
