import React from 'react';

import ActionGroup from "../../controls/c.actiongroup.jsx";

import EditorLayout from "../../controls/editors/editor.layout.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', "Save Change(s)"]
];

class ViewSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow">

            </div>
        )
    }
}

class ViewLayout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <EditorLayout/>
        )
    }
}

class ViewVariables extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">

            </div>
        )
    }
}

class ViewAdminHUD extends React.Component {
    state = {
        view: "DEFAULT",
        data: {}
    };

    constructor(props) {
        super(props);
    }

    _view = (view) => {
        this.setState({view})
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") view = <ViewSettings data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == "LAYOUT") view = <ViewLayout data={this.state.data} onChange={this._onChange}/>;
        if (this.state.layout == "VARIABLES") view = <ViewVariables data={this.state.data} onChange={this._onChange}/>;


        return (
            <div className="row dark">
                <div className="column br" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className="ide-tab">
                            <i className="fal fa-cog"></i>
                        </div>
                        <div
                            onClick={() => this._view("LAYOUT")}
                            className="ide-tab">
                            <i className="fal fa-list"></i>&nbsp;Layout
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">HUD</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>

                    {view}
                </div>
            </div>
        )
    }
}

export default ViewAdminHUD;
