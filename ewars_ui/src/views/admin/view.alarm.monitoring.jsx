import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import DimensionTree from "../../controls/c.tree.dimensions.jsx";
import MeasureTree from "../../controls/c.tree.measures.jsx";
import AlarmUtils from "../../utils/alarm_utils";

const GROUP_ACTIONS = [
    ['fa-calculator', 'CALCULATION', 'Add Calculation']
];


const THRESH_ACTIONS = [];
const CALC_ACTIONS = [];

class DataGroup extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="data-group">
                <div className="row data-group-header">
                    <div className="column grid-title">Monitored Data</div>
                    <ActionGroup
                        naked={true}
                        actions={GROUP_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="data-group-body">

                </div>
            </div>
        )
    }
}


class CalculationGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="data-group">
                <div className="row data-group-header" style={{background: "rgba(58, 32, 1, 1"}}>
                    <div className="column grid-title">Calculation</div>
                    <ActionGroup
                        actions={CALC_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div
                    style={{maxHeight: "30px"}}
                    className="data-group-calc">
                    <div className="column">
                        <div className="row">
                            <div
                                style={{maxWidth: "20px", textAlign: "center", background: "black"}}
                                className="column">
                                <i className="fal fa-calculator"></i>
                            </div>
                        </div>
                        <div className="column">
                            <input type="text"/>
                        </div>
                    </div>
                </div>
                <div className="data-group-body">
                    <div className="column">
                        <div
                            style={{
                                background: "rgba(0,0,0,1)",
                                overflowX: "show",
                                overflowY: "scroll",
                                position: "relative"
                            }}
                            className="row">

                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

class ThresholdGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="data-group">
                <div className="row data-group-header">
                    <div className="column grid-title">Criteria</div>
                    <ActionGroup
                        naked={true}
                        actions={THRESH_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="data-group-body">

                </div>
            </div>
        )
    }
}

class ViewAlarmMonitoring extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        console.log(this.props.data);
        return (
            <div className="column">
                <div className="row">
                    <div className="column panel br">
                        <div className="row block block-overflow bb" style={{maxHeight: "50%"}}>
                            <DimensionTree/>
                        </div>
                        <div className="row block block-overflow" style={{maxHeight: "50%"}}>
                            <MeasureTree/>
                        </div>
                    </div>
                    <div className="column block block-overflow">
                        <DataGroup/>
                        <ThresholdGroup/>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewAlarmMonitoring;