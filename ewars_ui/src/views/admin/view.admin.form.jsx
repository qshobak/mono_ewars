import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";
import DefinitionEditor from "../../controls/editors/editor.definition.jsx";
import ViewGrid from "../view.grid.jsx";
import MarkdownEditor from "../../controls/c.markdown.jsx";

const FORM = {
    t_header: {
        type: "header",
        label: "General Settings",
        icon: "fa-cog",
        order: 0 },
    name: {
        label: "Name",
        i18n: true,
        type: "text",
        required: true,
        order: 1
    },
    id: {
        type: "display",
        label: "ID",
        order: 2
    },
    status: {
        type: "select",
        options: [
            ["ACTIVE", 'Active'],
            ['INACTIVE', 'Inactive'],
            ['DRAFT', 'Draft'],
            ['ARCHIVED', 'Archived']
        ],
        label: "Status",
        require: true,
        order: 3
    },
    version: {
        type: "display",
        label: "Version",
        order: 4
    },
    eid_prefix: {
        type: "text",
        label: "EID Prefix",
        required: false,
        order: 5
    },
    p_header: {
        type: "header",
        label: "Permissions",
        order: 7
    },
    permissions: {
        type: "permissions",
        label: false,
        vertical: true,
        definition: {},
        order: 8
    }
};

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save Change(s)'],
    '-',
    ['fa-trash', 'DELETE', 'Delete form'],
    ['fa-download', 'EXPORT', 'Export form']
];

const _check = (view, _view) => {
    if (view == _view) return "ide-tab ide-tab-down";
    return "ide-tab";
};


class ViewGuidance extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block">
                <MarkdownEditor/>

            </div>
        )
    }

}

class ViewAdminForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "SETTINGS",
            data: {
                version: 2,
                name: {en: "New Form"},
                definition: {
                    definition: {},
                    stages: []
                },
                status: "DRAFT",
                etl: {},
                pub_submit_url: "",
                inline_edit: false,
                public: false
            }
        };

        if (this.props.config.uuid) {
            window.sonomaClient.tx("com.sonoma.form", this.props.config.uuid, (res) => {
                this.setState({
                    data: res || {}
                })
            }, (err) => console.log(err))
        }
    }

    _view = (view) => {
        this.setState({view: view})
    };

    _change = (data) => {
        this.setState({
            data: {
                ...this.state.data,
                definition: data
            }
        })
    };

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch(action) {
            case "SAVE":
                let cmd = "com.sonoma.form.create";
                let successMessage = "Form created";
                if (this.state.data.uuid) {
                    cmd = "com.sonoma.form.update";
                    successMessage = "Form updated";
                }
                window.sonomaClient.tx(cmd, this.state.data, (res) => {
                    if (!this.state.data.uuid) {
                        this.setState({
                            data: res
                        });
                    }

                    window.state.growl("fa-save", successMessage);
                }, (err) => {
                    window.state.error("An error occurred", err);
                })
                break;
            default:
                break;
        }
    };

    render() {
        let view;

        if (this.state.view == "SETTINGS") {
            view = (
                <div className="column system-form">
                    <Form
                        allRoot={true}
                        definition={FORM}
                        data={this.state.data || {}}
                        onChange={this._onChange}
                        errors={{}}/>
                </div>
            )
        }

        if (this.state.view == "DEFINITION") {
            view = <DefinitionEditor
                version={this.state.data.version || 1}
                definition={this.state.data.definition || {}} onChange={this._change}/>;
        }

        if (this.state.view == "GUIDANCE") {
            view = <ViewGuidance data={this.state.data} onChange={this._onGuidanceChange}/>;
        }

        return (
            <div className="row dark">
                <div className="column br" style={{maxWidth: "30px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div onClick={() => this._view("SETTINGS")}
                             className={_check("SETTINGS", this.state.view)}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div onClick={() => this._view("DEFINITION")}
                             className={_check("DEFINITION", this.state.view)}>
                            <i className="fal fa-list"></i>&nbsp;Definition
                        </div>
                        <div onClick={() => this._view("GUIDANCE")}
                             className={_check("GUIDANCE", this.state.view)}>
                            <i className="fal fa-question-circle"></i>&nbsp;Guidance
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Form</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewAdminForm;
