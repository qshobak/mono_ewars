import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)']
];

class ViewModem extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {

    };

    render() {
        return (
            <div className="column">
                <div className="row bb" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">GSM Modem</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">

                </div>
            </div>
        )
    }
}

export default ViewModem;
