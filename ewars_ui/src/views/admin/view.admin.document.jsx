import React from 'react';

import ActionGroup from "../../controls/c.actiongroup.jsx";
import EditorTemplate from "../../controls/editors/editor.template.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)']
];

class ViewSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">

            </div>
        )
    }
}

class ViewAdminDocument extends React.Component {
    state = {
        view: "DEFAULT",
        data: {}
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.document", this.props.config.uuid, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        })
    }

    _action = (action) => {

    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") view = <ViewSettings data={this.state.data} onChange={this._onSettingChange}/>;
        if (this.state.view == "LAYOUT") view = <EditorTemplate data={this.state.data} onChange={this._onChange} pages={true}/>;

        return (
            <div className="column dark">
                <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                    <div className="column grid-title"></div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row">
                    <div className="column br block" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs">
                            <div
                                onClick={() => this._view("DEFAULT")}
                                className={"ide-tab" + (this.state.view == 'DEFAULT' ? " ide-tab-down" : "")}>
                                <i className="fal fa-cog"></i>&nbsp;Settings
                            </div>
                            <div
                                onClick={() => this._view("LAYOUT")}
                                className={"ide-tab" + (this.state.view == "LAYOUT" ? " ide-tab-down" : "")}>
                                <i className="fal fa-list"></i>&nbsp;Layout
                            </div>
                        </div>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewAdminDocument;