import React from "react";

import EditorLayout from "../../controls/editors/editor.layout.jsx";
import Form from "../../controls/forms/form.reporting.jsx";


const FORM = {
    header_gen: {
        type: "header",
        label: "General Settings"
    },
    name: {
        type: 'text',
        label: 'Name',
        required: true
    },
    status: {
        type: "select",
        label: "Status",
        required: true,
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive']
        ]
    },
    header_permissions: {
        type: "header",
        label: "Permissions"
    },
    permissions: {
        vertical: true,
        type: "permissions",
        label: false
    }
};

class ViewAdminDashboard extends React.Component {
    state = {
        data: {},
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);

    }

    _onChange = (prop, data) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _view = (view) => {
        this.setState({view: view});
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <div className="column system-form">
                    <Form
                        errors={{}}
                        definition={FORM}
                        onChange={this._onChange}
                        allRoot={true}/>
                </div>
            )
        }

        if (this.state.view == "LAYOUT") {
            view = (
                <EditorLayout
                    definition={{}}
                    onChange={this._onChange}
                    variables={{}}/>
            )
        }

        return (
            <div className="row dark">
                <div className="column br block"
                     style={{maxWidth: "35px", position: 'relative', overflow: "hidden", background: "#CCCCCC"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cog"></i>&nbsp;Settings
                        </div>
                        <div
                            onClick={() => this._view("LAYOUT")}
                            className={"ide-tab" + (this.state.view == "LAYOUT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-list"></i>&nbsp;Layout
                        </div>
                    </div>
                </div>
                {view}
            </div>
        )
    }
}

export default ViewAdminDashboard;