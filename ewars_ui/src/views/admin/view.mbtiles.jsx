import React from "react";

const TILE_PACKAGES = [
    [
        "AFRICA",
        "Africa",
        [
            ["ALGERIA", "Algeria"],
            ["ANGOLA", "Angola"],
            ["BENIN", "Benin"],
            ["BOTSWANA", "Botswana"],
            ["BURKINA_FASO", "Burkina Faso"],
            ["BURUNDI", 'Burundi'],
            ["CAMEROON", "Cameroon"],
            ['CANARY_ISLANDS', 'Canary Islands'],
            ['CAPE_VERDE', "Cape Verde"],
            ["CENTRAL_AFRICAN_REPUBLIC", "Central African Republic"],
            ["CHAD", "Chad"],
            ["COMORES", 'Comores'],
            ["CONGO", "Congo (Democratic Republic/Kinshasa)"],
            ["CONGO_R", 'Congo (Republic/Brazzaville)'],
            ["DJIBOUTI", "Djibouti"],
            ["EGYPT", "Egypt"],
            ["EQUATORIAL_GUINEA", 'Equitorial Guinea'],
            ["ERITREA", "Eritrea"],
            ["ETHIOPIA", 'Ethiopia'],
            ['GABON', 'Gabon'],
            ['GHANA', 'Ghana'],
            ["GUINEA", "Guinea"],
            ["GUINEA_BISSAU", 'Guinea-Bissau'],
            ["IVORY_COAST", 'Ivory Coast'],
            ['KENYA', 'Kenya'],
            ["LESOTHO", "Lesotho"],
            ["LIBERIA", 'Liberia'],
            ['LIBYA', 'Libya'],
            ['MADAGASCAR', 'Madagascar'],
            ["MALAWI", 'Malawi'],
            ["MALI", "Mali"],
            ["MAURITANIA", 'Mauritania'],
            ["MAURITIUS", "Mauritius"],
            ["MOROCCO", 'Morocco'],
            ["MOZAMBIQUE", "Mozambique"],
            ["NAMIBIA", "Namibia"],
            ["NIGER", 'Niger'],
            ["NIGERIA", 'Nigeria'],
            ['REP_SUDAN', 'Republic of the Sudan'],
            ["RWANDA", "Rwanda"],
            ["ST_HELENA", "St. Helena, Ascension, and Tristan..."],
            ["SAO_TOME_AND_PRINCIPE", 'Sai Tome and Principe'],
            ["SENEGAL_GAMBIA", "Senegal and Gambia"],
            ["SEYCHELLES", 'Seychelles'],
            ['SIERRA_LEONE', 'Sierra Leone'],
            ['SOUTH_AFRICA', 'South Africa'],
            ['SOUTH_AFRICA_LESOTHO', 'South Africa and Lesotho'],
            ["SOUTH_SUDAN", 'South Sudan'],
            ["SWAZILAND", "Swaziland"],
            ["TANZANIA", "Tanzania"],
            ["TOGO", 'Togo'],
            ["TUNISIA", 'Tunisia'],
            ["UGANDA", 'Uganda'],
            ["ZAMBIA", 'Zambia'],
            ["ZIMBABWE", 'Zimbabwe']
        ],
    ],
    [
        "ASIA",
        "Asia",
        [
            ["RUSSIAN_FEDERATION", 'Russian Federation'],
            ["AFGHANISTAN", 'Afghanistan'],
            ["AZERBAIJAN", 'Azerbaijan'],
            ["BANGLADESH", 'Bangladesh'],
            ["CAMBODIA", 'Cambodia'],
            ["CHINA", 'China'],
            ["GCC_STATES", 'GCC States'],
            ["INDIA", 'India'],
            ["INDONESIA", 'Indonesia'],
            ["IRAN", "Iran"],
            ["IRAQ", 'Iraq'],
            ["ISRAEL_PALESTINE", 'Israel and Palestine'],
            ["JAPAN", 'Japan'],
            ['JORDAN', 'Jordan'],
            ['KAZAKHSTAN', 'Kazakhstan'],
            ["KYRGYZSTAN", "Kyrgyzstan"],
            ["LEBANON", "Lebanon"],
            ['MAL_SING_BRU', 'Malaysia, Singapore and Brunei'],
            ["MALDIVES", 'Maldives'],
            ["MONGOLIA", "Mongolia"],
            ["MYANMAR", 'Myanmar'],
            ['NEPAL', "Nepal"],
            ['NORTH_KOREA', 'North Korea'],
            ['PAKISTAN', 'Pakistan'],
            ["PHILLIPINES", 'Phillipines'],
            ["RUSSIA_ASIAN_PART", 'Russia Asian Part'],
            ["SOUTH_KOREA", 'South Korea'],
            ['SRI_LANKA', 'Sri Lanka'],
            ['SYRIA', 'Syria'],
            ['TAIWAN', 'Taiwan'],
            ["TAJIKISTAN", 'Tajikistan'],
            ["THAILAND", 'Thailand'],
            ["TURKMENISTAN", 'Turkmenistan'],
            ["UZBEKISTAN", 'Uzbekistan'],
            ["VIETNAM", 'Vietnam'],
            ["YEMEN", 'Yemen']
        ]
    ],
    [
        "AUS_OCEANIA",
        "Australia and Oceania",
        [
            ["AUSTRALIA", 'Australia'],
            ["FIJI", 'Fiji'],
            ['NEW_CALEDONIA', 'New Caledonia'],
            ["NEW_ZEALAND", "New Zealand"]
        ]
    ],
    [
        "CENTRAL_AMERICA",
        "Central America",
        [
            ["BELIZE", 'Belize'],
            ["CUBA", 'Cuba'],
            ["GUATEMALA", 'Guatemala'],
            ["HAITI_DOM", 'Haiti and Dominican Republic'],
            ["NICARAGUA", "Nicaragua"]
        ]
    ],
    [
        "EUROPE",
        "Europe",
        [
            ["ALBANIA", "Albania"],
            ['ALPS', 'Alps'],
            ['ANDORRA', 'Andorra'],
            ["AUSTRIA", 'Austria'],
            ["AZORES", 'Azores'],
            ["BELARUS", 'Belarus'],
            ["BELGIUM", "Belgium"],
            ["BOSNIA_HERZEGOVINA", "Bosnia and Herzegovina"],
            ["BRITISH_ISLES", 'British Isles'],
            ["BULGARIA", 'Bulgaria'],
            ["CROATIA", 'Croatia'],
            ["CYRPUS", 'Cyprus'],
            ["CZECH_REP", 'Czech Republic'],
            ["DACH", "Dach"],
            ["DENMARK", 'Denmark'],
            ["ESTONIA", 'Estonia'],
            ["FAROE_ISLANDS", "Faroe Islands"],
            ["FINLAND", 'Finland'],
            ["FRANCE", 'France'],
            ["GEORGIA", "Georgia"],
            ["GERMANY", "Germany"],
            ["GREAT_BRITAIN", "Great Britain"],
            ["GREECE", 'Greece'],
            ["HUNGARY", "Hungary"],
            ["ICELAND", 'Iceland'],
            ["IRE_NIRE", 'Ireland and Northern Ireland'],
            ["ISLE_OF_MAN", 'Isle of Man'],
            ["ITALY", 'Italy'],
            ["KOSOVO", 'Kosovo'],
            ["LATVIA", 'Latvia'],
            ["LIECHTENSTEIN", 'Liechtenstein'],
            ["LITHUANIA", 'Lithuania'],
            ["LUXEMBOURG", 'Luxembourg'],
            ["MACEDONIA", 'Macedonia'],
            ['MALTA', "Malta"],
            ["MOLDOVA", "Moldova"],
            ["MONACO", "Monaco"],
            ["MONTENEGRO", "Montenegro"],
            ["NETHERLANDS", 'Netherlands'],
            ["NORWAY", 'Norway'],
            ["POLAND", 'Poland'],
            ["PORTUGAL", 'Portugal'],
            ["ROMANIA", 'Romania'],
            ["RUSSIA_EUR", 'Russia European Part'],
            ["SERBIA", 'Serbia'],
            ["SLOVAKIA", 'Slovakia'],
            ["SLOVENIA", 'Slovenia'],
            ["SPAIN", 'Spain'],
            ["SWEDEN", 'Sweden'],
            ["SWITZERLAND", 'Switzerland'],
            ["TURKEY", 'Turkey'],
            ["UKRAINE", 'Ukraine']
        ]
    ],
    [
        "N_AMERICA",
        "North America",
        [
            ["USA", 'United States of America'],
            ["CAD", "Canada"],
            ["GREENLAND", "Greenland"],
            ["MEX", "Mexico"],
            ["US_MW", "US MidWest"],
            ["US_NE", 'US NorthEast'],
            ["US_P", "US Pacific"],
            ["US_S", 'US South'],
            ["US_W", "US West"]
        ]
    ],
    [
        "S_AMERICA",
        "South America",
        [
            ["ARGENTINA", "Argentina"],
            ["BOLIVIA", "Bolivia"],
            ["BRAZIL", 'Brazil'],
            ["CHILE", "Chile"],
            ["COLOMBIA", 'Colombia'],
            ["ECUADOR", 'Ecuador'],
            ["PARAGUAY", 'Paraguay'],
            ["PERU", "Peru"],
            ["SURINAME", "Suriname"],
            ["URUGUAY", 'Uruguay']
        ]
    ]
];

class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-handle sub-item">
               <i className="fal fa-circle"></i>&nbsp;{this.props.data[1]}
            </div>

        )
    }
}

class Geo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-section">{this.props.data[1]}</div>
        )
    }
}

class ViewMBTiles extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let items = [];

        TILE_PACKAGES.forEach(region => {
            items.push(<Geo data={region}/>);

            region[2].forEach(cot => {
                items.push(<Item data={cot}/>);
            })
        });

        return (
            <div className="column">
                <div className="row bb" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Tile Packages</div>
                </div>
                <div className="row block block-overflow">
                    {items}
                </div>
            </div>
        )
    }
}

export default ViewMBTiles;
