let state = window.state;

let _ws = null;
let _wsConnected = false;
let _reqs = {};
let _queued = [];
let _subscriptions = {};
let _heartBeatInterval = 100000;
let _apiVersion = 1;
let _wsAuthenticated = false;
let _wsConnecting = false;
let _clientId = null;
let _backoffTimer = null;

const ENDPOINT = "ws://_val.ewars.ws:9000/ws/desktop";
// const ENDPOINT = "ws://staging.ewars.ws:9000/ws/desktop";

const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};


const ws = function (command, data, opcode) {
    let _opcode = 0;
    if (opcode != null) _opcode = opcode;

    let reqId = uuid();

    let resolve, reject;
    let promise = new Promise((_resolve, _reject) => {
        resolve = _resolve;
        reject = _reject;
    });

    _reqs[reqId] = {
        resolve,
        reject,
    };

    if (!_ws || (_wsConnected === false && _wsConnecting === false)) {
        _queued.push({
            _: reqId,
            cmd: command,
            data,
            op: _opcode,
            tki: state.account ? state.account.tki : null,
            uid: state.account ? state.account.uid : null,
        });

        _wsConnecting = true;
        _ws = new WebSocket(ENDPOINT);
        _ws.onopen = function (evt) {
            console.log("WS CONNECTED");
            _wsConnected = true;
        };

        _ws.onmessage = function (evt) {
            let result = JSON.parse(evt.data);

            switch (result.t) {
                case 'READY': // We're connected to the remote
                    _heartBeatInterval = result.heartbeat_interval;
                    _apiVersion = result.v;
                    _wsConnected = true;
                    _wsConnecting = false;

                    let tokens = state.accounts.map(item => {
                        return item.token;
                    });

                    if (tokens.length > 0) {
                        _ws.send(JSON.stringify({
                            op: 2,
                            _: uuid(),
                            cmd: 'AUTH',
                            d: tokens
                        }));
                    } else {
                        let auth_calls = _queued.filter(item => {
                            return item.cmd == "AUTH"
                        });
                        _queued = _queued.filter(item => {
                            return item.cmd != "AUTH"
                        });
                        if (auth_calls.length > 0) {
                            auth_calls.forEach((item, index) => {
                                _ws.send(JSON.stringify({
                                    op: item.op,
                                    _: item._,
                                    cmd: item.cmd,
                                    d: item.data
                                }))
                            })
                        }
                    }
                    break;
                case 'AUTHED': // We're authenticated against the remote
                    _wsAuthenticated = true;
                    _wsConnected = true;

                    if (_queued.length > 0) {
                        _queued.forEach(item => {
                            _ws.send(JSON.stringify({
                                op: item.op,
                                _: item._,
                                cmd: item.cmd,
                                d: item.data,
                                tki: item.tki || null,
                                uid: item.uid || null
                            }))
                            _queued = [];
                        })
                    }
                    break;
                case 'ACTIVES':
                    console.log("ACTIVES", result);
                    break;
                case 'SEED_CHANNELS':
                    console.log(result);
                    state.setTeams(result.d[state.account.tki] || []);
                    break;
                default:
                    let resolvers = _reqs[result._] || null;
                    if (resolvers) {
                        resolvers.resolve(result.data || result.d);
                    }
                    break;
            }
        }

        _ws.onerror = function (evt) {
            console.log('ERR', evt);
        }
    } else {
        if (_wsConnected && _wsAuthenticated && !_wsConnecting) {
            _ws.send(JSON.stringify({
                _: reqId,
                op: _opcode,
                cmd: command,
                d: data,
                tki: state.account.tki,
                uid: state.account.uid
            }));
        } else {
            _queued.push({
                _: reqId,
                cmd: command,
                d: data,
                op: _opcode,
                tki: state.account.tki,
                uid: state.account.uid
            });
        }
    }

    return promise;
}


export default ws;
