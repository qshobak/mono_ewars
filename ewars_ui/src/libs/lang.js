const _l = (code) => {
    return code;
};

const __ = (data) => {
    return data.en || data;
};

export default _l;

export {
    __,
    _l
}
