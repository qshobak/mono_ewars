import React from "react";

import ADMIN_COMPONENTS from "../constants/const.admin.components.jsx";


class MenuItem extends React.Component {
    _click = () => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                grid: this.props.data.grid || null,
                name: this.props.data.name,
                type: this.props.data.type || "GRID"
            }
        }));
    };

    render() {
        return (
            <div className="dash-handle" onClick={this._click}>
                <div className="inner">
                    <i className={"fal " + this.props.data.icon}></i>&nbsp;{this.props.data.name}
                </div>
            </div>
        )
    }
}

class MenuAdmin extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row block block-overflow"
                 ref={(el) => this._el = el}>
                {ADMIN_COMPONENTS.map(item => {
                    return <MenuItem data={item}/>
                })}
            </div>
        )
    }
}

export default MenuAdmin;
