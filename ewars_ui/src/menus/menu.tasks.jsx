import React from "react";


const LOCATION_REQUEST = "LOCATION_REQUEST";
const ASSIGNMENT_REQUEST = "ASSIGNMENT_REQUEST";
const REGISTRATION_REQUEST = "REGISTRATION_REQUEST";
const ACCESS_REQUEST = "ACCESS_REQUEST";
const AMENDMENT_REQUEST = "AMENDMENT_REQUEST";
const RETRACTION_REQUEST = "RETRACTION_REQUEST";
const ORGANIZATION_REQUEST = "ORGANIZATION_REQUEST";

class MenuTasks extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            filter: null,
            modal: null,
            counts: {}
        };

        window.sonomaClient.tx("com.sonoma.self.tasks", [], (res) => {
            this.setState({
                items: res,
                counts: {
                    ASSIGNMENT_REQUEST: res.filter(item => {
                        return item.data.task_type == "ASSIGNMENT_REQUEST";
                    }).length,
                    RETRACTION_REQUEST: res.filter(item => {
                        return item.data.task_type == 'RETRACTION_REQUEST';
                    }).length,
                    AMENDMENT_REQUEST: res.filter(item => {
                        return item.data.task_type == "AMENDMENT_REQUEST";
                    }).length,
                    REGISTRATION_REQUEST: res.filter(item => {
                        return item.data.task_type == "REGISTRATION_REQUEST";
                    }).length,
                    ACCESS_REQUEST: res.filter(item => {
                        return item.data.task_type == "ACCESS_REQUEST";
                    }).length,
                    ORGANIZATION_REQUEST: res.filter(item => {
                        return item.data.task_type == "ORG_REQUEST"
                    }).length
                }
            })
        }, (err) => {
            console.log(err);
        });
    }

    _view = (task_type) => {
        window.state.addTab("TASKS", {
            filter: task_type,
            name: "Tasks"
        });
    };

    render() {
        let items = this.state.items.sort((a, b) => {
            if (a.created > b.created) return 1;
            if (a.created > b.created) return -1;
            return 0;
        });

        if (this.state.filter) {
            items = items.filter(item => {
                return item.data.task_type == this.state.filter;
            })
        }

        return (
            <div className="row block block-overflow">
                <div
                    onClick={() => this._view("ALL")}
                    className="dash-handle">
                    <i className="fal fa-tasks"></i>&nbsp;All Tasks ({this.state.items.length})
                </div>

                <div
                    onClick={() => this._view("COMPLETED")}
                    className="dash-handle">
                    <i className="fal fa-check-box"></i>&nbsp;Completed
                </div>

                {window.state.account.role != "USER" ?
                    <div>
                        <div className="dash-section">
                            <i className="fal fa-cogs"></i>&nbsp;Admin Tasks
                        </div>
                        <div
                            onClick={() => this._view(REGISTRATION_REQUEST)}
                            className="dash-handle sub-item">
                            Registration requests ({this.state.counts.REGISTRATION_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(ASSIGNMENT_REQUEST)}
                            className="dash-handle sub-item">
                            Assignment requests ({this.state.counts.ASSIGNMENT_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(ACCESS_REQUEST)}
                            className="dash-handle sub-item">
                            Access requests ({this.state.counts.ACCESS_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(LOCATION_REQUEST)}
                            className="dash-handle sub-item">
                            Location requests ({this.state.counts.LOCATION_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(RETRACTION_REQUEST)}
                            className="dash-handle sub-item">
                            Retraction requests ({this.state.counts.RETRACTION_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(AMENDMENT_REQUEST)}
                            className="dash-handle sub-item">
                            Amendment requests ({this.state.counts.AMENDMENT_REQUEST || 0})
                        </div>
                        <div
                            onClick={() => this._view(ORGANIZATION_REQUEST)}
                            className="dash-handle sub-item">
                            Organization requests ({this.state.counts.ORG_REQUEST || 0})
                        </div>
                    </div>
                    : null}

                <div className="dash-section">
                    <i className="fal fa-clipboard"></i>&nbsp;Reporting tasks
                </div>

                <div className="dash-section">
                    <i className="fal fa-bell"></i>&nbsp;Alert tasks
                </div>

            </div>
        )
    }
}

export default MenuTasks;
