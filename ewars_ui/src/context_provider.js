const React = require("react");

const AppContext = React.createContext({
    tx: null,
    cx: null,
    sonoma: null,
    dialog: null,
    Menu: null,
    MenuItem: null,
    growl: null,
    prompt: null,
    context_menu: null,
    user: null,
    account: null
});

module.exports = AppContext;
