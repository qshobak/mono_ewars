const _el = (el_type, className, style, text) => {
    let el = document.createElement(el_type);
    el.setAttribute("class", className);

    if (style) {
        for (let i in style) {
            el.style[i] = style[i];
        }
    }

    if (text) {
        let textNode = document.createTextNode(text);
        el.appendChild(textNode);
    }

    return el;
};

class Dialog {
    constructor(props, callback) {
        this._config = props;
        this._callback = callback;

        this.show();
    }

    show = () => {
        let wrapper = _el("div", 'dialog-wrapper');
        let inner = _el("div", "dialog-inner");

        let header = _el("div", "row bb gen-toolbar dialog-header", {maxHeight: "35px"});
        header.appendChild(_el("div", "column grid-title", null, this._config.title));

        inner.appendChild(header);

        let body = _el("div", "row dialog-body");
        body.appendChild(_el("p", 'dialog-message', null, this._config.body));

        inner.appendChild(body);

        let buttons = _el("div", "row dialog-buttons");
        buttons.appendChild(_el("div", "column"));
        let buttonHolder = _el('div', 'column block');

        let buttonGroup = _el("div", 'btn-group pull-right');

        this._config.buttons.forEach((item, index) => {
            let button = _el("button", "bttn", null, item);
            button.addEventListener("click", () => {
                this._buttonClick(index);
            })
            buttonGroup.appendChild(button);
        });

        buttonHolder.appendChild(buttonGroup);
        buttons.appendChild(buttonHolder);

        inner.appendChild(buttons);
        wrapper.appendChild(inner);

        let diag = document.getElementById("dialog");
        this._el = diag.appendChild(wrapper);
    };

    _buttonClick = (index) => {
        this.destroy();
        this._callback(index);
    };

    destroy = () => {
        let diag = document.getElementById("dialog");
        diag.removeChild(this._el);
    }
}

const prompt = (config) => {

};

const error = (message) => {
    let wrapper = _el("div", 'growl-error');
    let iconEl = _el("i", 'fal fa-exclamation-triangle');

    wrapper.appendChild(iconEl);
    wrapper.appendChild(document.createTextNode(message));

    let growls = document.getElementById("growls");

    if (growls) {
        let _el = growls.appendChild(wrapper);

        window.setTimeout(() => {
            growls.removeChild(_el);
        }, 2000);
    }
};

const growl = (icon, message) => {
    let wrapper = _el("div", 'growl');
    let iconEl = _el("i", `fal ${icon}`);
    wrapper.appendChild(iconEl);

    wrapper.appendChild(document.createTextNode(message));

    let growls = document.getElementById("growls");

    if (growls) {
        let _el = growls.appendChild(wrapper);

        window.setTimeout(() => {
            growls.removeChild(_el);
        }, 2000);
    }
};

const notification = (config) => {

};

class BlockerItem {
    constructor(message) {
        this.message = message;
        this._el = null;

        this.show();
    }

    show = () => {
        let wrapper = _el("div", 'gui-blocker');
        let inner = _el("div", "blocker-inner", {}, this.message);

        wrapper.appendChild(inner);

        this._el = document.body.appendChild(wrapper);
    };

    update = (message) => {

    };

    rm = () => {
        document.body.removeChild(this._el);
    }
}

const blocker = (message) => {
    return new BlockerItem(message);
}


export default {
    Dialog,
    error,
    prompt,
    growl,
    notification,
    blocker
};

export {
    Dialog,
    error,
    prompt,
    growl,
    notification,
    blocker
}