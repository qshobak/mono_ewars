import Moment from "moment";

const __ = function(data) {
    return data;
};

const MONTHS = {
    "0": __("JAN"),
    "1": __("FEB"),
    "2": __("MAR"),
    "3": __("APR"),
    "4": __("MAY"),
    "5": __("JUN"),
    "6": __("JUL"),
    "7": __("AUG"),
    "8": __("SEP"),
    "9": __("OCT"),
    "10": __("NOV"),
    "11": __("DEC")
};


const getDays = (year, month, start_date, end_date) => {
    let today = new Date();

    let date = new Date(year, month, 1);

    let result = [];
    while (date.getMonth() == month) {
        if (year == start_date.year()) {
            if (date >= start_date.toDate() && date <= today) {
                let day = String(date.getDate());
                if (day.length == 1) day = `0${day}`;
                let sMonth = String(month + 1);
                if (sMonth.length == 1) sMonth = `0${sMonth}`;
                result.push(year + "-" + sMonth + "-" + day);
            }
        } else {
            if (date <= today) {
                let day = String(date.getDate());
                if (day.length == 1) day = `0${day}`;
                let sMonth = String(month + 1);
                if (sMonth.length == 1) sMonth = `0${sMonth}`;
                result.push(year + "-" + sMonth + "-" + day);
            }
        }
        date.setDate(date.getDate() + 1);
    }

    return result.reverse();

}


function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};


const getWeeks = (year, end_date) => {
    let isoWeeksInYear = Moment.utc(year).isoWeeksInYear();

    // For each week in the year, generate a list item.
    var weeks2 = [];

    for (var i = 0; i <= isoWeeksInYear; i++) {
        let dt = Moment.utc().isoWeekYear(year).isoWeek(i + 1).isoWeekday(7);
        if (dt.isBefore(Moment.utc())) {
            if (end_date) {
                if (dt.isBefore(Moment.utc(end_date))) {
                    weeks2.push(dt);
                }
            } else {
                weeks2.push(dt)
            }
        }
    }

    weeks2.reverse();

    return weeks2;
};

const getMonths = (year, start_date, end_date) => {
    let today = new Date();
    let startMonth = 0;
    let endMonth = 11;
    if (year == start_date.year()) {
        startMonth = start_date.month();
    }
    if (year == end_date.year()) {
        endMonth = end_date.month();
    }

    if (year == today.getUTCFullYear()) {
        endMonth = today.getUTCMonth();
    }

    return range(startMonth, endMonth + 1, 1);

};


export default {
    getMonths,
    getDays,
    getWeeks,
    range,
    MONTHS
}