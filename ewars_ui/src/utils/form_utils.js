'use struct';
import Moment from "moment";

let state = window.state;
import utils from "./utils";

// Used to sort form fields by their order field
const ORDER_SORT = (a, b) => {
    if (parseInt(a.order) > parseInt(b.order)) return 1;
    if (parseInt(a.order) < parseInt(b.order)) return -1;
    return 0;
};

const CMP_EVALUATION = (val, cmp, crit) => {
    return true;
};

const DISPLAY_TYPES = [
    "header"
];

const BASE_COLUMNS = [
    {
        name: "stage",
        label: "Stage",
        type: "select",
        root: true
    },
    {
        name: "submitted",
        label: "Submitted",
        type: "date",
        root: true,
        fmt: function(val) {
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm");
        }
    },
    {
        name: "modified",
        label: "Modified",
        type: "date",
        root: true,
        fmt: function(val) {
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm");
        }
    },
    {
        name: "submitter",
        label: "Submitted by",
        type: "text",
        root: true
    },
    {
        name: "modifier",
        label: "Modified by",
        type: "text",
        root: true
    },
    {
        name: "eid",
        label: "EID",
        type: "text",
        root: true
    },
];

const ALERT_BASE_COLUMNS = [
    {
        name: "location_name",
        field_type: "location",
        label: "Location",
        fmt: function (val) {
            return val.en || val;
        },
        root: true
    },
    {
        label: "Alert Date",
        name: "alert_date",
        field_type: "date",
        root: true,
        fmt: function(val) {
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
        }
    },
    {
        label: "Raised On",
        name: "raised",
        type: "date",
        root: true,
        fmt: function (val) {
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
        }
    },
    {
        label: "Status",
        name: "status",
        field_type: "select",
        options: [
            ["OPEN", "Open"],
            ["CLOSED", "Closed"]
        ],
        root: true
    },
    {
        label: "Stage",
        name: "stage",
        field_type: "select",
        options: [],
        root: true
    },
    {
        label: "Last Updated",
        name: "modified",
        field_type: "datetime",
        root: true,
        fmt: function (val) {
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:SS zz")
        }
    },
    {
        label: "Closed",
        name: "closed",
        field_type: "datetime",
        root: true,
        fmt: function (val) {
            if (!val) return "N/A";
            return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:SS zz");
        }
    }
];

const FormUtils = {};
FormUtils.ORDER_SORT = ORDER_SORT;

FormUtils.canAccessForm = (form) => {
    if (Object.keys(form.permissions).length <= 0) return true;
    if (form.permissions[state.account.role]) return true;
    return false;
};

FormUtils.flatten = (definition, parentKey) => {
    let results = [];

    for (let i in definition) {
        let root_field = definition[i];

        let name = i;
        if (parentKey) name = `${parentKey}.${i}`;

        results.push({
            ...root_field,
            name: name
        })
    }

    results = results.sort(ORDER_SORT);

    return results;
};

FormUtils.getFieldsByName = (fieldNames) => {

};

FormUtils.getFieldsByUuid = (fieldUuids) => {

};

FormUtils.getUniquesFields = (definition) => {
    let uniques = [];

    return uniques;
};

FormUtils.alert_grid = (definition) => {
    let hasMatrices = false;

    let flattened = FormUtils.flatten(definition);

    let grid = [[]];
    flattened.forEach(item => {
        if (item.type == "matrix") {
            hasMatrices = true;
            grid = [
                [],
                [],
                []
            ]
        }
    });

    flattened.forEach(item => {
        if (item.type == "matrix") {
            let rows = FormUtils.flatten(item.fields, item.name);
            // Add the parent div

            let totalSpan = 0;

            rows.forEach(row => {
                let cells = FormUtils.flatten(row.fields, row.name);
                grid[1].push({
                    name: row.name,
                    uuid: row.uuid,
                    label: row.label.en || row.label,
                    span: cells.length,
                    type: "colspan"
                })

                totalSpan += cells.length;

                cells.forEach(cell => {
                    grid[2].push(cell);
                })
            })

            grid[0].push({
                name: item.name,
                uuid: item.uuid,
                label: item.label.en || item.label,
                span: totalSpan,
                type: "colspan"
            });

        } else {
            if (hasMatrices) {
                grid[0].push("FILL");
                grid[1].push("FILL");

                grid[2].push({
                    ...item
                });
            } else {
                grid[0].push({
                    ...item
                })
            }
        }
    });

    let endGrid = [[]];
    if (hasMatrices) {
        endGrid = [[], [], []];
        ALERT_BASE_COLUMNS.forEach(item => {
            endGrid[0].push("FILL");
            endGrid[1].push("FILL");
            endGrid[2].push(item);
        });
        endGrid[0] = endGrid[0].concat(grid[0]);
        endGrid[1] = endGrid[1].concat(grid[1]);
        endGrid[2] = endGrid[2].concat(grid[2]);
    } else {
        endGrid[0] = ALERT_BASE_COLUMNS;
        endGrid[0] = endGrid[0].concat(grid[0]);
    }

    return endGrid;
}

FormUtils.grid = (hasReportDate, hasLocation, definition) => {
    let hasMatrices = false;

    let flattened = FormUtils.flatten(definition);

    console.log(flattened);

    let grid = [[]];
    flattened.forEach(item => {
        if (item.type == "matrix") {
            hasMatrices = true;
            grid = [
                [],
                [],
                []
            ]
        }
    });

    flattened.forEach(item => {
        if (item.type == "matrix") {
            let rows = FormUtils.flatten(item.fields, item.name);
            // Add the parent div

            let totalSpan = 0;

            rows.forEach(row => {
                let cells = FormUtils.flatten(row.fields, row.name);
                grid[1].push({
                    name: row.name,
                    uuid: row.uuid,
                    label: row.label.en || row.label,
                    span: cells.length,
                    type: "colspan"
                })

                totalSpan += cells.length;

                cells.forEach(cell => {
                    grid[2].push({
                        ...cell,
                        rootProp: "data",
                        root: false
                    });
                })
            });

            let fmt;

            if (item.type == "location") {
                fmt = function(val, row) {
                    console.log(val, row);
                }
            }

            grid[0].push({
                name: item.name,
                uuid: item.uuid,
                label: item.label.en || item.label,
                span: totalSpan,
                type: "colspan",
                fmt: fmt
            });

        } else {
            if (DISPLAY_TYPES.indexOf(item.type) < 0) {
                if (hasMatrices) {
                    grid[0].push("FILL");
                    grid[1].push("FILL");

                    if (item.type == "location") {
                        // Need to add the mapping for the location name
                        item.fmt = function (val, cell, row) {
                            let alias = `l_${cell.name}`;
                            return row.metadata[alias] || val;
                        }
                    }

                    if (item.type == "date") {
                        item.fmt = function (val) {
                            return Moment.utc(val).local().format("YYYY-MM-DD");
                        }
                    }

                    grid[2].push({
                        ...item,
                        rootProp: "data",
                        root: false

                    });
                } else {
                    grid[0].push({
                        ...item,
                        rootProp: "data",
                        root: false
                    })
                }
            }
        }
    });

    let endGrid = [[]];
    if (hasMatrices) {
        endGrid = [[], [], []];
        BASE_COLUMNS.forEach(item => {
            endGrid[0].push("FILL");
            endGrid[1].push("FILL");
            endGrid[2].push(item);
        });
        endGrid[0] = endGrid[0].concat(grid[0]);
        endGrid[1] = endGrid[1].concat(grid[1]);
        endGrid[2] = endGrid[2].concat(grid[2]);
    } else {
        endGrid[0] = BASE_COLUMNS;
        endGrid[0] = endGrid[0].concat(grid[0]);
    }

    return endGrid;
};


/**
 * Evaluate a rule against a set of data
 * @param rule
 * @param data
 * @returns {boolean}
 */
const _getRuleResult = (rule, data) => {
    let result,
        sourceValue;

    if (rule[0].indexOf("data.") == 0) {
        sourceValue = data.data[rule[0].replace("data.", "")] || null;
    } else {
        sourceValue = data[rule[0]];
    }
    if (sourceValue == null || sourceValue == undefined) return false;

    const isNumber = (val) => {
        try {
            parseFloat(val);
            return true;
        } catch (e) {
            return false;
        }
    };

    switch (rule[1]) {
        case "eq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) >= 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue == rule[2];
            }
            break;
        case "ne":
        case "neq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) < 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue != rule[2];
            }
            break;
        case "gt":
            if (!isNumber(sourceValue)) {
                result = false;
            } else {
                result = parseFloat(sourceValue) > parseFloat(rule[2]);
            }
            break;
        case "lt":
            if (!isNumber(sourceValue)) {
                result = false;
            } else {
                result = parseFloat(sourceValue) < parseFloat(rule[2]);
            }
            break;
        case 'gte':
            if (!isNumber(sourceValue)) {
                result = false;
            } else {
                result = parseFloat(sourceValue) >= parseFloat(rule[2]);
            }
            break;
        case 'lte':
            if (!isNumber(sourceValue)) {
                result = false;
            } else {
                result = parseFloat(sourceValue) <= parseFloat(rule[2]);
            }
            break;
        default:
        case 'd_gte':
        case 'l_gt':
        case 'l_lte':
        case 'l_lt':
            result = false;
            break
    }

    return result;
}

FormUtils.HEADER_FILTER = (field) => {
    return field.field_type == "header";
};

FormUtils.isFieldVisibleLegacy = (field, data, stage) => {
    let compareData = utils.copy(data);
    if (stage) {
        compareData = {
            ...compareData
        };
        compareData._stage = stage;
    }

    if (field.conditions != null && field.conditions.length > 0) {
        let result = false;

        let ruleCount = field.conditions.length;
        let rulePassCount = 0;

        field.conditions.forEach(rule => {
            if (rule[0] === "OR" || rule[0] === "AND") {

            } else {
                let ruleResult = _getRuleResult(rule, compareData);
                if (ruleResult) rulePassCount++;
            }
        });

        if (ruleCount === rulePassCount) result = true;

        return result;
    }

    return true;
};

FormUtils.hasLocation = (features) => {
    return features.LOCATION_REPORTING != null;
};

FormUtils.hasDataDate = (features) => {
    return features.INTERVAL_REPORTING != null;
};

const isUndefined = (val) => {
    if (val == undefined) return true;
    if (val == null) return true;
    if (val == "") return true;
    return false;
};


const NON_VALIDATION_TYPES = ['header', 'display', 'matrix', 'row', 'group'];
FormUtils.validate = (form, data) => {
    let errors = {};

    let definition = FormUtils.flatten(form.definition);

    definition.forEach(item => {
        if (NON_VALIDATION_TYPES.indexOf(item.field_type) < 0) {
            if (FormUtils.isFieldVisibleLegacy(item, data)) {
                if (item.required && isUndefined(data.data[item.name])) {
                    errors[item.name] = "NONE";
                }
            }
        } else if (item.field_type == 'matrix') {
            if (FormUtils.isFieldVisibleLegacy(item, data)) {
                let rows = FormUtils.flatten(item.fields, item.name);
                rows.forEach(row => {
                    if (FormUtils.isFieldVisibleLegacy(row, data)) {
                        let cells = FormUtils.flatten(row.fields, row.name);

                        cells.forEach(cell => {
                            if (FormUtils.isFieldVisibleLegacy(cell, data)) {
                                if (cell.required && isUndefined(data.data[cell.name])) {
                                    errors[cell.name] = "NONE";
                                }
                            }
                        })
                    }
                })
            }
        } else if (item.field_type == "group") {

        } else if (item.field_type == "stage") {

        }
    })

    return errors;

};


FormUtils._iterateChildren = (childs, fn, basePath, parent) => {
    if (!basePath) basePath = "";
    for (var i in childs) {
        var path = basePath + "." + i;
        var child = childs[i];
        fn(i, child, childs, path.slice(1, path.length), parent);

        if (child.fields) {
            _iterateChildren(child.fields, fn, path, child);
        }
    }
};

FormUtils.deleteField = (uuid, sourceData) => {
    let field;

    FormUtils._iterateChildren(sourceData, function (name, child, items) {
        if (child.uuid == uuid) {
            let newField = JSON.parse(JSON.stringify(items[name]));
            let uuid = utils.uuid();
            let newName = "NEW_" + utils.uuid().split("-")[0];
            newField.uuid = uuid;
            newField.label = newField.label || {en: ""};
            newField.order = 99999999;
            items[newName] = newField;

            // Iterate nested cells
            if (newField.type == "row") {
                for (var i in newField.fields) {
                    newField.fields[i].uuid = utils.uuid();
                }
            }

            // iterate matrix children down to cells and update uuids
            if (newField.type == "matrix") {
                for (var n in newField.fields) {
                    let subField = newField.fields[n];
                    newField.fields[n].uuid = utils.uuid();

                    for (var t in subField.fields) {
                        subField.fields[t].uuid = utils.uuid();
                    }
                }
            }


            var arrayed = FormUtils.getFieldsAsArray(items);
            arrayed.sort(ORDER_SORT);
            items = FormUtils.getFieldsAsObject(arrayed);
        }
    })
}

FormUtils.findParentNode = (parentName, childObj) => {
    var testObj = childObj.parentNode;
    var count = 1;
    while (testObj.getAttribute('name') != parentName) {
        alert("My name is " + testObj.getAttribute("name") + ". Lets try moving up one level");
        testObj = testObj.parentNode;
        count++;
    }
};

FormUtils.normalize = (sourceFields) => {
    sourceFields.forEach(field => {
        if (!field.uuid) field.uuid = utils.uuid();
        if (field.fields) _normalize(field.fields);
    });
};

FormUtils.getFieldsAsArrayShallow = (sourceFields, parentId) => {
    var fields = [];
    for (let token in sourceFields) {
        let field = sourceFields[token];
        let fieldCopy = JSON.parse(JSON.stringify(field));
        fieldCopy.name = token;
        fieldCopy.parentId = parentId;
        if (!fieldCopy.uuid) fieldCopy.uuid = utils.uuid();

        fields.push(fieldCopy);
    }

    return fields.sort(ORDER_SORT);
}

FormUtils.getFieldsAsArray = (sourceFields, parentId) => {
    var fields = [];
    for (let token in sourceFields) {
        let field = sourceFields[token];
        let fieldCopy = JSON.parse(JSON.stringify(field));
        fieldCopy.name = token;
        fieldCopy.parentId = parentId;
        if (!fieldCopy.uuid) fieldCopy.uuid = utils.uuid();

        if (fieldCopy.fields) {
            var children = FormUtils.getFieldsAsArray(fieldCopy.fields, fieldCopy.uuid);
            fields.push.apply(fields, children);
        }

        delete fieldCopy.fields;

        fields.push(fieldCopy);
    }

    return fields.sort(ORDER_SORT);
};

FormUtils.getFieldsAsObject = (sourceArray) => {
    var fieldDict = {};
    var resultDict = {};

    sourceArray.forEach(item => {
        fieldDict[item.uuid] = item;
    });

    for (let i in fieldDict) {
        // find the parent of this item, id there is any
        let item = fieldDict[i];
        let parent = fieldDict[item.parentId];
        // if there's a parent, then we need to map this field into the parent
        if (parent) {
            // If the parent doesn't have
            if (!fieldDict[item.parentId].fields) fieldDict[item.parentId].fields = {};
            fieldDict[item.parentId].fields[item.name] = item;
        }
    }

    for (let i in fieldDict) {
        if (fieldDict[i].parentId == null) resultDict[fieldDict[i].name] = fieldDict[i];
    }

    return resultDict;
};

FormUtils.resort = (parentId, sourceData) => {
    var toSort = [];
    var fields = FormUtils.getFieldsAsArray(sourceData);

    var fieldDict = {};
    fields.forEach(item => {
        fieldDict[item.uuid] = item;
    });

    fields.forEach(item => {
        if (item.parentId == parentId) toSort.push(item);
    });

    var sortedResult = toSort.sort(ORDER_SORT);

    var count = 0;
    sortedResult.forEach(item => {
        item.order = count;
        fieldDict[item.uuid] = item;
        count++;
    });

    var result = FormUtils.getFieldsAsObject(_.map(fieldDict, function (item) {
        return item;
    }, this));

    return result;
};

FormUtils.fixNumbering = (items) => {
    var asArray = FormUtils.getFieldsAsArray(items);

    let groups = {};
    asArray.forEach(item => {
        if (groups[item.parentId]) {
            groups[item.parentId].push(item);
        } else {
            groups[item.parentId] = [item]
        }
    });

    let grouped = [];

    for (let i in groups) {
        grouped.push(groups[i]);
    }
    let result = [];

    grouped.forEach(group => {
        group = group.sort(ORDER_SORT);
        let count = 0;

        group.each(item => {
            item.order = count;
            result.push(item);
            count++;
        })
    });

    return FormUtils.getFieldsAsObject(result)
};

const NON_INPUT_TYPES = ['matrix', 'row', 'table', 'repeater', 'associated'];

FormUtils.asOptions = (definition, path, lLabel) => {
    var options = [];

    for (let key in utils.copy(definition)) {
        let field = definition[key];
        let fieldName = field.label.en || field.label;
        if (path) fieldName += ".${path}";
        let realName = fieldName;
        if (lLabel) realName = `${lLabel.join(" \\ ")} \\ ${fieldName}`;
        if (NON_INPUT_TYPES.indexOf(field.type) < 0) {
            options.push([key, realName, field]);
        }

        if (field.type == 'matrix') {
            for (let row in field.fields) {
                let rowField = field.fields[row];

                for (let cell in rowField.fields) {
                    let cellField = rowField.fields[cell];

                    options.push([
                        `${key}.${row}.${cell}`,
                        `${fieldName} \ ${rowField.label.en || rowField.label} \ ${cellField.label.en || cellField.label}`,
                        cellField
                    ])
                }
            }
        }
    }

    return options;
};

FormUtils._convertFormToTree = (definition, place, placeName) => {
    var items = [];

    let asArray = FormUtils.getFieldsAsArray(definition);

    asArray = asArray.sort(ORDER_SORT);

    asArray.forEach(field => {
        if (field.fields) {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = field.label.en || field.label;
            if (placeName) pathName = placeName + " / " + (field.label.en || field.label);

            var children = FormUtils._convertFormToTree(field.fields, path, pathName);

            items.push({
                label: field.label.en || field.label,
                pathLabel: pathName,
                name: field.name,
                path: path,
                field: field,
                nodeType: field.type,
                children: children
            })
        } else {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = field.label.en || field.label;
            if (placeName) pathName = placeName + ' / ' + (field.label.en || field.label);

            items.push({
                label: field.label.en || field.label,
                name: field.name,
                pathLabel: pathName,
                path: path,
                field: field,
                nodeType: field.type
            })
        }
    }, this);

    return items;
};

FormUtils.asTree = (form) => {
    let formItems;
    if (form.version) formItems = FormUtils._convertFormToTree(form.version.definition, null, "Form");
    if (!form.version) formItems = FormUtils._convertFormToTree(form, null, "Form");

    if (form.location_aware) {
        formItems.unshift({
            label: "Report Location",
            field: {
                type: "location",
                required: true
            },
            nodeType: "location",
            type: "location",
            name: "location_id",
            path: "form/location_id",
            pathLabel: "Form / Location"
        })
    }

    formItems.unshift({
        label: "Report Date",
        type: "date",
        field: {
            type: "date",
            required: true
        },
        nodeType: "date",
        name: "data_date",
        path: "form/data_date",
        pathLabel: "Form / Report Date"
    });

    return [{
        nodeType: "form",
        label: "Form",
        name: "form",
        field: {
            type: "form"
        },
        type: "form",
        children: formItems
    }];
}


FormUtils.getField = (definition, specPath) => {
    let item = definition;

    let path = specPath.split(".");
    if (path.indexOf("data.") >= 0) path.shift();

    path.forEach(function (pathItem) {
        if (item[pathItem]) {
            if (item[pathItem].fields) {
                item = item[pathItem].fields;
            } else {
                item = item[pathItem];
            }
        }
    }.bind(this));

    return item;
};

FormUtils.getFieldProperty = (definition, field, prop) => {
    if (field.indexOf(".") >= 0) {
        let splat = field.split(".");

        let defn = definition;
        let splatLen = splat.length;
        for (var i in splat) {
            if (defn[i]) {
                if (defn[i].fields) {
                    defn = defn[i].fields;
                } else {
                    defn = defn[i];
                }
            } else {

            }
        }
        return defn[prop];
    } else {
        return definition[field][prop] || null;
    }
};

FormUtils.getRuleResult = (rule, data) => {
    var result;

    let sourceValue = data[rule[0]] || null;
    if (sourceValue == null || sourceValue == undefined) return false;

    switch (rule[1]) {
        case "eq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) >= 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue == rule[2];
            }
            break;
        case "ne":
        case "neq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) < 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue != rule[2];
            }
            break;
        case "gt":
            result = sourceValue > rule[2];
            break;
        case "lt":
            result = sourceValue < rule[2];
            break;
        case 'lte':
            result = sourceValue <= rule[2];
            break;
        case 'gte':
            result = sourceValue >= rule[2];
            break;
        default:
            result = false;
            break
    }

    return result;
};

FormUtils.isFieldVisible = (data, field, fieldKey) => {
    if (!field.conditional_bool) return true;

    if (field.conditional_bool == true) {
        let result = false;

        if (!field.conditions) return true;
        if (!field.conditions.rules) return true;

        if (["ANY", "any"].indexOf(field.conditions.application) >= 0) {
            // Only one of the rules has to pass

            for (let conIdx in field.conditions.rules) {
                let rule = field.conditions.rules[conIdx];

                var tmpResult = FormUtils.getRuleResult(rule, data);
                if (result != true && tmpResult == true) result = true;
            }

        } else {
            let ruleCount = field.conditions.rules.length;
            let rulePassCount = 0;

            for (let ruleIdx in field.conditions.rules) {
                let rule = field.conditions.rules[ruleIdx];
                let ruleResult = FormUtils.getRuleResult(rule, data);
                if (ruleResult) rulePassCount++;
            }

            if (ruleCount == rulePassCount) result = true;

        }
        return result;
    } else {
        return true;
    }

}

FormUtils.isSet = (value) => {
    if (value == null) return false;
    if (value == undefined) return false;
    if (value == "") return false;
    if (value == "null") return false;
    if (value == "NULL") return false;
    if (value == "None") return false;

    return true
}

const NON_VALIDATE = ['matrix', 'row', 'header', 'display'];

FormUtils.validateLegacy = (definition, data, flattened) => {
    let errors = {};
    let hasErrors = false;

    for (var i in definition) {
        let field = definition[i];

        let value;
        if (flattened) {
            value = data[i] || null;
        } else {
            value = data[i] || null;
        }

        errors[i] = [];

        let dotCount = (i.match(/\./g) || []).length;

        let isVisible = true;
        if (dotCount > 0 && NON_VALIDATE.indexOf(field.type) < 0) {
            // This is a non-root field, we need to evaluate whether it's parents are
            // visible before evaluating whether the actual field is
            let key;
            let fieldTree = [];
            i.split(".").forEach(item => {
                if (!key) {
                    key = item;
                } else {
                    key += "." + item;
                }

                fieldTree.push(key);
            })
            fieldTree.pop();

            fieldTree.forEach(item => {
                let _vis = FormUtils.isFieldVisibleLegacy(data, definition[item], item);
                if (!_vis) isVisible = false;
            })
        } else {
            isVisible = FormUtils.isFieldVisibleLegacy(data, field, i);
        }

        if (isVisible && NON_VALIDATE.indexOf(field.type) < 0) {
            if (field.required && NON_VALIDATE.indexOf(field.type) < 0) {
                if (!FormUtils.isSet(value)) {
                    hasErrors = true;
                    errors[i].push(field.label.en || field.label + " is a required field");
                } else {
                    let typeErrors = [];
                    switch (field.type) {
                        case "number":
                            typeErrors = FormUtils.validateNumber(field, value);
                            break;
                        case "text":
                            typeErrors = FormUtils.validateText(field, value);
                            break;
                        case "select":
                            typeErrors = FormUtils.validateSelect(field, value);
                            break;
                        case "date":
                            typeErrors = FormUtils.validateDate(field, value);
                            break;
                        case "location":
                            typeErrors = FormUtils.validateLocation(field, value);
                            break;
                        default:
                            break;
                    }

                    if (typeErrors.length > 0) hasErrors = true;
                    errors[i].push.apply(errors[i], typeErrors);
                }
            }
        }
    }

    for (var f in errors) {
        if (errors[f].length <= 0) delete errors[f];
    }

    return {hasErrors, errors};
};


const DIMENSIONS = ['text', 'textarea', 'select', 'date', 'datetime', 'time', 'period', 'location', 'lat_lng', 'barcode', 'risk', 'location_type'];
const MEASURES = ['number', 'numeric'];
/**
 * Get fields within a form definition as a tree list for display in a tree
 * @param definition
 * @param parentName
 * @param parentLabel
 * @returns {Array}
 */
FormUtils.getOutputs = (definition, parentName, parentLabel, filter) => {
    let results = [];

    return results;
};

FormUtils.moveField = (definition, moving, target) => {
    let fields = FormUtils.getFieldsAsArray(definition);
    let movingIndex,
        targetIndex;

    fields.forEach((item, index) => {
        if (item.uuid == moving) {
            movingIndex = index;
        }
        if (item.uuid == target) {
            targetIndex = index;
        }
    });

    let movingItem = fields.filter(item => {
        return item.uuid == moving;
    })[0] || null;

    fields = fields.sort(_comparator);

    let result = [];
    fields.forEach(item => {
        if (item.uuid != moving) {
            if (item.uuid == target) {
                result.push(item);
                result.push(movingItem);
            } else {
                result.push(item);
            }
        }
    })

    let counter = 0;
    result.forEach(item => {
        item.order = counter;
        counter++;
    })

    return FormUtils.getFieldsAsObject(result);
};


FormUtils.updateField = (definition, fieldId, prop, value) => {
    let arr = FormUtils.getFieldsAsArray(definition);

    arr.forEach(field => {
        if (field.uuid == fieldId) field[prop] = value;
    });

    let res = FormUtils.getFieldsAsObject(arr);

    return FormUtils.getFieldsAsObject(arr);
};

FormUtils.removeField = (definition, uuid) => {
    let arr = FormUtils.getFieldsAsArray(definition);

    let rIndex;

    arr.forEach((item, index) => {
        if (item.uuid == uuid) rIndex = index;
    });

    arr.splice(rIndex, 1);

    return FormUtils.getFieldsAsObject(arr);
};

FormUtils.duplicateField = (definition, uuid) => {
    let field;

    FormUtils._iterateChildren(definition, function (name, child, items) {
        if (child.uuid == uuid) {
            let newField = utils.copy(items[name]);
            let uuid = utils.uuid();
            let newName = "NEW_" + utils.uuid().split("-")[0];
            newField.uuid = uuid;
            newField.label = newField.label || {en: ""};
            newField.order = 99999999;
            items[newName] = newField;

            // Iterate nested cells
            if (newField.type == "row") {
                for (var i in newField.fields) {
                    newField.fields[i].uuid = utils.uuid();
                }
            }

            // iterate matrix children down to cells and update uuids
            if (newField.type == "matrix") {
                for (var n in newField.fields) {
                    let subField = newField.fields[n];
                    newField.fields[n].uuid = utils.uuid();

                    for (var t in subField.fields) {
                        subField.fields[t].uuid = utils.uuid();
                    }
                }
            }


            var arrayed = FormUtils.getFieldsAsArray(items);
            arrayed.sort(ORDER_SORT);
            items = FormUtils.getFieldsAsObject(arrayed);
        }
    })
};


FormUtils.moveFieldUp = (definition, uuid) => {
    let fields = FormUtils.getFieldsAsArray(definition);
    let targetField = fields.filter(item => {
        return item.uuid == uuid;
    })[0] || null;

    let groups = {};
    fields.forEach(item => {
        if (!groups[item.parentId]) {
            groups[item.parentId] = [item];
        } else {
            groups[item.parentId].push(item);
        }
    });
    let targetGroup = groups[targetField.parentId];
    targetGroup.sort(ORDER_SORT);

    let curIndex = null;
    targetGroup.forEach((item, index) => {
        if (item.uuid == uuid) curIndex = index;
    });

    if (curIndex <= 0) return definition;

    let targetIndex = curIndex - 1;
    let tmp = targetGroup[targetIndex];
    targetGroup[targetIndex] = targetField;
    targetGroup[curIndex] = tmp;

    targetGroup.forEach((item, index) => {
        item.order = index;

        fields.forEach(field => {
            if (field.uuid == item.uuid) field.order = index;
        });
    });

    return FormUtils.getFieldsAsObject(fields);
};

FormUtils.moveFieldDown = (definition, uuid) => {
    let fields = FormUtils.getFieldsAsArray(definition);
    let targetField = fields.filter(item => {
        return item.uuid == field;
    })[0] || null;

    let groups = {};
    fields.forEach(item => {
        if (!groups[item.parentId]) {
            groups[item.parentId] = [item];
        } else {
            groups[item.parentId].push(item);
        }
    });
    let targetGroup = groups[targetField.parentId];
    targetGroup.sort(ORDER_SORT);

    let curIndex = null;
    targetGroup.forEac((item, index) => {
        if (item.uuid == field) curIndex = index;
    });

    if (curIndex >= targetGroup.length - 1) return definition;

    let targetIndex = curIndex + 1;
    let tmp = targetGroup[targetIndex];

    targetGroup[targetIndex] = targetField;
    targetGroup[curIndex] = tmp;

    targetGroup.forEach((item, index) => {
        item.order = index;

        fields.forEach(field => {
            if (field.uuid == item.uuid) field.order = index;
        })
    });

    FormUtils.getFieldsAsObject(fields);
};

FormUtils.dropField = (definition, field, target) => {
    let fields = FormUtils.getFieldsAsArray(definition || {});

    fields.sort(ORDER_SORT);

    field.order = 0;
    let _s = field.uuid.split("-");
    field.name = "untitled_" + _s[_s.length - 1];

    let result = [];

    if (target) {
        fields.forEach((item, index) => {
            result.push(item);
            if (item.uuid == target) {
                result.push(field);
            }
        });
    } else {
        result = fields;
        result.push(field);
    }

    let counter = 0;
    result.forEach(item => {
        item.order = counter;
        counter++;
    });

    return FormUtils.getFieldsAsObject(result);
};

/**
 * Upgrade a form from version 1 to version 2 definition with workflow
 * @param definition
 * @returns {{workflow: Array, definition: {}}}
 */
FormUtils.upgradeForm = (definition) => {
    return definition;
};

export default FormUtils;


