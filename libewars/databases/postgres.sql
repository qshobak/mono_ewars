CREATE EXTENSION "uuid-ossp";

CREATE SCHEMA __SCHEMA__;
SET search_path TO __SCHEMA__, public;


CREATE TABLE IF NOT EXISTS alarms (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  name        TEXT                     NOT NULL DEFAULT 'New alarm',
  status      TEXT                     NOT NULL DEFAULT 'INACTIVE',
  description TEXT                     NOT NULL DEFAULT 'New alarm',
  version     INT                      NOT NULL DEFAULT 2,

  definition  JSONB                    NOT NULL DEFAULT '{}',
  workflow    JSONB                    NOT NULL DEFAULT '{}',
  permissions JSONB                     NOT NULL DEFAULT '{}',
  guidance TEXT NULL,

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);

CREATE TABLE IF NOT EXISTS local_users (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    status TEXT NOT NULL DEFAULT 'PENDING_VERIFICATION',
    system BOOL NOT NULL DEFAULT FALSE,

    registered TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    verified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    approved TIMESTAMP WITH TIME ZONE NULL,
    
    created_by UUID NULL,
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

CREATE TABLE IF NOT EXISTS gateways (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    description TEXT NULL,
    status TEXT NOT NULL DEFAULT 'INACTIVE'
);

CREATE TABLE IF NOT EXISTS users (
  ruuid       UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  uuid         UUID REFERENCES core.users (uuid)                      NOT NULL,
  role        TEXT                     NOT NULL DEFAULT 'USER',
  status      TEXT                     NOT NULL DEFAULT 'PENDING',
  permissions JSONB                    NOT NULL DEFAULT '{}',
  settings    JSONB                    NOT NULL DEFAULT '{}',
  profile     JSONB                    NOT NULL DEFAULT '{}',
  org_id      UUID                     NULL,
  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);

CREATE TABLE conf (
  uuid        UUID PRIMARY KEY            NOT NULL DEFAULT uuid_generate_v4(),
  key         TEXT                        NOT NULL,
  value       JSONB                       NOT NULL DEFAULT '{}',
  grp         TEXT                        NULL,

  created     TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT NOW(),
  created_by  UUID                        NULL,
  modified    TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT NOW(),
  modified_by UUID                        NULL
);

CREATE TABLE alerts (
  uuid        UUID PRIMARY KEY                            NOT NULL DEFAULT uuid_generate_v4(),
  alid        UUID REFERENCES alarms (uuid)               NOT NULL,
  status      TEXT                                        NOT NULL DEFAULT 'OPEN',
  eid         TEXT                                        NULL,
  alert_date DATE NOT NULL DEFAULT NOW(),
  analysed_data JSONB NULL,

  lid         UUID                                        NULL,
  stage TEXT NULL,

  data        JSONB                                       NOT NULL DEFAULT '{}',
  workflow    JSONB                                       NOT NULL DEFAULT '{}',
  events      JSONB                                       NOT NULL DEFAULT '[]',

  raised      TIMESTAMP WITH TIME ZONE                    NOT NULL DEFAULT NOW(),
  closed      TIMESTAMP WITH TIME ZONE                    NULL,
  modified    TIMESTAMP WITH TIME ZONE                    NOT NULL DEFAULT NOW(),
  modified_by UUID                                        NULL
);
CREATE INDEX idx_alert_lid ON alerts (lid);
CREATE INDEX idx_alert_alid ON alerts (alid);

CREATE TABLE forms (
  uuid           UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  id             SERIAL                   NOT NULL,
  name           JSONB                    NOT NULL DEFAULT '{}',
  version        INT                      NOT NULL DEFAULT 1,
  status         TEXT                     NOT NULL DEFAULT 'DRAFT',
  description    TEXT                     NOT NULL,
  guidance       TEXT                     NULL,
  eid_prefix     TEXT                     NULL,

  features       JSONB                    NULL,
  definition     JSONB                    NOT NULL DEFAULT '{}',
  constraints    JSONB                    NOT NULL DEFAULT '{}',
  stages         JSONB                    NOT NULL DEFAULT '[]',
  etl            JSONB                    NOT NULL DEFAULT '{}',

  changes        JSONB                    NOT NULL DEFAULT '[]',
  permissions    JSONB                    NOT NULL DEFAULT '{}',

  created        TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by     UUID                     NULL,
  modified       TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by    UUID                     NULL

);

CREATE TABLE location_types (
  uuid           UUID PRIMARY KEY       NOT NULL DEFAULT uuid_generate_v4(),
  name         JSONB                    NOT NULL DEFAULT '{}',
  description  TEXT                     NULL,
  restrictions JSONB                    NULL,

  created      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by   UUID                     NULL,
  modified     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by  UUID                     NULL
);

CREATE TABLE resources (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL DEFAULT 'Untitled',
    status TEXT NOT NULL DEFAULT 'ACTIVE',
    version INT NOT NULL DEFAULT 2,
    description TEXT NULL,
    content_type TEXT NOT NULL DEFAULT 'UNKNOWN',
    shared BOOL NOT NULL DEFAULT FALSE,
    layout JSONB NOT NULL DEFAULT '[]',
    data JSONB NOT NULL DEFAULT '{}',
    style JSONB NOT NULL DEFAULT '{}',
    variables JSONB NOT NULL DEFAULT '[]',
    permissions JSONB NOT NULL DEFAULT '{}',
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

CREATE TABLE locations (
  uuid          UUID PRIMARY KEY                 NOT NULL DEFAULT uuid_generate_v4(),
  name          JSONB                            NOT NULL DEFAULT '{}',
  status        TEXT                             NOT NULL DEFAULT 'INACTIVE',
  pcode         TEXT UNIQUE                      NOT NULL,
  codes         JSONB                            NOT NULL DEFAULT '{}',
  groups        TEXT []                          NULL,
  data          JSONB                            NOT NULL DEFAULT '{}',
  lti           UUID REFERENCES location_types (uuid) NOT NULL,
  pid           UUID REFERENCES locations (uuid) NULL,
  lineage       UUID []                          NULL,
  organizations UUID[] NULL,

  geometry_type TEXT                             NOT NULL DEFAULT 'POINT',
  geojson       JSONB                            NULL,
  population    JSONB                            NOT NULL DEFAULT '{}',

  created       TIMESTAMP WITH TIME ZONE         NOT NULL DEFAULT NOW(),
  created_by    UUID                             NULL,
  modified      TIMESTAMP WITH TIME ZONE         NOT NULL DEFAULT NOW(),
  modified_by   UUID                             NULL
);
CREATE INDEX idx_loc_type ON locations (lti);

CREATE TABLE organizations (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  name        JSONB                    NOT NULL DEFAULT '{}',
  description TEXT                     NULL,
  acronym     TEXT                     NULL,

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);


CREATE TABLE documents (
  uuid          UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  template_name JSON                     NOT NULL DEFAULT '{}',
  instance_name JSON                     NOT NULL DEFAULT '{}',
  status        TEXT                     NOT NULL DEFAULT 'DRAFT',
  version       INT                      NOT NULL DEFAULT 1,
  theme TEXT NULL,

  layout        JSONB                    NOT NULL DEFAULT '[]',
  content       TEXT                     NULL,
  data          JSONB                    NOT NULL DEFAULT '{}',
  generation    JSONB                    NOT NULL DEFAULT '{}',
  orientation   TEXT                     NOT NULL DEFAULT 'PORTRAIT',
  template_type TEXT                     NOT NULL DEFAULT 'GENERATED',

  permissions   JSONB                    NOT NULL DEFAULT '{}',

  created       TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by    UUID                     NULL,
  modified      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by   UUID                     NULL
);

CREATE TABLE tasks (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  status      TEXT                     NOT NULL DEFAULT 'OPEN',
  task_type   TEXT                     NOT NULL DEFAULT 'UNKNOWN',

  data        JSONB                    NOT NULL DEFAULT '{}',
  actions     JSONB                    NOT NULL DEFAULT '[]',
  version     INTEGER                  NOT NULL DEFAULT 2,
  form        JSONB                    NOT NULL DEFAULT '{}',
  context JSONB NOT NULL DEFAULT '{}',
  roles TEXT[] NULL,
  location UUID NULL,
  outcome JSONB NOT NULL DEFAULT '{}',

  actioned    TIMESTAMP WITH TIME ZONE NULL,
  actioned_by UUID                     NULL,
  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);
CREATE INDEX idx_task_data ON tasks USING gin (context);

CREATE TABLE teams (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  name        TEXT                     NOT NULL DEFAULT 'New team',
  description TEXT                     NULL,
  private     BOOL                     NOT NULL DEFAULT FALSE,
  status      TEXT                     NOT NULL DEFAULT 'ACTIVE',

  permissions JSONB                    NOT NULL DEFAULT '{}',
  members     JSONB                    NULL,

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);

CREATE TABLE sites (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL DEFAULT 'New Site',
    description TEXT NOT NULL DEFAULT 'A new web site',
    status TEXT NOT NULL DEFAULT 'INACTIVE',
    auth TEXT NOT NULL DEFAULT 'PUBLIC',
    users JSONB NOT NULL DEFAULT '[]',

    definition JSONB NOT NULL DEFAULT '{}',
    cname TEXT NULL,

    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

CREATE TABLE team_messages (
  uuid        UUID PRIMARY KEY                     NOT NULL DEFAULT uuid_generate_v4(),
  ts_ms       INT                                  NOT NULL,
  tid         UUID REFERENCES teams (uuid)         NOT NULL,
  content     JSONB                                NOT NULL DEFAULT '{}',
  pid         UUID REFERENCES team_messages (uuid) NULL,

  created     TIMESTAMP WITH TIME ZONE             NOT NULL DEFAULT NOW(),
  created_by  UUID                                 NULL,
  modified    TIMESTAMP WITH TIME ZONE             NOT NULL DEFAULT NOW(),
  modified_by UUID                                 NULL
);
CREATE INDEX idx_tm_tid ON team_messages (tid);

CREATE TABLE invites (
  uuid    UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  email   TEXT                     NOT NULL,
  user_id UUID REFERENCES core.users (uuid) NOT NULL,
  details JSONB                    NOT NULL DEFAULT '{}',
  send    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  expires TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE indicator_groups (
  uuid        UUID PRIMARY KEY                        NOT NULL DEFAULT uuid_generate_v4(),
  name        TEXT                                    NOT NULL,
  description TEXT                                    NULL,
  pid         UUID  NULL,

  permissions JSONB                                   NULL,
  created     TIMESTAMP WITH TIME ZONE                NOT NULL DEFAULT NOW(),
  created_by  UUID                                    NULL,
  modified    TIMESTAMP WITH TIME ZONE                NOT NULL DEFAULT NOW(),
  modified_by UUID                                    NULL
);

CREATE TABLE indicators (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  group_id    UUID                     NULL,
  name        TEXT                     NOT NULL DEFAULT 'New indicator',
  status      TEXT                     NOT NULL DEFAULT 'INACTIVE',
  description TEXT                     NULL,

  itype       TEXT                     NOT NULL DEFAULT 'DEFAULT',
  icode       TEXT                     NOT NULL,

  permissions JSONB                    NULL,
  definition JSONB NOT NULL DEFAULT '{}',

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);

CREATE TABLE import_projects (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  name        TEXT                     NOT NULL,
  description TEXT                     NULL,
  status      TEXT                     NOT NULL DEFAULT 'PENDING',
  form_id     UUID                     NOT NULL,
  mapping     JSONB                    NOT NULL DEFAULT '{}',
  meta_map    JSONB                    NOT NULL DEFAULT '{}',
  src_file    TEXT                     NOT NULL,

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);
CREATE INDEX idx_prj_fid ON import_projects (form_id);

CREATE TABLE import_project_data (
  uuid        UUID PRIMARY KEY                       NOT NULL DEFAULT uuid_generate_v4(),
  project_id  UUID REFERENCES import_projects (uuid) NOT NULL,
  status      TEXT                                   NOT NULL DEFAULT 'PENDING',
  valid       BOOL                                   NOT NULL DEFAULT FALSE,
  data        JSONB                                  NOT NULL DEFAULT '{}',
  errors      JSONB                                  NOT NULL DEFAULT '{}'
);
CREATE INDEX idx_pj_pid ON import_project_data (project_id);

CREATE TABLE deleted_records (
    uuid UUID PRIMARY KEY NOT NULL,
    data JSONB NOT NULL DEFAULT '{}',
    reason TEXT NULL,
    deleted TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_by UUID REFERENCES core.users (uuid) NOT NULL
);

CREATE TABLE devices (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  device_type TEXT                     NOT NULL,
  last_seed   TIMESTAMP WITH TIME ZONE NULL,
  did         TEXT                     NOT NULL,
  uid         UUID                      NULL,
  info        JSONB                    NOT NULL DEFAULT '{}',
  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL,
  blocked     BOOL                     NOT NULL DEFAULT FALSE
);

CREATE TABLE records (
  uuid         UUID PRIMARY KEY                     NOT NULL DEFAULT uuid_generate_v4(),
  status       TEXT                                 NOT NULL DEFAULT 'DRAFT',
  stage        TEXT                                 NULL,
  workflow JSONB NULL,

  fid          UUID REFERENCES forms(uuid)         NOT NULL,

  data         JSONB                                NOT NULL DEFAULT '{}',

  submitted    TIMESTAMP WITH TIME ZONE             NOT NULL DEFAULT NOW(),
  submitted_by UUID                                 NULL,
  modified     TIMESTAMP WITH TIME ZONE             NOT NULL DEFAULT NOW(),
  modified_by  UUID                                 NULL,

  revisions    JSONB                                NOT NULL DEFAULT '[]',
  comments     JSONB                                NOT NULL DEFAULT '[]',

  eid          TEXT                                 NULL,
  import_id    UUID                                 NULL,
  source       TEXT                                 NOT NULL DEFAULT 'SYSTEM'
);

CREATE INDEX idx_records_data ON records USING gin (data);
CREATE INDEX idx_records_fid ON records (fid);

CREATE TABLE assignments (
  uuid        UUID PRIMARY KEY         NOT NULL DEFAULT uuid_generate_v4(),
  uid         UUID                     NOT NULL,

  status      TEXT                     NOT NULL DEFAULT 'INACTIVE',
  fid         UUID                     NULL,
  lid         UUID                     NULL,
  assign_group       TEXT                     NULL,
  assign_type TEXT                     NOT NULL,

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);
CREATE INDEX idx_assign_lid ON assignments (lid);

CREATE TABLE drafts (
  uuid        UUID                     PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),

  fid         UUID REFERENCES forms(uuid)                      NOT NULL,

  data        JSONB                    NOT NULL DEFAULT '{}',

  created     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  created_by  UUID                     NULL,
  modified    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  modified_by UUID                     NULL
);

CREATE TABLE outbreaks (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL DEFAULT 'New Outbreak',
    description TEXT NOT NULL DEFAULT 'New Outbreak',
    start_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    end_date TIMESTAMP WITH TIME ZONE NULL,
    status TEXT NOT NULL DEFAULT 'INACTIVE',
    locations UUID[] NOT NULL DEFAULT '{}',
    forms UUID[] NOT NULL DEFAULT '{}',
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

CREATE TABLE reporting (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    lid UUID REFERENCES locations (uuid) NOT NULL,
    fid UUID REFERENCES forms (uuid) NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NULL,
    status TEXT NOT NULL DEFAULT 'ACTIVE',
    pid UUID REFERENCES reporting (uuid) NULL,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);
CREATE INDEX idx_reporting_lid ON reporting (lid);
CREATE INDEX idx_reporting_fid ON reporting (fid);

CREATE TABLE event_log (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    ref_type TEXT NOT NULL,
    ref_id TEXT NOT NULL,
    ref_ts NUMERIC NOT NULL DEFAULT 0,
    command TEXT NOT NULL DEFAULT 'UPDATE',
    data JSONB NULL,
    source TEXT NOT NULL DEFAULT 'SYSTEM',
    ts_ms NUMERIC NOT NULL DEFAULT 0,
    applied BOOL NOT NULL DEFAULT FALSE,
    source_version INT NOT NULL DEFAULT 0,
    device_id TEXT NOT NULL DEFAULT 'CANON'
);
CREATE INDEX idx_ev_ref_type ON event_log (ref_type);

CREATE TABLE manifest (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    device_id TEXT NOT NULL,
    ts_ms INT NOT NULL DEFAULT 0
);

CREATE TABLE roles (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_V4(),
    name TEXT NOT NULL DEFAULT 'New role',
    description TEXT NULL,
    status TEXT NOT NULL DEFAULT 'ACTIVE',
    forms UUID[] NULL,
    permissions JSONB NOT NULL DEFAULT '{}',
    inh TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

CREATE TABLE externals (
    uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL DEFAULT 'New external data',
    description TEXT NULL,
    status TEXT NOT NULL DEFAULT 'INACTIVE',
    schema_cache JSONB NOT NULL DEFAULT '[]',
    db_type TEXT NOT NULL DEFAULT 'pg',
    db_host TEXT NOT NULL,
    db_user TEXT NOT NULL,
    db_pass TEXT NOT NULL,
    db_port INT NULL,
    config JSONB NULL,
    permissions JSONB NOT NULL DEFAULT '{}',
    created TIMESTAMP NOT NULL DEFAULT NOW(),
    created_by UUID NULL,
    modified TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_by UUID NULL
);

create or replace view v_alerts as
  SELECT
    a.*,
    m.name       AS alarm_name,
    m.definition as alarm_config,
    l.name       AS location_name,
    l.lti,
    lt.name      AS st_name,
    l.lineage
  FROM alerts a
    LEFT JOIN alarms m ON m.uuid = a.alid
    LEFT JOIN locations l ON l.uuid = a.lid
    LEFT JOIN location_types lt ON lt.uuid = l.lti;
    
CREATE OR REPLACE VIEW v_assignments AS 
    SELECT
        u.uuid,
        uu.name,
        uu.email,
        u.role,
        a.status AS assign_status,
        r.start_date,
        r.end_date,
        r.status AS period_status,
        l.name AS location_name,
        l.status as location_status
    FROM accounts AS u
        LEFT JOIN core.users AS uu ON uu.uuid = u.uuid
        LEFT JOIN assignments AS a on a.uid = u.uuid
        LEFT JOIN reporting AS r ON r.uuid = a.lid
        LEFT JOIN locations AS l ON l.uuid = a.lid;

create or replace view v_users as
  SELECT
    a.uuid,
    a.ruuid,
    a.role,
    a.permissions,
    a.settings,
    a.profile,
    a.org_id,
    a.created,
    a.created_by,
    a.modified,
    a.modified_by,
    a.status,
    ac.tki,
    ac.name AS account_name,
    ac.uuid   AS aid,
    u.name,
    u.email,
    u.password,
    u.accounts,
    o.name  AS org_name,
    u.system
  FROM users a
    LEFT JOIN core.users u ON u.uuid = a.uuid
    LEFT JOIN organizations o ON o.uuid = a.org_id
    LEFT JOIN core.accounts ac ON '48084f7f-5eae-4eda-811d-ac99dc1d2b91'::UUID = ac.uuid;


-- triggered when a resource is deleted
CREATE OR REPLACE FUNCTION fn_resource_delete() RETURNS TRIGGER AS
$BODY$
DECLARE
    event_id UUID;
    res_type TEXT;
    query TEXT;
BEGIN
    IF (TG_TABLE_NAME = 'records') THEN
        res_type = 'RECORD';
    ELSIF (TG_TABLE_NAME = 'alarms') THEN
        res_type = 'ALARM';
    ELSIF (TG_TABLE_NAME = 'tasks') THEN
        res_type = 'TASK';
    ELSIF (TG_TABLE_NAME = 'resources') THEN
        res_type = 'RESOURCE';
    ELSIF (TG_TABLE_NAME = 'alerts') THEN
        res_type = 'ALERT';
    ELSIF (TG_TABLE_NAME = 'documents') THEN
        res_type = 'DOCUMENT';
    ELSIF (TG_TABLE_NAME = 'assignments') THEN
        res_type = 'ASSIGNMENT';
    ELSIF (TG_TABLE_NAME = 'devices') THEN
        res_type = 'DEVICE';
    ELSIF (TG_TABLE_NAME = 'forms') THEN
        res_type = 'FORM';
    ELSIF (TG_TABLE_NAME = 'location_types') THEN
        res_type = 'LOCATION_TYPE';
    ELSIF (TG_TABLE_NAME = 'locations') THEN
        res_type = 'LOCATION';
    ELSIF (TG_TABLE_NAME = 'teams') THEN
        res_type = 'TEAM';
    ELSIF (TG_TABLE_NAME = 'team_messages') THEN
        res_type = 'TEAM_MESSAGE';
    ELSIF (TG_TABLE_NAME = 'indicators') THEN
        res_type = 'INDICATOR';
    ELSIF (TG_TABLE_NAME = 'indicator_groups') THEN
        res_type = 'INDICATOR_GROUP';
    ELSIF (TG_TABLE_NAME = 'reporting') THEN
        res_type = 'REPORTING';
    ELSIF (TG_TABLE_NAME = 'roles') THEN
        res_type = 'ROLE';
    ELSIF (TG_TABLE_NAME = 'intervals') THEN
        res_type = 'INTERVAL';
    ELSIF (TG_TABLE_NAME = 'organizations') THEN
        res_type = 'ORGANIZATION';
    END IF;
    
    query := 'INSERT INTO ' || QUOTE_IDENT(TG_TABLE_SCHEMA) || '.event_log (ref_type, ref_id, ref_ts, command, data, source, ts_ms, applied, source_version, device_id) VALUES (' || QUOTE_LITERAL(res_type) || 
             ', ' || QUOTE_LITERAL(OLD.uuid) ||
             ', ' || QUOTE_LITERAL(EXTRACT(epoch FROM NOW()) * 1000) ||
             ', ' || QUOTE_LITERAL('DELETE') ||
             ',NULL' ||
             ', ' || QUOTE_LITERAL('WEB') ||
             ', ' || QUOTE_LITERAL(EXTRACT(epoch FROM NOW()) * 1000) ||
             ',TRUE , 0' ||
             ', ' || QUOTE_LITERAL('GLOBAL') || ') RETURNING uuid;';
            
    EXECUTE query INTO event_id;

    EXECUTE 'NOTIFY sonoma, ' || QUOTE_NULLABLE(json_build_object('TASK_TYPE', 'DELETE', 'tki', TG_TABLE_SCHEMA, 'eid', event_id)) || ';';
    RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE;

-- Triggered when a resource changes
CREATE OR REPLACE FUNCTION fn_resource_change() RETURNS TRIGGER AS
$BODY$
DECLARE
    event_id UUID;
    res_type TEXT;
    command TEXT;
    query TEXT;
    ts_ms INTEGER;
BEGIN
    IF (TG_TABLE_NAME = 'records') THEN
        res_type = 'RECORD';
    ELSIF (TG_TABLE_NAME = 'alarms') THEN
        res_type = 'ALARM';
    ELSIF (TG_TABLE_NAME = 'tasks') THEN
        res_type = 'TASK';
    ELSIF (TG_TABLE_NAME = 'resources') THEN
        res_type = 'RESOURCE';
    ELSIF (TG_TABLE_NAME = 'alerts') THEN
        res_type = 'ALERT';
    ELSIF (TG_TABLE_NAME = 'documents') THEN
        res_type = 'DOCUMENT';
    ELSIF (TG_TABLE_NAME = 'assignments') THEN
        res_type = 'ASSIGNMENT';
    ELSIF (TG_TABLE_NAME = 'devices') THEN
        res_type = 'DEVICE';
    ELSIF (TG_TABLE_NAME = 'forms') THEN
        res_type = 'FORM';
    ELSIF (TG_TABLE_NAME = 'location_types') THEN
        res_type = 'LOCATION_TYPE';
    ELSIF (TG_TABLE_NAME = 'locations') THEN
        res_type = 'LOCATION';
    ELSIF (TG_TABLE_NAME = 'teams') THEN
        res_type = 'TEAM';
    ELSIF (TG_TABLE_NAME = 'team_messages') THEN
        res_type = 'TEAM_MESSAGE';
    ELSIF (TG_TABLE_NAME = 'indicators') THEN
        res_type = 'INDICATOR';
    ELSIF (TG_TABLE_NAME = 'indicator_groups') THEN
        res_type = 'INDICATOR_GROUP';
    ELSIF (TG_TABLE_NAME = 'reporting') THEN
        res_type = 'REPORTING';
    ELSIF (TG_TABLE_NAME = 'roles') THEN
        res_type = 'ROLE';
    ELSIF (TG_TABLE_NAME = 'intervals') THEN
        res_type = 'INTERVAL';
    ELSIF (TG_TABLE_NAME = 'organizations') THEN
        res_type = 'ORGANIZATION';
    END IF;


    IF (TG_OP = 'INSERT') THEN
        command = 'INSERT';
    END IF;

    IF (TG_OP = 'UPDATE') THEN 
        command = 'UPDATE';
    END IF;
    
     query := 'INSERT INTO ' || QUOTE_IDENT(TG_TABLE_SCHEMA) || '.event_log (ref_type, ref_id, ref_ts, command, data, source, ts_ms, applied, source_version, device_id) VALUES (' || QUOTE_LITERAL(res_type) || 
             ', ' || QUOTE_LITERAL(NEW.uuid) ||
             ', ' || QUOTE_LITERAL(EXTRACT(epoch FROM NOW()) * 1000) ||
             ', ' || QUOTE_LITERAL(command) ||
             ', ' || QUOTE_LITERAL(row_to_json(NEW)) ||
             ', ' || QUOTE_LITERAL('WEB') ||
             ', ' || QUOTE_LITERAL(EXTRACT(epoch FROM NOW()) * 1000) ||
             ',TRUE , 0' ||
             ', ' || QUOTE_LITERAL('GLOBAL') || ') RETURNING uuid;';
            
    EXECUTE query INTO event_id;

    EXECUTE 'NOTIFY sonoma, ' || QUOTE_NULLABLE(json_build_object('TASK_TYPE', 'DELETE', 'tki', TG_TABLE_SCHEMA, 'eid', event_id)) || ';';
    RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER trigger_records_change AFTER INSERT OR UPDATE ON records FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_records_delete AFTER DELETE ON records FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_alerts_change AFTER INSERT OR UPDATE ON alerts FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_alerts_delete AFTER DELETE ON alerts FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_alarms_change AFTER INSERT OR UPDATE ON alarms FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_alarms_delete AFTER DELETE ON alerts FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_locations_update AFTER INSERT OR UPDATE ON locations FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_locations_delete AFTER DELETE ON locations FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_location_types_update AFTER INSERT OR UPDATE ON location_types FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_location_types_delete AFTER DELETE ON location_types FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_organizations_update AFTER INSERT OR UPDATE ON organizations FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_organizations_delete AFTER DELETE ON organizations FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_indicators_update AFTER INSERT OR UPDATE ON indicators FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_indicators_delete AFTER DELETE ON indicators FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_indicator_groups_update AFTER INSERT OR UPDATE ON indicator_groups FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_indicator_groups_delete AFTER DELETE ON indicator_groups FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_assignment_update AFTER INSERT OR UPDATE ON assignments FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_assignment_delete AFTER DELETE ON assignments FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_teams_update AFTER INSERT OR UPDATE ON teams FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_teams_delete AFTER DELETE ON teams FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_documents_update AFTER INSERT OR UPDATE ON documents FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_documents_delete AFTER DELETE ON documents FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_tasks_update AFTER INSERT OR UPDATE ON tasks FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_tasks_delete AFTER DELETE ON tasks FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_roles_update AFTER INSERT OR UPDATE ON roles FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_roles_delete AFTER DELETE ON roles FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_intervals_update AFTER INSERT OR UPDATE ON intervals FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_intervals_delete AFTER DELETE ON intervals FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_reporting_update AFTER INSERT OR UPDATE ON reporting FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_reporting_delete AFTER DELETE ON reporting FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_invites_update AFTER INSERT OR UPDATE ON invites FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_invites_delete AFTER DELETE ON invites FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_team_messages_update AFTER INSERT OR UPDATE ON team_messages FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_team_messages_delete AFTER DELETE ON team_messages FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
CREATE TRIGGER trigger_forms_update AFTER INSERT OR UPDATE ON forms FOR EACH ROW EXECUTE PROCEDURE fn_resource_change();
CREATE TRIGGER trigger_forms_delete AFTER DELETE ON forms FOR EACH ROW EXECUTE PROCEDURE fn_resource_delete();
