use std::collections::HashMap;
use std::io::{Error};
use std::time::{Duration};

use chrono::prelude::*;

lazy_static! {
    static ref MONTH_DAYS: HashMap<u32, u32> = {
        let mut m = HashMap::new();

        m.insert(1, 31); // Jan
        m.insert(2, 28); // Feb
        m.insert(3, 31); // Mar
        m.insert(4, 30); // Apr
        m.insert(5, 31); // May
        m.insert(6, 30); // Jun
        m.insert(7, 31); // Jul
        m.insert(8, 31); // Aug
        m.insert(9, 30); // Sep
        m.insert(10, 31); // Oct
        m.insert(11, 30); // Nov
        m.insert(12, 31); // Dec

        m
    };
}

pub fn get_reporting_intervals(start_dat: &NaiveDate, end_date: &NaiveDate, interval: &String) -> Result<Vec<NaiveDate>, Error> {
   unimplemented!() 
}
