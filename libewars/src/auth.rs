use md5;
use rand;

use rand::Rng;

pub fn verify_pass(plaintext: &String, encoded: &String) -> Result<(), ()> {
    let splitted: Vec<&str> = encoded.split(":").collect();

    let pass: String = splitted[0].to_string();
    let salt: String = splitted[1].to_string();
    let digi_string: String = format!("{}{}", plaintext, salt);
    let m = md5::compute(&digi_string);
    let digest: String = format!("{:x}", m);

    if &pass == &digest {
        Ok(())
    } else {
        Err(())
    }
}

pub fn generate_password(plaintext: &String) -> Result<String, ()> {
    use rand::Rng;
    let mut coded_pass: String = String::new();
    let mut salt: String = rand::thread_rng().gen_iter::<char>().take(16).collect();

    let combined: String = format!("{}{}", plaintext, salt);

    let m = md5::compute(&combined);
    let digest: String = format!("{:x}", m);

    coded_pass = format!("{}:{}", digest, salt.to_string());

    Ok(coded_pass)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_verify() {

    }
}
