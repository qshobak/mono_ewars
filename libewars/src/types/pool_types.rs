use r2d2::{Pool, PooledConnection};
use r2d2_postgres::{PostgresConnectionManager};
use r2d2_sqlite::{SqliteConnectionManager};

pub type PgPoolConnection = PooledConnection<PostgresConnectionManager>;
pub type SqlitePoolConnection = PooledConnection<SqliteConnectionManager>;
