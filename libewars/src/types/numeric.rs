#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Numeric {
    Integer(i64),
    Float(f64),
    Str(String),
}
