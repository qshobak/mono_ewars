use srd::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row as PostgresRow};
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection AS SqliteConnection};

use crate::models::{Alarm};
use crate::types::{PgConnection, Numeric, IndicatorSource};
use crate::utils::*;
use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize)]
pub struct LegacyAlarm(HashMap<String, Value>);

impl Upgrade<&SqliteConnection> for LegacyAlarm {
    type Item = Alarm;

    fn upgrade(&self, conn; &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemented!");
    }
}

