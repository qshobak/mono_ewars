use std::collections::HashMap;

use uuid::Uuid;
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use crate::types::{PgConnection};
use crate::models::{AlarmDefinition, AlarmStage, Alarm};
use crate::traits::{Creatable};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlarmCreate {
    pub name: String,
    pub status: String,
    pub description: String,
    pub definition: AlarmDefinition,
    pub workflow: Option<Vec<AlarmStage>>,
    pub permissions: Option<HashMap<String, Value>>,
    pub guidance: Option<String>,
}

static PG_CREATE: &'static str = r#"
    INSERT INTO {SCHEMA}.alarms 
    (name, status, description, definition, workflow, permissions, guidance, created_by, modified_by)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
"#;

static PG_CREATE_RETURNING: &'static str = r#"
    INSERT INTO {SCHEMA}.alarms 
    (name, status, description, definition, workflow, permissions, guidance, created_by, modified_by)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *;
"#;


#[cfg(feature = "with-postgres")]
impl Creatable<&PgConnection> for AlarmCreate {
    type Item = Alarm;

    fn create(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_CREATE.replace("{SCHEMA}", &tki), &[
            &self.name,
            &self.status,
            &self.description,
            &json::to_value(&self.definition).unwrap(),
            &json::to_value(&self.workflow).unwrap(),
            &json::to_value(&self.permissions).unwrap(),
            &self.guidance,
            &uid,
            &uid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn create_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_CREATE.replace("{SCHEMA}", &tki), &[
            &self.name,
            &self.status,
            &self.description,
            &json::to_value(&self.definition).unwrap(),
            &json::to_value(&self.workflow).unwrap(),
            &json::to_value(&self.permissions).unwrap(),
            &self.guidance,
            &uid,
            &uid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Alarm::from(row))
                } else {
                    bail!("ERR_NOT_FOUND");
                }
            }
            Err(err) => bail!(err)
        }
    }
}
