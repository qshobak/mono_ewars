use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Device {
    pub uuid: Uuid,
    pub device_type: String,
    pub last_seed: DateTime<Utc>,
    pub did: Option<String>,
    pub uid: Option<Uuid>,
    pub info: HashMap<String, Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}
