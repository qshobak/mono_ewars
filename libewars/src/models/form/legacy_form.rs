use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::models::{Form};
use crate::types::{Numeric, IndicatorSource};
use crate::utils::*;
use crate::traits::{Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyForm(HashMap<String, Value>);

impl Upgrade<&SqliteConnection> for LegacyForm {
    type Item = Form;

    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemtened");
    }
}
