use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;
use chrono::prelude::*;
use uuid::Uuid;

use rusqlite::{Connection as SqliteConnection, Row as SqliteRow};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{Numeric, PgConnection};
use crate::models::{Field, Query, QueryResult};
use crate::traits::{Insertable, Queryable};
use crate::types::{SqlitePoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormFeature {
    pub interval: Option<String>,
    pub threshold: Option<Numeric>,
    pub lti: Option<Uuid>,
    pub site_type_id: Option<Uuid>,
    pub field: Option<String>,
    pub apprvals: Option<bool>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FeatureDefinition {
    Bool(bool),
    Definition(FormFeature),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormStage {

}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormRevision {}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EtlDefinition {}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Form {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub version: i32,
    pub status: String,
    pub description: String,
    pub guidance: Option<HashMap<String, Option<String>>>,
    pub eid_prefix: Option<String>,
    pub features: HashMap<String, Option<FeatureDefinition>>,
    pub definition: HashMap<String, Field>,
    pub constraints: Option<Value>,
    pub stages: Option<HashMap<Uuid, FormStage>>,
    pub etl: Option<HashMap<Uuid, EtlDefinition>>,
    pub changes: Option<Vec<FormRevision>>,
    pub permissions: Option<HashMap<String, Value>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

fn default_name() -> HashMap<String, String> {
    let mut name: HashMap<String, String> = HashMap::new();
    name.insert("en".to_string(), "UNK_FIELD_NAME".to_string());

    name
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Form {
    fn from(row: PostgresRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let features: HashMap<String, Option<FeatureDefinition>> = json::from_value(row.get("features")).unwrap();
        let definition: HashMap<String, Field> = json::from_value(row.get("definition")).unwrap();
        let stages: Option<HashMap<Uuid, FormStage>> = opt_field_json!(row, "stages");
        let etl: Option<HashMap<Uuid, EtlDefinition>> = Some(HashMap::new());
        let changes: Option<Vec<FormRevision>> = opt_field_value!(row, "changes");
        let permissions: Option<HashMap<String, Value>> = opt_field_json!(row, "permissions");
        let guidance: Option<HashMap<String, Option<String>>> = opt_field_json!(row, "guidance");

        Form {
            uuid: row.get("uuid"),
            name,
            version: row.get("version"),
            status: row.get("status"),
            description: row.get("description"),
            guidance,
            eid_prefix: row.get("eid_prefix"),
            features,
            definition,
            constraints: row.get("constraints"),
            stages,
            etl,
            changes,
            permissions,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Form {
    fn from(row: &SqliteRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let features: HashMap<String, Option<FeatureDefinition>> = json::from_value(row.get("features")).unwrap();
        let definition: HashMap<String, Field> = json::from_value(row.get("definition")).unwrap();
        let stages: Option<HashMap<Uuid, FormStage>> = opt_value!(row, "stages");
        let etl: Option<HashMap<Uuid, EtlDefinition>> = opt_value!(row, "etl");
        let changes: Option<Vec<FormRevision>> = opt_value!(row, "changes");
        let permissions: Option<HashMap<String, Value>> = opt_value!(row, "permissions");
        let guidance: Option<HashMap<String, Option<String>>> = opt_value!(row, "guidance");

        Form {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name,
            version: row.get("version"),
            status: row.get("status"),
            description: row.get("description"),
            guidance,
            eid_prefix: row.get("eid_prefix"),
            features,
            definition,
            constraints: row.get("constraints"),
            stages,
            etl,
            changes,
            permissions,
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }

    }
}


impl Form {
    pub fn get_fields_as_vec_path(&self) -> Vec<String> {
        let mut fields: Vec<String> = Vec::new();

        for (key, val) in &self.definition {
            let field_type: String = val.field_type.clone();

            match field_type.as_ref() {
                "matrix" => {
                    if let Some(rows) = &val.fields {
                        for (row_key, row_val) in rows {
                            if let Some(cells) = &row_val.fields {
                                for (cell_key, cell_val) in cells {
                                    fields.push(format!("{}.{}.{}", key, row_key, cell_key));
                                } 
                            }
                        }
                    }
                },
                "row" => {},
                "display" => {},
                "calculated" => {},
                _ => {
                    fields.push(key.clone());
                }
            }
        }

        fields
    }

    pub fn format_date(&self, dt: &Date<Utc>) -> String {
        if let Some(c) = self.definition.get("__dd__") {
            if let Some(interval) = &c.interval {
                match interval.as_ref() {
                    "WEEK" => {
                        dt.format("%G W%V").to_string()
                    },
                    "YEAR" => {
                        dt.format("%Y").to_string()
                    },
                    _ => {
                        dt.format("%Y-%m-%d").to_string()
                    }
                }
            } else {
                dt.format("%Y-%m-%d").to_string()
            }
        } else {
            dt.format("%Y-%m-%d").to_string()
        }
    }

    pub fn get_loc_fields(&self) -> Vec<(String, Field)> {
        let mut fields: Vec<(String, Field)> = Vec::new();

        for (key, field) in &self.definition {
            if &field.field_type == "location" {
                fields.push((key.clone(), field.clone()));
            }
        }

        fields
    }

    pub fn get_text_fields(&self) -> Vec<(String, Field)> {
        let mut fields: Vec<(String, Field)> = Vec::new();

        for (key, field) in &self.definition {
            if &field.field_type == "text" {
                fields.push((key.clone(), field.clone()));
            }

            if &field.field_type == "textarea" {
                fields.push((key.clone(), field.clone()));
            }
        }

        fields
    }

    pub fn get_date_fields(&self) -> Vec<(String, Field)> {
        let mut fields: Vec<(String, Field)> = Vec::new();

        for (key, field) in &self.definition {
            if &field.field_type == "date" {
                fields.push((key.clone(), field.clone()));
            }
        }

        fields
    }

    pub fn get_reporting_location_field(&self) -> Option<Field> {
        match self.definition.clone().get("__lid__") {
            Some(res) => Some(res.clone()),
            None => Option::None
        }
    }

    pub fn get_reporting_location_type(&self) -> Option<Uuid> {
        if let Some(c) = self.definition.get("__lid__") {
            if let Some(r) = c.lti {
                Some(r.clone())
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }

    // Get a field from the form
    pub fn get_field(&self, key: &String) -> Option<Field> {
        if key.contains(".") {
            let path: Vec<String> = key.split(".").map(|x| x.to_string()).collect();
            let mut cur_field: Option<Field> = Option::None;

            if path.len() == 3 {
                if let Some(matrix) = self.definition.clone().get(&path[0]) {
                    matrix.get_sub_field(&path[1..].join("."))
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            if let Some(c) = self.definition.clone().get(&key.clone()) {
                Some(c.clone())
            } else {
                Option::None
            }
        }
    }

}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO forms 
    (uuid, name, version, status, description, guidance, eid_prefix, features, definition, constraints, stages, etl, changes, permissions, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18);
"#;

static SQLITE_GET_FORM: &'static str = r#"
    SELECT * FROM forms WHERE uuid = ?;
"#;

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT f.*,
        (uu.name || ' (' || uu.email || ')') AS creator,
        (uc.name || ' (' || uc.email || ')') AS modifier
    FROM forms AS f
        LEFT JOIN users AS uu ON uu.uuid = f.created_by
        LEFT JOIN users AS uc ON uc.uuid = f.modified_by
    WHERE f.uuid = ?;
"#;

static SQLITE_GET_BY_ID_B: &'static str = r#"
    SELECT f.*
    FROM forms AS f
    WHERE f.uuid = ?;
"#;


#[cfg(feature = "with-sqlite")]
impl Queryable<&SqliteConnection> for Form {
    type Item = Form;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query_row(&SQLITE_GET_BY_ID, &[&id.to_string() as &ToSql], |row| {
            Form::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => {
                bail!(err);
            }
        }
    }

    fn get_base_by_id(conn: &SqliteConnection, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query_row(&SQLITE_GET_BY_ID_B, &[&id.to_string() as &ToSql], |row| {
            Form::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => {
                bail!(err);
            }
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl Insertable<&SqliteConnection> for Form {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;
        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &json::to_value(&self.name).unwrap(),
            &self.version,
            &self.status,
            &self.description,
            &in_opt_value!(&self.guidance),
            &self.eid_prefix,
            &json::to_value(&self.features).unwrap(),
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.constraints),
            &in_opt_value!(&self.stages),
            &in_opt_value!(&self.etl),
            &json::to_value(&self.changes).unwrap(),
            &in_opt_value!(&self.permissions).unwrap(),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                bail!("{:?}", err);
            }
        }
    }
}

static PG_GET_BY_ID: &'static str = r#"
    SELECT * FROM {SCHEMA}.forms WHERE uuid = ?;
"#;

static PG_QUERY: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.forms as r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static PG_COUNT: &'static str = r#"
    SELECT COUNT(r.uuid) AS total
    FROM {SCHEMA}.forms as r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

#[cfg(feature = "with-postgres")]
impl Queryable<&PgConnection> for Form {
    type Item = Form;
    type Query = Query;
    type QueryResult = QueryResult;

    fn query(conn: &PgConnection, tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        query_fn!(conn, query, PG_QUERY, PG_COUNT, tki, Form)
    }

    fn get_by_id(conn: &PgConnection, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query(&PG_GET_BY_ID.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Some(Form::from(row)))
                } else {
                    Ok(Option::None)
                }
            },
            Err(err) => bail!(err)
        }
    }
}

