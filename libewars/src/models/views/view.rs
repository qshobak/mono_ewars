use std::collections::HashMap;

use failure::Error;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use chrono::prelude::*;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use postgres::rows::{Row as PostgresRow};

use crate::traits::{Insertable, Queryable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct View {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub status: String,
    pub description: HashMap<String, String>,
    pub definition: HashMap<String, Value>,
    pub permissions: HashMap<String, Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for View {
    fn from(row: PostgresRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let description: HashMap<String, String> = json::from_value(row.get("description")).unwrap();
        let definition: HashMap<String, Value> = json::from_value(row.get("definition")).unwrap();
        let permissions: HashMap<String, Value> = json::from_value(row.get("permissions")).unwrap();

        View {
            uuid: row.get("uuid"),
            name,
            status: row.get("status"),
            description,
            definition,
            permissions,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for View {
    fn from(row: &SqliteRow) -> Self {
        View {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: opt_value!(row, "name").unwrap(),
            status: row.get("status"),
            description: opt_value!(row, "description").unwrap(),
            definition: opt_value!(row, "definition").unwrap(),
            permissions: opt_value!(row, "permissions").unwrap(),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

