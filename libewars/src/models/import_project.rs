use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ImportProject {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub status: String,
    pub form_id: Uuid,
    pub mapping: Option<HashMap<String, Value>>,
    pub meta_map: Option<HashMap<String, Value>>,
    pub src_file: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,

}
