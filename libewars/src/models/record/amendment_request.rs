pub struct AmendmentRequest {
    pub uuid: Uuid,
    pub reason: String,
    pub amendments: HashMap<String, Value>,
}
