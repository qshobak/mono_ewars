use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::models::{Record};
use crate::utils::*;
use crate::traits::{Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyRecord(HashMap<String, Value>);

impl LegacyRecord<&SqliteConnection> for LegacyRecord {
    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {

    }
}
