use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use rusqlite::{Connection as SqliteConnection};

use crate::traits::{Creatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordCreate {
    pub uuid: Option<Uuid>,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
}

static SQLITE_SUBMIT: &'static str = r#"
    INSERT INTO records
    (uuid, status, stage, fid, data, submitted, submitted_by, modified, modified_by, eid, source)
    VALUES (:uuid, :status, :stage, :fid, :data, :submitted, :submitted_by, :modified, :modified_by, :eid, :source);
"#;

#[cfg(feature = "with-sqlite")]
impl Creatable<&SqliteConnection>for RecordCreate {
    type Item = Record;

    fn create(&self, conn: &SqliConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        let new_uuid: Uuid = Uuid::new_v4();

        let mut eid: Option<String> = Option::None;
        let source: String = "DESKTOP".to_string();

        // Delete the draft if it exists
        let mut cur_uuid: Option<Uuid> = Option::None;
        if let Some(c) = self.uuid {
            cur_uuid = Some(c.clone());
            Draft::delete_by_id(&conn, Option::None, &c)?;
        } else {
            cur_uuid = Some(Uuid::new_v4());
        }

        let cur_uuid: Uuid = cur_uuid.unwrap();

        // Alerts which are raised on local are provisional

        conn.execute_named(&SQLITE_SUBMIT, &[
            (":uuid", &cur_uuid.to_string()),
            (":status", &"SUBMITTED"),
            (":stage", &"SUBMITTED"),
            (":fid", &self.fid),
            (":data", &json::to_value(&self.data).unwrap()),
            (":submitted_by", &uid),
            (":submitted", &cur_time),
            (":modified", &cur_time),
            (":modified_by", &cur_uuid),
            (":eid", &eid),
            (":source", &"DESKTOP"),
        ])?
    }
}
