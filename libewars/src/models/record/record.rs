use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;
use chrono::prelude::*;
use uuid::Uuid;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::{NO_PARAMS};
use rusqlite::types::{ToSql};

use postgres::rows::{Row as PgRow};

use crate::types::{PgConnection};
use crate::models::{Form, Field, RecordsQuery, QueryResult};
use crate::traits::{Insertable, Queryable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Record {
    pub uuid: Uuid,
    pub status: String,
    pub stage: Option<String>,
    pub workflow: Option<Value>,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
    pub submitted: DateTime<Utc>,
    pub submitted_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub revisions: Option<Value>,
    pub comments: Option<Value>,
    pub eid: Option<String>,
    pub import_id: Option<Uuid>,
    pub source: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub submitter: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, Value>>,
}

impl<'a> From<PgRow<'a>> for Record {
    fn from(row: PgRow) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        Record {
            uuid: row.get("uuid"),
            status: row.get("status"),
            stage: row.get("stage"),
            workflow: row.get("workflow"),
            fid: row.get("fid"),
            data,
            submitted: row.get("submitted"),
            submitted_by: row.get("submitted_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            revisions: row.get("revisions"),
            comments: row.get("comments"),
            eid: row.get("eid"),
            import_id: row.get("import_id"),
            source: row.get("source"),

            submitter: opt_field!(row, "submitter"),
            modifier: opt_field!(row, "submitter"),
            location_name: opt_field_json!(row, "location_name"),
            form_name: opt_field_json!(row, "form_name"),
            metadata: opt_field_json!(row, "metadata"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Record {
    fn from(row: &SqliteRow) -> Self {
        
        Record {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            status: row.get("status"),
            stage: row.get("stage"),
            workflow: opt_value!(row, "workflow"),
            fid: opt_uuid!(row, "fid").unwrap(),
            data: opt_value!(row, "data").unwrap(),
            submitted: row.get("submitted"),
            submitted_by: opt_uuid!(row, "submitted_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            revisions: opt_value!(row, "revisions"),
            comments: opt_value!(row, "comments"),
            eid: opt_string!(row, "eid"),
            import_id: opt_uuid!(row, "uuid"),
            source: opt_string!(row, "source"),

            submitter: opt_string!(row, "submitter"),
            modifier: opt_string!(row, "modifier"),
            location_name: opt_value!(row, "location_name"),
            form_name: opt_value!(row, "form_name"),
            metadata: opt_value!(row, "metadata"),
        }
    }
}

static SQLITE_UPDATE_STATUS: &'static str = r#"
    UPDATE records SET status = ?1 WHERE uuid = ?2;
"#;


#[cfg(feature = "with-sqlite")]
impl Record {
    // Update the status of a record we haven't done anythign with
    pub fn update_status_by_id(conn: &SqliteConnection, id: &Uuid, new_status: &str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_UPDATE_STATUS)?;
        match stmt.execute(&[
            &new_status as &ToSql,
            &id.to_string(),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err),
        }
    }

    // update the status of the current item
    pub fn update_state(&self, conn: &SqliteConnection, new_status: &str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_UPDATE_STATUS)?;

        match stmt.execute(&[
            &new_status as &ToSql,
            &self.uuid.to_string(),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }   
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO records 
    (uuid, status, stage, workflow, fid, data, submitted, submitted_by, modified, modified_by, revisions, comments, eid, import_id, source)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15);
"#;

#[cfg(feature = "with-sqlite")]
impl Insertable<&SqliteConnection> for Record {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.status,
            &self.stage,
            &json::to_value(&self.workflow).unwrap(),
            &self.fid.to_string(),
            &json::to_value(&self.data).unwrap(),
            &self.submitted,
            &in_opt_uuid!(&self.submitted_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
            &in_opt_value!(&self.revisions),
            &in_opt_value!(&self.comments),
            &self.eid,
            &in_opt_uuid!(&self.import_id),
            &self.source,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err),
        }
    }
}

static PG_GET_RECORD: &'static str = r#"
    SELECT * FROM {SCHEMA}.records WHERE uuid = $1;
"#;

impl Record {
    // Get the canonical date for the record
    pub fn get_record_date(&self) -> Option<Date<Utc>> {
        if let Some(c) = self.data.clone().get("__dd__") {
            let dt: String = json::from_value(c.clone()).unwrap();
            let dtand: String = format!("{}T09:10:11Z", dt);
            let data: DateTime<Utc> = dtand.parse::<DateTime<Utc>>().unwrap();
            Some(data.date())
        } else {
            Some(self.submitted.clone().date())
        }
    }

    // Apply a set of amendments to this record
    pub fn apply_amendments(&mut self) -> Result<(), Error> {
        unimplemented!()
    }
}

static SQLITE_QUERY: &'static str = r#"
    SELECT r.*,
        (uu.name || " (" || uu.email || ")") AS submiiter,
        (uc.name || " (" || uc.email || ")") AS modifier
"#;

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT r.*,
        (uu.name || ' (' || uu.email || ')') AS submitter,
        (uc.name || ' (' || uc.email || ')') AS modifier
    FROM records AS r
        LEFT JOIN users AS uu ON uu.uuid = r.submitted_by
        LEFT JOIN users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = ?;
"#;

#[cfg(feature = "with-sqlite")]
impl Queryable<&SqliteConnection> for Record {
    type Item = Record;
    type Query = RecordsQuery;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &SqliteConnection, tki: &'static str, id: &Uuid) -> Result<Option<Record>, Error> {
        let mut stmt = conn.prepare(&SQLITE_GET_BY_ID)?;

        match stmt.query_row(&[&id.to_string() as &ToSql], |row| {
            Record::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => {
                bail!(err);
            }
        }
    }

    fn query(conn: &SqliteConnection, tki: &'static str, query: &RecordsQuery) -> Result<QueryResult, Error> {
        // Get the form
        let form: Form = match Form::get_by_id(conn, "", &query.fid) {
            Ok(Some(res)) => res,
            Ok(Option::None) => bail!("ERR_NO_FORM"),
            Err(err) => bail!(err),
        };

        let mut sql: Vec<String> = vec![
            "SELECT r.*,".to_string(),
            " (uu.name || ' (' || uu.email || ')') AS submitter,".to_string(),
            " (uc.name || ' (' || uc.email || ')') AS modifier".to_string(),
        ];

        let mut sql_count: Vec<String> = vec![
            "SELECT COUNT(r.uuid) AS total".to_string(),
        ];

        // Set up dicts
        let mut loc_aliases: HashMap<String, String> = HashMap::new();
        let mut location_fields: Vec<(String, Field)> = Vec::new();
        let mut joins: Vec<String> = Vec::new();
        let mut meta_fields: Vec<String> = Vec::new();

        // Pull the location fields from the form so we know what to join on
        let loc_fields: Vec<(String, Field)> = form.get_loc_fields();

        // we use the counter to increment the field names
        let mut counter = 1;
        let total_fields = loc_fields.len();
        if total_fields > 0 {
            sql.push(",".to_string());
        }
        for lf in loc_fields {
            let field_name = lf.0.clone();
            if counter < total_fields {
                sql.push(format!("json_extract(l_{}.name, '$.en') AS l_{},", field_name, field_name));
            } else {
                sql.push(format!("json_extract(l_{}.name, '$.en') AS l_{}", field_name, field_name));
            }
            counter += 1;
            meta_fields.push(format!("l_{}", field_name));
            joins.push(format!("LEFT JOIN locations AS l_{} ON json_extract(r.data, '$.{}') = l_{}.uuid",
                               field_name,
                               field_name,
                               field_name
            ));
        }


        sql.push("FROM records AS r".to_string());
        sql_count.push("FROM records AS r".to_string());

        sql.push("LEFT JOIN users AS uu ON uu.uuid = r.submitted_by".to_string());
        sql.push("LEFT JOIN users AS uc ON uc.uuid = r.modified_by".to_string());


        for join in joins {
            sql.push(join.clone());
            sql_count.push(join);
        }

        let mut wheres: Vec<String> = Vec::new();
        let mut orders: Vec<String> = Vec::new();

        for item in &query.filters {
            let mut flt_str: Vec<String> = Vec::new();
            match item.key.as_ref() {
                "submitted" => {
                    flt_str.push("submitter".to_string());
                },
                _ => {
                    if item.key.contains("data.") {
                        flt_str.push(format!("json_extract(r.data, '$.{}')", item.key.replace("data.", "")));
                    } else {
                        flt_str.push(format!("r.{}", item.key));
                    }
                }
            }

            let mut use_val = true;
            let s_cmp = match item.cmp.as_ref() {
                "eq" => "=",
                "neq" => "!=",
                "gt" => ">",
                "gte" => ">=",
                "lt" => "lt",
                "lte" => "lte",
                "null" => {
                    use_val = false;
                    "IS NULL"
                },
                "nnull" => {
                    use_val = false;
                    "IS NOT NULL"
                },
                _ => "=",
            };
            flt_str.push(s_cmp.to_string());

            if use_val {
                if let Some(c) = &item.value {
                    let as_str: String = c.clone().as_str().unwrap().to_string(); 
                    flt_str.push(format!("'{}'", as_str));
                }
            }

            wheres.push(flt_str.join(" "));
        }

        for item in &query.orders {
            let mut ord_str: Vec<String> = Vec::new();

            match item.key.as_ref() {
                "submitter" => {
                    ord_str.push("submitter".to_string());
                }
                "modifier" => {
                    ord_str.push("modifier".to_string());
                }
                _ => {
                    if item.key.contains("data.") {
                        ord_str.push(format!("json_extract(r.data, '$.{}')", item.key.replace("data.", "")));

                        match item.val_type.as_ref() {
                            "number" => {
                                ord_str.push("::NUMERIC".to_string());
                            }
                            _ => {}
                        }
                    } else {
                        ord_str.push(format!("r.{}", item.key));
                    }
                }
            }

            ord_str.push(format!("{}", item.dir));

            orders.push(ord_str.join(" "));
        }

        wheres.push(format!("r.fid = '{}'", form.uuid.to_string()));

        if wheres.len() > 0 {
            sql.push("WHERE".to_string());
            sql_count.push("WHERE".to_string());

            let mut sql_f = true;
            for item in wheres {
                if sql_f != true {
                    sql.push("AND".to_string());
                    sql_count.push("AND".to_string());
                }
                sql.push(item.clone());
                sql_count.push(item.clone());
                sql_f = false;
            }
        }

        /*

        if orders.len() > 0 {
            sql.push("ORDER BY".to_string());
            sql.push(orders.join(", "));
        }
        */

        sql.push(format!("LIMIT {}", query.limit));
        sql.push(format!("OFFSET {}", query.offset));

        let c_sql = sql.join("\n");
        let c_sql_count = sql_count.join("\n");

        eprintln!("{}", c_sql);
        let mut stmt = conn.prepare(&c_sql)?;

        let rows = stmt.query_map(NO_PARAMS, |row| {
            let mut meta: HashMap<String, Value> = HashMap::new();
            for mf in &meta_fields {
                let val: Option<String> = row.get(mf.clone().as_str());
                if let Some(c) = val {
                    meta.insert(mf.clone(), Value::from(c));
                }
            }

            let mut rec = Record::from(row);
            rec.metadata = Some(meta);
            rec
        }).unwrap();

        let results: Vec<Value> = rows.map(|x| json::to_value(x.unwrap()).unwrap()).collect();

        let counter: i32 = conn.query_row(&c_sql_count, NO_PARAMS, |row| {
            row.get(0)
        }).unwrap();

        let qr = QueryResult {
            results,
            count: counter,
        };

        Ok(qr)
    }
}
