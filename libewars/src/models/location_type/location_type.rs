use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;

use rusqlite::{NO_PARAMS, Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Insertable, Updatable, Deletable, Creatable};
use crate::models::{EventLogEntry, LocationTypeUpdate, LocationTypeCreate};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationType {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub description: String,
    pub restrictions: Option<HashMap<String, Value>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,

}



#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for LocationType {
    fn from(row: &SqliteRow) -> Self {
        LocationType {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: opt_value!(row, "name").unwrap(),
            description: row.get("description"),
            restrictions: opt_value!(row, "restrictions"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for LocationType {
    fn from(row: PostgresRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        LocationType {
            uuid: row.get("uuid"),
            name,
            description: row.get("description"),
            restrictions: opt_field_json!(row, "restrictions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO location_types
    (uuid, name, description, restrictions, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8);
"#;


static SQLITE_DELETE_LOC_TYPE: &'static str = r#"
    DELETE FROM location_types WHERE uuid = ?;
"#;

static SQLITE_GET_ALL_LOC_TYPES: &'static str = r#"
    SELECT * FROM location_types;
"#;

impl Insertable<&SqliteConnection> for LocationType {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;
        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &json::to_value(&self.name).unwrap(),
            &self.description,
            &in_opt_value!(&self.restrictions),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => Err(format_err!("{:?}", err))
        }
    }
}


