use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;

use rusqlite::{Connection as SqliteConnection};
use rusqlite::types::{ToSql};

use crate::types::PgConnection;
use crate::models::{Assignment};
use crate::traits::{Creatable};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AssignmentCreate {
    pub uid: Uuid,
    pub fid: Uuid,
    pub lid: Option<Uuid>,
    pub assign_type: String,
    pub assign_group: Option<String>,
    pub status: String,
}


static PG_CREATE: &'static str = r#"
    INSERT INTO {SCHEMA}.assignments
    (uid, status, fid, lid, assign_group, assign_type, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
"#;

static PG_CREATE_RETURNING: &'static str = r#"
    INSERT INTO {SCHEMA}.assignments
    (uid, status, fid, lid, assign_group, assign_type, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *;
"#;

#[cfg(feature = "with-postgres")]
impl Creatable<&PgConnection> for AssignmentCreate {
    type Item = Assignment;

    fn create(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_CREATE.replace("{SCHEMA}", &tki), &[
            &self.uid,
            &self.status,
            &self.fid,
            &self.lid,
            &self.assign_group,
            &self.assign_type,
            &uid,
            &uid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn create_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_CREATE_RETURNING.replace("{SCHEMA}", &tki), &[
            &self.uid,
            &self.status,
            &self.fid,
            &self.lid,
            &self.assign_group,
            &self.assign_type,
            &uid,
            &uid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Assignment::from(row))
                } else {
                    bail!("ERR_NOT_FOUND");
                }
            },
            Err(err) => bail!(err)
        }
    }
}
