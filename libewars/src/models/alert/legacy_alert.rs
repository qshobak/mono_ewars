use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::models::{Alert};
use crate::types::{Numeric, IndicatorSource};
use crate::utils::*;
use crate::traits::{Insertable, Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyAlert(HashMap<String, Value>)

impl Upgrade<&SqliteConnection> for LegacyAlert {
    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemented");
    }
}

