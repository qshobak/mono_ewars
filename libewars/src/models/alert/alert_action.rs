use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlertAction {
    pub uuid: Uuid,
    pub stage: String,
    pub outcome: Option<String>,
    pub outcome_comments: Option<String>,
    pub verification_outcome: Option<String>,
    pub verification_comments: Option<String>,
    pub risk: Option<(String, (i32, i32))>,
    pub exposure_assessment: Option<String>,
    pub hazard_assessment: Option<String>,
    pub context_assessment: Option<String>,
}
