use std::collections::HashMap;

use failure::Error;
use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;

use crate::models::{Query, QueryResult};

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::PgConnection;
use crate::traits::{Queryable, Insertable};
use crate::types::{SqlitePoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Document {
    pub uuid: Uuid,
    pub template_name: HashMap<String, String>,
    pub instance_name: HashMap<String, String>,
    pub status: String,
    pub version: i32,
    pub theme: String,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<HashMap<String, Value>>,
    pub generation: HashMap<String, Value>,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Option<HashMap<String, Value>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for Document {
    fn from(row: PostgresRow) -> Self {
        let template_name: HashMap<String, String> = json::from_value(row.get("template_name")).unwrap();
        let instance_name: HashMap<String, String> = json::from_value(row.get("instance_name")).unwrap();
        let data: Option<HashMap<String, Value>> = opt_field_value!(row, "data");
        let permissions: Option<HashMap<String, Value>> = opt_field_value!(row, "permissions");

        Document {
            uuid: row.get("uuid"),
            template_name,
            instance_name,
            status: row.get("status"),
            version: row.get("version"),
            theme: row.get("theme"),
            layout: row.get("layout"),
            content: row.get("content"),
            data,
            generation: opt_field_json!(row, "generation").unwrap(),
            orientation: row.get("orientation"),
            template_type: row.get("template_type"),
            permissions,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            // optional fields
            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Document {
    fn from(row: &SqliteRow) -> Self {
        Document {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            template_name: opt_value!(row, "template_name").unwrap(),
            instance_name: opt_value!(row, "instance_name").unwrap(),
            status: row.get("status"),
            version: row.get("version"),
            theme: row.get("theme"),
            layout: row.get("layout"),
            content: row.get("content"),
            data: opt_value!(row, "data"),
            generation: opt_value!(row, "generation").unwrap(),
            orientation: row.get("orientation"),
            template_type: row.get("template_type"),
            permissions: opt_value!(row, "permissions"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO documents
    (uuid, template_name, instance_name, status, version, theme, layout, content, data, generation, orientation, template_type, permissions, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17);
"#;

#[cfg(feature = "with-sqlite")]
impl Insertable<&SqliteConnection> for Document {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &json::to_value(&self.template_name).unwrap(),
            &json::to_value(&self.instance_name).unwrap(),
            &self.status,
            &self.version,
            &self.theme,
            &in_opt_value!(&self.layout),
            &self.content,
            &in_opt_value!(&self.data),
            &json::to_value(&self.generation).unwrap(),
            &self.orientation,
            &self.template_type,
            &in_opt_value!(&self.permissions),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

static PG_QUERY: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email as creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static PG_COUNT: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    
"#;

static PG_GET_BY_ID: &'static str = r#"
    SELECT * FROM {SCHEMA}.documents WHERE uuid = ?;
"#;


static SQL_GET_BY_ID: &'static str = r#"
    SELECT * FROM documents WHERE uuid = ?;
"#;

#[cfg(feature = "with-sqlite")]
impl Queryable<&SqliteConnection> for Document {
    type Item = Document;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query_row(&SQL_GET_BY_ID, &[&id.to_string() as &ToSql], |row| {
            Document::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => bail!(err)
        }
    }
}


#[cfg(feature = "with-postgres")]
impl Queryable<&PgConnection> for Document {
    type Item = Document;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &PgConnection, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query(&PG_GET_BY_ID.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Some(Document::from(row)))
                } else {
                    Ok(Option::None)
                }
            },
            Err(err) => bail!(err)
        }
    }

    fn query(conn: &PgConnection, tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        query_fn!(conn, query, PG_QUERY, PG_COUNT, tki, Document)
    }
}
