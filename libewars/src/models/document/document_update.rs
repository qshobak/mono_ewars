use serde_json::Value;
use serde_json as json;

use failure::Error;
use uuid::Uuid;

use crate::types::PgConnection;
use crate::models::{Document};
use crate::traits::{Updatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DocumentUpdate {
    pub uuid: Uuid,
    pub template_name: Value,
    pub instance_name: Value,
    pub status: String,
    pub theme: Option<String>,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub generation: Value,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Value,
}

static PG_UPDATE: &'static str = r#"
    UPDATE {SCHEMA}.documents
        SET template_name = ?,
            instance_name = ?.
            status = ?,
            theme = ?,
            layout = ?,
            content = ?,
            data = ?,
            generation = ?,
            orientation = ?,
            template_type = ?,
            permissions = ?,
            modified = NOW(),
            modified_by = ?
        WHERE uuid = ?;
"#;

static PG_UPDATE_RETURNING: &'static str = r#"
    UPDATE {SCHEMA}.documents
        SET template_name = ?,
            instance_name = ?.
            status = ?,
            theme = ?,
            layout = ?,
            content = ?,
            data = ?,
            generation = ?,
            orientation = ?,
            template_type = ?,
            permissions = ?,
            modified = NOW(),
            modified_by = ?
        WHERE uuid = ? RETURNING *;
"#;

impl Updatable<&PgConnection> for DocumentUpdate {
    type Item = Document;

    fn update(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_UPDATE.replace("{SCHEMA}", tki), &[
            &self.template_name,
            &self.instance_name,
            &self.status,
            &self.theme,
            &self.layout,
            &self.content,
            &self.data,
            &self.generation,
            &self.orientation,
            &self.template_type,
            &self.permissions,
            &uid,
            &self.uuid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }

    }

    fn update_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_UPDATE_RETURNING.replace("{SCHEMA}", &tki), &[
            &self.template_name,
            &self.instance_name,
            &self.status,
            &self.theme,
            &self.layout,
            &self.content,
            &self.data,
            &self.generation,
            &self.orientation,
            &self.template_type,
            &self.permissions,
            &uid,
            &self.uuid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Document::from(row))
                } else {
                    bail!("ERR");
                }
            },
            Err(err) => bail!(err)
        }
    }
}
