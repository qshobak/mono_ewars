use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Indicator {
    pub uuid: Uuid,
    pub group_id: Option<Uuid>,
    pub name: String,
    pub status: String,
    pub description: Option<String>, 
    pub itype: Option<String>,
    pub icode: Option<String>,
    pub permissions: Option<HashMap<String, Value>>,
    pub definition: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for Indicator {
    fn from(row: PostgresRow) -> Self {
        let permissions: Option<HashMap<String, Value>> = match row.get_opt("permissions") {
            Some(res) => {
                if let Ok(r) = res {
                    Some(json::from_value(r).unwrap())
                } else {
                    Option::None
                }
            }, 
            None => Option::None
        };
        
        Indicator {
            uuid: row.get("uuid"),
            group_id: row.get("group_id"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            itype: row.get("itype"),
            icode: row.get("icode"),
            permissions,
            definition: row.get("definition"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Indicator {
    fn from(row: &SqliteRow) -> Self {
        Indicator {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            group_id: opt_uuid!(row, "group_id"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            itype: row.get("itype"),
            icode: row.get("icode"),
            permissions: opt_value!(row, "permissions"),
            definition: opt_value!(row, "definition"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO indicators 
    (uuid, group_id, name, status, description, itype, icode, permissions, definition, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13);
"#;

impl Insertable<&SqliteConnection> for Indicator {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &in_opt_uuid!(&self.group_id),
            &self.name,
            &self.status,
            &self.description,
            &self.itype,
            &self.icode,
            &in_opt_value!(&self.permissions),
            &in_opt_value!(&self.definition),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

