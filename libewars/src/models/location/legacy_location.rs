use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::models::{Location};
use crate::types::{Numeric};
use crate::utils::*;
use crate::traits::{Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyLocation(HashMap<String, Value>);

impl Upgrade<&SqliteConnection> for LegacyLocation {
    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {

    }
}
