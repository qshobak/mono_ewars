use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

#[derive(Debug, Deserialize)]
pub struct LegacyReporting(HashMap<String, Value>);

impl Upgrade<&SqliteConnection> for LegacyReportng {
    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {

    }
}
