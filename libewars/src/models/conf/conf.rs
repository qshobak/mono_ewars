use uuid::Uuid;
use chrono::prelude::*;
use serde_json as json;
use serde_json::Value;
use failure::Error;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Conf {
    pub uuid: Uuid,
    pub key: String,
    pub value: Value,
    pub grp: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}


impl<'a> From<PostgresRow<'a>> for Conf {
    fn from(row: PostgresRow) -> Self {
        Conf {
            uuid: row.get("uuid"),
            key: row.get("key"),
            value: row.get("value"),
            grp: row.get("grp"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Conf {
    fn from(row: &SqliteRow) -> Self {
        Conf {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            key: row.get("key"),
            value: row.get("value"),
            grp: row.get("grp"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO conf 
    (uuid, key, value, grp, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8);
"#;

impl Insertable<&SqliteConnection> for Conf {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.key,
            &self.value,
            &self.grp,
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

