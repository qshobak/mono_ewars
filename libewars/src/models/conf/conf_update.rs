use serde_json::Value;
use uuid::Uuid;

use crate::types::PgConnection;
use crate::models::{Conf};
use crate::traits::{Updatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ConfUpdate {
    pub uuid: Uuid,
    pub value: Value,
}

static PG_UPDATE: &'static str = r#"
    UPDATE {SCHEMA}.conf 
        set value = ?,
            modified = NOW(),
            modified_by = ?
    WHERE uuid = ?;
"#;

static PG_UPDATE_RETURN: &'static str = r#"
    UPDATE {SCHEMA}.conf 
        set value = ?,
            modified = NOW(),
            modified_by = ?
    WHERE uuid = ? RETURNING *;
"#;

impl Updatable<&PgConnection> for ConfUpdate {
    type Item = Conf;

    fn update(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_UPDATE.replace("{SCHEMA}", &tki), &[
            &self.value,
            &uid,
            &self.uuid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn update_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_UPDATE_RETURN.replace("{SCHEMA}", &tki), &[
            &self.value,
            &uid,
            &self.uuid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Conf::from(row))
                } else {
                    bail!("ERR_NOT_FOUND");
                }
            },
            Err(err) => bail!(err)
        }

    }
}
