use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;

use rusqlite::{Connection as SqliteConnection, Row as SqliteRow};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Insertable};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Task {
    pub uuid: Uuid,
    pub status: String,
    pub task_type: String,
    pub data: Value,
    pub actions: Option<Value>,
    pub version: i32,
    pub form: Option<Value>,
    pub roles: Vec<String>,
    pub location: Option<Uuid>,
    pub outcome: Option<Value>,
    pub actioned: Option<DateTime<Utc>>,
    pub actioned_by: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub actioner: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for Task {
    fn from(row: PostgresRow) -> Self {
        Task {
            uuid: row.get("uuid"),
            status: row.get("status"),
            task_type: row.get("task_type"),
            data: row.get("data"),
            actions: row.get("actions"),
            version: row.get("version"),
            form: row.get("form"),
            roles: row.get("roles"),
            location: row.get("location"),
            outcome: row.get("outcome"),
            actioned: row.get("actioned"),
            actioned_by: row.get("actioned_by"),
            created: row.get("created"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            actioner: opt_field!(row, "actioner"),
        }
    }
}


impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Task {
    fn from(row: &SqliteRow) -> Self {
        Task {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            status: row.get("status"),
            task_type: row.get("task_type"),
            data: row.get("data"),
            actions: row.get("actions"),
            version: row.get("version"),
            form: row.get("form"),
            roles: opt_value!(row, "roles").unwrap(),
            location: opt_uuid!(row, "location"),
            outcome: row.get("outcome"),
            actioned: row.get("actioned"),
            actioned_by: opt_uuid!(row, "actioned_by"),
            created: row.get("created"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            actioner: opt_string!(row, "actioner"),
        }

    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO tasks 
    (uuid, status, task_type, data, actions, version, form, context, roles, location, outcome, actioned, actioned_by, created, modified, modified_by)
    VALUES (:uuid, :status, :task_type, :data, :actions, :version, :form, :context, :roles, :location, :outcome, :actioned, :actioned_by, :created, :modified, :modified_by);
"#;

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT * FROM tasks WHERE uuid = :uuid;
"#;

impl Insertable<&SqliteConnection> for Task {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute_named(&[
            (":uuid", &self.uuid.to_string() as &ToSql),
            (":status", &self.status),
            (":task_type", &self.task_type),
            (":data", &json::to_value(&self.data).unwrap()),
            (":actions", &in_opt_value!(&self.actions)),
            (":version", &self.version),
            (":form", &in_opt_value!(&self.form)),
            (":roles", &json::to_value(&self.roles).unwrap()),
            (":location", &in_opt_uuid!(&self.location)),
            (":outcome", &in_opt_value!(&self.outcome)),
            (":actioned", &self.actioned),
            (":actioned_by", &in_opt_uuid!(&self.actioned_by)),
            (":modified", &self.modified),
            (":modified_by", &in_opt_uuid!(&self.modified_by)),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                dbg!(&err);
                bail!("{:?}", err)
            }
        }
    }
}

static SQLITE_CLOSE_TASK: &'static str = r#"
    UPDATE tasks
        SET status = 'CLOSED',
            outcome = :outcome,
            actioned_by = :actioner,
            actioned = :cur_time,
            modified = :modified_time,
            modifier = :modifier
    WHERE uuid = :uuid;
"#;

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO tasks
    (uuid, status, task_type, data, actions, version, form, context, roles, location, created, modified, modified_by)
    VALUES (:uuid, :status, :task_type, :data, 
"#;

#[cfg(feature = "with-sqlite")]
impl Task {
    // Close the task
    pub fn close(&self, conn: &SqliteConnection, task_type: &String, outcome: &Value, closer: &Uuid) -> Result<(), Error> {
        let cur_time: DateTime<Utc> = Utc::now();
        match &conn.execute_named(&SQLITE_CLOSE_TASK, &[
            (":outcome", &outcome),
            (":actioned_by", &closer.to_string()),
            (":actioned", &cur_time),
            (":modified_time", &cur_time),
            (":modifier", &closer.to_string()),
            (":uuid", &self.uuid.to_string()),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                bail!("Error");
            }
        }
    }
    
    // Create a new task
    pub fn create_task(&self, conn: &SqliteConnection, data: &Value, context: &Value, uid: &Uuid) -> Result<(), Error> {
        unimplemented!()
    }
}

