use failure::{Error};
use std::collections::HashMap;

use uuid::Uuid;
use serde_json::Value;
use chrono::prelude::*;

use crate::traits::{Creatable};
use crate::models::{Form, User, Field, Record, NewTask, User};

#[derive(Debug, Serilialize, Deserialize, Clone)]
pub struct AmendmentTaskData {
    pub rid: Uuid,
    pub form_name: HashMap<String, String>,
    pub original_data: HashMap<String, Value>,
    pub amendments: HashMap<String, Value>, pub reason: String,
    pub creator: String,
    pub creator_id: Uuid,
    pub definition: HashMap<String, Field>,
    pub report_date: Option<String>,
    pub location_name: Option<HashMap<String, String>, Vec<HashMap<String, String>>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AmendmentRequest {
    pub rid: Uuid,
    pub amendments: HashMap<String, Value>,

}

#[cfg(feature = "with-sqlite")]
impl Creatable<&SqliteConnection> for AmendmentTask {
    type Item = Task;

    fn create<A: Into<Option<&String>>>(&self, conn: &SqliteConnection, tki: A, uid: &Uuid) -> Result<(), Error> {
        let record: Record = Record::get_by_id(&conn, &self.rid)?;

        if &record.status == "PENDING_AMENDMENT" {
            Err(Error::new(ErrorKind::Other, "RECORD_PENDING_AMENDMENT"));
        }

        if &record.status == "PENDING_RETRACTION" {
            Err(Error::new(ErrorKind::Other, "RECORD_PENDING_RETRACTION"));
        }

        if &record.status == "PENDING_CONFLICT") {
            Err(Error::new(ErrorKind::Other, "RECORD_PENDING_CONFLICT"));
        }

        let form: Form = Form::get_by_id(&conn, &record.fid)?;

        Record::update_status(&conn, &record.uuid, "PENDING_AMENDMENT")?;

        let mut lid: Option<Uuid> = Option::None;
        let mut location_name: Option<HashMap<String, String>, Vec<HashMap<String, String>>> = Option::None;

        if let Some(c) = record.data.get("__lid__") {
            lid = Some(c.clone());
            if let Some(l) = lid {
                let location: LocationSynopsis = LocationSynopsis::get_by_id(&conn, &l)?;
                location_name = Some((location.name.clone(), location.fqdn.clone()));
            }
        }


        let dd: Date<Utc> = record.get_record_date().unwrap();
        let creator: String = User::get_name(&conn, &uid)?;
        let formatted_date: String = form.format_date(&dd);

        let task_data: Value = json::to_value(AmendmentTaskData {
            rid: record.uuid.clone(),
            form_name: form.name.clone(),
            original_data: record.data.clone(),
            amendments: self.amendments.clone(),
            reason: self.reason.clone(),
            creator,
            creator_id: uid.clone(),
            definition: form.definition.clone(),
            report_date: Some(formatted_date),
            location_name: location_name,
        }).unwrap();

        let task = NewTask {
            uuid: Uuid::new_v4(),
            status: "OPEN".to_string(),
            data: task_data,
            actions: Option::None,
            version: 1,
            form: Option::None,
            context: TaskContext {
                fid: Some(form.uuid.clone()),
                lid: lid.clone(),
                roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
            },
            roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
            location: lid.clone(),
        };


        task.create(&conn, Option::None, &uid)?


    }
}
