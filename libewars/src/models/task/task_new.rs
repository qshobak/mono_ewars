use failure::{Error};
use uuid::Uuid;
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;

use rusqlite::{Connection as SqliteConnection};

use crate::models::{Task};

use crate::traits::{Creatable};


#[derive(Debug, Clone)]
pub struct NewTask {
    pub uuid: Uuid,
    pub status: String,
    pub data: Value,
    pub actions: Option<Value>,
    pub version: i32,
    pub form: Option<Value>,
    pub context: TaskContext,
    pub roles: Vec<&str>,
    pub location: Option<Uuid>,
}

static SQLITE_CREATE_TASK: &'static str = r#"
    INSERT INTO tasks
    (uuid, status, data, actions, version, form, context, roles, location, created)
    VALUES (:uuid, :status, :data, :actions, :version, :form, :context, :roles, :location, :created);
"#;

#[cfg(feature = "with-sqlite")]
impl Creatable<&SliteConnection> for NewTask {
    type Item = Task;

    fn create(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let cur_time: DateTime<Utc> = Utc::now();
        match &conn.execute_named(&SQLITE_CREATE_TASK, &[
          (":uuid", &self.uuid.to_string()),
          (":status", &"OPEN"),
          (":data", &json::to_value(self.data.clone()).unwrap()),
          (":actions", &Option::None),
          (":version", &1),
          (":form", &Option::None),
          (":context", &self.context),
          (":roles", &json::to_value(self.roles.clone()).unwrap()),
          (":location", &self.location),
          (":created", &cur_time),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                bail!(format!("{:?}", err));
            }
        }
    }
}
