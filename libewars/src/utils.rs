use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::models::{Order, Query};


#[macro_use]
macro_rules! opt_field {
    ($row: expr, $name: expr) => {
        {
            match $row.get_opt($name) {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_field_json {
    ($row: expr, $name: expr) => {
        {
            match $row.get_opt($name) {
                Some(Ok(res)) => {
                    Some(json::from_value(res).unwrap())
                },
                Some(Err(err)) => {
                    dbg!(err);
                    Option::None
                }
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_field_value {
    ($row: expr, $name: expr) => {
        {
            let v_data: Option<Value> = $row.get($name);
            match v_data {
                Some(res) => {
                    if res.is_null() {
                        Option::None
                    } else {
                    Some(json::from_value(res).unwrap())
                    }
                }
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_uuid {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            Some(Uuid::parse_str(&c).unwrap())
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_int {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Integer(c) => {
                            Some(c)
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_value {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            eprintln!("{}", c);
                            match c {
                                "null" => Option::None,
                                "" => Option::None,
                                _ => Some(json::from_str(&c).unwrap())
                            }
                        },
                        ValueRef::Null => Option::None,
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_string {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            Some(c.to_string())
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

macro_rules! get_row {
    ($sql: expr, $conn: expr, $model: ident, $args: expr) => {
        {
            match $conn.query_row($sql, $args, |row| {
                $model::from(row)
            }) {
                Ok(res) => {
                    Some(res)
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! in_opt_value {
    ($val: expr) => {
        {
            if let Some(c) = $val {
                Some(json::to_value(c.clone()).unwrap())
            } else {
                Option::None
            }
        }
    }
}

macro_rules! in_opt_uuid {
    ($val: expr) => {
        {
            if let Some(c) = $val {
                Some(c.clone().to_string())
            } else {
                Option::None
            }
        }
    }
}


macro_rules! pg_get_row {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    if let Some(c) = rows.iter().next() {
                        Some($model::from(c))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! query_fn {
    ($conn: expr, $query: expr, $sql_query_base: expr, $sql_count_base: expr, $tki: expr, $model:ident) => {
        {
            use crate::utils::*;
            use crate::models::{QueryResult};
            let mut sql: String = $sql_query_base.to_string();
            let mut sql_count: String = $sql_count_base.to_string();

            let mut wheres: Vec<String> = Vec::new();

            for filter in &$query.filters {
                if let Some(val) = &filter.value {
                    let mut s_key: String = format_key(&filter.key);
                    let mut s_val = val.clone();
                    let s_cmp = get_comparator(&filter.cmp, &s_val);

                    s_key = format_key(&filter.key);
                    s_key = format!("r.{}", s_key);

                    let mut flt_str: String = format!("{} {}", &s_key, &s_cmp);
                    wheres.push(flt_str.to_owned());
                }
            }

            let orderings: Vec<String> = apply_ordering(&$query.orders);

            if wheres.len() > 0 {
                join_filters(&mut sql, &mut sql_count, &wheres);
            }

            if orderings.len() > 0 {
                join_orders(&mut sql, &orderings);
            }

            sql.push_str(&format!(" LIMIT {} ", $query.limit));
            sql.push_str(&format!(" OFFSET {} ", $query.offset));

            let records: Vec<$model> = get_vec_from!(&$conn, &sql, $model, &$tki);
            let records: Vec<Value> = records.iter().map(|x| json::to_value(x).unwrap()).collect();

            let count: i32 = get_scalar!(&$conn, &sql_count, &$tki);

            let result = QueryResult {
                results: records,
                count,
            };

            Ok(result)
        }
    }
}

macro_rules! get_scalar {
    ($conn: expr, $sql: expr, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    0
                }
            }
        }
    }
}


macro_rules! get_vec_from {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    rows.iter().map(|x| $model::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        }
    }
}

macro_rules! get_single_from {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $id: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[&$id]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        Some($model::from(row))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

#[macro_export]
macro_rules! get_single_from_args {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    if let Some(c) = rows.iter().next() {
                        Some($model::from(c))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! get_count_result {
    ($conn: expr, $sql: expr, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    } 
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    0
                }
            }
        }
    }
}

macro_rules! get_vec_from_args {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    rows.iter().map(|x| $model::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }

        }
    }
}

pub fn format_key(key: &str) -> String {
    if key.contains(".") {
        // We split it apart, then create a nested call
        // using the first slice item as the column name
        let res = String::new();
        let vec_s: Vec<String> = key.split(".").map(|s| s.to_string()).collect();

        let mut key: String = vec_s[0].to_string();
        let mut path: String = vec_s[1..].join(".").to_string();

        format!("{}->>'{}'", &key, &path).to_string()
    } else {
        key.to_string()
    }
}


// Get the comparator for a filter query
pub fn get_comparator(cmp: &str, val: &Value) -> String {
    let result: String = match cmp.as_ref() {
        "nnull" => {
            String::from("IS NOT NULL")
        },
        "null" => {
            String::from("IS NULL")
        },
        "in" => {
            if val.is_array() {
                let vals: Vec<String> = json::from_value(val.clone()).unwrap();
                let flt_str: String = vals.join("','");
                format!("::TEXT = ANY(ARRAY['{}']::TEXT[])", &flt_str)
            } else {
                format!("::TEXT = ANY(ARRAY['{}']::TEXT[])", &val.as_str().unwrap_or(""))
            }
        },
        "nin" => {
            if val.is_array() {
                let vals: Vec<String> = json::from_value(val.clone()).unwrap();
                let flt_str: String = vals.join("','");
                format!("!= ANY(ARRAY['{}'])", &flt_str)
            } else {
                format!("!= ANY(ARRAY['{}'])", val.as_str().unwrap_or(""))
            }
        },
        "contains" => {
            if val.is_array() {
                let vals: Vec<String> = json::from_value(val.clone()).unwrap();
                let flt_str: String = vals.join("','");
                format!("@> ARRAY['{}']", flt_str)
            } else {
                format!("@> ARRAY['{}']", val.as_str().unwrap_or(""))
            }
        }
        "ncontains" => {
            if val.is_array() {
                let vals: Vec<String> = json::from_value(val.clone()).unwrap();
                let flt_str: String = vals.join("','");
                format!("@> ARRAY['{}']", flt_str)
            } else {
                format!("@> ARRAY['{}']", val.as_str().unwrap_or(""))
            }
        },
        "under" => {
            format!("::TEXT[] @> ARRAY['{}']::TEXT[]", &val)
        },
        "nunder" => {
            format!("::TEXT[] @> ARRAY['{}']::TEXT[]", &val)
        },
        "like" => {
            format!("ILIKE '%{}%'", &val)
        },
        "nlike" =>{
            format!("NOT ILIKE '%{}%'", &val)
        },
        _ => {
            if val.is_string() {
                format!("= '{}'", val.as_str().unwrap_or(""))
            } else {
                format!("= {}", val.as_f64().unwrap_or(-1.0))
            }
        }
    }.to_string();

    result
}

pub fn join_filters(sql: &mut String, sql_count: &mut String, filters: &Vec<String>) {
    sql.push_str("WHERE ");
    sql_count.push_str("WHERE ");

    let mut sql_f = true;
    for item in filters {
        if sql_f != true {
            sql.push_str(" AND ");
            sql_count.push_str(" AND ");
        }
        sql.push_str(&item);
        sql_count.push_str(&item);
        sql_f = false;
    }
}

pub fn join_orders(sql: &mut String, orders: &Vec<String>) {
    sql.push_str(" ORDER BY ");
    sql.push_str(&orders.join(", ").to_string());
}

fn split_out(data: &String) -> String {
    let parts: Vec<&str> = data.split(".").collect::<Vec<&str>>();
    let r_string: Vec<String> = parts.iter().map(|x| x.to_string()).collect();
    let result: String = parts[1..].join(".").to_string();
    result
}

pub fn apply_ordering(orders: &Vec<Order>) -> Vec<String> {
    let mut results: Vec<String> = Vec::new();
    for order in orders.iter() {
        let mut r_key = order.key.clone();
        let el_type = order.val_type.clone();
        let dir = order.dir.clone();

        if r_key.contains("data.") {
            let mut new_key: String = split_out(&r_key);
            r_key = format!("r.data->>'{}'", new_key);

            match el_type.as_ref() {
                "date" => {
                    r_key = format!("CAST({} AS DATE)", r_key);
                },
                "number" => {
                    r_key = format!("CAST({} AS NUMERIC)", r_key);
                },
                _ => {}
            }
        } else {
            r_key = format!("r.{}", r_key);
        }

        let mut ord_str: String = String::new();
        let mut k_dir: String = dir.clone();
        if k_dir.contains(":") {
            let data: Vec<String> = k_dir.split(":").map(|x| x.to_string()).collect();
            k_dir = data[0].clone();
        }

        ord_str.push_str(&r_key);
        ord_str.push_str(" ");
        ord_str.push_str(&k_dir);
        results.push(ord_str.to_owned());
    }
    results
}
