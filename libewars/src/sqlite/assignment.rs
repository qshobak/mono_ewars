use uuid::Uuid;
use rusqlite::{Row, Connection};

use crate::models::{Assigment};
use crate::traits::{Queryable, CreateBool, Create, Deletable, Updatable};
use crate::utils::*;

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Assignment {
    fn from(row: &Row) -> Self {
        Assignment {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            uid: opt_uuid!(row, "uid").unwrap(),
            lid: opt_uuid!(row, "lid"),
            fid: opt_uuid!(row, "fid"),
            assign_type: row.get("assign_type"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            asssignee: opt_string!(row, "assignee"),
            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
            form_name: opt_value!(row, "form_name"),
            location_name: opt_value!(row, "location_name"),
            definition: opt_value!(row, "definition"),
            features: opt_value!(row, "features"),
        }
    }
}

impl Assignment {
    pub fn update(conn: &Connection, data: &AssignmentUpdate) -> Self {
        let result: bool = match &conn.execute_named(&SQL_UPDATE_ASSIGNMENT, &[

        ]) {
            Ok(_) => true,
            Err(err) => {
                eprintln!("{:?}", err);
                false
            }
        }

        Assignment::get_by_id(&conn, &data.uuid)
    }

    // Get a single assignment by it's  id
    pub fn get_by_id(conn: &Connection, id: &Uuid) -> Self {
        unimplemented!()
    }
}

