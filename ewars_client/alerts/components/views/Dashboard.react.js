import CONSTANTS from "../../../common/constants";

import {
    Tab,
    Spinner,
    LinkSelect
} from "../../../common";
import Data from "./Data.react";
import Map from "./Map.react"



var Dashboard = React.createClass({
    _map: null,
    _mapLoaded: false,
    _hasNaved: false,
    _dataLoaded: false,

    getInitialState: function () {
        return {
            view: "DATA",
            filter: CONSTANTS.OPEN,
            data: [],
            confinement: null
        }
    },

    _onChangeView: function (data) {
        this.setState({
            view: data.view
        })
    },

    render: function () {
        let view;
        if (this.state.view == "MAP") view = <Map data={this.state.data} filter={this.state.filter}/>;

        if (this.state.view == "DATA") view = <Data data={this.state.data} closed={false}/>;
        if (this.state.view == "DATA_CLOSED") view = <Data data={this.state.data} closed={true}/>;

        return (
            <div className="ide-layout ide-light alert-dash-wrapper">
                <div className="ide-row" style={{maxHeight: 36}}>
                    <div className="ide-col">
                        <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs" style={{display: "block"}}>
                            <Tab
                                label="Open Alerts"
                                icon="fa-bell"
                                active={this.state.view == "DATA"}
                                onClick={this._onChangeView}
                                data={{view: "DATA"}}/>
                            <Tab
                                label="Closed Alerts"
                                icon="fa-bell-slash"
                                active={this.state.view == "DATA_CLOSED"}
                                onClick={this._onChangeView}
                                data={{view: "DATA_CLOSED"}}/>
                            {!window.ewars.DESKTOP ?
                                <Tab
                                    label="Alert Map"
                                    icon="fa-map"
                                    active={this.state.view == "MAP"}
                                    onClick={this._onChangeView}
                                    data={{view: "MAP"}}/>
                                : null}

                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        {view}

                    </div>
                </div>
            </div>
        )
    }
});

export default Dashboard;
