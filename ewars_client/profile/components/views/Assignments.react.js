import Moment from "moment";

class AssignLocal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let label;
        if (this.props.data.start_date && this.props.data.end_date) {
            label = `${ewars.DATE(this.props.data.start_date)} to ${ewars.DATE(this.props.data.end_date)}`;
        } else {
            label = `${ewars.DATE(this.props.data.start_date)} to current`
        }

        return (
            <div className="iw-list-item">
                {this.props.data.name[1]} - {label}
            </div>
        )
    }
}

class Assignment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let items = this.props.data.locations.map(function (item) {
            return <AssignLocal data={item} assign={this.props.data}/>
        }.bind(this));

        let formName = ewars.I18N(this.props.data.form_name);

        return (
            <div className="widget">
                <div className="widget-header">
                    <span>{formName}</span>
                </div>
                <div className="body no-pad">
                    <div className="iw-list">
                        {items}
                    </div>
                </div>
            </div>
        )
    }
}

class Assignments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.user", ["ASSIGNMENTS", window.user.id, null])
            .then(function (resp) {
                this.setState({
                    assignments: resp
                })
            }.bind(this))
    }

    render() {
        let assigns = this.state.assignments.map(function (item) {
            return <Assignment data={item}/>
        }.bind(this));

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                {assigns}
            </div>
        )
    }
}

export default Assignments;
