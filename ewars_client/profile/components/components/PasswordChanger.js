import { TextField } from "../../../common/fields";

class PasswordChanger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            password: "",
            confirm_password: ""
        }
    }

    render() {
        return (
            <div className="article" style={{paddingRight: 150, paddingLeft: 30, paddingTop: 0}}>

                {this.props.error ?
                    <div className="error"><i className="fal fa-exclamation-triangle"></i> {this.props.error}</div>
                : null}

                <div className="ide-row" style={{marginBottom: 8}}>
                    <div className="ide-col"
                         style={{maxWidth: 200, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                        <label>New Password *</label>
                    </div>
                    <div className="ide-col">
                        <TextField
                            name="password"
                            placeholder="Enter new password..."
                            onUpdate={(prop, value) => {this.props.onChange(prop, value)}}
                            value={this.props.password}/>
                    </div>
                </div>

                <div className="ide-row">
                    <div className="ide-col"
                         style={{maxWidth: 200, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                        <label>Confirm New Password *</label>
                    </div>
                    <div className="ide-col">
                        <TextField
                            name="confirm_password"
                            placeholder="Confirm the new password..."
                            onUpdate={(prop, value) => {this.props.onChange(prop, value)}}
                            value={this.props.confirm_password}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default PasswordChanger;
