import { DataTable } from "../../common";
import Moment from "moment";

const COLUMNS = [
    {
        name: "title",
        config: {
            type: "text",
            label: "Outbreak Name"
        }
    },
    {
        name: "status",
        config: {
            type: "text",
            label: "Status",
        }
    },
    {
        name: "start_date",
        config: {
            type: "date",
            label: "Start date"
        },
        fmt: (val) => {
            return Moment.utc(val).local().format("LL");
        }
    },
    {
        name: "end_date",
        config: {
            type: "date",
            label: "End date"
        },
        fmt: (val) => {
            if (!val) return "";
            return Moment.utc(val).local().format("LL");
        }
    },
    {
        name: "__locations__",
        config: {
            type: "text",
            label: "Locations"
        },
        fmt: (val, row) => {
            return row.locations.length;
        }
    },
    {
        name: "__forms__",
        config: {
            type: "text",
            label: "Forms"
        },
        fmt: (val, row) => {
            return row.forms.length;
        }
    },
    {
        name: "created",
        config: {
            type: "date",
            label: "Created"
        },
        fmt: (val) => {
            console.log(val);
            return Moment.utc(val).local().format("LLL");
        }
    },
    {
        name: "modified",
        config: {
            type: "date",
            label: "Modified"
        },
        fmt: (val) => {
            return Moment.utc(val).local().format("LLL");
        }
    }
]

const ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

class ViewData extends React.Component {
    constructor(props) {
        super(props);
    }

    _queryFn = (filters, orders, limit, offset) => {
        return new Promise((resolve, reject) => {
            ewars.tx("com.sonoma.outbreaks", {filters, orders, limit, offset})
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });
        });
    };

    _onAction = (action, cell, data) => {
        switch (action) {
            case "EDIT":
                this.props.onEdit(data);
                break;
            case "DELETE":
                this._delete(data);
                break;
            default:
                break;
        }
    };

    _delete = (data) => {
        if (window.confirm("Are you sure you want to delete this outbreak?")) {
            let bl = new ewars.Blocker(null, "Deleting outbreak");
            ewars.tx("com.sonoma.outbreak.delete", [data.uuid])
                .then(res => {
                    ewars.growl("Outbreak deleted");
                    ewars.emit("RELOAD_DT");
                }).catch(err => {
                    ewars.growl("There was an error deleting this outbreak.");
                });

        }
    };

    render() {
        return (
            <DataTable
                id="DT_TRANSIENT"
                resource="outbreak"
                columns={COLUMNS}
                onCellAction={this._onAction}
                actions={ACTIONS}
                queryFn={this._queryFn}/>
        )
    }
}

export default ViewData;
