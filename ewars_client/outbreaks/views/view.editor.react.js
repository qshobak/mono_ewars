import {
    SelectField,
    TextField,
    TextAreaField,
    DateField
} from "../../common/fields";

import LocationsEditor from "../controls/c.locations.react";
import FormsEditor from "../controls/c.forms.react";

const STATUSES = [
    ['ACTIVE', 'Active'],
    ["CLOSED", "Closed"],
    ['INACTIVE', 'Inactive']
]

const FORM_ITEM_ACTIONS = [
    ["fa-caret-right", "ADD"]
]

const FORM_ADDED_ACTIONS = [
    ['fa-trash', "REMOVE"]
]

const isSet = (val) => {
    if (val == undefined) return false;
    if (val == "") return false;
    if (val == null) return false;
    return true;
}


class FormItem extends React.Component {
    _action = (action) => {
        switch (action) {
            case "ADD":
                this.props.onAdd(this.props.data);
                break;
            case "REMOVE":
                this.props.onRemove(this.props.data.uuid);
                break;
            default:
                break;
        }
    };

    render() {
        let actions = FORM_ITEM_ACTIONS;
        if (this.props.onRemove) actions = FORM_ADDED_ACTIONS;
        return (
            <div className="drag-list-item">
                <div className="drag-list-label">
                    {__(this.props.data.name)}
                </div>
                <div className="drag-list-controls">
                    <ewars.d.ActionGroup
                        actions={actions}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class FormAdder extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        ewars.tx("com.ewars.forms.accessible", null)
            .then(res => {
                this.setState({data: res});
            }).catch(err => {
                console.log(err);
            });
    }

    _add = (data) => {
        let items = this.props.data || [];
        items.push(data.uuid);
        this.props.onChange(items);
    };

    _remove = (id) => {
        let index = (this.props.data || []).indexOf(id);
        if (index >= 0) {
            let items = this.props.data;
            items.splice(index, 1);
            this.props.onChange(items);
        }
    };

    render() {
        let items = [],
            selected = [];
        this.state.data.forEach(item => {
            if (this.props.data.indexOf(item.uuid) >= 0) {
                selected.push(item);
            } else {
                items.push(item);
            }
        });
        items = items.sort((a, b) => {
            return __(a.name).localeCompare(__(b.name));
        });
        let source = items.map(item => {
            return <FormItem data={item} onAdd={this._add}/>;
        });

        selected = selected.sort((a, b) => {
            return __(a.name).localeCompare(__(b.name));
        });
        let target = selected.map(item => {
            return <FormItem data={item} onRemove={this._remove}/>;
        });

        return (
            <div className="drag-list">
                <div className="drag-list-source">
                    {source}
                </div>
                <div className="drag-list-middle">

                </div>
                <div className="drag-list-output">
                    {target}
                </div>
            </div>
        )
    }
}

class Box extends React.Component {
    render() {

        return (
            <div className="hsplit-box">
                <div className="ide-setting-label">{this.props.label}</div>
                <div className="ide-setting-control">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

class FormField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: []};
    }

    componentDidMount() {
        ewars.tx("com.ewars.forms.accessible", null)
            .then(res => {
                this.setState({data: res});
            })
            .catch(err => {
                console.log(err);
                ewars.growl("Error loading forms");
            });
    }

    render() {
        let options = this.state.data.map(item => {
            return [item.uuid, __(item.name)];
        });

        return (
            <div className="multi-field">
                <select multiple={true} name="fid" onChange={this._onChange}>
                    {this.state.data.map(item => {
                        return <option value={item.uuid}>{__(item.name)}</option>;
                    })}
                </select>
            </div>

        )
    }
}

class ViewEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: ewars.copy(this.props.data),
            view: "GENERAL"
        }

    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        });
    };

    _save = () => {
        if (!isSet(this.state.data.title)) {
            ewars.growl("Please specify a name");
            return;
        }

        if (!isSet(this.state.data.status)) {
            ewars.growl("Please specify a status");
            return;
        }

        if (!isSet(this.state.data.start_date)) {
            ewars.growl("Please specify a start date");
            return;
        }

        if (this.state.data.status == "CLOSED" && !isSet(this.state.data.end_date)) {
            ewars.growl("Please set an end date if this outbreak is closed");
            return;
        }

        let bl = new ewars.Blocker(null, "Saving change(s)");
        let com = this.state.data.uuid ? "com.sonoma.outbreak.update" : "com.sonoma.outbreak.create";
        ewars.tx(com, [this.state.data])
            .then(res => {
                bl.destroy();
                this.setState({
                    data: res
                }, () => {
                    ewars.growl("Changes saved!");
                });
            }).catch(err => {
                bl.destroy();
            });
    };

    _cancel = () => {
        this.props.onClose();
    };

    _onLocationsChange = (data) => {
        this.setState({
            data: {
                ...this.state.data,
                locations: data
            }
        });
    };

    _onFormsChange = (items) => {
        this.setState({
            data: {
                ...this.state.data,
                forms: items
            }
        });
    };


    render() {

        let view = (
            <ewars.d.Panel>
                <div className="ide-settings-content">
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="header"><i className="fal fa-cog"></i>&nbsp;General Settings</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Outbreak Name*:</div>
                                <div className="ide-setting-control">
                                    <TextField name="title" onUpdate={this._onChange} value={this.state.data.title}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Status*</div>
                                <div className="ide-setting-control">
                                    <SelectField config={{options: STATUSES}} name="status" onUpdate={this._onChange} value={this.state.data.status}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Start Date*</div>
                                <div className="ide-setting-control">
                                    <DateField name="start_date" onUpdate={this._onChange} value={this.state.data.start_date}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">End Date</div>
                                <div className="ide-setting-control">
                                    <DateField name="end_date" onUpdate={this._onChange} value={this.state.data.end_date}/>
                                </div>
                            </div>

                            <div className="vsplit-box">
                                <div className="ide-setting-label">Description</div>
                                <div className="ide-setting-control">
                                    <TextAreaField config={{}} name="description" onUpdate={this._onChange} value={this.state.data.description}/>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </ewars.d.Panel>
        );

        if (this.state.view == "LOCATIONS") {
            view = (
                <LocationsEditor data={this.state.data.locations || []} onChange={this._onLocationsChange}/>
            );
        }

        if (this.state.view == "FORMS") {
            view = <FormsEditor data={this.state.data.forms || []} onChange={this._onFormsChange}/>;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Outbreak Editor">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Save change(s)"
                            icon="fa-save"
                            onClick={this._save}/>
                        <ewars.d.Button
                            label="Cancel"
                            icon="fa-times"
                            onClick={this._cancel}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row style={{position: "relative"}}>
                    <div className="ide-col ide-relative ide-hide-overflow" style={{maxWidth: "34px"}}>
                        <div className="ide-tabs">
                            <div onClick={() => this.setState({view: "GENERAL"})}
                                className={"ide-tab " + (this.state.view == "GENERAL" ? "ide-tab-down" : "")}>
                                <i className="fal fa-cog"></i>&nbsp;General
                            </div>
                            <div
                                onClick={() => this.setState({view: "FORMS"})}
                                className={"ide-tab " + (this.state.view == "FORMS" ? "ide-tab-down" : "")}>
                                <i className="fal fa-clipboard"></i>&nbsp;Forms
                            </div>
                            <div onClick={() => this.setState({view: "LOCATIONS"})}
                                className={"ide-tab " + (this.state.view == "LOCATIONS" ? "ide-tab-down" : "")}>
                                <i className="fal fa-map-marker"></i>&nbsp;Locations
                            </div>
                        </div>
                    </div>
                    <ewars.d.Cell style={{position: "relative"}}>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewEditor;
