class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    _toggle = () => {
        this.props.onToggle(this.props.data.uuid);
    };

    render() {
        let icon = "fal fa-square";
        let style = {};
        if (this.props.forms.indexOf(this.props.data.uuid) >= 0) {
            icon = "fal fa-check-square";
            style = {color: "green"};
        }
        return (
            <div className="form-list-item" onClick={this._toggle}>
                <i style={style} className={icon}></i>&nbsp;{__(this.props.data.name)}
            </div>
        )
    }
}

class FormsEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            forms: []
        }
    }

    componentDidMount() {
        ewars.tx("com.ewars.forms.accessible", [])
            .then(res => {
                this.setState({
                    forms: res.sort((a, b) => {
                        return __(a.name).localeCompare(__(b.name));
                    })
                });
            }).catch(err => {
                ewars.growl("Error loading forms");
            });
    };

    _toggle = (uuid) => {
        console.log(uuid);
        let items = this.props.data;
        let index = items.indexOf(uuid);
        if (index >= 0) {
            items.splice(index, 1);
        } else {
            items.push(uuid);
        }
        this.props.onChange(items);
    };

    render() {
        return (
            <ewars.d.Panel>
                {this.state.forms.map(item => {
                    return <Form data={item} forms={this.props.data} onToggle={this._toggle}/>
                })}
            </ewars.d.Panel>
        )
    }
}

export default FormsEditor;
