import CONSTANTS from "../../../common/constants";

import {
    Layout, Row, Cell,
    Toolbar,
    Shade,
    Button,
    Modal,
    ButtonGroup
} from "../../../common";
import { TextAreaField } from "../../../common/fields";

var CellClick = React.createClass({
    onHover: function () {
        this.props.onHover(this.props.data, this.props.row, this.props.cell - 1);
    },

    onClick: function () {
        this.props.onClick(this.props.data, this.props.row, this.props.cell - 1);
    },

    render: function () {
        if (this.props.data.length == 1) {
            return (
                <div className="header">{this.props.data[0]}</div>
            )
        }

        var className = "cell " + this.props.data[0];
        if (this.props.active) {
            if (this.props.row == this.props.active[0] && this.props.cell == this.props.active[1] + 1) className += " active";
        }
        return (
            <div onClick={this.onClick} onHover={this.onHover} className={className}>
                <div className="icon"><i className="fal fa-check"></i></div>
            </div>
        )
    }
});

var RISK_CONTENT = {};
RISK_CONTENT[CONSTANTS.LOW] = "Managed according to standard response protocols, routine control programmes and regulation (e.g. monitoring through routine surveillance systems) ";
RISK_CONTENT[CONSTANTS.MODERATE] = "Roles and responsibility for the response must be specified. Specific monitoring or control measures required (e.g. enhanced surveillance, additional vaccination campaigns) ";
RISK_CONTENT[CONSTANTS.HIGH] = "Senior management attention needed: there may be a need to establish command and control structures; a range of additional control measures will be required some of which may have significant consequences ";
RISK_CONTENT[CONSTANTS.SEVERE] = "Immediate response required even if the event is reported out of normal working hours. Immediate senior management attention needed (e.g. the command and control structure should be established within hours); the implementation of control measures with serious consequences is highly likely ";

var LIKELIHOOD = [
    "Is expected to occur in most circumstances (e.g. probability of 95% or more) ",
    "Will probably occur in most circumstances (e.g. a probability of between 70% and 94%) ",
    "Will occur some of the time (e.g. a probability of between 30% and 69%) ",
    "Could occur some of the time (e.g. a probability of between 5% and 29%) ",
    "Could occur under exceptional circumstances (e.g. a probability of less than 5%)"
];

var CONSEQUENCES = [
    [
        "Limited impact on the affected population",
        "Little disruption to normal activities and services",
        "Routine responses are adequate and there is no need to implement additional control measures",
        "Few extra costs for authorities and stakeholders "
    ],
    [
        "Minor impact for a small population or at-risk group ",
        "Limited disruption to normal activities and services",
        "A small number of additional control measures will be needed that require minimal resources",
        "Some increase in costs for authorities and stakeholders"
    ],
    [
        "Moderate impact as a large population or at-risk group is affected",
        "Moderate disruption to normal activities and services",
        "Some additional control measures will be needed and some of these require moderate resources to implement",
        "Moderate increase in costs for authorities and stakeholders "
    ],
    [
        "Major impact for a small population or at-risk group ",
        "Major disruption to normal activities and services",
        "A large number of additional control measures will be needed and some of these require significant resources to implement",
        "Significant increase in costs for authorities and stakeholders "
    ],
    [
        "Severe impact for a large population or at-risk group ",
        "Severe disruption to normal activities and services",
        "A large number of additional control measures will be needed and most of these require significant resources to implement",
        "Serious increase in costs for authorities and stakeholders"
    ]
];


var definition = [
    [
        ["Almost Certain"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Highly Likely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Likely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Unlikely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Very Unlikely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH]
    ],
    [
        [""],
        ["Minimal"],
        ["Minor"],
        ["Moderate"],
        ["Major"],
        ["Severe"]
    ]
];

var RISK_NAMES = {
    LOW: [CONSTANTS.GREEN, "Low Risk"],
    MODERATE: [CONSTANTS.YELLOW, "Moderate Risk"],
    HIGH: [CONSTANTS.ORANGE, "High Risk"],
    SEVERE: [CONSTANTS.RED, "Very High Risk"]
};


const RiskCharacterization = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            showModal: false,
            current: null,
            showHelp: false,
            view: null,
            cell: null,
            row: null,
            risk: null
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "RISK_CHAR"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                } else {
                    this.state.current = {
                        type: "RISK_CHAR",
                        status: "DRAFT_OPEN",
                        user_id: window.user.id,
                        data: {
                            risk: null,
                            riskIndex: []
                        },
                        alert_id: this.props.data.uuid
                    }
                }
                this._isLoaded = true;

                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _setOptions: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "RISK_CHAR"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                }
                this._isLoaded = true;

                this.state.showModal = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onHelpToggle: function (state) {
        this.setState({
            showHelp: state
        })
    },

    _onChange: function (prop, value) {
        this.state.current.data[prop] = value;
        this.forceUpdate();
    },

    _cancel: function () {
        this.state.current = null;
        this.state.showModal = false;
        this.forceUpdate();
    },

    _onShadeAction: function () {
        this.setState({
            current: null,
            showModal: false
        })
    },

    _save: function () {
        var data = ewars.copy(this.state.current);

        var blocker = new ewars.Blocker(null, "Saving...");
        ewars.tx("com.ewars.alert.action.update", [this.props.data.uuid, this.state.current.uuid || null, data])
            .then(function (resp) {
                this.state.current = resp;
                blocker.destroy();
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                ewars.growl("Draft updated...");
                this.forceUpdate();
            }.bind(this))
    },

    _submit: function () {
        if (this.state.current.data.risk == "" || !this.state.current.data.risk) {
            ewars.growl("Please select a risk characterisation");
            return;
        }

        var blocker = new ewars.Blocker(null, "Submitting...");
        ewars.tx("com.ewars.alert.action.submit", [this.props.data.uuid, this.state.current.uuid || null, this.state.current])
            .then(function (resp) {
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                this.state.current = resp;
                this.state.showModal = false;
                this.forceUpdate();
                blocker.destroy();
            }.bind(this))
    },

    _onClick: function (data, row, cell) {
        var readOnly = false;
        if (this.state.current.status == "SUBMITTED" && this.props.data.outcome != "MONITOR") readOnly = true;
        if (this.state.current.status == "DRAFT" && window.user.id != this.state.current.user_id) readOnly = true;

        if (!readOnly) {
            this.state.current.data.risk = data[1];
            this.state.current.data.riskIndex = [row, cell];
            this.forceUpdate();
        }
    },

    _swapGuidance: function (prop, value) {
        this.setState({showHelp: value})
    },

    render: function () {
        var buttonText = "Start risk characterisation";
        var headerText = "Risk Characterisation - ";

        if (this.props.data.stage == CONSTANTS.RISK_CHAR) {
            if (this.props.data.stage_state == CONSTANTS.PENDING) {
                buttonText = "Start risk characterisation";
                headerText += "Pending"
            }
            if (this.props.data.stage_state == CONSTANTS.ACTIVE) {
                buttonText = "Open risk characterisation";
                headerText += "In Progress";
            }
        }

        if (this.props.data.stage == CONSTANTS.OUTCOME) {
            if (this.props.data.outcome != CONSTANTS.MONITOR) {
                buttonText = "Review risk characterisation";
                headerText += "Completed";
            } else {
                buttonText = "Update risk characterisation";
                headerText += "Monitoring";
            }
        }

        var riskTag,
            actionsContent,
            readOnly = false,
            buttons,
            matrixRows,
            likeContent,
            riskName,
            consContent;
        if (this.state.showModal) {
            var view;

            if (this.props.value == "HIGH") view = null;

            matrixRows = definition.map((row, rowIndex) => {
                let cells = row.map((cell, cellIndex) => {
                    return <CellClick onClick={this._onClick}
                                      active={this.state.current.data.riskIndex}
                                      data={cell}
                                      row={rowIndex}
                                      cell={cellIndex}/>
                });
                return (
                    <div className="risk-row">{cells}</div>
                )
            }, this);

            if (this.state.current.data.risk) actionsContent = RISK_CONTENT[this.state.current.data.risk];
            if (this.state.current.data.riskIndex != null) likeContent = LIKELIHOOD[this.state.current.data.riskIndex[0]];
            if (this.state.current.data.riskIndex != null) {
                console.log(this.state.current.data.riskIndex);
                if (this.state.current.data.riskIndex.length > 0) {
                    let list = CONSEQUENCES[this.state.current.data.riskIndex[1]].map(item => {
                        return <li>{item}</li>
                    });
                    consContent = <ul className="consequences">{list}</ul>
                }
            }

            if (this.state.current.data.risk) {
                var className = "risk-tag " + RISK_NAMES[this.state.current.data.risk][0];
                riskTag = <div className={className}>{RISK_NAMES[this.state.current.data.risk][1]}</div>
            }

            if (this.state.showModal) {
                if (this.state.current.status == "SUBMITTED" && this.props.data.outcome != "MONITOR") readOnly = true;
                if (this.state.current.status == "DRAFT" && window.user.id != this.state.current.user_id) readOnly = true;

                if (readOnly) {
                    buttons = [
                        {label: "Close", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                    ]
                } else {
                    buttons = [
                        {label: "Submit", icon: 'fa-share', onClick: this._submit, colour: "green"},
                        {label: "Save Draft", icon: CONSTANTS.ICO_SAVE, onClick: this._save, colour: "amber"},
                        {label: "Cancel", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                    ]
                }
            }

        }

        return (
            <div className="dialog">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div className="dialog-title">{headerText}</div>
                            <p>Characterise the alert to assign a level of risk, from low to very high</p>
                        </td>
                        <td width="25%">
                            <ewars.d.Button
                                colour="green"
                                onClick={this._setOptions}
                                data={{view: "RISK_CHAR"}}
                                label={buttonText}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {this.state.showModal ?
                    <Shade
                        shown={this.state.showModal}
                        toolbar={false}
                        onAction={this._onShadeAction}
                        title="Stage 3 | Risk Characterisation">
                        <Layout>
                            <Toolbar label="Stage 3 | Risk Characterisation">



                                <div className="btn-group pull-right">
                                    {buttons.map(button => {
                                        return (
                                            <ewars.d.Button
                                                icon={button.icon}
                                                label={button.label}
                                                color={button.colour}
                                                onClick={button.onClick}/>
                                        )
                                    })}
                                </div>
                            </Toolbar>
                            <Row>
                                <Cell>
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="content" style={{padding: 14}}>

                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td width="50%">
                                                        <div className="alert-stage-heading">Risk Matrix</div>
                                                        <p>Select likelihood and potential consequences of the
                                                            hazard/event in the grid
                                                            below:</p>

                                                        <div className="risk-table">
                                                            {matrixRows}
                                                        </div>
                                                    </td>
                                                    <td className="risk-description" style={{position: "relative"}}>
                                                        <div className="alert-stage-heading">Overall level of risk</div>
                                                        {actionsContent ?
                                                            <div>
                                                                {riskTag}
                                                                <div className="clearer"></div>
                                                                <label htmlFor="">Likelihood</label>
                                                                <div className="panel">{likeContent}</div>
                                                                <label htmlFor="">Consequences</label>
                                                                <div className="panel">{consContent}</div>
                                                                <label htmlFor="">Actions</label>
                                                                <div className="panel">
                                                                    {actionsContent}
                                                                </div>
                                                            </div>
                                                            : null}
                                                        {!actionsContent ?
                                                            <div className="placeholder">Please select a risk level from
                                                                the options at
                                                                left.</div>
                                                            : null}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </Cell>
                            </Row>
                        </Layout>

                    </Shade>
                    : null}
            </div>
        )
    }
});

export default RiskCharacterization;
