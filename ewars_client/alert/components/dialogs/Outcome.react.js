import CONSTANTS from "../../../common/constants";

import {
    TextAreaField
} from "../../../common/fields";
import {
    ListComponent,
    Button,
    Layout, Row, Cell,
    Toolbar,
    Shade
} from "../../../common";

var OPTIONS = [
    {id: "DISCARD", label: {en: "Discard"}},
    {id: "MONITOR", label: {en: "Monitor"}},
    {id: "RESPONSE", label: {en: "Respond"}}
];

var Outcome = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            showModal: false,
            current: null,
            showHelp: false
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "OUTCOME"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                } else {
                    this.state.current = {
                        type: "OUTCOME",
                        status: "DRAFT_OPEN",
                        user_id: window.user.id,
                        data: {
                            comments: "",
                            outcome: null
                        },
                        alert_id: this.props.data.uuid
                    }
                }
                this._isLoaded = true;

                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _setOptions: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "OUTCOME"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                }
                this._isLoaded = true;

                this.state.showModal = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onHelpToggle: function (state) {
        this.setState({
            showHelp: state
        })
    },

    _onChange: function (prop, value) {
        this.state.current.data[prop] = value;
        this.forceUpdate();
    },

    _cancel: function () {
        this.state.current = null;
        this.state.showModal = false;
        this.forceUpdate();
    },

    _onShadeAction: function() {
        this.setState({
            current: null,
            showModal: false
        })
    },

    _save: function () {
        var data = ewars.copy(this.state.current);

        var blocker = new ewars.Blocker(null, "Saving...");
        ewars.tx("com.ewars.alert.action.update", [this.props.data.uuid, this.state.current.uuid || null, data])
            .then(function (resp) {
                this.state.current = resp;
                blocker.destroy();
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                ewars.growl("Draft updated...");
                this.forceUpdate();
            }.bind(this))
    },

    _submit: function () {
        if (this.state.current.data.outcome == "" || !this.state.current.data.outcome) {
            ewars.growl("Please select a valid outcome");
            return;
        }

        var blocker = new ewars.Blocker(null, "Submitting...");
        ewars.tx("com.ewars.alert.action.submit", [this.props.data.uuid, this.state.current.uuid || null, this.state.current])
            .then(function (resp) {
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                this.state.current = resp;
                this.state.showModal = false;
                this.forceUpdate();
                blocker.destroy();
            }.bind(this))
    },

    render: function () {
        var buttonText = "Start outcome";
        var headerText = "Outcome - ";

        if (this.props.data.stage == CONSTANTS.OUTCOME) {
            if (this.props.data.stage_state == CONSTANTS.PENDING) {
                buttonText = "Start outcome";
                headerText += "Pending"
            }
            if (this.props.data.stage_state == CONSTANTS.ACTIVE) {
                buttonText = "Open outcome";
                headerText += "In Progress";
            }
            if (this.props.data.stage_state == CONSTANTS.COMPLETED) {
                buttonText = "Review outcome";
                headerText += "Completed"
            }

            if (this.props.data.stage_state == CONSTANTS.MONITOR) {
                buttonText = "Update Outcome";
                headerText += "Monitoring";
            }
        }

        var buttons,
            readOnly = false;
        if (this.state.showModal) {
            if (this.state.current.status == "SUBMITTED" && this.props.data.outcome != "MONITOR") readOnly = true;
            if (this.state.current.status == "DRAFT" && window.user.id != this.state.current.user_id) readOnly = true;
            if (this.props.data.state == CONSTANTS.CLOSED) readOnly = true;

            if (readOnly) {
                buttons = [
                    {label: "Close", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                ]
            } else {
                buttons = [
                    {label: "Submit", icon: 'fa-share', onClick: this._submit, colour: "green"},
                    {label: "Save Draft", icon: CONSTANTS.ICO_SAVE, onClick: this._save, colour: "amber"},
                    {label: "Cancel", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                ]
            }
        }


        return (
            <div className="dialog">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div className="dialog-title">{headerText}</div>
                            <p>Designate a final outcome to the alert.</p>
                        </td>
                        <td width="25%">
                            <ewars.d.Button
                                onClick={this._setOptions}
                                data={{view: "OUTCOME"}}
                                colour="green"
                                label={buttonText}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {this.state.showModal ?
                    <Shade
                        toolbar={false}
                        onAction={this._onShadeAction}
                        shown={this.state.showModal}>
                        <Layout>
                            <Toolbar label="Stage 4 | Outcome">
                                <div className="btn-group pull-right">
                                    {buttons.map(btn => {
                                        return (
                                            <ewars.d.Button
                                                label={btn.label}
                                                icon={btn.icon}
                                                color={btn.colour}
                                                onClick={btn.onClick}/>
                                        )
                                    })}
                                </div>
                            </Toolbar>
                            <Row>
                                <Cell>
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="article" style={{padding: 14}}>

                                            <div className="clearer"></div>

                                            <div className="alert-stage-heading">Outcome</div>
                                            <p>Please select an outcome for the alert below</p>

                                            <ListComponent
                                                items={OPTIONS}
                                                name="outcome"
                                                readOnly={readOnly}
                                                value={this.state.current.data.outcome}
                                                onUpdate={this._onChange}/>

                                            <div className="clearer" style={{height: 16}}></div>

                                            <div className="alert-stage-heading">Comments</div>
                                            <p>Add any other relevant comments</p>

                                            <TextAreaField
                                                name="comments"
                                                onUpdate={this._onChange}
                                                readOnly={readOnly}
                                                value={this.state.current.data.comments}
                                                config={{i18n: false}}/>

                                        </div>
                                    </div>
                                </Cell>
                            </Row>
                        </Layout>
                    </Shade>
                    : null}
            </div>
        )
    }
});

export default Outcome;
