import SeriesChart from "../../../common/analysis/SeriesChart";

import CONSTANTS from "../../../common/constants";

var Chart = React.createClass({
    getInitialState: function () {
        return {}
    },

    componentWillMount: function () {

    },

    componentDidMount: function () {
        var definition = {
            interval: this.props.interval,
            title: "",
            period: [this.props.state, this.props.end],
            series: this.props.series,
            tools: false
        };
        var inst = SeriesChart(this.refs.chart, definition);
    },

    render: function () {
        return (
            <div className="widget">
                <div className="widget-header"><span>{this.props.title}</span></div>
                <div className="body">
                    <div className="chart" ref="chart"></div>
                </div>
            </div>
        )
    }
});

export default Chart;
