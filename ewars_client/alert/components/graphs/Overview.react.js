import Moment from "moment";
import DateUtils from "../../../common/utils/DateUtils";
import Chart from "./Chart.react";
import SeriesChart from "../../../common/analysis/SeriesChart";
import CONSTANTS from "../../../common/constants";

const ALARM_TREND_TRIGGERED = {definition: {type: "ALARM_TREND_TRIGGERED", alarm_id: null}, itype: CONSTANTS.CUSTOM};
const ALARM_TREND_VERIFICATION = {
    definition: {type: "ALARM_TREND_VERIFICATION", alarm_id: null},
    itype: CONSTANTS.CUSTOM
};
var ALARM_TREND_RISK = {definition: {type: "ALARM_TREND_RISK", alarm_id: null}, itype: CONSTANTS.CUSTOM};
var ALARM_TREND_INVESTIGATION = {
    definition: {type: "ALARM_TREND_INVESTIGATION", alarm_id: null},
    itype: CONSTANTS.CUSTOM
};

var ALARM_TREND_OPEN = {definition: {type: "ALARM_TREND_OPEN", alarm_id: null}, itype: CONSTANTS.CUSTOM};
var ALARM_TREND_CLOSED = {definition: {type: "ALARM_TREND_CLOSED", alarm_id: null}, itype: CONSTANTS.CUSTOM};

var STYLE = {margin: 10, background: "#FFF", padding: 8};


var Overview = React.createClass({
    getInitialState: function () {
        return {
            filter: CONSTANTS.SIXM,
            interval: CONSTANTS.WEEK,
            data: [],
            primaryChart: {},
            trendChart: {}
        }
    },

    componentWillMount: function () {
        var indicatorName = this.props.data.indicator_definition ? this.props.data.indicator_definition.name : this.props.data.indicator.name;
        var mainTitle = "Alert Indicator Trend: " + ewars.I18N(indicatorName) + " - " + ewars.I18N(this.props.data.location.name);

        var alertPlot = {
            color: "orange",
            value: Moment(this.props.data.trigger_end).valueOf(),
            label: {
                text: "Alert"
            },
            zIndex: 2,
            width: 2
        };

        var plotBand;


        var primarySeries = [{
            style: "line",
            colour: "red",
            indicator: this.props.data.indicator_id || this.props.data.indicator_definition,
            location: this.props.data.location.uuid,
            type: CONSTANTS.SERIES
        }];

        if (this.props.data.alarm.comparator_source == "DATA") {
        } else {
            plotBand = [{
                color: '#CCCCCC',
                opacity: 0.3,
                from: 0,
                to: parseFloat(this.props.data.alarm.comparator_value),
                label: {
                    text: "Alert Threshold",
                    align: "center",
                    style: {
                        color: "gray"
                    }
                },
                zIndex: 0
            }]
        }

        this.state.primaryChart = {
            type: "SERIES",
            title: {en: "Alert Indicator Trend"},
            show_title: true,
            interval: this.state.interval,
            tools: false,
            height: "450px",
            filter: this.props.filter,
            navigator: true,
            show_legend: true,
            xPlotLines: [alertPlot],
            yPlotBands: plotBand,
            series: primarySeries
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.primaryChart.filter = nextProps.filter;
        this.state.primaryChart.interval = nextProps.interval;
        this.forceUpdate();

        this._mainChart.updateDefinition(this.state.primaryChart);
    },

    componentDidMount: function () {
        this._mainChart = SeriesChart(this.refs.mainChart, this.state.primaryChart, null, this.props.data.location.uuid, null, false);
    },

    render: function () {
        var indicatorName = this.props.data.indicator_definition ? this.props.data.indicator_definition.name : this.props.data.indicator.name;
        var mainTitle = "Alert Indicator Trend: " + ewars.I18N(indicatorName) + " - " + ewars.I18N(this.props.data.location.name);

        return (
            <div className="chart-widget" ref="mainChart" style={STYLE}></div>
        )
    }
});

export default Overview;
