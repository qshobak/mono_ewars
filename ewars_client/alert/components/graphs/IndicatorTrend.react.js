import SeriesChart from "../../../common/analysis/SeriesChart";
import CONSTANTS from "../../../common/constants";


var IndicatorTrend = React.createClass({
    _rendered: false,
    _chart: null,
    _isLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            locations: [],
            primaryChart: {}
        }
    },

    componentWillMount: function () {
        this.state.primaryChart = {
            title: "Indicator Trend - Nearby",
            show_title: true,
            type: "SERIES",
            timeInterval: this.props.interval,
            tools: false,
            height: 350,
            filter: this.props.filter,
            show_legend: true,
            navigator: true,
            series: []
        };


        ewars.tx("com.ewars.query", ["location", ["uuid", "name"], {
                parent_id: {eq: this.props.data.location.parent_id},
                status: {eq: "ACTIVE"},
                uuid: {neq: this.props.data.location.uuid}
            }, null, null, null, null])
            .then(function (resp) {
                this.state.locations = resp;
                this._isLoaded = true;
                if (this.isMounted()) {
                    this.forceUpdate();
                    this._renderUpdate();
                }
            }.bind(this))
    },

    componentDidMount: function () {
        this.state.primaryChart = {
            title: "Indicator Trend - Nearby",
            show_title: true,
            type: "SERIES",
            interval: this.props.interval || "DAY",
            tools: false,
            height: 350,
            filter: this.props.filter,
            show_legend: true,
            navigator: true,
            series: _.map(this.state.locations, function (location) {
                return {
                    title: ewars.I18N(location.name),
                    style: "line",
                    indicator: this.props.data.indicator_id || this.props.data.indicator_definition,
                    location: location.uuid,
                    source_type: CONSTANTS.SERIES,
                    type: CONSTANTS.SERIES
                }
            }, this)
        };
        this._chart = SeriesChart(this.refs.mainChart, this.state.primaryChart, null, null, null, false);
        if (this._isLoaded) this._renderUpdate();
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.primaryChart.filter = nextProps.filter;
        this.state.primaryChart.timeInterval = nextProps.interval;
        this.forceUpdate();
        this._renderUpdate();
    },

    _renderUpdate: function () {
        this.state.primaryChart = {
            title: "Indicator Trend - Nearby Locations",
            show_title: true,
            type: "SERIES",
            interval: this.props.interval || "DAY",
            tools: false,
            height: 350,
            filter: this.props.filter,
            show_legend: true,
            navigator: true,
            series: _.map(this.state.locations, function (location) {
                return {
                    title: ewars.I18N(location.name),
                    style: "line",
                    indicator: this.props.data.indicator_id || this.props.data.indicator_definition,
                    location: location.uuid,
                    type: CONSTANTS.SERIES
                }
            }, this)
        };

        this._chart.updateDefinition(this.state.primaryChart);
    },

    render: function () {

        return (
            <div className="grid">
                <div className="row">
                    <div className="col-12">
                        <div className="chart-widget" ref="mainChart"
                             style={{margin: 10, background: "#FFF", padding: 8}}></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                    </div>
                    <div className="col-6">
                    </div>
                </div>
            </div>
        )
    }
});

export default IndicatorTrend;
