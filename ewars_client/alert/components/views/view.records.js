import ReportComponent from "./Report.react";

class RecordDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="report"></div>
        )
    }
}

const ACTIONS = [
    ['fa-eye', 'VIEW']
]

class Record extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _action = (action) => {
        this.setState({show: !this.state.show})
    };

    render() {
        let label = `${this.props.data.form_name} - ${this.props.data.location_name} - ${this.props.data.data_date} by ${this.props.data.user_name} (${this.props.data.user_email})`;
        return (
            <div
                onClick={() => {
                    this.props.onSelect(this.props.data);
                }}
                className="block hoverable">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <strong>{this.props.data.form_name}</strong><br />
                            {this.props.data.location_name || ""}<br/>
                            {this.props.data.data_date}<br/>
                            {this.props.data.user_name} ({this.props.data.user_email})
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

class Records extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            report: null
        }

        ewars.tx('com.ewars.alert.records', [this.props.data.uuid])
            .then(resp => {
                this.setState({
                    data: resp
                })
            })
    }

    _onSelect = (data) => {
        this.setState({
            report: data
        })
    };

    render() {

        let view;

        if (this.state.report) {
            view = (
                <ReportComponent
                    data={this.state.report}/>
            )
        }

        console.log(this.state.data);
        return (
            <ewars.d.Layout style={{height: '300px'}}>
                <ewars.d.Row>
                    <ewars.d.Cell width="25%" borderRight={true}>
                        <div className="block-tree" style={{position: 'relative'}}>
                            {this.state.data.map(item => {
                                return (
                                    <Record
                                        onSelect={this._onSelect}
                                        data={item}/>
                                )
                            })}
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            {view}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Records;
