import Moment from 'moment';

import CONSTANTS from '../../../common/constants';
import FlowStatus from '../facets/FlowStatus.react';
import AlertFlow from '../facets/AlertFlow.react';

import { Modal } from "../../../common";
import { TextAreaField } from "../../../common/fields";


const MOMENT = {
    "DAY": "d",
    "WEEK": "w",
    "MONTH": "m",
    "YEAR": "y"
};

class Dashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            reopenReason: "",
            showReopenModal: false
        }
    }

    _reopen = () => {
        this.setState({
            showReopenModal: true
        })
    };

    _onReasonChange = (name, value) => {
        this.setState({reopenReason: value})
    };

    _cancelReason = () => {
        this.setState({
            showReopenModal: false,
            reopenReason: ""
        })
    };

    _reopenAlert = () => {
        if (this.state.reopenReason == "") {
            ewars.growl("Please provide a valid reason for re-opening this alert.");
            return;
        }

        let blocker = new ewars.Blocker(null, "Reopening alert...");

        ewars.tx("com.ewars.alert.reopen", [this.props.data.uuid], {reason: this.state.reopenReason})
            .then((resp) => {
                this.setState({
                    showReopenModal: false,
                    reopenReason: ""
                });
                blocker.destroy();
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
            })
    };

    _cancelReopen = () => {
        this.setState({
            showReopenModal: false,
            reopenReason: ""
        })
    };

    render() {
        let overdueString,
            showRecovery = false;
        if (this.props.data.alarm.recovery_enabled) {
            let overDate = Moment(this.props.data.created).add(this.props.data.alarm.recovery_interval_period, MOMENT[this.props.data.alarm.recovery_interval_type]);
            if (overDate.isAfter(Moment(), 'd')) {
                showRecovery = true;
                if (this.props.data.stage != CONSTANTS.VERIFICATION && this.props.data.stage_state != CONSTANTS.PENDING) showRecovery = false;
                overdueString = "This alert will be auto-discarded " + overDate.fromNow();
            }
        }

        let canReopen = false;
        if (this.props.data.state == CONSTANTS.CLOSED) canReopen = true;
        if (this.props.data.state == CONSTANTS.AUTODISCARDED) canReopen = true;

        let reopenButtons = [
            {label: "Re-open Alert", icon: "fa-undo", onClick: this._reopenAlert, colour: "green"},
            {label: "Cancel", icon: "fa-times", onClick: this._cancelReopen, colour: "red"}
        ];

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>

                            {showRecovery ?
                                <div style={{display: "block", margin: "16px"}}>
                                    <i className="fal fa-info"></i>&nbsp;{overdueString}
                                </div>
                                : null}
                            {canReopen ?
                                <div className="widget">
                                    <div className="body no-pad">
                                        <div className="warning"><strong>Alert Closed</strong>
                                            <ewars.d.Button label="Re-open alert"
                                                            onClick={this._reopen}/>
                                        </div>
                                    </div>
                                </div>
                                : null}


                            <div className="widget">
                                <div className="widget-header"><span>Alert Workflow</span></div>
                                <div className="body">
                                    <FlowStatus data={this.props.data}/>

                                </div>
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <Modal
                    title="Reopen Alert"
                    buttons={reopenButtons}
                    visible={this.state.showReopenModal}>
                    <div className="article" style={{padding: 14}}>
                        <label htmlFor="">Reason</label>
                        <p>Please enter a reason below for re-opening this alert</p>
                        <TextAreaField
                            name="reason"
                            config={{i18n: false}}
                            value={this.state.reopenReason}
                            onUpdate={this._onReasonChange}/>
                    </div>
                </Modal>

            </ewars.d.Layout>
        )
    }
}

export default Dashboard;
