import CONSTANTS from "../../../common/constants";

import Overview from "../graphs/Overview.react";
import IndicatorTrend from "../graphs/IndicatorTrend.react";

import { Button } from "../../../common";

var STYLE = {};

var VIEWS = {
    DEFAULT: "DEFAULT",
    NEARBY: "NEARBY"
};

var Graphs = React.createClass({
    getInitialState: function () {
        return {
            view: VIEWS.DEFAULT,
            filter: CONSTANTS.SIXM,
            interval: CONSTANTS.WEEK
        }
    },

    _swapView: function (data) {
        this.setState({
            view: data.view
        })
    },

    _changeFilter: function (data) {
        this.setState({
            filter: data.filter
        })
    },

    _changeInterval: function (data) {
        this.setState({
            interval: data.interval
        })
    },

    render: function () {
        var view;

        if (this.state.view == VIEWS.DEFAULT) view =
            <Overview interval={this.state.interval} filter={this.state.filter} data={this.props.data}/>;
        if (this.state.view == VIEWS.NEARBY) view =
            <IndicatorTrend interval={this.state.interval} filter={this.state.filter} data={this.props.data}/>;

        return (
            <div className="basic" style={STYLE}>
                <div className="grid">
                    <div className="row">
                        <div className="col-12">
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default Graphs;
