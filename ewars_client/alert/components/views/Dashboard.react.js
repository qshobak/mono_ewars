import AlertFlow from "../facets/AlertFlow.react";
import FlowStatus from "../facets/FlowStatus.react";
import CONSTANTS from "../../../common/constants"


const CONSTANTS = {
    ONEM: "ONEM",
    THREEM: "THREEM",
    SIXM: "SIXM",
    ONEY: "ONEY",
    YTD: "YTD",
    ALL: "ALL",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR"
};

var Dashboard = React.createClass({
    _chart: null,
    getInitialState: function () {
        return {
            data: [],
            filter: "6M",
            interval: CONSTANTS.WEEK,
            showVerifyModal: false
        }
    },

    render: function () {
        return (
            <div className="alert-wrapper">
                <AlertFlow data={this.props.data}/>
            </div>
        )
    }
});

export default Dashboard;
