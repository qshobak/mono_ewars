import { Button } from "../../../common";

var Reports = React.createClass({
    getInitialState: function () {
        return {
            data: [],
            ordering: null
        }
    },

    _query: function () {
        ewars.tx("com.ewars.alert.reports", [this.props.data.uuid])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {
        var reports, paginationString;

        if (this.state.data.length <= 0) {
            reports = <div className="placeholder">There are currently no reports associated with this this alert</div>;
        }

        return (
            <div className="panel">
                <ul className="filter-bar">
                </ul>

                <div className="clearer"></div>

                <ul className="reports-list">
                    {reports}
                </ul>

                <div className="pagination">
                    <table width="100%">
                        <tr>
                            <td>
                                <div className="pagination-status">{paginationString}</div>
                            </td>
                            <td>
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        icon="fa-step-backward"
                                        onClick={this._changePage}
                                        data={{page: "B"}}/>
                                    <ewars.d.Button
                                        icon="fa-fast-backward"
                                        onClick={this._changePage}
                                        data={{page: "BB"}}/>
                                    <ewars.d.Button
                                        icon="fa-fast-forward"
                                        onClick={this._changePage}
                                        data={{page: "FF"}}/>
                                    <ewars.d.Button
                                        icon="fa-step-forward"
                                        onClick={this._changePage}
                                        data={{page: "F"}}/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
});

export default Reports;
