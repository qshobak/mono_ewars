import Moment from "moment";

import CONSTANTS from "../../../common/constants";

import {
    ListComponent,
    Button,
    Form,
    Modal
} from "../../../common";
import { SelectField } from "../../../common/fields";
import Validator from "../../../common/utils/Validator";

var ReportType = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var formName = ewars.I18N(this.props.data.name);
        return (
            <li onCLick={this._onClick}>
                {formName}
            </li>
        )
    }
});

var InvReportListItemTemplate = React.createClass({
    render: function () {
        var formName = ewars.I18N(this.props.data.form_name);
        var reportDate = Moment(this.props.data.data_date).format(CONSTANTS.DEFAULT_DATE);

        return (
            <div className="inv-report">
                <div className="invReportHeading">{formName} - {this.props.data.user_name} - {reportDate}</div>
            </div>
        )
    }
});


var Dashboard = React.createClass({
    getInitialState: function () {
        return {
            reports: [],
            id_filter: null
        }
    },

    _onReportSelect: function (data) {
        this.props.onClick(data);
    },

    componentWillMount: function () {
        ewars.subscribe("RELOAD_REPORTS", this._reload);
        this._reload();
    },

    _reload: function () {
        ewars.tx("com.ewars.alert.collections", [this.props.data.uuid])
            .then(function (resp) {
                this.state.reports = resp;
                this.forceUpdate();
            }.bind(this))
    },

    _createReport: function () {
        this.props.createReport();
    },

    render: function () {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="tbar">
                    <div className="btn-group">
                        <ewars.d.Button
                            label="Create Report"
                            colour="green"
                            onClick={this._createReport}
                            icon="fa-plus"/>
                    </div>
                </div>
                <div className="widget">
                    <div className="body no-pad">
                        <ListComponent
                            items={this.state.reports}
                            Template={InvReportListItemTemplate}
                            onClick={this._onReportSelect}/>
                    </div>
                </div>
            </div>
        )
    }
});


var Investigation = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            attachments: [],
            notes: [],
            activity: [],
            view: "DASHBOARD",
            activeForm: {},
            activeFormId: null,
            editingFormId: null,
            reportErrors: []
        }
    },

    componentWillMount: function () {
        this.state.draft = {
            uuid: null,
            created_by: window.user.id,
            form_id: null,
            data: {},
            status: "DRAFT",
            data_date: Moment().toDate(),
            alert_id: this.props.data.uuid,
            location_id: this.props.data.location_id
        };

        var query = {};
        query.id = {in: this.props.data.alarm.inv_form_ids};
        ewars.tx("com.ewars.query", ["form", null, query, null, null, null, ['version:version_id:uuid']])
            .then(function (resp) {
                this.state.forms = resp;
                this.forceUpdate();
            }.bind(this))
    },

    _viewReport: function (data) {
        var form = _.find(this.state.forms, function (item) {
            if (item.id == data.form_id) return item;
        }, this);

        this.setState({
            showModal: true,
            draft: data,
            editingFormId: data.form_id,
            activeForm: form
        })
    },

    _createReport: function () {
        this.setState({
            showModal: true,
            draft: {
                uuid: null,
                created_by: window.user.id,
                form_id: null,
                data: {},
                status: "DRAFT",
                data_date: Moment().toDate(),
                alert_id: this.props.data.uuid,
                location_id: this.props.data.location_id
            }
        })
    },

    _cancelReport: function () {
        this.setState({
            showModal: false,
            draft: {
                uuid: null,
                created_by: window.user.id,
                form_id: null,
                data: {},
                status: "DRAFT",
                data_date: Moment().toDate(),
                alert_id: this.props.data.uuid,
                location_id: this.props.data.location_id
            },
            activeFormId: null,
            editingFormId: null,
            activeForm: {}
        })
    },

    _saveDraft: function () {
    },

    _submit: function () {
        var blocker = new ewars.Blocker(null, "Validating Report...");

        var resultData = Validator(this.state.activeForm.version.definition, this.state.draft.data);

        if (resultData) {
            blocker.destroy();
            ewars.growl("There is an error in the report");
            this.setState({
                reportErrors: resultData
            });
            return;
        }

        blocker.destroy();

        ewars.prompt("fa-paper-plane", "Submit Report", "Are you sure you want to submit this report? Once submitted, the report will not be editable.", function () {
            var subBlocker = new ewars.Blocker(null, "Submitting Report...");
            this.state.draft.status = "SUBMITTED";
            this.state.draft.data_date = Moment().toDate();
            ewars.tx("com.ewars.form", ["SUBMIT", this.state.activeForm.id, this.state.draft])
                .then(
                    function (resp) {
                        subBlocker.destroy();
                        ewars.emit("RELOAD_REPORTS");
                        this.state.draft = {
                            uuid: null,
                            created_by: window.user.id,
                            form_id: null,
                            data: {},
                            status: "DRAFT",
                            data_date: Moment().toDate(),
                            alert_id: this.props.data.uuid,
                            location_id: this.props.data.location_id
                        };
                        this.state.activeForm = {};
                        this.state.showModal = false;
                        this.state.activeFormId = null;
                        this.state.editingFormId = null;
                        ewars.growl("Report submitted successfully");
                        this.forceUpdate();
                    }.bind(this)
                )
        }.bind(this))
    },

    _changeForm: function (prop, form_id) {
        var form = _.find(this.state.forms, function (formItem) {
            if (formItem.id == form_id) return formItem;
        }, this);

        this.setState({
            draft: {
                form_id: form.id,
                data: {},
                uuid: null,
                status: "DRAFT",
                data_date: Moment().toDate(),
                alert_id: this.props.data.uuid,
                location_id: this.props.data.location_id,
                created_by: window.user.id
            },
            editingFormId: form_id,
            activeForm: form
        })
    },

    _formUpdate: function (data, prop, value, path) {
        this.setState({
            draft: {
                ...this.state.draft,
                [prop]: value
            }
        })
    },

    _formDataUpdate: function (data, prop, value, path) {
        console.log(arguments);

        let key = path || prop;

        this.setState({
            draft: {
                ...this.state.draft,
                data: {
                    ...this.state.draft.data,
                    [key]: value
                }
            }
        })
    },

    render: function () {
        var view;

        if (this.state.view == "DASHBOARD") view =
            <Dashboard onClick={this._viewReport} data={this.props.data} createReport={this._createReport}/>;

        var invButtons = [];
        if (this.state.draft.status == "DRAFT") {
            invButtons = [
                {label: "Submit", icon: "fa-send", onClick: this._submit, colour: "green"},
                {label: "Save Draft", icon: "fa-save", onClick: this._saveDraft, colour: "amber"},
                {label: "Cancel", icon: "fa-times", onClick: this._cancelReport, colour: "red"}
            ];
        } else {
            invButtons = [
                {label: "Cancel", icon: "fa-times", onClick: this._cancelReport, colour: "red"}
            ];
        }

        var formSelectOptions = _.map(this.state.forms, function (form) {
            return [form.id, ewars.I18N(form.name)];
        }, this);

        var readOnly = true;
        if (this.state.draft.status == "DRAFT" || !this.state.draft) readOnly = false;

        var formName = "Create Report";
        if (this.state.showModal) formName = ewars.I18N(this.state.activeForm.name || "Create Report");

        var formTypes = _.map(this.state.forms, function (form) {
            var name = ewars.I18N(form.name);
            return (
                <div className="item">
                    <div>{name}</div>
                </div>
            )
        });

        let reportDetails = {};
        if (this.state.activeForm.features) {
            if (this.state.activeForm.features.LOCATION_REPORTING) {
                let site_type_id = this.state.activeForm.features.LOCATION_REPORTING.site_type_id || null;

                reportDetails.location_id = {
                    type: "location",
                    label: "Location",
                    selectionTypeId: site_type_id
                }
            }
        }

        return (
            <div className="ide-layout light">
                <div className="ide-row">
                    <div className="ide-col" style={{maxWidth: 250}}>
                        <div className="ide-panel ide-panel-absolute border-right">

                            <div className="zone">
                                <div className="zone-title">Reports</div>
                                <div className="zone-items">
                                    {formTypes}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="ide-col">
                        {view}
                    </div>
                </div>
                {this.state.showModal ?
                    <Modal
                        title={formName}
                        visible={true}
                        buttons={invButtons}>
                        <div style={{minHeight: 200, padding: 16}}>
                            {this.state.draft.status != "SUBMITTED" ?
                                <div style={{paddingBottom: 10}}>
                                    <label htmlFor="">Report Type</label>
                                    <SelectField
                                        config={{options: formSelectOptions}}
                                        onUpdate={this._changeForm}
                                        value={this.state.editingFormId}
                                        name="form_id"/>
                                </div>
                                : null}

                            {this.state.editingFormId ?
                                <div style={{padding: 0}}>
                                    <Form
                                        readOnly={readOnly}
                                        definition={reportDetails}
                                        data={this.state.draft}
                                        errors={this.state.reportErrors}
                                        updateAction={this._formUpdate}/>
                                    <Form
                                        readOnly={readOnly}
                                        definition={this.state.activeForm.version.definition}
                                        data={this.state.draft.data}
                                        errors={this.state.reportErrors}
                                        updateAction={this._formDataUpdate}/>
                                </div>
                                : null}
                        </div>

                    </Modal>
                    : null}

            </div>
        )
    }
});

export default Investigation;
