import user_types from "../../../common/constants/user_types";
import User from "../../../user/components/User";

var UserItem = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        return (
            <div className="item" onClick={this._onClick}>
                <div className="">{this.props.data.name}</div>
            </div>
        )
    }
});

class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            user: null
        }

        ewars.tx('com.ewars.alert.users', [this.props.data.uuid])
            .then(resp => {
                this.setState({data: resp})
            })
    }

    _userSelect = (user) => {
        this.setState({user: user})
    };

    render() {
        var selections;

        if (this.state.data) {
            let user_groups = {};
            (this.state.data || []).forEach(item => {
                if (user_groups[item.role]) {
                    user_groups[item.role].push(item);
                } else {
                    user_groups[item.role] = [item];
                }
            });
            selections = [];

            for (let userType in user_groups) {
                let typeUsers = user_groups[userType];
                let items = typeUsers.map(user => {
                    return <UserItem data={user} onClick={this._userSelect}/>
                });

                let zoneName = __(user_types[userType].name);

                selections.push(
                    <div className="zone">
                        <div className="zone-title">{zoneName}</div>
                        <div className="zone-items">
                            {items}
                        </div>
                    </div>
                )
            }
        }

        let view;
        if (this.state.user) {
            view = <User id={this.state.user.id}/>;
        }

        return (
            <div className="ide light">
                <div className="ide-layout light">
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 250}}>
                            <div className="ide-panel ide-panel-absolute border-right ide-scroll">
                                {selections}
                            </div>
                        </div>
                        <div className="ide-col">
                            <div className="ide-panel ide-panel-absolute">
                                {view}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default Users;
