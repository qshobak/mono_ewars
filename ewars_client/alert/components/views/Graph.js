import Moment from "moment";
import { DateRangeField as Range } from "../../../common/fields";
import SeriesChart from "../../../common/analysis/SeriesChart";


class Graph extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chart: null,
            filter: "3M"
        }
    }

    componentWillMount() {
        let alertPlot = {
            color: "orange",
            value: Moment(this.props.data.trigger_end).valueOf(),
            label: {
                text: "Alert"
            },
            zIndex: 2,
            width: 2
        };

        let plotBand;

        if (this.props.data.alarm.crit_source == "DATA") {
        } else {
            plotBand = [{
                color: '#CCCCCC',
                opacity: 0.3,
                from: 0,
                to: parseFloat(this.props.data.alarm.crit_value),
                label: {
                    text: "Alert Threshold",
                    align: "center",
                    style: {
                        color: "gray"
                    }
                },
                zIndex: 0
            }]
        }


        console.log(this.props);
        let indicator = this.props.data.alarm.ds_indicator;
        if (indicator) {
            if (indicator.indexOf("{") >= 0) indicator = JSON.parse(indicator);
        }
        let primarySeries = [{
            style: "line",
            colour: "red",
            indicator: indicator,
            location: this.props.data.location_id,
            type: "SERIES"
        }];

        let chart = {
            type: "SERIES",
            interval: "WEEK",
            tools: false,
            height: "450px",
            navigator: true,
            show_legend: true,
            xPlotLines: [alertPlot],
            yPlotBands: plotBand,
            series: primarySeries,
            period: ["2016-01-01", Moment().utc().format("YYYY-MM-DD")]
        };

        this.state.chart = chart;
    }

    componentWillReceiveProps(nextProps) {
        let chart = this.state.chart;
    }

    componentDidMount() {
        this._mainChart = SeriesChart(this.refs.chart, this.state.chart, null, this.props.data.location_id, null, false);
    }


    render() {
        return (
            <div style={{background: "#FFFFFF"}}>
                {/*<Range/>*/}
                <div id="chart" ref="chart"></div>
            </div>
        )
    }
}

export default Graph;
