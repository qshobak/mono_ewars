import Moment from "moment";

import {
    Button,
    ListComponent,
    Spinner
} from "../../../common";

const NAMES = {
    MONITOR: "Monitor",
    DISCARD: "Discarded",
    VERIFICATION: "Verification",
    RISK_CHAR: "Risk Characterisation",
    RISK_ASSESS: "Risk Assessment",
    OUTCOME: "Outcome",
    PENDING: "Pending",
    ACTIVE: "In Progress",
    COMPLETE: "Completed",
    OPEN: "Open",
    CLOSED: "Closed",
    UNSET_RISK: "Risk unassigned",
    UNSET_OUTCOME: "Outcome unassigned"
};

var ListItemTemplate = React.createClass({
    onClick: function () {
        window.open("/alert#?uuid=" + this.props.data.uuid);
    },

    render: function () {
        var triggerDate = Moment(this.props.data.trigger_end).format("YYYY-MM-DD");
        var alarmName = ewars.I18N(this.props.data.alarm_name);
        var stateName = NAMES[this.props.data.state];
        var riskName = NAMES[this.props.data.risk || "UNSET_RISK"];
        var outcomeName = NAMES[this.props.data.outcome || "UNSET_OUTCOME"];

        return (
            <div className="block" onClick={this.onClick}>
                <div className="block-content">
                    <div className="ide-row">
                        <div className="ide-col">
                            {alarmName} - {this.props.data.location_name_full}
                        </div>
                        <div className="ide-col" style={{textAlign: "right"}}>
                            {triggerDate}
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            {stateName} - {riskName} - {outcomeName}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var Related = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            user: null
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.alert.related", [this.props.data.uuid])
            .then(function (resp) {
                this.state.data = resp;
                this._isLoaded = true;
                this.forceUpdate();
            }.bind(this))
    },

    _userSelect: function (item) {
        this.setState({
            user: item
        })
    },

    _onClick: function (item) {
        window.open("/alert#?uuid=" + item.uuid);
    },

    render: function () {

        var view;

        if (this._isLoaded) {
            if (this.state.data.length > 0) {
                view = this.state.data.map(item => {
                    return <ListItemTemplate
                        data={item}/>
                })
            } else {
                view = <div className="placeholder">There are currently no related alerts.</div>
            }
        } else {
            view = <Spinner/>;
        }

        return view;
    }
});

export default Related;
