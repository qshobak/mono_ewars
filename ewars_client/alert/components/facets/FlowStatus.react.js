import Moment from "moment";

import {
    Button
} from "../../../common";
import CONSTANTS from "../../../common/constants";

import utils from "../../utils";

import Verification from "../dialogs/Verification.react";
import RiskAssessment from "../dialogs/RiskAssessment.react";
import RiskCharacterisation from "../dialogs/RiskCharacterisation.react";
import Outcome from "../dialogs/Outcome.react";

const VIEWS = {
    VERIFICATION: Verification,
    RISK_ASSESS: RiskAssessment,
    RISK_CHAR: RiskCharacterisation,
    OUTCOME: Outcome
}


var VerificationNodeItem = React.createClass({
    _onClick: function (data) {
        this.props.onClick(data);
    },

    render: function () {
        var className = "flow-state right";

        var marker = 1;
        if ([CONSTANTS.RISK_ASSESS, CONSTANTS.RISK_CHAR, CONSTANTS.OUTCOME].indexOf(this.props.alert.stage) >= 0) {
            marker = <i className="fal fa-check"></i>;
            className += " complete clickable";
        }

        if (this.props.alert.stage == CONSTANTS.VERIFICATION) {
            if (this.props.alert.stage_state == CONSTANTS.ACTIVE) {
                className += " active clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.PENDING) {
                className += " clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.COMPLETED) {
                marker = <i className="fal fa-check"></i>;
                className += " complete clickable";
            }
        }

        var showArrow = false;
        if (this.props.view == CONSTANTS.VERIFICATION) showArrow = true;

        return (
            <div className={className}>
                <div className="marker-wrapper">
                    <div className="label">Verification</div>
                    <div className="marker" onClick={() => this._onClick("VERIFICATION")}>
                        <div className="marker-icon">{marker}</div>
                    </div>
                    {showArrow ?
                        <div className="flow-state-arrow"></div>
                        : null}
                </div>
            </div>
        )
    }
});


var RiskAssessNodeItem = React.createClass({
    _onClick: function (data) {
        this.props.onClick(data);
    },

    render: function () {
        var className = "flow-state right";

        var marker = 2;
        if (this.props.alert.stage == CONSTANTS.RISK_CHAR) {
            marker = <i className="fal fa-check"></i>;
            className += " complete clickable";
        }

        if (this.props.alert.stage == CONSTANTS.OUTCOME) {
            if (this.props.alert.stage_state == CONSTANTS.MONITOR) {
                marker = <i className="fal fa-pencil"></i>;
                className += " complete clickable";
            } else {
                marker = <i className="fal fa-check"></i>;
                className += " complete clickable";
            }
        }

        if (this.props.alert.stage == CONSTANTS.RISK_ASSESS) {
            if (this.props.alert.stage_state == CONSTANTS.PENDING) {
                className += " clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.ACTIVE) {
                className += " active clickable";
            }

        }

        var showArrow = false;
        if (this.props.view == CONSTANTS.RISK_ASSESS) showArrow = true;

        return (
            <div className={className}>
                <div className="marker-wrapper">
                    <div className="label">Risk Assessment</div>
                    <div className="marker" onClick={() => this._onClick("RISK_ASSESS")}>
                        <div className="marker-icon">{marker}</div>
                    </div>
                    {showArrow ?
                        <div className="flow-state-arrow"></div>
                        : null}
                </div>
            </div>
        )
    }
});

var RiskCharNode = React.createClass({
    _onClick: function (data) {
        this.props.onClick(data);
    },

    render: function () {
        var className = "flow-state right";

        var marker = 3;
        if ([CONSTANTS.OUTCOME].indexOf(this.props.alert.stage) >= 0) {
            if (this.props.alert.stage_state == CONSTANTS.MONITOR) {
                marker = <i className="fal fa-pencil"></i>;
                className += " complete clickable";
            } else {
                marker = <i className="fal fa-check"></i>;
                className += " complete clickable";
            }
        }

        if (this.props.alert.stage == CONSTANTS.RISK_CHAR) {
            className += " clickable";
            if (this.props.alert.stage_state == CONSTANTS.ACTIVE) {
                className += " active";
            }
        }

        var showArrow = false;
        if (this.props.view == CONSTANTS.RISK_CHAR) showArrow = true;

        return (
            <div className={className}>
                <div className="marker-wrapper">
                    <div className="label">Risk Characterisation</div>
                    <div className="marker" onClick={() => this._onClick("RISK_CHAR")}>
                        <div className="marker-icon">{marker}</div>
                    </div>
                    {showArrow ?
                        <div className="flow-state-arrow"></div>
                        : null}
                </div>
            </div>
        )
    }
});

var OutcomeNode = React.createClass({
    _onClick: function (data) {
        this.props.onClick(data);
    },

    render: function () {
        var className = "flow-state right";

        var marker = 4;

        if (this.props.alert.stage == CONSTANTS.OUTCOME) {
            if (this.props.alert.stage_state == CONSTANTS.MONITOR) {
                marker = <i className="fal fa-pencil"></i>;
                className += " complete clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.COMPLETED) {
                marker = <i className="fal fa-check"></i>;
                className += " complete clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.RESPOND) {
                marker = <i className="fal fa-check"></i>;
                className += " complete clickable";
            }

            if (this.props.alert.stage_state == CONSTANTS.ACTIVE) {
                className += " active clickable";
            }
        }

        if (this.props.alert.stage == CONSTANTS.VERIFICATION) {
            if (this.props.alert.stage_state == CONSTANTS.DISCARD) {
                className += " complete";
                marker = <i className="fal fa-check"></i>;
            }
        }

        var showArrow = false;
        if (this.props.view == CONSTANTS.OUTCOME) showArrow = true;

        return (
            <div className={className}>
                <div className="marker-wrapper">
                    <div className="label">Outcome</div>
                    <div className="marker" onClick={() => this._onClick("OUTCOME")}>
                        <div className="marker-icon">{marker}</div>
                    </div>
                    {showArrow ?
                        <div className="flow-state-arrow"></div>
                        : null}
                </div>
            </div>
        )
    }
});

var FlowStatus = React.createClass({
    getInitialState: function () {
        return {
            view: "VERIFICATION"
        }
    },

    componentWillMount: function () {
        this.state.view = this.props.data.stage;
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.view = nextProps.data.stage;
    },

    _setOptions: function (data) {
        ewars.emit("ALERT_MODAL", data);
    },

    _onNodeClick: function (view) {
        var canClick = utils.canClickStage(this.props.data, this.props.data.stage, view);
        console.log(canClick);
        if (canClick) {
            this.setState({
                view: view
            })
        }
    },

    _reopen: function () {
        ewars.prompt("fa-exclamation-circle", "Re-Open Alert", "Are you sure you want to re-open this alert?", function () {
            var blocker = new ewars.Blocker(null, "Re-opening alert...");
                ewars.tx("com.ewars.alert", ["REOPEN", this.props.data.uuid, null])
                    .then(function(resp) {
                        ewars.emit("RELOAD_ALERT");
                        ewars.emit("RELOAD_ACTIVITY");
                        blocker.destroy();
                    }.bind(this))
        }.bind(this))
    },

    render: function () {
        var modalView;
        if (this.state.modalView == "VERIFICATION") modalView = null;

        var ViewCmp = VIEWS[this.state.view];

        // Connectors
        var CONN_1 = "flow-connector",
            CONN_2 = "flow-connector",
            CONN_3 = "flow-connector";


        // Map out which flow connectors need to be set
        if ([CONSTANTS.RISK_ASSESS, CONSTANTS.RISK_CHAR, CONSTANTS.OUTCOME].indexOf(this.props.data.stage) >= 0) CONN_1 += " passed";
        if ([CONSTANTS.RISK_CHAR, CONSTANTS.OUTCOME].indexOf(this.props.data.stage) >= 0) CONN_2 += " passed";
        if ([CONSTANTS.OUTCOME].indexOf(this.props.data.stage) >= 0) CONN_3 += " passed";
        if (this.props.data.stage == CONSTANTS.VERIFICATION && this.props.data.stage_state == CONSTANTS.DISCARD) {
            CONN_1 += " passed";
            CONN_2 += " passed";
            CONN_3 += " passed";
        }

        var VER_ASS,
            ASS_CHAR,
            CHAR_OUT,
            DONE;

        if (this.props.data.actions[CONSTANTS.VERIFICATION]) VER_ASS = Moment(this.props.data.actions.VERIFICATION.submitted_date).from(this.props.data.created, true);
        if (this.props.data.actions.RISK_ASSESS && this.props.data.actions.RISK_ASSESS.submitted_date) ASS_CHAR = Moment(this.props.data.actions.RISK_ASSESS.submitted_date).from(this.props.data.created, true);
        if (this.props.data.actions.RISK_CHAR && this.props.data.actions.RISK_CHAR.submitted_date) CHAR_OUT = Moment(this.props.data.actions.RISK_CHAR.submitted_date).from(this.props.data.created, true);
        if (this.props.data.actions.OUTCOME && this.props.data.actions.OUTCOME.submitted_date) DONE = Moment(this.props.data.actions.OUTCOME.submitted_date).format("D MMM YYYY");

        var triggerDate = Moment(this.props.data.created).format("D MMM YYYY");

        return (
            <div className="flow-status">

                <div className="clearer" style={{height: 16}}></div>
                <div className="flow-states">
                    <div className="flow-row">
                        <div className="flow-date left">{triggerDate}</div>
                        <div className="flow-state left triggered">
                            <div className="marker-wrapper">
                                <div className="label">Alert Triggered</div>
                                <div className="marker">
                                    <div className="marker-icon">
                                        <i className="fal fa-bell"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flow-connector passed">
                        </div>
                        <VerificationNodeItem
                            onClick={this._onNodeClick}
                            view={this.state.view}
                            alert={this.props.data}/>
                        <div className={CONN_1}><div className="flow-time">{VER_ASS}</div></div>
                        <RiskAssessNodeItem
                            onClick={this._onNodeClick}
                            view={this.state.view}
                            alert={this.props.data}/>
                        <div className={CONN_2}><div className="flow-time">{ASS_CHAR}</div></div>
                        <RiskCharNode
                            onClick={this._onNodeClick}
                            view={this.state.view}
                            alert={this.props.data}/>
                        <div className={CONN_3}><div className="flow-time">{CHAR_OUT}</div></div>
                        <OutcomeNode
                            onClick={this._onNodeClick}
                            view={this.state.view}
                            alert={this.props.data}/>
                        <div className="flow-date right">{DONE}</div>
                    </div>
                </div>

                <ViewCmp data={this.props.data}/>
            </div>
        )
    }
});

export default FlowStatus;
