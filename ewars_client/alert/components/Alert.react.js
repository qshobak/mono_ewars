import Moment from "moment";

// var Dashboard = require("./views/Dashboard.react");
import Investigation from "./views/Investigation.react";
import Report from "./views/Report.react";
import reports from "./views/Reports.react";
import Graphs from "./views/Graphs.react";
import Graph from "./views/Graph";

import Records from './views/view.records';
import Dashboard from './views/view.dashboard';
import Activity from './views/view.activity';
import Users from './views/view.users';
import Related from "./views/view.related";
import FlowStatus from "./facets/FlowStatus.react";
import ControlDropDown from "./facets/ControlDropDown.react";

import { Modal } from "../../common";
import { TextAreaField } from "../../common/fields";
import CONSTANTS from "../../common/constants";

var VIEWS = {
    DASHBOARD: "DASHBOARD",
    GRAPHS: "GRAPHS",
    INVESTIGATION: "INVESTIGATION",
    REPORT: "REPORT",
    RELATED: "RELATED",
    USERS: "USERS"
};

var RISK_DATA = {
    LOW: [CONSTANTS.GREEN, "Low Risk"],
    MODERATE: [CONSTANTS.YELLOW, "Moderate Risk"],
    HIGH: [CONSTANTS.ORANGE, "High Risk"],
    SEVERE: [CONSTANTS.RED, "Very High Risk"]
};

var PANEL_BUTTONS = [
    {id: VIEWS.DASHBOARD, icon: CONSTANTS.ICO_DASHBOARD, label: "Activity"},
    {id: VIEWS.GRAPHS, icon: CONSTANTS.ICO_GRAPH, label: "Graphs"},
    {id: VIEWS.INVESTIGATION, icon: CONSTANTS.ICO_INVESTIGATION, label: "Investigation"},
    {id: VIEWS.REPORT, icon: CONSTANTS.ICO_REPORT, label: "Records"},
    {id: VIEWS.RELATED, icon: CONSTANTS.ICO_RELATED, label: "Related"},
    {id: VIEWS.USERS, icon: CONSTANTS.ICO_USERS, label: "Users"}
];

var MOMENT = {
    "DAY": "d",
    "WEEK": "w",
    "MONTH": "m",
    "YEAR": "y"
};

var RISK_NAMES = {
    LOW: "Low Risk",
    MODERATE: "Moderate Risk",
    HIGH: "High Risk",
    SEVERE: "Very High Risk",
    OUTCOME: "Outcome",
    RISK_CHAR: "Risk Characterisation",
    RISK_ASSESS: "Risk Assessment",
    VERIFICATION: "Verification",
    PENDING: "Pending",
    COMPLETED: "Completed",
    ACTIVE: "In Progress",
    DISCARD: "Discarded",
    RESPOND: "Respond",
    RESPONSE: "Respond",
    MONITOR: "Monitor",
    DISCARDED: "Discarded"
};

var PanelBarButton = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "panelsbar-btn";
        if (this.props.data.id == this.props.activeView) className += " panelsbar-btn-active";

        return (
            <div xmlns="http://www.w3.org/1999/xhtml"
                 onClick={this._onClick}
                 className={className}><i
                className={this.props.data.icon}></i>&nbsp;{this.props.data.label}
            </div>
        )
    }
});

class Alert extends React.Component {
    _isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            alert: null,
            alarm: null,
            location: null,
            alert_uuid: null,
            modalView: null,
            view: 'DEFAULT',
            stageData: null,
            showReOpen: false,
            showReopenModal: false,
            reopenReason: ""
        }
    }

    componentWillMount() {
        ewars.subscribe("RELOAD_ALERT", this._reload, "ALERT");


        let alert_uuid;
        if (this.props.uuid) {
            alert_uuid = this.props.uuid;
        } else {
            let location = document.location;
            alert_uuid = String(location).split("?")[1].split("=")[1];
        }

        this._loadAlert(alert_uuid);
    }

    componentWillUnmount() {
        ewars.unsubscribe("RELOAD_ALERT", this._reload, "ALERT");
    }

    _loadAlert = (uuid) => {
        let blocker = new ewars.Blocker(null, "Loading alert...");
        ewars.tx("com.ewars.alert", [uuid])
            .then((resp) => {
                blocker.destroy();
                this._isLoaded = true;
                this.setState({
                    alert: resp,
                    alert_uuid: uuid
                })
            })
    };

    _reload = () => {
        this._loadAlert(this.state.alert_uuid || this.props.uuid);
    };

    _swapView = (data) => {
        this.setState({view: data.id});
    };

    _alertActionDismiss = () => {
        this.setState({
            modalView: null,
            showModal: false
        })
    };

    _reopen = () => {
        this.setState({
            showReopenModal: true
        })
    };

    _onReasonChange = (name, value) => {
        this.setState({reopenReason: value})
    };

    _cancelReason = () => {
        this.setState({
            showReopenModal: false,
            reopenReason: ""
        })
    };

    _reopenAlert = () => {
        if (this.state.reopenReason == "") {
            ewars.growl("Please provide a valid reason for re-opening this alert.");
            return;
        }

        let blocker = new ewars.Blocker(null, "Reopening alert...");

        ewars.tx("com.ewars.alert.reopen", [this.state.alert.uuid], {reason: this.state.reopenReason})
            .then((resp) => {
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                this.setState({
                    showReopenModal: false,
                    reopenReason: ""
                });
                blocker.destroy();
            })
    };

    _cancelReopen = () => {
        this.setState({
            showReopenModal: false,
            reopenReason: ""
        })
    };

    render() {
        if (!this._isLoaded) return <div className="dummy"></div>;

        var overdueString,
            showRecovery = false;
        if (this.state.alert.alarm.recovery_enabled) {
            var overDate = Moment(this.state.alert.created).add(this.state.alert.alarm.recovery_interval_period, MOMENT[this.state.alert.alarm.recovery_interval_type]);
            if (overDate.isAfter(Moment(), 'd')) {
                showRecovery = true;
                if (this.state.alert.stage != CONSTANTS.VERIFICATION && this.state.alert.stage_state != CONSTANTS.PENDING) showRecovery = false;
                overdueString = "This alert will be auto-discarded " + overDate.fromNow();
            }
        }

        var view;

        if (this.state.view === VIEWS.DASHBOARD) view = <Dashboard data={this.state.alert}/>;
        if (this.state.view === VIEWS.INVESTIGATION) view = <Investigation data={this.state.alert}/>;
        if (this.state.view === VIEWS.REPORTS) view = <Reports data={this.state.alert}/>;
        if (this.state.view === VIEWS.USERS) view = <Users data={this.state.alert}/>;
        if (this.state.view === VIEWS.REPORT) view = <Records data={this.state.alert}/>;
        if (this.state.view === VIEWS.GRAPHS) view = <Graph data={this.state.alert}/>;
        if (this.state.view === VIEWS.RELATED) view = <Related data={this.state.alert}/>;

        var style = {background: "#EDF1F4"};
        if (this.state.view === VIEWS.INVESTIGATION) style.height = 650;
        if (this.state.view === VIEWS.REPORTS) style.height = 650;
        if (this.state.view === VIEWS.USERS) style.height = 650;
        if (this.state.view === VIEWS.RELATED) style.height = 650;

        var alarmName = ewars.I18N(this.state.alert.alarm.name);
        var alarmDescription = ewars.I18N(this.state.alert.alarm.description);

        let tabs = PANEL_BUTTONS.map(btn => {
            if (btn.id === VIEWS.INVESTIGATION) {
                if ([CONSTANTS.RISK_ASSESS, CONSTANTS.RISK_CHAR, CONSTANTS.OUTCOME].indexOf(this.state.alert.stage) >= 0) {
                    return <PanelBarButton
                        data={btn}
                        activeView={this.state.view}
                        key={btn.id}
                        onClick={this._swapView}/>
                }
            } else {
                return <PanelBarButton
                    data={btn}
                    activeView={this.state.view}
                    key={btn.id}
                    onClick={this._swapView}/>
            }
        });

        var riskName = "Unassigned";
        if (this.state.alert.risk) riskName = RISK_NAMES[this.state.alert.risk];
        var stageName = "Verification";
        if (this.state.alert.stage) stageName = RISK_NAMES[this.state.alert.stage];
        var outcomeName = "No outcome";
        if (this.state.alert.outcome) outcomeName = RISK_NAMES[this.state.alert.outcome];
        if (this.state.alert.stage == CONSTANTS.VERIFICATION && this.state.alert.stage_state == CONSTANTS.DISCARD) outcomeName = ewars.I18N(RISK_NAMES[CONSTANTS.DISCARD]);

        var riskTag = <div className="risk-tag static">Unassigned</div>;
        if (this.state.alert.risk) {
            var className = "risk-tag static " + RISK_DATA[this.state.alert.risk][0];
            riskTag = <div className={className}>{RISK_DATA[this.state.alert.risk][1]}</div>
        }

        var canReopen = false;
        if (this.state.alert.state == CONSTANTS.CLOSED) canReopen = true;
        if (this.state.alert.state == CONSTANTS.AUTODISCARDED) canReopen = true;

        var reopenButtons = [
            {label: "Re-open Alert", icon: "fa-undo", onClick: this._reopenAlert, colour: "green"},
            {label: "Cancel", icon: "fa-times", onClick: this._cancelReopen, colour: "red"}
        ];

        let mainView;

        if (this.state.view =='DEFAULT') mainView = <Dashboard data={this.state.alert}/>;
        if (this.state.view == 'ACTIVITY') mainView = <Activity data={this.state.alert}/>;
        if (this.state.view == 'DATA') mainView = <Graph data={this.state.alert}/>;
        if (this.state.view == 'USERS') mainView = <Users data={this.state.alert}/>;
        if (this.state.view == 'RELATED') mainView = <Related data={this.state.alert}/>;
        if (this.state.view == 'RECORDS') mainView = <Records data={this.state.alert}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row height="123px">
                    <ewars.d.Cell>
                        <div className="alert-header">
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    <div className="alert-title">
                                        {alarmName}
                                    </div>
                                    <div className="alert-meta">
                                        <div className="item">
                                            <strong><i className="fal fa-map-marker"></i>&nbsp;
                                                Location:</strong>&nbsp;{this.state.alert.location_name_full}
                                        </div>
                                        {alarmDescription ?
                                            <div className="item">
                                                {alarmDescription}
                                            </div>
                                            : null}
                                        {this.state.alert.eid ?
                                            <div className="item">
                                                <strong>EID: </strong>{this.state.alert.eid}
                                            </div>
                                            : null}

                                    </div>

                                </ewars.d.Cell>
                                <ewars.d.Cell style={{display: "block"}}>
                                    <div className="alert-details">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th>Level of risk:</th>
                                                <td>{riskTag}</td>
                                            </tr>
                                            <tr>
                                                <th>Stage:</th>
                                                <td>{stageName}</td>
                                            </tr>
                                            <tr>
                                                <th>Outcome:</th>
                                                <td>{outcomeName}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row height="35px">
                    <ewars.d.Cell height="35px" borderBottom={true} style={{background: "#edf1f4"}} borderTop={true}>
                        <div className="ux-tabs">
                            <div
                                onClick={() => {
                                    this.setState({view: "DEFAULT"})
                                }}
                                className={"ux-tab " + (this.state.view == 'DEFAULT' ? ' active' : "")}>
                                <i className="fal fa-tachometer"></i>&nbsp;Overview
                            </div>
                            <div
                                onClick={() => {
                                    this.setState({view: "RECORDS"})
                                }}
                                className={"ux-tab" + (this.state.view == "RECORDS" ? " active" : "")}>
                                <i className="fal fa-file"></i>&nbsp;Records
                            </div>
                            <div
                                onClick={() => {
                                    this.setState({view: "ACTIVITY"})
                                }}
                                className={"ux-tab" + (this.state.view == "ACTIVITY" ? " active" : "")}>
                                <i className="fal fa-comment"></i>&nbsp;Activity
                            </div>
                            <div
                                onClick={() => {
                                    this.setState({view: "DATA"})
                                }}
                                className={"ux-tab" + (this.state.view == "DATA" ? " active" : "")}>
                                <i className="fal fa-chart-line"></i>&nbsp;Analysis
                            </div>
                            <div
                                onClick={() => {
                                    this.setState({view: "USERS"})
                                }}
                                className={"ux-tab" + (this.state.view == "USERS" ? " active" : "")}>
                                <i className="fal fa-users"></i>&nbsp;Users
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {mainView}

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Alert;

