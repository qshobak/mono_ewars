import CONSTANTS from "./constants";

/**
 * Check if a user can click on a specific flow item
 * @param alert
 * @param curStage
 * @param targetStage
 * @returns {boolean}
 */
const canClickStage = (alert, curStage, targetStage) => {
    if (curStage == CONSTANTS.VERIFICATION) {
        if (targetStage == CONSTANTS.VERIFICATION) return true;
        if (targetStage == CONSTANTS.RISK_ASSESS) return false;
        if (targetStage == CONSTANTS.RISK_CHAR) return false;
        if (targetStage == CONSTANTS.OUTCOME) return false;
    }

    if (curStage == CONSTANTS.RISK_ASSESS) {
        if (targetStage == CONSTANTS.VERIFICATION) return true;
        if (targetStage == CONSTANTS.RISK_ASSESS) return true;
        if (targetStage == CONSTANTS.RISK_CHAR) return false;
        if (targetStage == CONSTANTS.OUTCOME) return false;
    }

    if (curStage == CONSTANTS.RISK_CHAR) {
        if (targetStage == CONSTANTS.VERIFICATION) return true;
        if (targetStage == CONSTANTS.RISK_ASSESS) return true;
        if (targetStage == CONSTANTS.RISK_CHAR) return true;
        if (targetStage == CONSTANTS.OUTCOME) return false;
    }

    if (curStage == CONSTANTS.OUTCOME) {
        if (targetStage == CONSTANTS.VERIFICATION) return true;
        if (targetStage == CONSTANTS.RISK_ASSESS) return true;
        if (targetStage == CONSTANTS.RISK_CHAR) return true;
        if (targetStage == CONSTANTS.OUTCOME) return true;
    }
}

export default {
    canClickStage
}

