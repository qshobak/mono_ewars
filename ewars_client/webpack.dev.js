const Merge = require("webpack-merge");
const CommonConfig = require("./webpack.common.js");
// const PACKAGE = require(`./${env}/package.json`);

// const filename = `${PACKAGE.name}-dev.js`;
// const mapFilename = `${PACKAGE.name}-dev.map`;

module.exports = Merge(CommonConfig, {
    devtool: "cheap-module-source-map",

    output: {
        filename: '[name]-dev.js',
        sourceMapFilename: '[name]-dev.map'
    }

})

