import SettingsForm from "./c.form.settings";

const FORM = [
    "General",
    {
        n: "name",
        t: "language_string",
        r: true,
        l: _l("NAME")
    },
    {
        n: "status",
        t: "button_group",
        r: true,
        l: _l("STATUS"),
        c: {
            options: [
                ["ACTIVE", _l("ACTIVE")],
                ["INACTIVE", _l("INACTIVE")]
            ]
        }
    },
    {
        n: "description",
        t: "textarea",
        l: _l("DESCRIPTION")
    }
];

class LocationTypeEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: {}}
    }

    componentWillMount() {
        this.state.data = ewars.copy(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = ewars.copy(nextProps.data);
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _saved = (data) => {
        ewars.growl("Changes saved");
        this.props.onSave();
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Saving location type...");

        let args = [this.state.data.id, this.state.data];
        let com = "com.ewars.locations.type.update";
        if (this.state.data.id == null || this.state.data.id == undefined) {
            com = "com.ewars.locations.type.create";
            args = [this.state.data];
        }
        ewars.tx(com, args)
            .then((resp) => {
                bl.destroy();
                if (resp.err) {
                    ewars.growl(resp.err_code);
                } else {
                    this._saved(resp);
                }
            })
    };

    _close = () => {
        this.props.onSave();
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Location Type">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._save}
                            label="SAVE_CHANGES"
                            icon="fa-save"/>
                        <ewars.d.Button
                            onClick={this._close}
                            label="CLOSE"
                            icon="fa-times"/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <SettingsForm
                                toolbar={false}
                                onChange={this._onChange}
                                data={FORM}
                                dn={this.state.data}/>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default LocationTypeEditor;
