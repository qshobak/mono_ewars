import { EditableList } from "../../common";

const ACTIONS = [
    {icon: "fa-plus", action: "ADD"},
    {icon: "fa-folder", action: "ADD_SECTION"},
    {
        icon: "fa-pencil", action: "EDIT_SECTION", show: (item) => {
            if (Array.isArray(item)) return true;
            return false;
        }
    },
    {icon: "fa-minus", action: "REMOVE"},
    {icon: "fa-caret-up", action: "UP"},
    {icon: "fa-caret-down", action: "DOWN"}
];

const ITEM_STYLE = {
    padding: 8
};

const _ID = "DASHBOARDS";

class Item extends React.Component {
    constructor(props) {
        super(props)
    }

    onClick = () => {
        this.props.onClick(this.props.index, this.props.data);
    };

    render() {
        let className = "iw-list-edit-item";
        if (this.props.active) className += " active";

        if (!Array.isArray(this.props.data)) {
            return (
                <div className={className} style={ITEM_STYLE} onClick={this.onClick}>{this.props.data.name}</div>
            )
        } else {
            return (
                <div className={className} style={ITEM_STYLE} onClick={this.onClick}>
                    {this.props.data[0]} [Section]
                </div>
            )
        }
    }
}

class DashboardsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    componentWillMount() {

    }

    _onAction = (action, index, data) => {
        if (action == "REMOVE") ewars.z.dispatch(_ID, "REMOVE_DASH", index);
        if (action == "UP") ewars.z.dispatch(_ID, "DASH_MOVE_UP", index);
        if (action == "DOWN") ewars.z.dispatch(_ID, "DASH_MOVE_DOWN", index);


        if (action == "ADD") ewars.z.dispatch(_ID, "SHOW_DASH_SELECT", data);
        if (action == "ADD_SECTION") ewars.z.dispatch(_ID, "SHOW_SECTION_EDIT");
        if (action == "EDIT_SECTION") ewars.z.dispatch(_ID, "EDIT_SECTION", index);
    };

    render() {
        let selected = [];
        this.props.data.forEach(dash => {
            if (this.props.dashboards.length > 0) {
                this.props.dashboards.forEach(item => {
                    if (dash.uuid == item.uuid) dash.name = item.name;

                })
            }
            selected.push(dash);
        });

        let sorted = selected.sort(function (a, b) {
            let aO = a.order || a[1];
            let bO = b.order || b[1];
            if (aO < bO) return -1;
            if (aO > bO) return 1;
            return 0;
        });

        return (
            <div style={{padding: 16}}>
                <EditableList
                    Template={Item}
                    onAction={this._onAction}
                    items={sorted}
                    actions={ACTIONS}/>
            </div>
        )
    }
}

export default DashboardsList;
