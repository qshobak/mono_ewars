import DASHBOARD_ITEMS from "../constants/dashboard.items";

class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data.v);
    };

    render() {
        let iconClass = `fal ${this.props.data.i}`;
        let title = __(this.props.data.t);

        return (
            <ewars.d.Row borderBottom={true}>
                <ewars.d.Cell borderBottom={true} width="60px" style={{padding: "25px 16px 25px 16px", textAlign: "center"}}>
                    <i className={iconClass} style={{fontSize: "24px", textAlign: "center"}}></i>
                </ewars.d.Cell>
                <ewars.d.Cell borderBottom={true} style={{padding: 16}}>
                    <h3 style={{fontSize: "24px"}}>{title}</h3>

                    <div className="article" dangerouslySetInnerHTML={{__html: this.props.data.p}}>

                    </div>

                    <div className="clearer"></div>

                    <div className="btn-group">
                    <ewars.d.Button
                        onClick={this._onClick}
                        label="Manage"/>

                    </div>

                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel>
                <ewars.d.Layout>
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: "16px"}}>
                            <div>
                            {DASHBOARD_ITEMS.map((row, index) => {
                                let items = [];

                                if (index % 2 == 0) {
                                    items.push(
                                        <Item
                                            onClick={this.props.onView}
                                            data={row}/>
                                    )
                                }

                                return items;
                            })}
                            </div>

                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: "16px"}}>
                            <div>
                            {DASHBOARD_ITEMS.map((row, index) => {
                                let items = [];

                                if (index % 2 > 0) {
                                    items.push(
                                        <Item
                                            onClick={this.props.onView}
                                            data={row}/>
                                    )
                                }

                                return items;
                            })}
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </ewars.d.Panel>
        )
    }
}