import {
    Layout, Row, Cell,
    Toolbar,
    ButtonGroup,
    Button
} from "../../common";

import {
    NumericField as NumberField,
    TextField,
    SelectField,
    TextAreaField as TextArea,
    DisplayField,
    LanguageStringField as LanguageString,
    FileField as FileUploadField,
    MapEditorField as GeoEditor
} from "../../common/fields";

const FIELD_TYPES = {
    button_group: ButtonGroup,
    number: NumberField,
    text: TextField,
    select: SelectField,
    textarea: TextArea,
    display: DisplayField,
    file: FileUploadField,
    bounds: GeoEditor,
    language_string: LanguageString
};

const STYLES = {
    panel: {
        background: "#2d3032"
    },
    article: {
        paddingRight: 150,
        paddingLeft: 30,
        paddingTop: 30
    },
    label: {
        maxWidth: 150,
        fontWeight: "bold",
        textAlign: "right",
        paddingRight: 10,
        paddingTop: 8
    },
    row: {
        marginBottom: 8
    },
    header: {
        maxWidth: 150
    }
};

class PField extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Row>
                <Cell style={STYLES.header}></Cell>
                <Cell><p>{this.props.data}</p></Cell>
            </Row>
        )
    }
}

class Field extends React.Component {
    constructor(props) {
        super(props)
    }

    _onUpdate = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let FieldType;

        if (FIELD_TYPES[this.props.data.t]) {
            FieldType = FIELD_TYPES[this.props.data.t];
        }

        let value;
        if (this.props.dn[this.props.data.n]) {
            value = this.props.dn[this.props.data.n];
        }

        let help;
        if (this.props.data.h) {
            help = (
                <div className="adm-help">{this.props.data.h}</div>
            )
        }

        return (
            <Row style={STYLES.row}>
                <Cell style={STYLES.label}>{this.props.data.l}</Cell>
                <Cell>
                    {FieldType ?
                        <FieldType
                            value={value}
                            name={this.props.data.n}
                            onUpdate={this._onUpdate}
                            config={this.props.data.c || {}}/>
                        : null}
                    {help}
                </Cell>
            </Row>
        )
    }
}

class SettingsForm extends React.Component {
    static defaultProps = {
        toolbar: true
    };

    constructor(props) {
        super(props);


        this.state = {
            data: {}
        }

    }

    componentWillMount() {
        if (this.props.dn) {
            this.state.data = ewars.copy(this.props.dn);
        } else {
            ewars.tx("com.ewars.conf", [this.props.type])
                .then((resp) => {
                    this._parseConfig(resp);
                })
        }
    }

    _parseConfig = (cfg) => {
        let dt = {};

        for (var i in cfg) {
            let val = cfg[i];

            if ([true,false].indexOf(val) < 0) {
                if (val != null && val != undefined) {
                    if (val == "true") {
                        val = true;
                    } else if (val == "false") {
                        val = false;
                    }
                }
            }

            dt[i] = val;
        }
        this.setState({
            data: dt
        })
    };

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.dn) {
            this.state.data = ewars.copy(nextProps.dn);
        } else {
            ewars.tx("com.ewars.conf", [nextProps.type])
                .then((resp) => {
                    this._parseConfig(resp);
                })
        }
    };

    _onChange = (prop, value) => {
        if (this.props.onChange) {
            this.props.onChange(prop, value);
        } else {
            this.setState({
                data: {
                    ...this.state.data,
                    [prop]: value
                }
            })
        }
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Saving settings...");
        ewars.tx("com.ewars.conf.update", [this.props.type, this.state.data])
            .then((resp) => {
                bl.destroy();
                ewars.growl("Settings updated");
            })
    };

    render() {
        return (
            <Layout>
                {this.props.toolbar ?
                    <Toolbar label="Settings">
                        <div className="bnt-group pull-right" style={{marginRight: 8}}>
                            <Button
                                color="green"
                                icon="fa-save"
                                onClick={this._save}
                                label="Save Change(s)"/>
                        </div>

                    </Toolbar>
                    : null}
                <Row>
                    <Cell>


                        <div style={STYLES.panel} className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="article" style={STYLES.article}>
                                {this.props.data.map(item => {
                                    if (item.l) {
                                        return (
                                            <Field key={item.n} dn={this.state.data} data={item} onChange={this._onChange}/>
                                        )
                                    } else {
                                        return (
                                            <PField key={item.n} dn={this.state.data} data={item} onChange={this._onChange}/>
                                        )
                                    }
                                })}

                            </div>
                        </div>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}


export default SettingsForm;
