import { DataTable } from "../../../common";

const COLUMNS = [
    {
        name: "form_name",
        config: {
            label: "Form Name",
            type: "select",
            optionsSource: {
                resource: "form",
                valSource: "id",
                labelSource: "name",
                query: {}
            },
            filterKey: "form_id"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            options: [
                ["ACTIVE", "Active"],
                ["INACTIVE", "Inactive"]
            ]
        }
    },
    {
        name: "start_date",
        config: {
            label: "Start Date",
            type: "date"
        },
        fmt: ewars.DATE
    },
    {
        name: "end_date",
        config: {
            type: "date",
            label: "End Date"
        },
        fmt: ewars.DATE
    },
    {
        name: "user_email",
        config: {
            type: "text",
            label: "User email"
        }
    },
    {
        name: "user_name",
        config: {
            type: "text",
            label: "User name"
        }
    },
    {
        name: "location_name",
        config: {
            type: "location",
            label: "Location",
            filterKey: "location_id"
        },
        fmt: ewars.I18N
    },
    {
        name: "site_name",
        config: {
            type: "select",
            label: "Location type",
            optionsSource: {
                resource: "location_type",
                valSource: "id",
                labelSource: "name",
                query: {}
            }
        },
        fmt: ewars.I18N
    },
    {
        name: "org_name",
        config: {
            type: "select",
            label: "Organization",
            optionsSource: {
                resource: "organization",
                valSource: "uuid",
                labelSource: "name",
                query: {}
            }
        },
        fmt: ewars.I18N
    },
    {
        name: "uuid",
        config: {
            type: "text",
            label: "UUID"
        }
    }
];

export default class AssignmentsManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            data: null
        }
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Assignments Manager">
                    <div className="bnt-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={() => {
                                this._action("NEW");
                            }}
                            label="NEW_ASSIGNMENT"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            resource="assignment_full"
                            columns={COLUMNS}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}
