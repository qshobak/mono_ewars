const ACTIONS = [
    ["fa-envelope", "RESEND"]
];

class Invite extends React.Component {
    _action = (action) => {
        let bl = new ewars.Blocker(null, "Resending invite...");
        ewars.tx("com.ewars.invite.resend", [this.props.data.uuid])
            .then((resp) => {
                bl.destroy();
                ewars.growl("Invitation resent");
            })
    };

    render() {
        return (
            <div className="block" style={{padding: 0}}>
                <div className="block-content" style={{padding: 0}}>
                    <ewars.d.Row style={{padding: 8}}>
                        <ewars.d.Cell style={{lineHeight: "20px"}}>{this.props.data.email} - {__(this.props.data.details.role)}</ewars.d.Cell>
                        <ewars.d.Cell style={{display: "block"}}>
                            <ewars.d.ActionGroup
                                right={true}
                                onAction={this._action}
                                actions={ACTIONS}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

export default class Invitations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

        ewars.tx("com.ewars.invites")
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="placeholder">Loading invites...</div>
            )
        }

        if (this.state.data.length <= 0) {
            return (
                <div className="placeholder">There are currently no invites within the system</div>
            )
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Invitations">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: "relative"}}>
                                {this.state.data.map((item) => {
                                    return (
                                        <Invite data={item}/>
                                    )
                                })}
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}