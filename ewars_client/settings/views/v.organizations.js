import {
    Layout, Row, Cell,
    Toolbar,
    Shade,
    Button,
    DataTable
} from "../../common";
import SettingsForm from "../controls/c.form.settings";


if (!ewars.isSet) {
    ewars.isSet = function (value) {
        if (value == undefined) return false;
        if (value == null) return false;
        if (value == "") return false;
        return true;
    }
}

const ORG_FIELDS = [
    {
        n: "name",
        t: "language_string",
        l: "Organization Name",
        r: true
    },
    {
        n: "acronym",
        t: "text",
        l: "Acronym"
    },
    {
        n: "site",
        t: "text",
        l: "Web Site"
    },
    {
        n: "status",
        t: "select",
        l: "Status",
        c: {
            options: [
                ["ACTIVE", _l("ACTIVE")],
                ["INACTIVE", _l("INACTIVE")]
            ]
        }
    }
];

const COLUMNS = [
    {
        name: "name",
        config: {
            label: "Name"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", "Active"],
                ["INACTIVE", "Inactive"]
            ]
        }
    },
    {
        name: "acronym",
        config: {
            label: "Acronym"
        }
    },
    {
        name: "site",
        config: {
            label: "Site"
        }
    },
    {
        name: "created_by",
        config: {
            label: "Created by"
        }
    }
];

const ACTIONS = [
    {icon: "fa-pencil", label: "Edit", action: "EDIT"},
    {icon: 'fa-trash', label: 'Delete', action: 'DELETE'}
];


class OrganizationsManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            org: null,
            showShade: false
        }
    }

    _onDTAction = (action, cell, row) => {
        switch(action) {
            case 'CLICK':
            case 'EDIT':
                this.setState({
                    org: row,
                    showShade: true
                })
                break;
            case 'DELETE':
                ewars.prompt('fa-trash', 'Remove Organization', 'Are you sure you want to remove this organization?', () => {
                    let bl = new ewars.Blocker(null, 'Removing organization...');

                    ewars.tx('com.ewars.organization.remove', [row.uuid])
                        .then(() => {
                            bl.destroy();
                            ewars.emit("RELOAD_DT");
                        })
                        .catch(err => {
                            if (err) {
                                console.log(err);
                            }
                        })
                })
                break;
            default:
                break;
        }
    };

    _onShadeAction = () => {
        this.setState({
            org: null,
            showShade: false
        });
        ewars.emit("RELOAD_DT");
    };

    _create = () => {
        this.setState({
            org: {
                name: {en: ""},
                status: "ACTIVE"
            },
            showShade: true
        })
    };

    _save = () => {
        if (!ewars.isSet(this.state.org.name.en)) {
            ewars.growl("Please provide a valid name");
            return;
        }

        let bl = new ewars.Blocker(null, "Updating organization...");
        if (this.state.org.uuid) {
            ewars.tx('com.ewars.organization.update', [this.state.org.uuid, this.state.org])
                .then((resp) => {
                    bl.destroy();
                    if (resp.err) {
                        if (resp.code == "EXISTING") {
                            let conf = new ewars.Confirmation({
                                icon: "fa-warning",
                                title: "Duplicate Organization",
                                body: "We found an existing organization within EWARS, would you like to add this organization instead",
                                buttons: [
                                    ["YES", "Add Existing"],
                                    ["NO", "Create New"]
                                ],
                            });

                            conf.listen((action) => {
                                conf.destroy();
                                if (action == "YES") {
                                    let blu = new ewars.Blocker(null, "Adding organization");
                                    ewars.tx("com.ewars.organization.add", [resp.uuid])
                                        .then((resp) => {
                                            blu.destroy();
                                            ewars.growl("Organization added");
                                            this.setState({
                                                org: null,
                                                showShade: false
                                            });
                                            ewars.emit("RELOAD_DT");
                                        })
                                }

                            })
                        }
                    } else {
                        ewars.growl("Organization updated");
                        this.setState({
                            org: null,
                            showShade: false
                        });
                        ewars.emit("RELOAD_DT");
                    }
                })
        } else {
            ewars.tx("com.ewars.organization.create", [this.state.org])
                .then((resp) => {
                    bl.destroy();

                    if (resp.err) {
                        if (resp.code == "EXISTING") {
                            let conf = new ewars.Confirmation({
                                icon: "fa-warning",
                                title: "Duplicate Organization",
                                body: "We found an existing organization within EWARS, would you like to add this organization instead",
                                buttons: [
                                    ["YES", "Add Existing"],
                                    ["NO", "Create New"]
                                ],
                            });

                            conf.listen((action) => {
                                conf.destroy();
                                if (action == "YES") {
                                    let blu = new ewars.Blocker(null, "Adding organization");
                                    ewars.tx("com.ewars.organization.add", [resp.uuid])
                                        .then((resp) => {
                                            blu.destroy();
                                            ewars.growl("Organization added");
                                            this.setState({
                                                org: null,
                                                showShade: false
                                            });
                                            ewars.emit("RELOAD_DT");
                                        })
                                }

                            })
                        }
                    } else {
                        ewars.growl("Organization created");
                        this.setState({
                            org: resp,
                            showShade: false
                        });
                        ewars.emit("RELOAD_DT");
                    }
                })
        }
    };

    _onFieldChange = (prop, value) => {
        this.setState({
            ...this.state,
            org: {
                ...this.state.org,
                [prop]: value
            }
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Organizations">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Create New"
                            icon="fa-plus"
                            onClick={this._create}/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            columns={COLUMNS}
                            actions={ACTIONS}
                            order={{"name.en": "ASC"}}
                            id="ORGANIZATIONS"
                            paused={this.state.showShade}
                            onCellAction={this._onDTAction}
                            resource="organization"/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    toolbar={false}
                    onAction={this._onShadeAction}
                    shown={this.state.showShade}>
                    {this.state.org ?
                        <ewars.d.Layout>
                            <ewars.d.Toolbar label="Editor">
                                <div className="btn-group pull-right">
                                    <Button
                                        icon="fa-save"
                                        color="green"
                                        onClick={this._save}
                                        label="Save Change(s)"/>
                                    <Button
                                        icon="fa-times"
                                        color="amber"
                                        onClick={this._onShadeAction}
                                        label="Close"/>
                                </div>
                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    <ewars.d.Panel>
                                        <SettingsForm
                                            onChange={this._onFieldChange}
                                            toolbar={false}
                                            dn={this.state.org}
                                            data={ORG_FIELDS}/>
                                    </ewars.d.Panel>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                        : null}
                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default OrganizationsManager;
