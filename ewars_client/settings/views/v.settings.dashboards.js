import actions from "../constants/actions";

import {
    Toolbar,
    Layout, Row, Cell,
    Button,
    Spinner,
    Shade
} from "../../common";

import {
    TextField,
    SelectField,
    LinkedListsField as LinkedLists
} from "../../common/fields";

import DashboardsList from "../controls/c.list.dashboards";

const USER_OPTIONS = [
    ["USER", _l("USER")],
    ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")],
    ["REGIONAL_ADMIN", _l("REGIONAL_ADMIN")]
];

const DEFAULT_DASHES = {
    USER: [
        {uuid: "USER_OVERVIEW", name: "Overview (Default}", order: 0},
        {uuid: "USER_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "USER_ALERTS", name: "Alerts (Default)", order: 2}
    ],
    ACCOUNT_ADMIN: [
        {uuid: "ACCOUNT_ADMIN_OVERVIEW", name: "Overview (Default)", order: 0},
        {uuid: "ACCOUNT_ADMIN_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "ACCOUNT_ADMIN_ALERTS", name: "Alerts (Default)", order: 2}
    ],
    REGIONAL_ADMIN: [
        {uuid: "REGIONAL_ADMIN_OVERVIEW", name: "Overview (Default)", order: 0},
        {uuid: "REGIONAL_ADMIN_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "REGIONAL_ADMIN_ALERTS", name: "Alerts (Default)", order: 2}
    ]
};

const modal_actions = [
    {label: "Close", action: "CLOSE"}
];

class DashRow extends React.Component {
    constructor(props) {
        super(props)


    }

    _toggle = () => {
    };

    _delete = () => {
        ewars.store.dispatch({
            type: actions.REMOVE_DASH,
            role: this.props.role,
            uuid: this.props.data.uuid
        })
    };

    _moveUp = () => {
        ewars.store.dispatch({
            type: actions.DASH_MOVE_UP,
            role: this.props.role,
            uuid: this.props.data.uuid
        })
    };

    _moveDown = () => {
        ewars.store.dispatch({
            type: actions.DASH_MOVE_DOWN,
            role: this.props.role,
            uuid: this.props.data.uuid
        })
    };

    render() {
        let label = "Loading...";
        if (this.props.data.name) {
            label = this.props.data.name;
        } else {
            if (this.props.dashes.length > 0) {
                let item;
                this.props.dashes.forEach(dash => {
                    if (dash.uuid == this.props.data.uuid) label = dash.name;
                })
            }
        }
        return (
            <tr>
                <th style={{textAlign: "left", padding: 13}}>{label}</th>
                <td>
                    <div className="btn-group pull-right">
                        <Button
                            onClick={this._moveUp}
                            icon="fa-caret-up"/>
                        <Button
                            onClick={this._moveDown}
                            icon="fa-caret-down"/>
                        <Button
                            onClick={this._delete}
                            icon="fa-trash"/>

                    </div>
                </td>
            </tr>
        )
    }
}

class SectionEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentWillMount() {
        this.state.data = ewars.copy(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = ewars.copy(this.props.data);
    }

    _onChange = (name, value) => {
        let data = ewars.copy(this.state.data);
        if (name == "item") {
            data[2] = value;
        }
        if (name == "name") {
            data[0] = value;
        }
        this.setState({
            data: data
        })
    };

    _save = () => {
        this.props.onSave(this.props.index, this.state.data);
    };

    render() {
        let config = {};
        config.multiple = true;

        config.options = this.props.dashboards.map((item) => {
            return [item.uuid, item.name]
        });
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Section">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._save}
                            label="Save Change(s)"/>
                        <ewars.d.Button
                            onClick={this.props.cancel}
                            label="Cancel"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll" style={{padding: 16}}>
                            <div className="ide-settings-form">
                                <div className="vsplit-box">
                                    <div className="ide-setting-label">Section Name</div>
                                    <div className="ide-setting-control">
                                        <TextField
                                            name="name"
                                            value={this.state.data[0]}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div style={{height: 20}}></div>

                                <div className="vslpit-box">
                                    <div className="ide-setting-label">Dashboards</div>
                                    <div className="ide-setting-control">
                                        <LinkedLists
                                            source={this.props.dashboards}
                                            target={this.state.data[2]}
                                            correlary="uuid"
                                            name="item"
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    _add = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: 5}}>{this.props.data.name}</ewars.d.Cell>
                        <ewars.d.Cell width={30}>
                            <ewars.d.Button
                                icon="fa-plus"
                                onClick={this._add}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

const reducer = function (state, action, data) {
    let t_state = ewars.copy(state);

    switch (action) {
        case "SET_DASHBOARDS":
            return Object.assign({}, t_state, {data: data});
        case "SET_ROLE":
            return Object.assign({}, t_state, {role: data});
        case "SHOW_SHADE":
            return Object.assign({}, t_state, {show: true});
        case "HIDE_SHADE":
            return Object.assign({}, t_state, {show: false});
        case "SET_VIEW":
            return Object.assign({}, t_state, {view: data});
        case "SHOW_DASH_SELECT":
            return Object.assign({}, t_state, {view: "DASHBOARD", show: true});
        case "ADD_DASH":
            t_state.dashboards[state.role].push(data);
            t_state.show = false;
            t_state.view = null;
            return t_state;
        case "SHOW_SECTION_EDIT":
            return Object.assign({}, t_state, {view: "SECTION", show: true, section: data || []});
        case "REMOVE_DASH":
            let items = ewars.copy(t_state.dashboards[t_state.role]);
            items.sort((a, b) => {
                let aO = a.order || a[1];
                let bO = b.order || b[1];
                if (aO < bO) return -1;
                if (aO > bO) return 1;
                return 0;
            });
            items.splice(data, 1);
            t_state.dashboards[t_state.role] = items;
            return t_state;
        case "SET_SECTION":
            let t_items = ewars.copy(t_state.dashboards[t_state.role]);
            t_items[data.index] = data.data;
            t_state.dashboards[t_state.role] = t_items;
            t_state.show = false;
            return t_state;
        case "ADD_SECTION":
            t_state.dashboards[t_state.role].push(["New Section", 999, []]);
            t_state.dashboards[t_state.role].sort((a, b) => {
                let aO = a.order || a[1];
                let bO = b.order || b[1];
                if (aO < bO) return -1;
                if (aO > bO) return 1;
                return 0;
            });
            let cur = 0;
            t_state.dashboards[t_state.role].forEach((item) => {
                if (item.uuid) item.order = cur;
                if (!item.uuid) item[1] = cur;
                cur++;
            });
            return t_state;
        case "EDIT_SECTION":
            t_state.view = "SECTION";
            t_state.show = true;
            t_state.section = t_state.dashboards[t_state.role].sort((a, b) => {
                let aO = a.order || a[1];
                let bO = b.order || b[1];
                if (aO < bO) return -1;
                if (aO > bO) return 1;
                return 0;
            })[data];
            t_state.sectionIndex = data;
            return t_state;
        case "DASH_MOVE_UP":
            if (data <= 0) return t_state;

            let dash_t = ewars.copy(t_state.dashboards[t_state.role]);

            dash_t.sort((a, b) => {
                let aO = a.order || a[1];
                let bO = b.order || b[1];
                if (aO < bO) return -1;
                if (aO > bO) return 1;
                return 0;
            });

            let tmp_t = dash_t[data - 1];
            dash_t[data - 1] = dash_t[data];
            dash_t[data] = tmp_t;

            let t_cur = 0;
            dash_t.forEach((item) => {
                if (_.isArray(item)) {
                    item[1] = t_cur;
                } else {
                    item.order = t_cur;
                }
                t_cur++;
            });

            t_state.view = null;
            t_state.show = false;
            t_state.dashboards[t_state.role] = dash_t;

            return t_state;
        case "DASH_MOVE_DOWN":
            let dash_d = ewars.copy(t_state.dashboards[t_state.role]);
            if (!dash_d[data + 1]) return t_state;

            dash_d.sort((a, b) => {
                let aO = a.order || a[1];
                let bO = b.order || b[1];
                if (aO < bO) return -1;
                if (aO > bO) return 1;
                return 0;
            });

            let tmp_d = dash_d[data + 1];
            dash_d[data + 1] = dash_d[data];
            dash_d[data] = tmp_d;

            let d_cur = 0;
            dash_d.forEach((item) => {
                if (_.isArray(item)) {
                    item[1] = d_cur;
                } else {
                    item.order = d_cur;
                }
                d_cur++;
            });

            t_state.show = false;
            t_state.view = null;
            t_state.dashboards[t_state.role] = dash_d;

            return t_state;
        default:
            return t_state;
    }
};

const _ID = "DASHBOARDS";

class DashboardsSettings extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = ewars.z.register(_ID, reducer, {
            role: "USER",
            data: null,
            show: false,
            dashboards: ewars.copy(props.account.dashboards)
        });
    }

    componentWillMount() {
        if (!this.state.data) {
            let query = {
                layout_type: {eq: "DASHBOARD"},
                status: {eq: "ACTIVE"}
            };
            ewars.tx("com.ewars.query", ["layout", ["uuid", "name"], query, null, null, null, null])
                .then(resp => {
                    if (this._isMounted) {
                        ewars.z.dispatch(_ID, "SET_DASHBOARDS", resp);
                    } else {
                        this.state = ewars.z.force(_ID, {...this.state, data: resp})
                    }
                })
        }

        ewars.z.subscribe(_ID, this._onChange);
    }

    _onChange = () => {
        this.setState(ewars.z.getState(_ID))
    };

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
        ewars.z.unsubscribe(_ID, this._onChange);
    }

    _onChangeRole = (prop, value) => {
        ewars.z.dispatch(_ID, "SET_ROLE", value);
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Updating dashboards");
        ewars.tx("com.ewars.account.dashboards.update", [this.state.dashboards])
            .then(resp => {
                bl.destroy();
            })
    };

    _showAdd = () => {
        ewars.z.dispatch("DASHBOARDS", "SHOW_SHADE");
    };

    _onModalAction = (action) => {
        this.setState({
            show: false
        })
    };

    _addDashboard = (dash) => {
        ewars.z.dispatch(_ID, "ADD_DASH", dash);
    };

    _onSaveSection = (index, data) => {
        ewars.z.force(_ID, {...this.state, view: "DASHBOARD", section: null, show: false});
        ewars.z.dispatch(
            _ID,
            this.state.sectionIndex != null ? "SET_SECTION" : "ADD_SECTION",
            {
                index: this.state.sectionIndex,
                data: data
            }
        );
    };

    _cancelSection = () => {
        ewars.z.force(_ID, {...this.state, view: "DASHBOARD", section: null, show: false});
        ewars.z.signal(_ID);
    };

    render() {
        if (!this.state.data) return <Spinner/>;
        let cur_dashes = this.state.dashboards[this.state.role];

        let value = cur_dashes.map(item => {
            return item.uuid;
        });


        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Dashboard">
                    <div className="btn-group pull-right">
                        <Button
                            icon="fa-save"
                            color="green"
                            label={_l("SAVE_CHANGES")}
                            onClick={this._save}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="ide-settings-panel">
                                <div className="ide-settings-content">
                                    <div className="ide-settings-form">

                                        <div className="ide-section-basic">

                                            <div className="vsplit-box">
                                                <div className="ide-setting-label">Role</div>
                                                <div className="ide-setting-control">
                                                    <SelectField
                                                        name="role"
                                                        value={this.state.role}
                                                        config={{options: USER_OPTIONS}}
                                                        onUpdate={this._onChangeRole}/>
                                                </div>
                                            </div>

                                            <DashboardsList
                                                role={this.state.role}
                                                active={value}
                                                onAction={this._onDashAction}
                                                dashboards={this.state.data}
                                                data={cur_dashes}/>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Dashboards"
                    icon="fa-gauge"
                    toolbar={false}
                    shown={this.state.show}
                    onAction={this._onModalAction}
                    actions={modal_actions}>
                    {this.state.view == "DASHBOARD" ?
                        <div className="block-tree" style={{height: 400}}>
                            {this.state.data.map(item => {
                                return <Dashboard
                                    data={item}
                                    user_type={this.state.role}
                                    onClick={this._addDashboard}/>
                            })}
                        </div>
                        : null}
                    {this.state.view == "SECTION" ?
                        <SectionEditor
                            onSave={this._onSaveSection}
                            data={this.state.section}
                            cancel={this._cancelSection}
                            dashboards={this.state.data}/>
                        : null}
                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default DashboardsSettings;
