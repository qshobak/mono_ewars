class TabInstance {
    constructor(id, data) {
        this.id = id;
        this.data = data;
    }
}

let state;

class ComponentState {
    constructor(props) {
        state = this;
        this.props = props;
        this.tabs = [];
        this.activeTab = 'DEFAULT';
    }

    setTab = (id) => {

    };

    closeTab = (id) => {

    };

    addTab = (id, data) => {
        this.tabs.push(new TabInstance(id, data));
    };
}

export {state, ComponentState};
export default ComponentState;