export default [
    "General settings",
    {
        l: "Default Bounds",
        t: "bounds",
        n: "ALARM_DEFAULT_BOUNDS",
        h: "Draw the default bounds that you would like alert maps to focus to"
    },
    {
        l: "Location type",
        t: "select",
        n: "ALARM_DEFAULT_SITE_TYPE",
        h: "Select the location type which should default to being visualized on alert maps",
        c: {
            optionsSource: {
                resource: "location_type",
                labelSource: "name",
                valSource: "id",
                query: {}
            }
        },
    },
    "Misc.",
    {
        l: "Notify on Auto-Discard",
        t: "button_group",
        n: "ALARM_NOTIFY_AUTODISCARD",
        c: {
            options: [
                [true, "Yes"],
                [false, "No"]
            ]
        },
        h: "Notify all stakeholders when an alert is auto-discarded?"
    }
    // {
    //     l: "Default auto-discard days",
    //     t: "number",
    //     n: "ALARM_DEFAULT_AUTODISCARD",
    //     h: "The default number of days after which an alert will be auto-discarded"
    // }
]