export default [
    "Location Types",
    {
        l: "Location types",
        t: "text",
        n: "LOC_TYPES"
    },
    {
        l: "Allow user created",
        t: "button_group",
        n: "LOC_ALLOW_CREATE",
        c: {
            options: [
                ["ENABLED", "Yes"],
                ["DISABLED", "No"]
            ]
        }
    }
]