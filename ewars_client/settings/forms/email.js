export default {
    override: {
        type: "switch",
        label: "Use Custom"
    },
    type: {
        type: "select",
        label: "Email System Type",
        options: [
            ["SES", "Amazon SES"],
            ["IMAP", "IMAP"],
            ["POP", "POP"]
        ]
    },
    access_key: {
        type: "text",
        label: "Amazon AWS Access Key ID"
    },
    aws_secret_key: {
        type: "text",
        label: "Amazon AWS Secret Key"
    },
    sender: {
        type: "text",
        label: "Sender"
    },
    reply_to: {
        type: "text",
        label: "Reply-To"
    },
    pre_subject: {
        type: "text",
        label: "Subject Prefix"
    },
    host: {
        type: "text",
        label: "Email Host"
    },
    password: {
        type: "text",
        label: "Email Account Password"
    },
    rate_limit: {
        type: "number",
        label: "Sending Rate Limit (emails/sec.)",
        help: {en: "Limit the rate at which emails are sent out from the system"}
    }
}