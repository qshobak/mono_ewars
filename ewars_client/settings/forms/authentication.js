export default {
    auth_type: {
        type: "select",
        label: "Authentication System",
        options: [
            ["EWARS_SSO", "EWARS Single Sign-On"],
            ["LDAP", "LDAP"]
       ]
    }
}