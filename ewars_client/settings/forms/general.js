export default {
    name: {
        type: "text",
        label: "Account Name",
        required: true
    },
    logo: {
        type: "file",
        label: "Logo Image"
    },
    icon_flag: {
        type: "text",
        label: "Flag Icon"
    },
    default_language: {
        type: "select",
        label: "Default Language",
        options: [
            ["en", "English [en]"],
            ["fr", "French [fr]"]
        ]
    },
    domain: {
        type: "text",
        label: "Custom Domain"
    },
    flag_code: {
        type: "text",
        label: "ISO2 Code"
    },
    description: {
        type: "textarea",
        label: "Description"
    },
    account_logo: {
        type: "text",
        label: "Account Logo"
    },
    account_flag: {
        type: "text",
        label: "Account Flag"
    }
}