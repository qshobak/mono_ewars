export default [
    {
        n: 'Appearance & Behaviour',
        c: [
            {n: 'Account'},
            {n: 'Theme'},
            {n: 'Email'},
            {n: 'Notifications'},
            {n: 'Security & Authentication'},
            {n: 'Menu'},
            {
                n: 'Legal',
                c: [
                    {n: 'Terms of use'},
                    {n: 'Privacy policy'}
                ]
            },
            {n: 'Translation(s)'}
        ]
    },
    {
        n: 'Alarms',
        c: [
            {n: 'Notifications'},
            {n: 'Export'},
            {n: 'Integrations'}
        ]
    },
    {
        n: 'Data sets'
    },
    {
        n: 'Locations',
        c: [
            {n: 'Location types'},
            {n: 'Location groups'},
            {n: 'Identification codes'},
            {n: 'Export'},
            {n: 'Import'},
            {n: 'Profiles'},
            {n: 'Integrations'},
            {n: 'Reporting'},
            {n: 'Location statuses'}
        ]
    },
    {
        n: 'Users',
        c: [
            {n: 'Assignments'},
            {n: 'Roles'},
            {n: 'Organizations'},
            {n: 'Profile extensions'},
            {n: 'Teams'},
            {n: 'Distribution lists'},
            {n: 'Invitations'}
        ]
    },
    {
        n: 'Forms',
        c: [
            {n: 'Integrations'},
            {n: 'Push API'}
        ]
    },
    {
        n: 'Devices',
        c: [
            {n: 'Inventory'},
            {n: 'SMS gateways'},
            {n: 'WAP gateway'},
            {n: 'Relay nodes'},
            {n: 'Event log'}
        ]
    },
    {
        n: 'Workflows',
        c: [
            {n: 'Alert'},
            {n: 'Form'},
            {n: 'System'}
        ]
    },
    {
        n: 'Hub',
        c: [
            {n: 'Subscriptions'},
            {n: 'Shared data'},
            {n: 'Subscribers'}
        ]
    },
    {
        n: 'Content',
        c: [
            {n: 'Feeds'},
            {n: 'Wikis'},
            {n: 'Digest emails'},
            {n: 'Notebooks'},
            {n: 'HUDs'},
            {n: 'Documents'},
            {n: 'Dashboards'},
            {n: 'Plots'},
            {n: 'Maps'}
        ]
    },
    {
        n: 'Components'
    },
    {
        n: 'Supplementary data',
        c: [
            {n: 'Data sets'},
            {n: 'External connectors'}
        ]
    },
    {
        n: 'API Access'
    },
    {
        n: 'System administration',
        c: [
            {n: 'Usage'},
            {n: 'Data import'},
            {n: 'Data export'},
            {n: 'Backup(s)'},
            {n: 'System logs'},
            {n: 'System backpressure'},
            {n: 'Performance'},
            {n: 'System notices'},
            {n: 'EWARS releases'},
            {n: 'Translation(s)'},
            {n: 'PDF cache'},
            {n: 'Indexing'}
        ]
    }
]
