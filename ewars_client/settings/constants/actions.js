export const DASH_MOVE_UP = "DASH_MOVE_UP";
export const DASH_MOVE_DOWN = "DASH_MOVE_DOWN";
export const DASH_TOGGLE = "DASH_TOGGLE";
export const ADD_DASH = "ADD_DASH";
export const REMOVE_DASH = "REMOVE_DASH";
export const SET_PROP = "SET_PROP";
export const ADD_SECTION = "ADD_SECTION";
export const SET_SECTION = "SET_SECTION";

export const UPDATE_ACCOUNT_PROP = "UPDATE_ACCOUNT_PROP";

export const SET_ACCOUNT = "SET_ACCOUNT";

export default {
    DASH_MOVE_UP,
    DASH_MOVE_DOWN,
    DASH_TOGGLE,
    UPDATE_ACCOUNT_PROP,
    SET_ACCOUNT,
    ADD_DASH,
    REMOVE_DASH,
    SET_PROP,
    SET_SECTION,
    ADD_SECTION
}