export default [
    ['fa-cogs', 'GENERAL', 'General Settings'],
    ['fa-alarms', 'ALARMS', 'Alarms'],
    ['fa-clipboard', 'ASSIGNMENTS', 'Assignments'],
    ['fa-map-marker', 'LOCATIONS', 'Locations'],
    ['fa-tachometer', 'DASHBOARDS', 'Dashboards'],
    ['fa-phone', 'DEVICES', 'Devices'],
    ['fa-building', 'ORGANIZATIONS', 'Organizations'],
    ['fa-user', 'USERS', 'Users'],
    ['fa-group', 'ROLES', 'Roles'],
    ['fa-send', 'INVITES', 'Invitations'],
    ['fa-paint-brush', 'THEME', 'Theme']
]