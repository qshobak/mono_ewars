import actions from "../constants/actions";

const DEFAULT_DASHES = {
    USER: [
        {uuid: "USER_OVERVIEW", name: "Overview (Default)", order: 0},
        {uuid: "USER_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "USER_ALERTS", name: "Alerts (Default)", order: 2}
    ],
    ACCOUNT_ADMIN: [
        {uuid: "ACCOUNT_ADMIN_OVERVIEW", name: "Overview (Default)", order: 0},
        {uuid: "ACCOUNT_ADMIN_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "ACCOUNT_ADMIN_ALERTS", name: "Alerts (Default)", order: 2}
    ],
    REGIONAL_ADMIN: [
        {uuid: "REGIONAL_ADMIN_OVERVIEW", name: "Overview (Default)", order: 0},
        {uuid: "REGIONAL_ADMIN_SURVEILLANCE", name: "Surveillance (Default)", order: 1},
        {uuid: "REGIONAL_ADMIN_ALERTS", name: "Alerts (Default)", order: 2}
    ]
};

function _setAccount(state, data) {
    state = data;

    if (!state.dashboards.USER) state.dashboards.USER = DEFAULT_DASHES.USER;
    if (!state.dashboards.ACCOUNT_ADMIN) state.dashboards.ACCOUNT_ADMIN = DEFAULT_DASHES.ACCOUNT_ADMIN;
    if (!state.dashboards.REGIONAL_ADMIN) state.dashboards.REGIONAL_ADMIN = DEFAULT_DASHES.REGIONAL_ADMIN;

    return state;
}

function _addDash(state, userType, dash) {
    let item = {uuid: dash.uuid, order: 9999};
    state.dashboards[userType].push(item);

    let count = 0;
    let dashes = state.dashboards[userType];
    dashes.sort((a, b) => {
        if (a.order > b.order) return 1;
        if (a.order < b.order) return -1;
        return 0;
    });

    dashes.forEach(dash => {
        dash.order = count;
        count++;
    });

    state.dashboards[userType] = dashes;

    return state;
}

function _removeDash(state, role, uuid) {
    let index;
    state.dashboards[role].forEach((item, i) => {
        if (item.uuid == uuid) index = i;
    });

    if (index != null && index != undefined) {
        state.dashboards[role].splice(index, 1);
    }

    let dashboards = state.dashboards[role];
    dashboards.sort((a, b) => {
        if (a.order > b.order) return 1;
        if (a.order < b.order) return -1;
        return 0;
    });

    let count = 0;
    dashboards.forEach(item => {
        item.order = count;
        count++;
    });
    state.dashboards[role] = dashboards;

    return state;
}

function cmp(a, b) {
    if (a.order > b.order) return 1;
    if (a.order < b.order) return -1;
    return 0;
}

function _moveUp(state, role, uuid) {
    let dashes = state.dashboards[role];

    dashes.sort(cmp);

    let index;
    dashes.forEach((item, i) => {
        if (item.uuid == uuid) index = i;
    });

    if (index > 0) {
        let targetIndex = index - 1;
        let tmp = dashes[targetIndex];
        dashes[targetIndex] = dashes[index];
        dashes[index] = tmp;
    }

    let count = 0;
    dashes.forEach(dash => {
        dash.order = count;
        count++;
    });

    state.dashboards[role] = dashes;

    return state;
}

function _moveDown(state, role, uuid) {
    let dashes = state.dashboards[role];

    dashes.sort(cmp);

    let index;
    dashes.forEach((item, i) => {
        if (item.uuid == uuid) index = i;
    });

    if (index < dashes.length) {
        let targetIndex = index + 1;
        let tmp = dashes[targetIndex];
        dashes[targetIndex] = dashes[index];
        dashes[index] = tmp;
    }

    let count = 0;
    dashes.forEach(dash => {
        dash.order = count;
        count++;
    });

    state.dashboards[role] = dashes;

    return state;
}

function _setProp(state, prop, value) {
    state[prop] = value;
    return state;
}

function _addSection(state, role, data) {
    state.dashboards[role].push(data);
    return state;
}

const account = (state = {}, action) => {
    let tState = ewars.copy(state);

    switch (action.type) {
        case actions.SET_ACCOUNT:
            return _setAccount(tState, action.data);
        case actions.ADD_DASH:
            return _addDash(tState, action.userType, action.data);
        case actions.REMOVE_DASH:
            return _removeDash(tState, action.role, action.uuid);
        case actions.DASH_MOVE_DOWN:
            return _moveDown(tState, action.role, action.uuid);
        case actions.DASH_MOVE_UP:
            return _moveUp(tState, action.role, action.uuid);
        case actions.SET_PROP:
            return _setProp(tState, action.prop, action.value);
        case actions.SET_SECTION:
            return tState;
        case actions.ADD_SECTION:
            return _addSection(tState, action.role, action.data);
        default:
            return tState;
    }
};

export default account;
