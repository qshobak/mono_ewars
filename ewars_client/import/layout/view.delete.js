class Delete extends React.Component {
    constructor(props) {
        super(props);
    }

    _delete = () => {
        ewars.prompt('fa-trash', 'Delete Import Project?', 'Are you sure you want to delete this project? All data associated with the project (including imported records) will be destroyed.', () => {
            let bl = new ewars.Blocker(null, 'Deleting project...');
            ewars.tx('com.ewars.import.delete', [this.props.data.uuid])
                .then(resp => {
                    bl.destroy();
                    ewars.growl('Project being deleted, this may take some time.')
                    this.props.onDelete();
                })
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div style={{margin: '30px auto', width: '500px', padding: '8px', background: 'rgba(0,0,0,0.1)'}}>
                            <h3 style={{fontSize: '16px', marginBottom: '8px'}}>Delete Import Project</h3>

                            <p>If you delete this project, any unimported <b style={{fontWeight: 'bold', lineHeight: '12px'}}>as well as imported</b> data will be
                                deleted from the system.</p>

                            <div className="clearer" style={{height: '30px'}}></div>
                            <div className="btn-group">
                                <ewars.d.Button
                                    color="red"
                                    label="Delete Project"
                                    icon="fa-trash"
                                    onClick={this._delete}/>
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Delete;