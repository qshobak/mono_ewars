import { FileField as FileUploadField } from "../../common/fields";
import Project from "./Project";
import ProjectList from "../components/ProjectList";
import { DataTable } from "../../common";

const STYLES = {
    enter: {
        margin: "90px auto",
        display: "block",
        width: 800,
        textAlign: "center"
    },
    icon: {
        fontSize: "100px",
        marginBottom: 8
    },
    par: {
        marginBottom: 8
    }
};

const COLUMNS = [
    {
        name: "name",
        config: {
            label: "Project Name",
            type: "text"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["LOADED", "Data Loaded"],
                ["IN_PROGRESS", "In Progress"],
                ["COMPLETE", "Completed"]
            ]
        }
    },
    {
        name: "_status",
        config: {
            label: "Status",
            type: "text"
        },
        fmt: function (val, row) {
            if (row.meta_map.imported < row.meta_map.total_rows) {
                return "In Progress";
            }

            if (row.meta_map.imported >= row.meta_map.total_rows) {
                return "Complete";
            }
        }
    },
    {
        name: "target_type",
        config: {
            label: "Target Import Type",
            type: "select",
            options: [
                ["FORM", "Form"],
                ["DATASET", "Dataset"]
            ]
        }
    },
    {
        name: "form.name",
        config: {
            label: "Form",
            type: "select",
            filterKey: "form_id"
        },
        fmt: ewars.I18N
    },
    {
        name: "meta_map.total_rows",
        config: {
            label: "Total Items",
            type: "number"
        },
        fmt: (val) => {
            return ewars.NUM(val, "0,0");
        }
    },
    {
        name: "meta_map.imported",
        config: {
            label: "Items Imported",
            type: "number"
        },
        fmt: (val) => {
            return ewars.NUM(val, "0,0");
        }
    },
    {
        name: 'created',
        config: {
            label: 'Created',
            type: 'date'
        },
        fmt: ewars.DATE
    },
    {
        name: "last_modified",
        config: {
            label: "Last Modified",
            type: "date"
        },
        fmt: ewars.DATE
    },
    {
        name: "user.name",
        config: {
            label: "Created by",
            type: "user",
            filterKey: "created_by"
        }
    }
];

const JOINS = [
    "form:form_id:id:id,name",
    "user:created_by:id:id,name"
];

const ACTIONS = [
    {icon: "fa-pencil", action: "EDIT"}
]

class DefaultView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showUpload: false
        }
    }

    _createNew = () => {
        this.setState({
            showUpload: true
        })
    };

    _fileUpped = (name, value) => {
        let bl = new ewars.Blocker(null, "Creating import project...");
        ewars.tx("com.ewars.import.create", [value])
            .then((resp) => {
                bl.setMessage("Extrapolating data...");
                this._setTime(resp, bl);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _setTime = (project, blocker) => {
        let self = this;
        ewars._t = window.setInterval(() => {
            ewars.tx("com.ewars.import.status", [project.uuid])
                .then((resp) => {
                    if (resp == "LOADED") {
                        blocker.destroy();
                        clearInterval(ewars._t);
                        ewars.tx("com.ewars.resource", ["import_project", project.uuid, null, null])
                            .then((resp) => {
                                self.props.onProject(resp);
                            })
                    }
                })
                .catch((err) => {
                    blocker.destroy();
                    clearInterval(ewars._t);
                })
        }, 5000)
    };

    _modalAction = () => {
        this.setState({
            showUpload: false
        })
    };

    _onCellAction = (action, cell, row) => {
        if (action == "CLICK") {
            this.props.onProject(row);
        }

        if (action == "EDIT") {
            this.props.onProject(row);
        }
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Import Projects (beta)">
                    <div className="btn-group pull-right" style={{marginTop: "-4px"}}>
                        <FileUploadField
                            name="file"
                            local={true}
                            showInput={false}
                            showPreview={false}
                            label="Upload CSV for new Project"
                            onUpdate={this._fileUpped}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            resource="import_project"
                            actions={ACTIONS}
                            columns={COLUMNS}
                            join={JOINS}
                            initialOrder={{created: 'DESC'}}
                            onCellAction={this._onCellAction}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }

}


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT"
        }
    }

    _setProject = (project) => {
        this.setState({
            project: project,
            view: "PROJECT"
        })
    };

    _setClosed = () => {
        this.setState({
            view: "DEFAULT",
            project: null
        })
        ewars.emit("RELOAD_DT");
    };

    render() {

        let view;
        if (this.state.view == "DEFAULT") view = <DefaultView onProject={this._setProject}/>;
        if (this.state.view == "PROJECT") view = <Project data={this.state.project} onClose={this._setClosed}/>;

        return view;
    }
}

export default Component;
