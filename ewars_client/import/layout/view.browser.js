import Imports from './view.imports';


class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        let style = {borderLeft: '5px solid transparent'};
        if (this.props.data.status == 'ACTIVE') style.borderColor = 'green';
        if (this.props.data.status == 'DRAFT') style.borderColor = 'orange';

        return (
            <div className="block" style={style} onClick={this._click}>
                <div className="block-content">{this.props.data.name}</div>
            </div>
        )
    }
}

class Browser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            import: null
        }

        ewars.tx('com.ewars.import.importables', [])
            .then(resp => {
                this.setState({
                    data: resp
                })
            })
    }

    _onSelect = (item) => {
        this.setState({import: item})
    };

    render() {

        let items = this.state.data.sort((a, b) => {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
        });

        let view;
        if (this.state.import) view = <Imports data={this.state.import}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="350px">
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: 'relative'}}>
                                {items.map(item => {
                                    return <Item
                                        onClick={this._onSelect}
                                        data={item}/>
                                })}
                            </div>
                        </ewars.d.Panel>

                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Browser;
