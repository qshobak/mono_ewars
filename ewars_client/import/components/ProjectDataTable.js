import { DataTable } from "../../common";
import Moment from "moment";

const ERRORS = [
    "NO_MATCH",
    "INVALID_DATE",
    "INVALID_SELECT",
    "NO_MAP",
    "MULTIPLE_MATCHES",
    "IGNORE_ROW",
    "LOC_NOT_FOUND",
    "DATE_FUTURE"
];

const ACTION = [
    {icon: "fa-trash", action: "DELETE"}
];

const JOINS = [
    "location:location_id:uuid:uuid,name"
];

const cellColorFn = (val) => {
    if (ERRORS.indexOf(val[1]) >= 0) {
        return "#5c3b3c";
    } else if (val[1] == "NULL") {
        return "#615d10";
    } else {
        return "transparent";
    }
}

const cellColorFnOther = (val) => {
    if (ERRORS.indexOf(val) >= 0) {
        return "#5c3b3c";
    } else if (val == "NULL") {
        return "#615d10";
    } else if (val == null) {
        return "#615d10";
    } else {
        return "transparent";
    }
};

const statusErrColorFn = (val) => {
    if (val == 'CONFLICT') return 'red';
    return 'transparent';
}


class ProjectDataTable extends React.Component {
    static defaultProps = {
        readOnly: false
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            editing: null
        }
    }

    _onAction = (action, cell, row) => {
        if (action == "DELETE") {
            this._delete(row);
        }
    };

    _delete = (row) => {
        let bl = new ewars.Blocker(null, "Removing row...");
        ewars.prompt("fa-trash", "DELETE_ROW", "DELETE_ROW_TXT", function() {
            ewars.tx("com.ewars.import.delete_row", [row.uuid])
                .then((resp) => {
                    bl.destroy();
                    ewars.emit("RELOAD_DT");
                })
        })
    };

    render() {
        let self = this;

        let realColumns = {};
        for (let i in this.props.data.mapping) {
            let def = this.props.data.mapping[i];
            if (def.target) {
                if (def.target != "IGNORE") {
                    realColumns[i] = def;
                }
            }
        }

        let canEdit = !this.props.readOnly;

        let filter = {};
        filter.project_id = {eq: this.props.data.uuid};
        let columns = [
            {
                name: "status",
                config: {
                    label: "Import Status",
                    type: "select",
                    options: [
                        ["BASE", "Unimported"],
                        ["ERROR", "Errors"],
                        ["DONE", "Imported"]
                    ]
                },
                color: statusErrColorFn
            },
            {
                name: "dry",
                config: {
                    label: "Run State",
                    type: "text"
                },
                fmt: function (val, row) {
                    let isValid = true;
                    let values = []


                    for (let i in realColumns) {
                        values.push(row.data[i][1])
                        if (row.data[i][1] != null && row.data[i][1] != undefined) {
                            if (ERRORS.indexOf(row.data[i][1]) >= 0) {
                                isValid = false;
                            }
                        }
                    }

                    if (row.data_date) {
                        if (ERRORS.indexOf(row.data_date) >= 0) {
                            isValid = false;
                        }
                    } else {
                        isValid = false;
                    }

                    if (row.location_id) {
                        if (ERRORS.indexOf(row.location_id) >= 0) {
                            isValid = false;
                        }
                    } else {
                        isValid = false;
                    }

                    if (!isValid) {
                        return __("INVALID")
                    } else {
                        return __("VALID");
                    }
                }
            },
            {name: "uuid", config: {label: "UUID"}},
            {
                name: "data_date",
                config: {
                    label: "=> Report Date",
                    type: "date"
                },
                fmt: (val) => {
                    if (ERRORS.indexOf(val) < 0) {
                        return ewars.DATE(val, "DAY")
                    } else {
                        return val;
                    }
                },
                color: cellColorFnOther
            },
            {
                name: "location_id",
                config: {
                    label: "=> Location",
                    type: "location",
                    filterKey: "location_id"
                },
                fmt: function (val, row) {
                    if (ERRORS.indexOf(val) >= 0) {
                        let lid_key = self.props.data.meta_map.lid_key;
                        let raw = row.data[lid_key][0];
                        return val + " (" + raw + ")";

                    } else {
                        return val;
                    }
                },
                editable: canEdit,
                editor: {
                    type: "TEXT",
                    onChange: function (col, row, value) {
                        let bl = new ewars.Blocker(null, "Updating property...");
                        ewars.tx("com.ewars.import.update_prop", [row.uuid, col.editor.editColumn, value])
                            .then((resp) => {
                                ewars.emit("RELOAD_DT");
                                bl.destroy();
                                ewars.growl("Re-validate to see changes");
                            })
                    },
                    editColumn: `data.${this.props.data.meta_map.lid_key}` || null,
                    editIndex: 0
                },
                color: cellColorFnOther
            },
            {
                name: "location.name",
                config: {
                    label: "Location",
                    type: "location",
                    filterKey: "location_id"
                },
                fmt: ewars.I18N
            }

        ];

        let sMapping = [];
        for (let i in this.props.data.mapping) {
            sMapping.push([i, this.props.data.mapping[i]]);
        }

        sMapping.sort((a, b) => {
            if (a[0] > b[0]) return 1;
            if (a[0] < b[0]) return -1;
            return 0;
        })

        sMapping.forEach((srcDef) => {
            let i = srcDef[0];
            let def = srcDef[1];
            let show = true;
            let targetFieldLabel = "";

            let formFunction;

            if (def.target) {
                if (def.target == "IGNORE") {
                    show = false;
                } else {
                    let targetField;
                    switch (def.target) {
                        case "location_id":
                            targetField = {label: "Location", type: "location"};
                            break;
                        case "data_date":
                            targetField = {label: "Report Date", type: "date"};
                            break;
                        default:
                            targetField = this.props.fields.find((item) => {
                                return item.name == def.target;
                            })
                            break;
                    }

                    if (targetField) {
                        targetFieldLabel = ` => ${__(targetField.label)}`;

                        if (targetField.type == "date") {
                            if (def.date_format) {
                                formFunction = (val) => {
                                    if (def.null_ && (val == null || val == undefined)) {
                                        switch (def.null_) {
                                            case "NULL":
                                                return "null";
                                            case "ZERO":
                                                return 0;
                                            default:
                                                return "null";

                                        }
                                    }
                                    return Moment.utc(val, def.date_format).format("YYYY-MM-DD");
                                }
                            }
                        } else {
                            formFunction = (val) => {
                                if (def.null_ && (val == null || val == undefined || val == "")) {
                                    switch (def.null_) {
                                        case "NULL":
                                            return "null";
                                        case "ZERO":
                                            return 0;
                                        default:
                                            return "null";

                                    }
                                }
                                return val;
                            }
                        }
                    }

                }
            }

            if (show) {
                columns.push({
                    name: "data." + i,
                    config: {
                        label: i + targetFieldLabel,
                        type: "text"
                    },
                    fmt: function (val) {
                        let realVal = val[0];
                        if (realVal == null || realVal == undefined || realVal == "") realVal = "null";
                        let mappedVal = val[1];

                        if (mappedVal == null || mappedVal == undefined) {
                            return `${realVal} => INVALID`;
                        } else {
                            return `${realVal} => ${mappedVal}`;
                        }

                        // return `${realVal} => ${mappedVal}`;
                    },
                    editable: canEdit,
                    editor: {
                        type: "TEXT",
                        onChange: function (col, row, value) {
                            let bl = new ewars.Blocker(null, "Updating property...");
                            ewars.tx("com.ewars.import.update_prop", [row.uuid, col.name, value])
                                .then((resp) => {
                                    ewars.emit("RELOAD_DT");
                                    bl.destroy();
                                    ewars.growl("Re-validate to see changes");
                                })
                        },
                        editIndex: 0
                    },
                    color: cellColorFn
                })
            }
        });

        let actions = ewars.copy(ACTION);
        if (!canEdit) actions.pop();

        return (
            <ewars.d.Row>
                <ewars.d.Cell>
                    <DataTable
                        id={"ALL_DATA" + this.props.data.uuid}
                        paused={this.state.shown}
                        columns={columns}
                        onCellAction={this._onAction}
                        filter={filter}
                        actions={actions}
                        join={JOINS}
                        initialSort={{"report_date": "DESC"}}
                        resource="import_data"/>
                </ewars.d.Cell>
                {this.state.shown ?
                    <ewars.d.Shade
                        shown={this.state.shown}
                        onAction={this._onAction}
                        actions={[]}
                        title="Entry Editor">

                    </ewars.d.Shade>
                    : null}
            </ewars.d.Row>
        )
    }
}

export default ProjectDataTable;
