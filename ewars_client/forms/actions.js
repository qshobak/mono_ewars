// Definition actions
export const ADD_FIELD = "ADD_FIELD";
export const REMOVE_FIELD = "REMOVE_FIELD";
export const SET_FIELD_PROP = "SET_FIELD_PROP";
export const FIELD_UP = "FIELD_UP";
export const FIELD_DOWN = "FIELD_DOWN";
export const DUPLICATE_FIELD = "DUPLICATE_FIELD";
export const SET_FORM_DEFINITION = "SET_FORM_DEFINITION";
export const SET_FORM_LOGIC = "SET_FORM_LOGIC";
export const UPDATE_CIRCUIT = "UPDATE_CIRCUIT";
export const REMAP_CIRCUIT = "REMAP_CIRCUIT";
export const CIRCUIT_REMOVE_ITEM = "CIRCUIT_REMOVE_ITEM";
export const CIRCUIT_UPDATE_REDUCTION = "CIRCUIT_UPDATE_REDUCTION";
export const SET_CHANGE_RULE = "SET_CHANGE_RULE";
export const ADD_SET = "ADD_SET";
export const SET_ADD_FIELD = "SET_ADD_FIELD";
export const REMOVE_SET = "REMOVE_SET";
export const UPDATE_SET_ITEM = "UPDATE_SET_ITEM";

// Form actions
export const SET_FORM_PROP = "SET_FORM_PROP";
export const SET_FORM = "SET_FORM";
export const UNSET_FORM = "UNSET_FORM";
export const ADD_FEATURE = "ADD_FEATURE";
export const REMOVE_FEATURE = "REMOVE_FEATURE";
export const SET_FEATURE_PROP = "SET_FEATURE_PROP";
export const ADD_INTEGRATION = "ADD_INTEGRATION";
export const REMOVE_INTEGRATION = "REMOVE_INTEGRATION";

// Logic Actions

// UI Actions
export const SET_VIEW = "SET_VIEW";

// Editor actions
export const SET_EDIT_VIEW = "SET_EDIT_VIEW";
export const SET_LOADED = "SET_LOADED";
export const SET_UNLOADED = "SET_UNLOADED";
export const SET_ERRORS = "SET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const SET_SETTINGS_VIEW = "SET_SETTINGS_VIEW";

