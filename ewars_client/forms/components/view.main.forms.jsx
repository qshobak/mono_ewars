class ViewFormsMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="34px" style={{position: "relative", overflow: "hidden"}} borderRight={true}>
                        <div className="ide-tabs">
                            <div className="ide-tab"><i className="fal fa-clipboard"></i>&nbsp;Forms</div>
                            <div className="ide-tab"><i className="fal fa-tree"></i>&nbsp;Form Groups</div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewFormsMain;
