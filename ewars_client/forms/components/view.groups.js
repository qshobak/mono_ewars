import ModalComponent from "../../common/c.modal";

const DEFAULTS = [
    {
        uuid: "PINNED",
        name: {en: "Pinned"},
        pinned: true,
        order: -1,
        forms: []
    },
    {
        uuid: "EARLY_WARNING",
        name: {en: "Early Warning"},
        order: 0,
        forms: []
    },
    {
        uuid: "ALERT",
        name: {en: "Alert"},
        order: 1,
        forms: []
    },
    {
        uuid: "RESPONSE",
        name: {en: "Response"},
        order: 2,
        forms: []
    },
    {
        uuid: "OTHER",
        name: {en: "Other"},
        order: 3,
        forms: []
    }
]

class FormControl extends React.Component {
    constructor(props) {
        super(props);
    }

    _dragStart = (e) => {
        window.dispatchEvent(new CustomEvent("FORM_DRAG_START"));
        e.dataTransfer.setData("uuid", this.props.data.uuid);
    };

    _dragEnd = (e) => {
        window.dispatchEvent(new CustomEvent("FORM_DRAG_STOP"));
    };

    render() {
        let className = "block-content";
        if (this.props.data.status == "ACTIVE") {
            className += " status-green";
        } else {
            className += " status-black";
        }
        return (
            <div
                onDragStart={this._dragStart}
                onDragEnd={this._dragEnd}
                draggable={true}
                className="block">
                <div className={className}>
                    {__(this.props.data.name)}
                </div>
            </div>
        )
    }
}

class GroupHeader extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="dash-header">{this.props.data.name}</div>
        )
    }
}

const ACTIONS = [
    ["fa-caret-up", "MOVE_UP", "Move Up"],
    ["fa-caret-down", "MOVE_DOWN", "Move Down"],
    ["fa-trash", "DELETE", "DELETE"]
]

class FormGroupItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case "DELETE":
                this.props.removeForm(this.props.data.uuid);
                break;
            case "MOVE_UP":
                this.props.moveFormUp(this.props.data.uuid);
                break;
            case "MOVE_DOWN":
                this.props.moveFormDown(this.props.data.uuid);
                break;
            default:
                break;
        }
    };

    render() {
        let form = this.props.forms.filter(item => {
            return item.uuid == this.props.data.uuid;
        })[0] || null;
        let label = "Unknown Form";
        if (form) label = __(form.name);

        return (
            <div
                draggable={true}
                className="adm-form-group-item">
                <div className="adm-form-group-fname">{label}</div>
                <div className="adm-form-group-fcontrols">
                    <ewars.d.ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}
                        right={true}/>
                </div>
            </div>
        )
    }
}

const HEADER_ACTIONS = [
    ["fa-pencil", "EDIT", "Edit Group Name"]
]

class FormGroup extends React.Component {
    static defaultProps = {
        data: {
            name: {en: "New Group"}
        },
        pinned: false
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener("FORM_DRAG_START", this._dragStart);
        window.addEventListener("FORM_DRAG_STOP", this._dragEnd);

    }

    componentWillUnmount() {
        window.removeEventListener("FROM_DRAG_START", this._dragStart);
        window.removeEventListener("FORM_DRAG_STOP", this._dragEnd);
    }

    _onDrop = (e) => {
        e.preventDefault();
        let formId = e.dataTransfer.getData("uuid");
        window.dispatchEvent(new CustomEvent("FORM_DRAG_STOP"));
        let inGroup = this.props.data.forms.findIndex(x => x.uuid == formId);
        if (inGroup < 0) {
            this.props.addForm(this.props.data.uuid, formId);
        } else {
            ewars.growl("The form is already in this group");
            return;
        }
    };

    _dragStart = (e) => {
        e.preventDefault();
        if (this._el) this._el.style.display = "block";
    };

    _dragEnd = () => {
        if (this._el) this._el.style.display = "none";
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _removeForm = (uuid) => {
        let forms = ewars.copy(this.props.data.forms);
        let rIndex = forms.findIndex(x => x.uuid == uuid);
        forms.splice(rIndex, 1);
        this.props.onChange({
            ...this.props.data,
            forms: forms
        });
    };

    _action = (action) => {
        switch (action) {
            case "DELETE":
                this.props.onDelete(this.props.data.uuid);
                break;
            case "MOVE_UP":
                this.props.onMoveUp(this.props.data.uuid);
                break;
            case "MOVE_DOWN":
                this.props.onMoveDown(this.props.data.uuid);
                break;
            default:
                break;
        }
    };

    _moveFormUp = (uuid) => {
        let forms = this.props.data.forms;
        let curIndex = forms.findIndex(x => x.uuid == uuid);
        let curOrder = forms[curIndex].order;
        if (curOrder <= 0) return;

        let targetOrder = curOrder - 1;
        if (targetOrder < 0) return;

        let targetIndex = forms.findIndex(x => x.order == targetOrder);

        forms[curIndex].order -= 1;
        forms[targetIndex].order += 1;

        forms = forms.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        this.props.onChange({
            ...this.props.data,
            forms: forms
        });
    };

    _moveFormDown = (uuid) => {
        let forms = this.props.data.forms;

        let curIndex = forms.findIndex(x => x.uuid == uuid);
        let curOrder = forms[curIndex].order;
        if (curOrder >= forms.length) return;

        let targetOrder = curOrder + 1;
        let targetIndex = forms.findIndex(x => x.order == targetOrder);

        forms[curIndex].order += 1;
        forms[targetIndex].order -= 1;

        forms = forms.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        this.props.onChange({
            ...this.props.data,
            forms: forms
        });
    };

    _onNameChange = (e) => {
        console.log(e);
    };

    _emitChange = (e) => {
        console.log(e.target.innerHTML);
        this.props.onChange({
            ...this.props.data,
            name: {
                en: e.target.innerHTML.replace("<br>", "")
            }
        });
    };

    _nameInput = (e) => {
        console.log("HEY", e.target.innerHTML);
    };

    render() {
        let label = this.props.pinned ? "Pinned" : __(this.props.data.name);
        let icon = this.props.pinned ? "fal fa-thumbtack" : "fa-folder-open";

        let forms = this.props.data.forms.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        })

        return (
            <div className="adm-form-group">
                <div className="adm-form-group-header">
                    <div className="adm-form-group-name">
                        <i className={"fal " + icon}></i>&nbsp;
                        <div className="adm-editable-label">
                            {this.props.pinned ?
                                <div>{label}</div>
                                    :
                                <div
                                    onInput={this._nameInput}
                                    onBlur={this._emitChange}
                                    contentEditable
                                    dangerouslySetInnerHTML={{__html: label}}></div>
                            }
                        </div>
                    </div>

                    {!this.props.pinned ?
                    <div className="adm-form-group-controls">
                        <ewars.d.ActionGroup
                            right={true}
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    : null}
                </div>
                <div className="adm-form-group-children">
                    {forms.map(item => {
                        return (
                            <FormGroupItem
                                data={item}
                                removeForm={this._removeForm}
                                moveFormUp={this._moveFormUp}
                                moveFormDown={this._moveFormDown}
                                key={item.uuid}
                                forms={this.props.forms}/>
                        )
                    })}

                    <div
                        style={{display: "none"}}
                        ref={(el) => this._el = el}
                        onDrop={this._onDrop}
                        onDragOver={this._onDragOver}
                        className="adm-form-group-drop">Drop Forms Here</div>
                </div>
            </div>
        )
    }
}

class FormList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <ewars.d.Cell style={{position: "relative"}} width="30%" borderRight={true}>
                <div className="block-tree">
                    {this.props.forms.map(item => {
                        return <FormControl data={item}/>;
                    })}
                </div>
            </ewars.d.Cell>
        )
    }
}

class ViewGroups extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pinned: [],
            groups: [],
            forms: [],
            newGroup: null
        }
    }

    componentDidMount() {
        ewars.tx('com.ewars.conf', ["*"])
            .then(res => {
                let groups = JSON.parse(res.FORM_GROUPS || JSON.stringify(DEFAULTS));
                groups = groups.sort((a, b) => {
                    if (a.order > b.order) return 1;
                    if (a.order < b.order) return -1;
                    return 0;
                })

                this.setState({groups: groups});
            })
            .catch(err => {
                console.log(err)
            });

        ewars.tx("com.ewars.forms.accessible", [])
            .then(res => {
                this.setState({
                    forms: res
                })
            }).catch(err => {
                console.log(err)
            });
    }

    _addGroup = () => {
        let groups = ewars.copy(this.state.groups);
        groups.push({
            uuid: ewars.utils.uuid(),
            name: {en: "New Group"},
            forms: [],
            order: groups.length + 1
        });
        this.setState({
            groups: groups
        });
    };

    _addForm = (groupId, formId) => {
        let groups = ewars.copy(this.state.groups);
        let groupIndex;
        this.state.groups.forEach((item, index) => {
            if (item.uuid == groupId) groupIndex = index;
        });

        groups[groupIndex].forms.push({
            uuid: formId,
            order: groups[groupIndex].forms.length + 1
        });
        this.setState({
            groups: groups
        });
    };

    _onChange = (data) => {
        let groups = ewars.copy(this.state.groups);
        let itemIndex = groups.findIndex(x => x.uuid == data.uuid);
        groups[itemIndex] = data;
        this.setState({
            groups: groups
        });
    };

    _deleteGroup = (uuid) => {
        let groups = ewars.copy(this.state.groups);
        let gIndex = groups.findIndex(x => x.uuid == uuid);
        groups.splice(gIndex, 1);
        let counter = 0;
        groups.forEach(item => {
            if (!item.pinned) {
                item.order = counter;
                counter++;
            }
        })
        this.setState({
            groups: groups
        });
    };

    _groupUp = (uuid) => {
        let curIndex = this.state.groups.findIndex(x => x.uuid == uuid);
        let curOrder = this.state.groups[curIndex].order;
        if (curOrder == 0) return;

        let targetOrder = curOrder - 1;
        let groups = ewars.copy(this.state.groups);
        // Find the index of the peer that will have to move
        let targetIndex = groups.findIndex(x => x.order == targetOrder);

        groups[targetIndex].order += 1;
        groups[curIndex].order -= 1;

        groups = groups.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        this.setState({
            groups: groups
        });
    };

    _groupDown = (uuid) => {
        let curIndex = this.state.groups.findIndex(x => x.uuid == uuid);
        if (curIndex == this.state.groups.length - 1) return;

        let targetIndex = curIndex + 1;
        let groups = ewars.copy(this.state.groups);

        // Re set the order prop for the groups accordingly
        groups[targetIndex].order = curIndex;
        groups[curIndex].order = targetIndex;
        groups = groups.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        });

        this.setState({
            groups: groups
        });
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Saving change(s)");

        let data = {FORM_GROUPS: this.state.groups};
        ewars.tx("com.ewars.conf.update", ["FORM_GROUPS", data])
            .then(res => {
                bl.destroy();
                console.log(res);
            }).catch(err => {
                bl.destroy();
                console.log(err);
            });
    };

    render() {
        return (
            <ewars.d.Row>
                <FormList forms={this.state.forms}/>
                <ewars.d.Cell>
                    <ewars.d.Toolbar label={__("Form Groups")} icon="fal fa-folder-open">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-question-circle"
                                onClick={() => ewars.showHelp("FORM_GROUPS")}/>
                        </div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                label={__("SAVE_CHANGES")}
                                onClick={this._save}
                                colour="green"
                                icon="fa-save"/>
                            <ewars.d.Button
                                label={__("Add Group")}
                                icon="fa-plus"
                                onClick={this._addGroup}/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            {this.state.groups.map(item => {
                                return (
                                    <FormGroup
                                        key={item.uuid}
                                        data={item}
                                        onChange={this._onChange}
                                        onDelete={this._deleteGroup}
                                        onMoveDown={this._groupDown}
                                        onMoveUp={this._groupUp}
                                        addForm={this._addForm}
                                        pinned={item.pinned}
                                        forms={this.state.forms}/>
                                )
                            })}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

export default ViewGroups;
