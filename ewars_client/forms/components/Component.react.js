import FormsManagementComponent from "./FormsManagementComponent.react";
import FormEditorComponent from "./FormEditorComponent.react";

import {SET_FORM, UNSET_FORM, SET_VIEW} from "../actions";

import CONSTANTS from "../../common/constants";

var Component = React.createClass({
    getInitialState: function () {
        return {
            view: "FORMS",
            form: null,
            errors: {}
        }
    },

    editForm: function (data) {
        this.setState({
            view: "EDIT",
            form: data
        });
    },

    _cancelEdit: function () {
        this.setState({view: "FORMS", form: null})
    },

    _newForm: function () {

        let accId;
        if (window.user.role != "SUPER_ADMIN") accId = window.user.aid || window.user.account_id;

        this.setState({
            view: "EDIT",
            form: {
                name: {en: ""},
                created_by: window.user.id,
                version_id: "NEW",
                account_id: accId,
                status: "DRAFT",
                description: {
                    en: null
                },
                features: {},
                version: {
                    uuid: null,
                    definition: {},
                    etl: {}
                }
            }
        });
    },

    render: function () {
        var view;

        if (this.state.view == "FORMS") {
            view = (
                <FormsManagementComponent
                    onEdit={this.editForm}
                    onNew={this._newForm}
                    onCancel={this._cancelEdit}/>
            )
        }

        if (this.state.view == "EDIT") {
            view = (
                <FormEditorComponent
                    data={this.state.form}
                    errors={this.state.errors}
                    onCancel={this._cancelEdit}/>
            )
        }


        return view;

    }
});

export default Component;
