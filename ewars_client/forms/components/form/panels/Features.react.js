import {
    LinkedList,
    SwitchField
} from "../../../../common/fields";
import Form from "../../../../common/c.form";

const FEATURES = [
    {
        id: 'INTERVAL_REPORTING',
        label: "Interval-Based Reporting",
        description: "Form is configure to be submitted on a routine basis",
        options: {
            interval: {
                type: "select",
                label: "Reporting Interval",
                options: [
                    ["DAY", "Daily"],
                    ["WEEK", "Weekly"],
                    ["MONTH", "Monthly"],
                    ["YEAR", "Annual"]
                ]
            },
            block_future_dates: {
                type: "switch",
                label: "Block dates in the future"
            }
        }
    },
    {
        id: "LOCATION_REPORTING",
        label: "Location-Based Reporting",
        description: "Forms are submitted with a correlated location of a specific type",
        options: {
            site_type_id: {
                type: "select",
                label: "Location Type",
                optionsSource: {
                    resource: "location_type",
                    valSource: "id",
                    labelSource: "name",
                    query: {}
                }
            }
        }
    },
    {
        id: 'OVERDUE',
        label: "Overdue Thresholds",
        description: "Form can be configured to be overdue",
        options: {
            interval: {
                type: "select",
                label: "Overdue Interval Type",
                options: [
                    ["DAY", "Day(s)"],
                    ["WEEK", "Week(s)"],
                    ["MONTH", "Month(s)"],
                    ["YEAR", "Year(s)"]
                ]
            },
            threshold: {
                type: "number",
                label: "Overdue Threshold"
            }

        }
    },
    //{
    //    id: 'WORKFLOW',
    //    label: "Workflow",
    //    description: "Create a custom linear workflow for the form",
    //    options: {
    //        workflow: {
    //            type: "select",
    //            label: "Workflow",
    //            optionsSource: {
    //                resource: "account",
    //                labelSource: "name",
    //                valSource: "id",
    //                query: {},
    //                additionalOptions: [
    //                    ["NEW", "Create New Workflow"]
    //                ]
    //            }
    //        },
    //        name: {
    //            type: "text",
    //            label: "Workflow Name",
    //            conditions: {
    //                application: "all",
    //                rules: [
    //                    ["workflow", "eq", "NEW"]
    //                ]
    //            }
    //        },
    //        flow: {
    //            type: "display",
    //            label: "Flow Definition",
    //            conditions: {
    //                application: "all",
    //                rules: [
    //                    ["workflow", "eq", "NEW"]
    //                ]
    //            }
    //        }
    //    }
    //},
    {
        id: 'AMENDMENTS',
        label: "Amendments",
        description: "Force users to have to submit amendments for approval",
        options: {
            approval: {
                type: "switch",
                label: "Require approval"
            }
        }
    },
    {
        id: 'RETRACTIONS',
        label: "Retractions",
        description: "Force users to have to submit deletions for approval",
        options: {
            require_approval: {
                type: "switch",
                label: "Require Approval?"
            }
        }
    },
    //{
    //    id: 'TRACKING',
    //    label: "Track and Trace",
    //    description: "Associate forms with an entity that is being tracked geographically"
    //},
    // {
    //     id: 'BULK_EDIT',
    //     label: "Line Edit",
    //     description: "Individual submissions make up a table of submission which can be edited all at once from a central editor",
    //     options: {
    //         allowed: {
    //             type: "select",
    //             multiple: true,
    //             label: {en: "Restrict Access"},
    //             options: [
    //                 ["ACCOUNT_ADMIN", "Account Administrator"],
    //                 ["REGIONAL_ADMIN", "Regional Administrator"],
    //                 ["USER", "Reporting User"]
    //             ]
    //         }
    //     }
    // },
    //{
    //    id: "EMBEDDABLE",
    //    label: "External Embed",
    //    description: "Allows externally embedding the form in another web site"
    //},
    // {
    //     id: "ATTACHMENTS",
    //     label: "Attachments",
    //     description: "Allow users to attach files to the reports",
    //     options: {
    //         allowed_file_types: {
    //             type: "select",
    //             multiple: true,
    //             label: "Allowed File Types",
    //             options: [
    //                 ["PDF", "PDF"],
    //                 ["WORD", "Word"],
    //                 ["EXCEL", "Excel"],
    //                 ["CSV", "CSV"],
    //                 ["RTF", "Rich-Text Format"],
    //                 ["JPG", "Jpg/Jpeg"],
    //                 ["PNG", "Png"],
    //                 ["GIF", "Gif"]
    //             ]
    //         }
    //     }
    // },
    //{
    //    id: "SCHEDULED",
    //    label: "Scheduled Form",
    //    description: "Schedule a period when this form is available",
    //    options: {
    //        start_date: {
    //            type: "date",
    //            label: "Period Start Date"
    //        },
    //        end_date: {
    //            type: "date",
    //            label: "Period End Date"
    //        }
    //    }
    //},
    {
        id: "ALERT",
        label: "Attachable to Alerts",
        description: "Allow this form to be configured to be used in alert investigations"
    },
    //{
    //    id: "EXPIRE_SUBMISSIONS",
    //    label: "Expiring Forms",
    //    description: "Remove form submissions from the system a specified period after submission",
    //    options: {
    //        expire_interval: {
    //            type: "select",
    //            label: "Expiry Interval",
    //            options: [
    //                ["DAY", "Day(s)"],
    //                ["WEEK", "Week(s)"],
    //                ["MONTH", "Month(s)"],
    //                ["YEAR", "Year(s)"]
    //            ]
    //        },
    //        expiry_threshold: {
    //            type: "number",
    //            label: "Expiry Threshold"
    //        }
    //    }
    //},
    //{
    //    id: "PASSWORD",
    //    label: "Password-Protected",
    //    description: "Restrict users from accessing this form without the specific passowrd",
    //    options: {
    //        password: {
    //            type: "text",
    //            label: "Password"
    //        }
    //    }
    //},
    // {
    //     id: "API",
    //     label: "HTTP API Submission",
    //     description: "Enable an HTTP API to send form submissions to from external systems",
    //     options: {
    //         endpoint: {
    //             type: "display",
    //             label: "API Endpoint",
    //             value: `http://${ewars.domain}/api/submit/<form_id>`
    //         },
    //         auth_token: {
    //             type: "display",
    //             label: "Authentication Token",
    //             value: "<token>"
    //         }
    //     }
    // }
];

// Pre-defined option sets
// More complex conditional logic
// Better control over field valuidate
// Repeating Sections
// Calculatied fields

class Feature extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showOptions: false
        }
    }

    _update = (prop, value) => {
        let hasOptions = this.props.data.options ? true : false;
        if (prop == "enabled") {
            if (value == true) {
                window.dispatchEvent(new CustomEvent("toggle-feature", {
                    detail: {
                        id: this.props.id,
                        hasOptions: hasOptions
                    }
                }));
            } else {
                window.dispatchEvent(new CustomEvent("toggle-feature", {
                    detail: {
                        id: this.props.id,
                        hasOptions: hasOptions
                    }
                }));
            }
        }
    };

    _toggleOptions = (e) => {
        this.setState({
            showOptions: !this.state.showOptions
        })
    };

    _featureSettingChange = (data, prop, value) => {
        window.dispatchEvent(new CustomEvent("feature-change", {
            detail: {
                id: this.props.id,
                prop: prop,
                value: value
            }
        }));
    };

    render() {
        let features = this.props.features || {};
        let iconClass = "fal";
        if (this.state.showOptions) iconClass += " fa-minus-square";
        if (!this.state.showOptions) iconClass += " fa-plus-square";

        let enabled = false;
        if (features[this.props.id]) enabled = true;

        let className = "feature";
        if (enabled) className += " left-green";

        let settings = {};
        if (enabled) settings = features[this.props.id] || {};

        return (
            <div className={className}>
                <div className="feature-header">
                    <div className="name">{this.props.data.label}</div>
                    {enabled && this.props.data.options ?
                        <div className="handle" onClick={this._toggleOptions}>
                            <i className={iconClass}></i>
                        </div>
                        : null}
                    <div className="ide-col" style={{maxWidth: "35px", paddingTop: 3}}>
                        <SwitchField
                            name="enabled"
                            value={enabled}
                            onUpdate={this._update}/>

                    </div>
                </div>
                {!this.state.showOptions ?
                    <div className="description">
                        <p>{this.props.data.description}</p>
                    </div>
                    : null}
                {this.state.showOptions && this.props.data.options ?
                    <div className="options">
                        <Form
                            data={settings}
                            readOnly={false}
                            updateAction={this._featureSettingChange}
                            definition={this.props.data.options}/>
                    </div>
                    : null}
            </div>
        )
    }
}

class Features extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let features = FEATURES.map(function (item, key) {
            return <Feature
                onChange={this.props.onFeatureChange}
                onToggleFeature={this.props.onFeatureToggle}
                data={item}
                key={key}
                features={this.props.data.features || {}}
                id={item.id}/>
        }.bind(this));

        return (
            <div className="ide-section-basic">
                <div className="header">Features</div>
                <div className="vsplit-box">
                    <div className="ide-setting-control">
                        <div className="feature-list">
                            {features}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Features;
