import CONSTANTS from "../../../../common/constants";
import Features from "./Features.react";

import {
    SettingsForm,
    SettingsSection,
    SettingsField
} from "../../../../common";

import {
    TextField,
    SelectField,
    TextAreaField,
    SwitchField,
    LanguageStringField as I18NString,
    NumericField as NumberField
} from "../../../../common/fields";

import FormForm from "../../../constants/FormForm";

const VERSIONS = [
    [1, "Version 1 (Legacy)"],
    [2, "Version 2 (Current)"]
]


class GeneralPanel extends React.Component {
    constructor(props) {
        super(props);
    }

    _onFormUpdate = (prop, value) => {
        window.dispatchEvent(new CustomEvent("form-setting-change", {
            detail: {
                prop: prop,
                value: value
            }
        }));
    };

    render() {
        var isAdmin = false;
        if (window.user.role == "SUPER_ADMIN") isAdmin = true;

        return (
            <SettingsForm title="Form Settings">

                <SettingsSection title="General Settings">
                    <SettingsField label="Form Title">
                        <I18NString
                            name="name"
                            onUpdate={this._onFormUpdate}
                            value={this.props.data.name}/>
                        {this.props.errors.name ?
                            <p className="error">{this.props.errors.name}</p>
                            : null}
                    </SettingsField>

                    <SettingsField label="Status">
                        <SelectField
                            value={this.props.data.status}
                            name="status"
                            onUpdate={this._onFormUpdate}
                            config={FormForm.status}/>
                        {this.props.errors.status ?
                            <p className="error">{this.props.errors.status}</p>
                            : null}
                        </SettingsField>

                        <SettingsField label="Description">
                            <TextAreaField
                                name="description"
                                value={this.props.data.description}
                                onUpdate={this._onFormUpdate}
                                config={FormForm.description}/>
                            {this.props.errors.description ?
                                <p className="error">{this.props.errors.description}</p>
                                : null}
                            </SettingsField>

                        <SettingsField label="Submission EID Prefix">
                            <TextField
                                name="eid_prefix"
                                value={this.props.data.eid_prefix}
                                onUpdate={this._onFormUpdate}/>
                        </SettingsField>

                </SettingsSection>

                <SettingsSection title="Form Restrictions">

                    <SettingsField label="Allow Export?">
                        <SwitchField
                            readOnly={false}
                            value={this.props.data.exportable}
                            name="exportable"
                            onUpdate={this._onFormUpdate}/>
                    </SettingsField>

                </SettingsSection>

                <Features
                    data={this.props.data}/>

            </SettingsForm>
        )
    }
}


export default GeneralPanel;
