import { TextAreaField as TextArea } from "../../../common/fields";


var GuidanceEditor = React.createClass({
    _update: function (prop, value) {
        ewars.store.dispatch({type: "SET_FORM_PROP", prop: "guidance", value: value});
    },

    render: function () {
        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Guidance</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Describe and inform users about how to use and understand the form.</p>
                </div>
                <div className="ide-settings-form">
                    <div style={{margin: 8}}>
                        <TextArea
                            name="guidance"
                            onUpdate={this._update}
                            config={{i18n: true}}
                            value={this.props.data.guidance}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default GuidanceEditor;
