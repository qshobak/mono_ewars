import { FileField } from "../../common/fields";

export default class FormImporter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {name: ""}
    }

    _onChange = (prop, data, additional) => {
    };

    _onFileData = (data) => {
        let definition = JSON.parse(window.decodeURI(data));
        this.props.onSuccess(definition);
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div style={{padding: 20}}>
                            <h3>Upload an .ewars_form file to import</h3>
                            <FileField
                                name="file"
                                value={this.state.name}
                                return_data={true}
                                binary={true}
                                onData={this._onFileData}
                                onUpdate={this._onChange}/>
                        </div>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}
