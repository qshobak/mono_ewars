import moment from "moment";

import CONSTANTS from "../../common/constants";
import { SelectField } from "../../common/fields";
import DeleteModal from "../modals/DeleteModal";
import FormImporter from "./FormImporter";
import ViewGroups from "./view.groups";
import DataView from "./data";

import {
    Layout, Cell, Row,
    Modal,
    Spinner,
    Button,
    DataTable
} from "../../common";


var STATUSES = {
    ACTIVE: "Active",
    DRAFT: "Inactive"
};

var FORM_TYPES = {
    PRIMARY: "Primary Collection",
    INVESTIGATIVE: "Investigative",
    RESPONSE: "Response",
    SURVEY: "Survey",
    QUIZ: "Quiz",
    LAB: "Laboratory Reporting",
    TRACE: "Track & Trace"
};

const STATUS_OPTIONS = [
    ["ACTIVE", "Active"],
    ["DRAFT", "Draft"]
];

const ACCOUNT_SOURCE = {
    resource: "account",
    labelSource: "name",
    valSource: "id",
    query: {}
};

const COLUMNS = [
    {name: "name", fmt: ewars.I18N, config: {label: "Name", type: "text", filterKey: "name.en"}, width: 350},
    {name: "status", config: {label: "Status", type: "select", options: STATUS_OPTIONS}},
    {name: "last_modified", config: {label: "Last Modified", type: "date"}, fmt: ewars.DATE},
    {name: "created", config: {label: "Created", type: "date"}, fmt: ewars.DATE}
];

const ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"},
    {label: "Duplicate", icon: "fa-copy", action: "DUPLICATE"},
    {label: "Export", icon: "fa-download", action: "EXPORT"}
];

const JOIN = [];

const MODAL_ACTIONS = [
    {label: "I understand the consequences, delete this form", colour: "red", action: "DELETE"},
    {label: "Cancel", action: "CANCEL"}
];

class FormsManagementComponent extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            filters: {
                status: {neq: "DELETED"}
            },
            ordering: {
                last_modified: "DESC"
            },
            showFormDelete: false,
            form: null,
            formNameDeleteCheck: "",
            showImport: false,
            view: "FORMS"
        }
    }

    _onAction = (action, dell, data) => {
        if (action == "EDIT") this.props.onEdit(data);
        if (action == "DUPLICATE") {
            this._duplicateForm(data);
        }
        if (action == "DELETE") this._deleteForm(data);
        if (action === "EXPORT") this._exportForm(data);
    };

    _exportForm = (data) => {
        let bl = new ewars.Blocker(null, "Retrieving data...");
        let joins = [
            "version:version_id:uuid"
        ];
        ewars.tx("com.ewars.resource", ["form", data.id, null, joins])
            .then((resp) => {
                bl.setMessage("Building package...");
                let c_data = {
                    name: resp.name,
                    description: resp.description,
                    features: resp.features,
                    guidance: resp.guidance,
                    status: "DRAFT",
                    definition: resp.version.definition
                };

                let j_data = JSON.stringify(c_data);
                let b_data = window.btoa(window.encodeURI(j_data));

                let element = document.createElement("a");
                element.setAttribute("href", "data:application/octet-stream;base64," + b_data);
                element.setAttribute("download", "form_export.ewars_form");

                element.style.display = "none";
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
                bl.destroy();
            })
    };

    _duplicateForm = (form) => {
        ewars.prompt("fa-copy", "Duplicate Report?", "Are you sure you want to duplicate the form " + ewars.I18N(form.name) + "?", function () {
            var blocker = new ewars.Blocker(null, "Duplicating form...");
            ewars.tx("com.ewars.form.duplicate", [form.id])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.emit("RELOAD_DT")
                }.bind(this))
        }.bind(this))
    };


    _deleteForm = (form) => {
        this.setState({
            showFormDelete: true,
            form: form
        })
    };

    _onModalAction = (action, data) => {
        if (action == "CLOSE") {
            this.setState({
                showImport: false
            })
        }

        if (action == "CANCEL") {
            this.setState({
                showFormDelete: false,
                form: null,
                formNameDeleteCheck: ""
            });
            return;
        }

        if (action == "DELETE") {
            let formName = ewars.I18N(this.state.form.name);
            if (formName == this.state.formNameDeleteCheck) {
                let blocker = new ewars.Blocker(null, "Deleting form...");
                ewars.tx("com.ewars.form.delete", [this.state.form.id])
                    .then((resp) => {
                        blocker.destroy();
                        ewars.growl("Form deleted");
                        this.setState({
                            showFormDelete: false,
                            form: null,
                            formNameDeleteCheck: ""
                        });
                        ewars.emit("RELOAD_DT");
                    })

            } else {
                ewars.growl("Please check name of report and try again.")
            }
        }
    };

    _uploadForm = () => {
        this.setState({showImport: true})
    };

    _onFormNameCheckChange = (prop, value) => {
        this.setState({
            formNameDeleteCheck: value
        })
    };

    _importForm = (data) => {
        data.version = {
            definition: data.definition,
            etl: {}
        };
        this.props.onEdit(data);
    };

    _setView = (view) => {
        this.setState({
            view
        })
    };

    render() {
        let filter = {status: {neq: "DELETED"}};

        let view;

        if (this.state.view == "FORMS") {
            view = (
                <ewars.d.Cell>
                    <ewars.d.Toolbar label="Forms">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                label="Import Form"
                                icon="fa-upload"
                                onClick={this._uploadForm}/>
                            <ewars.d.Button
                                label="New Form"
                                icon="fa-plus"
                                onClick={this.props.onNew}/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <DataTable
                                id="DT_TRANSIENT"
                                onCellAction={this._onAction}
                                actions={ACTIONS}
                                paused={this.state.showFormDelete}
                                filter={filter}
                                join={JOIN}
                                resource="form"
                                columns={COLUMNS}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    {this.state.showFormDelete ?
                        <ewars.d.Modal
                            visible={this.state.showFormDelete}
                            buttons={MODAL_ACTIONS}
                            onAction={this._onModalAction}
                            icon="fa-trash"
                            title="Delete form?">
                            <DeleteModal
                                value={this.state.formNameDeleteCheck}
                                onChange={this._onFormNameCheckChange}
                                data={this.state.form}/>
                        </ewars.d.Modal>
                        : null}
                    <ewars.d.Shade
                        onAction={this._onModalAction}
                        shown={this.state.showImport}
                        title="Import Form">
                        <FormImporter onSuccess={this._importForm}/>
                    </ewars.d.Shade>
                </ewars.d.Cell>
            )
        }

        if (this.state.view == "GROUPS") {
            view = <ViewGroups/>;
        }

        return (
            <Layout>
                <Row>
                    <Cell width="34px" style={{position: "relative", overflow: "hidden"}} borderRight={true}>
                        <div className="ide-tabs">
                            <div className={"ide-tab" + (this.state.view == "FORMS" ? " ide-tab-down" : "")}
                                 onClick={() => this._setView("FORMS")}>
                                <i className="fal fa-clipboard"></i>&nbsp;Forms
                            </div>
                            <div className={"ide-tab" + (this.state.view == "GROUPS" ? " ide-tab-down" : "")}
                                onClick={() => this._setView("GROUPS")}>
                                <i className="fal fa-folder-open"></i>&nbsp;Form Groups
                            </div>
                        </div>
                    </Cell>
                    {view}
                </Row>
            </Layout>
        )
    }
}


export default FormsManagementComponent;
