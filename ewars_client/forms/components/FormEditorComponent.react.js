import {SET_EDIT_VIEW, SET_FORM_PROP, SET_FORM, UNSET_FORM, SET_LOADED} from "../actions";

import CONSTANTS from "../../common/constants";

import {
    TextAreaField,
    SelectField
} from "../../common/fields";

import {
    Filler,
    Spinner
} from "../../common";
import SubNavComponent from "../../common/c.sub_nav";
import Validator from "../../common/utils/Validator";

import FormDefinitionComponent from "../../common/editors/form_definition/FormDefinitionComponent.react";
import LogicEditor from "../../common/editors/logic/LogicEditor.react";
import GeneralSettingsViewComponent from "./form/GeneralSettingsViewComponent.react";
import GuidanceEditor from "./form/GuidanceEditor.react";
import Integrations from "./form/Integrations.react";

import FormForm from "../constants/FormForm";

import ViewWorkflow from '../views/v.workflow';
import ViewOutput from '../views/v.output';
import ViewStages from "../views/view.stages";
import ViewHelp from "../views/view.help";

const _statusOptions = [
    ["ENABLED", "Enabled"],
    ["DISABLED", "Disabled"]
];

const btnControlStyle = {
    marginTop: "5px"
};

var TabButton = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-htab-btn";
        if (this.props.current == this.props.view) className += " ide-htab-btn-active";

        var iconClass = "fal " + this.props.icon;

        return (
            <div xmlns="http://www.w3.org/1999/xhtml" onClick={this._onClick}
                 className={className}><i className={iconClass}></i>&nbsp;{this.props.label}
            </div>
        )
    }
});

class FormEditorComponent extends React.Component {
    static defaultProps = {
        id: null
    }

    constructor(props) {
        super(props);

        this.state = {
            view: "SETTINGS",
            form: null
        }
    }

    componentDidMount() {
        window.addEventListener("toggle-feature", this._toggleFeature);
        window.addEventListener("feature-change", this._featureChange);
        window.addEventListener("form-setting-change", this._formSettingChange);
        if (this.props.data.id) {
            ewars.tx('com.ewars.resource', ["form", this.props.data.id, null, ["version:version_id:uuid"]])
                .then(res => {
                    Object.keys(res.version.etl || {}).forEach(key => {
                        if (!res.version.etl[key].key) res.version.etl[key].key = ewars.utils.uuid();
                    });
                    this.setState({form: res});
                })
                .catch(err => {
                    console.log(err)
                });
        } else {
            this.setState({
                form: {
                    id: null,
                    name: {en: "New Form"},
                    version: {
                        definition: this.props.data.version.definition || {},
                        etl: {}
                    },
                    status: "INACTIVE"
                }
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener("toggle-feature", this._toggleFeature);
        window.removeEventListener("feature-change", this._featureChange);
        window.removeEventListener('form-setting-change', this._formSettingChange);
    }

    _formSettingChange = (e) => {
        let prop = e.detail.prop,
            value = e.detail.value;

        this.setState({
            form: {
                ...this.state.form,
                [prop]: value
            }
        });
    };

    _featureChange = (e) => {
        let id = e.detail.id,
            prop = e.detail.prop,
            value = e.detail.value;

        let features = this.state.form.features || {};

        if (!features[id]) features[id] = {};
        features[id] = {
            ...features[id],
            [prop]: value
        }

        this.setState({
            form: {
                ...this.state.form,
                features: features
            }
        });
    };

    _toggleFeature = (e) => {
        let id = e.detail.id,
            hasOptions = e.detail.hasOptions;

        let features = this.state.form.features || {};
        if (features[id]) {
            features[id] = null;
            delete features[id];
        } else {
            if (!hasOptions) {
                features[id] = true;
            } else {
                features[id] = {};
            }
        }

        this.setState({
            form: {
                ...this.state.form,
                features: features
            }
        });
    };

    validate = () => {
        var errors = {};

        if (Object.keys(this.state.form.name.en).length <= 0) {
            errors.name = "Please provide a valid name for the form";
        }

        if (Object.keys(this.state.form.description.en).length <= 0) {
            errors.description = "Please provide a valid description for this form";
        }

        if (this.props.data.status == CONSTANTS.ACTIVE) {
            //errors = this.validateDefinition(this.props.data, errors);
            //errors = this.validateLogic(this.props.data, errors);
        }

        return Object.keys(errors).length > 0 ? errors : null;
    };

    validateDefinition = (form, errors) => {
        if (!form.version.definition) errors.definition = "NO_DEFINITION";

        return errors;
    };

    validateLogic = (form, errors) => {
        return errors;
    };

    _saveForm = () => {
        let blocker = new ewars.Blocker(null, "Validating...");
        console.log("HERE", this.state.form);
        //let errors = this.validate();
        let errors = null;

        if (errors) {
            blocker.destroy();

            this.setState({
                errors: errors
            });
            if (errors.definition) {
                if (errors.definition == "NO_DEFINITION") ewars.error("Form Definition Error", "There is a problem with your form definition, please review and fix any issues before saving.");
                if (errors.definition == "MISSING_CONDITIONS") ewars.error("Form Definition Error", "One of your form fields has an unset conditions field");
            } else if (errors.logic) {

            } else {
                ewars.error("Error", "There are errors in the form, please review settings and try again");
            }
            return null;
        }

        blocker.setMessage("Saving form...");
        if (this.state.form.id) {
            ewars.tx("com.ewars.form.update", [this.state.form.id, this.state.form])
                .then(function (resp) {
                    blocker.destroy();
                    this.setState({
                        form: resp
                    })
                    ewars.growl("Form updated");
                }.bind(this))
        } else {
            ewars.tx("com.ewars.form.create", [this.state.form])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.store.dispatch({type: "SET_FORM", data: resp});
                    this.setState({
                        form: resp
                    });
                    ewars.growl("Form created");
                }.bind(this))
        }
    };

    _onTabChange = (view) => {
        this.setState({
            view: view
        })
    };

    _cancel = () => {
        this._requiresVersionHUP = false;
        this._isDirty = false;
        this.props.onCancel();
    };

    _onFormDefinitionChange = (data) => {
        this.setState({
            form: {
                ...this.state.form,
                version: {
                    ...this.state.form.version,
                    definition: data
                }
            }
        });
    };

    _onChange = (p, prop, value) => {
        console.log(p, prop, value);

        this.setState({
            form: {
            ...this.state.form,
                [prop]: value
            }
        });
    };

    _onFeatureToggle = (id) => {

    };

    _onLogicChange = (data) => {
        this.setState({
            form: {
                ...this.state.form,
                version: {
                    ...this.state.form.version,
                    etl:data
                }
            }
        });
    };

    render() {
        if (!this.state.form) return <Filler/>;

        var canEditGeneral = true;
        var canEditFields = true;

        if (this.props.data.version_id == "NEW") {
            canEditGeneral = true;
            canEditFields = true;
        }

        var view;

        if (this.state.view == "FIELDS") {
            view = <FormDefinitionComponent
                definition={this.state.form.version.definition}
                form={this.state.form}
                onChange={this._onFormDefinitionChange}/>
        }

        if (this.state.view == "STAGES") {
            view = <ViewStages data={this.props.data} onChange={this._onStagesChange}/>;
        }

        if (this.state.view == "LOGIC") {
            view = <LogicEditor
                onChange={this._onLogicChange}
                form={this.state.form}/>
        }
        if (this.state.view == "SETTINGS") {
            view = <GeneralSettingsViewComponent
                editable={canEditGeneral}
                data={this.state.form}
                onChange={this._onChange}
                onSave={this._saveForm}
                onCancel={this._cancel}
                errors={this.props.errors}/>;
        }

        if (this.state.view == "WORKFLOW") {
            view = (
                <ViewWorkflow
                    data={this.props.data}/>
            )
        }

        if (this.state.view == "OUTPUT") {
            view = (
                <ViewOutput
                    data={this.props.data}/>
            )
        }

        if (this.state.view == "HELP") view = <ViewHelp/>;

        let saveText = "Save";
        if (this.props.data.id) saveText = "Save Change(s)";

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar style={{padding: "0"}}>
                    <div className="ux-tabs" style={{width: "50%", float: "left"}}>

                        <div
                            onClick={() => {
                                this.setState({view: "SETTINGS"});
                            }}
                            className={"ux-tab" + (this.state.view == "SETTINGS" ? " active" : "")}>
                            <i className="fal fa-cog"></i>&nbsp;Settings
                        </div>

                        <div
                            onClick={() => {
                                this.setState({view: "FIELDS"});
                            }}
                            className={"ux-tab" + (this.state.view == "FIELDS" ? " active" : "")}>
                            <i className="fal fa-list"></i>&nbsp;Fields
                        </div>

                        {false ?
                        <div className={"ux-tab" + (this.state.view == "STAGES" ? " active" : "")}
                             onClick={() => this.setState({view: "STAGES"})}>
                             <i className="fal fa-microchip"></i>&nbsp;Stages
                         </div>
                        : null}

                         <div
                            className={"ux-tab" + (this.state.view == "LOGIC" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "LOGIC"})
                            }}>
                            <i className="fal fa-code-branch"></i>&nbsp;Logic
                        </div>

                        {false ?
                        <div className={"ux-tab" + (this.state.view == "CALENDAR" ? " active" : "")}
                            onClick={() => {this.setstate({view: "CALENDAR"})}}>
                                <i className="fal fa-calendar"></i>&nbsp;Calendar
                            </div>
                        : null}

                        {false ?
                        <div className={"ux-tab" + (this.state.view == "ASSIGNMENTS" ? " active" : "")}
                            onClick={() => {this.setstate({view: "ASSIGNMENTS"})}}>
                                <i className="fal fa-clipboard"></i>&nbsp;Assignments
                            </div>
                        : null}

                        {false ?
                        <div className={"ux-tab" + (this.state.view == "HELP" ? " active" : "")}
                            onClick={() => {this.setState({view: "HELP"})}}>
                                <i className="fal fa-question-circle"></i>
                            </div>
                        : null}

                    </div>
                    <div className="btn-group pull-right" style={btnControlStyle}>
                        <ewars.d.Button
                            label={saveText}
                            icon="fa-save"
                            colour="green"
                            onClick={this._saveForm}/>
                        <ewars.d.Button
                            label="Cancel"
                            icon="fa-times"
                            colour="red"
                            onClick={this._cancel}/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}


export default FormEditorComponent;
