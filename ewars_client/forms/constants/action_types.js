const ACTION_TYPES = [
    {
        action: "GENERATE_RECORD",
        label: "Generate Record",
        description: "Generate a record using information from this form"
    },
    {
        action: "MODIFY_FIELDS",
        label: "Modify Form",
        description: "Modify the fields in the form, for instance hiding, moving or updating the options in a form field",
    },
    {
        action: "APPROVAL",
        label: "Approval",
        description: "Require approval before this record can be moved to the next stage."
    },
    {
        action: "NOTIFICATION",
        label: "Send Notification",
        description: "Send a notification to user(s) that the form has advanced to the next stage."
    },
    {
        action: "GENERATE_ALERT",
        label: "Generate Alert",
        description: "Generate a custom alert based off of this record"
    }
];

export default ACTION_TYPES;
