import FormMapper from '../controls/c.form_mapper';
import FormModifier from '../controls/c.form_modifier';

const STYLES = {
    LIST_ITEM: {
        display: "block",
        padding: "8px",
        margin: "0 8px 8px 8px",
        border: "1px solid rgba(255,255,255,0.5)"
    },
    FORM_STAGE: {
        display: "block",
        border: "1px solid rgba(255,255,255,0.5)",
        margin: "0 8px 8px 8px",
        minHeight: "100px",
        background: "rgba(255,255,255,0.9)"
    },
    STAGE_HEADER: {
        display: "block",
        height: "30px",
        borderBottom: "1px solid #333333"
    },
    INITIAL: {
        display: "block",
        margin: "0 8px 8px 8px"
    },
    INITIAL_CIRCLE: {
        display: "block",
        textAlign: "center",
        margin: "0 auto",
        textTransform: "uppercase",
        fontWeight: "bold",
        padding: "8px",
        border: "6px solid white"
    },
    STAGE_ACTION: {
        display: "block",
        margin: "0 8px 8px 8px",
        background: "#333333",
        padding: "8px",
        borderRadius: "20px",
        color: "white"
    },
    STAGE_BODY: {
        display: "block",
        paddingTop: "8px"
    },
    SHADE_INNER: {
        display: "block",
        padding: "8px",
        overflowY: "auto"
    }
};

const ACTION_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit action'],
    ['fa-copy', 'DUPE', 'Copy action'],
    ['fa-trash', 'DELETE', 'Remove action']
];


const MODAL_ACTIONS = [
    {label: "Save change(s)", icon: "fa-save", action: "SAVE"},
    {label: "Close", icon: "fa-times", action: "CLOSE"}
];


const ACTION_TYPES = [
    ["GENERATE_RECORD", "Generate new record"],
    ["NOTIFICATION", "Send notification(s)"],
    ["MODIFY_FORM", "Modify form"],
    ["GENERATE_ALERT", "Generate alert"],
    ["LOCK_TO_ROLE", "Lock to role"]
];

class StageAction extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            data: {}
        }
    }

    _action = (action) => {
        switch (action) {
            case 'EDIT':
                this.setState({
                    showModal: true
                });
                break;
            case "CLOSE":
                this.setState({
                    showModal: false
                });
                break;
            default:
                break;
        }
    };

    _close = () => {
        this.setState({
            showModal: false
        })
    };

    render() {
        return (
            <div style={STYLES.STAGE_ACTION}>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        [action_name]
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{display: "block"}}>
                        <ewars.d.ActionGroup
                            actions={ACTION_ACTIONS}
                            onAction={this._action}
                            right={true}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.showModal ?
                    <ewars.d.Shade
                        shown={true}
                        title="Action Editor"
                        onAction={this._action}
                        actions={MODAL_ACTIONS}
                        onClose={this._close}
                        icon="fa-cog">

                        <ewars.d.Layout>
                            <ewars.d.Toolbar>

                                <div className="select-custom">
                                    <select
                                        onChange={(e) => {
                                            this.setState({
                                                data: {
                                                    ...this.state.data,
                                                    type: e.target.value
                                                }
                                            })
                                        }}
                                        name="" id="">
                                        <option value="">No action type selected</option>
                                        {ACTION_TYPES.map(item => {
                                            return <option value={item[0]}>{item[1]}</option>
                                        })}
                                    </select>
                                </div>
                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                {this.state.data.type == "GENERATE_RECORD" ?
                                    <FormMapper/>
                                    : null}

                                {this.state.data.type == "MODIFY_FORM" ?
                                    <FormModifier/>
                                : null}

                            </ewars.d.Row>

                        </ewars.d.Layout>
                    </ewars.d.Shade>
                    : null}
            </div>
        )
    }
}

class ListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={STYLES.LIST_ITEM} draggable={true}>
                {this.props.label}
            </div>
        )
    }
}

class InitialStage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div style={STYLES.INITIAL}>
                <div style={STYLES.INITIAL_CIRCLE}>
                    {this.props.label}
                </div>
            </div>
        )
    }
}

const STAGE_ACTIONS = [
    ['fa-plus', 'ADD_ACTION', 'Add action'],
    ['fa-trash', 'DELETE_STAGE', 'Delete stage']
]

class FormStage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div style={STYLES.FORM_STAGE} className="form-stage" draggable={true}>
                <div style={STYLES.STAGE_HEADER}>
                    <ewars.d.ActionGroup
                        height="30px"
                        actions={STAGE_ACTIONS}
                        onAction={this._action}
                        right={true}/>
                </div>
                <div style={STYLES.STAGE_BODY}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

class ViewWorkflow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "SUBMIT"
        }
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row style={{display: "block"}} height="35px" borderBottom={true} borderTop={true}>
                    <div className="ux-tabs">
                        <div
                            onClick={() => {
                                this.setState({
                                    view: "SUBMIT"
                                })
                            }}
                            className={"ux-tab" + (this.state.view == "SUBMIT" ? " active" : "")}>
                            <i className="fal fa-share"></i>&nbsp;On submit
                        </div>
                        <div
                            onClick={() => {
                                this.setState({
                                    view: "UPDATE"
                                })
                            }}
                            className={"ux-tab" + (this.state.view == "UPDATE" ? " active" : "")}>
                            <i className="fal fa-database"></i>&nbsp;On update
                        </div>
                        <div
                            onClick={() => {
                                this.setState({
                                    view: "DELETE"
                                })
                            }}
                            className={"ux-tab" + (this.state.view == "DELETE" ? " active" : "")}>
                            <i className="fal fa-trash"></i>&nbsp;On delete
                        </div>
                    </div>
                </ewars.d.Row>
                <ewars.d.Row height="200px" borderBottom={true} style={{background: "#F2F2F2", overflowX: "auto"}}>
                    <ewars.d.Cell width="150px" style={{margin: "8px", background: "#FFFFFF"}}>

                    </ewars.d.Cell>

                    <ewars.d.Cell width="50px" style={{margin: "8px", background: "#FFFFFF"}}>
                        <i className="fal fa-plus"></i>
                    </ewars.d.Cell>

                </ewars.d.Row>
                <ewars.d.Row height="30px">
                    <input type="text" value="Stage Name"/>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell width="200px" borderRight={true}>
                        <div className="son-list-item">
                            <i className="fal fa-envelope"></i>&nbsp;Notifications
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-database"></i>&nbsp;Data mapping
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-tasks"></i>&nbsp;Task
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-users"></i>&nbsp;Stakeholders
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-code-branch"></i>&nbsp;Hooks
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-caret-circle-right"></i>&nbsp;Pre
                        </div>
                        <div className="son-list-item">
                            <i className="fal fa-caret-circle-left"></i>&nbsp;Post
                        </div>

                    </ewars.d.Cell>
                    <ewars.d.Cell style={{display: "flex"}}>
                        <ewars.d.Cell style={{display: "block", overflowY: "auto", paddingTop: "16px"}}>
                            <InitialStage label="Submitted"/>
                            <FormStage>
                                <StageAction/>
                                <StageAction/>
                                <StageAction/>
                            </FormStage>

                            <InitialStage label="Complete"/>

                        </ewars.d.Cell>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewWorkflow;