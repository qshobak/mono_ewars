const HELP_ITEMS = [
    {t: "Field Types", n: "FIELD_TYPES"},
    {t: "Mobile Support", n: "MOBILE_SUPPORT"},
    {t: "Stages", n: "STAGES"},
    {t: "Logic", n: "LOGIC"},
    {t: "Stage Actions", n: "STAGE_ACTIONS"}
]


class HelpItem extends React.Component {
    render() {
        return (
            <div className="help-item">
                {this.props.data.t}
            </div>
        )
    }
}

class ViewHelp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="250px">
                        <ewars.d.Panel>
                            {HELP_ITEMS.map(item => {
                                return <HelpItem data={item}/>
                            })}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewHelp;
