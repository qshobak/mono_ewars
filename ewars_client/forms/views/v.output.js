const STYLES = {
    LIST_ITEM: {
        display: "block",
        margin: "0px 8px 8px 8px",
        border: "1px solid rgba(255,255,255,0.5)",
        padding: "8px",
        cursor: "pointer"
    }
}

const DIM_ACTIONS = [
    ['fa-plus', 'ADD_DIM', 'Create dimension']
];

const MEAS_ACTIONS = [
    ['fa-plus', 'ADD_MEAS', 'Create measure']
]

class ListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="list-item" style={STYLES.LIST_ITEM}>
                {this.props.data.label.en || this.props.data.label}
                [{this.props.data.name || ""}]

            </div>
        )
    }
}



const DIM_FIELDS = ["select", "date", "location", 'lat_long', "text", "textarea"];
const MEAS_FIELDS = ['number'];

class ViewOutput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props);
        let dimensions = [],
            measures = [];

        for (let i in this.props.data.version.definition) {
            let field = this.props.data.version.definition[i];
            if (DIM_FIELDS.indexOf(field.type) >= 0) dimensions.push(field);
            if (MEAS_FIELDS.indexOf(field.type) >= 0) measures.push(field);

            if (field.fields) {
                for (let row in field.fields) {
                    let _row = field.fields[row];

                    for (let cell in _row.fields) {
                        let _cell = _row.fields[cell];
                        if (DIM_FIELDS.indexOf(_cell.type) >= 0) dimensions.push(_cell);
                        if (MEAS_FIELDS.indexOf(_cell.type) >= 0) measures.push(_cell);

                    }
                }
            }
        }


        return (
            <ewars.d.Layout style={{flexDirection: "row"}}>
                <ewars.d.Cell width="350px" borderRight={true} style={{display: "flex"}}>
                    <ewars.d.Toolbar label="Dimensions">
                        <ewars.d.ActionGroup
                            actions={DIM_ACTIONS}
                            right={true}
                            onAction={this._action}/>
                    </ewars.d.Toolbar>
                    <ewars.d.Row height="50%" style={{overflowY: "auto", display: "block", paddingTop: "8px"}}>
                        {dimensions.map(item => {
                            return <ListItem data={item}/>
                        })}
                    </ewars.d.Row>
                    <ewars.d.Toolbar label="Measures">
                        <ewars.d.ActionGroup
                            actions={MEAS_ACTIONS}
                            right={true}
                            onAction={this._action}/>
                    </ewars.d.Toolbar>
                    <ewars.d.Row height="50%" style={{overflowY: "auto", display: "block", paddingTop: "8px"}}>
                        {measures.map(item => {
                            return <ListItem data={item}/>
                        })}
                    </ewars.d.Row>

                </ewars.d.Cell>
                <ewars.d.Cell style={{display: "flex"}}>
                    <ewars.d.Toolbar>

                    </ewars.d.Toolbar>
                    <ewars.d.Cell style={{display: "block"}}>

                    </ewars.d.Cell>
                </ewars.d.Cell>
            </ewars.d.Layout>
        )
    }
}

export default ViewOutput;