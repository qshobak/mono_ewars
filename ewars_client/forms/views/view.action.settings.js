import { SelectField } from "../../common/fields";

const ACTION_TYPES = [
    ["GENERATE_RECORD", "Generate new record"],
    ["NOTIFICATION", "Send notification"],
    ["MODIFY_FORM", "Modify form"],
    ["MODIFY_RECORD", "Modify Record"],
    ["GENERATE_ALERT", "Generate alert"]
];

class ViewActionSettings extends React.Component {
    render() {
        return (
            <ewars.d.Panel>
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <div className="ide-section-basic">

                            <div className="header">Action</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Action Type</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="action"
                                        config={{
                                            options: ACTION_TYPES
                                        }}
                                        value={this.props.data.action}/>

                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Target Form</div>
                                <div className="ide-setting-control">

                                </div>
                            </div>

                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Form Mapping</div>

                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Form Modifications</div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Notification Targets</div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Notification Content</div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Conditions</div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Related Record Modifications</div>
                        </div>
                    </div>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default ViewActionSettings;
