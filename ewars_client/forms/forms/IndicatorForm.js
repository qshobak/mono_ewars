export default {
    indicator_id: {
        type: "indicator",
        label: {en: "Indicator"}
    },
    operation_type: {
        type: "select",
        label: {
            en: "Operation Type"
        },
        options: [
            ["ADDITION", "Add 'in' to indicator value"],
            ["SUBTRACTION", "Subtract 'in' from indicator value"],
            ["INCREMENT", "Increment Indicator"],
            ["DECREMENT", "Decrement Indicator"]
        ]
    }
};
