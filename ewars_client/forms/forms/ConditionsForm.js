export default {
    conditions: {
        type: "conditions",
        label: {
            en: "Conditions"
        },
        required: true,
        defaultValue: {
            application: "all",
            rules: [
                ["form_id", "ne", "NULL"]
            ]
        }
    }
};
