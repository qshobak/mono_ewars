import "@babel/polyfill";
window.__assign_hack__ = window.__hack__ = document.getElementById("application");

ewars = {};

const i18n = function (code) {
    let userLang = navigator.language || navigator.userLanguage;
    if (!code) return "UNKNOWN";
    // Check if this is an internationalization string
    if (JSON.stringify(code).indexOf("{") >= 0) {
        return code.en;
    }
    // Get users lang and load string
    if (__.__dict__[code]) {
        return __.__dict__[code];
    } else {
        return code;
    }
};
ewars.__ = i18n;
window.__ = i18n;
window._l = i18n;

window.__.__dict__ = EN;

window.__.register = function (items) {
    for (let key in items) {
        __.__dict__[key] = items[key];
    }
};

import notifications from "./components/notifications";

import formatters from "./utils/formatters";
import general from "./utils/general";
import Cache from "./utils/cache";
import prompt from "./components/prompt";
import Blocker from "./components/Blocker";
import Confirmation from "./components/Confirmation";
import Combi from "./lib/combi";
import EN from "./lang/en-en.js";


var ewars = {
    z: new Combi(),
    isSet: function (value) {
        if (value == undefined) return false;
        if (value == null) return false;
        if (value == "") return false;
        if (value == NaN) return false;
        if (Infinity) {
            if (value == Infinity) return false;
            if (value == -Infinity) return false;
        }
        return true;
    },
    notifications: notifications,
    utils: general,
    prompt: prompt,
    growl: function (content) {
        notifications.notification("fa-bullhorn", "", content, null, null);
    },
    error: function (message) {
        notifications.error(message);
    },
    cache: Cache,
    Blocker: Blocker,
    formats: formatters,
    formatters: formatters,
    I18N: formatters.I18N_FORMATTER,
    DATE: formatters.DATE_FORMATTER,
    NUM: formatters.NUM,
    notify: function (title, message) {
        notifications.notification("fa-bullhorn", title, message, null, null);
    },
    success: function (title, message) {
        notifications.notification("fa-check", title, message, null, "green");
    },
    isEmpty: function (obj) {
        if (obj == null) return true;
        if (obj == "") return true;
        return false;
    },
    Confirmation: Confirmation,
    fetch: function (uri) {
        return new Promise((resolve, reject) => {
            if (window.fetch) {
                fetch(uri)
                    .then((resp) => {
                        resolve(resp);
                    })
                    .catch((err) => {
                        reject(err);
                    })
            }
        });
    },
    _l: _l,
    help: function (source) {
        if (source.indexOf("page:") >= 0) {
            //. This is a help page
        } else {
            // This is a token and we need to retrieve it
        }
    },
    _saveRegister: [],
    registerSave: function (method) {
        ewars._saveRegister.push(method);
    },
    deregisterSave: function (method) {
        let removalIndex = null;
        ewars._saveRegister.forEach((item, index) => {
            if (item == method) {
                removalIndex = index;
            }
        })

        if (removalIndex != null)
            ewars._saveRegister.splice(removalIndex, 1);
    }
};

window.ewars = ewars;
ewars.d = {};
ewars.g = {};
ewars.g.online = true;

document.addEventListener("keydown", (e) => {
    if ((e.ctrlKey || e.metaKey) && e.key == "s") {
        e.preventDefault();
        ewars._saveRegister.forEach((method) => {
            method();
        })
    }
})
/**
 * Domain Setup
 */
if (["127.0.0.1", "localhost"].indexOf(document.domain) >= 0) {
    ewars.domain = document.domain + ":9000";
} else {
    ewars.domain = document.domain
}


/**
 * Convenience check to see if a user is an administrative role
 * @param user
 * @returns {boolean}
 */
ewars.isAdmin = function (user) {
    return false;
};

ewars.addEvent = function (object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on" + type] = callback;
    }
};

ewars.removeEvent = function (object, type, callback) {
    if (object == null || typeof(object) == "undefined") return;
    if (object.removeEventListener) {
        object.removeEventListener(type, callback);
    } else if (object.removeEvent) {
        object.removeEvent("on" + type, callback);
    } else {
        object['on' + type] = null;
    }
};

/**
 * Get the intersect between two arrays
 * @param a
 * @param b
 * @returns {Array}
 */
ewars.intersect = function intersect_safe(a, b) {
    var ai = bi = 0;
    var result = [];

    while (ai < a.length && bi < b.length) {
        if (a[ai] < b[bi]) {
            ai++;
        }
        else if (a[ai] > b[bi]) {
            bi++;
        }
        else /* they're equal */
        {
            result.push(ai);
            ai++;
            bi++;
        }
    }

    return result;
};

/**
 * Quick and dirty copy mechanism
 */
ewars.copy = function (data) {
    return JSON.parse(JSON.stringify(data));
};

var _queue = [];
var _queueLimit = 1;
var _isQueueRunning = false;
var WorkerQueue = function (frequency) {
    this.queue = [];
    this.timeout = 0;
    this.current = null;
    this.frequency = frequency;
    this.load = 0;
    this.max = 6;

    this.pop = function () {
        if (this.load >= this.max) return;
        let current;
        if (!this.queue.length) {
            window.clearInterval(this.timeout);
            this.timeout = 0;
            return;
        }

        current = this.queue.shift();
        this.load++;

        if (current) {
            let deferred = current[0];
            let uri = current[1];
            let query = current[2];

            //TODO testing rust
            // uri = "http://localhost:3000/ping";

            let xhr = new XMLHttpRequest();

            let oReq = new XMLHttpRequest();
            oReq.open("POST", uri, true);
            oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            if (window.dt) {
                if (window.dt.token) oReq.setRequestHeader("Authorization", window.dt.token);
            }

            oReq.onreadystatechange = function () {
                let DONE = 4;
                let OK = 200;

                if (oReq.readyState == DONE) {
                    if (oReq.status == OK) {
                        let resp = JSON.parse(oReq.responseText);
                        deferred[0](resp);
                        this.load--;
                    } else {
                        if (oReq.status == 500) {
                        }

                        if (oReq.status == 101) {
                        }
                        deferred[1](false);
                        this.load--;
                    }
                }
            }.bind(this);

            oReq.send(JSON.stringify(query));
        }
    };

    this.clear = function () {
        this.queue = [];
    };

    this.push = function (uri, query) {
        var self = this;
        let dResolve, dReject;
        let deferred = new Promise((resolve, reject) => {
            dResolve = resolve;
            dReject = reject;
        });
        this.queue.push([[dResolve, dReject], uri, query]);
        if (!this.timeout) {
            this.timeout = window.setInterval(function () {
                if (self.load < self.max) {
                    self.pop()
                }
            }, this.frequency);
        }
        this.pop();

        return deferred;
    }
};

ewars._queue = new WorkerQueue(6);

/**
 * Fallback method if websockets fail
 */
ewars.tx = function (method, args, kwargs) {
    return new Promise((resolve, reject) => {
        let oReq = new XMLHttpRequest();
        oReq.open("POST", "/api/_w", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        oReq.onreadystatechange = function () {
            let DONE = 4;
            let OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    if (oReq.responseText == "NO_AUTH") {
                        document.location = "/login";
                    }
                    let resp = JSON.parse(oReq.responseText);
                    resolve(resp.data);
                } else {
                    if (oReq.status == 500) {
                    }

                    if (oReq.status == 101) {
                    }
                    reject(oReq.status);
                }
            }
        }.bind(this);

        oReq.send(JSON.stringify([method, args, kwargs]));
    })
};

ewars.once = function (func) {
    var ran = false, memo;
    return function () {
        if (ran) return memo;
        ran = true;
        memo = func.apply(this.arguments);
        func = null;
        return memo;
    }
};

ewars.setKeyPath = function (obj, prop, value) {
    if (typeof prop === "string")
        prop = prop.split(".");

    if (prop.length > 1) {
        var e = prop.shift();
        ewars.setKeyPath(obj[e] = Object.prototype.toString.call(obj[e]) === "[object Object]" ? obj[e] : {},
            prop,
            value);
    } else {
        obj[prop[0]] = value;
    }
};

ewars.getKeyPath = function (path, obj) {
    try {
        return path.split('.').reduce((o, i) => o[i], obj)
    } catch (e) {
        return null;
    }
};

ewars.requestNewCookie = function () {
    $.ajax({
        url: "/cookie",
        contentType: "application/json",
        dataType: "json",
        success: function (resp) {
            if (resp.d != true) {
                ewars.notifications.notification("fa-exclamation-triangle", "Error", "There was an error requesting new cookie.", 3000, "red");
            }
        }
    })
};

var _events = {};
ewars.subscribe = function (event_name, callback, id) {
    var args = [[callback]];
    if (id) args[0].unshift(id);
    if (_events[event_name]) {
        _events[event_name].push.apply(_events[event_name], args);
    } else {
        _events[event_name] = [];
        _events[event_name].push.apply(_events[event_name], args);
    }
};

ewars.unsubscribe = function (event_name, id) {
    var eventArray = _events[event_name];

    var events = [];
    eventArray.forEach((item, index) => {
        if (item.length > 1) {
            if (item[0] != id) {
                events.push(item);
            }
        } else {
            events.push(item);
        }
    });

    _events[event_name] = events;
};

ewars.emit = function (event_name, data) {
    for (var i in _events[event_name]) {
        var callback = _events[event_name][i];
        if (callback.length > 1) {
            callback[1](data);
        } else {
            callback[0](data);
        }
    }
};

function _setConnectionClosed() {

    if (!document.getElementById("offline")) {
        var el = document.createElement("div");
        el.setAttribute("id", "offline");
        el.setAttribute("class", "offline");
        el.innerText = "You are currently offline, please check your internet connection before proceeding with any work.";
        document.body.appendChild(el)
    }
}

function _reconnected() {
    let node = document.getElementById("offline");
    node.parentNode.removeChild(node);
    ewars.notifications.notification("fa-plug", "Reconnected", "Your internet connection has been re-established.")
}

var forceOffline = false;
if (!window._isPDF) {

    if (!forceOffline) {
        window.setInterval(function () {
            if (navigator.onLine == false) {
                _setConnectionClosed();
                ewars.g.online = false;
            } else {
                if (!ewars.g.online) {
                    ewars.g.online = true;
                    _reconnected();
                }
            }
        }, 3000);
    }
}

window._isNull = (value) => {
    if (value == null) return true;
    if (value == undefined) return true;
    return false;
}

window._isNullOrEmpty = (value) => {
    if (value == null) return true;
    if (value == "") return true;
    if (value == undefined) return true;
    if (value instanceof Array) {
        if (value.length <= 0) return true;
    }

    return false;
};

let accLink = document.getElementById('acc-swapper'),
    accMenu = document.getElementById('acc-drop');

const checkAccMenuConstraint = (e) => {
    if (accMenu != e.target && !accMenu.contains(e.target)) {
        if (accMenu.style.display == 'block') {
            accMenu.style.display = 'none';
        }
    }
};

if (accLink) {
    accLink.addEventListener('click', (e) => {
        e.stopPropagation();
        accMenu.style.display = accMenu.style.display != 'none' ? 'none' : 'block';
        if (accMenu.style.display != 'none') {
            document.body.addEventListener('click', checkAccMenuConstraint);
        } else {
            document.body.removeEventListener('click', checkAccMenuConstraint);
        }
    });
}

const restArguments = (func, startIndex) => {
    startIndex = startIndex == null ? func.length - 1 : +startIndex;
    return function() {
        let length = Math.max(arguments.length - startIndex, 0),
            rest = Array(length),
            index = 0;
        for (; index < length; index++) {
            rest[index] = arguments[index + startIndex];
        }
        switch (startIndex) {
            case 0: return func.call(this, rest);
            case 1: return func.call(this, arguments[0], rest);
            case 3: return func.call(this, arguments[0], arguments[1], rest);
        }

        let args = Array(startIndex + 1);
        for (index = 0; index < startIndex; index++) {
            args[index] = arguments[index];
        }

        args[startIndex] = rest;
        return func.apply(this, args);
    };
};

const _delay = restArguments(function(func, wait, args) {
    return setTimeout(function() {
        return func.apply(null, args);
    }, wait);
});

ewars.debounce = function(func, wait, args) {
    let timeout, result;

    let later = function(context, args) {
        timeout = null;
        if (args) result = func.apply(context, args);
    }

    let debounced = restArguments(function(args) {
        if (timeout) clearTimeout(timeout);
        if (immediate) {
            let callNow = !timeout;
            timeout = setTimeout(later, wait);
            if (callNow) result = func.apply(this, args);
        } else {
            tiemout = _delay(later, wait, this, args);
        }
        return result;
    });

    debounced.cancel = function() {
        clearTimeout(timeout);
        timeout = null;
    };

    return debounced;
}


export default ewars;
