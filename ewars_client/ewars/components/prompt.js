const OK_TEXT = "Confirm";
const CANCEL_TEXT = "Cancel";

function _fadeOut(el) {
    el.style.opacity = 1;

    (function fade() {
        if ((el.style.opacity -= .1) < 0) {
            el.parentNode.removeChild(el);
        } else {
            requestAnimationFrame(fade);
        }
    })();
}

const Prompt = function (icon, title, content, callback, cancelCallback) {
    var _id = ewars.utils.uuid();

    var _base = document.createElement("div");
    _base.setAttribute("class", "prompt-wrapper");
    _base.setAttribute("id", _id);

    var _main = document.createElement("div");
    _main.setAttribute("class", "prompt-container");

    var _header = document.createElement("div");
    _header.setAttribute("class", "prompt-header");

    // Set up icon
    var _icon = document.createElement("div");
    _icon.setAttribute("class", "prompt-icon");
    var _iconItem = document.createElement("i");
    _iconItem.setAttribute("class", "fal " + icon);
    _icon.appendChild(_iconItem);

    // Set up prompt title
    var _title = document.createElement("div");
    _title.setAttribute("class", "prompt-title");

    var _titleText = document.createTextNode(title);
    _title.appendChild(_titleText);
    _header.appendChild(_icon);
    _header.appendChild(_title);

    _main.appendChild(_header);
    // Header setup complete

    var _body = document.createElement("div");
    _body.setAttribute("class", "prompt-body");
    var _content = document.createTextNode(content);
    _body.appendChild(_content);

    _main.appendChild(_body);
    // Main body set up

    // Buttons
    var buttonsWrapper = document.createElement("div");
    buttonsWrapper.setAttribute("class", "footer");

    var okBtn = document.createElement('button');
    okBtn.setAttribute("class", "btn-flat green");
    okBtn.appendChild(document.createTextNode(OK_TEXT));
    okBtn.addEventListener("click", function () {
        var item = document.getElementById(_id);
        _fadeOut(item);
        callback();
    });

    var cancelBtn = document.createElement("button");
    cancelBtn.setAttribute("class", "btn-flat red");
    cancelBtn.appendChild(document.createTextNode(CANCEL_TEXT));
    cancelBtn.addEventListener("click", function () {
        var item = document.getElementById(_id);
        _fadeOut(item);
        if (cancelCallback) cancelCallback();
    });

    var btnGroup = document.createElement("div");
    btnGroup.setAttribute("class", "btn-group pull-right");
    btnGroup.appendChild(okBtn);
    btnGroup.appendChild(cancelBtn);
    buttonsWrapper.appendChild(btnGroup);

    // BUG: Need to fix layout with pull-right
    var clearer = document.createElement("div");
    clearer.setAttribute("class", "clearer");
    buttonsWrapper.appendChild(clearer);

    _main.appendChild(buttonsWrapper);
    _base.appendChild(_main);

    document.getElementsByTagName('body')[0].appendChild(_base);

    return {
        el: _base,
        id: _id,
        close: function () {},
        show: function () {}
    }

};

export default Prompt;
