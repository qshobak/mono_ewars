export default {
    CHART_TITLE: "Chart Title",
    CHART_TYPE: "Chart Type",
    SAMPLE_INTERVAL: "Sampling Interval",
    LOCATION: "Location",
    START_DATE: "Start Date",
    END_DATE: "End Date",
    REDUCTION: "Reduction",
    ASSIGNMENT: "Assignment",
    REPORT_DATE: "Report Date",
    BARCODE: "Barcode Scanning"
}