var cache = {};

function _createCache (id, data) {
    cache[id] = data;
}

function _getCached (key, data) {
    if (cache[key]) return cache[key];
    return null;
}

export default {
    get: _getCached,
    add: _createCache
};
