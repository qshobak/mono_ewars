import Moment from "moment";

export default {
    getNearestPastReportDate: function (source_date, interval) {

    },
    getIntervalDates: function (start_date, end_date, interval) {
        var start = Moment(start_date);
        var end = Moment(end_date);
        var cur = start;

        var intervalDates = [];

        if (interval == "DAY") {
            while (cur.isBefore(end, 'day')) {
                cur = cur.add(1, "days");
                intervalDates.push(cur);
            }
            intervalDates.push(end);
        }

        if (interval == "YEAR") {
            while(cur.isBefore(end, "YEAR")) {
                cur = cur.add(1, "years");
                intervalDates.push(cur);
            }
            intervalDates.push(end);
        }

        if (interval == "WEEK") {
            while(cur.isBefore(end, "isoweeks")) {

            }
        }

        return intervalDates;
    }
};
