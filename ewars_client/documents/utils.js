export default class Utils {
    static formatData = (data) => {
        let sheets = [];
        let raws = [];
        data.forEach((dataPoint) => {
            if (dataPoint) {
                if (dataPoint[0] == "RAW") {
                    let dp = dataPoint[1][1];
                    let dc = dataPoint[1][0];
                    raws.push(
                        [
                            __(dc.title || "No Title"),
                            ewars.NUM(dp.data, dc.format || "0,0")
                        ]
                    )
                } else if (dataPoint[0] == "MAP") {
                    let mc = dataPoint[1][0];
                    let md = dataPoint[1][1];

                    let rowHeaders = [[
                        "Location",
                        "Pcode",
                        "Value"
                    ]];

                    let rows = [];

                    let indName = null;
                    md.d.forEach((loc) => {
                        rows.push([
                            __(loc.location.name),
                            loc.location.pcode,
                            loc.data
                        ])
                    })

                    rowHeaders.push.apply(rowHeaders, rows);
                    sheets.push([__(mc.title || "Map - No Title"), rowHeaders]);
                } else if (dataPoint[0] == "TABLE") {
                    let tc = dataPoint[1][0];
                    let td = dataPoint[1][1];

                    let matrix = [];
                    let headers = [
                        "Location",
                        "PCode",
                    ];

                    if (tc.b_parent_col) headers.unshift("Parent");

                    let hUUIDS = [];
                    tc.columns.forEach((col) => {
                        hUUIDS.push(col.uuid);
                        headers.push(__(col.title || "No Title"));
                    });


                    let rows = [];

                    for (let i in td) {
                        let row = [];
                        if (tc.b_parent_col) row.push(td[i].parent_name);
                        row.push(__(td[i].name));
                        row.push(td[i].pcode);
                        hUUIDS.forEach((id) => {
                            row.push(td[i].data[id]);
                        });
                        rows.push(row);
                    }

                    matrix.push(headers);
                    matrix.push.apply(matrix, rows);


                    sheets.push([__(tc.title || "Unknown Table"), matrix]);
                } else if (dataPoint[0] == "SERIES") {
                    let sc = dataPoint[1][0];
                    let sd = dataPoint[1][1];

                    let rowHeaders = [];
                    let dateHeaders = [];
                    let matrix = {};

                    sd.forEach((series, sI) => {
                        let rowLabel = __(series.series.title);
                        if (series.series.loc_spec == "GENERATOR") {
                            rowLabel = __(series.location.name) + " - " + __(series.series.title);
                        }
                        rowHeaders.push(rowLabel);

                        series.data.forEach((d, index) => {
                            dateHeaders.push(d[0]);
                            if (matrix[d[0]]) {
                                matrix[d[0]][String(sI)] = ewars.NUM(d[1] || 0, sc.y_axis_format || "0");
                            } else {
                                matrix[d[0]] = {};
                                matrix[d[0]][String(sI)] = ewars.NUM(d[1] || 0, sc.y_axis_format || "0");
                            }
                        })
                    });

                    let c_dateHeaders = [];
                    dateHeaders.forEach(item => {
                        if (c_dateHeaders.indexOf(item) < 0) c_dateHeaders.push(item);
                    });
                    dateHeaders = c_dateHeaders;

                    dateHeaders = dateHeaders.sort((a, b) => {
                        if (a < b) return -1;
                        if (a > b) return 1;
                        return 0;
                    });

                    let sheet = [
                        [""].concat(dateHeaders)
                    ];

                    rowHeaders.forEach((row, index) => {
                        let sheetRow = [row];
                        dateHeaders.forEach((dt) => {
                            sheetRow.push(matrix[dt][String(index)]);
                        });
                        sheet.push(sheetRow);
                    });

                    sheets.push([ewars.I18N(sc.title), sheet]);

                } else if (dataPoint[0] == "CATEGORY") {
                    let cc = dataPoint[1][0];
                    let sd = dataPoint[1][1];

                    let rows = [];
                    sd.forEach((series) => {
                        rows.push([
                            __(series.series.title || "No Title"),
                            ewars.NUM(series.result.data, cc.y_axis_format || "0")
                        ])
                    });

                    sheets.push([ewars.I18N(cc.title), rows])


                }
            }


        });

        return [raws, sheets];
    }
}
