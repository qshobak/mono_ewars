var AppDispatcher = require("./AppDispatcher");
var EventEmitter = require("events").EventEmitter;
var assign = require('object-assign');

const CONSTANTS = require("../../common/constants");

const CHANGE_EVENT = "CHANGE";

STATE = {

};


var Store = assign({}, EventEmitter.prototype, {
    getAll: function () {

    },

    emitChange: function (event) {
        this.emit(event);
    },

    addChangeListener: function (event, callback) {
        this.on(event, callback);
    },

    removeChangeListener: function (event, callback) {
        this.removeListener(event, callback);
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        let action = payload.action.actionType;
        let data = payload.action.data;

        // Add a new tab to the interface
        if (action == "ADD_TAB") {
            _addTab(data);
            Store.emitChange("ROOT_CHANGE");
        }



        return true;
    })
});

module.exports = Store;