var CoreDispatcher = require("../../common/CoreDispatcher");
var assign = require("object-assign");

var AppDispatcher = assign({}, CoreDispatcher.prototype, {
    handleViewAction: function (action) {
        this.dispatch({
            source: "VIEW_ACTION",
            action: action
        })
    }
});

module.exports = AppDispatcher;
