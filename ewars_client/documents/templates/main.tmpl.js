module.exports = [
    '<div class="ide-layout">',
        '<div class="ide-row">',
            '<div class="ide-col" id="dms-shoulder-wrap" style="max-width: 250px">',
                '<div class="ide-panel border-right ide-scroll ide-panel-absolute">',
                    '<div class="reports" id="dms-shoulder">',
                    '</div>',
                '</div>',
            '</div>',
            '<div class="ide-col documents-dashboard" id="dms-main">',
            '</div>',
        '</div>',
    '</div>'
].join("");