import { DataTable } from "../../../common";
import ReportUtils from "../../utils/ReportUtils";
import Report from "../views/Report.react";

let COLUMNS = [
    {
        name: "null",
        config: {
            label:"Document Title",
            type:"text"
        }
    },
    {
        name: "data_date",
        config: {
            label: "Document Date",
            type: "date",
            filterKey: "data_date"
        },
        fmt: ewars.DATE
    },
    {
        name: "location.name",
        config: {
            label: "Location",
            type: "location"
        },
        fmt: ewars.I18N
    },
    {
        name: "form.name",
        config: {
            label: "Form",
            type: "text"
        },
        fmt: ewars.I18N
    },
    {
        name: "user.name",
        config: {
            label: "User",
            type: "text"
        }
    }
];

const ACTIONS = [
    {label: "PDF", icon: "fa-file-pdf", action: "PDF"},
    {label: "View", icon: "fa-eye", action: "VIEW"},
    {label: "Share", icon: "fa-share", action: "SHARE"}
];

const SELECTS = [
    "uuid",
    "data_date",
    "location_id",
    "created_by",
    "form_id"
];

const JOINS = [
    "form:form_id:id:id,name",
    "location:location_id:uuid:uuid,name",
    "user:created_by:id:name"
];

const ORDER = {
    data_date: "DESC"
};

/**
 * Renders a data table of reports with the titles changed to
 * represent the report title
 */
class PerReport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            report: null,
            showShade: false
        }
    }

    _view = (row) => {
        this.setState({
            view: "REPORT",
            report: row
        });
    };

    _clear = () => {
        this.setState({
            view: "DEFAULT",
            report: null
        })
    };

    _shareReport = (report) => {
        this.setState({
            report: report,
            showShade: true
        })
    };

    _download = (row) => {
        let bl = new ewars.Blocker(null, "Generating pdf...");

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        if (this.props.data) segments.d = ewars.DATE(this.props.data, "DAY");
        segments.c = row.uuid;
        segments.l = row.location_id;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.name);
            }
        };

        r.send();
    };

    _onAction = (action, cell, row) => {
        console.log(action, row, cell);

        if (action == "PDF") this._download(row);
        if (action == "CLICK") this._view(row);
        if (action == "VIEW") this._view(row);
    };

    render() {

        COLUMNS[0].fmt = (col, row) => {
            return ReportUtils.applyFormattingTags(
                ewars.I18N(this.props.data.instance_name),
                {
                    location: row.location,
                    doc_date: row.data_date,
                    interval: "DAY"
                }
            )
        };

        let filter = {
            form_id: {eq: this.props.data.generation.source_form_id},
            status: {eq: "SUBMITTED"}
        };

        let view;
        if (this.state.view == "DEFAULT") {
            view = (
                <DataTable
                    filter={filter}
                    join={JOINS}
                    order={ORDER}
                    columns={COLUMNS}
                    actions={ACTIONS}
                    onCellAction={this._onAction}
                    resource="collection"/>
            )
        }

        if (this.state.view == "REPORT") {
            view = (
                <ewars.d.Layout>
                    <ewars.d.Toolbar>
                        <div className="btn-group">
                            <ewars.d.Button
                                icon="fa-caret-left"
                                onClick={this._clear}
                                label="Back"/>
                        </div>

                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-file-pdf"
                                onClick={() => {this._download(this.state.report);}}/>
                            <ewars.d.Button
                                icon="fa-share"
                                onClick={this._share}/>

                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <Report
                                    data={{
                                        doc_date: this.state.report.data_date,
                                        location: this.state.report.location,
                                        uuid: this.props.data.uuid,
                                        template_type: this.props.data.template_type
                                    }}/>
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Shade
                        label="Share Document"
                        onAction={this._onModalAction}
                        show={this.state.showShade}>

                    </ewars.d.Shade>
                </ewars.d.Layout>
            )
        }

        return view;
    }
}

export default PerReport;
