import Report from "../views/Report.react";
import ReportUtils from "../../utils/ReportUtils";

class OneOff extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showShade: false,
            shareEmails: ""
        }

        ewars.subscribe("DATA_AVAILABLE", this._dataAvailable);
    };

    componentWillUnmount() {
        ewars.unsubscribe("DATA_AVAILABLE", this._dataAvailable);
    }

    _dataAvailable = (data) => {
        this.setState({
            reportData: data,
            canDownload: true
        })
    };

    _downloadData = () => {
        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.data.instance_name),
            {
                location: {uuid: window.user.clid, name: "Global"},
                doc_date: this.props.data.generation.document_date,
                interval: "DAY"
            }
        );

        let bl = new ewars.Blocker(null, "Generating excel...");

        ewars.tx("com.ewars.document.data", [instanceName, this.state.reportData[0], this.state.reportData[1]])
            .then((resp) => {
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.n);
            })

    };

    _onModalAction = (action) => {
        this.setState({
            showShade: false,
            shareEmails: ""
        })
    };

    _share = (report, location) => {
        if (report) {
            let doc_date = report;
            if (Moment.isMoment(doc_date)) {
                doc_date = doc_date.format("YYYY-MM-DD");
            }
            this.setState({
                showShade: true,
                report: {
                    doc_date: this.props.data.generation.document_date,
                    location: window.user.clid
                }
            });
        } else {
            this.setState({
                showShade: true
            })
        }
    };

    _onEmailsChange = (e) => {
        this.setState({
            shareEmails: e.target.value
        })
    };

    _sendShare = () => {
        let bl = new ewars.Blocker(null, "Sharing document...");

        let uri = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        segments.d = this.props.data.generation.document_date;
        segments.l = window.user.clid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.data.instance_name),
            {
                location: {uuid: window.user.clid, name: "Global"},
                doc_date: this.props.data.generation.document_date,
                interval: "DAY"
            }
        );

        ewars.tx('com.ewars.document.share', [uri, this.state.shareEmails, instanceName])
            .then((resp) => {
                bl.destroy();
                this.setState({
                    showShare: false,
                    shareEmails: ""
                });
                ewars.growl("Document shared");

            })
    };

    _download = (report) => {
        let bl = new ewars.Blocker(null, "Generating pdf...");

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        segments.d = this.props.data.generation.document_date;
        segments.l = window.user.clid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.name);
            }
        };

        r.send();
    };


    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._download}
                            icon="fa-file-pdf"/>
                        <ewars.d.Button
                            onClick={this._share}
                            icon="fa-share"/>
                        {this.state.canDownload ?
                        <ewars.d.Button
                            onClick={this._downloadData}
                            icon="fa-download"/>
                            : null}
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <Report
                            data={{
                                doc_date: this.props.data.generation.document_date,
                                location: {uuid: window.user.clid},
                                uuid: this.props.data.uuid,
                                template_type: this.props.data.template_type
                            }}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Share Document"
                    onAction={this._onModalAction}
                    shown={this.state.showShade}>
                    <div className="article" style={{padding: 16}}>
                        <p>Enter an email address (or multiple email addresses separated by commas (,) to share this
                            document.</p>

                        <label>Email Addresses</label>
                        <input type="text" value={this.state.shareEmails} onChange={this._onEmailsChange}/>
                        <div className="clearer" style={{height: 20}}></div>

                        <ewars.d.Button
                            label="Share"
                            onClick={this._sendShare}/>
                    </div>
                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }

}

export default OneOff;
