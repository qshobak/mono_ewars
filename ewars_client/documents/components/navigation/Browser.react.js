import Moment from "moment";

import { Button } from "../../../common";
import LocationTreeView from "../../../locations/components/LocationTreeView.react";

import Generated from "../template_types/Generated";
import OneOff from "../template_types/OneOff";
import PerReport from "../template_types/PerReport";

const VIEWS = {
    GENERATED: Generated,
    ONE_OFF: OneOff,
    PER_REPORT: PerReport
};

class Browser extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = (year, month) => {
        this.props.onPeriodSelect(year, month);
    };

    render() {
        let ViewCmp = VIEWS[this.props.data.template_type];
        let view = <ViewCmp data={this.props.data}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={ewars.I18N(this.props.data.template_name)}>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Browser;
