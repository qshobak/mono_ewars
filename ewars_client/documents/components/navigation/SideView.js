import Documents from "./Documents.react";

class SideView extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            collapsed: null
        }
    }

    _toggleCollapse = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    };

    _onTemplateSelect = (data) => {
        this.props.onSelect(data);
    };

    render() {
        let dWidth = "300px",
            dIcon = "fa-angle-double-left";
        if (this.state.collapsed) {
            dWidth = "44px";
            dIcon = "fa-angle-double-right";
        }

        return (
            <ewars.d.Cell
                width={dWidth}
                borderTop={true}
                borderRight={true}>
                <ewars.d.Layout>
                    <ewars.d.Toolbar>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this._toggleCollapse}
                                icon={dIcon}/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            {!this.state.collapsed ?
                                <Documents
                                    onTemplateSelect={this._onTemplateSelect}/>
                                : null}

                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </ewars.d.Cell>
        )
    }
}

export default SideView;