import Component from "./components/Main.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Panel,
    Toolbar
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Panel,
    Modal,
    Button,
    Toolbar
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);
