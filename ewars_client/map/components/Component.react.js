import Main from "./Main";
import Editor from "./Editor";

function _addScale(state, scale) {
    state.map.definition.scales.push(scale);
    return state;
}

function _setProp(state, prop, value) {

    if (prop.indexOf(".") >= 0) {
        ewars.setKeyPath(state.map, prop, value);
    } else {
        state.map[prop] = value;
    }

    return state;
}

const map = (state = null, action, data) => {
    let tState = ewars.copy(state);

    switch(action) {
        case "SET_MAP":
            return {map: data.data};
        case "SET_MAP_PROP":
            return _setProp(tState, data.prop, data.value);
        case "MAP_ADD_SCALE":
            return _addScale(tState, data.data);
        case "MAP_REMOVE_SCALE":
            tState.map.definition.scales.splice(data.index, 1);
            return tState;
        case "MAP_SET_SCALES":
            tState.map.definition.scales = data.data;
            return tState;
        default:
            return state;
    }
};

class Component extends React.Component {
    constructor(props) {
        super(props);

        ewars.z.register("MAP", map, {map: null});

        this.state = {
            view: "MAIN",
            edit: ewars.z.getState("MAP").map
        }
    }

    componentWillMount() {
        ewars.z.subscribe("MAP", this._onStoreChange);
    }

    componentWillUnmount() {
        ewars.z.unsubscribe("MAP", this._onStoreChange);
    }

    _onStoreChange = () => {
        let map = ewars.z.getState("MAP").map;
        let view = this.state.view;
        if (!map) view = "MAIN";
        this.setState({
            ...this.state,
            view: view,
            edit: ewars.z.getState("MAP").map
        })
    };

    render() {

        let view;

        if (this.state.edit) {
            view = <Editor
                data={this.state.edit}
                onChange={this._onChange}/>;
        } else {
            view = <Main
                onSelect={this._onEdit}/>;
        }

        return view;
    }
}

export default Component;
