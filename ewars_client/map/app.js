import Component from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    Form
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    Form
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


