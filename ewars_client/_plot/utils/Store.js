var AppDispatcher = require("./AppDispatcher");
var EventEmitter = require("events").EventEmitter;
var assign = require('object-assign');

const CONSTANTS = require("../../common/constants");

const CHANGE_EVENT = "CHANGE";

var STATE = {
    config: {
        widget_type: "LINE",
        interval: "DAY",
        start_date: new Date(),
        end_date: new Date(),
        y_axis_label: "Number",
        x_axis_label: "Date",
        format: "0,0.0",
        navigator: false,
        title: "New Chart",
        type: "SERIES"
    },
    indicators: {},
    locations: {}
};

function _updateConfigItem (prop, value) {
    STATE.config[prop] = value;
}
function _updateData(data) {
    STATE.config.series = data;
}
function _updateDataItem(index, prop, value) {
    STATE.config.series[index][prop] = value;
}
function _removeDataItem(index) {
    STATE.config.series.splice(index, 1);
}
var Store = assign({}, EventEmitter.prototype, {
    getAll: function () {

    },

    emitChange: function (event) {
        this.emit(event);
    },

    addChangeListener: function (event, callback) {
        this.on(event, callback);
    },

    removeChangeListener: function (event, callback) {
        this.removeListener(event, callback);
    },

    getChartState: function () {
        return STATE
    },


    dispatcherIndex: AppDispatcher.register(function (payload) {
        let action = payload.action.actionType;
        let data = payload.action.data;

        switch (action) {
            case "ADD_INDICATOR":
                Store.emitChange("ROOT_CHANGE");
                break;
            case "UPDATE_CHART_SETTING":
                _updateConfigItem(data.prop, data.value);
                Store.emitChange("SETTING_CHANGE");
                break;
            case "UPDATE_DATA":
                _updateData(data);
                Store.emitChange("SETTING_CHANGE");
                break;
            case "UPDATE_DATA_ITEM":
                _updateDataItem(data[0], data[1], data[2]);
                Store.emitChange("SETTING_CHANGE");
                break;
            case "REMOVE_DATA_ITEM":
                _removeDataItem(data.index);
                Store.emitChange("SETTING_CHANGE");
                break;
            default:
                break;
        }

        return true;
    })
});

module.exports = Store;