const FORM = [
    {
        _o: 0,
        n: "title",
        t: "TEXT",
        l: "TITLE",
        r: true
    },
    {
        _o: 1,
        t: "BUTTONSET",
        l: "Public?",
        n: "public",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        t: "TEXT",
        l: "Description",
        n: "description",
        ml: true
    }
]

class General extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel>
                <ewars.d.SystemForm
                    definition={FORM}
                    data={{}}
                    vertical={true}
                    onChange={this._onChange}
                    enabled={true}/>
            </ewars.d.Panel>
        )
    }
}

export default General;