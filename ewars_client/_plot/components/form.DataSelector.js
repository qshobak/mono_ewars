const FORM = [
    {
        _o: -1,
        l: "General",
        t: "H",
        i: "fa-cog"
    },
    {
        _o: 0,
        n: "title",
        t: "TEXT",
        l: "DATA_SOURCE_NAME"
    },
    {
        _o: 0.01,
        t: "H",
        i: "fa-code-branch",
        l: "Data Source"
    },
    {
        _o: 0.02,
        t: "DATASOURCEDROP",
        n: "sources",
        l: "Source(s)",
        r: true
    },
    {
        _o: 0.1,
        t: "H",
        i: "fa-map-marker",
        l: "Source location"
    },
    {
        _o: 1,
        n: "location_spec",
        t: "BUTTONSET",
        l: "Source location",
        o: [
            ["SPECIFIC", "Specific location"],
            ["GENERATOR", "Location(s) of type"],
            ["GROUPS", "Location group(s)"]
        ]
    },
    {
        _o: 2,
        n: "location",
        t: "LOCATION",
        l: "Location",
        c: [
            "ANY",
            "location_spec:=:SPECIFIC",
            "location_spec:=:GENERATOR"
        ]
    },
    {
        _o: 3,
        n: "site_type_id",
        l: "Location type",
        t: "SELECT",
        os: [
            "location_type",
            "id",
            "name"
        ],
        c: [
            "ALL",
            "location_spec:=:GENERATOR"
        ]
    },
    {
        _o: 4,
        n: "groups",
        l: "Location group(s)",
        t: "LOCGROUP",
        c: [
            "ALL",
            "location_spec:=:GROUPS"
        ]
    },
    {
        _o: 5,
        l: "Source time period",
        t: "H",
        i: "fa-calendar"
    },
    {
        _o: 5,
        n: "period_source",
        l: "Period",
        t: "BUTTONSET",
        o: [
            ["INHERIT", "Inherit"],
            ["OVERRIDE", "Override"]
        ]
    },
    {
        _o: 6,
        n: "period",
        t: "PERIOD",
        l: "Date range",
        c: [
            "ALL",
            "period_source:=:OVERRIDE"
        ]
    },
    {
        _o: 7,
        n: "reduction",
        l: "Reduction",
        t: "BUTTONSET",
        o: [
            ["SUM", "Sum/Cumulative"],
            ["AVG", "Average/Median"]
        ]
    },
    {
        _o: 7.1,
        l: "Styling",
        t: "H",
        i: "fa-paint-brush"
    },
    {
        _o: 8,
        m: "color",
        n: "color",
        t: "COLOUR",
        l: "Colour"
    }

];

export default FORM;
