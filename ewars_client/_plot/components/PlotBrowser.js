class Map extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {

        if (this.props.data.created_by != window.user.id) {
            return (
                <div className="block" onClick={this._onClick}>
                    <div className="block-content">
                        <div className="ide-row">
                            <div className="ide-col">{this.props.data.name}</div>
                            <div className="ide-col" style={{textAlign: "right"}}>
                                {this.props.data.user_name} [{this.props.data.user_email}]
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="block" onClick={this._onClick}>
                    <div className="block-content">{this.props.data.name}</div>
                </div>
            )
        }
    }
}

class PlotBrowser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.plots", ["USER"])
            .then(function (resp) {
                this.setState({
                    data: resp
                })
            }.bind(this))
    };

    showShared = () => {
        ewars.tx("com.ewars.plot", ["SHARED"])
            .then(function (resp) {
                this.setState({
                    data: resp
                })
            }.bind(this))
    };

    showUsers = () => {
        ewars.tx("com.ewars.plot", ["USER"])
            .then(function (resp) {
                this.setState({
                    data: resp
                })
            }.bind(this))
    };

    _onSelect = (data) => {
        this.props.onSelect(data);
    };


    render() {

        let items = this.state.data.map(item => {
            return <Map data={item} onClick={this._onSelect}/>
        });

        if (items.length <= 0) items = <div className="placeholder">There are currently no plots available</div>;

        return (
            <div className="ide" style={{minHeight: 300, height: 300}}>
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col border-right" style={{maxWidth: 200}}>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="block-tree">
                                    <div className="block" onClick={this.showUsers}>
                                        <div className="block-content">Saved Plot(s)</div>
                                    </div>
                                    <div className="block" onClick={this.showShared}>
                                        <div className="block-content">Shared</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="ide-col">
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="block-tree">
                                    {items}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PlotBrowser;