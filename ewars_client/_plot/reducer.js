const addColumn = (state, data) => {
    if (!state.definition.columns) state.definition.columns = [];
    let item;
    if (data[0] == "dimensions") {
        item = ["DIM", data[1][1], null, {}, ewars.utils.uuid()];
    }

    if (data[0] == "indicator") {
        item = ["I", data[1].uuid, null, {}, ewars.utils.uuid()];
    }
    state.definition.columns.push(item);

    return state;
};

const addRow = (state, data) => {
    if (!state.definition.rows) state.definition.rows = [];
    let item;
    if (data[0] == "dimensions") {
        item = ["DIM", data[1][1], null, {}, ewars.utils.uuid()];
    }

    if (data[0] === "indicator") {
        item = ["I", data[1].uuid, null, {}, ewars.utils.uuid()];
    }
    if (item) {
        state.definition.rows.push(item);
    }
    return state;
};

const reducer = (state, action, data) => {
    {
        let newState = ewars.copy(state);

        switch (action) {
            case "SET_VIEW":
                newState.view = data;
                return newState;
            case "ADD_COL":
                return addColumn(newState, data);
            case "ADD_ROW":
                return addRow(newState, data);
            case "SET_ACTIVE_DROP":
                newState.activeDrop = data;
                return newState;
            default:
                return newState;
        }
    }
};

export default reducer;