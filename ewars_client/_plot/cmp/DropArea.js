import STYLES from "../styles";

const NAMES = {
    DATE: "Date",
    LOCATION: "Location"
};

ewars.g.indicators = {};

class DropItem extends React.Component {
    static defaultProps = {
        activeDrop: null
    };

    constructor(props) {
        super(props);

        this.state = {data: null};

        if (this.props.data[0] == "I") {
            ewars.tx('com.ewars.resource', ['indicator', this.props.data[1], ["uuid", "name"], null])
                .then((resp) => {
                    ewars.g.indicators[resp.uuid] = resp;
                    this.setState({
                        data: resp
                    })
                })
        }
    }

    _toggleActive = () => {
        ewars.z.dispatch("E", "SET_ACTIVE_DROP", this.props.data);
    };

    render() {
        let uuid = this.props.data[this.props.data.length - 1];
        let label;

        if (this.props.data[0] == "DIM") {
            label = NAMES[this.props.data[1][0]];
        } else if (this.props.data[0] == "I") {
            if (!this.state.data) {
                label = "...";
            } else {
               label = __(this.state.data.name);
            }
        }

        let style = {};

        if (this.props.activeDrop) {
            if (this.props.activeDrop[this.props.activeDrop.length -1 ] == uuid) style.background = "blue";
        }

        return (
            <div style={{...STYLES.DROP_ITEM, ...style}} onClick={this._toggleActive}>
                <ewars.d.Row>
                    <ewars.d.Cell style={{marginRight: "8px"}}>{label}</ewars.d.Cell>
                    <ewars.d.Cell width="15px">
                        <i className="fal fa-caret-down"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class DropArea extends React.Component {
    static defaultProps = {
        name: null,
        icon: null,
        target: "COL",
        items: [],
        activeDrop: null
    };

    constructor(props) {
        super(props);

        ewars.subscribe("DRAG_START", this._onDragAlive);
        ewars.subscribe("DRAG_DEAD", this._onDragDead);
    }

    _onDragAlive = () => {
        this._el.style.background = "rgba(0,0,0,0.4)";
        this._el.style.display = "block";
    };

    _onDragDead = () => {
        this._el.style.background = "transparent";
        this._el.style.display = "none";
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("e"));
        if (this.props.target == "COL") {
            ewars.emit("ADD_COL", data);
            ewars.z.dispatch("E", "ADD_COL", data);
        } else {
            ewars.emit("ADD_ROW", data);
            ewars.z.dispatch("E", "ADD_ROW", data);
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    render() {
        return (
            <ewars.d.Row height="44px">
                <ewars.d.Cell>
                    <div style={STYLES.PLOT_COLS}>

                        <ewars.d.Row>
                            <ewars.d.Cell width="100px" style={STYLES.DIM_LABEL}>
                                <i className="fal fa-list"></i>&nbsp;{this.props.name}
                            </ewars.d.Cell>
                            <ewars.d.Cell style={{display: "block", overflowX: "scroll"}}>
                                <div
                                    onDrop={this._onDrop}
                                    onDragOver={this._onDragOver}
                                    className="drop-area"
                                    style={{height: "100%", width: "100%", display: "none"}}
                                    ref={(el) => {
                                        this._el = el
                                    }}>
                                </div>
                                {this.props.items.map((item) => {
                                    return <DropItem data={item} activeDrop={this.props.activeDrop}/>;
                                })}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

export default DropArea;