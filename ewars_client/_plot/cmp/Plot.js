import * as d3 from "d3";


const REQUIREMENTS = {
}

class Dimension {
    constructor(r, props, parent) {
        this.r = r;
        this._parent = parent;
        this.type = props[0];
        this.id = props[1];
        this.name = props[2];
        this.config = props[3] || {};
        this._index = 0;
        this._indicator = null;
        this._location = null;
        this._reduction = "SUM";

        this._loaded = false;

        this._id = props[props.length - 1];

        this._mean = null;
        this._sum = null;
        this._data = {};

        this._g = null;

        if (["indicator"].indexOf(this.type) >= 0) {
            this._load();
        } else {
            // form field, data set, dimension
        }
    }

    _load = () => {
        let query = {
            interval: "DAY",
            start_date: "2016-01-01",
            end_date: new Date().toISOString(),
            type: "SERIES",
            location: window.user.clid,
            indicator: this.id
        };

        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                this._indicator = resp.indicator;
                this._location = resp.location;
                this._process(resp.data);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    attr = (attr, value) => {
        switch(attr) {
            case "locationName":
                return __(this.location.name || "Unknown");
            case "sourceName":
                return __(this._indicator.name || "Unknown");
            case "sum":
                return this._sum;
            case "mean":
                return this._mean;
            default:
                return null;
        }
    };

    _process = (data) => {
        this._sum = 0;
        data.forEach((item) => {
            let val = parseFloat(item[1] || 0.0);
            this._sum += val;
            this._data[item[0]] = val;
            if (val > this._high) this._high = val;
            if (val < this._low) this._low = val;
        })

        this._mean = this._sum / data.length;
        ewars.emit("RENDER");
    };
}

class Plot {
    constructor(el) {
        this._el = el;

        let bounds = this._el.getBoundingClientRect();
        this.width = bounds.width;
        this.height = bounds.height;

        this._start_date = new Date().toISOString();
        this._end_date = new Date().toISOString();
        this._location = window.user.lid || window.user.clid;
        this._style = "SERIES";

        this._sources = [];

        this._svg = d3.select(this._el)
            .append("svg")
            .attr("fill", "red")
            .style("fill", "red")
            .attr("width", this.width - 8 - 16)
            .attr("height", this.height - 16 - 16)
            .attr("transform", `translate(${8},${16})`);

        ewars.subscribe("ADD_ROW", this._addRow);
        ewars.subscribe("REMOVE_ROW", this._removeRow);
        ewars.subscribe("ADD_COL", this._addCol);
        ewars.subscribe("REMOVE_COL", this._removeCol);
        ewars.subscribe("PL_ATTR", this._attr);
        ewars.subscribe("D_ATTR", this._dAttr);
        ewars.subscribe("ADD_ANAL", this._addAnal);
        ewars.subscribe("REMOVE_ANAL", this._removeAnal);
        ewars.subscribe("ADD_MARK", this._addMark);
        ewars.subscribe("REMOVE_MARK", this._removeMark);

    }

    destroy = () => {
        ewars.unsubscribe("ADD_ROW", this._addRow);
        ewars.unsubscribe("REMOVE_ROW", this._removeRow);
        ewars.unsubscribe("ADD_COL", this._addCol);
        ewars.unsubscribe("REMOVE_COL", this._removeCol);
        ewars.unsubscribe("PL_ATTR", this._attr);
        ewars.unsubscribe("D_ATTR", this._dAttr);
        ewars.unsubscribe("ADD_ANAL", this._addAnal);
        ewars.unsubscribe("REMOVE_ANAL", this._removeAnal);
        ewars.unsubscribe("ADD_MARK", this._addMark);
        ewars.unsubscribe("REMOVE_MARK", this._removeMark);
    };

    _addRow = (row) => {
        this._sources.push(new Dimension("ROW", row, this));

    };

    _addCol = (col) => {
        this._sources.push(new Dimension("COL", col, this));
    };

    _removeRow = (row) => {

    };

    _removeCol = (col) => {

    };

    _attr = (data) => {

    };

    _dAttr = (data) => {

    };

    getDefinition = () => {

    };

    _addAnal = () => {

    };

    _removeAnal = () => {

    };

    _addMark = () => {

    };

    _removeMark = () => {

    };

}

export default Plot;