const TYPES = {
    SUMMARY: [
        ["Constant Line", "CONSTANT_LINE"],
        ["Average Line", "AVG_LINE"],
        ["Median with Quartiles", "MED_QUARTILE"],
        ["Box Plot", "BOX_PLOT"]
    ],
    MODEL: [
        ["Average with 95% CI", "AVG_CI_95"],
        ["Trend line", "TREND"],
        ["Forecast", "FORECAST"],
        ["Cluster", "CLUSTER"]
    ],
    CUSTOM: [
        ["Reference line", "REF_LINE"],
        ["Reference Band", "REF_BAND"],
        ["Distribution band", "DIST_BAND"],
        ["Box plot", "BOX_PLOT"]
    ]
};

const STYLE_OVERRIDE = {
    position: "relative",
    display: "block",
    height: "auto"
};

const HEADER = {
    display: "block",
    fontWeight: "bold",
    margin: "0px 8px 0px 8px"
}

class Item extends React.Component {
    render() {
        return (
            <div className="block">
                <div className="block-content">{this.props.data[0]}</div>
            </div>
        )
    }
}

class Analysis extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel style={{paddingTop: "8px"}}>
                
                <h4 style={HEADER}>Summarize</h4>

                <div className="block-tree" style={STYLE_OVERRIDE}>
                    {TYPES.SUMMARY.map((item) => {
                        return <Item data={item}/>
                    })}
                </div>

                <h4 style={HEADER}>Model</h4>

                <div className="block-tree" style={STYLE_OVERRIDE}>
                    {TYPES.MODEL.map((item) => {
                        return <Item data={item}/>;
                    })}
                </div>

                <h4 style={HEADER}>Custom</h4>

                <div className="block-tree" style={STYLE_OVERRIDE}>
                    {TYPES.CUSTOM.map((item) => {
                        return <Item data={item}/>;
                    })}
                </div>

            </ewars.d.Panel>
        )
    }
}

export default Analysis;