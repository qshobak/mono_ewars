import DateField from "../../common/components/fields/DateField.react";
const SelectField = require("../../common/components/fields/SelectField.react");
const TextField = require("../../common/components/fields/TextInputField.react");
const SwitchField = require("../../common/components/fields/SwitchField.react");

const FieldTypes = {
    "date": DateField,
    "select": SelectField,
    "text": TextField,
    "switch": SwitchField
};

export default FieldTypes;