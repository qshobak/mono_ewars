const account_form = {
    t_g_header: {
        type: "header",
        label: "Base Settings"
    },
    name: {
        type: "text",
        label: "Name",
        required: true
    },
    status: {
        type: "button_group",
        label: "Status",
        options: [
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ]
    },
    domain: {
        type: "text",
        label: "Domain",
        required: true
    },
    default_language: {
        type: "select",
        label: "Default language",
        options: [
            ["en", "English (en)"],
            ["fr", "French (fr)"]
        ]
    },
    description: {
        type: "textarea",
        i18n: true,
        label: "Description"
    },
    iso2: {
        type: "text",
        label: "ISO2 Code"
    },
    iso3: {
        type: "text",
        label: "ISO3 Code"
    },
    account_flag: {
        type: "file",
        label: "Account Flag"
    },
    account_logo: {
        type: "file",
        label: "Account Logo"
    },
    t_header: {
        type: "header",
        label: "Default Account Administrator"
    },
    admin_email: {
        type: "text",
        label: "Account Administrator Email"
    },
    admin_password: {
        type: "text",
        label: "Account Administrator Password"
    }

};

export default account_form;