import account_form from "../../constants/account_form";
import DataTable from "../../../common/DataTable/DataTable.react";

const COLUMNS = [
    {
        name: "name",
        config: {
            label: "Account Name"
        }
    },
    {
        name: "domain",
        config: {
            label: "Account Domain"
        }
    },
    {
        name: "status",
        config: {
            label: "Status"
        }
    },
    {
        name: "created",
        config: {
            label: "Created"
        }
    },
    {
        name: "last_modified",
        config: {
            label: "Last Modified"
        }
    },
    {
        name: "uuid",
        config: {
            label: "UUID"
        }
    },
    {
        name: "tki",
        config: {
            label: "TKI"
        }
    }
];


const ACTIONS = [
    {label: "Delete", action: "DELETE", icon: "fa-trash"}
];

const account_defaults = {
    name: "New Account",
    domain: "",
    status: "ACTIVE",
    admin_email: "",
    admin_password: "",
    admin_password_confirm: "",
    default_language: "en",
    description: "",
    iso2: "DEFAULT",
    iso3: "DEFAULT",

};

const EDITOR_ACTION = [
    {label: "Save", icon: "fa-save", action: "SAVE"},
    {label: "Cancel", icon: "fa-close", action: "CLOSE"}
];

class AccountManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            account: null,
            shade: false
        }
    }

    _action = (action) => {
        if (action == "CREATE_ACCOUNT") {
            this.setState({
                shade: true,
                account: ewars.copy(account_defaults)
            })
        }

        if (action == "CLOSE") {
            this.setState({
                shade: false,
                account: null
            })
        }

        if (action === "SAVE") {
            this._saveAccount();
        }

    };

    _saveAccount = () => {
        let bl = new ewars.Blocker(null, "Creating account");
        ewars.tx("com.ewars.admin.account.create", [this.state.account])
            .then((resp) => {
                bl.destroy();
                ewars.emit("DT_RELOAD");
                ewars.emit("RELOAD_DT");
            })
    };

    _onAction = (action, cell, row) => {
        if (action == "EDIT") {
            this.setState({
                account: row,
                shade: true
            })
        }

        if (action === "DELETE") {
            this._deleteAccount(row);
        }
    };

    _deleteAccount = (account) => {
        ewars.prompt("fa-exclamation-triangle", "Delete Account?", "This action is irreversible, all data for the account will be lost.", function () {
            let bl = new ewars.Blocker(null, "Deleting account...");
            ewars.tx('com.ewars.admin.account.delete', [account.id])
                .then((resp) => {
                    bl.destroy();
                    ewars.emit("DT_RELOAD");
                    ewars.emit("RELOAD_DT");
                })
        })
    };

    _updateProp = (data, prop, value) => {
        this.setState({
            ...this.state,
            account: {
                ...this.state.account,
                [prop]: value
            }
        })
    };

    render() {
        let readOnly = false;
        if (this.state.account) {
            if (this.state.account.id != null) {
                readOnly = true;
            }
        }

        let actions = EDITOR_ACTION;
        if (readOnly) {
            actions = [
                {label: "Close", icon: "fa-close", action: "CLOSE"}
            ]
        }
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Accounts">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={() => {
                                this._onAction("EDIT", null, {})
                            }}/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            id="accounts"
                            resource="account"
                            onCellAction={this._onAction}
                            actions={ACTIONS}
                            columns={COLUMNS}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Account"
                    onAction={this._action}
                    actions={actions}
                    shown={this.state.shade}>
                    <ewars.d.Panel>
                        <div style={{padding: 8}}>
                            <ewars.d.Form
                                readOnly={readOnly}
                                updateAction={this._updateProp}
                                definition={account_form}
                                data={this.state.account}/>
                        </div>
                    </ewars.d.Panel>

                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default AccountManager;