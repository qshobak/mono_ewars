import FORM_ISSUE from "../../forms/issue";
import COLS_ISSUES from "../../tables/issues";
import DataTable from "../../../common/DataTable/DataTable.react";

const ACTIONS = [
    {icon: "fa-pencil", action: "EDIT"}
]


class IssueManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            issue: null
        }
    }

    _onChange = (prop, value) => {
        this.setState({
            issue: {
                [prop]: value
            }
        })
    };

    _onAction = (action, cell, row) => {
        if (action == "EDIT") {
            this.setState({
                shown: true,
                issue: row
            })
        }

        if (action == "CLOSE") {
            this.setState({
                shown: false,
                issue: null
            })
        }
    };

    _new = () => {
        this.setState({
            issue: {
                priority: 0,
                assigned_to: window.user.id
            },
            shown: true
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Issues">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={this._new}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            paused={this.state.shown}
                            id="ISSUES"
                            actions={ACTIONS}
                            onCellAction={this._onAction}
                            resource="v_issue"
                            columns={COLS_ISSUES}/>

                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.shown ?
                    <ewars.d.Shade
                        onAction={this._onAction}
                        shown={this.state.shown}>
                        <ewars.d.SystemForm
                            definition={FORM_ISSUE}
                            data={this.state.issue}
                            enabled={true}
                            onChange={this._onChange}/>
                    </ewars.d.Shade>
                : null}
            </ewars.d.Layout>
        )
    }
}

export default IssueManager;