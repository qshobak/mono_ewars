import RTCProxy from "../../lib/RTCProxy";

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: "",
            messages: []
        }

        this._rtc = new RTCProxy();
        this._rtc.onMessage((data) => {
            let messages = this.state.messages;
            messages.push(data);
            this.setState({
                messages
            })
        })

    }


    _onMessageChange = (e) => {
        this.setState({
            message: e.target.value
        })
    };

    _call = () => {
        this._rtc.connect();
    };

    _receive = () => {
        this._rtc.receive();
    };

    _disconnect = () => {
        this._rtc.disconnect();
    };

    _send = () => {
        this._rtc.send({n: window.user.name, c: this.state.message});
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Chat">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderBottom={true}>

                        <ewars.d.Panel>
                            <div>
                                <div className="block-tree" style={{position: "relative"}}>
                                    {this.state.messages.map((item) => {
                                        return (
                                            <div className="block">
                                                <div className="block-content">
                                                    <ewars.d.Row>
                                                        <ewars.d.Cell>{item.n}</ewars.d.Cell>
                                                        <ewars.d.Cell>{item.c}</ewars.d.Cell>
                                                    </ewars.d.Row>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row height="25px">
                    <ewars.d.Cell borderTop={true}>
                        <input type="text" value={this.state.message} onChange={this._onMessageChange}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true} style={{display: "block"}}>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this._send}
                                label="Send Message"/>
                            <ewars.d.Button
                                onClick={this._call}
                                label="Connect"/>
                            <ewars.d.Button
                                onClick={this._receive}
                                label="Receive"/>
                            <ewars.d.Button
                                onClick={this._startDisconnect}
                                label="Disconnect"/>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Chat;