import AccountManager from "./accounts/AccountManager";
import ProjectManager from "./projects/ProjectManager";
import TranslationManager from "./translations/TranslationManager";
import BudgetManager from "./budget/BudgetManager";
import DocManager from "./documentation/DocManager";
import ProposalManager from "./proposals/ProposalManager";
import IssueManager from "./issues/IssueManager";
import OrganizationManager from "./organizations/OrganizationManager";
import ReleaseManager from "./releases/ReleaseManager";

const STYLES = {
    BUTTON: {
        display: "block",
        textAlign: "center",
        color: "#F2F2F2",
        height: "40px",
        lineHeight: "40px",
        fontSize: "16px",
        cursor: "pointer"
    }
}


class Section extends React.Component {
    _onClick = () => {
        this.props.onClick(this.props.view, this.props.Cmp);
    };

    render() {
        return (
            <div style={STYLES.BUTTON} onClick={this._onClick}>
                <i className={"fal " + this.props.i}/>
            </div>
        )
    }
}

const SECTIONS = [
    ["fa-globe", "DASHBOARD", null],
    ["fa-building", "ACCOUNTS", AccountManager],
    ["fa-briefcase", "PROJECTS", ProjectManager],
    ["fa-file-code", "RELEASES", ReleaseManager],
    ["fa-exclamation-triangle", "ISSUES", IssueManager],
    ["fa-file", "PROPOSALS", ProposalManager],
    ["fa-money-bill", "BUDGET", BudgetManager],
    ["fa-language", "TRANSLATIONS", TranslationManager],
    ["fa-university", "DOCUMENTATION", DocManager],
    ["fa-sitemap", "ORGANIZATIONS", OrganizationManager],
    ["fa-newspaper", "ARTICLES", null],
    ["fa-exclamation", "NOTICES", null],
    ["fa-code", "Code", null],
    ["fa-info", "LOGS", null]
];


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DASHBOARD",
            Cmp: null
        }
    }

    _onTabChange = (view, Cmp) => {
        this.setState({
            view,
            Cmp
        })
    };


    render() {

        let view;
        if (this.state.Cmp) view = <this.state.Cmp/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Layout>
                            <ewars.d.Row>
                                <ewars.d.Cell width="50px" borderRight={true}>
                                    <ewars.d.Panel>
                                        {SECTIONS.map((item) => {
                                            return (
                                                <Section
                                                    i={item[0]}
                                                    view={item[1]}
                                                    Cmp={item[2]}
                                                    onClick={this._onTabChange}
                                                    active={this.state.view == item[1]}/>
                                            )
                                        })}
                                    </ewars.d.Panel>
                                </ewars.d.Cell>
                                <ewars.d.Cell borderRight={true}>
                                    {view}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                </ewars.d.Row>

            </ewars.d.Layout>
        )
    }
}

export default Component;