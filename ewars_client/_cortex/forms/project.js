const FORM_PROJECT = [
    {
        _o: 0,
        n: "name",
        l: "Name",
        t: "TEXT"
    },
    {
        _o: 1,
        n: "status",
        l: "Status",
        t: "SELECT",
        o: [
            ["DRAFT", "Draft"],
            ["ACTIVE", "Active"],
            ["ARCHIVED", "Archived"],
            ["ABANDONED", "Abandoned"]
        ]
    },
    {
        _o: 1.1,
        n: "description",
        t: "TEXT",
        l: "Description",
        ml: true
    },
    {
        _o: 2,
        n: "repo",
        l: "Code Repository",
        t: "TEXT"
    },
    {
        _o: 3,
        n: "tags",
        l: "Tags",
        h: "Tags available for issues",
        t: "OPTIONSET"
    }
]

export default FORM_PROJECT;
