const FORM_ISSUE = [
    {
        _o: 0,
        t: "TEXT",
        l: "Title",
        n: "title",
        r: 1
    },
    {
        _o: 1,
        n: "project_id",
        t: "SELECT",
        l: "Project",
        os: [
            'c_project',
            'uuid',
            'name',
            {status: {eq: "ACTIVE"}}
        ]
    },
    {
        _o: 2,
        t: "SELECT",
        n: "parent_id",
        l: "Parent Issue",
        os: [
            "c_issue",
            'uuid',
            'title',
            {status: {eq: "OPEN"}}
        ]
    },
    {
        _o: 2.1,
        t: "SELECT",
        n: "issue_type",
        l: "Issue Type",
        multi: true,
        o: [
            ["BUG", "Bug"],
            ["FEATURE", "Feature Request"],
            ["TASK", "Task"],
            ["SUPPORT", "Support"],
            ["QUESTION", "Question"]
        ]
    },
    {
        _o: 3,
        n: "description",
        l: "Description",
        t: "TEXT",
        ml: true
    },
    {
        _o: 4,
        n: "tags",
        l: "Tags",
        t: "TEXT"
    },
    {
        _o: 5,
        n: "priority",
        l: "Priority",
        t: "SELECT",
        o: [
            ["0", "Low"],
            ["1", "Moderate"],
            ["2", "High"],
            ["3", "Severe"],
            ["999", "HOLY %$#@"]
        ]
    },
    {
        _o: 6,
        n: "assigned_to",
        t: "SELECT",
        l: "Assigned To",
        os: [
            "v_user",
            "id",
            "name",
            {role: {eq: "SUPER_ADMIN"}}
        ]
    },
    {
        _o: 7,
        n: "screen",
        t: "FILE",
        l: "Attachment"
    }
]

export default FORM_ISSUE;