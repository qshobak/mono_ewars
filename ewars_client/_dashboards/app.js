import { LitElement, html } from "lit-element";

import VizEditor from "../common/editors/viz/viz.editor";
import Toolbar from "../lib/c.toolbar";

class Dashboards extends LitElement {
    render() {
        return html`
            <div class="adm-app">
                <div class="ide-tbar">

                </div>
                <div class="ide-row">

                </div>
            </div>
        `;
    }
}
window.customElements.define("dashboards", Dashboards);

class Application {
    constructor(el, props) {
        this.el = el;
        this.props = props

        this.dashboards = [];
        this.filters = {};
        this.orders = {};
        this.limit = 100;
        this.offset = 0;

        this.render();
    }

    _loadDashboards = () => {

    };

    _action = (action, data) => {

    };

    render() {

        let application = document.createElement("dashboards");

        this.el.appendChild(dashboards);
    }
}


let app = new Application(document.getElementById("application"), {});
