const Button = require("../../common/components/ButtonComponent.react");
import ContextMenu from "../../common/ContextMenu.react";
import RangeUtils from "../../common/utils/RangeUtils";

const TextField = require("../../common/components/fields/TextInputField.react");
import SelectField from "../../common/components/fields/SelectField.react";
const TextAreaField = require("../../common/components/fields/TextAreaField.react");

const DEFAULTS_SERIES = require("../../common/components/widget_editors/series/defaults_widget");
const DEFAULTS_CATEGORY = require("../../common/components/widget_editors/category/defaults_widget");

const WidgetEditor = require("../../common/components/widget_editors/WidgetEditor.react");
import Tab from "../../common/components/ide/Tab.react";
import Shade from "../../common/cmp/Shade";

import * as definitions from "../definitions";

import form from "../constants/form";
const Form = require("../../common/components/Form.react");

const CONTEXT_ACTIONS = [
    {icon: "fa-cog", label: "Settings", action: "EDIT"},
    {icon: "fa-caret-up", label: "Move Row Up", action: "MOVE_UP"},
    {icon: "fa-caret-down", label: "Move Row Down", action: "MOVE_DOWN"},
    {icon: "fa-caret-left", label: "Move Cell Left", action: "MOVE_LEFT"},
    {icon: "fa-caret-right", label: "Move Cell Right", action: "MOVE_RIGHT"},
    {icon: "fa-copy", label: "Duplicate Row", action: "DUPE_ROW"},
    {icon: "fa-copy", label: "Duplicate Cell", action: "DUPE_CELL"},
    {icon: "fa-trash", label: "Remove Cell", action: "REMOVE_CELL"},
    {icon: "fa-trash", label: "Remove Row", action: "REMOVE_ROW"}
];

const WIDGET_DEFAULT = {
    SERIES: DEFAULTS_SERIES,
    CATEGORY: DEFAULTS_CATEGORY
};

const ACCESS = {
    options: [
        ["ACCOUNT_ADMIN", "Account Administrator"],
        ["REGIONAL_ADMIN", "Geographic Administrator"],
        ["USER", "Reporting User"]
    ],
    multiple: true
};

const STATUS = {
    options: [
        ["ACTIVE", "Active"],
        ["INACTIVE", "Inactive"]
    ]
};

const TYPES = [
    {n: "ROW", d: true, l: "Row"},
    {n: "CELL", d: true, l: "Cell"},
    {
        l: "Chart Components",
        c: [
            {n: "SERIES", d: true, l: "Series Chart"},
            {n: "CATEGORY", d: true, l: "Category Widget"}
        ]
    },
    {
        l: "Mapping",
        c: [
            {n: "MAP", d: true, l: "Map"}
        ]
    },
    {
        l: "Content",
        c: [
            {n: "TEXT", d: true, l: "Text"},
            {n: "IMAGE", d: true, l: "Image"}
        ]
    },
    {
        l: "Other",
        c: [
            {n: "RAW", d: true, l: "Raw Widget"},
            {n: "GAUGE", d: true, l: "Gauge"},
            {n: "INFO", d: true, l: "Info"}
        ]
    },
    {
        l: "User",
        c: [
            {n: "ALERTS", d: true, l: "Alerts"},
            {n: "ACTIVITY", d: true, l: "Activity Feed"},
            {n: "ASSIGNMENTS", d: true, l: "Assignments"},
            {n: "SUMMARY_STATS", d: true, l: "Summary Statistics"},
            {n: "OVERDUE", d: true, l: "Overdue Reports"},
            {n: "AMENDMENTS", d: true, l: "Amendment"},
            {n: "DELETIONS", d: true, l: "Deletions"},
            {n: "DOCUMENTS", d: true, l: "Documents"},
            {n: "RECENT", d: true, l: "Recent Submissions"},
            {n: "METRICS", d: true, l: "Metrics"},
            {n: "RECENT", d: true, l: "Recent Form Submissions"}
        ]
    }
];

const STYLES = {
    ROW_DROP: {
        display: 'none',
        border: "1px dashed #F2F2F2",
        paddingTop: 15,
        paddingBottom: 15,
        textAlign: "center"
    },
    ROW_GROW: {
        display: "block",
        height: 3,
        background: "#000000",
        cursor: "pointer"
    },
    CELL_DROP: {
        maxWidth: 30,
        border: "1px solid #F2F2F2",
        display: "none",
        height: "100%"
    }
};

const WIDGET_NAMES = {
    SERIES: "Time Series",
    CATEGORY: "Category Chart",
    PIE: "Category Chart",
    MAP: "Map",
    ACTIVITY: "Activity Feed",
    ASSIGNMENTS: "Assignments",
    METRICS: "Metrics",
    OVERDUE: "Overdue Submissions",
    DRAFTS: "Draft Submissions",
    DOCUMENTS: "Documents",
    RECENT: "Recent Submissions",
    ALERTS: "Alerts",
    RAW: "Raw",
    TEXT: "Text",
    IMAGE: "Image",
    GAUGE: "Gauge",
    INFO: "Info Bar"
};

class Node extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        if (!this.props.data.c) return;
        this.setState({
            show: !this.state.show
        })
    };

    _onDragStart = (e) => {
        e.dataTransfer.setData("d", this.props.data.n);
        if (this.props.data.n == "ROW") {
            ewars.emit("SHOW_ROW_DROP");
        } else if (this.props.data.n == "CELL") {
            ewars.emit("SHOW_CELL_DROP");
        } else {
            ewars.emit("SHOW_WIDGET_DROP");
        }
    };

    render() {
        let iconClass = "fa fa-caret-right";
        if (this.state.show) iconClass = "fa fa-caret-down";

        let children;
        if (this.props.data.c && this.state.show) {
            children = this.props.data.c.map(function (child) {
                return <Node data={child}/>
            }.bind(this))
        }

        let onDragStart = this.props.data.d ? this._onDragStart : null;

        return (
            <div className="block" onDragStart={onDragStart} draggable={this.props.data.d}>
                <div className="block-content" onClick={this._toggle}>
                    <div className="ide-row">
                        {this.props.data.c ?
                            <div className="ide-col" style={{maxWidth: 13}}>
                                <i className={iconClass}></i>
                            </div>
                            : null}
                        <div className="ide-col">{this.props.data.l}</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="block-children">
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
}

class Cell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            context: false,
            widget: null
        }
    }

    componentWillMount() {
        window.__hack__.addEventListener("click", this._setter);

    }

    componentDidMount() {
        ewars.subscribe("SHOW_WIDGET_DROP", () => {
            this._event("SHOW_WIDGET_DROP");
        });
        ewars.subscribe("HIDE_WIDGET_DROP", () => {
            this._event("HIDE_WIDGET_DROP");
        })
    }

    _event = (event) => {
        if (!this.state.widget) {
            if (event == "SHOW_WIDGET_DROP") {
                if (this.refs.widgetDrop) this.refs.widgetDrop.style.display = "block";
            }
        }
        if (event == "HIDE_WIDGET_DROP") {
            if (this.refs.widgetDrop) this.refs.widgetDrop.style.display = "none";
        }
    };

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._setter);
    }

    _setter = (e) => {
        if (this.state.context) {
            if (!this.refs.menu.contains(e.target)) {
                this.setState({
                    context: false
                })
            }
        }
    };

    _onDrop = (e) => {
        let data = e.dataTransfer.getData("d");

        ewars.emit("HIDE_WIDGET_DROP");

        if (['ROW', "CELL"].indexOf(data) < 0) {
            // It's oke to drop here
            let widget = ewars.copy(WIDGET_DEFAULT[data] || {type: data});
            this.props.onChange(this.props.index, widget);
        }
    };

    _context = (e) => {
        e.preventDefault();
        this.setState({context: true})
    };

    _onAction = (action) => {
        this.setState({
            context: false
        });

        switch (action) {
            case "EDIT":
                ewars.emit("EDIT_WIDGET", [this.props.rowIndex, this.props.index]);
                break;
            case "REMOVE_CELL":
                ewars.emit("REMOVE_CELL", [this.props.rowIndex, this.props.index]);
                break;
            case "REMOVE_ROW":
                ewars.emit("REMOVE_ROW", [this.props.rowIndex]);
                break;
            case "MOVE_LEFT":
                ewars.emit("MOVE_LEFT", [this.props.rowIndex, this.props.index]);
                break;
            case "MOVE_RIGHT":
                ewars.emit("MOVE_RIGHT", [this.props.rowIndex, this.props.index]);
                break;
            case "MOVE_UP":
                ewars.emit("MOVE_UP", [this.props.rowIndex]);
                break;
            case "MOVE_DOWN":
                ewars.emit("MOVE_DOWN", [this.props.rowIndex]);
                break;
            case "DUPE_ROW":
                ewars.emit("DUPE_ROW", [this.props.rowIndex]);
                break;
            case "DUPE_CELL":
                ewars.emit("DUPE_CELL", [this.props.rowIndex, this.props.index]);
                break;
            default:
                break;
        }

    };

    _edit = () => {
        if (!this.props.data.type) return;
        ewars.emit("EDIT_WIDGET", [this.props.rowIndex, this.props.index]);
    };

    render() {
        let label = "Empty";
        if (this.props.data.type) label = WIDGET_NAMES[this.props.data.type];
        return (
            <div className="ide-col" onClick={this._edit} onContextMenu={this._context}>
                <div className="layout-editor-cell" onDrop={this._onDrop} onDragOver={(e) => {
                    e.preventDefault()
                }}>
                    <div className="layout-editor-widget">
                        <div className="name">{label}</div>
                    </div>

                    <div className="widget-drop" ref="widgetDrop" style={STYLES.ROW_DROP} onDrop={this._onDrop}></div>
                    {this.state.context ?
                        <div style={{position: "absolute", top: 10, left: 10}} ref="menu">
                            <ContextMenu
                                onClick={this._onAction}
                                actions={CONTEXT_ACTIONS}
                                visible={this.state.context}/>
                        </div>
                        : null}
                </div>
            </div>
        )
    }
}

class LayoutRow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cells: [{}]
        }
    }

    componentDidMount() {
        ewars.subscribe("SHOW_CELL_DROP", () => {
            this._event("SHOW_CELL_DROP");
        });
        ewars.subscribe("HIDE_CELL_DROP", () => {
            this._event("HIDE_CELL_DROP");
        })
    }

    _event = (event) => {
        if (event == "SHOW_CELL_DROP") {
            if (this.refs.cellDrop) this.refs.cellDrop.style.display = "flex";
        }
        if (event == "HIDE_CELL_DROP") {
            if (this.refs.cellDrop) this.refs.cellDrop.style.display = "none";
        }
    };

    _onDrop = (e) => {
        e.preventDefault();
        ewars.emit("HIDE_ROW_DROP");
        ewars.emit("HIDE_CELL_DROP");

        let data = e.dataTransfer.getData("d");

        if (data == "CELL") {
            let cells = this.props.data;
            cells.push({});
            this.props.onChange(this.props.index, cells);
        }
    };

    _onCellChange = (cellIndex, data) => {
        let cells = ewars.copy(this.props.data);
        cells[cellIndex] = data;
        this.props.onChange(this.props.index, cells);
    };

    render() {
        return (
            <div className="row" onDragOver={(e) => {
                e.preventDefault()
            }} onDrop={this._onDrop}>
                <div className="col-12">
                    <div className="ide-row">
                        {this.props.data.map(function (item, index) {
                            return <Cell
                                data={item}
                                index={index}
                                onChange={this._onCellChange}
                                rowIndex={this.props.index}
                                onAction={(action, cellIndex, data) => this.props.onAction(action, this.props.index, cellIndex, data)}/>
                        }.bind(this))}
                        <div className="cell-drop ide-col" ref="cellDrop" style={STYLES.CELL_DROP}></div>
                    </div>
                </div>
                <div className="row-grow" style={STYLES.ROW_GROW}></div>
            </div>
        )
    }

}

class Editor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editor: false,
            view: "DEFAULT",
            layout: ewars.copy(props.data)
        }

    }

    componentWillReceiveProps(nextProps) {
        this.state.layout = ewars.copy(nextProps.data);
    }

    componentWillMount() {
        ewars.subscribe("SHOW_ROW_DROP", () => {
            this._event("SHOW_ROW_DROP")
        }, "SHOW_ROW_DROP");
        ewars.subscribe("HIDE_ROW_DROP", () => {
            this._event("HIDE_ROW_DROP")
        }, "SHOW_ROW_DROP");
        ewars.subscribe("EDIT_WIDGET", this._editWidget, "EDIT_WIDGET");
        ewars.subscribe("REMOVE_CELL", this._deleteCell, "REMOVE_CELL");
        ewars.subscribe("REMOVE_ROW", this._deleteRow, "REMOVE_ROW");
        ewars.subscribe("MOVE_LEFT", this._moveCellLeft, "MOVE_LEFT");
        ewars.subscribe("MOVE_RIGHT", this._moveCellRight, "MOVE_RIGHT");
        ewars.subscribe("MOVE_UP", this._moveRowUp, "MOVE_UP");
        ewars.subscribe("MOVE_DOWN", this._moveRowDown, "MOVE_DOWN");
        ewars.subscribe("DUPE_ROW", this._duplicateRow, "DUPE_ROW");
        ewars.subscribe("DUPE_CELL", this._duplicateCell, "DUPE_CELL");
    }

    componentWillUnmount() {
        let items = ["SHOW_ROW_DROP", "EDIT_WIDGET", "REMOVE_CELL", "REMOVE_ROW", "MOVE_LEFT", "MOVE_RIGHT", "MOVE_UP", "MOVE_DOWN", "DUPE_ROW", "DUPE_CELL"];

        items.forEach(item => {
            ewars.unsubscribe(item, item);
        })
    }

    _editWidget = (data) => {
        let widget = this.state.layout.definition[data[0]][data[1]];
        if (!widget.period) {
            widget.period = RangeUtils.convertLegacy(widget);
            delete widget.start_date_spec;
            delete widget.end_date_spec;
            delete widget.start_intervals;
            delete widget.end_intervals;
            delete widget.start_date;
            delete widget.end_date;
        }

        this.setState({
            editor: true,
            widget: widget,
            editPath: data
        })
    };

    /***
     * Remove a row from the definition
     * @param row
     * @private
     */
    _deleteRow = (args) => {
        let row = parseInt(args[0]);
        let rows = ewars.copy(this.state.layout.definition);
        rows.splice(row, 1);
        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })
    };

    _moveRowUp = (args) => {
        let row = parseInt(args);
        if (row == 0) return;

        let def = this.state.layout.definition;
        let tmp = def[row - 1];
        def[row - 1] = def[row];
        def[row] = tmp;

        this.setState({
            layout: {
                ...this.state.layout,
                definition: def
            }
        })
    };

    _moveRowDown = (args) => {
        let row = parseInt(args[0]);

        if (row >= this.state.layout.definition.length) return;

        let def = ewars.copy(this.state.layout.definition);
        let tmp = def[row + 1];
        def[row + 1] = def[row];
        def[row] = tmp;

        this.setState({
            layout: {
                ...this.state.layout,
                definition: def
            }
        })

    };

    _moveCellLeft = (args) => {
        let rows = ewars.copy(this.state.layout.definition);

        let row = parseInt(args[0]);
        let cell = parseInt(args[1]);

        if (cell == 0) return;

        let cells = rows[row];
        let tmp = cells[cell - 1];
        cells[cell - 1] = cells[cell];
        cells[cell] = tmp;

        rows[row] = cells;

        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })

    };

    _moveCellRight = (args) => {
        let row = args[0];
        let cell = args[1];
        let rows = this.state.layout.definition;

        if (cell > rows[row].length) return;

        let cells = rows[row];
        let tmp = cells[cell + 1];
        cells[cell + 1] = cells[cell];
        cells[cell] = tmp;

        rows[row] = cells;

        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })

    };

    _duplicateRow = (args) => {
        let rows = ewars.copy(this.state.layout.definition);
        let row = parseInt(args[0]);

        let rowCopy = ewars.copy(rows[row]);
        rows.push(rowCopy);

        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })
    };

    _duplicateCell = (args) => {
        let row = args[0];
        let cell = args[1];

        let rows = ewars.copy(this.state.layout.definition);

        let tmp = ewars.copy(rows[row][cell]);
        rows[row].push(tmp);

        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })
    };

    /**
     * Delete a cell from a row
     * @param row
     * @param index
     * @private
     */
    _deleteCell = (args) => {
        let row = args[0];
        let cell = args[1];

        let definition = ewars.copy(this.state.layout.definition);

        if (cell == 0 && definition[row].length == 1) {
            definition.splice(row, 1);
        } else {
            definition[row].splice(cell, 1);
        }

        this.setState({
            layout: {
                ...this.state.layout,
                definition: definition
            }
        })
    };

    _event = (event) => {
        if (event == "SHOW_ROW_DROP") {
            if (this.refs.rowDrop) this.refs.rowDrop.style.display = "block";
        }
        if (event == "HIDE_ROW_DROP") {
            if (this.refs.rowDrop) this.refs.rowDrop.style.display = "none";
        }
    };

    _onDrop = (e) => {
        e.preventDefault();
        this.refs.rowDrop.style.display = "none";
        var data = e.dataTransfer.getData("d");

        if (data == "ROW") {
            let layout = this.state.layout.definition;
            layout.push([{}]);
            this.setState({
                layout: {
                    ...this.state.layout,
                    definition: layout
                }
            })
        }
    };

    _onDragStart = (e) => {
        e.dataTransfer.setData("d", "Hello");
    };

    _onAction = (action, rowIndex, cellIndex, data) => {
        switch (action) {
            case "REMOVE_ROW":
                this.state.layout.splice(rowIndex, 1);
                let rows = ewars.copy(this.state.layout.definition);
                rows.splice(rowIndex, 1);
                this.setState({
                    layout: {
                        ...this.state.layout,
                        definition: rows
                    }
                });
                break;
            default:
                break;
        }
    };

    _cancelEdit = () => {
        this.setState({
            widget: null,
            editor: false
        })
    };

    _onRowChange = (rowIndex, data) => {
        let rows = ewars.copy(this.state.layout.definition);
        rows[rowIndex] = data;
        this.setState({
            layout: {
                ...this.state.layout,
                definition: rows
            }
        })
    };

    _save = () => {
        let com = "com.ewars.dashboard.create",
            args = [];
        if (this.state.layout.uuid) {
            com = "com.ewars.dashboard.update";
            args.push(this.state.layout.uuid);
        }
        args.push(this.state.layout);

        let blocker = new ewars.Blocker(null, "Saving change(s)");
        ewars.tx(com, args)
            .then((resp) => {
                blocker.destroy();
                this.setState({
                    layout: resp
                });
                ewars.growl("Updates saved.");
            })
    };

    _onWidgetSave = (unused1, unused2, widgetData) => {

        let data = this.state.layout.definition;
        data[this.state.editPath[0]][this.state.editPath[1]] = widgetData;
        this.setState({
            widget: null,
            editor: false,
            editPath: null,
            layout: {
                ...this.state.layout,
                definition: data
            }
        })
    };

    _onChange = (data, prop, value) => {
        this.setState({
            layout: {
                ...this.state.layout,
                [prop]: value
            }
        })
    };

    _onShadeAction = () => {
        this.setState({
            widget: null,
            editor: false
        })
    };

    render() {
        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">Editor</div>


                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    onClick={this._save}
                                    icon="fa-save"
                                    label="Save Change(s)"/>
                                <ewars.d.Button
                                    icon="fa-close"
                                    onClick={this.props.onClose}
                                    label="Cancel"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col border-right" style={{maxWidth: 200}}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">

                                {TYPES.map(function (item) {
                                    return <Node data={item}/>
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll" onDrop={this._onDrop}
                             onDragOver={(e) => {
                                 e.preventDefault()
                             }}>
                            <div className="layout-editor-container">

                                <div className="grid">

                                    {this.state.layout.definition.map(function (item, index) {
                                        return <LayoutRow data={item}
                                                          index={index}
                                                          onChange={this._onRowChange}
                                                          key={"ROW_" + index}
                                                          onAction={this._onAction}/>;
                                    }.bind(this))}

                                    <div ref="rowDrop" className="row-drop" style={STYLES.ROW_DROP}></div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="ide-col border-left" style={{maxWidth: 300}}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="ide-settings-content">
                                <div className="ide-section-basic" style={{padding: 8}}>
                                    <Form
                                        definition={form}
                                        data={this.state.layout}
                                        readOnly={false}
                                        updateAction={this._onChange}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.editor ?
                    <Shade
                        toolbar={false}
                        onAction={this._onShadeAction}
                        shown={this.state.editor}>
                        <WidgetEditor
                            definition={definitions[this.state.widget.type] || null}
                            onSave={this._onWidgetSave}
                            onCancel={this._cancelEdit}
                            visible={this.state.editor}
                            mode="DASHBOARD"
                            data={this.state.widget}/>
                    </Shade>
                    : null}
            </div>
        )
    }
}

export default Editor;