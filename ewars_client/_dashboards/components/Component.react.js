import LayoutEditor from "../../_test/components/LayoutEditor";
import FORM_DASHBOARD from "../../common/forms/form.layout.dashboard";

import Manager from "./Manager.react";

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: null,
            view: "DEFAULT"
        }
    }

    _onSelect = (node) => {
        if (!node.definition) node.definition = [];
        this.setState({
            editing: node
        })
    };

    _onChange = (prop, value) => {
        this.setState({
            editing: {
                ...this.state.editing,
                [prop]: value
            }
        })
    };

    _onCloseEdit = () => {
        this.setState({
            editing: null
        })
    };

    _create = () => {
        this.setState({
            editing: {
                name: "",
                description: {en: ""},
                definition: []
            }
        })
    };

    _onSave = () => {

    };

    _onClose = () => {

    };

    render() {

        let view = <Manager onSelect={this._onSelect} createNew={this._create}/>;

        if (this.state.editing) {

            // TODO Need to detect if this is an older style definition
            // original dashboards were made up of rows, need to reformat to new syntax
            let rejig = false;
            let newData = [];

            if (this.state.editing.definition.length > 0) {
                if (this.state.editing.definition[0].t == undefined) {
                    rejig = true;
                }
            }
            if (rejig) {
                this.state.editing.definition.forEach((row, index) => {
                    let hBox = {
                        t: "H",
                        _: index,
                        h: 200,
                        i: []
                    }

                    row.forEach((subItem, index) => {
                        hBox.i.push({
                            t: "W",
                            c: subItem,
                            w: 100 / row.length,
                            _: `${hBox._}.${index}`
                        })
                    })

                    newData.push(hBox);
                })
            } else {
                newData = this.state.editing.definition;
            }


            view = (
                <LayoutEditor
                    onChange={this._onChange}
                    onSave={this._onSave}
                    settings={FORM_DASHBOARD}
                    data={this.state.editing}
                    onClose={this._onClose}
                    definition={newData}
                    mode="DASHBOARD"/>
            )
        }

        return (
            <div className="ide">
                {view}
            </div>
        )
    }
}

export default Component;

