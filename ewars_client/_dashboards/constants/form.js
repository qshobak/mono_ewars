const form = {
    name: {
        type: "text",
        required: true,
        label: "Layout Name"
    },
    status: {
        type: "select",
        label: "Status",
        required: true,
        options: [
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ]
    },
    color: {
        type: "text",
        label: "Color",
        conditions: {
            application: "all",
            rules: [
                ["layout_type", "eq", "DASHBOARD"]
            ]
        }
    },
    description: {
        type: "textarea",
        label: "Description",
        i18n: true
    }

};

export default form;