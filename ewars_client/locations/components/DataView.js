import DataTable from "../../common/datatable/c.datatable";

const ACTIONS = [
    ["fa-pencil", "EDIT", "Edit location"],
    ["fa-trash", "DELETE", "Delete location"]
]

const COLUMNS = [
    {
        root: true,
        name: "uuid",
        config: {
            label: "UUID",
            type: "text"
        }
    },
    {
        root: true,
        name: "name",
        config: {
            label: "Name",
            type: "text"
        },
        fmt: ewars.I18N
    },
    {
        root: true,
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", "Active"],
                ["DISABLED", "Inactive"]
            ]
        }
    },
    {
        root: true,
        name: "groups",
        config: {
            label: "Location Group(s)",
            type: "text"
        },
        fmt: (val) => {
            if (val) return val.join(", ")
            return "";
        }
    },
    {
        root: true,
        name: "location.name",
        config: {
            label: "Parent",
            type: "location",
            filterKey: "parent_id"
        },
        fmt: ewars.I18N
    },
    {
        root: true,
        name: "pcode",
        config: {
            label: "pcode",
            type: "text"
        }
    },
    {
        root: true,
        name: "location_type.name",
        config: {
            type: "select",
            label: "Location Type",
            filterKey: "site_type_id",
            optionsSource: {
                resource: "location_type",
                valSource: "id",
                labelSource: "name",
                query: {}
            }
        }
    },
    {
        root: true,
        name: "geometry_type",
        config: {
            label: "Geometry Type",
            type: "select",
            options: [
                ["POINT", "Point"],
                ["ADMIN", "MultiPolygon"]
            ]
        }
    },
    {
        root: true,
        name: "default_zoom",
        config: {
            label: "Default zoom",
            type: "text"
        }
    },
    {
        root: true,
        name: "created_date",
        config: {
            label: "Created",
            type: "date"
        },
        fmt: function (val) {
            return ewars.DATE(val, "DAY")
        }
    }
];

const JOINS = [
    "location_type:site_type_id:id:id,name",
    "location:parent_id:uuid:uuid,name"
];


export default class DataView extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action, cell, data) => {
        console.log(action, cell, data);
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Cell>
                    <DataTable
                        resource="location"
                        join={JOINS}
                        rowActions={ACTIONS}
                        onCellAction={this._action}
                        columns={COLUMNS}/>

                </ewars.d.Cell>
            </ewars.d.Layout>
        )
    }
}
