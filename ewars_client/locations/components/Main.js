import Component from "./Component.react";
import DataView from "./DataView";

var TabButton = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-htab-btn";
        if (this.props.current == this.props.view) className += " ide-htab-btn-active";

        var iconClass = "fal " + this.props.icon;

        return (
            <div xmlns="http://www.w3.org/1999/xhtml" onClick={this._onClick}
                 className={className}><i className={iconClass}></i>&nbsp;{this.props.label}
            </div>
        )
    }
});

export default class MainView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT"
        }
    }

    _onTabChange = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") view = <Component/>;
        if (this.state.view == "DATA") view = <DataView/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row height="35px">
                    <ewars.d.Cell>
                        <div xmlns="http://www.w3.org/1999/xhtml" className="ide-htabs">
                            <TabButton
                                label="Locations"
                                icon="fa-map-marker"
                                current={this.state.view}
                                onClick={this._onTabChange}
                                view="DEFAULT"/>
                            <TabButton
                                label="Data"
                                icon="fa-database"
                                current={this.state.view}
                                onClick={this._onTabChange}
                                view="DATA"/>

                            <div className="btn-group pull-right" style={{marginTop: '5px'}}>
                                <ewars.d.Button
                                    label="Create New"
                                    icon="fa-plus"
                                    onClick={this._create}/>
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}