import { LocationField } from "../../../common/fields";

class TransferModal extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDataChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let locationName = ewars.I18N(this.props.item.name);

        return (
            <div>
                <p>This action <strong>CANNOT</strong> be undone. This will transfer all data associated with
                    <strong>{locationName}</strong> to another location.</p>
                <p>Please select the location that you would like to transfer to.</p>

                <LocationSelectField
                    name="location_uuid"
                    onUpdate={this._onDataChange}
                    value={this.props.data.location_uuid}/>

            </div>
        )
    }
}

export default TransferModal;
