import { Layout, Row, Cell } from "../../../common/layout";

import { TextField } from "../../../common/fields";
import Button from "../../../common/c.button";

class DeleteModal extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDataChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let locationName = ewars.I18N(this.props.item.name);

        return (
            <Layout>
                <Row>
                    <Cell padding={14}>
                        <p>This action <strong>CANNOT</strong> be undone. This will delete the <strong>{locationName}</strong> location, its child locations and any alerts, form submissions, assignments or other data associated with the location(s).</p>
                        <p>Please type in the name of the location to confirm.</p>


                        <TextField
                            name="location_name"
                            onUpdate={this._onDataChange}
                            value={this.props.data.location_name}/>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

export default DeleteModal;
