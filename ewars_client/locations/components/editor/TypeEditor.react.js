import CONSTANTS from "../../../common/constants";

import {
    LanguageStringField,
    TextAreaField,
    SelectField
} from "../../../common/fields";

import Button from "../../../common/c.button";

const statusField = {
    type: "select",
    options: [
        ["ACTIVE", "Active"],
        ["DISABLED", "Disabled"]
    ]
};

var TypeEditor = React.createClass({
    getInitialState: function () {
        return {
            errors: {},
            data: {}
        }
    },

    componentWillMount: function () {
        this.state.data = _.extend({}, this.props.data);
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = _.extend({}, nextProps.data);
    },

    _onUpdate: function (prop, value) {
        this.state.data[prop] = value;
        this.forceUpdate();
    },

    _onSave: function () {
        let isRequest = false;
        if (CONSTANTS.ACCOUNT_ADMIN == window.user.user_type) isRequest = !isRequest;

        let successMessage = "Location type created";
        if (isRequest) successMessage = "Location type requested";

        let bl = new ewars.Blocker(null, "Saving changes...");
        if (this.state.data.id) {
            ewars.tx("com.ewars.location", ["UPDATE_TYPE", this.state.data.id, this.state.data])
                .then(resp => {
                    bl.destroy();
                    this.setState({
                        data: resp
                    });
                    ewars.growl("Changes saved");
                    ewars.emit("RELOAD_TYPES");
                })
        } else {
            ewars.tx("com.ewars.location", ["CREATE_TYPE", null, this.state.data])
                .then(resp => {
                    bl.destroy();
                    ewars.growl("Changes saved");
                    this.setState({
                        data: resp
                    });

                    ewars.emit("RELOAD_TYPES");
                })
        }
    },

    render: function () {
        let isStored = false,
            canEdit = false;

        if (this.state.data.id) isStored = !isStored;

        let saveText = "Save";
        if (this.state.data.id) saveText = "Save Change(s)";

        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 36}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">Location Type</div>

                            <div className="btn-group pull-right">
                                <Button
                                    icon="fa-save"
                                    label={saveText}
                                    ref="saveBtn"
                                    onClick={this._onSave}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-settings-panel">
                            <div className="ide-settings-content">

                                <div className="ide-settings-form">

                                    <div className="ide-section-basic">
                                        <div className="header">General Settings</div>
                                        <div className="hsplit-box">
                                            <div className="ide-setting-label">Type Name*</div>
                                            <div className="ide-setting-control">
                                                <LanguageStringInput
                                                    name="name"
                                                    colour="green"
                                                    value={this.state.data.name}
                                                    onUpdate={this._onUpdate}/>
                                                {this.state.errors.name ?
                                                    <div className="ide-setting-error"><i
                                                        className="fal fa-exclamation-triangle"></i>&nbsp;Please enter a
                                                        valid value
                                                    </div>
                                                    : null}
                                            </div>
                                        </div>
                                        <div className="hsplit-box">
                                            <div className="ide-setting-label">Status*</div>
                                            <div className="ide-setting-control">
                                                <SelectField
                                                    name="status"
                                                    value={this.state.data.status}
                                                    onUpdate={this._onUpdate}
                                                    config={statusField}/>
                                                {this.state.errors.status ?
                                                    <div className="ide-setting-error">
                                                        <i className="fal fa-exclamation-triangle"></i>&nbsp;Please enter
                                                        a valid value
                                                    </div>
                                                    : null}
                                            </div>
                                        </div>
                                        <div className="hsplit-box">
                                            <div className="ide-setting-label">Description</div>
                                            <div className="ide-setting-control">
                                                <TextAreaField
                                                    name="description"
                                                    config={{i18n: false}}
                                                    value={this.state.data.description}
                                                    onUpdate={this._onUpdate}/>
                                                {this.state.errors.description ?
                                                    <div className="ide-setting-error"><i
                                                        className="fal fa-exclamation-triangle"></i>&nbsp;Please enter a
                                                        valid value
                                                    </div>
                                                    : null}
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
});

export default TypeEditor;
