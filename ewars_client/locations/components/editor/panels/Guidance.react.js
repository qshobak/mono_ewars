import CONSTANTS from "../../../../common/constants";

var Guidance = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onUpdate: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {
        var readOnly = false;

        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Guidance</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Provide additional information about the location for users.</p>
                </div>
                <div className="ide-settings-form">
                    <div className="placeholder">Guidance editor coming soon</div>


                </div>
            </div>
        )
    }
});


export default Guidance;
