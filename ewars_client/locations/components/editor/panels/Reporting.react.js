import CONSTANTS from "../../../../common/constants";

import {
    SwitchField as Switch,
    SelectField,
    DateField
} from "../../../../common/fields/";


var FIELD_CONFIG = {
    form_id: {
        type: "select",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            additionalProperties: ["site_type_id"],
            query: {
                status: {eq: "ACTIVE"},
                ftype: {eq: "PRIMARY"}
            }
        }
    },
    application: {
        options: [
            ["NONE", "This location only"],
            ["ALL", "All Children"],
            ["TYPE", "All Children of specified type"]
        ]
    },
    application_location_type: {
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    }
};

var ListItem = React.createClass({
    _delete: function () {
        if (this.props.data.type == "PROPAGATED") {
            ewars.prompt("fa-trash", "Delete Period", "Are you sure you want to delete this reporting period, the reporting period will be removed from all locations which inherit it.", function () {
                var blocker = new ewars.Blocker(null, "Deleting reporting period...");
                ewars.tx("com.ewars.location.period.delete", [this.props.data.uuid])
                    .then(resp => {
                        blocker.destroy();
                        ewars.growl("Reporting period removed");
                        ewars.emit("RELOAD_REPORTING");
                    })
            }.bind(this))
        } else {
            ewars.prompt("fa-trash", "Delete Period?", "Are you sure you want to delete this reporting period?", function () {
                var blocker = new ewars.Blocker(null, "Deleting reporting period...");
                ewars.tx('com.ewars.location.period.delete', [this.props.data.uuid])
                    .then(resp => {
                        blocker.destroy();
                        ewars.growl("Reporting period removed");
                        ewars.emit("RELOAD_REPORTING");
                    })
            }.bind(this))
        }
    },

    _edit: function () {
        this.props.onEdit(this.props.data);
    },

    render: function () {
        var formName = ewars.I18N(this.props.data.form_name);
        var startDate = ewars.DATE(this.props.data.start_date);
        var endDate = "Open";
        if (this.props.data.end_date) endDate = ewars.DATE(this.props.data.end_date);
        var typeName = this.props.data.pid ? 'Inherited' : 'Custom';

        if (this.props.data.status == "DISABLED") {
            startDate = "N/A";
            endDate = "N/A";
        }

        let canDelete = false;
        if (this.props.data.pid == null) canDelete = true;

        return (
            <tr>
                <td>{formName}</td>
                <td>{startDate}</td>
                <td>{endDate}</td>
                <td>{typeName}</td>
                <td>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._edit}
                            icon="fa-pencil"/>
                        {canDelete ?
                        <ewars.d.Button
                            icon="fa-trash"
                            onClick={this._delete}/>
                            : null}
                    </div>
                </td>
            </tr>
        )
    }
});

var Reporting = React.createClass({
    getInitialState: function () {
        return {
            forms: [],
            view: CONSTANTS.DEFAULT,
            form: null,
            period: {}
        }
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading reporting details...");
        ewars.tx("com.ewars.location.periods", [this.props.data.uuid])
            .then(resp => {
                blocker.destroy();
                this.setState({
                    reporting: resp
                })
            });
        ewars.subscribe("RELOAD_REPORTING", this._reload);
    },

    _reload: function () {
        var blocker = new ewars.Blocker(null, "Loading reporting details...");
        ewars.tx("com.ewars.location.periods", [this.props.data.uuid])
            .then(resp => {
                blocker.destroy();
                this.setState({
                    reporting: resp
                })
            })
    },

    _addPeriod: function () {
        this.state.period = {
            form_id: null,
            start_date: null,
            end_date: null
        };
        this.state.view = "EDIT";
        this.forceUpdate();
    },

    _onChange: function (prop, value, path, additionalData) {
        this.state.period[prop] = value;
        if (prop == "form_id") {
            this.state.form = additionalData;
        }
        this.forceUpdate();
    },

    _cancelPeriod: function () {
        this.state.period = {};
        this.state.view = CONSTANTS.DEFAULT;
        this.forceUpdate();
    },

    _update: function (data) {
        var blocker = new ewars.Blocker(null, "Updating reporting period...");
        ewars.tx("com.ewars.location.period.update", [data.uuid, data])
            .then(resp => {
                blocker.destroy();
                ewars.growl("Reporting period updated");
            })
    },

    _savePeriod: function () {
        if (!this.state.period.form_id) {
            ewars.growl("Please select a valid form");
            return;
        }

        if (!this.state.period.start_date) {
            ewars.growl("Please select a valid Start Date");
            return;
        }

        var data = JSON.parse(JSON.stringify(this.state.period));

        if (!this.state.period.override_disable) {
            if (this.state.form.site_type_id != this.props.data.site_type_id) {
                data.type = "PROPAGATED";
            } else {
                data.type = "DEFAULT";
            }
        }

        if (this.state.period.location_id != this.props.data.uuid) {
            data.location_id = this.props.data.uuid;
            delete data.id;
        } else {
            this._update(data);
            return;
        }

        var blocker = new ewars.Blocker(null, "Saving reporting period...");
        ewars.tx('com.ewars.location.period.create', [data])
            .then(resp => {
                if (resp.success = true) {
                    this.setState({
                        view: CONSTANTS.DEFAULT,
                        period: {}
                    });
                    blocker.destroy();
                    ewars.growl("Reporting period created");
                    ewars.emit("RELOAD_REPORTING");
                } else {
                    blocker.destroy();
                    ewars.prompt("fa-error", "Error", resp.error, function () {

                    })
                }
            })
    },

    _onEdit: function (item) {
        this.state.period = item;
        this.state.form = {
            name: item.name,
            site_type_id: item.site_type_id,
            time_interval: item.time_interval
        };

        this.state.view = "EDIT";
        this.forceUpdate();
    },

    render: function () {
        var view;
        if (this.state.view == CONSTANTS.DEFAULT) {
            let rows = (this.state.reporting || []).map(item => {
                return <ListItem data={item} location={this.props.data} onEdit={this._onEdit}/>
            }, this);

            view = (
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">Reporting Periods</div>

                        <div style={{display: "block", height: "30px", marginTop: "14px", paddingRight: "8px"}}>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Add Reporting Period"
                                    icon="fa-plus"
                                    onClick={this._addPeriod}/>
                            </div>
                        </div>


                        <div className="ide-inline-grid">
                            <table width="100%" className="ide-table">
                                <thead>
                                <tr>
                                    <th>Form Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Type</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {rows}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            )
        }

        if (this.state.view == "EDIT") {
            var warning;
            if (this.state.period.form_id && this.props.data.site_type_id != this.state.form.site_type_id) {
                warning = (
                    <div className="warning" style={{margin: 10, display: "block"}}>
                        <p><strong>Warning:</strong> This reporting period will be applied to all this locations
                            children which match the forms specified site type.</p>
                        <p>Reporting periods set for the same form under this location will override this locations
                            settings.</p>
                    </div>
                )
            }

            if (this.state.period.location_id != this.props.data.uuid && this.state.period.id) {
                warning = (
                    <div className="warning" style={{margin: 10, display: "block"}}>
                        <strong>Warning </strong>You are creating an override for an inherited
                        reporting
                        period, please note that if you update/remove the original
                        reporting period, this new reporting period will remain intact.
                    </div>
                )
            }

            var editingInheritance = false;
            if (this.state.period.id && this.state.period.location_id != this.props.data.uuid && this.state.period.type == "PROPAGATED") {
                editingInheritance = true;
            }

            var readOnly = false;
            var formReadOnly = false;
            if (this.state.period.type == "DISABLED") readOnly = true;
            if (this.state.period.id || this.state.period.type == CONSTANTS.DISABLED) formReadOnly = true;

            view = (
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">Reporting Periods</div>

                        {warning}

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Form*</div>
                            <div className="ide-setting-control">
                                <SelectField
                                    name="form_id"
                                    template="form"
                                    readOnly={formReadOnly}
                                    onUpdate={this._onChange}
                                    value={this.state.period.form_id}
                                    config={FIELD_CONFIG.form_id}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Start Date*</div>
                            <div className="ide-setting-control">
                                <DateField
                                    name="start_date"
                                    readOnly={readOnly}
                                    value={this.state.period.start_date}
                                    config={{date_type: "DAY"}}
                                    onUpdate={this._onChange}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">End Date</div>
                            <div className="ide-setting-control">
                                <DateField
                                    name="end_date"
                                    readOnly={readOnly}
                                    config={{date_type: "DAY"}}
                                    value={this.state.period.end_date}
                                    onUpdate={this._onChange}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Status</div>
                            <div className="ide-setting-control">
                                <SelectField
                                    name="status"
                                    onUpdate={this._onChange}
                                    value={this.state.period.status}
                                    config={{options: [['ACTIVE', 'Active'], ['DISABLED', 'Disabled']]}}/>
                            </div>
                        </div>

                        {editingInheritance ?
                            <div className="hsplit-box">
                                <div className="ide-setting-label">
                                    Disable
                                </div>
                                <div className="ide-setting-control">
                                    <Switch
                                        name="override_disable"
                                        readOnly={readOnly}
                                        value={this.state.period.override_disable}
                                        onUpdate={this._onChange}/>
                                </div>
                            </div>
                            : null}

                    </div>

                    <div className="ide-section-basic">

                        <div className="btn-group">
                            <ewars.d.Button
                                icon="fa-save"
                                label="Save"
                                colour="green"
                                onClick={this._savePeriod}/>
                            <ewars.d.Button
                                icon="fa-times"
                                label="Cancel"
                                onClick={this._cancelPeriod}/>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-briefcase"></i></div>
                    <div className="ide-settings-title">Reporting</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Set what forms this location should be reporting and the period that it should report for.</p>
                </div>
                {view}
            </div>
        )
    }
});

export default Reporting;
