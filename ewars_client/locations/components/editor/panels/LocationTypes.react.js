import Button from "../../../../common/c.button";

var ListItem = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var name = ewars.formatters.I18N_FORMATTER(this.props.data.name);
        let className = "block-content";

        if (this.props.data.status == "ACTIVE") className += " status-green";
        if (this.props.data.status == "DISABLED") className += " status-red";


        return (
            <div className="block" onClick={this._onClick}>
                <div className={className}>{name}</div>
            </div>
        )
    }
});

var LocationTypes = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.query", ['location_type', null, {}, {"name.en": "ASC"}, null, null, null])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    componentDidMount: function () {
        ewars.subscribe("RELOAD_TYPES", this._reload);
    },

    _reload: function () {
        ewars.tx("com.ewars.query", ["location_type", null, {}, {"name.en": "ASC"}, null, null, null])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onItemClick: function (item) {
        this.props.onEdit(item);
    },

    render: function () {
        let items = this.state.data.map(function (item) {
            return <ListItem data={item} onClick={this._onItemClick}/>;
        }.bind(this));

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="block-tree">
                    {items}
                </div>
            </div>
        )
    }
});

export default LocationTypes;
