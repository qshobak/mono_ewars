import content from "../../ewars/lang/en/content";

const ACTIONS = [
    ['fa-plus', 'ADD']
];

class LocType extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div
                onClick={() => {
                    this.props.onClick(this.props.data);
                }}
                className="block">
                <div className="block-content">{__(this.props.data.name)}</div>
            </div>
        )
    }
}

class ViewLocationTypes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: []};

        ewars.tx('com.ewars.locations.types', [])
            .then(resp => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        return (
            <ewars.d.Panel>
                <div className="block-tree" style={{position: 'relative'}}>
                    {this.state.data.map(item => {
                        return <LocType
                            onClick={(data) => {
                                this.props.onAction('EDIT', data)
                            }}
                            data={item}/>;
                    })}
                </div>
            </ewars.d.Panel>
        )
    }
}

export default ViewLocationTypes;