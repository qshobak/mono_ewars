import ControlField from '../../common/controls/ui.control_field';

class ViewGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={this.props.data}>
                    <ewars.d.ActionGroup
                        actions={[]}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-control-editor">
                            <ControlField label="Name">
                                <input
                                    value={this.props.data}
                                    type="text"/>
                            </ControlField>
                        </div>

                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Toolbar label="Location(s)">
                    <ewars.d.ActionGroup
                        actions={[]}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewGroup;