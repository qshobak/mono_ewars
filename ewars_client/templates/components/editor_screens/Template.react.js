import WidgetEditor from "../../../common/editors/widgets/c.widget_editor";

import { Shade } from "../../../common";
import RangeUtils from "../../../common/utils/RangeUtils";

var Template = React.createClass({
    _isReady: false,
    getInitialState: function () {
        return {
            editWidget: false,
            showChartEditor: false,
            editId: null
        }
    },

    componentWillMount: function () {
        ewars.TEMPLATE_MODE = "TEMPLATE";
        ewars.subscribe("TEMPLATE_EDIT_SERIES", function (data) {
            this.state.showChartEditor = true;
            this.state.editWidget = data[0];
            this.state.editId = data[1];

            if (this.props.data.data) {
                let widget = this.props.data.data[data[1]] || {type: "SERIES"};
                if (!widget.period) {
                    widget.period = RangeUtils.convertLegacy(widget);
                    delete widget.start_date_spec;
                    delete widget.end_date_spec;
                    delete widget.start_intervals;
                    delete widget.end_intervals;
                    delete widget.start_date;
                    delete widget.end_date;
                }

                this.state.editDefinition = widget || {type: "SERIES"};
            } else {
                this.state.editDefinition = {type: "SERIES"}
            }

            if (this.isMounted()) this.forceUpdate();
        }.bind(this));
    },

    componentWillUnmount: function () {
        this._isReady = false;
    },

    componentDidMount: function () {
        var cmp = this;
        var editorEl = this.refs.reportWrapper.clientHeight;
        CKEDITOR.disableAutoInline = false;

        CKEDITOR.replace('editor1', {
            extraPlugins: 'ewarschart,codemirror,uploadimage',
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                '/',
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                '/',
                {name: 'tools', groups: ['tools']},
                {name: 'others', groups: ['others']},
                {name: 'about', groups: ['about']}
            ],
            removeButtons: 'Save,NewPage,Preview,Print,Templates,Cut,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,Language,BidiRtl,BidiLtr,Flash,Smiley,Iframe,Maximize,ShowBlocks,About',
            config: {
                autoGrow_onStartup: true,
                autoParagraph: false,
                allowedContent: true,
                extraAllowedContent: 'script{*}[*](*),div{*}[*](*)',
                disallowedContent: '',
                htmlEncodeOutput: false,
                imageUploadUrl: "/upload/reports",
                entities: false,
                enterMode: CKEDITOR.ENTER_DIV,
                forceEnterMode: true,
                disableNativeSpellChecker: true
            },
            imageUploadUrl: "/upload/reports",
            allowedContent: true,
            extraAllowedContent: 'script{*}[*](*),div{*}[*](*)',
            disallowedContent: '',
            enterMode: CKEDITOR.ENTER_DIV,
            forceEnterMode: true,
            autoParagraph: false,
            htmlEncodeOutput: false,
            entities: false,
            disableNativeSpellChecker: true,
            on: {
                instanceReady: function (evt) {
                    evt.editor.resize("100%", editorEl);
                    this._isReady = true;
                }.bind(this),
                change: function (evt) {
                    if (this._isReady) {
                        var content = evt.editor.getData();
                        cmp.props.onChange("content", content);
                    }
                }.bind(this)
            }
        });
    },

    _handleWidgetEditCancel: function () {
        this.state.editWidget = null;
        this.state.showChartEditor = false;
        this.state.editId = null;
        this.forceUpdate();
    },

    _onWidgetSettingsSave: function (rI, cI, data) {
        let t_data = ewars.copy(this.props.data.data || {});
        t_data[this.state.editId] = data;
        this.state.editWidget.setData({
            type: data.type
        });
        this.state.editWidget = null;
        this.state.showChartEditor = false;
        this.state.ediId = null;
        this.forceUpdate();

        this.props.onChange("data", [CKEDITOR.instances.editor1.getData(), t_data]);

    },

    _onShadeAction: function (action) {
        this.setState({
            editWidget: null,
            showChartEditor: false,
            editId: null
        })
    },

    render: function () {
        return (
            <div className="ide-panel ide-panel-absolute" ref="reportWrapper">
                <textarea id="editor1" style={{height: "100%"}} defaultValue={this.props.data.content}/>
                <Shade
                    toolbar={false}
                    onAction={this._onShadeAction}
                    shown={this.state.showChartEditor}>
                    <WidgetEditor
                        onSave={this._onWidgetSettingsSave}
                        onCancel={this._handleWidgetEditCancel}
                        showPreview={true}
                        mode="TEMPLATE"
                        data={this.state.editDefinition}
                        visible={this.state.showChartEditor}/>
                </Shade>
            </div>
        )
    }
});

export default Template;
