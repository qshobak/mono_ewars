import HtmlEditor from "./editor.html";
import { VertTab, Shade } from "../../common";
import { default as WEditor } from "../../common/editors/widgets/c.widget_editor";

import RangeUtils from "../../common/utils/RangeUtils";

import DecoupledEditor from "@ckeditor/ckeditor5-build-decoupled-document/build/ckeditor";
import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";

import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import Heading from "@ckeditor/ckeditor5-heading/src/heading";
import List from "@ckeditor/ckeditor5-list/src/list";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
import Code from "@ckeditor/ckeditor5-basic-styles/src/code";
import Subscript from "@ckeditor/ckeditor5-basic-styles/src/subscript";
import Superscript from "@ckeditor/ckeditor5-basic-styles/src/superscript";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
import CKFinder from "@ckeditor/ckeditor5-ckfinder/src/ckfinder";
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
import Font from "@ckeditor/ckeditor5-font/src/font";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";

import Image from "@ckeditor/ckeditor5-image/src/image";
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar";
import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption";
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle";

import WidgetEditor from "./plugins/widget_editor/widget_editor";

import CKEditorInspector from "@ckeditor/ckeditor5-inspector";

const plugins = [Image, ImageToolbar, ImageCaption, ImageStyle, Font, Alignment, Table, TableToolbar, CKFinder, Essentials, Paragraph, Heading, List, Bold, Italic, Underline, Code, Superscript, Subscript, WidgetEditor];
const toolbar = ["insertTable", "fontSize", "fontFamily", "alignment", "ckfinder", "heading", "bold", "italic", "underline", "code", "superscript", "subscript", "numberedlist", "bulletedlist", "|", "widget_editor"];

const PAGE_TEMPLATE = {
    position: "relative",
    display: "block",
    margin: "5px"
}

class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = (e) => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let style = PAGE_TEMPLATE;
        if (this.props.active) {
            style = {
                ...PAGE_TEMPLATE,
                borderLeft: "6px solid #CCCCCC"
            }
        }
        return (
            <div style={style}>
                <ewars.d.Row>
                    {this.props.data.title}
                </ewars.d.Row>
                <ewars.d.Toolbar>
                    <div className="btn-group pull-right">
                        <ewars.d.Button icon="fa-pencil" onClick={this._click}/>
                        <ewars.d.Button icon="fa-caret-up"/>
                        <ewars.d.Button icon="fa-caret-down"/>
                        <ewars.d.Button icon="fa-circle"/>
                        <ewars.d.Button icon="fa-copy"/>
                        <ewars.d.Button icon="fa-trash"/>
                    </div>
                </ewars.d.Toolbar>
            </div>
        )
    }
}

class EditorV2 extends React.Component {
    _editor = null;
    constructor(props) {
        super(props);

        let pages = [
            {
                uuid: ewars.utils.uuid(),
                title: "New Page",
                orient: 1,
                content: "<p>New Page</p>",
                order: 0
            }
        ];

        this.state = {
            widget: null,
            view: "EDITOR",
            pages: pages,
            curPage: pages[0]
        };
    }

    _changeView = (view) => {
        if (view == "EDITOR" && this.state.view != "EDITOR") {
            this.setState({view: view}, () => {
                this._initEditor();
            })
        } else {
            this.setState({view: view})
        }
    };

    _select = (page) => {
        let pages = this.state.pages;
        let curPage = this.state.curPage;

        if (this.state.view == "EDITOR") {
            curPage.content = window.editor.getData();
        }

        pages.forEach(item => {
            if (item.uuid == curPage.uuid) {
                item.content = curPage.content;
            }
        });


        this.setState({
            curPage: page,
            pages,
        }, () => {
            if (this.state.view == "EDITOR") {
                window.editor.setData(page.content);
            }
        });
    };

    _addPage = () => {
        let pages = this.state.pages;
        pages.push({
            uuid: ewars.utils.uuid(),
            title: "New Page",
            orientation: 1,
            content: "",
            order: 1
        });

        this.setState({
            pages: pages
        });
    };

    componentDidMount() {
        window.addEventListener("edit-widget", this._editWidget);
        this._initEditor();
    }

    componentWillUnmount() {
        window.removeEventListener("edit-widget", this._editWidget);
    }

    _editWidget = (e) => {
        this.setState({
            widget: this.props.data.data[e.detail] | {}
        });
    };

    _initEditor = () => {
        if (this.state.view == "EDITOR") {
            this._editor = DecoupledEditor.create(document.querySelector(".document-editor__editable"), {
                plugins: plugins,
                toolbar: toolbar,
                table: {
                    contentToolbar: ["tableColumn", "tableRow", "mergeTableCells"]
                },
                ckfinder: {
                    uploadUrl: "/upload",
                    openerMethod: "popup",
                    options: {
                        resourceType: "Images"
                    }
                }
            })
                .then( editor => {
                    editor.setData(this.state.curPage.content);
                    const toolbarContainer = document.querySelector(".document-editor__toolbar");
                    toolbarContainer.appendChild(editor.ui.view.toolbar.element);

                    window.editor = editor;
                })
                .catch(err => {
                    console.log(err)
                });
        }
    };

    _onChange = (val) => {
        let pages = this.state.pages;
        pages[0].content = val;
        this.setState({
            pages: pages
        })
    };

    _onCancelWidgetEdit = () => {
        this.setState({widget: null});
    };

    render() {
        let view;

        if (this.state.view == 'EDITOR') {
            view = (
                    <div className="document-editor">
                        <div className="document-editor__toolbar">

                        </div>
                        <div className="document-editor__editable-container">

                            <div className="document-editor__editable">
                            </div>
                        </div>
                    </div>
            )
        } else {
            view = <HtmlEditor onChange={this._onChange} data={this.state.curPage}/>
        }
        return (
            <ewars.d.Layout style={{flexDirection: "row"}}>
                <ewars.d.Cell style={{flexDirection: "column"}} width="250" borderRight={true}>
                    <ewars.d.Toolbar label="Pages">
                        <ewars.d.Button
                            onClick={this._addPage}
                            label="Add Page"/>

                    </ewars.d.Toolbar>
                    <ewars.d.Row style={{display: "block", overflowY: "auto", padding: "5px"}}>
                        {this.state.pages.map(item => {
                            return <Page
                                active={this.state.curPage.uuid == item.uuid}
                                onSelect={this._select}
                                data={item}
                                key={item.uuid}/>;
                        })}

                    </ewars.d.Row>
                </ewars.d.Cell>
                <ewars.d.Cell width="34" borderRight={true} style={{position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <VertTab
                            view={"EDITOR"}
                            label="Editor"
                            icon="fa-code"
                            active={this.state.view == "EDITOR"}
                            onClick={this._changeView}/>

                        <VertTab
                            view={"SOURCE"}
                            label="Source"
                            icon="fa-code"
                            active={this.state.view == "SOURCE"}
                            onClick={this._changeView}/>
                    </div>
                </ewars.d.Cell>
                <ewars.d.Cell>
                    {view}
                </ewars.d.Cell>
                <Shade
                    toolbar={false}
                    onAction={this._onShadeAction}
                    shown={this.state.widget != null}>
                    <WEditor
                        onSave={this._onSaveWidget}
                        onCancel={this._onCancelWidgetEdit}
                        showPreview={true}
                        mode="TEMPLATE"
                        data={this.state.widget}
                        visible={this.state.showChartEditor}/>
                </Shade>
            </ewars.d.Layout>
        )
    }

}

export default EditorV2;
