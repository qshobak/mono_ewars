import Command from "@ckeditor/ckeditor5-core/src/command";

export default class InsertWidgetEditorCommand extends Command {
    execute() {
        this.editor.model.change( writer => {
            this.editor.model.insertContent( createWidgetEditor( writer ) );
        });
    }

    refresh() {
        const model = this.editor.model;
        const selection = model.document.selection;
        const allowedIn = model.schema.findAllowedParent( selection.getFirstPosition(), "widget_editor");

        this.isEnabled = allowedIn !== null;
    }
}

function createWidgetEditor( writer ) {
    const widgetEditor = writer.createElement("widget_editor", { wid: ewars.utils.uuid() });
    return widgetEditor;
}
