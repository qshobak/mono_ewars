import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import ClickObserver from "@ckeditor/ckeditor5-engine/src/view/observer/clickobserver";

import { toWidget, toWidgetEditable } from "@ckeditor/ckeditor5-widget/src/utils";
import Widget from "@ckeditor/ckeditor5-widget/src/widget";

import InsertWidgetEditorCommand from "./insert_widget_editor_command";

export default class WidgetEditorEditing extends Plugin {
    static get requires() {
        return [ Widget ];
    }

    init() {
        this._defineSchema();
        this._defineConverters();

        this.editor.commands.add("insertWidgetEditor", new InsertWidgetEditorCommand( this.editor ) );

        this.listenTo(this, "click", (e) => {
            console.log(e);
        });

        this.listenTo(this, "execute", (e) => {
            console.log(e)
        });

        const editor = this.editor;
        const model = editor.model;
        const editingView = editor.editing.view;
        const viewDocument = editor.editing.view.document;
        editor.editing.view.addObserver( ClickObserver);

        this.listenTo(viewDocument, "click", (event, data) => {
            const target = data.target;
            const modelObj = editor.editing.mapper.toModelElement(target);
            console.log(modelObj);
            if (modelObj.name == "widget_editor") {
                window.dispatchEvent(new CustomEvent("edit-widget", {
                    detail: modelObj.getAttribute("wid")
                }));
            }
        });

    }

    _defineSchema() {
        const schema = this.editor.model.schema;

        schema.register("widget_editor", {
            isObject: true,
            allowWhere: "$block",
            allowAttributes: [ "attribute", "content", "wid" ]
        });

    }

    _defineConverters() {
        const conversion = this.editor.conversion;

        conversion.attributeToAttribute({
            model: {
                name: "widget_editor",
                key: "wid"
            },
            view: {
                key: "data-wid"
            }
        });

        conversion.for("upcast").elementToElement({
            model: ( viewElement, modelWriter ) => {
                const firstChild = viewElement.getChild(0);
                const wid = viewElement.getAttribute("wid");

                return modelWriter.createElement( "widget_editor", { wid: wid });
            },
            view: {
                name: "div",
                classes: "widget_editor"
            }
        });

        conversion.for("downcast").elementToElement({
            model: "widget_editor",
            view: (viewElement, viewWriter) => {
                const container = viewWriter.createContainerElement("div", { class: "widget_editor" } );
                return toWidget(container, viewWriter, {label: "Widget Editor"});
            }
        });



        /**

        conversion.for("upcast").elementToElement({
            model: "widget_editor",
            view: {
                name: "section",
                classes: "widget_editor"
            }
        });

        conversion.for("dataDowncast").elementToElement( {
            model: "widget_editor",
            view: {
                name: "section",
                classes: "widget_editor"
            }
        });

        conversion.for("editingDowncast").elementToElement( {
            model: "widget_editor",
            view: ( modelElement, viewWriter ) => {
                const section = viewWriter.createContainerElement("section", {class: "widget_editor" } );
                return toWidget( section, viewWriter, {label: "widget editor"} );
            }
        });
         **/
    }
}
