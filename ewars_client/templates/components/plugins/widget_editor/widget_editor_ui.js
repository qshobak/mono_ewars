import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import ButtonView from "@ckeditor/ckeditor5-ui/src/button/buttonview";

export default class WidgetEditorUI extends Plugin {
    init() {
        console.log("WidgetEditorUID#init() got called");

        const editor = this.editor;
        const t = editor.t;

        editor.ui.componentFactory.add("widget_editor", locale => {
            const command = editor.commands.get("insertWidgetEditor");
            const buttonView = new ButtonView( locale );

            buttonView.set({
                label: t("Widget (block)"),
                withText: true,
                tooltip: true
            });

            buttonView.bind("isOn", "isEnabled" ).to( command, "value", "isEnabled");

            this.listenTo(buttonView, "execute", () => editor.execute("insertWidgetEditor") );

            return buttonView;
        });
    }
}
