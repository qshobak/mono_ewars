import {
    Button,
    VertTab,
    Spinner
} from "../../common";

import Settings from "./editor_screens/Settings.react";
import Template from "./editor_screens/Template.react";
import Help from "./editor_screens/Help.react";
import EditorV2 from "./EditorV2";

const CONSTANTS = {
    SETTINGS: "SETTINGS",
    TEMPLATE: "TEMPLATE",
    REFERENCE: "REFERENCE"
};


var TemplateEditor = React.createClass({
    _redactor: null,
    getInitialState: function () {
        return {
            data: null,

            showLegend: false,
            shoulderView: CONSTANTS.SETTINGS
        }
    },

    _onChange: function (prop, value) {
        this.props.onChange(prop, value);
    },

    _changeShoulder: function (view) {
        this.setState({
            shoulderView: view
        })
    },

    render: function () {

        var view;

        if (this.state.shoulderView == CONSTANTS.SETTINGS) view =
            <Settings data={this.props.data} onChange={this._onChange}/>;
        if (this.state.shoulderView == CONSTANTS.TEMPLATE) view =
            <Template data={this.props.data} onChange={this._onChange}/>;
        if (this.state.shoulderView == CONSTANTS.REFERENCE) view = <Help/>;
        if (this.state.shoulderView == "EDITOR") view = <EditorV2 data={this.props.data}/>;

        return (
            <div className="ide-panel ide-panel-absolute" ref="reportWrapper">
                <div className="ide">
                    <div className="ide-layout">
                        <div className="ide-row">
                            <div className="ide-col ide-relative ide-hide-overflow" style={{maxWidth: 34}}>
                                <div className="ide-tabs">
                                    <VertTab
                                        view={CONSTANTS.SETTINGS}
                                        label="Settings"
                                        icon="fa-cog"
                                        active={this.state.shoulderView == CONSTANTS.SETTINGS}
                                        onClick={this._changeShoulder}/>
                                    <VertTab
                                        view={CONSTANTS.TEMPLATE}
                                        label="Template"
                                        icon="fa-code"
                                        active={this.state.shoulderView == CONSTANTS.TEMPLATE}
                                        onClick={this._changeShoulder}/>
                                    <VertTab
                                        view={CONSTANTS.REFERENCE}
                                        label="Help"
                                        icon="fa-question"
                                        active={this.state.shoulderView == CONSTANTS.REFERENCE}
                                        onClick={this._changeShoulder}/>
                                    <VertTab
                                        view={"EDITOR"}
                                        label="Editor"
                                        icon="fa-code"
                                        active={this.state.shoulderView == "EDITOR"}
                                        onClick={this._changeShoulder}/>

                                </div>
                            </div>
                            <div className="ide-col">
                                {view}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default TemplateEditor;
