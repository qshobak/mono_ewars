import Component from '../components/Component.react';

const VIEWS = [
    ['fa-user', 'Users', 'DEFAULT'],
    ['fa-clipboard', 'Assignments', 'ASSIGNMENTS'],
    ['fa-child', 'Roles', 'ROLES'],
    ['fa-users', 'Teams', 'TEAMS'],
    ['fa-building', 'Organizations', 'ORGS'],
    ['fa-envelope', 'Invitations', 'INVITES']
]

class ViewMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'DEFAULT'
        }
    }

    _setView = (view) => {
        this.setState({view: view})
    };

    render() {
        let view;

        if (this.state.view == 'DEFAULT') view = <Component/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="btn-group btn-tabs">
                        {VIEWS.map(item => {
                            return (
                                <ewars.d.Button
                                    highlight={this.state.view == item[2]}
                                    icon={item[0]}
                                    label={item[1]}
                                    onClick={() => {
                                        this._setView(item[2])
                                    }}/>
                            )
                        })}
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewMain;