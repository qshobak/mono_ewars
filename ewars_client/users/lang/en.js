const LANG = {
    ACTIVE_INVITE_EXISTS: "An invite has already been sent for the email address specified",
    USER_EXISTS_IS_ACTIVE: "The email specified belongs to an already active user.",
    USER_EXISTS_IS_INACTIVE: "The email specified belongs to an already existing user whose status is Inactive.",
    USER_EXISTS_IS_REVOKED: "The email specified belongs to an existing user whose access is currently revoked.",
    USER_IS_SYSTEM: "A user already exists with this email address and is controlled by another account, account-controlled users can not be invited to other accounts at this time.",

    ACCOUNT_CONTROLLED: "Account controlled",
    EMAIL: "Email",
    NAME: "Name",
    CONFIRM_PASSWORD: "Confirm password",
    PASSWORD: "Password",
    ORGANIZATION: "Organization",
    PHONE: "Phone",
    OCCUPATION: "Occupation",
    ACCOUNT_PERMISSIONS: "Account permissions",
    ACCESS_DETAILS: "Access details",
    ROLE: "Role",
    STATUS: "Status",
    LOCATION: "Location",

    ADD_USER: "Add user",
    USER: "User",
    SEND_INVITE: "Send invite",

    USER_REINSTATED: "User reinstated",

    ERROR_EMAIL: "Please provide a valid email address",
    REINSTATE_USER: "Reinstate user",
    CANCEL: "Cancel",

    USER_EDITOR: "User Editor",
    ASSIGNMENT_EDITOR: "Assignment Editor",
    UPDATE_PASSWORD: "Update Password",

    REVOKE_USER_ACCESS_T: "Revoke User Access?",
    REVOKE_USER_ACCESS_P: "Are you sure you want to revoke this users access? They will no longer be able to access this account.",

    UPDATING_PASSWORD: "Updating password...",
    LOADING_USER: "Loading user details...",

    NEW: "New",
    SAVE_CHANGES: "Save change(s)",
    EDIT: "Edit",
    EDIT_ASSIGNMENTS: "Edit assignments",
    CHANGE_PASSWORD: "Change password",
    REVOKE_ACCESS: "Revoke access",
    CLOSE: "Close",

    TRUE_SYSTEM: "You will be able to control this users password and other details",
    FALSE_SYSTEM: "You will NOT be able to control this users password and other details, please ensure you enter a valid, active email address."

};

export default LANG;