import Button from "../../common/c.button";
import DataTable from "../../common/datatable/c.datatable";
import Modal from "../../common/c.modal";
import Shade from "../../common/c.shade";

import COLUMNS from "../constants/columns";
import { Layout, Row, Cell, Panel }  from "../../common/layout";
import Toolbar from "../../common/c.toolbar";

import UserEditor from "./UserEditor.react";
import AssignmentManager from "./AssignmentManager.react";
import PasswordEditor from "./PasswordEditor.react";
import AddUser from "./AddUser";
import AssignmentsControl from './c.assignments';

const EDITORS = {
    USER: {
        Cmp: UserEditor,
        title: "USER_EDITOR",
        icon: "fa-user"
    },
    ASSIGNMENTS: {
        // Cmp: AssignmentManager,
        Cmp: AssignmentsControl,
        title: "ASSIGNMENT_EDITOR",
        icon: "fa-clipboard"
    },
    PASSWORD: {
        Cmp: PasswordEditor,
        title: "UPDATE_PASSWORD",
        icon: "fa-lock"
    },
    ADD_USER: {
        Cmp: AddUser,
        title: "ADD_USER",
        icon: "fa-plus"
    }
};

const VIEW_ACTIONS = [
    ['fa-filter', 'FILTER'],
    ['fa-plus', 'ADD'],
    ['fa-envelope', 'INVITE']
]

const MODAL_ACTIONS = [
    {label: __("SAVE_CHANGES"), icon: "fa-save", action: "SAVE"},
    {label: __("CANCEL"), icon: "fa-times", action: "CANCEL"}
];

const ASSIGN_ACTIONS = [
    {label: __("CLOSE"), icon: "fa-times", action: "CANCEL"}
];

const GRID_ACTIONS = [
    {label: __("EDIT"), icon: "fa-pencil", action: "EDIT"},
    {label: __("EDIT_ASSIGNMENTS"), icon: "fa-clipboard", action: "EDIT_ASSIGNMENTS"},
    {label: __("CHANGE_PASSWORD"), icon: "fa-lock", action: "PASSWORD"},
    {label: __("REVOKE_ACCESS"), icon: "fa-ban", action: "REVOKE"}
];


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: null,
            edit: {
                password: ""
            },
            password: ""
        }
    }

    _onEdit = (user) => {
        let bl = new ewars.Blocker(null, __("LOADING_USER"));
        ewars.tx("com.ewars.resource", ["sso", this.props.data.id, null, null])
            .then(function (resp) {
                bl.destroy();
                this.setState({
                    edit: resp,
                    editMode: "USER"
                })
            }.bind(this))
    };

    _onAction = (action, cell, row) => {
        if (row.status == "REVOKED") {
            ewars.prompt(
                "fa-check",
                "User Access Revoked",
                "This users access has been revoked, would you like to reinstate their access?",
                () => {
                    let bl = new ewars.Blocker(null, "Reinstating user access...");
                    ewars.tx("com.ewars.user.reinstate", [row.id])
                        .then((resp) => {
                            bl.destroy();
                            ewars.emit("RELOAD_DT");
                            ewars.growl("User access reinstated");
                        })
                }
            );
            return;
        }
        if (action == "EDIT") this.setState({editMode: "USER", edit: row, paused: true});
        if (action == "CLICK") this.setState({editMode: "USER", edit: row, paused: true});
        if (action == "CANCEL") this.setState({editMode: null, edit: null, paused: true});
        if (action == "EDIT_ASSIGNMENTS") this.setState({editMode: "ASSIGNMENTS", edit: row, paused: true});
        if (action == "PASSWORD") this.setState({editMode: "PASSWORD", edit: row, paused: true});
        if (action == "REVOKE") this._revoke(row);
    };

    _updateUser = (user) => {
        this.setState({
            edit: user,
            createError: null
        })
    };

    _closeModal = () => {
        this.setState({
            editMode: null,
            edit: null,
            password: ""
        });
        ewars.emit("RELOAD_DT");
    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({editMode: null, edit: null, password: "", paused: false});
        if (action == "CANCEL") this.setState({editMode: null, edit: null, password: "", paused: false});

        if (action == "SAVE") {
            if (this.state.editMode == "PASSWORD") {
                // Handle updating password
                let bl = new ewars.Blocker(null, "Saving changes...");
                ewars.tx('com.ewars.user.password.update', [this.state.edit.id, this.state.password])
                    .then(resp => {
                        bl.destroy();
                        ewars.growl("Password updated");
                        this.setState({
                            edit: null,
                            editMode: null,
                            password: ""
                        })
                    })
            }

            if (this.state.editMode == "USER") {
                if (this.state.edit.system) {
                    if (!this.state.edit.name || this.state.edit.name == "") {
                        ewars.growl("Please provide a valid name");
                        return;
                    }

                    if (!this.state.edit.email || this.state.edit.email == "") {
                        ewars.growl("Please provide a valid email");
                        return;
                    }

                    if (!this.state.edit.org_id || this.state.edit.org_id == "") {
                        ewars.growl("Please provide a valid organization");
                        return;
                    }
                }

                if (!this.state.edit.role || this.state.edit.role == "") {
                    ewars.growl("Please provide a valid role");
                    return;
                }

                if (!this.state.edit.status || this.state.edit.status == "") {
                    ewars.growl("Please provide a valid status");
                    return;
                }

                if (this.state.edit.role == "REGIONAL_ADMIN") {
                    if (!this.state.edit.location_id || this.state.edit.location_id == "") {
                        ewars.growl("Please provide a valid location for this Geogrpahic Administrator");
                        return;
                    }
                }


                // Handle create or update a users details
                if (this.state.edit.id) {
                    let bl = new ewars.Blocker(null, "Savings changes...");
                    ewars.tx("com.ewars.user.update", [this.state.edit.id, this.state.edit])
                        .then(resp => {
                            bl.destroy();
                            this.setState({
                                edit: null,
                                editMode: null,
                                paused: false
                            });
                            ewars.growl("Settings updated");
                            ewars.emit("RELOAD_DT");
                        })
                } else {
                    let bl = new ewars.Blocker(null, "Creating user...");
                    ewars.tx("com.ewars.user.create", [this.state.edit])
                        .then((resp) => {
                            bl.destroy();
                            if (resp.err) {
                                this.setState({
                                    createError: resp
                                })
                            } else {
                                this.setState({
                                    edit: resp
                                })
                            }
                        })
                }

            }
        }
    };

    _create = () => {
        this.setState({
            editMode: "ADD_USER",
            edit: {
                aid: window.user.user_type == "SUPER_ADMIN" ? null : window.user.account_id,
                created_by: window.user.id,
                profile: {}
            }
        })
    };

    _onChange = (prop, value) => {
        if (prop == "password" & this.state.edit.id != null) {
            this.setState({
                password: value
            })
        } else {
            if (prop.indexOf(".") >= 0) {
                let edited = this.state.edit;
                ewars.setKeyPath(edited, prop, value);
                this.setState({
                    edit: edited
                })
            } else {
                this.setState({
                    edit: {
                        ...this.state.edit,
                        [prop]: value
                    }
                })
            }
        }
    };

    _revoke = (user) => {

        ewars.prompt("fa-ban",
            __("REVOKE_USER_ACCESS_T"),
            __("REVOKE_USER_ACCESS_P"),
            function () {
                let bl = new ewars.Blocker(null, "Revoking user access...");
                ewars.tx("com.ewars.user.revoke", [user.id])
                    .then(function (resp) {
                        bl.destroy();
                        ewars.emit("RELOAD_DT");
                        ewars.growl("User access revoked");
                    }.bind(this))
            }.bind(this))
    };

    _changePassword = () => {
        let bl = new ewars.Blocker(null, __("UPDATING_PASSWORD"));

        ewars.tx("com.ewars.user.password.update", [this.state.edit.id, this.state.password])
            .then((resp) => {

            });
    };

    render() {
        let filter = {
            status: {in: ["ACTIVE", "INACTIVE", "REVOKED"]},
            role: {neq: "BOT"}
        };

        let editor, modalTitle, modalIcon;
        if (this.state.editMode) {
            let edit = EDITORS[this.state.editMode];
            let Cmp = edit.Cmp;
            modalTitle = __(edit.title);
            modalIcon = edit.icon;

            let value = this.state.edit;
            let user;
            if (this.state.editMode == "PASSWORD") {
                value = this.state.password;
                user = this.state.edit;
            }

            editor = <Cmp
                error={this.state.createError}
                updateUser={this._updateUser}
                close={this._closeModal}
                user={user}
                onChange={this._onChange}
                data={value}/>
        }


        let M_ACTIONS = MODAL_ACTIONS;
        if (this.state.editMode == "ASSIGNMENTS") {
            M_ACTIONS = ASSIGN_ACTIONS;
        }
        if (this.state.editMode == "ADD_USER") M_ACTIONS = [];

        let paused = false;
        if (this.state.editMode) paused = true;

        /*
         *
                    <ewars.d.Cell width="34px" style={{position: "relative", overflow: "hidden"}} borderRight={true}>
                        <div className="ide-tabs">
                            <div className="ide-tab ide-tab-down"><i className="fal fa-users"></i>&nbsp;Users</div>
                            <div className="ide-tab"><i className="fal fa-clipboard"></i>&nbsp;Assignments</div>
                            <div className="ide-tab"><i className="fal fa-hospital"></i>&nbsp;Organizations</div>
                            <div className="ide-tab"><i className="fal fa-user"></i>&nbsp;Roles</div>
                        </div>
                    </ewars.d.Cell>
        */

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Toolbar
                            label="Users">

                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Create User"
                                    icon="fa-plus"
                                    onClick={this._create}/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <DataTable
                                    resource="user"
                                    paused={this.state.paused}
                                    actions={GRID_ACTIONS}
                                    columns={COLUMNS}
                                    order={{registered: "DESC"}}
                                    initialOrder={{registered: "DESC"}}
                                    id="USERS"
                                    onCellAction={this._onAction}
                                    filter={filter}/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <ewars.d.Shade
                            title={modalTitle}
                            icon={modalIcon}
                            actions={M_ACTIONS}
                            onAction={this._onModalAction}
                            shown={this.state.editMode}>
                            {editor}
                        </ewars.d.Shade>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}


export {
    Component as UserManager
}

export default Component;

