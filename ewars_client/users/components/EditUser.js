import {
    TextField,
    TextAreaField,
    SelectField,
    LocationField as LocationSelectField
} from "../../common/fields/";

class EditUser extends React.Component {
    constructor(props) {
        super(props)

    }

    _updateUser = () => {

    };

    render() {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                let isDisabled = false;
                if (this.props.data.id) isDisabled = true;
                return (
                <div className="ide-settings-panel">
                    <div className="ide-settings-content">
                        <div className="ide-settings-form">
                            {this.props.error}

                            <div className="form light">
                                <div className="form-field header">
                                    <div className="label"><label>User</label></div>
                                    <div className="field-wrapper">
                                        <ewars.d.Row>
                                            <ewars.d.Cell>
                                                <div className="form-header style-title">User</div>
                                            </ewars.d.Cell>
                                        </ewars.d.Row>
                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="email">Email *</label></div>
                                    <div className="field-wrapper">
                                        <input
                                            type="text"
                                            name="email"
                                            disabled={isDisabled}
                                            value={this.props.data.email}
                                            onChange={this._onFieldChange}/>
                                    </div>
                                </div>


                                <div className="form-field">
                                    <div className="label"><label htmlFor="name">Name *</label></div>
                                    <div className="field-wrapper">
                                        <input
                                            type="text"
                                            name="name"
                                            disabled={isDisabled}
                                            value={this.props.data.name}
                                            onChange={this._onFieldChange}/>
                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="organization">Organization *</label></div>
                                    <div className="field-wrapper">

                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="phone">Phone</label></div>
                                    <div className="field-wrapper">
                                        <input
                                            type="text"
                                            name="phone"
                                            disabled={isDisabled}
                                            value={this.props.data.phone}
                                            onChange={this._onFieldChange}/>
                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="occupation">Occupation</label></div>
                                    <div className="field-wrapper">
                                        <input
                                            type="text"
                                            name="profile.occupation"
                                            disabled={isDisabled}
                                            value={this.props.data.profile.occupation}
                                            onChange={this._onFieldChange}/>
                                    </div>
                                </div>

                                <div className="form-field header">
                                    <div className="label"><label>Account Access</label></div>
                                    <div className="field-wrapper">
                                        <ewars.d.Row>
                                            <ewars.d.Cell>
                                                <div className="form-header style-title">Account Access</div>
                                            </ewars.d.Cell>
                                        </ewars.d.Row>
                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="role">Role *</label></div>
                                    <div className="field-wrapper">
                                    </div>
                                </div>

                                <div className="form-field">
                                    <div className="label"><label htmlFor="status">Status *</label></div>
                                    <div className="field-wrapper">
                                    </div>
                                </div>

                                {this.props.data.role == "REGIONAL_ADMIN" ?
                                    <div className="form-field">
                                        <div className="label"><label htmlFor="location">Location *</label></div>
                                        <div className="field-wrapper">

                                        </div>
                                    </div>
                                    : null}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default EditUser;
