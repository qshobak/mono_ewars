const CONSTANTS = require("../../../common/constants");
const Button = require("../../../common/components/ButtonComponent.react");


class Node extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false,
            isLoaded: false,
            users: null,
            location: null,
            data: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    expand = () => {
        if (!this.state.isLoaded) {
            if (this.props.type == "ACCOUNT") {
                this.setState({
                    isExpanded: !this.state.isExpanded,
                    isLoaded: true
                })
            }
            if (this.props.type == "ADMINS") {
                this.loadAdmins();
            }
            return;
        }

        this.setState({
            isExpanded: !this.state.isExpanded
        })
    };

    loadAdmins() {
        ewars.connect().then(function (session) {
            session.call("com.ewars.org.action", ["ADMINS", this.props.data.account_id, "ACCOUNT", null])
                .then(function (resp) {
                    this.state.data = resp;
                    this.state.isLoaded = true;
                    if (this._isMounted) this.forceUpdate();
                }.bind(this))
        }.bind(this))
    }

    render() {
        let name = ewars.I18N(this.props.data.name);

        if (this.props.type == "USER") {
            name = <span><i className="fal fa-user"></i>&nbsp;{this.props.data.name}
                [{this.props.data.email}] - {this.props.data.user_type}</span>;
        }

        let canExpand = false;
        if (this.props.type == "ACCOUNT") canExpand = true;
        if (this.props.type == "ADMINS") canExpand = true;
        if (this.props.type == "LOCATIONS") canExpand = true;
        if (this.props.type == "FORMS") canExpand = true;

        let iconClass = "fa fa-caret-right";
        if (this.state.isExpanded) iconClass = "fa fa-caret-down";
        if (this.state.isExpanded && !this.state.isLoaded) iconClass = "fa fa-spin fa-spinner";

        let nodes;

        if (this.props.type == "ACCOUNT") {
            nodes = [];
            let adminNode = {name: "Account Administrators", type: "ADMINS", account_id: this.props.data.id};
            nodes.push(<Node data={adminNode} type="ADMINS"/>);
            let locationsNode = {name: "By Location", type: "LOCATIONS", account_id: this.props.data.id};
            nodes.push(<Node data={locationsNode} type="LOCATIONS"/>);
            let formsNode = {name: "By Form", type: "FORMS", account_id: this.props.data.id};
            nodes.push(<Node data={formsNode} type="FORMS"/>);
        }

        if (this.props.type == "ADMINS" && this.state.isLoaded) {
            nodes = this.state.data.map(function (item) {
                return <Node data={item} type="USER"/>;
            }.bind(this))
        }

        return (
            <div className="org-node">
                <div className="ide-row header">
                    {canExpand ?
                        <div className="ide-col" onClick={this.expand}
                             style={{maxWidth: 25, textAlign: "center", borderRight: "1px solid #CCC"}}>
                            <i className={iconClass}></i>
                        </div>
                        : null}
                    <div className="ide-col">{name}</div>
                    {this.props.type == "USER" ?
                        <div className="ide-col" style={{flexBasis: 0, flexGrow: 1}}>
                            <div className="btn-group pull-right" style={{margin: 0}}>
                                <Button
                                    icon="fa-pencil"/>
                                <Button
                                    colour="red"
                                    icon="fa-trash"/>
                            </div>
                        </div>
                        : null}
                </div>
                {this.state.isExpanded ?
                    <div className="org-children">
                        {nodes}
                    </div>
                    : null}
            </div>
        )
    }

}

class Org extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            data: []
        };

        this.query();
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    query() {
        let filter = {status: {eq: "ACTIVE"}};

        ewars.connect().then(function (session) {
            session.call("com.ewars.query", ["account", ["id", "name"], filter, null, null, null, null])
                .then(function (resp) {
                    this.state.data = resp;
                    if (this._isMounted) this.forceUpdate();
                }.bind(this))
        }.bind(this))
    }

    render() {
        let nodes = this.state.data.map(function (item) {
            return <Node
                type="ACCOUNT"
                types={[CONSTANTS.ACCOUNT_ADMIN, CONSTANTS.REGIONAL_ADMIN]}
                data={item}/>
        }.bind(this));

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                {nodes}
            </div>
        )
    }
}

export default Org;