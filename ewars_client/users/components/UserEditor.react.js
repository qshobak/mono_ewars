import Form from "../../common/c.form";
import Spinner from "../../common/c.spinner";
import Button from "../../common/c.button";

import {
    TextField,
    TextAreaField,
    SelectField,
    LocationField as LocationSelectField
} from "../../common/fields";

import {ROLES, STATUS} from "../constants/fields";

const ORG_CONFIG = {
    optionsSource: {
        resource: "organization",
        valSource: "uuid",
        labelSource: "name",
        query: {}
    }
};

class UserSSO extends React.Component {
    render() {
        return (
            <div style={ADD_STYLE}>
                <p>A user already exists within EWARS with this email address, would you like to grant them access to
                    your account?</p>
                <div className="clearer"></div>
                <div className="btn-group pull-right">
                    <ewars.d.Button
                        icon="fa-check"
                        label="Invite User"
                        onClick={this.props.grant}/>
                </div>
                <div className="clearer"></div>
            </div>
        )
    }
}


class UserEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            view: "INVITE"
        }
    }

    _onChange = (prop, value) => {
        this.props.onChange(prop, value)
    };

    _onFieldChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    render() {
        let isDisabled = false;
        if (this.props.data.id) isDisabled = true;
        if (this.props.data.id && this.props.data.system) isDisabled = false;

        let occupation = "";
        if (this.props.data.profile) {
            if (this.props.data.profile.occupation) occupation = this.props.data.profile.occupation;
        }

        return (
            <div className="widget">
                <div className="body no-pad">
                    {isDisabled ?
                        <p className="warning" style={{padding: 8, margin: 8}}><i className="fal fa-exclamation-triangle"></i>&nbsp;This user is not controlled by your account, you can not edit their personal details, only their access details for your account.</p>
                    : null}

                    <div className="ide-settings-panel">
                        <div className="ide-settings-content">
                            <div className="ide-settings-form">
                                {this.props.error}

                                <div className="form light">
                                    <div className="form-field header">
                                        <div className="label"><label>User</label></div>
                                        <div className="field-wrapper">
                                            <ewars.d.Row>
                                                <ewars.d.Cell>
                                                    <div className="form-header style-title">User</div>
                                                </ewars.d.Cell>
                                            </ewars.d.Row>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="email">Email *</label></div>
                                        <div className="field-wrapper">
                                            <input
                                                type="text"
                                                name="email"
                                                disabled={isDisabled}
                                                value={this.props.data.email}
                                                onChange={this._onFieldChange}/>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="name">Name *</label></div>
                                        <div className="field-wrapper">
                                            <input
                                                type="text"
                                                name="name"
                                                disabled={isDisabled}
                                                value={this.props.data.name}
                                                onChange={this._onFieldChange}/>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="organization">Organization *</label>
                                        </div>
                                        <div className="field-wrapper">
                                            <SelectField
                                                value={this.props.data.org_id}
                                                name="org_id"
                                                readOnly={isDisabled}
                                                config={ORG_CONFIG}
                                                onUpdate={this._onChange}/>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="phone">Phone</label></div>
                                        <div className="field-wrapper">
                                            <input
                                                type="text"
                                                name="phone"
                                                disabled={isDisabled}
                                                value={this.props.data.phone}
                                                onChange={this._onFieldChange}/>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="occupation">Occupation</label></div>
                                        <div className="field-wrapper">
                                            <input
                                                type="text"
                                                name="profile.occupation"
                                                disabled={isDisabled}
                                                value={occupation}
                                                onChange={this._onFieldChange}/>
                                        </div>
                                    </div>

                                    <div className="form-field header">
                                        <div className="label"><label>Account Access</label></div>
                                        <div className="field-wrapper">
                                            <ewars.d.Row>
                                                <ewars.d.Cell>
                                                    <div className="form-header style-title">Account Access</div>
                                                </ewars.d.Cell>
                                            </ewars.d.Row>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="role">Role *</label></div>
                                        <div className="field-wrapper">
                                            <SelectField
                                                name="role"
                                                value={this.props.data.role}
                                                onUpdate={this._onChange}
                                                config={{options: ROLES}}/>
                                        </div>
                                    </div>

                                    <div className="form-field">
                                        <div className="label"><label htmlFor="status">Status *</label></div>
                                        <div className="field-wrapper">
                                            <SelectField
                                                name="status"
                                                value={this.props.data.status}
                                                onUpdate={this._onChange}
                                                config={{options: STATUS}}/>
                                        </div>
                                    </div>

                                    {this.props.data.role == "REGIONAL_ADMIN" ?
                                        <div className="form-field">
                                            <div className="label"><label htmlFor="location">Location *</label></div>
                                            <div className="field-wrapper">
                                                <LocationSelectField
                                                    name="location_id"
                                                    value={this.props.data.location_id}
                                                    onUpdate={this._onChange}/>
                                            </div>
                                        </div>
                                        : null}
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )

    }
}

export default UserEditor;
