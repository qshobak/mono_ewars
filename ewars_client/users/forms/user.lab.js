
module.exports = {
    name: {
        order: 0,
        label: "Name",
        type: "string",
        required: true,
        help: "Enter your full name",
        placeholder: ""
    },
    email: {
        order: 1,
        label: "Email Address",
        type: "string",
        required: true,
        help: "Enter a valid email address for the user"
    },
    confirm_email: {
        order: 2,
        label: "Confirm Email Address",
        type: "text",
        required: true,
        help: "Enter the email address"
    },
    password: {
        order: 3,
        type: "password",
        label: {
            en: "Password"
        },
        required: true
    },
    confirm_password: {
        order: 4,
        type: "password",
        label: {
            en: "Confirm Password"
        },
        required: true
    },
    language: {
        order: 5,
        label: "Default Language",
        type: "select",
        required: true,
        options: [
            ["en", "English"],
            ["fr", "French"]
        ]
    },
    lab_id: {
        type: "select",
        label: {
            en: "Laboratory"
        },
        required: true,
        optionsSource: {
            resource: "Laboratory",
            query: {
                $filter: [
                    "status eq 'ACTIVE'"
                ]
            },
            valSource: "uuid",
            labelSource: "name"
        }
    },
    send_email: {
        order: 8,
        label: {
            en: "Send email to user with email address and password?"
        },
        type: "select",
        required: true,
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    }
};
