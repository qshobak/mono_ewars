module.exports = {
    name: {
        order: 0,
        label: "Name",
        type: "string",
        required: true,
        help: "Enter your full name",
        placeholder: ""
    },
    email: {
        order: 1,
        label: "Email Address",
        type: "string",
        required: true,
        help: "Enter a valid email address for the user",
        placeholder: "john@smith.com"
    },
    language: {
        order: 2,
        label: "Default Language",
        type: "select",
        required: true,
        options: [
            ["en", "English"],
            ["fr", "French"]
        ]
    },
    password: {
        order: 3,
        label: "Password",
        type: "text"
    },
    confirm_password: {
        order: 4,
        label: "Confirm Password",
        type: "text"
    },
    account_id: {
        order: 3,
        type: "select",
        label: "Account",
        required: true,
        optionsSource: {
            resource: "Account",
            query: {},
            valSource: "id",
            labelSource: "name"
        }
    },
    org_id: {
        order: 4,
        label: "Organization",
        type: "select",
        required: true,
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name"
        }
    },
    group_id: {
        order: 4,
        label: "User Group",
        type: "select",
        required: true,
        options: [
            [2, 'Global Administrator'],
            [3, 'Global User'],
            [5, 'Country Administrator'],
            [6, 'Country User']
        ]
    },
    phone: {
        order: 5,
        label: "Mobile Phone #",
        type: "string",
        required: true
    }
};