export default [
    {
        name: "name",
        width: 250,
        config: {
            label: "Name",
            type: "text"
        }
    },
    {
        name: "email",
        width: 250,
        config: {
            label: "Email",
            type: "text"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", _l("ACTIVE")],
                ["INACTIVE", _l("INACTIVE")],
                ["REVOKED", _l("Access Revoked")]
            ]
        }
    },
    {
        name: "location_name",
        width: 250,
        config: {
            label: "Location",
            type: "location",
            filterKey: "location_name"
        }
    },
    {
        name: "role",
        config: {
            label: "User Type",
            type: "select",
            options: [
                ["SUPER_ADMIN", _l("SUPER_ADMIN")],
                ["USER", _l("REPORTING_USER")],
                ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")],
                ["REGIONAL_ADMIN", _l("GEOGRAPHIC_ADMIN")]
            ]
        }
    },
    {
        name: "org_name",
        width: 300,
        config: {
            label: "Organization",
            type: "select",
            optionsSource: {
                resource: "organization",
                valSource: "uuid",
                labelSource: "name",
                query: {}
            },
            filterKey: "org_id"
        }
    },
    {
        name: "registered",
        config: {
            label: "Registered",
            type: "date"
        },
        fmt: ewars.DATE
    },
    {
        name: "language",
        config: {
            label: "Language",
            type: "select",
            options: [
                ["en", "English (en)"],
                ["fr", "French (fr)"]
            ]
        }
    },
    {
        name: "system",
        config: {
            label: "System User",
            type: "select",
            options: [
                [true, "Yes"],
                [false, "No"]
            ]
        }
    },
    {
        name: "phone",
        config: {
            label: "Phone",
            type: "text"
        }
    }
]
