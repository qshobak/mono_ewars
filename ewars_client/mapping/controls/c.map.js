import mapboxgl from 'mapbox-gl';

import d3 from "d3";
import turf from 'turf';

const _validateLayer = (layer) => {
    return true;
};

const _isValidGeom = (geom) => {
    if (geom == null) return false;
    if (geom == undefined) return false;
    if (geom == "") return false;
    if (geom.features == null) return false;
    return true;
};

const _isValidPoint = (data) => {
    if (data == null) return false;
    if (data == undefined) return false;
    if (data == "") return false;
    if (data.coordinates == null) return false;
    return true;
};

const DATA_FIELDS = [
    "type",
    "source",
    "ind_id",
    "lid",
    "sti",
    "group",
    "formula"
];

class Map {
    constructor(config, el) {
        this._config = config;
        this._el = el;

        this._root = null;
        this._layers = {};
        this._layerData = {};
        this._mapbox = null;
        this._mounted = false;

        ewars.subscribe("UPDATE_MAP", this.update);
        ewars.subscribe("CHANGE_MAP", this._rebuild);
    }

    mount = () => {
        this._mounted = true;
        mapboxgl.accessToken = "pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg";

        let options = {
            container: this._el,
            style: "mapbox://styles/jduren/cj95cps6g154x2rmnu6xphlya",
            center: [-0.1, 51.5119112],
            zoom: 0
        }

        if (this._config.config.center) {
            options.zoom = this._config.config.center.zoom;
            options.center = [
                this._config.config.center.lng,
                this._config.config.center.lat
            ]
        }

        this._mapbox = new mapboxgl.Map(options);
        this._mapbox.addControl(new mapboxgl.NavigationControl({}));

        this._mapbox.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true
        }));


        let self = this;
        this._mapbox.on("load", () => {

            let _canvas = this._mapbox.getCanvasContainer();
            this._metaContainer = document.createElement("div");
            this._metaContainer.setAttribute("class", "map-meta-container");
            _canvas.appendChild(this._metaContainer);
            this._root = d3.select(_canvas).append("svg");

            this.transform = d3.geoTransform({
                point: function (lon, lat) {
                    let point = self._mapbox.project(new mapboxgl.LngLat(lon, lat));
                    this.stream.point(point.x, point.y);
                }
            });
            this.path = d3.geoPath();
            this.path.projection(this.transform);

            this._mapbox.on("viewreset", this._updateRender);
            this._mapbox.on("movestart", function () {
                self._root.attr("class", "hidden");
            })
            this._mapbox.on("rotate", function () {
                self._root.attr("class", "hidden");
            });
            this._mapbox.on("moveend", function () {
                self._updateRender();
                self._root.attr("class", "");

                ewars.emit("POS_UPDATED", [self._mapbox.getZoom(), self._mapbox.getCenter()]);
            })

            if (this._config.layers && this._config.layers.length > 0) {
                this._config.layers.forEach(item => {
                    this._addLayer(item);
                })
            }

        })

    };

    _checkComplete = () => {

    };

    _updateRender = () => {
        let self = this;
        for (let i in this._layers) {
            if (this._layers[i].config.source != "NONE" && this._layers[i].config.type != "FORM_POINT") {
                this._layers[i]._el_chloros
                    .selectAll("path")
                    .attr("d", function (datum) {
                        console.log(datum);
                        return self.path(datum.location.geometry);
                    })

                this._layers[i]._el_points
                    .selectAll("circle")
                    .attr("cx", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.location.geometry.coordinates[0], d.location.geometry.coordinates[1])).x;
                    })
                    .attr("cy", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.location.geometry.coordinates[0], d.location.geometry.coordinates[1])).y;
                    })
            } else if (this._layers[i].config.type == "FORM_POINT") {
                this._layers[i]._el_points
                    .selectAll("circle")
                    .attr("cx", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.geom.coordinates[0], d.geom.coordinates[1])).x;
                    })
                    .attr("cy", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.geom.coordinates[0], d.geom.coordinates[1])).y;
                    })
            } else {
                this._layers[i]._el_chloros
                    .selectAll("path")
                    .attr("d", function (datum) {
                        return self.path(datum.geojson);
                    })

                this._layers[i]._el_points
                    .selectAll("circle")
                    .attr("cx", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.geometry.coordinates[0], d.geometry.coordinates[1])).x;
                    })
                    .attr("cy", function (d) {
                        return self._mapbox.project(new mapboxgl.LngLat(d.geometry.coordinates[0], d.geometry.coordinates[1])).y;
                    })
            }
        }
    };

    _addLayer = (layer) => {
        let dataChanged = false;
        if (!this._layers[layer.uuid]) {
            let layerRoot = this._root.append("g");
            let chloroSect = layerRoot.append("g");
            let pointSect = layerRoot.append("g");
            let labelSect = layerRoot.append('g');

            let metabox = document.createElement("div");
            metabox.setAttribute("class", "meta-container");
            this._metaContainer.appendChild(metabox);

            this._layers[layer.uuid] = {
                elem: layerRoot,
                config: layer,
                _el_chloros: chloroSect,
                _el_points: pointSect,
                _el_labels: labelSect,
                metabox: metabox
            }
        } else {
            this._layers[layer.uuid].config = layer;
        }

        if (layer.source == "NONE" && layer.type != "FORM_POINT") {
            ewars.tx("com.ewars.mapping.loc_geoms", [{
                type: layer.type,
                lid: layer.lid,
                loc_type: layer.loc_type
            }])
                .then(res => {
                    this._populateStaticLayer(layer.uuid, res);
                })
                .catch(err => {
                    this._layerLoadCount++;
                    this._checkComplete();
                });
            // We're not querying for data so don't carry on
            return;
        }

        if (layer.type == "FORM_POINT") {
            ewars.tx('com.ewars.mapping.form_points', [{
                field: layer.form_field
            }])
                .then(res => {
                    this._populateFormPoint(layer.uuid, res);
                })
                .catch(err => {
                    this._layerLoadCount++;
                    this._checkComplete();
                })
            // We're note querying for data, so don't continue on
            return;
        }

        let query = {
            reduction: "SUM",
            interval: "DAY",
            location: null,
            tid: this._tid,
            geometry: true,
            centroid: true
        };

        if (layer.type == "TYPE") {
            query.location = {
                parent_id: layer.lid,
                site_type_id: layer.loc_type,
                status: "ACTIVE"
            };
        } else if (layer.type == "GROUP") {
            query.location = {
                groups: [layer.group],
                agg: "INDIVIDUAL"
            }
        } else if (layer.type == "SINGLE") {
            query.location = layer.lid;
        } else if (layer.type == "FORM_POINT") {
        }

        if (layer.source == "INDICATOR") {

            query.type = 'SLICE';
            query.indicator = layer.ind_id;
            query.start_date = layer.period[0];
            query.end_date = layer.period[1];

        } else if (layer.source == "COMPLEX") {

        } else if (layer.source == "NONE") {

        }

        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                this._populateLayerData(layer.uuid, resp);
            })
            .catch(err => {
                console.log(err);
                ewars.error("Error loading layer data");
            })

    };

    _populateFormPoint = (lid, res) => {
        let _layer = this._layers[lid];
        this._layerData[lid] = res;
        let validPointLocs = res.d.filter(item => {
            return item.point != null && item.point != "";
        })

        validPointLocs = validPointLocs.map(item => {
            let coords = [
                item.point[1],
                item.point[0]
            ]
            return {
                ...item,
                geom: {
                    type: "Point",
                    coordinates: coords
                }
            }
        });

        let self = this;

        _layer._el_points.selectAll("circle")
            .data(validPointLocs)
            .enter()
            .append("circle")
            .attr("r", parseFloat(_layer.config.marker_size) || 10)
            .attr("cx", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(parseFloat(d.geom.coordinates[0]), parseFloat(d.geom.coordinates[1]))).x;
            })
            .attr("cy", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(parseFloat(d.geom.coordinates[0]), parseFloat(d.geom.coordinates[1]))).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", _layer.config.stroke_width || "0");


        _layer.metabox.innerHTML = "";
        let mTitle = document.createElement("div");
        mTitle.setAttribute("class", "metabox-title");
        mTitle.appendChild(document.createTextNode(_layer.config.title || "New layer"));

        let mTable = document.createElement("div");
        mTable.setAttribute("class", "metabox-table");
        mTable.setAttribute("width", "100%");
        let missingRow = document.createElement("div");
        missingRow.setAttribute("class", "metabox-table-row");
        missingRow.appendChild(document.createTextNode("Missing: " + res.missing));

        let foundRow = document.createElement("div");
        foundRow.setAttribute("class", "metabox-table-row");
        foundRow.appendChild(document.createTextNode("Found: " + res.valid));

        mTable.appendChild(missingRow);
        mTable.appendChild(foundRow);

        _layer.metabox.appendChild(mTitle);
        _layer.metabox.appendChild(mTable);




        this._updateRender();
    };

    _drawFormPointCount = () => {

    };

    _populateStaticLayer = (lid, res) => {
        let _layer = this._layers[lid];
        this._layerData[lid] = res;

        let validPoints = [],
            validGeoms = [];

        res.forEach(item => {
            if (item.geometry_type == "ADMIN") {
                if (_isValidGeom(item.geojson)) validGeoms.push(item);
            } else {
                if (_isValidPoint(item.geojson)) validPoints.push(item);
            }
        });

        let self = this;

        _layer._el_chloros.selectAll("path")
            .data(validGeoms)
            .enter()
            .append("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", _layer.config.fill_color || "red")
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", _layer.config.stroke_width || 0)
            .attr("d", function (datum) {
                return self.path(datum.geojson);
            })
            .attr("id", function (d) {
                return d.uuid;
            });

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .data(validPoints)
            .enter()
            .append(_layer.config.marker_type || "circle")
            .attr("r", _layer.config.marker_size || 10)
            .attr("cx", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(d.geom.coordinates[0], d.geom.coordinates[1])).x;
            })
            .attr("cy", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(d.geom.coordinates[0], d.geom.coordinates[1])).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", _layer.config.stroke_width || "0");

    };

    _updateLayerStyleStatic = (lid) => {
        let _layer = this._layers[lid];

        let self = this;

        _layer._el_chloros.selectAll("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", _layer.config.fill_color || "red")
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", _layer.config.stroke_width || 0);

        _layer._el_points.selectAll("circle")
            .attr("r", _layer.config.marker_size || 10)
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", _layer.config.stroke_width || "0");


    };

    _updateLayerStyleNonStatic = (lid) => {
        let _layer = this._layers[lid];
        let self = this;

        _layer._el_chloros.selectAll("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", function (d) {
                return self._getColour(parseFloat(d.data), null, null, _layer.config.scales || []);
            })
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", _layer.config.stroke_width || 0);

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .attr("r", _layer.config.marker_size)
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", _layer.config.stroke_width || "0");
    };

    _populateLayerData = (lid, res) => {
        if (res.err) {
            if (res.code == "INDICATOR_MISSING") {
                ewars.error("A layer is missing an indicator specification.");
            }
        }

        let _layer = this._layers[lid];
        this._layerData[lid] = res;

        let validPoints = [],
            validGeoms = [];

        res.d.forEach(item => {
            if (item.location.geometry_type == "ADMIN") {
                if (_isValidGeom(item.location.geometry)) validGeoms.push(item);
            } else {
                if (_isValidPoint(item.location.geometry)) validPoints.push(item);
            }

        })

        let self = this;

        _layer._el_chloros.selectAll("path")
            .data(validGeoms)
            .enter()
            .append("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", function (d) {
                return self._getColour(parseFloat(d.data), null, null, _layer.config.scales || []);
            })
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", _layer.config.stroke_width || 0)
            .attr("d", function (datum) {
                return self.path(datum.location.geometry);
            })
            .attr("id", function (d) {
                return d.location.uuid;
            });

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .data(validPoints)
            .enter()
            .append(_layer.config.marker_type || "circle")
            .attr("r", _layer.config.marker_size || 10)
            .attr("cx", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(d.location.geometry.coordinates[0], d.location.geometry.coordinates[1])).x;
            })
            .attr("cy", function (d) {
                return self._mapbox.project(new mapboxgl.LngLat(d.location.geometry.coordinates[0], d.location.geometry.coordinates[1])).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", _layer.config.stroke_width || "0");

    };

    _projectPoint = (lon, lat) => {
        var point = this._mapbox / project(new mapboxgl.LngLat(lon, lat));
        this.stream.point(point.x, point.y);
    };

    getD3 = () => {
        let bbox = this._el.getBoundingClientRect();
        let center = this._mapbox.getCenter();
        let zoom = this._mapbox.getZoom();
        let scale = (512) * 0.5 / Math.PI & Math.pow(2, zoom);

        let d3Projection = d3.geoMercator()
            .center([center.lng, center.lat])
            .translate([bbox.width / 2, bbox.height / 2])
            .scale(scale);

        return d3Projection;
    };

    unmount() {
        ewars.unsubscribe("UPDATE_MAP", this.update);

    }

    _removeLayer = (lid) => {
        this._layers[lid].elem.selectAll("g").remove();
        delete this._layers[lid];
        delete this._layerData[lid];
    };

    update = (config) => {
        this._config = config[0];
        let updatedLayer = config[1][0] || null;

        if (!this._mounted) {
            this.mount();
        } else {
            if (updatedLayer) {
                let newLayer = config[0].layers.filter(item => {
                    return item.uuid == updatedLayer;
                })[0] || null;


                let origLayer = this._layers[newLayer.uuid];

                let hasDataChanged = false;
                DATA_FIELDS.forEach(field => {
                    if (newLayer[field] != origLayer.config[field]) hasDataChanged = true;
                });


                if (hasDataChanged || !origLayer) {
                    this._addLayer(newLayer);
                } else {
                    if (newLayer.type == "FORM_POINT") {
                        this._layers[newLayer.uuid].config = newLayer;
                        this._updateLayerStyleStatic(newLayer.uuid);
                    } else {
                        if (newLayer.source == "NONE") {
                            this._layers[newLayer.uuid].config = newLayer;
                            this._updateLayerStyleStatic(newLayer.uuid);
                        } else if (newLayer.source == "INDICATOR" || newLayer.source == "COMPLEX") {
                            this._layers[newLayer.uuid].config = newLayer;
                            this._updateLayerStyleNonStatic(newLayer.uuid);
                        }
                    }
                }
            } else {
                // Remove layers that were removed from config
                for (let uid in this._layers) {
                    let layer = config[0].layers.filter(item => {
                        return item.uuid == uid;
                    })[0] || null;

                    if (!layer) {
                        this._removeLayer(uid);
                    }
                }
            }
        }

        this._root.attr("class", "");
    };

    _rebuild = (config) => {
        this._config = config;

        this._root.selectAll("g").remove();
        this._layerData = {};
        this._layers = {};
        this._metaContainer.innerHTML = "";

        if (this._config.config.center) {
            this._mapbox.jumpTo({
                zoom: this._config.config.center.zoom,
                center: [
                    this._config.config.center.lng,
                    this._config.config.center.lat
                ]
            })
        }

        (this._config.layers || []).forEach(layer => {
            this._addLayer(layer);
        })

    };

    _getColour = (value, ot, oth, scales) => {
        if (!scales || scales.length <= 0) {
            return "rgb(0,0,0)";
        }
        let colour;
        scales.forEach(scale => {
            if (scale[1] === "INF") {
                if (value >= parseFloat(scale[0])) colour = scale;
            } else {
                if (value >= parseFloat(scale[0]) && value <= parseFloat(scale[1]))
                    colour = scale;
            }
        });

        if (!colour) return "rgb(0,0,0)";
        return colour[2];
    };
}

const STYLE = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
};

class MapControl extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this._el) {
            this._map = new Map(this.props.data || {}, this._el);
            this._map.mount();
        }
    }

    componentWillUnmount() {
        if (this._el) {
            this._map.unmount();
            this._map = null;
            this._el.innerHTML = "";
        }
    }

    componentWillReceiveProps(nextProps) {
    }


    render() {
        return (
            <div
                className="map-wrap-canvas"
                style={STYLE}
                ref={(el) => {
                    this._el = el;
                }}>

            </div>
        )
    }

}

export default MapControl;
