import L from 'leaflet';

import turf from 'turf';
import centroid from "@turf/centroid";
import clustersDbscan from '@turf/clusters-dbscan';

import RangeUtils from "../../common/utils/RangeUtils";

const _validateLayer = (layer) => {
    return true;
};

const _isValidGeom = (geom) => {
    if (geom == null) return false;
    if (geom == undefined) return false;
    if (geom == "") return false;
    if (geom.features == null) return false;
    return true;
};

const _isValidPoint = (data) => {
    if (data == null) return false;
    if (data == undefined) return false;
    if (data == "") return false;
    if (data.coordinates == null) return false;
    return true;
};

const DATA_FIELDS = [
    "type",
    "source",
    "ind_id",
    "lid",
    "sti",
    "group",
    "formula",
    "marker_type"
];

/**
 *
 *

 var map = new L.Map("map", {center: [37.8, -96.9], zoom: 4})
 .addLayer(new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"));

 var svg = d3.select(map.getPanes().overlayPane).append("svg"),
 g = svg.append("g").attr("class", "leaflet-zoom-hide");

 d3.json("us-states.json", function(error, collection) {
  if (error) throw error;

  var transform = d3.geo.transform({point: projectPoint}),
      path = d3.geo.path().projection(transform);

  var feature = g.selectAll("path")
      .data(collection.features)
    .enter().append("path");

  map.on("viewreset", reset);
  reset();

  // Reposition the SVG to cover the features.
  function reset() {
    var bounds = path.bounds(collection),
        topLeft = bounds[0],
        bottomRight = bounds[1];

    svg .attr("width", bottomRight[0] - topLeft[0])
        .attr("height", bottomRight[1] - topLeft[1])
        .style("left", topLeft[0] + "px")
        .style("top", topLeft[1] + "px");

    g   .attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

    feature.attr("d", path);
  }

  // Use Leaflet to implement a D3 geometric transformation.
  function projectPoint(x, y) {
    var point = map.latLngToLayerPoint(new L.LatLng(y, x));
    this.stream.point(point.x, point.y);
  }
});


 *
 *
 */

class Map {
    constructor(config, el) {
        this._config = config;
        this._el = el;

        this._el.style.width = this._el.parentElement.clientWidth + "px";
        this._el.style.height = this._el.parentElement.clientHeight + "px";

        this._root = null;
        this._layers = {};
        this._layerData = {};
        this._map = null;
        this._mounted = false;

        d3.select("head")
            .append("style").attr("type", "text/css")
            .text("g.d3-overlay *{pointer-events:visiblePainted !important;}");

        ewars.subscribe("UPDATE_MAP", this.update);
        ewars.subscribe("CHANGE_MAP", this._rebuild);

    }

    _undef = (a) => {
        return typeof a === "undefined";
    };

    mount = () => {
        this._mounted = true;
        var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
        this._map = L.map(this._el, {})
        this._map.zoomControl.setPosition('bottomright');

        if (this._config.config.center) {
            this._map.setView(
                [this._config.config.center.lat, this._config.config.center.lng],
                this._config.config.center.zoom);
        } else {
            this._map.setView(
                [0, 0],
                10
            );
        }
        var osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 20, attribution: osmAttrib});
        this._map.addLayer(osm);

        let self = this;

        this._svg = L.svg();
        this._map.addLayer(this._svg);
        this._root = d3.select(this._svg._rootGroup)
            .classed("d3-overlay", true);
        this._root.classed("leaflet-zoom-hide", true);

        // this._canvas = d3.select(this._map.getPanes().overlayPane).append("svg");
        // this._canvas.style("height", this._el.clientHeight)
        //     .style("width", this._el.clientWidth);
        // this._root = this._canvas.append("g").attr("class", "leaflet-zoom-hide");
        // this._metaContainer = document.createElement("div");
        // this._metaContainer.setAttribute("class", "map-meta-container");
        // _canvas.append(this._metaContainer);

        this._pixelOrigin = this._map.getPixelOrigin();
        this._wgsOrigin = L.latLng([0, 0]);
        this._wgsInitialShift = this._map.latLngToLayerPoint(this._wgsOrigin);
        this._zoom = this._map.getZoom();
        this._shift = L.point(0, 0);
        this._scale = 1;

        this.projection = {
            latLngToLayerPoint: function (latLng, zoom) {
                let zoomp = zoom ? zoom : self._zoom;
                let projectedPoint = self._map.project(L.latLng(latLng), zoomp)._round();
                return projectedPoint._subtract(self._pixelOrigin);
            },
            layerPointToLatLng: function (point, zoom) {
                let zoomc = zoom ? zoom : self._zoom;
                let projectedPoint = L.point(point).add(self._pixelOrigin);
                return self._map.unproject(projectedPoint, zoomc);
            },
            unitsPerMeter: 256 * Math.pow(2, self._zoom) / 40075017,
            map: this._map,
            layer: this,
            scale: 1
        };

        this.projection._projectPoint = function (x, y) {
            let point = self.projection.latLngToLayerPoint(new L.LatLng(y, x));
            this.stream.point(point.x, point.y);
        };

        this.transform = d3.geoTransform({
            point: this.projection._projectPoint
        });
        this.path = d3.geoPath();
        this.path.projection(this.transform);
        this.projection.pathFromGeojson = this.path;

        this.projection.latLngToLayerFloatPoint = this.projection.latLngToLayerPoint;
        this.projection.getZoom = this._map.getZoom.bind(this._map);
        this.projection.getBounds = this._map.getBounds.bind(this._map);

        this.rootInfo = L.control("topright");
        this.rootInfo.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'meta-container'); // create a div with a class "info"
            return this._div;
        };

        this.rootInfo.addTo(this._map);

        this.rootInfo.update = function(props) {
            let _html = "";

            _html = `
                    <div class="metabox-title">Records (${props.data.length})</div>
                    <div class="metabox-table overflow">`;

            props.data.forEach(item => {
                _html += `
                        <a href="#" class="map-meta-record" onClick="window.open('/reporting#?uuid=${item.properties.uuid}'); return false;">
                            <div class="form-name">${item.properties.form_name.en || item.properties.form_name}</div>
                            <div class="record-date">${item.properties.data_date}</div>
                            <div class="user">${item.properties.user_name} (${item.properties.user_email})</div>
                        </a>`;
            })

            _html += '</div></div>';

            this._div.innerHTML = _html;
        }

        // this.transform = d3.geoTransform({
        //     point: function (lon, lat) {
        //         let point = self._map.latLngToLayerPoint(new L.LatLng(lat, lon));
        //         this.stream.point(point.x, point.y);
        //     }
        // });
        // this.path = d3.geoPath();
        // this.path.projection(this.transform);

        // this._map.on("viewreset", this._updateRender);

        // this._leaflet_round = L.Point.prototype._round;
        L.Point.prototype._round = function () {
            return this;
        };
        // L.Point.prototype._round = function() { return this;};


        if (this._config.layers && this._config.layers.length > 0) {
            this._config.layers.forEach(item => {
                this._addLayer(item);
            })
        }

        this._map.on("zoomend", this._updateRender);
        this._map.on("moveend", this._updatePos);
        // this._updateRender({});
    };

    _reset = () => {

    };

    _updatePos = () => {
        ewars.emit("POS_UPDATED", [this._map.getZoom(), this._map.getCenter()])
    }

    _checkComplete = () => {

    };

    _updateRender = (evt) => {
        ewars.emit("POS_UPDATED", [this._map.getZoom(), this._map.getCenter()]);
        let self = this;

        let newZoom = this._undef(evt.zoom) ? this._map._zoom : evt.zoom; // "viewreset" event in Leaflet has not zoom/center parameters like zoomanim
        this._zoomDiff = newZoom - this._zoom;
        this._scale = Math.pow(2, this._zoomDiff);
        this.projection.scale = this._scale;
        this._shift = this._map.latLngToLayerPoint(this._wgsOrigin)
            ._subtract(this._wgsInitialShift.multiplyBy(this._scale));

        let shift = ["translate(", this._shift.x, ",", this._shift.y, ") "];
        let scale = ["scale(", this._scale, ",", this._scale, ") "];
        this._root.attr("transform", shift.concat(scale).join(""));

        for (let i in this._layers) {
            let _layer = this._layers[i];
            if (this._layers[i].config.source != "NONE" && this._layers[i].config.type != "FORM_POINT") {
                this._layers[i]._el_chloros
                    .selectAll("path")
                    .attr("d", function (datum) {
                        return self.projection.pathFromGeojson(datum.location.geometry);
                    })
                    .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

                this._layers[i]._el_points
                    .selectAll("circle")
                    .attr("cx", function (d) {
                        return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).x;
                    })
                    .attr("cy", function (d) {
                        return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).y;
                    })
                    .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
                    .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

            } else if (this._layers[i].config.type == "FORM_POINT") {
                // need to recaldate based on new zoom
                this._renderPoints(this._layerData[i], this._layers[i]);
            } else {
                this._layers[i]._el_chloros
                    .selectAll("path")
                    .attr("d", function (datum) {
                        return self.projection.pathFromGeojson(datum.geojson);
                    })
                    .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

                this._layers[i]._el_points
                    .selectAll("circle")
                    .attr("cx", function (d) {
                        return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).x;
                    })
                    .attr("cy", function (d) {
                        return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).y;
                    })
                    .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
                    .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);
            }
        }
    };

    // Get the distance between two points
    _getCoordsDistance = (point1, point2) => {
        var xs = 0;
        var ys = 0;

        xs = point2[0] - point1[0];
        xs = xs * xs;

        ys = point2[1] - point1[1];
        ys = ys * ys;

        return Math.sqrt(xs + ys);
    };

    _addLayer = (layer) => {
        let dataChanged = false;
        if (!this._layers[layer.uuid]) {
            let layerRoot = this._root.append("g");
            let chloroSect = layerRoot.append("g");
            let pointSect = layerRoot.append("g");
            let pointLabelSelect = layerRoot.append('g');
            let chloroLabelSelect = layerRoot.append('g');


            let info = L.control("topright");
            if (layer.type == "FORM_POINT") {

                info.onAdd = function (map) {
                    this._div = L.DomUtil.create('div', 'meta-container'); // create a div with a class "info"
                    return this._div;
                };

                info.addTo(this._map);
            }

            this._layers[layer.uuid] = {
                elem: layerRoot,
                config: layer,
                _el_chloros: chloroSect,
                _el_points: pointSect,
                _el_point_labels: pointLabelSelect,
                _el_chloro_labels: chloroLabelSelect,
                info: info
            }
        } else {
            this._layers[layer.uuid].config = layer;
        }

        let startDate,
            endDate;

        if (layer.period) {
            endDate = RangeUtils.process(layer.period[1], this._rpDate);
            startDate = RangeUtils.process(layer.period[0], this._rpDate, endDate);
        }

        if (layer.source == "NONE" && layer.type != "FORM_POINT") {

            ewars.tx("com.ewars.mapping.loc_geoms", [{
                type: layer.type,
                lid: layer.lid,
                loc_type: layer.loc_type
            }])
                .then(res => {
                    this._populateStaticLayer(layer.uuid, res);
                })
                .catch(err => {
                    this._layerLoadCount++;
                    this._checkComplete();
                });
            // We're not querying for data so don't carry on
            return;
        }

        if (layer.type == "FORM_POINT") {
            this._layers[layer.uuid].info.update = function (props) {
                this._div.innerHTML = `
                    <div class="metabox-title">${props.name || ""}</div>
                    <div class="metabox-table">
                        <div class="metabox-table-row">With GPS: ${props.found}</div>
                        <div class="metabox-table-row">Without GPS: ${props.missing}</div>
                    </div>
                `;
            };

            this._layers[layer.uuid].info.update({
                name: layer.title || "Unnamed Layer",
                found: 0,
                missing: 0
            });

            ewars.tx('com.ewars.mapping.form_points', [{
                field: layer.form_field
            }])
                .then(res => {
                    this._populateFormPoint(layer.uuid, res);
                })
                .catch(err => {
                    this._layerLoadCount++;
                    this._checkComplete();
                })
            // We're note querying for data, so don't continue on
            return;
        }

        let query = {
            reduction: "SUM",
            interval: "DAY",
            location: null,
            tid: this._tid,
            geometry: true,
            centroid: true
        };

        if (layer.type == "TYPE") {
            query.location = {
                parent_id: layer.lid,
                site_type_id: layer.loc_type,
                status: "ACTIVE"
            };
        } else if (layer.type == "GROUP") {
            query.location = {
                groups: [layer.group],
                agg: "INDIVIDUAL"
            }
        } else if (layer.type == "SINGLE") {
            query.location = layer.lid;
        }

        if (layer.source == "INDICATOR") {

            query.type = 'SLICE';
            query.indicator = layer.ind_id;
            query.start_date = startDate;
            query.end_date = endDate;

        } else if (layer.source == "COMPLEX") {
            query.type = 'COMPLEX';
            query.formula = layer.formula || "";
            query.series = layer._series;
            query.start_date = startDate;
            query.end_date = endDate;
        } else {

        }


        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                this._populateLayerData(layer.uuid, resp);
            })
            .catch(err => {
                ewars.error("Error loading layer data");
            })

    };

    _populateFormPoint = (lid, res) => {
        let _layer = this._layers[lid];
        this._layerData[lid] = res;
        let validPointLocs = res.d.filter(item => {
            return item.point != null && item.point != "";
        })

        _layer.info.update({
            name: _layer.config.title || "Unnamed layer",
            missing: res.missing,
            found: res.valid
        });

        this._renderPoints(this._layerData[lid], this._layers[lid]);

    };

    _renderPoints = (data, _layer) => {
        let locScale = (parseFloat(_layer.config.marker_size) || 10) / this._scale;

        let validPointLocs = data.d.map(item => {
            if (item.point != null && item.point != "") {
                let coords = [
                    parseFloat(item.point[1]),
                    parseFloat(item.point[0])
                ]
                let uuid = ewars.utils.uuid();

                return {
                    ...item,
                    _: uuid,
                    geom: {
                        type: "Point",
                        coordinates: coords.reverse()
                    }
                }
            }
        });

        let gj = {
            type: "FeatureCollection",
            features: validPointLocs.map(item => {
                return {
                    type: "Feature",
                    geometry: item.geom,
                    properties: {
                        ...item
                    }
                }
            })
        };

        /// Calculate how many kilometres it is from the center from a given circle to it's edge
        let markerSize = _layer.config.marker_size || 10;
        let metresPerPixel = 40075016.686 * Math.abs(Math.cos(this._map.getCenter().lat * 180 / Math.PI)) / Math.pow(2, this._map.getZoom() + 8);
        let kmRadius = (metresPerPixel * (markerSize * 2)) / 1000;


        let cluster = clustersDbscan(gj, kmRadius, {minPoints: 2});

        let clusters = {};
        let results = [];

        let self = this;

        cluster.features.forEach(item => {
            if (item.properties.dbscan == "core") {
                if (!clusters["_" + String(item.properties.cluster)]) {
                    clusters["_" + String(item.properties.cluster)] = [item];
                } else {
                    clusters["_" + String(item.properties.cluster)].push(item);
                }
            } else {
                results.push(item);
            }
        });

        for (let i in clusters) {
            let _centroid = centroid({
                type: "FeatureCollection",
                features: clusters[i]
            }, {properties: {center: true}})
            results.push({
                type: "Feature",
                properties: {
                    count: clusters[i].length,
                    items: clusters[i]
                },
                geometry: _centroid.geometry
            })
        }

        _layer._el_points.selectAll("g").remove();

        let elem = _layer._el_points.selectAll("g").data(results);

        let elemEnter = elem.enter().append("g")
            .on("click", function (d) {
                if (d.properties.count > 0) {

                    self.rootInfo.update({
                        name: _layer.config.title,
                        data: d.properties.items
                    });
                } else {
                    window.open("/reporting#?uuid=" + d.properties.uuid)
                }
            })

        elemEnter.append("circle")
            .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
            .attr("cx", function (d) {
                return self.projection.latLngToLayerPoint(
                    new L.LatLng(
                        d.geometry.coordinates[0],
                        d.geometry.coordinates[1]
                    )
                ).x;
            })
            .attr("cy", function (d) {
                return self.projection.latLngToLayerPoint(
                    new L.LatLng(
                        d.geometry.coordinates[0],
                        d.geometry.coordinates[1]
                    )
                ).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale)

        elemEnter.append('text')
            .style("text-anchor", 'middle')
            .style('fill', _layer.config.label_colour || '#F2F2F2')
            .attr("x", function (d) {
                return self.projection.latLngToLayerPoint(
                    new L.LatLng(
                        d.geometry.coordinates[0],
                        d.geometry.coordinates[1]
                    )
                ).x;
            })
            .attr("y", function (d) {
                return self.projection.latLngToLayerPoint(
                    new L.LatLng(
                        d.geometry.coordinates[0],
                        d.geometry.coordinates[1]
                    )
                ).y;
            })
            .attr('dy', "0.35em")
            .text(function (d) {
                return d.properties.count || 1;
            })
            .style("font-size", (12 / this._scale) + "px")
    };

    _drawFormPointCount = () => {

    };

    _populateStaticLayer = (lid, res) => {
        let _layer = this._layers[lid];
        this._layerData[lid] = res;

        let validPoints = [],
            validGeoms = [];

        res.forEach(item => {
            if (item.geometry_type == "ADMIN") {
                if (_isValidGeom(item.geojson)) validGeoms.push(item);
            } else {
                if (_isValidPoint(item.geojson)) validPoints.push(item);
            }
        });

        let self = this;

        _layer._el_chloros.selectAll("path")
            .data(validGeoms)
            .enter()
            .append("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", _layer.config.fill_color || "red")
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale)
            .attr("d", function (datum) {
                return self.projection.pathFromGeojson(datum.geojson);
            })
            .attr("id", function (d) {
                return d.uuid;
            });

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .remove()
            .data(validPoints)
            .enter()
            .append(_layer.config.marker_type || "circle")
            .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
            .attr("cx", function (d) {
                return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).x;
            })
            .attr("cy", function (d) {
                return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);


        if (_layer.config.lab_type !== "NONE" && _layer.config.lab_type != null) {
            _layer._el_chloro_labels.selectAll("text")
                .data(validGeoms)
                .enter()
                .append("text")
                .style("text-anchor", "middle")
                .style("fill", _layer.config.label_colour || "#000")
                .style("font-size", _layer.config.label_font_size || 12)
                .style("text-align", "center")
                .attr("x", function (d) {
                    return self.path.centroid(d.location.geometry || "{}")[0];
                })
                .attr("y", function (d) {
                    return self.path.centroid(d.location.geometry || "{}")[1];
                })
                .text(function (d) {
                    return __(d.location.name.en);
                });

            _layer._el_point_labels.selectAll('text')
                .data(validPoints)
                .enter()
                .append('text')
                .style('text-anchor', function (d) {
                    let geo = d.location.geometry || '{}';
                    return geo.coordinates[0] > -1 ? "start" : "end";
                })
                .style('fill', _layer.config.label_colour || '#000')
                .attr('transform', function (d) {
                    let geo = d.location.geometry || '{}';
                    return "translate(" + self.projection(geo.coordinates) + ")";
                })
                .attr('dy', '.35em')
                .style('font-size', '11px')
                .text(function (d) {
                    return __(d.location.name.en || d.location.name);
                })
        }
    };

    _updateLayerStyleStatic = (lid) => {
        let _layer = this._layers[lid];

        let self = this;

        _layer._el_chloros.selectAll("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", _layer.config.fill_color || "red")
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

        _layer._el_points.selectAll("circle")
            .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);


    };

    _updateLayerStyleNonStatic = (lid) => {
        let _layer = this._layers[lid];
        let self = this;

        _layer._el_chloros.selectAll("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", function (d) {
                return self._getColour(parseFloat(d.data), null, null, _layer.config.scales || []);
            })
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);
    };

    _populateLayerData = (lid, res) => {
        if (res.err) {
            if (res.code == "INDICATOR_MISSING") {
                ewars.error("A layer is missing an indicator specification.");
            }
        }


        let info = this._layers[lid].info;

        let _layer = this._layers[lid];
        this._layerData[lid] = res;

        let validPoints = [],
            validGeoms = [];

        res.d.forEach(item => {
            if (item.location.geometry_type == "ADMIN") {
                if (_isValidGeom(item.location.geometry)) validGeoms.push(item);
            } else {
                if (_isValidPoint(item.location.geometry)) validPoints.push(item);
            }
        })


        if (_layer.config.ctrl_scale) {
            if (!_layer.legend) {
                let legend = L.control(_layer.config.scale_pos || "topright");
                info.onAdd = function (map) {
                    this._div = L.DomUtil.create('div', 'meta-container'); // create a div with a class "info"
                    return this._div;
                };

                info.addTo(this._map);

                info.update = function (props) {
                    let _html = "";

                    _html = `
                    <div class="metabox-title">${props.name || ""}</div>
                    <div class="metabox-table">`;

                    props.scales.forEach(item => {
                        _html += `
                        <div class="map-meta-scale">
                            <span class="color" style="background: ${item[2]}"></span>
                            <span>${item[0]} to ${item[1]}</span>
                        </div>`;
                    })

                    _html += '</div></div>';

                    this._div.innerHTML = _html;
                }

                info.update({
                    name: _layer.config.title || "Unnamed layer",
                    scales: _layer.config.scales || []
                })
            } else {
                _layer.legend.update({
                    name: _layer.config.title || "Unnamed layer",
                    scales: _layer.config.scales || []
                })
            }
        }

        let self = this;

        _layer._el_chloros.selectAll("path")
            .data(validGeoms)
            .enter()
            .append("path")
            .style("fill-opacity", _layer.config.fill_opacity || '1')
            .style("fill", function (d) {
                return self._getColour(parseFloat(d.data), null, null, _layer.config.scales || []);
            })
            .style("stroke", _layer.config.stroke_color || "transparent")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale)
            .attr("d", function (datum) {
                return self.projection.pathFromGeojson(datum.location.geometry);
            })
            .attr("id", function (d) {
                return d.location.uuid;
            });

        _layer._el_points.selectAll(_layer.config.marker_type || "circle")
            .data(validPoints)
            .enter()
            .append(_layer.config.marker_type || "circle")
            .attr("r", (parseFloat(_layer.config.marker_size) || 10) / this._scale)
            .attr("cx", function (d) {
                return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).x;
            })
            .attr("cy", function (d) {
                return self.projection.latLngToLayerPoint(new L.LatLng(d.location.geometry.coordinates[1], d.location.geometry.coordinates[0])).y;
            })
            .style("fill", _layer.config.fill_color || "green")
            .style("fill-opacity", _layer.config.fill_opacity || "1")
            .style("stroke", _layer.config.stroke_color || "white")
            .style("stroke-width", (_layer.config.stroke_width || "0") / this._scale);

        if (_layer.config.lab_type !== "NONE" && _layer.config.lab_type != null) {
            _layer._el_chloro_labels.selectAll("text")
                .data(validGeoms)
                .enter()
                .append("text")
                .style("text-anchor", "middle")
                .style("fill", _layer.config.label_colour || "#000")
                .style("font-size", _layer.config.label_font_size || 12)
                .style("text-align", "center")
                .attr("x", function (d) {
                    return self.path.centroid(d.location.geometry || "{}")[0];
                })
                .attr("y", function (d) {
                    return self.path.centroid(d.location.geometry || "{}")[1];
                })
                .text(function (d) {
                    return __(d.location.name.en);
                });


            _layer._el_point_labels.selectAll('text')
                .data(validPoints)
                .enter()
                .append('text')
                .style('text-anchor', function (d) {
                    let geo = d.location.geometry || '{}';
                    return geo.coordinates[0] > -1 ? "start" : "end";
                })
                .style('fill', _layer.config.label_colour || '#000')
                .attr('transform', function (d) {
                    let geo = d.location.geometry || '{}';
                    return "translate(" + self.projection(geo.coordinates) + ")";
                })
                .attr('dy', '.35em')
                .style('font-size', '11px')
                .text(function (d) {
                    return __(d.location.name.en || d.location.name);
                })
        }
    };

    unmount() {
        ewars.unsubscribe("UPDATE_MAP", this.update);

    }

    _removeLayer = (lid) => {
        this._layers[lid].elem.selectAll("g").remove();
        delete this._layers[lid];
        delete this._layerData[lid];
    };

    update = (config) => {
        this._config = config[0];
        let updatedLayer = config[1][0] || null;

        if (!this._mounted) {
            this.mount();
        } else {
            if (updatedLayer) {
                let newLayer = config[0].layers.filter(item => {
                    return item.uuid == updatedLayer;
                })[0] || null;


                let origLayer = this._layers[newLayer.uuid];

                let hasDataChanged = false;
                DATA_FIELDS.forEach(field => {
                    if (newLayer[field] != origLayer.config[field]) hasDataChanged = true;
                });


                if (hasDataChanged || !origLayer) {
                    this._addLayer(newLayer);
                } else {
                    if (newLayer.type == "FORM_POINT") {
                        this._layers[newLayer.uuid].config = newLayer;
                        this._updateLayerStyleStatic(newLayer.uuid);
                    } else {
                        if (newLayer.source == "NONE") {
                            this._layers[newLayer.uuid].config = newLayer;
                            this._updateLayerStyleStatic(newLayer.uuid);
                        } else if (newLayer.source == "INDICATOR" || newLayer.source == "COMPLEX") {
                            this._layers[newLayer.uuid].config = newLayer;
                            this._updateLayerStyleNonStatic(newLayer.uuid);
                        }
                    }
                }
            } else {
                // Remove layers that were removed from config
                for (let uid in this._layers) {
                    let layer = config[0].layers.filter(item => {
                        return item.uuid == uid;
                    })[0] || null;

                    if (!layer) {
                        this._removeLayer(uid);
                    }
                }
            }
        }

    };

    _rebuild = (config) => {
        this._config = config;

        this._root.selectAll("g").remove();
        this._layerData = {};
        this._layers = {};
        // this._metaContainer.innerHTML = "";

        if (this._config.config.center) {
            this._map.setView(
                [this._config.config.center.lat, this._config.config.center.lng],
                this._config.config.center.zoom);
        }

        (this._config.layers || []).forEach(layer => {
            this._addLayer(layer);
        })

    };

    _getColour = (value, ot, oth, scales) => {
        if (!scales || scales.length <= 0) {
            return "rgb(0,0,0)";
        }
        let colour;
        scales.forEach(scale => {
            if (scale[1] === "INF") {
                if (value >= parseFloat(scale[0])) colour = scale;
            } else {
                if (value >= parseFloat(scale[0]) && value <= parseFloat(scale[1]))
                    colour = scale;
            }
        });

        if (!colour) return "rgb(0,0,0)";
        return colour[2];
    };
}

const STYLE = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
};

class MapControl extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        if (this._el) {
            this._map = new Map(this.props.data || {}, this._el);
            this._map.mount();
        }
    }

    componentWillUnmount() {
        if (this._el) {
            this._map.unmount();
            this._map = null;
            this._el.innerHTML = "";
        }
    }

    componentWillReceiveProps(nextProps) {

    }


    render() {
        return (
            <div
                className="map-wrap-canvas"
                style={STYLE}>
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    className="map-inner">
                </div>

            </div>
        )
    }

}

export default MapControl;
