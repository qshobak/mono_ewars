import { Layout, Row, Cell } from "../../common";

class MapItem extends React.Component {
    constructor(props) {
        super(props);
    }

    onSelect = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        return (
            <div className="block hoverable" onClick={this.onSelect}>
                <div className="block-content">{this.props.data.title}</div>
            </div>
        )
    }
}

class MapBrowser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            shared: false
        }

        this._load();
    }

    _load = (shared) => {
        ewars.tx("com.ewars.mapping.query", [shared])
            .then(res => {
                this.setState({
                    shared: shared,
                    data: res
                })
            })
            .catch(err => {
                ewars.error("An error occurred retrieving maps")
            })
    };

    _showImport = () => {

    };

    _onSelect = (data) => {
        this.props.onSelect(data);
    };

    render() {
        return (
            <div className="map-browser">
                <div className="map-header">
                    <i className="fal fa-map"></i>&nbsp;Maps

                    <div className="btn-group pull-right" style={{marginTop: "-5px", marginRight: "3px"}}>
                        <button
                            onClick={this.props.onCloseMenu}><i className="fal fa-times"></i></button>
                    </div>
                </div>
                <div className="map-body">
                    <Layout>
                        <Row>
                            <Cell
                                style={{
                                    display: "flex"
                                }}
                                width="70%"
                                borderRight={true}>
                                <Row height="35px" style={{display: "block", background: "#F2F2F2"}} borderBottom={true}>
                                    <div className="ux-tabs">
                                        <div
                                            onClick={() => {
                                                this._load(false);
                                            }}
                                            className={"ux-tab" + (this.state.shared ? "" : " active")}>
                                            <i className="fal fa-user"></i>&nbsp;My Maps
                                        </div>
                                        <div
                                            onClick={()=> {
                                                this._load(true);
                                            }}
                                            className={"ux-tab" + (this.state.shared ? " active" : "")}>
                                            <i className="fal fa-users"></i>&nbsp;Shared Maps
                                        </div>
                                    </div>
                                </Row>
                                <Row style={{display: "block", overflowY: "auto"}}>
                                    <div className="block-tree hoverable" style={{position: "relative"}}>
                                        {this.state.data.map(item => {
                                            return <MapItem
                                                onSelect={this._onSelect}
                                                data={item}/>
                                        })}
                                    </div>
                                </Row>
                            </Cell>
                            <Cell style={{padding: "8px"}}>
                                <button
                                    onClick={this.props.onCreateNew}>
                                    <i className="fal fa-plus"></i>&nbsp;Create New
                                </button>
                                <div style={{height: "8px"}}></div>
                                <button onClick={this._showImport}>
                                    <i className="fal fa-upload"></i>&nbsp;Import Map
                                </button>
                            </Cell>
                        </Row>
                    </Layout>
                </div>
            </div>
        )
    }
}

export default MapBrowser;
