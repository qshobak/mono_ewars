import { Button } from "../../../common";

var defaults = {
    source_type: "DEFAULT",
    location_spec: "MANUAL",
    style: "DEFAULT",
    scale: "DEFAULT",
    opacity: 1,
    reduction: "SUM"
};

var Layer = React.createClass({
    getInitialState: function () {
        return {
            data: {
                opacity: 1,
                steps: 10,
                start: 'yellow',
                end: 'red'
            },
            isCollapsed: false,
            isLayerVisible: true
        }
    },

    _onUpdate: function (form, prop, value) {
        this.props.onUpdate(this.props.index, prop, value);
    },

    collapse: function () {
        this.state.isCollapsed = true;
        this.forceUpdate();
    },

    _collapse: function () {
        this.state.isCollapsed = this.state.isCollapsed ? false : true;
        this.forceUpdate();
    },

    _deleteLayer: function () {
        this.props.onDelete(this.props.index);
    },

    _editLayer: function () {
        this.props.onEdit(this.props.index);
    },

    _onChange: function (e) {
        this.state.data[e.target.name] = e.target.value;
        this.forceUpdate();
    },

    render: function () {

        var collapseIcon;
        if (this.state.isCollapsed) collapseIcon = "fa-caret-square-o-right";
        if (!this.state.isCollapsed) collapseIcon = "fa-caret-square-o-down";

        var eyeClass;
        if (this.state.isLayerVisible) eyeClass = "fa-eye";
        if (!this.state.isLayerVisible) eyeClass = "fa-eye-slash";

        var style = {display: "block"};
        if (this.state.isCollapsed) style.display = "none";

        return (
            <div className="map-layer">
                <div className="ide-tbar">
                    <div className="ide-tbar-text">Layer {this.props.index + 1}</div>

                    <div className="btn-group pull-right">
                        <Button
                            icon="fa-caret-up"/>
                        <Button
                            icon="fa-caret-down"/>
                    </div>
                    <div className="btn-group pull-right">
                        <Button
                            icon="fa-pencil"
                            onClick={this._editLayer}/>
                        <Button
                            colour="red"
                            onClick={this._deleteLayer}
                            icon="fa-trash"/>
                    </div>
                    <div className="btn-group pull-right">
                        <Button
                            icon={eyeClass}
                            onClick={this._toggleLayer}/>
                        <Button
                            onClick={this._collapse}
                            icon={collapseIcon}/>
                    </div>
                </div>
                <div className="layer-settings" style={style}>
                    <table>
                        <tr>
                            <td>
                                <label htmlFor="">Opacity</label>
                            </td>
                            <td>
                                <input onChange={this._onChange} value={this.state.data.opacity} name="opacity"
                                       type="range" min="" max="1" step="0.1"/> {this.state.data.opacity}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="">Scale Steps</label>
                            </td>
                            <td>
                                <input id="test" type="range" min="2" max="50" step="1"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="">Scale Start Colour</label>
                            </td>
                            <td>
                                <input id="background-color" type="color"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label htmlFor="Scale End Color">Scale End Colour</label>
                            </td>
                            <td>
                                <input id="background" type="color"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
});

module.exports = Layer;
