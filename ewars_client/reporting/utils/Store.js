import AppDispatcher from "./AppDispatcher";
import { EventEmitter } from "events";
import assign from "object-assign";

import CONSTANTS from "../../common/constants";

const CHANGE_EVENT = "CHANGE";

const DEFAULT_DASH = {
    id: "com.ewars.dashboard",
    Cmp: "DASHBOARD"
};

var STATE = {
    // Tabs
    _tabs: [DEFAULT_DASH],
    _activeTab: "com.ewars.dashboard",
    _tabMap: {
        "com.ewars.dashboard": DEFAULT_DASH
    },

    // Assignments
    _assignments: null,
    _assignsByFormId: {},

    // Submissions
    _drafts: {},

    // Forms
    _definitions: {},
    _versions: {},
    _grids: {},

    // OVerall State
    _showAssignmentWizard: false,
    _view: "DEFAULT"
};

function _destroyTab (tabId) {
    delete STATE._tabMap[tabId];

    let index;
    for (var i in STATE._tabs) {
        if (tabId == STATE._tabs[i].id) {
            index = i;
            STATE._tabs.splice(i, 1);
        }
    }

    if (STATE._activeTab == tabId) {
        STATE._activeTab = STATE._tabs[index-1].id;
    }

}

function _receiveSubmission(data) {
    let id = `com.ewars.submission.${data.form_id}.${data.uuid}`;
    STATE._drafts[data.uuid] = data;
}

function _updateDraft (id, prop, value) {
    if (prop.indexOf(".") >= 0) {
        STATE._drafts[id].data[prop] = value;
    } else {
        ewars.setKeyPath(STATE._drafts[id], prop, value);
    }
}


function _setAssignments(assigns) {
    assigns.forEach(function (item) {
        STATE._assignsByFormId[item.form_id] = item;
    });

    STATE._assignments = assigns;
}

function _addFormDef(def) {
    STATE._definitions[def.id] = def;
    STATE._versions[def.version_id] = def;
}

function _receiveGrid(grid) {
    // Need to do some work to the grid.
    STATE._grids[grid.form_id] = grid
}

/**
 * Set the active tab
 * @param id
 * @private
 */
function _addTab(id) {
    if (STATE._tabMap[id]) {
        STATE._activeTab = id;
        return;
    }

    let tab;
    if (id.includes("com.ewars.browser")) {
        tab = {
            Cmp: "REPORT_BROWSER",
            form_id: id.split(".")[3],
            id: id
        }
    }

    if (id.includes("com.ewars.report")) {
        tab = {
            Cmp: "REPORT",
            form_id: id.split(".")[3],
            id: id
        }
    }

    if (id.includes("com.ewars.submission")) {
        let splat = id.split(".");
        tab = {
            Cmp: "REPORT",
            form_id: splat[splat.length - 2],
            id: id,
            report_id: splat[-1]
        }
    }

    if (id.includes("com.ewars.draft")) {
        let splat = id.split(".");
        let uuid = splat[splat.length - 1];
        tab = {
            Cmp: "REPORT",
            form_id: splat[splat.length - 2],
            uuid: uuid,
            id: id
        };

        STATE._drafts[uuid] = {
            location_id: null,
            created_by: window.user.id,
            data_date: new Date(),
            data: {}
        }
    }

    STATE._tabs.push(tab);
    STATE._tabMap[id] = tab;
    STATE._activeTab = id;
}
function _updateTabState(id, state) {
    STATE._tabState[id] = state;
}


function _setActiveTab(id) {
    STATE._activeTab = id;
}

function _setWizardViz(state) {
    STATE._showAssignmentWizard = state;
}

/**
 * Remove a tab from the store
 * @param id The id of the tab
 * @param index The index of the tab to remove
 * @private
 */
function _closeTab(id, index) {
    if (index <= 1) {
        STATE._activeTab = "com.ewars.dashboard";
    } else {
        STATE._activeTab = STATE._tabs[index - 1].id;
    }


    delete STATE._tabMap[id];
    STATE._tabs.splice(index, 1);

    if (id.includes("com.ewars.draft")) {
        let splat = id.split(".");
        delete STATE._drafts[splat[splat.length - 1]];
    }
}


var Store = assign({}, EventEmitter.prototype, {
    getAll: function () {

    },

    emitChange: function (event) {
        this.emit(event);
    },

    addChangeListener: function (event, callback) {
        this.on(event, callback);
    },

    removeChangeListener: function (event, callback) {
        this.removeListener(event, callback);
    },

    saveForm: function (data) {

    },

    saveGridDefinition: function (data) {

    },

    getGridDefinition: function (id) {

    },

    getForm: function (id, version) {

    },

    getReportTabState: function (id) {
        let splat = id.split('.');
        let form_id = splat[splat.length - 2];
        let draft_id = splat[splat.length - 1];

        return {
            dirty: false,
            isLoaded: false,
            form: STATE._definitions[form_id] || null,
            errors: null,
            uuid: draft_id,
            data: STATE._drafts[draft_id] || null
        }
    },

    getBrowserTabState: function (id) {
        let splat = id.split(".");
        let formId = splat[splat.length - 1];

        return {
            assignment: STATE._assignsByFormId[formId],
            grid: STATE._grids[formId] || null
        }
    },

    getTabs: function () {
        return STATE._tabs;
    },

    getGridState: function (id) {
        if (id in STATE._grids) {
            return STATE._grids[id];
        }

        STATE._grids[id] = {
            data: [],

        };
        return STATE._grids[id];
    },

    getAssignmentByFormId: function (id) {
        return STATE._assignsByFormId[id];
    },

    getAssignsState: function () {
        return {
            assignments: STATE._assignments
        };
    },

    getFormDefinition: function (id) {
        return STATE._definitions[id] || null;
    },

    /**
     * Retrieve the state of the root component
     * @returns {{view: string, showAssignmentWizard: boolean, tabs: *[], activeTab: string, tabMap: {[com.ewars.dashboard]: {id: string, Cmp: string}}}}
     */
    getOverallState: function () {
        return {
            view: STATE._view,
            showAssignmentWizard: STATE._showAssignmentWizard,
            tabs: STATE._tabs,
            activeTab: STATE._activeTab,
            tabMap: STATE._tabMap
        }
    },

    addAssignments: function (assignments) {
        _setAssignments(assignments);
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        let action = payload.action.actionType;
        let data = payload.action.data;

        // Add a new tab to the interface
        if (action == "ADD_TAB") {
            _addTab(data);
            Store.emitChange("ROOT_CHANGE");
        }

        // Toggle assignment viewier
        if (action == "TOGGLE_ASSIGN") {
            _setWizardViz(data);
            Store.emitChange("ROOT_CHANGE");
        }

        // Set the active tab
        if (action == "SET_TAB") {
            _setActiveTab(data);
            Store.emitChange("ROOT_CHANGE");
        }

        // Close a tab
        if (action == "CLOSE_TAB") {
            _closeTab(data);
            Store.emitChange("ROOT_CHANGE");
        }

        // Remove a tab from the interface
        if (action == "REMOVE_TAB") {
            _closeTab(data[0], data[1]);
            Store.emitChange("ROOT_CHANGE");
        }

        // Individual Tab State

        // Update a components state?
        if (action == "UPDATE_CMP_STATE") {
            _updateTabState(data.id, data.state);
            Store.emitChange("CMP_CHANGE");
        }

        // Receive Assignments
        if (action == "SET_ASSIGNS") {
            _setAssignments(data);
            Store.emitChange("ASSIGNS_CHANGE");
        }

        // Receive a form definition
        if (action == "RECEIVE_FORM_DEF") {
            _addFormDef(data);
            Store.emitChange("TAB_CHANGE");
        }

        // Update a draft document
        if (action == "UPDATE_DRAFT") {
            _updateDraft(data.did, data.prop, data.value);
            Store.emitChange("TAB_CHANGE");
        }
        // Receive the grid from the API call
        if (action == "RECV_GRID") {
            _receiveGrid(data);
            Store.emitChange("TAB_CHANGE");
        }

        if (action == "SET_DATA") {
            _receiveSubmission(data);
            Store.emitChange("TAB_CHANGE");
        }

        if (action == "DESTROY_TAB") {
            _destroyTab(data);
            Store.emitChange("ROOT_CHANGE");
        }

        return true;
    })
});

export default Store;
