import Actions from "./Actions";


var API = {
    getUserAssignments: function () {
        ewars.tx("com.ewars.user", ["ASSIGNMENTS", window.user.id, null])
            .then(function (resp) {
                Actions.setAssignments(resp);
            })
    },

    getFormDefinition: function (form_id) {
        ewars.tx("com.ewars.form", ["CURRENT", form_id, null])
            .then(function (resp) {
                Actions.setFormDefinition(resp);
            })
    },

    getFormGrid: function (form_id) {
        ewars.tx("com.ewars.form", ["DEFINITION", form_id, null])
            .then(function (resp) {
                Actions.setFormGrid(resp);
            })
    },

    getSubmission: function (uuid) {
        ewars.tx("com.ewars.resource", ['report', uuid, ['uuid', 'data_date', 'location_id', 'data', 'status', 'created_by'], null])
            .then((resp) => {
                Actions.setData(resp);
            })
    }
};

export default API;
