import AppDispatcher from "./AppDispatcher";
import CONSTANTS from "../../common/constants";

const Actions = {
    swapDraft: function (tabId) {
        AppDispatcher.handleViewAction({
            actionType: "SWAP_TAB",
            data: tabId
        })
    },
    addTab: function (tab) {
        AppDispatcher.handleViewAction({
            actionType: "ADD_TAB",
            data: tab
        })
    },

    removeTab: function (id, index) {
        AppDispatcher.handleViewAction({
            actionType: "REMOVE_TAB",
            data: [id, index]
        })
    },

    destroyTab: function (id) {
        AppDispatcher.handleViewAction({
            actionType: "DESTROY_TAB",
            data: id
        })
    },

    setActiveTab: function (id) {
        AppDispatcher.handleViewAction({
            actionType: "SET_TAB",
            data: id
        })
    },

    toggleAssignmentWizard: function (isShown) {
        AppDispatcher.handleViewAction({
            actionType: "TOGGLE_ASSIGN",
            data: isShown
        })
    },

    setAssignments: function (assigns) {
        AppDispatcher.handleViewAction({
            actionType: "SET_ASSIGNS",
            data: assigns
        })
    },

    setFormDefinition: function (data) {
        AppDispatcher.handleViewAction({
            actionType: "RECEIVE_FORM_DEF",
            data: data
        })
    },

    updateDraftField: function (draft_id, prop, value) {
        AppDispatcher.handleViewAction({
            actionType: "UPDATE_DRAFT",
            data: {
                did: draft_id,
                prop: prop,
                value: value
            }
        })
    },

    setFormGrid: function (grid) {
        AppDispatcher.handleViewAction({
            actionType: "RECV_GRID",
            data: grid
        })
    },

    setData: function (report) {
        AppDispatcher.handleViewAction({
            actionType: "SET_DATA",
            data: report
        })
    }

};

export default Actions;
