import {
    DataTable,
    Spinner
} from "../../../common";

const COLUMNS = [
    {
        name: "data_date",
        config: {
            label: "Report Date",
            type: "date"
        },
        width: 80,
        fmt: function (value) {
            if (!value) return "None Set";
            return ewars.DATE(value, "DAY")
        }
    },
    {
        name: "location.name",
        config: {
            label: "Location",
            type: "location",
            filterKey: "location_id"
        },
        fmt: function (value) {
            if (!value) return "None Set";
            return ewars.I18N(value);
        },
        width: 250
    },
    {
        name: "created",
        config: {
            label: "Created",
            type: "date"
        },
        width: 100,
        fmt: ewars.DATE
    },
    {
        name: "form.name",
        config: {
            label: "Form",
            type: "select",
            optionsSource: {
                resource: "form",
                valSource: "id",
                labelSource: "name",
                query: {}
            },
            filterKey: "form_id"
        },
        width: 100
    },
    {
        name: "source",
        config: {
            label: "Source",
            type: "text"
        },
        width: 100
    }
];

const actions = [
    {label: "View/Edit", action: "EDIT", icon: "fa-pencil", colour: "green"},
    {label: "Delete", action: "DELETE", icon: "fa-trash", colour: "red"}
];

const JOINS = [
    "location:location_id:uuid:uuid,name",
    "form:form_id:id:id,name"
];


class Drafts extends React.Component {
    constructor(props) {
        super(props);
    }

    _onAction = (action, cell, data) => {
        if (action == "CLICK") this._edit(data);
        if (action == "EDIT") this._edit(data);
        if (action == "DELETE") this._delete(data);
    };

    _delete = (data) => {
        ewars.prompt("fa-trash", "Delete Draft", "Are you sure you want to delete this draft?", function () {
            let bl = new ewars.Blocker(null, "Deleting draft....");

            ewars.tx("com.ewars.collection.draft.delete", [data.uuid])
                .then(function (resp) {
                    bl.destroy();
                    ewars.growl("Draft deleted");
                    ewars.emit("RELOAD_DT")
                }.bind(this))
        }.bind(this))
    };

    _edit = (data) => {
        ewars.z.dispatch("REPORTING", "ADD_TAB", {
            type: "DRAFT",
            title: "Draft - " + ewars.I18N(data.form.name),
            icon: "fa-clipboard",
            collection: data,
            form: {
                id: data.form.id
            }
        })
    };

    render() {

        let FILTER = {
            created_by: {eq: window.user.id},
            status: {eq: "DRAFT"}
        };

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <DataTable
                            resource="collection"
                            columns={COLUMNS}
                            id="DRAFTS"
                            join={JOINS}
                            onCellAction={this._onAction}
                            actions={actions}
                            filter={FILTER}/>
                    </div>
                </div>
            </div>
        )
    }
}
export default Drafts;
