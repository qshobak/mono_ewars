import CONSTANTS from "../../../common/constants";

import Deletions from "../../../common/widgets/widget.deletions";
import Amendments from "../../../common/widgets/widget.amendments";
import Overdue from "../../../common/widgets/widget.overdue";
import Upcoming from "../../../common/widgets/widget.upcoming";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-layout">
                <ewars.d.Row>
                    <ewars.d.Cell width="50%" borderRight={true} style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar label="Upcoming" icon="fa-calendar">

                        </ewars.d.Toolbar>
                        <ewars.d.Cell style={{overflowY: "auto"}}>

                            <Upcoming fitted={true}/>
                        </ewars.d.Cell>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar label="Overdue records" icon="fa-exclamation-triangle">

                        </ewars.d.Toolbar>
                        <ewars.d.Cell style={{overflowY: "auto"}}>
                            <Overdue fitted={true}/>
                        </ewars.d.Cell>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default Dashboard;
