import Actions from "../../utils/Actions";
import Moment from "moment";

import {
    DataTable,
    Spinner
} from "../../../common";
import {
    SwitchField
} from "../../../common/fields";

import API from "../../utils/api";
import Store from "../../utils/Store";

const actions = [
    {label: "View/Edit", action: "EDIT", icon: "fa-pencil", colour: "green"},
    {label: "Print", action: "PRINT", icon: "fa-print"}
];

if (window.user.role == "ACCOUNT_ADMIN") {
    actions.push({label: "Re-evaluate", action: "EVALUATE", icon: "fa-bell"})
}

const TABLE_ACTIONS = [
    ['fa-plus', 'CREATE', "Create new record"]
];

class ReportBrowser extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            inline: false
        }
    }

    componentWillMount() {
        if (!this.props.data.grid) {
            ewars.tx("com.ewars.form.definition", [this.props.data.form_id])
                .then(function (resp) {
                    ewars.z.dispatch("REPORTING", "SET_FORM_GRID", {
                        index: this.props.data.order,
                        form_id: this.props.data.form_id,
                        data: resp
                    })
                }.bind(this))
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.data.grid) {
            ewars.tx("com.ewars.form.definition", [nextProps.data.form_id])
                .then(function (resp) {
                    ewars.z.dispatch("REPORTING", "SET_FORM_GRID", {
                        index: nextProps.data.order,
                        form_id: nextProps.data.form_id,
                        data: resp
                    })
                }.bind(this))
        }
    }

    _onCellAction = (action, cell, data) => {
        switch (action) {
            case "CLICK":
            case "EDIT":
                ewars.z.dispatch("REPORTING", "ADD_TAB", {
                    type: "DRAFT",
                    title: this.props.data.title,
                    icon: "fa-clipboard",
                    collection: data,
                    form: {
                        id: this.props.data.form_id
                    }
                });
                break;
            case "PRINT":
                let w = window.open(`/print/record/${data.uuid}`, "RECORD PRINT");
                break;
            case "DELETE":
                break;
            case "EVALUATE":
                this._evaluate(data.uuid);
                break;
            case "CREATE":
                console.log(this.props.data);
                ewars.z.dispatch("REPORTING", "ADD_TAB", {
                    title: `Draft - ${ewars.I18N(this.props.data.form_name)}`,
                    icon: "fa-clipboard",
                    form: {
                        id: this.props.data.form_id,
                        name: this.props.data.form_name
                    },
                    collection: {
                        form_id: this.props.data.form_id,
                        created_by: window.user.id,
                        data: {},
                        location_id: null,
                        data_date: null
                    },
                    uuid: ewars.utils.uuid(),
                    type: "DRAFT"
                });
                break;
            default:
                break;
        }
    };

    _evaluate = (uuid) => {
        let bl = new ewars.Blocker(null, "Re-evaluating report");
        ewars.tx("com.ewars.collection.evaluate", [uuid])
            .then((resp) => {
                bl.destroy();
                ewars.growl("Report sent for alert re-evaluation");
            })
    };

    _inline = (prop, value) => {
        this.setState({
            inline: value
        })
    };

    render() {
        if (!this.props.data.grid) return <Spinner/>;
        const buttons = [
            {
                label: "Create New",
                icon: "fa-plus",
                action: this.createNew
            }
        ];

        let formName = this.props.data.title;

        let filter = {status: {eq: "SUBMITTED"}};
        if (window.user.role == "USER") {
            let location_ids = [];
            ewars.g.assignments.forEach(assignment => {
                if (assignment.form_id == this.props.data.form_id) {
                    assignment.locations.forEach(location => {
                        if (location.location_id) {
                            location_ids.push(location.location_id);
                        }
                    })
                }
            });

            if (location_ids.length <= 0) {
                filter.created_by = {eq: window.user.id}
            } else {
                filter.location_id = {in: location_ids};
            }
        }

        if (window.user.role == "REGIONAL_ADMIN") {
            if (!filter.location_id) {
                filter.location_id = {under: window.user.location_id};
            }
        }

        let editable = false;
        let isAdmin = false;
        if (["SUPER_ADMIN", "ACCOUNT_ADMIN"].indexOf(window.user.role)) {
            editable = true;
            isAdmin = true;
        }

        isAdmin = false;

        let id = "RP_BROWSE_" + this.props.data.form_id;

        let grid;
        if (this.props.data.grid) {
            grid = ewars.copy(this.props.data.grid.grid);

            grid.columns.forEach((col) => {
                switch (col.name) {
                    case "submitted_date":
                        col.fmt = (val) => {
                            return Moment.utc(val).format("YYYY-MM-DD HH:mm")
                        };
                        break;
                    case "created":
                        col.fmt = (val) => {
                            return Moment.utc(val).format("YYYY-MM-DD HH:mm")
                        };
                        break;
                    case "data_date":
                        col.fmt = (val) => {
                            return ewars.DATE(val, this.props.data.grid.interval);
                        };
                        break;

                    case "groups":
                        col.fmt = (val) => {
                            if (val) return val.join(",");
                            return ""
                        };
                        break;
                    default:
                        break;
                }
            })
        }

        return (
            <div className="ide-layout">
                {isAdmin ?
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">Admin Controls</div>

                                <div style={{float: "right", width: 150}}>
                                    <div className="ide-row">
                                        <div className="ide-col" style={{padding: 5, maxWidth: 45}}>Editor</div>
                                        <div className="ide-col" style={{maxWidth: 120}}>
                                            <SwitchField
                                                name="inline"
                                                value={this.state.inline}
                                                onUpdate={this._inline}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : null}
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-v">
                            <DataTable
                                grid={grid}
                                id={id}
                                header_actions={TABLE_ACTIONS}
                                editable={this.state.inline}
                                isReports={true}
                                filter={filter}
                                initialOrder={{"submitted_date": "DESC"}}
                                formId={this.props.data.form_id}
                                onCellAction={this._onCellAction}
                                onAction={this._onCellAction}
                                buttons={buttons}
                                actions={actions}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ReportBrowser;
