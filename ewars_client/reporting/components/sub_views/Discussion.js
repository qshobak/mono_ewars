import Moment from "moment";

class DiscussNode extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let showDate = true;
        let showUser = true;

        if (this.props.prev) {
            if (this.props.prev.user_id == this.props.data.user_id) {
                showDate = false;
                showUser = false;
            }
        }
        return (
            <div className="discuss-node">
                <ewars.d.Row>
                    <ewars.d.Cell width="120px">{showDate ? this.props.data.time : null}</ewars.d.Cell>
                    <ewars.d.Cell>
                        <p>{showUser ? <a href="#">{this.props.data.user_name}</a> : null} {this.props.data.content}</p>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default class Discussion extends React.Component {
    _lastCall = null;

    constructor(props) {
        super(props);

        this.state = {
            feed: [],
            content: "",
            today: Moment.utc().format("YYYY-MM-DD"),
            yesterday: Moment.utc().subtract(1, 'd').format("YYYY-MM-DD")
        };

        ewars.tx("com.ewars.collection.comments", [this.props.data.collection.uuid])
            .then((resp) => {
                this._lastCall = Moment.utc();
                this.setState({
                    feed: resp
                })
            });

        this._interval = window.setInterval(this._checkMessages, 3000);
    }

    componentWillUnmount() {
        window.clearInterval(this._interval);
    }

    _checkMessages = () => {
        ewars.tx("com.ewars.collection.comments", [this.props.data.collection.uuid])
            .then((resp) => {
                this._lastCall = Moment.utc();
                this.setState({
                    feed: resp
                })
            })
    };

    _onInputChange = (e) => {
        this.setState({
            content: e.target.value
        })
    };

    _onKeyDown = (e) => {
        if (e.key == "Enter" && !e.shiftKey) {
            this._submit();
        }

        if (e.key == "Enter" && e.shiftKey) {
            e.preventDefault();
            let content = this.state.content;
            content = content + "\n";
            this.setState({
                content: content
            })
        }
    };

    _submit = () => {

        ewars.tx("com.ewars.collection.comment", [this.props.data.collection.uuid, this.state.content])
            .then((resp) => {
                ewars.tx("com.ewars.collection.comments", [this.props.data.collection.uuid])
                    .then((resp) => {
                        this._lastCall = Moment.utc();
                        this.setState({
                            feed: resp,
                            content: ""
                        })
                    })
            })
    };

    componentDidMount() {
        let height = this.refs.scroll.scrollHeight;
        this.refs.scroll.scrollTop = height;
    }

    componentDidUpdate() {
        console.log("HERE");
        let height = this.refs.scroll.scrollHeight;
        this.refs.scroll.scrollTop = height;
    }

    render() {

        let items = {};

        this.state.feed.forEach((item) => {
            if (!items[item.day]) {
                items[item.day] = [item];
            } else {
                items[item.day].push(item);
            }
        });

        let groups = [];
        for (let i in items) {
            let nodes = items[i];
            let dateString = i;

            if (this.state.today == i) {
                dateString = "Today";
            }

            if (this.state.yesterday == i) {
                dateString = "Yesterday";
            }

            groups.push(
                <div className="discuss-date-sep">
                    <div className="line">
                        <div className="text"><h2>{dateString}</h2></div>
                    </div>
                </div>
            );

            let sortedNodes = nodes.sort((a, b) => {
                if (a.created > b.created) return 1;
                if (a.created < b.created) return -1;
                return 1;
            });

            let prev;
            sortedNodes.forEach((node) => {
                groups.push(
                    <DiscussNode
                        prev={prev}
                        key={node.uuid}
                        data={node}/>
                );
                prev = node;
            })
        }


        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Discussion">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll" ref="scroll">

                            <div className="discuss-feed">
                                {groups}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row height="100px">
                    <ewars.d.Cell>

                        <div className="discuss-input">
                            <ewars.d.Row>
                                <ewars.d.Cell width="40px" borderRight={true}>

                                </ewars.d.Cell>
                                <ewars.d.Cell>
                                    <input
                                        type="text"
                                        onChange={this._onInputChange}
                                        onKeyDown={this._onKeyDown}
                                        value={this.state.content}
                                        className="discuss-input-text"/>
                                </ewars.d.Cell>
                                <ewars.d.Cell width="40px" borderLeft={true}>

                                </ewars.d.Cell>
                            </ewars.d.Row>

                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}