import User from "../../../user/components/User";

export default class UserView extends React.Component {
    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Submitting User">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <User id={this.props.data.collection.created_by}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}
