import Alerts from "./Alerts";
import Amendments from "./Amendments";
import Discussion from "./Discussion";
import History from "./History";
import Info from "./Info";
import Location from "./Location";
import UserView from "./UserView";

export default {
    Alerts,
    Discussion,
    Amendments,
    History,
    Info,
    Location,
    UserView
}