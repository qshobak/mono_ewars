export default class History extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        };

        ewars.tx("com.ewars.collection.history", [this.props.data.collection.uuid])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }


    render() {
        console.log(this.state.data);
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Form Submission History">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

