import LocationProfile from "../../../common/p.location";

export default class Location extends React.Component {
    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Location">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <LocationProfile uuid={this.props.data.collection.location_id}/>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}
