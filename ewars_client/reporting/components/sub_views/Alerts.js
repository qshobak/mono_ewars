import Moment from "moment";

const STATES = {
    OPEN: "Open",
    CLOSED: "Closed"
};

const STAGES = {
    VERIFICATION: "Verification",
    RISK_ASSESS: "Risk Assessment",
    RISK_CHAR: "Risk Characterization",
    OUTCOME: "Outcome"
};

class Alert extends React.Component {
    _click = () => {
        window.open("http://" + ewars.domain + "/alert#?uuid=" + this.props.data.uuid);
    };

    render() {
        console.log(this.props.data);
        let locationName = ewars.I18N(this.props.data.location_name);
        let triggeredOn = Moment.utc(this.props.data.created);
        let alertPeriod = ewars.DATE(this.props.data.trigger_end, this.props.data.ds_interval);
        let alarmName = this.props.data.alarm_name;
        let stiName = ewars.I18N(this.props.data.sti_name);
        let state = STATES[this.props.data.state];
        let stage = STAGES[this.props.data.stage];

        return (
            <div className="block hoverable" onClick={this._click} style={{padding: 0}}>
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: 8, flex: 2}}>
                            {alarmName}, {locationName} ({stiName}), {alertPeriod}
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: 8, flex: 1}}>
                            {state} - {stage}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

export default class Alerts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

        ewars.tx('com.ewars.collection.alerts', [this.props.data.collection.uuid])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {

        let view;
        if (this.state.data == null) {
            view = (
                <div className="placeholder">Loading alerts...</div>
            )
        }

        if (this.state.data) {
            if (this.state.data.length <= 0) {
                view = (
                    <div className="placeholder">No associated alerts</div>
                )
            }

            if (this.state.data.length > 0) {
                view = this.state.data.map((item) => {
                    return <Alert key={item.uuid} data={item}/>
                })
            }
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Related Alerts">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: "relative"}}>
                                {view}
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}