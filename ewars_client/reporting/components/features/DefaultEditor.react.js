import CONSTANTS from "../../../common/constants";

import ReportDetails from "./ReportDetails.react";
import { Form } from "../../../common";

import Actions from "../../utils/Actions";

import Attachments from "../features/Attachments.react";


const FEATURES = [
    "ATTACHMENTS"
];

const FEATURE_MAP = {
    ATTACHMENTS: Attachments
};


function nest(data) {
    let unnested = {};

    for (var i in data) {
        let path = i.split(".");
        let last = path[path.length - 1 ];

        let term = unnested;

        path.forEach(item => {
            if (term[item]) {
                term = term[item];
            } else {
                if (item != last) {
                    term[item] = {};
                    term = term[item]
                } else {
                    term[item] = data[i]
                }
            }
        })

    }

    return unnested;
}


class DefaultEditing extends React.Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        readOnly: true,
        rid: null
    };

    onDataChange = (data, prop, value, path) => {
        Actions.updateDraftField(this.props.rid, path, value);
    };

    render() {
        let features = [];

        FEATURES.forEach((item) => {
            if (this.props.form.features[item]) {
                let Cmp = FEATURE_MAP[item];
                features.push(<Cmp form={this.props.data.form} data={this.props.data.data}/>)
            }
        });


        let nested = nest(this.props.data.data);

        return (
            <div className="ide-panel">
                <ReportDetails
                    readOnly={this.props.readOnly}
                    data={this.props.data}
                    rid={this.props.rid}
                    form={this.props.form}/>

                <div className="widget">
                    <div className="body">
                        <Form
                            readOnly={this.props.readOnly}
                            definition={this.props.form.definition}
                            data={nested}
                            updateAction={this.onDataChange}/>
                    </div>
                </div>

                {features}

                <div className="widget">
                    <div className="widget-header"><span>Alerts</span></div>
                    <div className="body"></div>
                </div>

                <div className="widget">
                    <div className="widget-header"><span><i className="fal fa-history"></i>&nbsp;History</span></div>
                    <div className="body">

                    </div>
                </div>

            </div>
        )
    }
}

export default DefaultEditing;
