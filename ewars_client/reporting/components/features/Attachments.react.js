const DROPZONE = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "rgba(0,0,0,0.5)",
    color: "#F2F2F2",
    textAlign: "center",
    display: "none"
};

class Attachment extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDragOver = () => {
        e.preventDefault();
        this.refs.dropzone.style.display = "block";
    };

    render() {
        return (
            <div className="widget">
                <div className="widget-header"><span>Attachments</span></div>
                <div className="body">
                    <div className="attachment-list" oDragOver={this._onDragOver} onDrop={this._onDrop}>

                        <div style={DROPZONE} ref="dropzone" className="dropzone">
                            Drop Files Here
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default Attachment;