import {
    Layout, Row, Cell,
    Button,
    Toolbar,
    Form,
    Shade,
    Modal,
    Panel
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Toolbar,
    Button,
    Form,
    Shade,
    Modal,
    Panel
};

import ReportingComponent from "./components/Component.react";

let curLoc = document.location.toString();

let DEFAULT;


// if (curLoc.indexOf("#?") > 0) {
//     DEFAULT = {
//         activeTab: 0,
//         tabs: [{
//             order: 0,
//             title: "Dashboard",
//             icon: "fa-tachometer",
//             type: "DASHBOARD"
//         }]
//     };
//
//
//     // Specifying a form to load up
//     let data = (curLoc.split("#?")[1]).split("&");
//     let dict = {};
//     data.forEach(item => {
//         let spec = item.split("=");
//         dict[spec[0]] = spec[1];
//     });
//
//     if (dict.form_id) {
//
//         DEFAULT.activeTab = 1;
//         DEFAULT.tabs.push({
//             order: 1,
//             title: "Draft",
//             icon: "fa-clipboard",
//             form: {
//                 id: dict.form_id,
//                 name: null,
//                 version: null
//             },
//             collection: {
//                 form_id: dict.form_id,
//                 created_by: window.user.id,
//                 data: {},
//                 location_id: dict.location_id,
//                 data_date: dict.data_date
//             },
//             uuid: ewars.utils.uuid(),
//             type: "DRAFT"
//         })
//         render(DEFAULT);
//     }
//
//     if (dict.uuid) {
//         let joins = [
//             "form:form_id:id:id,name"
//         ]
//         ewars.tx("com.ewars.resource", ["collection", dict.uuid, null, joins])
//             .then(resp => {
//                 render(DEFAULT);
//             })
//     }
// } else {
    render(null);
// }

function render(defaults) {
    ewars.tx('com.ewars.conf', ["*"])
        .then(res => {
            let form_groups = null;
            if (res.FORM_GROUPS) form_groups = JSON.parse(res.FORM_GROUPS);

            ReactDOM.render(
                <ReportingComponent
                    form_groups={form_groups}
                    defaults={defaults}/>,
                document.getElementById('application')
            );
        })
        .catch(err => {

        });
}




