export const SET_FORM = "SET_FORM";
export const SET_COLLECTION = "SET_COLLECTION";
export const SET_COLLECTION_PROP = "SET_COLLECTION_PROP";
export const SET_COLLECTION_DATA = "SET_COLLECTION_DATA";
export const SET_ACTIVE_TAB = "SET_ACTIVE_TAB";
export const SET_BACKUP = "SET_BACKUP";
export const RESTORE_BACKUP = "RESTORE_BACKUP";
export const DESTROY_BACKUP = "DESTROY_BACKUP";
export const SET_ERRORS = "SET_ERRORS";

export const SET_FORM_GRID = "SET_FORM_GRID";

export const ADD_TAB = "ADD_TAB";
export const REMOVE_TAB = "REMOVE_TAB";

