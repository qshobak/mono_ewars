import Moment from "moment";


const CONSTANTS + {
    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD"
}
/**
 * Get most recently completed interval date
 * @param specDate
 * @param interval
 * @returns {*}
 */
var getActualDate = function (specDate, interval) {
    if (interval == "DAY") return specDate;

    if (interval == "WEEK") {
        var actualEndWeek = specDate.clone().endOf("isoweek");

        if (specDate.isSame(actualEndWeek, "day")) return actualEndWeek;
        if (specDate.isBefore(actualEndWeek, "day")) return actualEndWeek.clone().subtract(7, "d");
    }

    if (interval == "MONTH") {
        var actualEndMonth = specDate.clone().endOf("month");

        if (specDate.isSame(actualEndMonth, "day")) return actualEndMonth;
        if (specDate.isBefore(actualEndMonth, "day")) return actualEndMonth.clone()
            .subtract(1, "M")
            .endOf("month");
    }

    if (interval == "YEAR") {
        var actualEndYear = specDate.clone().endOf("year");

        if (specDate.isSame(actualEndYear, "day")) return actualEndYear;
        if (specDate.isBefore(actualEndYear, "day"))
            return actualEndYear.clone().subtract(1, "y").endOf("year");
    }
};

var getDaysBetween = function (startDate, endDate) {
    var resultantDates = [startDate];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, "DAY")) {
        var newDate = cur_date.clone().add(1, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;

};

var getWeeksBetween = function (startDate, endDate) {
    var resultantDates = [startDate];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, "day")) {
        var newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;

};

var getMonthsBetween = function (startDate, endDate) {

};

var getYearsBetween = function (startDate, endDate) {

};

var INTERVAL_MAP = {
    "DAY": "d",
    "WEEK": "d",
    "MONTH": "M",
    "YEAR": "Y"
};

const DateUtils = {
    /**
     * Returns a list of dates occurring between two dates
     * of a specific interval type
     * @param start_date
     * @param end_date
     * @param interval
     */
    getDatesInInterval: function (start_date, end_date, interval) {

        var startDate = Moment(start_date),
            endDate;

        if (end_date) endDate = Moment(end_date);
        if (!end_date) endDate = Moment();

        var actualEnd = getActualDate(endDate, interval);
        var actualStart = getActualDate(startDate, interval);

        if (interval == "DAY") return getDaysBetween(startDate, endDate);
        if (interval == "WEEK") return getWeeksBetween(startDate, endDate);
        if (interval == "MONTH") return getMonthsBetween(startDate, endDate);
        if (interval == "YEAR") return getYearsBetween(startDate, endDate);

        return []
    },

    getStartDate: function (specDate, interval) {
        if (interval == "DAY") return Moment(specDate).clone();
        if (interval == "WEEK") return Moment(specDate).clone().startOf("isoweek");
        if (interval == "MONTH") return Moment(specDate).clone().startOf("month");
        if (interval == "YEAR") return Moment(specDate).clone().startOf("year");
    },

    getEndDate: function (specDate, interval) {
        if (interval == "DAY") return Moment(specDate).clone();
        if (interval == "WEEK") return Moment(specDate).clone().endOf("isoweek");
        if (interval == "MONTH") return Moment(specDate).clone().endOf("month");
        if (interval == "YEAR") return Moment(specDate).clone().endOf("year");
    },

    processDateSpec: function (definition, reportDate) {
        var startDate,
            endDate;


        if (definition.end_date_spec == "MANUAL") {
            endDate = Moment(definition.end_date);
        }

        if (definition.end_date_spec == "REPORT_DATE") {
            endDate = Moment(reportDate);
        }

        if (definition.start_date_spec == "MANUAL") {
            startDate = Moment(definition.start_date);
        }

        if (definition.start_date_spec == "END_SUBTRACTION") {
            var units = parseInt(definition.start_intervals);
            // If the time interval is weeks, we use days for the subtraction as
            // isoweeks isn't very reliable
            if (definition.timeInterval == "WEEK") {
                units = 7 * units;
            }
            startDate = endDate.clone().subtract(units, INTERVAL_MAP[definition.timeInterval]);
        }

        if (definition.start_date_spec == "REPORT_DATE") {
            startDate = Moment(reportDate);
        }

        return [startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD")];

    }

};

export default DateUtils;
