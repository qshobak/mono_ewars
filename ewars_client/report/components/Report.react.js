import Moment from "moment";
import ReportUtils from "../../documents/utils/ReportUtils";
import TableWidget from "../../common/analysis/TableWidget";

import SeriesChart from "../../common/analysis/SeriesChart";
import Map2 from "../../common/analysis/Map2";
import PieChart from "../../common/analysis/PieChart";
import Mapping from "../../common/analysis/Mapping";
import RawValue from "../../common/analysis/RawValue";

const WIDGETS = {
    TABLE: TableWidget,
    RAW: RawValue,
    MAP: Map2,
    CATEGORY: PieChart,
    PIE: PieChart,
    SERIES: SeriesChart,
    MAPPING: Mapping,
};

var REPORT_DATA = [];

$.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : $.param(data);
        //split params into form inputs
        var inputs = '';
        $.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        $('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
            .appendTo('body').submit().remove();
    }
};

var Report = {
    _isLoading: true,
    _widgets: [],
    _widgetsLoaded: 0,
    _widgetsTotal: 0,

    state: {
        template_id: null,
        report_date: null,
        location_id: null,
        pdf: false,
        inline: true,
        error: null
    },

    _setProgress: function (progress) {
        let progDOM = document.getElementById("progress");
        if (progDOM) {
            progDOM.style.width = progress + "%";
        }
    },

    init: function () {
        document.getElementById("progress-wrapper").style.display = "block";
        this.state.template_id = window.dt.template_id;
        this.state.report_date = window.dt.report_date;
        this.state.location_id = window.dt.location_id;
        this.state.pdf = window.dt.pdf == "true";
        this.state.inline = window.dt.inline == "true";
        if (window.dt.report != "None") {
            this.state.report = JSON.parse(window.dt.report || '{}');
        }

        if (window.dt.pdf == "True") {
            document.getElementById("progress-wrapper").style.display = "none";
        }

        if (window.dt.geom_dict) {
            window.dt.geom_dict = JSON.parse(window.dt.geom_dict);
        }

        this.state.report_template = JSON.parse(window.dt.template) || null;
        //this.state.location = window.dt.location || null;
        this.state.location = {name: JSON.parse(window.dt.location_name) || null};

        this._widgetsTotal = document.querySelectorAll('.ewarschart').length;

        ewars.subscribe("WIDGET_LOADED", function () {
            this._widgetsLoaded++;
            this._checkWidgets();
        }.bind(this));

        this._renderWidgets();
    },

    _checkWidgets: function () {
        // console.log(this._widgetsLoaded, this._widgetsTotal);
        var progress = (this._widgetsLoaded / this._widgetsTotal) * 100;
        this._setProgress(progress);
        if (this._widgetsLoaded >= this._widgetsTotal) {
            setTimeout(function () {
                $('#progress-wrapper').hide();
                $('#progress').css({width: "0%"});
            }, 1000);

            if (window.top.ewars) {
                let data = this._widgets.map((widget) => {
                    if (widget.getData) {
                        return widget.getData();
                    }
                });
                window.top.ewars.emit("DATA_LOADED", data);
            }

            ewars.emit("PRE_PDF_PRINT");
            ewars.emit("WIDGETS_LOADED");
            let flagged = document.createElement("div");
            flagged.setAttribute("id", "uxflagged");
            document.body.appendChild(flagged);
            // let eventEmitInterval = setInterval(function () {
            //     document.body.dispatchEvent(new Event('view-ready'))
            // }, 25)
            //
            // document.body.addEventListener('view-ready-acknowledged', function(){
            //     clearInterval(eventEmitInterval)
            // })
            window.status = "ALLPRESENT";
        }
    },

    _renderWidgets: function () {
        var self = this;
        var items = document.querySelectorAll('.ewarschart');
        if (items.length <= 0) {
            ewars.emit("PRE_PDF_PRINT");
            ewars.emit("WIDGETS_LOADED");
            let flagged = document.createElement("div");
            flagged.setAttribute("id", "uxflagged");
            document.body.appendChild(flagged);
            // let eventEmitInterval = setInterval(function () {
            //     document.body.dispatchEvent(new Event('view-ready'))
            // }, 25)
            //
            // document.body.addEventListener('view-ready-acknowledged', function(){
            //     clearInterval(eventEmitInterval)
            // })
            window.status = "ALLPRESENT";
        } else {

            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                if (node) {
                    node.innerHTML = "";
                    let icon = document.createElement('i');
                    icon.setAttribute('class', 'fal fa-cog fa-spin');
                    node.appendChild(icon);
                }
                self._initWidget(node, this.state.report_date, this.state.location_id)
                    .then(() => {
                    })
                    .catch(err => {
                        let el = err[0];
                        if (el) {
                            el.innerHTML = '';
                            let icon = document.createElement('i');
                            icon.setAttribute('class', 'fal fa-exclamation-triangle');
                            icon.style.color = 'red';
                            el.appendChild(icon);
                        }
                        ewars.emit('WIDGET_LOADED');
                    })
            }
        }
    },

    _initWidget: function (widgetEl, reportDate, location_uuid) {
        return new Promise((resolve, reject) => {
            if (!widgetEl) {
                reject([widgetEl, 'NO_DOM']);
                return;
            }
            let widgetId = widgetEl.getAttribute("data-id");

            if (!widgetId || widgetId == '') {
                reject([widgetEl, 'NO_CONFIG']);
                return;
            }

            let widgetConfig = this.state.report_template.data[widgetId] || null;

            if (!widgetConfig || widgetConfig == '' || widgetConfig == undefined) {
                reject([widgetEl, 'NO_CONFIG']);
                return;
            }
            if (!widgetConfig.type || widgetConfig.type == '') {
                reject([widgetEl, 'NO_CONFIG']);
                return;
            }

            let widget;

            let widgetFn = WIDGETS[widgetConfig.type];


            if (!widgetFn) {
                reject([widgetEl, 'NO_WIDGET_' + widgetConfig.type]);
                return;
            }

            if (['MAPPING', 'MAP'].indexOf(widgetConfig.type) >= 0) {
                widget = new widgetFn(widgetEl, widgetConfig, reportDate, location_uuid, true);
            } else {
                widget = widgetFn(widgetEl, widgetConfig, reportDate, location_uuid, true);
            }
            this._widgets.push(widget);
            resolve();

        })
    },

    _download: function () {
        var sheets = [];
        _.each(this._widgets, function (widget) {
            var data = widget.getData();
            sheets.push(data);
        }, this);

        var instanceName = ReportUtils.applyFormattingTags(ewars.formatters.I18N_FORMATTER(this.state.report_template.instance_name),
            this.state.report_template,
            this.state.report_date,
            this.state.location);

        $.ajax({
            url: "http://" + ewars.domain + "/authenticated",
            type: "GET",
            dataType: "json",
            success: function (resp) {
                if (resp.AUTHENTICATED) {
                    $.download("http://" + ewars.domain + "/echo_file", {
                        sourceData: JSON.stringify(dataItems),
                        fileName: instanceName + "xlsx"
                    }, "POST");
                } else {
                    ewars.prompt("fa-plus", "Authentication Required", "You must be logged in in order to download report data.", function () {
                    });
                }
            }
        })

    },

    _print: function () {
        var instanceName = ReportUtils.applyFormattingTags(ewars.formatters.I18N_FORMATTER(this.state.report_template.instance_name),
            this.state.report_template,
            this.state.report_date,
            this.state.location);

        $.download("http://" + ewars.domain + "/report/pdf", {
            tid: this.state.report_template.uuid,
            rid: Moment(this.state.report_date).format("YYYY-MM-DD"),
            lid: this.state.location.uuid,
            fileName: instanceName
        }, "POST");
    },

    _renderError: function () {
    }
};

export default Report;
