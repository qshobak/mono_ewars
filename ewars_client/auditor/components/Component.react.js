import Moment from "moment";

import {
    SelectField,
    DateField
} from "../../common/fields";

import AuditTableView from "./AuditTableView.react";
import UserAuditTableView from "./UserAuditTableView.react";

import CONSTANTS from "../../common/constants";

// Metric Definitions
import CompletenessDefinition from "./metric_definitions/CompletenessDefinition.react";
import TimelinessDefinition from "./metric_definitions/TimelinessDefinition.react";

const form_fields = {
    form_id: {
        type: 'select',
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            select: ["id", "name", "features"],
            query: {
                status: {eq: CONSTANTS.ACTIVE},
                "features.INTERVAL_REPORTING": {eq: "NOT NULL"}
            }
        }
    },
    metric: {
        type: "select",
        options: [
            ["COMPLETENESS", "Reporting Completeness"],
            ["TIMELINESS", "Reporting Timeliness"]
            //["TASK_COMPLETENESS", "Task Completeness"],
            //["TASK_TIMELINESS", "Take Timeliness"],
            //["ALERT_COMPLETENESS", "Alert Completeness"],
            //["ALERT_TIMELINESS", "Alert Timeliness"],
            //["USER_REGISTRATIONS", "User Registrations"],
            //["ASSIGNMENTS", "User Assignments"]
        ]
    }
};

var METRICS = {
    COMPLETENESS: {
        definition: CompletenessDefinition
    },
    TIMELINESS: {
        definition: TimelinessDefinition
    }
};

var FormSelectTemplate = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "item form-select";
        if (this.props.value == this.props.data[0]) className += " active";

        var formName = ewars.I18N(this.props.data[2].name);

        return (
            <div onClick={this._onClick} className={className}>
                <div className="form-name">{formName}</div>
            </div>
        )
    }
});


var Component = React.createClass({
    getInitialState: function () {
        return {
            form_id: null,
            metric: "COMPLETENESS",
            view: CONSTANTS.DEFAULT,
            search: null,
            form: null,
            start_date: Moment().subtract(7 * 6, 'd'),
            end_date: Moment()
        }
    },

    componentWillMount: function () {

    },

    componentDidMount: function () {
    },

    _onChange: function (prop, value, path, data) {
        if (['start_date', 'end_date'].indexOf(prop) >= 0) {
            this.state[prop] = Moment(value);
        } else if (prop == "form_id") {
            this.state.form_id = value;
            this.state.form = data;
        } else {
            this.state[prop] = value;
        }
        this.forceUpdate();
    },

    render: function () {
        var view;

        let interval = "DAY";

        if (this.state.form) {
            if (this.state.form.features.INTERVAL_REPORTING) {
                interval = this.state.form.features.INTERVAL_REPORTING.interval || "DAY";
            }
        }

        if (this.state.view == CONSTANTS.DEFAULT && window.user.role != "USER") {
            view = <AuditTableView
                start_date={this.state.start_date}
                end_date={this.state.end_date}
                metric={this.state.metric}
                interval={interval}
                form_id={this.state.form_id}/>;
        }

        if (this.state.view == CONSTANTS.DEFAULT && window.user.role == "USER") {
            view = <UserAuditTableView
                start_date={this.state.start_date}
                interval={interval}
                end_date={this.state.end_date}
                metric={this.state.metric}/>
        }

        var viewDef;
        if (this.state.metric) {
            var Comp = METRICS[this.state.metric].definition;
            viewDef = <Comp/>;
        }

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col overflow-auto">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">Monitoring &amp; Evaluation Auditor</div>

                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            <div
                                style={{position: "absolute", top: 0, left: 0, height: "100%", width: "100%", overflowY: "auto"}}>

                                <div className="widget">
                                    <div className="widget-header">
                                        <span>Configuration</span>
                                    </div>
                                    <div className="body">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div className="pull-right"
                                                         style={{minWidth: 200, marginTop: -5, marginRight: 8}}>
                                                        <label htmlFor="">Indicator</label>
                                                        <SelectField
                                                            name="metric"
                                                            emptyText="Metric"
                                                            config={form_fields.metric}
                                                            value={this.state.metric}
                                                            onUpdate={this._onChange}/>
                                                    </div>
                                                </td>
                                                {window.user.role != "USER" ?
                                                    <td>
                                                        <div style={{minWidth: 200, marginRight: 8, marginTop: -5}}>
                                                            <label>Form</label>
                                                            <SelectField
                                                                name="form_id"
                                                                emptyText="Form"
                                                                config={form_fields.form_id}
                                                                value={this.state.form_id}
                                                                onUpdate={this._onChange}/>
                                                        </div>
                                                    </td>
                                                    : null}
                                                <td>
                                                    <div style={{marginRight: 8, marginTop: -5}}>
                                                        <label>Start Date</label>
                                                        <DateField
                                                            name="start_date"
                                                            value={this.state.start_date}
                                                            onUpdate={this._onChange}
                                                            config={{date_type: "DAY"}}/>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style={{marginRight: 8, marginTop: -5}}>
                                                        <label>End Date</label>
                                                        <DateField
                                                            name="end_date"
                                                            value={this.state.end_date}
                                                            onUpdate={this._onChange}
                                                            config={{date_type: "DAY"}}/>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                {view}

                                <div className="widget">
                                    <div className="widget-header"><span>Indicator Description</span></div>
                                    <div className="body no-pad">
                                        {viewDef}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default Component;
