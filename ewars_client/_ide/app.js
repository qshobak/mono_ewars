// Constants
import USERS from "./constants/users";
import ICONS from "./constants/icons";
import CONSTANTS from "./constants/ewars";

// Components
//// UI
import Button from "./components/ui/Button.react";
import ButtonGroup from "./components/ui/ButtonGroup.react";
import Field from "./components/ui/Field.react";
import Link from "./components/ui/Link.react";
import Modal from "./components/ui/Modal.react";
import PanelButtons from "./components/ui/PanelButtons.react";
import TabSet from "./components/ui/TabSet.react";
import TabPane from "./components/ui/TabPane.react";

//// Layout
import HBox from "./components/layout/HBox.react";
import VBox from "./components/layout/VBox.react";
import SettingsPanel from "./components/layout/SettingsPanel.react";
import Toolbar from "./components/layout/Toolbar.react";
import Basic from "./components/layout/Basic.react";

//// Fields
import CheckboxField from "./components/fields/CheckboxField.react";
import SelectField from "./components/fields/SelectField.react";
import SwitchField from "./components/fields/SwitchField.react";
import TextAreaField from "./components/fields/TextAreaField.react";
import TextField from "./components/fields/TextField.react";
import RichEditor from "./components/fields/RichEditor.react";
import LocationSelector from "./components/fields/LocationSelector.react";
import AgeField from "./components/fields/AgeField.react";
import IndicatorField from "./components/fields/IndicatorField.react";
import DateField from "./components/fields/DateField.react";
import TimeField from "./components/fields/TimeField.react";
import DateTimeField from "./components/fields/DateTimeField.react";
import LinkedListField from "./components/fields/LinkedListField.react";

// Utilities
import * as _SignalUtils from "./lib/signalling";
import * as _FormUtils from "./lib/form_utils";
import * as _DateUtils from "./lib/date_utils";
import * as _AnalysisUtils from "./lib/analysis_utils";
import * as _Connect from "./lib/socket";



window.ewars = ewars = {};

let constants = CONSTANTS;
CONSTANTS.USERS = USERS;
CONSTANTS.ICONS = ICONS;

const IDE = {
    CONSTANTS: CONSTANTS,

    // Functionality
    _applications: window.ewars._applications,
    _hooks: window.ewars._hooks,
    Connect: _Connect,

    // Components

    // Utilities
    SignalUtils: _SignalUtils,
    FormUtils: _FormUtils,
    DateUtils: _DateUtils,
    AnalysisUtils: _AnalysisUtils
};

export default IDE;
