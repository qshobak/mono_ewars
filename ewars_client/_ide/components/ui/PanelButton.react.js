export default class PanelButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {active: false}
    }

    render() {
        var className = "ide-tab";
        if (this.props.active == this.props.id) className += " ide-tab-down";

        var iconClass = "fa " + this.props.icon;

        return (
            <div className={className} onClick={this._onClick}>
                <i className={iconClass}></i>&nbsp;{this.props.label}
            </div>
        )
    }
}
