const CONSTANTS = {
    // Reporting Intervals
    NONE: "NONE",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR",

    // ORDERING
    ASC: "ASC",
    DESC: "DESC",

    // STATES
    DEFAULT: "DEFAULT",
    DISABLED: "DISABLED",
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    DRAFT: "DRAFT",
    DELETED: "DELETED",

    // TIME FILTERS
    ONEM: "1M",
    THREEM: "3M",
    SIXM: "6M",
    ONEY: "1Y",
    YTD: "YTD",
    ALL: "ALL",

    // USER TYPES
    SUPER_ADMIN: "SUPER_ADMIN",
    INSTANCE_ADMIN: "INSTANCE_ADMIN",
    GLOBAL_ADMIN: "GLOBAL_ADMIN",
    ACCOUNT_ADMIN: "ACCOUNT_ADMIN",
    USER: "USER",
    ORG_ADMIN: "ORG_ADMIN",
    LAB_USER: "LAB_USER",
    LAB_ADMIN: "LAB_ADMIN",
    REGIONAL_ADMIN: "REGIONAL_ADMIN",
    BOT: "BOT",

    // Directions
    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD",

    // Chart Types
    SERIES: "SERIES",
    PIE: "PIE",
    CATEGORY: "CATEGORY",
    RAW: "RAW",
    TABLE: "TABLE",

    // Form Types
    PRIMARY: "PRIMARY",
    INVESTIGATIVE: "INVESTIGATE",
    LAB: "LAB",
    SURVEY: "SURVEY",

    // Colours
    RED: "red",
    GREEN: "green",
    ORANGE: "orange",
    YELLOW: "yellow",

    // Default Formats
    DATE: "YYYY-MM-DD"
};

export default CONSTANTS;