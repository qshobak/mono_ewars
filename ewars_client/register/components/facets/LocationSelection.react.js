import { Button } from "../../../common";

var LocationNode = React.createClass({
    getInitialState: function () {
        return {
            isExpanded: false,
            children: [],
            isLoaded: false
        }
    },

    _onCaretClick: function () {
        if (this.state.isLoaded) {
            this.state.isExpanded = true;
            this.forceUpdate();
        } else {
            ewars.tx("com.ewars.register.locations", [this.props.account_id, this.props.data.uuid])
                .then(function (resp) {
                    this.state.children = resp;
                    this.state.isExpanded = true;
                    this.forceUpdate();
                }.bind(this))
        }
    },

    _onLabelClick: function () {
        this.props.onClick(this.props.data);
    },

    _onPrev: function () {
        this.props.onPrev("LOCATION");
    },


    render: function () {
        var locationName = ewars.I18N(this.props.data.name);
        locationName += " - " + ewars.I18N(this.props.data.site_type_name);

        var hasHandle = false;
        if (this.props.data.child_count > 0) {
            hasHandle = true;
        }

        var handleClass = "fa fa-caret-right";
        if (this.state.isExpanded) handleClass = "fa fa-caret-down";

        var items;
        if (this.state.isExpanded) {
            items = _.map(this.state.children, function (item) {
                return <LocationNode
                    data={item}
                    selected={this.props.selected}
                    onClick={this.props.onClick}
                    account_id={this.props.account_id}/>;
            }, this);
        }

        var selectClass = "ide-col";
        selectClass += " selectable";
        if (this.props.selected == this.props.data.uuid) selectClass += " active";

        return (
            <div className="loc-node">
                <div className="wrapper">
                    <div className="ide-layout">
                        <div className="ide-row">
                            {hasHandle ?
                                <div className="ide-col handle" style={{maxWidth: 25}} onClick={this._onCaretClick}>
                                    <i className={handleClass}></i>
                                </div>
                                : null}
                            <div className={selectClass} onClick={this._onLabelClick}>
                                <div className="title">{locationName}</div>
                            </div>
                        </div>
                    </div>
                </div>
                {items ?
                    <div className="items">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }
});

var LocationSelection = React.createClass({
    getInitialState: function () {
        return {
            locations: null
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.register.locations", [this.props.data.account_id, null])
            .then(function (resp) {
                this.state.locations = resp;
                this.forceUpdate();
            }.bind(this))
    },

    _onNext: function () {
        this.props.onNext();
    },

    _onLocationSelect: function (node) {
        this.props.onChange("location_id", node.uuid);
    },

    _onPrev: function () {
        this.props.onPrev("LOCATION");
    },

    render: function () {

        var items = _.map(this.state.locations, function (item) {
            return <LocationNode
                data={item}
                selected={this.props.data.location_id}
                account_id={this.props.data.account_id}
                onClick={this._onLocationSelect}/>
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"><span>Location Selection</span></div>
                <div className="body">
                    <p>Please select the location that you would like to administrate.</p>
                    <div className="location-tree">
                        {items}
                    </div>
                </div>
                <div className="widget-footer">
                    <div className="btn-group">
                        <Button
                            label="Back"
                            colour="amber"
                            icon="fa-caret-left"
                            onClick={this._onPrev}/>
                    </div>
                    <div className="btn-group pull-right">
                        <Button
                            label="Next"
                            colour="green"
                            icon="fa-caret-right"
                            onClick={this._onNext}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default LocationSelection;
