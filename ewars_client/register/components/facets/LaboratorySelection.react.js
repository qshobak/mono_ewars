import { Button } from "../../../common";

var Item = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "item";
        if (this.props.active) className += " active";

        return (
            <div className={className} onClick={this._onClick}>
                <div className="wrapper">
                    <div className="details">
                        <div className="title">{this.props.data.name}</div>
                        <div className="sub-title">{this.props.data.domain}</div>
                    </div>
                </div>
            </div>
        )
    }
});

var LaboratorySelection = React.createClass({
    getInitialState: function () {
        return {}
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading laboratories...");
        ewars.tx("com.ewars.labs", [])
            .then(function (resp) {
                this.state.labs = resp;
                blocker.destroy();
                this.forceUpdate();
            }.bind(this))
    },

    _onNext: function () {
        if (this._validate()) {
            this.props.onNext();
        }
    },

    _validate: function () {
        if (!this.props.data.lab_id) {
            ewars.growl("Please select a laboratory");
            return false;
        }
        return true;
    },

    _selectItem: function (node) {
        this.props.onChange("lab_id", node.uuid);
    },

    _onPrev: function () {
        this.props.onPrev("LABORATORY");
    },

    render: function () {

        var accs = _.map(this.state.labs, function (lab) {
            var active = false;
            if (this.props.data.lab_id == lab.uuid) active = true;
            return (
                <Item data={lab} active={active} onClick={this._selectItem}/>
            );
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"><span>Laboratory</span></div>
                <div className="body">
                    <p>Please select the laboratory you are registering to</p>

                    <div className="select-list">
                        {accs}
                    </div>
                </div>
                <div className="widget-footer">
                    <div className="btn-group">
                        <Button
                            label="Back"
                            icon="fa-caret-left"
                            colour="amber"
                            onClick={this._onPrev}/>
                    </div>

                    <div className="btn-group pull-right">
                        <Button
                            label="Next"
                            icon="fa-caret-right"
                            colour="green"
                            onClick={this._onNext}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default LaboratorySelection;
