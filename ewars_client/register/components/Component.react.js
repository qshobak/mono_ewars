import UserDetails from "./UserDetails.react";
import Selector from "./Selector";

const CODES = {
    NO_PASSWORD: "Please provide a valid password.",
    NO_EMAIL: "Please provide a valid email address.",
    USER_PENDING_APPROVAL: "You have already requested access to the account specified, access is awaiting approval of an account administrator.",
    USER_REVOKED: "You had prior access to this account which was revoked by an account administrator, please contact an admin for assistance.",
    USER_EXISTS: "A user is already registered under the email address provided, please login or register with a different email address.",
    PENDING_VERIFICATION: "The account you are trying to request access for is currently awaiting email verification, please check your email account for an email from EWARS asking you to verify your email address.",
    USER_REMOVED: "You has prior access to this account which was revoked by an account administrator, please contact an admin for assistance.",
    SSO_EXISTS_NO_CONTEXT: "",
    PENDING_APPROVAL: "You have already submitted an access request for this account, it is currently pending approval form an Account Administrator."
};


var Completed = React.createClass({
    render: function () {
        return (
            <div className="registration-wizard">
                <h1 className="register">Registration Complete</h1>
                <div className="widget">
                    <div className="body">
                        <div className="article">
                            <p>A verification email has been sent to the email that you have provided, please check your
                                email inbox
                                and click the link provided in the email to verify your ownership of the email
                                address.</p>
                            <p>Once your email address has been verified, an administrator will review your registration
                                request
                                and either approve or reject it, you will receive an email with the outcome. If you
                                your
                                account is approved, you will be able to login at that time.</p>

                            <p>Thank you for submitting your registration request.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

class CompletedExisting extends React.Component {
    render() {
        return (
            <div className="registration-wizard">
                <h1 className="register">Request Sent</h1>
                <div className="widget">
                    <div className="body">
                        <div className="article">
                            <p>Account administrators for the account you've requested access to have been notified of
                                your request, you will receive an email once an Account Administrator approves or
                                rejects your request for access.</p>

                            <p>Once approved you will be able to log in and access the account.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const CELL_STYLE = {
    maxWidth: 150,
    fontWeight: "bold",
    textAlign: "right",
    paddingRight: 10,
    paddingTop: 8
};

class Help extends React.Component {
    render() {
        return (
            <div className="article" style={{padding: 16}}>
                <h4>Getting into EWARS</h4>

                <p>You'll notice two sections above if you are currenty not logged into the system.</p>

                <ul className="register">
                    <li className="bolder">New Users</li>
                    <li>Use this form to become a user of the EWARS system and request access to an EWARS account when
                        you haven't previously
                        registered before.
                    </li>
                    <li className="bolder">Existing Users</li>
                    <li>If you currently already registered in the system, but would like access to other accounts
                        (such as a different country or contextual account) you can do so from here.
                    </li>
                    <li className="bolder">Help</li>
                    <li>This help document.</li>
                </ul>

                <h4>Trouble logging in?</h4>

                <p>If you've already registered as a user on EWARS and can't log in, there may be a
                    number of reasons:</p>
                <ul className="register">
                    <li className="bolder">You haven't verified your email address</li>
                    <li>When you registered, the EWARS system will have sent an email to your email
                        address requesting you click a link in the email. This is to ensure that the
                        email address you registered with belongs to you. If you can't find the email,
                        please check your junk/spam folder. You can request a new verification email <a
                            href="/verifier">here</a>.
                    </li>
                    <li className="bolder">You're using an incorrect password</li>
                    <li>If you suspect the password you're using is incorrect, you can reset your
                        password <a href="/recovery">here</a></li>
                    <li className="bolder">Your account access is currently awaiting approval</li>
                    <li>Access to specific accounts under EWARS is subject to approval from an administrator of the
                        account. Until an administrator approves your registration or access request, you will not
                        be able to access the account. Once access is approved you will receive an email at the address
                        you registered with notifying you that you can now log in.
                    </li>
                    <li className="bolder">An Account Administrator revoked your access</li>
                    <li>If you only have access to a single account, an account administrator may have revoked your
                        access for internal reasons. Please contact an Account administrator for the account in question
                        to find out why.
                    </li>
                    <li className="bolder">Poor device connection</li>
                    <li>If you are attempting to log in using either the EWARS mobile or desktop applications, please
                        check that you have internet access in order for the application to access EWARS to
                        authenticate. You will only need to be connected to the internet for the initial login and
                        syncing for these devices.
                    </li>
                    <li className="bolder">Account Controlled Users</li>
                    <li>If your user is an <strong>account-controlled user</strong> you can not request access to other accounts. Account controlled users are users which are created strictly to work with the account in question in order that account administrators can exercise a higher level of control (changing passwords for instance). Because of this, account-controlled users can not be added to other accounts.</li>
                </ul>

                <h4>Single Sign On</h4>
                <p>The EWARS system uses a single sign-on scheme for managing users. This means that you can use your
                    email address and password to access multiple different accounts without having to re-register the
                    same address or manage multiple passwords.</p>

                <h4>More help?</h4>

                <p>If you're not sure what to do, or are experiencing problems not addressed above
                    please contact <a href="mailto:support@ewars.ws">support@ewars.ws</a></p>
            </div>
        )
    }
}

class ExistingUserRequest extends React.Component {
    constructor(props) {
        super(props);

        let email = "";
        if (window.user) email = window.user.email;
        this.state = {
            show: false
        }
    }

    componentWillMount() {
    }

    _onSelect = (data) => {
        this.props.onChange("account", data);
        this.setState({
            show: false
        })
    };

    _selectAccount = () => {
        this.setState({show: true});
    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({show: false});
    };

    _clearAccount = () => {
        this.props.onChange("account_id", null);
    };

    _onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    render() {
        console.log(this.props.data);
        let readOnly = false;
        if (window.user) readOnly = true;

        let emailLabel = "Email *";
        if (window.user) emailLabel = "Your Account Email";

        if (window.user) {
            if (window.user.system) {
                return (
                    <div>
                        <div className="article"
                            style={{paddingRight: 150, paddingLeft: 150, paddingTop: 30, paddingBottom: 30, marginBottom: 40}}>
                            <p className="register">You are signed in as an account-controlled user, account-controlled users are not allowed to access accounts other than their origin account at this time.</p>
                        </div>
                    </div>
                )
            }
        }

        return (
            <div>
                <div className="article"
                     style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30, marginBottom: 40}}>
                    <div className="ide-layout">

                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 150}}></div>
                            <div className="ide-col">
                                <p>Please fill out the details below to request access to an account.</p>
                            </div>

                        </div>

                        {!window.user ?
                        <ewars.d.Row style={{marginBottom: 8}}>
                            <ewars.d.Cell style={CELL_STYLE}>
                                {emailLabel}
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                <input
                                    type="text"
                                    name="email"
                                    disabled={readOnly}
                                    onChange={this._onChange}
                                    value={this.props.data.email}
                                    placeholder="Enter your email..."/>

                            </ewars.d.Cell>
                        </ewars.d.Row>
                            : null}

                        {!window.user ?
                            <ewars.d.Row style={{marginBottom: 8}}>
                                <ewars.d.Cell style={CELL_STYLE}>
                                    Password *
                                </ewars.d.Cell>
                                <ewars.d.Cell>
                                    <input
                                        type="password"
                                        name="password"
                                        disabled={readOnly}
                                        onChange={this._onChange}
                                        value={this.props.data.password}
                                        placeholder="Enter your password..."/>

                                </ewars.d.Cell>
                            </ewars.d.Row>
                            : null}

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                New Account *
                            </div>
                            <div className="ide-col">

                                {!this.props.data.account ?
                                    <ewars.d.Button label="Select Account" onClick={this._selectAccount}/>
                                    :
                                    <div className="reg-item">
                                        <div className="ide-row">
                                            <div
                                                className="ide-col result-name">{ewars.I18N(this.props.data.account.name)}</div>
                                            <div className="ide-col other" style={{maxWidth: 30}}>
                                                <ewars.d.Button icon="fa-times" onClick={this.props.clearAccount}/>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.show ?
                        <ewars.d.Shade
                            title="Select Item"
                            icon="fa-search"
                            onAction={this._onModalAction}
                            shown={this.state.show}>
                            <Selector
                                resource={"account"}
                                onSelect={this._onSelect}/>
                        </ewars.d.Shade>
                        : null
                }
            </div>
        )
    }
}

var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var Component = React.createClass({
    getInitialState: function () {
        let email = "";
        if (window.user) email = window.user.email;

        return {
            data: {
                name: "",
                email: "",
                confirm_email: "",
                password: "",
                confirm_password: "",
                account_id: "",
                org_id: "",
                user_id: null
            },
            exist: {
                email: email,
                password: "",
                account_id: null
            },
            view: "NEW"
        }
    },

    _validate: function () {
        if (this.state.view == "EXIST") {
            if (window.user) {
                // A logged in user exists
                if (!this.state.data.account_id || this.state.data.account_id == "") {
                    this.setState({error: _l("Please select an account")});
                    return;
                }

                // We're OK, process the request
                this._doAccessRequest();

            } else {
                // No logged in user, so we need to authenticate
                if (!this.state.data.email || this.state.data.email == "") {
                    this.setState({error: _l("Please enter a valid email")});
                    return;
                }

                if (!re.test(this.state.data.email)) {
                    this.setState({error: _l("Please enter a valid email")});
                    return;
                }

                if (!this.state.data.password || this.state.data.password == "") {
                    this.setState({error: l("Please enter a valid password")});
                    return;
                }

                if (!this.state.data.account_id || this.state.data.account_id == "") {
                    this.setState({error: _l("Please select an account")});
                    return;
                }

                // We're OK, can now process the request
                this._doAccessRequest();
            }
            return;
        }

        if (!this.state.data.name || this.state.data.name == "") {
            this.setState({error: "Please provide a valid name."});
            return;
        }

        if (!this.state.data.email || this.state.data.email == "") {
            this.setState({error: "Please provide a valid email"});
            return;
        }

        if (!re.test(this.state.data.email)) {
            this.setState({error: _l("Please provide a valid email address")});
            return;
        }

        if (this.state.data.email != this.state.data.confirm_email) {
            this.setState({error: "Emails do not match"});
            return;
        }

        if (!this.state.data.password || this.state.data.password == "") {
            this.setState({error: "Please provide a valid password: passwords must be a minimum of 6 characters one of which must be a number or non-standard character"});
            return;
        }

        if (this.state.data.password != this.state.data.confirm_password) {
            this.setState({error: "Passwords do not match"});
            return;
        }

        if (!this.state.data.org_id || this.state.data.org_id == "") {
            this.setState({error: "Please select a valid organization, or select Other if your organization isn't represented."});
            return;
        }

        if (!this.state.data.account_id || this.state.data.account_id == "") {
            this.setState({error: "Please select an account to register for."});
            return;
        }

        this._doRegister();
    },

    _doAccessRequest: function() {
        let bl = new ewars.Blocker(null, "Requesting access...");
        let data = JSON.stringify({
            type: "EXISTS",
            email: this.state.data.email,
            password: this.state.data.password,
            account_id: this.state.data.account_id
        });

        let r = new XMLHttpRequest();
        r.open("POST", "/register/access");
        r.onreadystatechange = () => {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                this.setState({
                    error: "An unknown error occurred, please contact a system administrator at support@ewars.ws"
                });
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        this.setState({error: CODES[resp.code]});
                    } else {
                        this.setState({completed: true});
                    }
                }
            }
        };

        r.send(data);
    },

    _doRegister: function () {
        let bl = new ewars.Blocker(null, "Registering...");
        let data = JSON.stringify({
            name: this.state.data.name,
            email: this.state.data.email,
            account_id: this.state.data.account_id,
            password: this.state.data.password,
            org_id: this.state.data.org_id
        });

        let r = new XMLHttpRequest();
        r.open("POST", "/register");
        r.onreadystatechange = () => {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                this.setState({
                    error: "An unknown error occurred, please contact a system administrator at support@ewars.ws"
                })
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        this.setState({
                            error: CODES[resp.code]
                        })
                    } else {
                        this.setState({completed: true});
                    }
                }
            }
        };

        r.send(data);
    },

    componentWillMount() {
        if (window.user) this.state.view = "EXIST";
    },

    _showNew: function () {
        this.setState({view: "NEW"});
    },

    _showExist: function () {
        this.setState({view: "EXIST"});
    },

    _showHelp: function () {
        this.setState({view: "HELP"});
    },

    _onExistChange: function (prop, value) {
        this.setState({
            ...this.state,
            exist: {
                ...this.state.exist,
                [prop]: value
            }
        })
    },

    _onChange: function (prop, value) {
        let account_id = this.state.data.account_id;
        if (prop === "account") {
            account_id = value.id;
        }

        if (prop == "account_id") {
            account_id = value;
        }
        this.setState({
            ...this.state,
            error: null,
            data: {
                ...this.state.data,
                [prop]: value,
                account_id: account_id
            }
        })
    },

    clearAccount: function() {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                account: null,
                account_id: null
            }
        });
    },

    _back: function () {
        document.location = "/";
    },

    render: function () {
        if (this.state.completed) {
            if (this.state.view == "NEW") return <Completed/>;
            if (this.state.view == "EXIST") return <CompletedExisting/>;
        }

        let view,
            buttonLabel = "Submit registration",
            title = "New User Registration",
            content = "Please follow the steps below to register for an account.";

        if (this.state.view == "NEW") {
            view = <UserDetails onChange={this._onChange} data={this.state.data}/>;
        } else if (this.state.view == "EXIST") {
            view = <ExistingUserRequest onChange={this._onChange} data={this.state.data}/>;
            title = "Account Access Request";
            content = "Fill out the form below to request access to an account.";
            buttonLabel = "Request access";
        } else {
            view = <Help/>;
            title = "Registration Help";
            content = "Guidance on registering an account with EWARS.";
        }

        let newClass = "ux-tab";
        if (this.state.view == "NEW") newClass += " active";
        let existClass = "ux-tab";
        if (this.state.view == "EXIST") existClass += " active";
        let helpClass = "ux-tab";
        if (this.state.view == "HELP") helpClass += " active";

        let backButton;
        if (window.user) {
            backButton = (
                <div className="btn-group">
                    <ewars.d.Button
                        label="Back to EWARS"
                        onClick={this._back}/>
                </div>
            )
        }

        let hideSubmit = false;
        if (this.state.view == "HELP") hideSubmit = true;
        if (this.state.completed) hideSubmit = true;
        if (window.user) {
            if (window.user.system) hideSubmit = true;
        }

        let error;
        if (this.state.error) {
            error = (
                <div className="error"><i className="fal fa-exclamation-triangle"></i>&nbsp;{this.state.error}</div>
            )
        }

        return (
            <div className="registration-wizard">
                <h1 className="register">{title}</h1>
                <p className="register">{content}</p>

                <div className="widget">
                    <div className="widget-header">
                        <div className="ux-tabs">
                            {!window.user ?
                                <div onClick={this._showNew} className={newClass}>New Users</div>
                                : null}
                            <div onClick={this._showExist} className={existClass}>Existing Users</div>
                            <div onClick={this._showHelp} className={helpClass}>
                                <i className="fal fa-question-circle"></i>&nbsp;Help
                            </div>
                        </div>
                    </div>
                    <div className="body no-pad">
                        {error}
                        {view}
                    </div>
                    {!hideSubmit ?
                        <div className="widget-footer">
                            {backButton}
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label={buttonLabel}
                                    color="green"
                                    icon="fa-submit"
                                    onClick={this._validate}/>
                            </div>
                        </div>
                        : null}
                </div>

                <p className="register">Already have an account? Sign in <a href="/login">here</a></p>
            </div>
        )
    }
});

export default Component;
