var NewUserForm = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        return (
            <div>
                <label for="account">Country</label>
                <select name="account_id" required="true" className="form-control"
                        value={this.state.data.account_id} onChange={this.props.onChange}>
                    {accountOptions}
                </select>
                <br />

                <label for="org">Organization</label>
                <select name="org_id" required="true" className="form-control"
                        value={this.state.data.org_id} onChange={this.props.onChange}>
                    {orgOptions}
                </select>

                <div className="clearer"></div>
                <br />

                <label for="name">Name</label>
                <input name="name" type="text" className="form-control"
                       onChange={this.props.onChange} value={this.state.data.name}/>
                <br/>

                <label>Email</label>
                <input type="text" ref="emailInput" name="email" className="form-control"
                       onChange={this.props.onChange}
                       value={this.state.data.email}/>
                <br />

                <label>Confirm Email Address</label>
                <input type="email" ref="emailConfirm" name="email_confirm" className="form-control"
                       onChange={this.props.onChange} value={this.state.data.confirm_email}/>
                <br />

                <label>Password</label>
                <input type="password" name="password" className="form-control"
                       onChange={this.props.onChange}
                       value={this.state.data.password}/>
                <br />

                <label>Confirm Password</label>
                <input type="password" name="password_confirm" className="form-control"
                       onChange={this.props.onChange}
                       value={this.state.data.confirm_password}/>
                <br />

                <div className="btn-group">
                    <button className="green" onClick={this.props.attemptRegister}>Register</button>
                </div>
            </div>
        )
    }
});

export default NewUserForm;
