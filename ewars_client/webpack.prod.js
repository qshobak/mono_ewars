const Merge = require("webpack-merge");
const CommonConfig = require("./webpack.common.js");
const CompressionPlugin = require("compression-webpack-plugin");
const path = require("path");
const webpack = require("webpack");



module.exports = Merge(CommonConfig, {
    mode: "production",
    output: {
        filename: "[name]-[hash].js",
        sourceMapFilename: "[name]-[hash].map"
    },

    optimize: {
        minimize: true
    },

    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify("production")
            }
        }),
        new webpack.BannerPlugin({
            banner: 'ewars.__NAME__ = "[name]"; ewars.__VERSION__ = "[hash]"',
            raw: true
        }),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "zopfli",
            test: /\.(js|html)$/,
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: true
        })
    ]
});
