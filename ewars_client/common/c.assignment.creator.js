import Moment from "moment";
import Button from "../ButtonComponent.react";
import Form from "../Form.react";
import Modal from "../ModalComponent.react";
import Validator from "../../utils/Validator";

function _convertFormsToOptions(forms) {
    var options = [];
    _.each(forms, function (form) {
        options.push([
            form.id,
            ewars.formatters.I18N_FORMATTER(form.name)
        ])
    }, this);
    return options;
}

var LABEL_MAP = {
    "MONTH": "Monthly",
    "WEEK": "Weekly",
    "DAY": "Daily",
    "YEAR": "Annual"
};

var AssignmentCreatorComponent = React.createClass({
    getInitialState: function () {
        return {
            forms: null,
            form: null,
            buttonEnabled: true,
            errors: [],
            mainErrors: null,
            assignment: {
                form: null,
                location_uuid: null
            }
        };
    },

    componentWillMount: function () {

        this.state.assignment = {
            form: null,
            location_uuid: null
        };

        ewars.tx("com.ewars.user.assignables", [])
            .then(function (resp) {
                this.state.forms = _convertFormsToOptions(resp);
                this.state.rawForms = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onFormSelected: function (data) {
        this.setState({
            form: data
        })
    },

    _handleAssignChange: function (data, prop, value) {
        var assign = this.state.assignment;
        assign[prop] = value;

        this.setState({
            assignment: assign
        })
    },

    _createAssignment: function () {
        this.state.errors = null;
        this.state.mainErrors = null;

        var valid = Validator(this._buildForm(), this.state.assignment);

        if (valid) {
            this.setState({
                errors: valid
            });
            return;
        }

        this.setState({
            buttonEnabled: false
        });

        ewars._connect().then(function (session) {
            session.call("com.ewars.reporting.create_assignment", [this.props.user.id, this.state.assignment])
                .then(
                    function (resp) {
                        if (resp.d.result == false) {
                            this.state.mainErrors = resp.d.message;
                        } else {
                            this.props.onAfterSave();
                            if ([1, 2, 5].indexOf(window.user.group_id) >= 0) {
                                ewars.notifications.notification("fa-bell", "Assignment Created", "The requested assignment has been added to the user " + this.props.user.name, null, 'green');
                            } else {
                                ewars.notifications.notification("fa-bell", "Assignment Requested", "The assignment specified has been requested and is awaiting approval by an account administrator.", null, 'green')
                            }
                        }

                        this.state.buttonEnabled = true;
                        this.state.assignment = {
                            form: null,
                            location_uuid: null
                        };
                        this.state.form = null;
                        this.forceUpdate();
                    }.bind(this)
                )
        }.bind(this))
    },

    _handleOptionsChange: function (data, prop, value) {
        if (prop == 'form_id') {
            var form = _.find(this.state.rawForms, function (form) {
                if (form.id == value) return true;
            });
            this.state.assignment.form_id = value;
            this.setState({
                form: form,
                assignment: this.state.assignment
            })
        } else {
            this.state.assignment[prop] = value;
            this.forceUpdate();
        }
    },

    _buildForm: function () {
        var formDefinition = {};

        formDefinition.form_id = {
            type: "select",
            label: "Form",
            required: true,
            options: this.state.forms,
            help: "Select the form you would like to be able to perform data entry on"
        };

        if (this.state.form) {
            if (this.state.form.location_aware) {
                formDefinition.location_uuid = {
                    type: "location",
                    label: "Assignment Location",
                    required: true,
                    hideInactive: true,
                    parent_id: window.user.account.location_id,
                    location_type: this.state.form.location_type[0] || null,
                    help: "Select the location you would like to be able to report for."
                }
            }

            if (this.state.form.time_interval != "WEEK") {
                formDefinition.start_date = {
                    type: "date",
                    date_type: this.state.form.time_interval,
                    label: "Assignment Start Date",
                    required: true,
                    help: "Select the date that this assignment should start"
                };

                formDefinition.end_date = {
                    type: "date",
                    date_type: this.state.form.time_interval,
                    label: "Assignment End Date",
                    required: true,
                    help: "Select the date that this assignment should end (estimate)"
                }
            } else if (this.state.form.time_interval == "WEEK") {
                formDefinition.start_date = {
                    type: "week",
                    label: "Assignment Start Date",
                    required: true,
                    help: "Select the date that this assignment should start"
                };

                formDefinition.end_date = {
                    type: "week",
                    label: "Assignment End Date",
                    required: true,
                    help: "Select the date that this assignment should end (estimate)"
                }
            }
        }

        return formDefinition;
    },

    _cancelAssign: function () {
        this.setState({
            assignment: {
                form: null,
                location_uuid: null
            },
            mainErrors: null,
            form: null
        });

        this.props.onCancel();
    },

    render: function () {
        var formDef = this._buildForm();

        var buttons = [
            {
                label: "Request Permission",
                colour: "green",
                onClick: this._createAssignment,
                enabled: this.state.buttonEnabled
            },
            {label: "Cancel", colour: "red", onClick: this._cancelAssign}
        ];

        var hasMainErrors = false;
        if (this.state.mainErrors) hasMainErrors = ewars.formatters.I18N_FORMATTER(this.state.mainErrors);

        return (
            <Modal
                buttons={buttons}
                width={500}
                visible={this.props.visible}>
                <h3>Assignment Request</h3>

                {hasMainErrors ?
                    <div className="error">{hasMainErrors}</div>
                    : null}

                <Form
                    ref="assignForm"
                    definition={formDef}
                    updateAction={this._handleOptionsChange}
                    errors={this.state.errors}
                    readOnly={false}
                    data={this.state.assignment}/>
            </Modal>
        )
    }

});

export default AssignemntCreatorComponent;
