import Button from "./c.cmp.button";

class ButtonSet extends React.Component {
    static defaultProps = {
        value: null,
        data: {
            n: null,
            context_help: {},
            o: []
        },
        ro: false
    };

    static defaultProps = {
        context_help: null,
        value: null
    };

    _onClick = (data) => {
        if (this.props.ro) return;
        this.props.onChange(this.props.data.n, data[0], this.props.p || this.props.n);
    };

    render() {
        let contextHelp;
        if (this.props.data.context_help) {
            if (this.props.value != null) {
                contextHelp = __(this.props.data.context_help[this.props.value])
            }
        }
        return (
            <div className="btn-group formed">
                {this.props.data.o.map((button, index) => {
                    let key = `BUTTON_${index}`;
                    return <ewars.d.Button
                        icon={button[2] || null}
                        label={button[1]}
                        key={key}
                        active={this.props.value == button[0]}
                        onClick={this._onClick}
                        data={button}/>
                })}
                <div className="clearer" style={{display: "block", paddingBottom: "5px"}}></div>
                {contextHelp ?
                    <p>{contextHelp}</p>
                    : null}
            </div>
        )
    }
}

export default ButtonSet;
