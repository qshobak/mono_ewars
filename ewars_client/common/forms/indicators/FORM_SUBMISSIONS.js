const FORM_SUBMISSIONS = {
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            }
        }
    },
    metric: {
        type: "select",
        label: "Dimension",
        options: [
            ["SUBMITTED", "Submitted"],
            ["LATE", "Late"],
            ["EXPECTED", "Expected"],
            ["MISSING", "Missing"],
            ["COMPLETENESS", "Completeness"],
            ["TIMELINESS", "Timeliness"]
        ]
    },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    source: {
        type: "select",
        label: "Source",
        options: [
            ["ANDROID", "Mobile"],
            ["SYSTEM", "Web"],
            ["DESKTOP", "Desktop"],
            ["SMS", "SMS"]
        ]
    }
};

export default FORM_SUBMISSIONS;

