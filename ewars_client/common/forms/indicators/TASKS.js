const TASKS = {
    type: {
        type: "select",
        label: "Task Type",
        options: [
            ["REGISTRATION", "Registration"],
            ["AMENDMENT", "Amendment"],
            ["DISCARD", "Discard"]
        ]
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ["ANY", "Any"],
            ["PENDING", "Pending"],
            ["APPROVED", "Approved"],
            ["REJECTED", "Rejected"]
        ]
    }
};

export default TASKS;

