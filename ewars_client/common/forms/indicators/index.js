import ALERTS from "./ALERTS";
import ASSIGNMENTS from "./ASSIGNMENTS";
import DEVICES from "./DEVICES";
import FORM_SUBMISSIONS from "./FORM_SUBMISSIONS";
import FORMS from "./FORMS";
import LOCATIONS from "./LOCATIONS";
import OLD_ALERTS from "./OLD_ALERTS";
import TASKS from "./TASKS";
import USERS from "./USERS";

export {
    ALERTS,
    ASSIGNMENTS,
    DEVICES,
    FORM_SUBMISSIONS,
    FORMS,
    LOCATIONS,
    OLD_ALERTS,
    TASKS,
    USERS
}
