const DEVICES = {
    type: {
        type: "select",
        label: "Device Type",
        options: [
            ["ANDROID", "Android"],
            ["IOS", "iOS"],
            ["DESKTOP", "Desktop"],
            ["NODE", "Node"]
        ]
    },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ["ANY", "Any"],
            ["SYNCED", "Synced"],
            ["UNSYNCED", "UnSynced"]
        ]
    }
};

export default DEVICES;

