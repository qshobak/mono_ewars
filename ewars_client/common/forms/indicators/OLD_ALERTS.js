const OLD_ALERTS = {
    alarm_id: {
        type: "select",
        label: "Alarm",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    state: {
        type: "select",
        label: "State",
        options: [
            ["OPEN", "Open"],
            ["CLOSED", "Closed"],
            ["AUTODISCARDED", "Auto-Discarded"]
        ]
    },
    stage: {
        type: "select",
        label: "Stage",
        options: [
            ["VERIFICATION", "Verification"],
            ["RISK_ASSESS", "Risk Assessment"],
            ["RISK_CHAR", "Risk Characterization"],
            ["OUTCOME", "Outcome"]
        ]
    },
    stage_state: {
        type: "select",
        label: "Stage State",
        options: [
            ["PENDING", "Pending"],
            ["COMPLETED", "Completed"]
        ]
    },
    risk: {
        type: "select",
        label: "Risk",
        options: [
            ["LOW", "Low risk"],
            ["MODERATE", "Moderate risk"],
            ["HIGH", "High risk"],
            ["SEVERE", "Very high risk"]
        ]
    },

};

export default OLD_ALERTS;

