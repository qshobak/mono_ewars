const FORM_SERIES = [
    {
        _o: 0,
        t: "H",
        l: "General"
    },
    {
        _o: 1,
        t: "TEXT",
        n: "title",
        l: "Title",
        r: true
    },
    {
        _o: 1.1,
        t: "TEXT",
        n: "subtitle",
        l: "Sub-title",
        r: false
    },
    {
        _o: 2,
        t: "SELECT",
        n: "interval",
        l: "Time Interval",
        r: true,
        o: [
            ["DAY", "DAY"],
            ["WEEK", "WEEK"],
            ["MONTH", "MONTH"],
            ["YEAR", "YEAR"]
        ]
    },
    {
        _o: 3,
        t: "H",
        l: "Period"
    },
    {
        _o: 4,
        t: "PERIOD",
        l: "Period",
        n: "period",
        r: true
    },
    {
        _o: 4.1,
        t: "H",
        l: "Location"
    },
    {
        _o: 5,
        t: "LOCATION",
        l: "Location",
        n: "location",
        r: true
    }
]

export default FORM_SERIES;