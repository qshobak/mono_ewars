export default {
    t_general: {
        t: "H",
        l: "General Settings"
    },
    show_title: {
        t: "o",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    show_subtitle: {
        t: "o",
        l: "Show sub-title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    t_table: {
        t: "H",
        l: "Table Styling"
    },
    header_bgd: {
        l: "Header Bgd"
    },
    header_color: {
        l: "Header Colour"
    },
    alt_rows: {
        t: "o",
        l: "Striped rows",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    row_head_bgd: {
        l: "Row header bgd"
    },
    row_header_color: {
        l: "Row header color"
    }
}

