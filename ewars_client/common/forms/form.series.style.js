const FORM_SERIES_STYLE = [
    {
        _o: 0,
        t: "H",
        l: "Y Axis"
    },
    {
        _o: 1,
        t: "BUTTONSET",
        n: "hide_y_axis",
        l: "Show Y axis label",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        t: "TEXT",
        n: "y_axis_label",
        l: "Y axis label",
        c: [
            "ALL",
            "hide_y_axis:eq:false"
        ]
    },
    {
        _o: 3,
        t: "SELECT",
        n: "y_axis_format",
        l: "Y axis format",
        o: [
            ["0", "0"],
            ["0,0", "0,0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]
        ]
    },
    {
        _o: 4,
        t: "BUTTONSET",
        l: "Show decimals",
        n: "y_axis_allow_decimals",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 5,
        t: "TEXT",
        n: "max_y_value",
        l: "Max. Y value"
    },
    {
        _o: 6,
        t: "SELECT",
        n: "y_tick_interval",
        l: "Y axis tick interval"
    },
    {
        _o: 7,
        t: "H",
        l: "X Axis"
    },
    {
        _o: 8,
        t: "BUTTONSET",
        n: "hide_x_labels",
        l: "Hide X axis labels",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 9,
        t: "TEXT",
        n: "x_label_rotation",
        l: "X axis label rotation"
    }
];

export default FORM_SERIES_STYLE;