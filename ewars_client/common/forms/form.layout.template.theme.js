const FORM_LAYOUT_TEMPLATE_THEME = [
    {
        _o: 0,
        t: "SELECT",
        l: "Theme",
        n: "theme",
        o: [
            ["DEFAULT", "Default"]
        ]

    },
    {
        _o: 0.1,
        t: "SELECT",
        l: "Orientation",
        n: "orientation",
        o: [
            ["PORTRAIT", "Portrait"],
            ["LANDSCAPE", "Landscape"]
        ]
    },
    {
        _o: 0.2,
        t: "SELECT",
        l: "Page size",
        n: "pSize",
        o: [
            ["A4", "A4"],
            ["LETTER", "Letter"],
            ["TABLOID", "Tabloid"]
        ]
    },
    {
        _o: 1,
        t: "COLOR",
        l: "Background colour",
        n: "bgdColor"
    }
]

export default FORM_LAYOUT_TEMPLATE_THEME;