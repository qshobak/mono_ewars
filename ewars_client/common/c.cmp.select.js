import { Layout, Row, Cell } from "./layout";

class SelectOption extends React.Component {
    _select = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="block hoverable" onClick={this._select}>
                <div className="block-content hoverable">
                    {__(this.props.data[1])}
                </div>
            </div>
        )
    }
}

class SingleSelect extends React.Component {
    _isLoaded = false;
    _initialLoaded = false;

    static defaultProps = {
        o: [],
        os: null,
        value: null,
        ro: false,
        searchProp: null
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            options: props.o || [],
            rawOptions: null,
            align: "left",
            search: ''
        };

        this.handleChange = this._onSearchChange.bind(this);
        this.emitChangeDebounced = ewars.debounce(this.emitChange, 3);
    }

    componentDidMount() {
        window.__hack__.addEventListener("click", this.handleBodyClick);

        let winWidth = this.refs.selector.parentNode.clientWidth;
        let right = this.refs.selector.getBoundingClientRect().left + this.refs.selector.getBoundingClientRect().width;
        let left = this.refs.selector.getBoundingClientRect().right + this.refs.selector.getBoundingClientRect().width;
        if ((winWidth - right) < 200) {
            this.setState({
                align: "right"
            })
        }

        if ((winWidth - left) < 200) {
            this.setState({
                align: "left"
            });
        }
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this.handleBodyClick);
    }

    handleBodyClick = (e) => {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(e.target)) {
                this.setState({
                    shown: false,
                    search: ''
                })
            }
        }
    };

    _handleClick = (e) => {
        e.stopPropagation();
    };

    componentWillMount() {
        this._id = ewars.utils.uuid();
        if (this.props.os && this.props.value) {
            this._init(this.props);
        } else {
            this._initialLoaded = true;
            this.state.options = this.props.o;
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.os && nextProps.value) {
            if (nextProps.value != this.props.value) {
                this._init(nextProps);
            }
        } else {
            this._initialLoaded = true;
            this.state.options = nextProps.o;
        }
    }

    _select = (data) => {
        this.setState({
            shown: false
        });
        this.props.onSelect(data[0]);
    };

    _init = (props) => {
        if (props.os && props.value) {
            // Need to initialized and load up the value for the selected value
            if (!this._initialLoaded) {
                this._initialLoaded = true;
                let resource = props.os[0].toLowerCase();
                let selects = [props.os[1], props.os[2]];


                ewars.tx("com.ewars.resource", [resource, props.value, selects, null])
                    .then((resp) => {
                        if (resp) {
                            this.setState({
                                options: [
                                    [
                                        resp[selects[0]],
                                        __(resp[selects[1]])
                                    ],
                                ]
                            })
                        }
                    })
            }
        }
    };

    _toggle = (e) => {
        if (e) e.stopPropagation();

        if (this.props.os) {
            if (!this._isLoaded) {
                let resource = this.props.os[0];
                let select = [this.props.os[1], this.props.os[2]];

                ewars.tx("com.ewars.query", [resource, select, this.props.os[3] || null, null, null, null, null])
                    .then((resp) => {
                        this._isLoaded = true;
                        this.setState({
                            options: resp.map((item) => {
                                return [
                                    item[this.props.os[1]],
                                    __(item[this.props.os[2]]),
                                    item
                                ]
                            }),
                            search: '',
                            rawOptions: resp,
                            shown: true
                        })

                    })
            } else {
                this.setState({
                    shown: !this.state.shown,
                    search: ''
                })
            }
        } else {
            this.setState({
                shown: !this.state.shown,
                search: ''
            })
        }

    };

    _onSearchChange = (e) => {
        this.emitChangeDebounced(e.target.value);
    };

    emitChange = (value) => {
        this.setState({
            search: value
        })
    };

    render() {
        let dispValue = __("NONE_SELECTED");

        if (this.props.value) {
            this.state.options.forEach((item) => {
                if (item[0] == this.props.value) {
                    dispValue = item[1]
                }
            })
        }

        let style = {
            zIndex: 9999
        };
        if (this.state.align == "right") {
            style.right = 0;
            style.left = "auto";
        } else {
            style.left = 0;
            style.right = "auto";
        }

        let options = this.state.options;
        if (this.state.search != '') {
            let lowered = this.state.search.toLowerCase();
            options = this.state.options.filter(item => {
                return item[1].toLowerCase().indexOf(lowered) >= 0;
            })
        }

        return (
            <div ref="selector" className="ew-select">
                <div className="handle" onClick={this._toggle} style={{maxHeight: "26px", overflow: "hidden"}}>
                    <Row>
                        <Cell style={{overflowY: "hidden", lineHeight: "12px"}}>
                            {dispValue}
                        </Cell>
                        {!this.props.ro ?
                            <Cell borderLeft={true} width="30px"
                                          style={{textAlign: "center", background: "rgba(0,0,0,0.1)"}}>
                                <i className="fal fa-caret-down"></i>
                            </Cell>
                            : null}
                    </Row>
                </div>
                {this.state.shown && !this.props.ro ?
                    <div className="ew-select-data" style={style}>
                        {this.props.searchProp ?
                            <input
                                placeholder="Search..."
                                onChange={this._onSearchChange}
                                value={this.state.search}
                                type="text"/>
                            : null}
                        <div className="block-tree" style={{position: "relative", padding: 0}}>
                            {options.map((item) => {
                                return (
                                    <SelectOption
                                        onClick={this._select}
                                        data={item}/>
                                )
                            })}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

class MultiSelect extends React.Component {
    render() {
        return (
            <div></div>
        )
    }
}

export default class SelectField extends React.Component {
    static defaultProps = {
        data: {
            o: [],
            os: null,
            c: [],
            l: "",
            s: false,
            r: false,
            ml: false

        },
        searchProp: null,
        ro: false
    };

    _select = (value) => {
        this.props.onChange(this.props.data.n, value);
    };

    render() {
        if (this.props.data.ml) {
            return (
                <MultiSelect
                    ro={this.props.ro}
                    onAction={this._action}
                    value={this.props.value}
                    o={this.props.data.o}/>
            )
        } else {
            return (
                <SingleSelect
                    searchProp={this.props.searchProp}
                    ro={this.props.ro}
                    onSelect={this._select}
                    onAction={this._action}
                    value={this.props.value}
                    os={this.props.data.os}
                    o={this.props.data.o}/>
            )
        }

    }
}
