var Spinner = React.createClass({
    render: function () {
        return (
            <div className="spinner-wrap">
                <div className="spinner">
                    <i className="fal fa-spin fa-circle-o-notch"></i>
                </div>
            </div>
        )
    }
});

export default Spinner;
