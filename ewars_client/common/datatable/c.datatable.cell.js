import {
    DateField,
    SelectField,
    TextAreaField,
    SwitchField,
    LocationField as LocationSelect,
    NumericField as NumberField,
    TextField
} from "../fields/";

import Button from "../c.button";
import ContextMenu from "../c.context_menu";

const TYPES = {
    date: DateField,
    select: SelectField,
    text: TextField,
    textarea: TextAreaField,
    switch: SwitchField,
    location: LocationSelect,
    number: NumberField
};


const CELL_ACTIONS = [
    ["fa-save", "SAVE", "Save"],
    ["fa-times", "CANCEL", "Cancel"]
]

class DataTableCell extends React.Component {
    static defaultProps = {
        editable: false,
        editor: null
    };

    constructor(props) {
        super(props);

        this.state = {
            context: false,
            edit: false,
            tempVal: null
        }
    }

    componentWillMount() {
        window.__hack__.addEventListener("contextmenu", this._clear);
        ewars.subscribe("UNMOUNT_EDIT", this._unmountEdit);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("contextmenu", this._clear);
    }

    _unmountEdit = () => {
        this.setState({
            edit: false
        })
    };

    _clear = (evt) => {
        if (this.refs.item) {
            if (!this.refs.item.contains(evt.target)) {
                this.setState({
                    context: false
                })
            }
        }
    };

    _edit = () => {
    };

    _context = (e) => {
        e.preventDefault();
        this.setState({
            context: true
        })
    };

    _onAction = (data) => {
        this.setState({
            context: false
        });
        this.props.onAction(data, this.props.col, this.props.row);
    };

    _onDblClick = (e) => {
        e.preventDefault();
        if (!this.props.editable) {
            this.props.onAction("CLICK", this.props.col, this.props.row);
        } else {
            let val;
            if (this.props.col.name.startsWith("data.")) {
                val = this.props.row.data[this.props.col.name.replace("data.", "")]
            } else if (this.props.col.name.indexOf(".") > 0) {
                val = ewars.getKeyPath(this.props.col.name, this.props.row);
            } else {
                val = this.props.row[this.props.col.name] || null;
            }

            let editVal = val;
            if (this.props.col.editor) {
                // Edit a column other than this one
                if (this.props.col.editor.editColumn != null) {
                    editVal = ewars.getKeyPath(this.props.col.editor.editColumn, this.props.row);
                }

                if (this.props.col.editor.editIndex != null) {
                    editVal = editVal[this.props.col.editor.editIndex];
                }
            }

            ewars.emit("UNMOUNT_EDIT");
            this.setState({
                edit: true,
                tempVal: editVal
            })
        }
    };

    _onCellAction = (action) => {
        switch (action) {
            case "CANCEL":
                this.setState({
                    edit: false,
                    tempVal: null
                });
                return true;
            case "SAVE":
                this.setState({
                    edit: false
                });
                this.props.col.editor.onChange(this.props.col, this.props.row, this.state.tempVal);
                return true;
            default:
                return true;
        }
    };

    _onValueChange = (e) => {
        this.setState({
            tempVal: e.target.value
        })
    };

    render() {
        let style = {width: this.props.width};

        let val;
        if (this.props.col.name.startsWith("data.")) {
            val = this.props.row.data[this.props.col.name.replace("data.", "")]
        } else if (this.props.col.name.indexOf(".") > 0) {
            val = ewars.getKeyPath(this.props.col.name, this.props.row);
        } else {
            val = this.props.row[this.props.col.name] || null;
        }
        let rawVal = val;


        if (this.props.col.config.type == "select" && this.props.col.config.options) {
            if (this.props.col.config.multiple) {
                if (val) {
                    let newVal = [];
                    this.props.col.config.options.forEach((item) => {
                        if (val.indexOf(item[0]) >= 0) {
                            newVal.push(item[1]);
                        }
                    });

                    val = newVal.join(", ")
                }

            } else {
                this.props.col.config.options.forEach(function (item) {
                    if (item[0] == val) val = item[1]
                })
            }
        }

        if (this.props.col.config.type == "lat_long" && val != null) val = `Lat: ${rawVal[0]}, Lng: ${rawVal[1]}`;

        if (this.props.col.fmt) val = this.props.col.fmt(val, this.props.row);

        if (val instanceof Object) val = ewars.I18N(val);

        let valueClass = "dt-cell-value";
        if (this.props.col.config.type == "number") valueClass += " rh-align";

        let EditControl;
        // if (this.state.edit) {
        //     EditControl = TYPES[this.props.col.config.type];
        // }

        let cellStyle = {
            border: "1px dashed transparent",
            position: "relative",
            height: "100%"
        };
        if (this.props.col.color) {
            let color = this.props.col.color(rawVal);
            cellStyle.borderColor = color;
            cellStyle.textShadow = `0px -2px 3px ${color}`;
        }


        return (
            <td style={style} ref="cell" onContextMenu={this._context} onDoubleClick={this._onDblClick}>
                <div style={cellStyle}>
                    {this.state.edit ?
                        <div className="dt-cell-inline">
                            <input type="text" value={this.state.tempVal} onChange={this._onValueChange}/>
                        </div>
                        : null}
                    {!this.state.edit ?
                        <div className={valueClass}>{val}</div>
                        : null}
                    {this.state.context ?
                        <div style={{position: "absolute", top: 22, left: 0}} ref="item">
                            <ContextMenu
                                absolute={false}
                                onClick={this._onAction}
                                actions={this.props.actions}
                                visible={this.state.context}/>
                        </div>
                        : null}
                    {this.state.edit ?
                        <div className="dt-cell-editor">
                            <ewars.d.ActionGroup
                                actions={CELL_ACTIONS}
                                onAction={this._onCellAction}/>
                        </div>
                        : null}
                </div>
            </td>
        )
    }
}

export default DataTableCell;
