class DataTableColumnHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let label = ewars.I18N(this.props.data.label);
        let width = (this.props.data.width || 100) + "px";
        return (
            <th style={{height: 25}} width={width}>
                <div className="relative">
                    <span className="colHeader">{label}</span>
                </div>
            </th>
        )
    }
}

export default DataTableColumnHeader;