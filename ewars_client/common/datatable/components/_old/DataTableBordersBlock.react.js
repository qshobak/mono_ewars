class DataTableBordersBlock extends React.Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {
        let styleOne = {height: 1, width: 1, display: "none", backgroundColor: "red"};
        let styleTwo = {
            height: 5,
            width: 5,
            border: "2px solid rgb(255,255,255",
            display: "none",
            backgroundColor: "red"
        };
        let styleThree = {height: 1, width: 1, display: "none", backgroundColor: "rgb(137, 175, 249"};
        let styleFour = {height: 2, width: 2, display: "none", backgroundColor: "rgb(82, 146, 247"};
        let styleFive = {
            height: 5,
            width: 5,
            border: "2px solid rgb(255,255,255",
            display: "none",
            backgroundColor: "rgb(82, 146, 247"
        };

        return (
            <div className="dtBorders">
                <div style={{position: 'absolute', top: 0, left: 0}}>
                    <div className="dtBorder fill" style={styleOne}></div>
                    <div className="dtBorder fill" style={styleOne}></div>
                    <div className="dtBorder fill" style={styleOne}></div>
                    <div className="dtBorder fill" style={styleOne}></div>
                    <div className="dtBorder fill corner" style={styleTwo}></div>
                </div>
                <div style={{position: "absolute", top: 0, left: 0}}>
                    <div className="dtBorder area" style={styleThree}></div>
                    <div className="dtBorder area" style={styleThree}></div>
                    <div className="dtBorder area" style={styleThree}></div>
                    <div className="dtBorder area" style={styleThree}></div>
                    <div className="dtBorder area corner" style={styleTwo}></div>
                </div>
                <div style={{position: "absolute", top: 0, left: 0}}>
                    <div className="dtBorder current" style={styleFour}></div>
                    <div className="dtBorder current" style={styleFour}></div>
                    <div className="dtBorder current" style={styleFour}></div>
                    <div className="dtBorder current" style={styleFour}></div>
                    <div className="dtBorder current corner" style={styleFive}></div>
                </div>
            </div>
        )
    }
}

export default DataTableBordersBlock;