import RowAction from "../RowAction.react";

class RowActionControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showActions: false,

        }
    }

    componentDidMount() {
        window.__hack__.addEventListener("click", this.handleBodyClick);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this.handleBodyClick);
    }

    handleBodyClick = (evt) => {
        if (this.refs.dd) {
            const area = this.refs.dd;
            if (!area.contains(evt.target)) {
                this.setState({
                    showActions: false
                })
            }
        }
    };

    handleClick = (e) => {
        e.stopPropagation();
    };

    toggleDisplay = (e) => {
        if (e) e.stopPropagation();

        let rect = this.refs.dd.getBoundingClientRect();
        this.setState({
            showActions: this.state.showActions ? false : true,
            left: rect.left,
            top: rect.top + 22
        })
    };

    onAction = (action) => {
        this.setState({
            showActions: false
        });
        this.props.onAction(action, this.props.row);
    };

    shouldComponentUpdate() {
        return false;
    }

    render() {

        let actions = this.props.actions.map(function (item) {
            return <RowAction
                onAction={this.onAction}
                data={item}/>
        }.bind(this));

        return (
            <div className="dtDropDown" ref="dd" onClick={this.handleBodyClick}>
                <div className="handle" onClick={this.toggleDisplay}>
                    <div className="ide-row">
                        <div className="ide-col" style={{width: 35, textAlign: "center"}}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div className="ide-col">
                            <i className="fal fa-caret-down"></i>
                        </div>
                    </div>
                </div>
                {this.state.showActions ?
                    <div className="dtActions" style={{top: this.state.top, left: this.state.left}}>
                        <div className="dtActionsInner">
                            {actions}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

export default RowActionControl;
