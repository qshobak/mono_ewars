import Button from "../c.button";
import Toolbar from "../c.toolbar";
import { Row, Cell, Layout } from "../layout";
import { ActionGroup } from "../c.action_button";

import DataTableHeaderCell from "./c.datatable.header_cell";
import DataTableFilterControl from "./c.datatable.filter_control";
import DataTableCell from "./c.datatable.cell";

import Form from "../c.form";
import FormUtils from "../../common/utils/FormUtils";

import DTReducer from "./r.reducer";

// if (!Array.prototype.unique) {
//     Array.prototype.unique = function () {
//         var a = this.concat();
//         for (var i = 0; i < a.length; ++i) {
//             for (var j = i + 1; j < a.length; ++j) {
//                 if (a[i] === a[j])
//                     a.splice(j--, 1);
//             }
//         }
//
//         return a;
//     };
// }

const STYLES = {
    INPUT_SEARCH: {
        borderRadius: "3px",
        fontSize: "11px"
    }
}

class DataControlCell extends React.Component {
    static defaultProps = {
        actions: []
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _onClick = () => {
        this.props.onAction(this.props.data.action, null, this.props.row);
    };

    _show = () => {
        this.setState({
            show: true
        })
    };

    _hide = () => {
        this.setState({
            show: false
        })
    };

    _action = (action) => {
        this.props.onAction(action, null, this.props.row);
    };

    render() {
        let dtActions = this.props.actions.map((item) => {
            return [
                item.icon,
                item.action,
                item.label
            ]
        });
        return (
            <td width="30px" style={{width: 30}} onMouseEnter={this._show} onMouseLeave={this._hide}>
                <ActionGroup
                    actions={dtActions}
                    onAction={this._action}/>
                {/*<div className="dt-row-control" onClick={this._onClick}>*/}
                {/*<i className={"fal " + this.props.data.icon}></i>*/}
                {/*{this.state.show ?*/}
                {/*<div className="popover">{this.props.data.label}</div>*/}
                {/*: null}*/}
                {/*</div>*/}
            </td>
        )
    }
}

if (!ewars.g.DTS) {
    ewars.g.DTS = 0;
}

class DataTable extends React.Component {
    static defaultProps = {
        resource: null,
        editable: false,
        columns: [],
        filter: {},
        searchParams: [],
        baseFilters: {},
        actions: [],
        isReports: false,
        grid: null,
        join: null,
        order: {},
        select: null,

        initialOrder: null,
        initialFilter: null
    };
    _forceWidth = false;

    constructor(props) {
        super(props);

        ewars.z.register(this.props.id || "DT_TRANSIENT", DTReducer, {
            data: [],
            initialized: false,
            order: ewars.copy(this.props.initialOrder || {}),
            select: this.props.select || null,
            filter: {},
            limit: 10,
            offset: 0,
            page: 0,
            search: {},
            searchParam: "",
            count: null,
            join: this.props.join || null,
            filterControl: {
                shown: false,
                x: null,
                y: null,
                ref: null
            },
            context: {
                shown: false,
                ref: null,
                rowIndex: null
            },
            editor: {}
        });

        this.state = ewars.z.getState(this.props.id || "DT_TRANSIENT");

        ewars.z.subscribe(this.props.id || "DT_TRANSIENT", this._onChange);
        ewars.subscribe("RELOAD_DT", this._onQueryChange);
        ewars.addEvent(window, "resize", this._recalc);
    }

    componentDidMount() {
        let height = this.refs.grid.clientHeight;
        let width = this.refs.grid.clientWidth;

        let numRows = (height - (height % 25)) / 25;
        this.state.limit = numRows + 5;

        this.refs.dt.style.height = height + "px";

        if (!this.state.initialized) {
            this.query(this.props, this.state);
        }
    }

    componentWillUnmount() {
        ewars.removeEvent(window, "resize", this.recalc);
        ewars.z.unsubscribe(this.props.id || "DT_TRANSIENT", this._onChange);
    }

    _recalc = () => {
        if (this._timer) {
            clearTimeout(this._timer);
            this._timer = null
        }
        this._timer = setTimeout(() => {
            let height = this.refs.grid.clientHeight;

            let numRows = (height - (height % 25)) / 25;
            this.state.limit = numRows + 5;

            this.refs.dt.style.height = height + "px";

            this._timer = null;
            this.query(this.props, this.state);
        }, 500);
    };

    componentWillReceiveProps(nextProps, nextState) {
        let id = nextProps.id || "DT_TRANSIENT";
        // If the new id doesn't match the current id, swap registers
        if (id != this.props.id) {
            ewars.z.unsubscribe(this.props.id, this._onChange);
            ewars.z.subscribe(id, this._onChange);
        }

        if (ewars.z.exists(id)) {
            let state = ewars.z.getState(id);
            if (!state.initialized) {
                this.query(nextProps, nextState);
            } else {
                this.setState({...state});
            }
        } else {
            let height = this.refs.grid.clientHeight;

            let numRows = (height - (height % 25)) / 25;
            let limit = numRows + 5;

            this.refs.dt.style.height = height + "px";
            ewars.z.register(id, DTReducer, {
                data: [],
                intialized: false,
                order: ewars.copy(nextProps.initialOrder || {}),
                select: nextProps.select || null,
                filter: {},
                limit: limit || 10,
                offset: 0,
                page: 0,
                search: {},
                searchParam: "",
                count: null,
                join: nextProps.join || null,
                filterControl: {
                    shown: false,
                    x: null,
                    y: null,
                    ref: null
                },
                context: {
                    shown: false,
                    ref: null,
                    rowIndex: null
                },
                editor: {}
            });

            let state = ewars.z.getState(nextProps.id || "DT_TRANSIENT");
            ewars.z.subscribe(id, this._onChange);
            this.query(nextProps, state);
        }
    }

    _onChange = (action) => {
        let newState = ewars.z.getState(this.props.id || "DT_TRANSIENT");

        if (["PAGINATE", "UPDATE_SORT", "UPDATE_FILTER", "CLEAR_SEARCH"].indexOf(action) >= 0) {
            this.query(this.props, newState);
        } else {
            this.setState({
                ...newState
            })
        }
    };

    _onQueryChange = () => {
        this.query(this.props, this.state);
    };


    query(props, state) {
        if (props.paused == true) return;

        let blocker = new ewars.Blocker(this.refs.wrapper, "Loading...");
        let com = "com.ewars.query";
        if (props.isReports) com = "com.ewars.collections.get";

        let args = [];

        let joins = ewars.copy(state.join || []);
        (props.join || []).forEach(item => {
            if (joins.indexOf(item) < 0) joins.push(item);
        });

        if (props.isReports) {
            args = [
                props.formId,
                Object.assign({}, props.filter || {}, state.filter),
                state.order,
                state.limit || null,
                state.offset,
                joins
            ]
        } else {
            args = [
                props.resource,
                state.select || null,
                Object.assign({}, props.filter || {}, state.filter),
                state.order,
                state.limit,
                state.offset,
                joins
            ]
        }

        ewars.tx(com, args, {count: true})
            .then(function (resp) {
                blocker.destroy();
                ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "SET_ITEMS", {
                    ...resp,
                    state: state
                });
            }.bind(this));
    }

    onButtonClick = (data) => {
        if (data.action) {
            data.action();
        }

    };

    onAction = (action, row) => {
        this.props.onAction(action, row);
    };

    _move = (data) => {
        ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "PAGINATE", data);
    };

    _onCellAction = (action, cell, row) => {
        this.props.onCellAction(action, cell, row);
    };

    _modifyRow = (index, path, value) => {
        let data = ewars.copy(this.state.data);
        let row = data[index];

        if (path.split(".")[0] == "data") {
            let realPath = path.replace("data.", "");
            row.data[realPath] = value;
        } else {
            row[path] = value;
        }

        data[index] = row;
        this.setState({
            data: data
        })
    };

    _searchParamChange = (e) => {
        ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "UPDATE_SEARCH", {
            value: e.target.value
        })
    };

    _performSearch = () => {
        if (this.state.searchParam == "") {
            ewars.error("Please provide a search term");
            return;
        }
        ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "UPDATE_FILTER", {
            key: "data",
            config: {LIKE: this.state.searchParam}
        });

    };

    _clearSearch = () => {
        ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "CLEAR_SEARCH", null);
    };

    render() {
        let colHeaders,
            colGroupsMaster = [],
            cellWidth = 200;

        if (this._forceWidth) cellWidth = this.refs.dt.clientWidth / this.props.columns.length;

        //Used in report manager, if we're showing a grid composition
        if (this.props.grid) {
            colHeaders = [];

            if (this.props.actions) {
                this.props.actions.forEach(cell => {
                    colGroupsMaster.push(<col></col>)
                })
            }

            let grouped = {};
            this.props.grid.definition.forEach(function (item) {
                if (!grouped[item.row]) grouped[item.row] = [];
                grouped[item.row].push(item);
            });

            this.props.grid.columns.forEach(function (item) {
                let labelLength = ewars.I18N(item.config.label).length;
                let width = labelLength * 6.3;
                if (width < 200) width = 200;
                if (this._forceWidth) width = cellWidth < item.width || 0 ? cellWidth : item.width;
                if (item.config.width) width = item.config.width;
                colGroupsMaster.push(<col width={width + "px"}></col>);
            }.bind(this));

            for (let k in grouped) {
                // Group
                grouped[k].sort(function (a, b) {
                    if (a.cell < b.cell) return -1;
                    if (a.cell > b.cell) return 1;
                    return 0;
                });

                let cells = [];

                if (this.props.actions) {
                    cells.push(<th style={{width: 30}}></th>)
                }

                grouped[k].forEach(function (cell) {
                    cells.push(<DataTableHeaderCell
                        id={this.props.id || "DT_TRANSIENT"}
                        filter={this.state.filter}
                        sort={this.state.order}
                        data={cell}/>);
                }.bind(this));

                colHeaders.push(
                    <tr>
                        {cells}
                    </tr>
                )
            }

        } else {
            let headers = [];

            if (this.props.actions) {
                colGroupsMaster.push(<col width="30px"></col>);
                headers.push(<th style={{width: "30px"}}></th>)
                // this.props.actions.forEach(cell => {
                //     colGroupsMaster.push(<col width="30px"></col>)
                //     headers.push(<th style={{width: 30}}></th>)
                // })
            }

            this.props.columns.forEach(function (col, index) {
                let width = col.width || 200;
                let style = {width: width};
                if (this._forceWidth) style.width = cellWidth;
                colGroupsMaster.push(<col style={style}></col>);
                headers.push(
                    <DataTableHeaderCell
                        id={this.props.id || "DT_TRANSIENT"}
                        filter={this.state.filter}
                        sort={this.state.order}
                        data={col}/>
                )
            }.bind(this));
            colHeaders = <tr style={{height: "100%"}}>
                {headers}
            </tr>
        }

        let rowHeaders;

        let rows = this.state.data.map(function (item, index) {
            let cells = [];
            let className = "dt-row";

            if (index % 2 != 0) {
                className += " striped";
            }

            if (this.props.actions) {
                cells.push(
                    <DataControlCell
                        key={item.uuid + 'DT_CT_CELL'}
                        actions={this.props.actions}
                        id={this.props.id || "DT_TRANSIENT"}
                        onAction={this._onCellAction}
                        row={item}/>
                )
            }

            if (this.props.grid) {
                this.props.grid.columns.forEach(function (col) {
                    cells.push(
                        <DataTableCell
                            col={col}
                            row={item}
                            key={item.uuid + '_' + col.name}
                            id={this.props.id || "DT_TRANSIENT"}
                            modifyRow={this._modifyRow}
                            inline={this.props.editable}
                            onAction={this._onCellAction}
                            actions={this.props.actions}
                            editable={col.editable}
                            rowIndex={index}/>
                    )
                }.bind(this))
            } else {
                this.props.columns.forEach(function (col) {
                    cells.push(
                        <DataTableCell
                            rowIndex={index}
                            row={item}
                            key={item.uuid + '_' + col.name}
                            id={this.props.id || "DT_TRANSIENT"}
                            inline={this.props.editable}
                            modifyRow={this._modifyRow}
                            onAction={this._onCellAction}
                            editable={col.editable}
                            actions={this.props.actions}
                            col={col}/>
                    )
                }.bind(this))
            }

            return (
                <tr style={{height: "100%"}} className={className}>
                    {cells}
                </tr>
            )
        }.bind(this));


        let buttons;
        if (this.props.buttons) {
            buttons = this.props.buttons.map(function (btn) {
                return <Button
                    label={btn.label}
                    icon={btn.icon}
                    data={btn}
                    onClick={this.onButtonClick}/>
            }.bind(this));
        }

        var paginationString;
        if (this.state.count) {
            var startShow, endShow;
            if (this.state.page == 0) {
                startShow = 1;
                endShow = this.state.limit;
            } else {
                startShow = ((this.state.limit) * this.state.page) + 1;
                endShow = startShow + 9;
            }

            if (this.state.count == this.state.data.length) {
                endShow = this.state.count;
            }

            paginationString = [
                "Showing ",
                startShow,
                " to ",
                endShow,
                " of ",
                this.state.count
            ].join("");
        } else {
            paginationString = "No items found";
        }

        return (
            <div className="dt-wrapper" ref="wrapper">
                <div className="ide-layout">
                    {this.props.grid ?
                        <Toolbar>
                            <div className="input-search"
                                 style={{width: "200px", float: "left", marginTop: "-3px"}}>
                                <Row>
                                    <Cell>
                                        <input
                                            style={STYLES.INPUT_SEARCH}
                                            placeholder="Search..."
                                            value={this.state.searchParam}
                                            onChange={this._searchParamChange}
                                            type="text"/>
                                    </Cell>
                                    <Cell width="80px" style={{paddingTop: "3px"}}>
                                        <div className="btn-group">

                                            <Button
                                                icon="fa-search"
                                                onClick={this._performSearch}/>
                                            {this.state.searchParam != "" ?
                                                <Button
                                                    icon="fa-times"
                                                    onClick={this._clearSearch}/>
                                                : null}
                                        </div>
                                    </Cell>

                                </Row>
                            </div>


                            {this.props.header_actions.length > 0 ?
                                <div className="btn-group pull-right">
                                    {this.props.header_actions.map(item => {
                                        return (
                                            <Button
                                                icon={item[0]}
                                                onClick={() => {
                                                    this.props.onAction(item[1]);
                                                }}/>
                                        )
                                    })}
                                </div>
                                : null}
                        </Toolbar>
                        : null}
                    <div className="ide-row">
                        <div className="ide-col" ref="grid">
                            <div className="dt" ref="dt">

                                <table className="dtCore">
                                    <thead>
                                    {colHeaders}
                                    </thead>
                                    <tbody>
                                    {rows}
                                    </tbody>
                                </table>
                                {/*<DataTableBordersBlock/>*/}

                            </div>
                        </div>
                    </div>
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">{paginationString}</div>

                                <div className="btn-group pull-right">
                                    {this.state.offset > 0 ?
                                        <Button
                                            icon="fa-fast-backward"
                                            onClick={this._move}
                                            data={{page: "BB"}}/>
                                        : null}
                                    {this.state.offset > 0 ?
                                        <Button
                                            icon="fa-step-backward"
                                            onClick={this._move}
                                            data={{page: "B"}}/>
                                        : null}
                                    {this.state.data.length >= this.state.limit ?
                                        <Button
                                            icon="fa-step-forward"
                                            onClick={this._move}
                                            data={{page: "F"}}/>
                                        : null}
                                    {this.state.data.length >= this.state.limit ?
                                        <Button
                                            icon="fa-fast-forward"
                                            onClick={this._move}
                                            data={{page: "FF"}}/>
                                        : null}
                                </div>
                                <div className="btn-group pull-right">
                                    <Button
                                        icon="fa-sync"
                                        onClick={this._onQueryChange}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {
                    this.state.showFilterControls ?
                        <DataTableFilterControl
                            filter={this.state.filter}
                            id={this.props.id || "DT_TRANSIENT"}
                            cell={this.state.cell}
                            top={this.state.filtY}
                            left={this.state.filtX}/>
                        : null
                }

            </div>
        )
    }
}

export default DataTable;
