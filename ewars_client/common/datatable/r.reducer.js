function _updatePagination (STATE, data) {
    switch (data.page) {
        case "FF":
            if (STATE.offset < STATE.count) {
                var pages = STATE.count / STATE.limit;
                STATE.page = Math.ceil(pages);
                STATE.offset = (STATE.page - 1) * STATE.limit;
            }
            break;
        case "F":
            if (STATE.offset < STATE.count) {
                STATE.offset = (STATE.page + 1) * STATE.limit;
                STATE.page++;
            }
            break;
        case "B":
            STATE.offset = (STATE.page - 1) * STATE.limit;
            STATE.page--;
            break;
        case "BB":
            STATE.offset = 0;
            STATE.page = 0;
            break;
        default:
            break;
    }

    if (STATE.page < 0) STATE.page = 0;
    if (STATE.page <= 0) STATE.offset = 0;

    return STATE;
}

function _updateSort(state, cell, fieldType) {
    if (!state.order[cell]) {
        state.order = {};
        state.order[cell] = `DESC:${fieldType}`;
    } else if (state.order[cell] == `DESC:${fieldType}`) {
        state.order[cell] = `ASC:${fieldType}`;
    } else if (state.order[cell] == `ASC:${fieldType}`) {
        delete state.order[cell];
    } else if (state.order[cell] == "DESC") {
        state.order[cell] = "ASC";
    } else if (state.order[cell] == "ASC") {
        delete state.order[cell];
    }

    return state;
}

function _updateFilter(state, col, data) {
    if (!data) {
        delete state.filter[col];
        return state;
    }

    if (!col) return state;
    state.offset = 0;
    state.page = 0;
    state.filter[col] = data;

    return state;
}

const _updateSearch = (state, value) => {
    if (value == "") {
        delete state.filter.data;
        state.searchParam = "";
    } else {
        state.searchParam = value;
    }
    return state;
};

const _clearSearch = (state) => {
    state.searchParam = "";
    if (state.filter.data) {
        delete state.filter.data;
    }
    return state;
};

export default (rawState, action, data) => {
    let state = ewars.copy(rawState);

    switch(action) {
        case "SET_ITEMS":
            data.state.initialized = true;
            return Object.assign({}, state, data.state, {data: data.results, count: data.count});
        case "PAGINATE":
            return _updatePagination(state, data);
        case "UPDATE_SORT":
            return _updateSort(state, data.field, data.fieldType);
        case "UPDATE_FILTER":
            return _updateFilter(state, data.key, data.config);
        case "UPDATE_SEARCH":
            return _updateSearch(state, data.value);
        case "CLEAR_SEARCH":
            return _clearSearch(state);
        default:
            return state;
    }
}