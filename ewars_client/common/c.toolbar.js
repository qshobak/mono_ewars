class Toolbar extends React.Component {
    static defaultProps = {
        label: null,
        buttons: []
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-row" style={{maxHeight: 35}}>
                <div className="ide-col">
                    <div className="ide-tbar" style={this.props.style || {}}>
                        {this.props.icon ?
                            <div className="ide-tbar-icon">
                                <i className={"fal " + this.props.icon}></i>
                            </div>
                            : null}
                        {this.props.label ?
                            <div className="ide-tbar-text">{__(this.props.label)}</div>
                            : null}

                        <div className="tbar-btns">
                            {this.props.buttons}
                        </div>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default Toolbar;