import ewars from "../ewars/app";

class Application extends React.Component {

    constructor(props) {
        super(props);

        window.ewars = ewars;

        window.__ = ewars.setup();
    }

    render() {
        return this.props.children;
    }
}

export default Application;
