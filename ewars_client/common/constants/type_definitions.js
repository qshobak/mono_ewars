import FORM_SERIES from "../forms/form.series";
import * as _FORM_SERIES_STYLE from "../forms/form.series.style";
import FORM_DASHBOARD_STYLE from "../forms/form.dashboard.style";
import FORM_SERIES_CONTROLS from "../forms/form.series.controls";
import FORM_SERIES_X from "../forms/form.series.x";
import FORM_SERIES_Y from "../forms/form.series.y";
import FORM_RAW from "../forms/form.raw";
import FORM_RAW_SOURCE from "../forms/form.raw.source";
import FORM_CATEGORY from "../forms/form.category";
import FORM_CATEGORY_STYLE from "../forms/form.category.style";
import FORM_CATEGORY_CONTROLS from "../forms/form.category.controls";
import FORM_ALERTS_LIST from "../forms/form.alerts.list";
import FORM_ALERTS_MAP from "../forms/form.alerts.map";
import FORM_TABLE from "../forms/form.table";
import FORM_TABLE_STYLE from "../forms/form.table.style";
import FORM_HEATMAP from "../forms/form.heatmap";
import FORM_HEATMAP_STYLE from "../forms/form.heatmap.style";
import FORM_ASSIGNMENTS from "../forms/form.assignments";
import FORM_ACTIVITY from "../forms/form.activity";
import FORM_WIDGET_BASE from "../forms/form.widget.base";
import FORM_TREEMAP from "../forms/form.treemap";
import FORM_TREEMAP_STYLE from "../forms/form.treemap.style";
import FORM_SUBMISSIONS_RECENT from "../forms/form.submissions.recent";

import FORM_RAW_STYLE from "../forms/widgets/form.widget.raw.style";
import FORM_SERIES_STYLE from "../forms/widgets/form.widget.series.style";
import FORM_PIVOT_STYLE from "../forms/widgets/form.widget.pivot.style";
import FORM_ACTIVITY_STYLE from "../forms/widgets/form.widget.activity.style";

import FORM_LIST from "../forms/widgets/form.list";
import FORM_LIST_SOURCE from "../forms/widgets/form.list.source";

import FORM_MAP from "../forms/widgets/form.map";
import FORM_MAP_STYLE from "../forms/widgets/form.map.style";
import FORM_MAP_DATA from "../forms/widgets/form.map.data";

import FORM_PIVOT from "../forms/widgets/form.pivot";

const ICONS = {
    DEFAULT: "fa-cog",
    DATA: "fa-database",
    STYLE: "fa-paint-brush",
    CONTROLS: "fa-tachometer",
    LAYERS: "fa-map-marker",
    HTML: "fa-code",
    V: "fa-bars",
    H: "fa-bars fa-rotate-90",
    SERIES: "fa-chart-line",
    CATEGORY: "fa-chart-pie",
    RAW: "fa-square",
    ALERT: "fa-exclamation-triangle",
    MAP: "fa-map",
    HEAT: "fa-fire",
    TREEMAP: "fa-tree",
    X_AXIS: "fa-times",
    TABLE: 'fa-table'
};

const PLOT_TYPES = [
    'DEFAULT',
    'BULLET',
    'CANDLE',
    'COLUMN',
    'PIE',
    'SPARK',
    'WATERFALL',
    'XRANGE',
    'MAP_DEFAULT',
    'HEATMAP',
    'HEXMAP',
    'TREEMAP',
    'FUNNEL',
    'GAUGE',
    'PYRAMID',
    'SANKEY',
    'STREAM',
    'TABLE',
    'PIVOT',
    'DATAPOINT',
    'SCATTER',
    'BUBBLE',
    'MAP'
]

const TYPE_DEFINITIONS = {
    // VBOX: {
    //     t: "V",
    //     i: ICONS.V,
    //     bgd: '#CCCCCC',
    //     tabs: [
    //         {n: "DEFAULT", t: "F", f: [], i: ICONS.DEFAULT}
    //     ],
    //     modes: ["DASHBOARD", "TEMPLATE", "HUD"]
    // },
    // HBOX: {
    //     t: "H",
    //     i: ICONS.H,
    //     bgd: '#CCCCCC',
    //     tabs: [
    //         {n: "DEFAULT", t: "F", f: [], i: ICONS.DEFAULT}
    //     ],
    //     modes: ["DASHBOARD", "TEMPLATE", "HUD"]
    // },
    DATAPOINT: {
        t: 'DATAPOINT',
        i: ICONS.SERIES,
        tabs: [],
        modes: '*'
    },
    DEFAULT: {
        t: 'DEFAULT',
        i: ICONS.SERIES,
        controls: ['NAVIGATOR', 'TEXT', 'LEGEND', 'BELL', 'TREND', 'LOG', 'BAND', 'LINE', 'SCALE', 'ANNOTATION'],
        features: ['COLOR', 'SIZE', 'LABEL', 'MARKERS', 'TOOLTIP', 'DETAIL', 'STYLE_DEFAULT'],
        modes: '*'
    },
    // BULLET: {
    //     t: 'BULLET',
    //     i: ICONS.SERIES,
    //     controls: ['LEGEND', 'TEXT'],
    //     features: ['LABEL', 'TOOLTIP'],
    //     modes: '*'
    // },
    // CANDLE: {
    //     t: 'CANDLE',
    //     i: ICONS.SERIES,
    //     controls: ['LEGEND', 'TEXT', 'NAVIGATOR', 'BELL', 'TREND', 'LOG', 'BAND', 'LINE', 'SCALE', 'ANNOTATION'],
    //     features: ['COLOR', 'LABEL', 'MARKERS', 'TOOLTIP', 'DETAIL'],
    //     modes: '*'
    // },
    PIE: {
        t: 'PIE',
        i: ICONS.SERIES,
        controls: ['LEGEND', 'TEXT', 'SCALE'],
        features: ['COLOR', 'LABEL', 'TOOLTIP', 'STYLE_PIE'],
        modes: '*'
    },
    // SPARK: {
    //     t: 'SPARK',
    //     i: ICONS.SERIES,
    //     tabs: [],
    //     modes: '*'
    // },
    // XRANGE: {
    //     t: 'XRANGE',
    //     i: ICONS.SERIES,
    //     tabs: [],
    //     modes: '*'
    // },
    MAP: {
        t: 'MAP',
        i: ICONS.MAP,
        tabs: [],
        modes: '*'
    },
    HEATMAP: {
        t: 'HEATMAP',
        i: ICONS.SERIES,
        tabs: [],
        modes: '*'
    },
    // TREEMAP: {
    //     t: 'TREEMAP',
    //     i: ICONS.SERIES,
    //     tabs: [],
    //     modes: '*'
    // },
    FUNNEL: {
        t: 'FUNNEL',
        i: ICONS.SERIES,
        tabs: [],
        modes: '*'
    },
    // GAUGE: {
    //     t: 'GAUGE',
    //     i: ICONS.SERIES,
    //     tabs: [],
    //     modes: '*'
    // },
    PYRAMID: {
        t: 'PYRAMID',
        i: ICONS.SERIES,
        tabs: [],
        modes: '*'
    },
    // STREAM: {
    //     t: 'STREAM',
    //     i: ICONS.SERIES,
    //     tabs: [],
    //     modes: '*'
    // },
    TABLE: {
        t: 'TABLE',
        i: ICONS.TABLE,
        controls: ['LEGEND', 'TEXT', 'NAVIGATOR', 'BELL', 'TREND', 'LOG', 'BAND', 'LINE', 'SCALE', 'ANNOTATION'],
        features: ['COLOR', 'LABEL', 'MARKERS', 'TOOLTIP', 'DETAIL'],
        tabs: [],
        modes: '*'
    },
    // PIVOT: {
    //     t: 'PIVOT',
    //     i: ICONS.TABLE,
    //     tabs: [],
    //     modes: '*'
    // },
    RECENT_SUBMISSIONS: {
        t: "RECENT_SUBMISSIONS",
        i: ICONS.DEFAULT,
        tabs: [
            {n: "DEFAULT", t: "F", f: FORM_SUBMISSIONS_RECENT, i: ICONS.DEFAULT}
        ],
        modes: ["DASHBOARD"]
    },
    DOCUMENTS: {
        t: "DOCUMENTS",
        i: ICONS.DEFAULT,
        tabs: [
            {n: "DEFAULT", t: "F", f: FORM_WIDGET_BASE, i: ICONS.DEFAULT}
        ],
        modes: ["DASHBOARD"]
    },
    PERFORMANCE: {
        t: 'PERFORMANCE',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    HTML: {
        t: "HTML",
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [
            {n: "DEFAULT", t: "F", f: [{n: "html", t: "HTML", l: "HTML"}], i: ICONS.DEFAULT},
            {n: "__", t: "W_STYLE", i: ICONS.STYLE}
        ],
        modes: "*"
    },
    ALERTS: {
        t: 'ALERTS',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    OUTBREAKS: {
        t: "OUTBREAKS",
        i: "fal fa-ambulance",
        e: "WIDGET",
        tabs: [],
        modes: '*'
    },
    ACTIVITY: {
        t: 'ACTIVITY',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    NOTIFICATIONS: {
        t: 'NOTIFICATIONS',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    OVERDUE: {
        t: 'OVERDUE',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    ASSIGNMENTS: {
        t: 'ASSIGNMENTS',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    IMAGE: {
        t: 'IMAGE',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    LINKS: {
        t: 'LINKS',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    FEED: {
        t: 'FEED',
        i: ICONS.DEFAULT,
        e: 'WIDGET',
        tabs: [],
        modes: '*'
    },
    TASKS: {
        t: 'TASKS',
        i: ICONS.DEFAULT,
        tabs: [],
        modes: '*'
    },
    WIKIS: {
        t: 'WIKIS',
        i: ICONS.DEFAULT,
        tabs: [],
        modes: '*'
    }
}

export default TYPE_DEFINITIONS;

export {
    PLOT_TYPES
}
