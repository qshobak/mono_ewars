// Indicator definition controls
var CONSTANTS = require("./constants");

var FORMS = {
    alarm_id: {
        type: "select",
        label: {en: "Alarm"},
        template: "alarm",
        optionsSource: {
            resource: "alarm",
            labelSource: "name",
            valSource: "uuid",
            query: {
                status: {eq: true}
            },
            select: ["uuid", "name", "account_id"],
            join: ["account:account_id:id"]
        }
    },
    form_id: {
        type: "select",
        label: {en: "Form"},
        template: "form",
        optionsSource: {
            resource: "form",
            labelSource: "name",
            valSource: "id",
            query: {
                status: {eq: CONSTANTS.ACTIVE}
            },
            select: ["id", "name", "account_id"],
            join: ["account:account_id:id"]
        }
    },
    org_id: {
        type: "select",
        label: {en: "Organization"},
        optionsSource: {
            resource: "organization",
            labelSource: "name",
            valSource: "uuid",
            query: {
                status: {eq: CONSTANTS.ACTIVE}
            }
        }
    },
    lab_id: {
        type: "select",
        label: {en: "Laboratory"},
        optionsSource: {
            resource: "laborator",
            labelSource: "name",
            valSource: "uuid",
            query: {
                status: {eq: "ACTIVE"}
            }
        }
    }
};

function getIndicatorForm(indicator) {
    var definition = {};

    if (!indicator.definition) return {};
    if (indicator.definition.length <= 0) return {};

    _.each(indicator.definition, function (item) {
        if (item == "form") definition.form_id = FORMS.form_id;
        if (!ewars.isAdmin(window.user)) definition.form_id.query.account_id = {eq: window.user.account_id};

        if (item == "alarm") definition.alarm_id = FORMS.alarm_id;
        if (!ewars.isAdmin(window.user)) definition.alarm_id.query.account_id = {eq: window.user.account_id};

        if (item == "organisation") definition.org_id = FORMS.org_id;
    }, this);

    return definition;
}

module.exports = {
    getIndicatorForm: getIndicatorForm
};