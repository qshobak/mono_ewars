export const FIELD_FORM = [
    {
        _o: 0,
        n: "label",
        i18: true,
        t: "TEXT",
        l: "FIELD_LABEL",
        d: {en: "New Field"}
    },
    {
        _o: 1,
        n: "type",
        t: "SELECT",
        l: "FIELD_TYPE",
        d: "text",
        o: [
            ["text", "Text Field"],
            ["number", "Numeric Field"],
            ["select", "Select Field"],
            ["textarea", "Textarea Field"],
            ["header", "Header Field"],
            ["matrix", "Matrix Field"],
            ["row", "Matrix Row Field"],
            ["display", "Display Field"],
            ["date", "Date Field"],
            ['location', 'Location'],
            ['lat_long', 'Lat/Lng Field'],
            ['calculated', 'Calculated (Display)'],
            ['repeater', 'Repeater (Web Only)'],
            ['related', 'Related Records (Web Only)'],
            ["time", "Time Field"]
        ]
    },
    {
        _o: 2,
        n: "name",
        t: "TEXT",
        l: "FIELD_NAME"
    },
    {
        _o: 3,
        n: "required",
        l: "REQUIRED",
        t: "BUTTONSET",
        o: [
            [false, "NO"],
            [true, "YES"]
        ],
        d: false,
        c: [
            "ANY",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location",
            "type:=:number",
            "type:=:lat_long"
        ]
    },
    /**
    {
        _o: 3.1,
        n: "unique",
        l: "Unique Constraint",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        h: "Fields marked as unique will restrict the form from being submitted if a record with the same combination of values in its unique fields already exists.",
        d: false,
        c: [
            "ANY",
            "type:!=:header",
            "type:!=:matrix",
            "type:!=:row",
            "type:!=:display",
            "type:!=:markdown",
        ]
    },
     **/
    {
        _: 4,
        n: "show_mobile",
        t: "BUTTONSET",
        l: "Show on mobile?",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 4,
        n: 'sti',
        t: 'SELECT',
        l: 'Location type',
        r: true,
        o: [],
        c: [
            'ALL',
            'type:=:location'
        ],
        os: ['location_type', 'id', 'name', {}]

    },
    {
        _o: 5,
        n: "show_row_label",
        t: "BUTTONSET",
        l: "Show row label",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "type:=:row"
        ]
    },
    {
        _o: 6,
        n: "date_type",
        t: "SELECT",
        l: "DATE_TYPE",
        r: true,
        d: "DAY",
        o: [
            ["DAY", _l("DAY")],
            ["WEEK", "ISO8601 Weekly"],
            ["MONTH", _l("MONTH")],
            ["YEAR", _l("YEAR")]
            // ["CUSTOM", "Custom"]
        ],
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    /**
    {
        _o: 6.1,
        n: "is_canonical_date",
        t: "BUTTONSET",
        r: true,
        l: "Canonical Date?",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "type:=:date"
        ]
    },
     **/
    {
        _o: 6.2,
        n: "interval_days",
        t: "NUMERIC",
        l: "Number of days in interval",
        r: true,
        d: 0,
        c: [
            "ALL",
            "date_type:=:CUSTOM"
        ]
    },
    {
        _o: 6.3,
        n: "interval_base_date",
        t: "DATE",
        dt: "DAY",
        l: "Base Date",
        r: true,
        c: [
            "ALL",
            "date_type:=:CUSTOM"
        ]
    },
    {
        _o: 6.4,
        n: "date_format",
        t: "TEXT",
        l: "Output Format",
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    {
        _o: 7,
        n: "help",
        t: "TEXT",
        i18: true,
        l: "FIELD_INSTRUCTIONS",
        ml: true,
        c: [
            "ANY",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location",
            "type:=:number"
        ]
    },
    {
        _o: 8,
        n: "options",
        t: "OPTIONSDEF",
        l: "OPTIONS",
        i18: true,
        c: [
            "ALL",
            "type:=:select"
        ]
    },
    {
        _o: 9,
        n: "default",
        t: "TEXT",
        l: _l("DEFAULT_VALUE"),
        c: [
            "ANY",
            "type:=:number",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location"
        ]
    },
    {
        _o: 10,
        n: "redacted",
        t: "BUTTONSET",
        l: "REDACTED",
        d: false,
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ANY",
            "type:=:select",
            "type:=:number",
            "type:=:text",
            "type:=:date",
            "type:=:location",
            "type:=:textarea"
        ]
    },
    {
        _o: 11,
        n: "header_style",
        t: "SELECT",
        l: _l("HEADER_STYLE"),
        d: "TITLE",
        o: [
            ["style-title", "TITLE"],
            ["style-sub-title", "SUB_TITLE"]
        ],
        c: [
            "ALL",
            "type:=:header"
        ]
    },
    {
        _o: 12,
        n: "placeholder",
        t: "TEXT",
        l: "PLACEHOLDER",
        c: [
            "ANY",
            "type:=:text",
            "type:=:number",
            "type:=:textarea"
        ]
    },
    {
        _o: 13,
        n: "barcode",
        t: "BUTTONSET",
        l: "BARCODE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ANY",
            "type:=:text",
            "type:=:number"
        ]
    },
    {
        _o: 14,
        n: "allow_future_dates",
        t: "BUTTONSET",
        l: "ALLOW_FUTURE_DATES",
        d: false,
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    {
        _o: 15,
        n: "multiple",
        t: "BUTTONSET",
        l: "SELECT_MULTIPLE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        d: false,
        c: [
            "ALL",
            "type:=:select"
        ]
    },
    {
        _o: 16,
        n: "max",
        t: "TEXT",
        l: "MAXIMUM",
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 17,
        n: "min",
        t: "TEXT",
        l: "MINIMUM",
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 18,
        n: "negative",
        t: "BUTTONSET",
        l: "ALLOW_NEGATIVE",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        d: true,
        c: [
            "ALL",
            "type:=:number"
        ]
    },
    {
        _o: 19,
        n: "conditional_bool",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        l: "CONDITIONAL_LOGIC"
    },
    {
        _o: 20,
        n: "conditions",
        t: "FIELDRULES",
        l: "Rules editor",
        c: [
            "ALL",
            "conditional_bool:=:true"
        ]
    },
    {
        _o: 21,
        n: "calc_def",
        t: "CALC_FIELD_DEF",
        l: "Source field(s)",
        c: [
            "ALL",
            "type:=:calculated"
        ]
    }
];
