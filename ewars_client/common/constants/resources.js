const RESOURCES = {
    location: {
        props: ["uuid", "name", "path_name", "site_type_id", "status", "groups"]
    },
    alert: {
        props: ["alert_date", "indicator_name", "location_id", "alarm_name", "alarm_id"]
    },
    alarms: {
        props: ["name", "status"]
    },
    user: {
        props: ["email", "name", "status"]
    },
    assignment: {
        props: ["user_id", "form_name"]
    },
    form: {
        props: ["name", "id", "version_id", "status"]
    }
};

export default RESOURCES;