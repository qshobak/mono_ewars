const PAGE_SIZES = {
    LETTER: [216, 279],
    LETTER_HALF: [140, 216],
    LEGAL: [216, 356],
    TABLOID: [279, 432],
    LEDGER: [432, 279],
    JLEGAL: [127, 203],
    A0: [841, 1189],
    A00: [1189, 1682],
    A1: [594, 841],
    A2: [420, 594],
    A3: [297, 420],
    A4: [210, 297],
    A5: [148, 210],
    A6: [105, 148],
    A7: [74, 105],
    A8: [52, 74],
    A9: [37, 52],
    A10: [26, 37],
    ARCHA: [229, 305],
    ARCHB: [305, 457],
    ARCHC: [457, 610],
    ARCHD: [610, 914],
    ARCHE: [914, 1219],
    ARCHE1: [762, 1067],

}

export default PAGE_SIZES;