var DateUtils = require("../../documents/utils/DateUtils");

/**
 * DataCollection instance constructor
 * Represents a collection of series
 * @param config
 * @constructor
 */
function DataSource(config) {
    this.id = _.uniqueId("DS_");
    this._config = config;
    this.data = [];
    this.indicator = null;
    this.location = null;
    this.type = config.dataType;
    this.colour = config.colour || null;
    this.title = config.title || null;
    this.interval = config.interval || config.timeInterval;
}

/**
 * Retrieve data from the datasource
 */
DataSource.prototype.getData = function () {

};

/**
 * Gets the data in the item as a time series
 */
DataSource.prototype.getTimeSeries = function () {

};

/**
 * Gets the data in the source reduced to a single value
 * @param reduction
 */
DataSource.prototype.getReduced = function (reduction) {

};

/**
 * Retrieves the data source defined as a highcharts series
 */
DataSource.prototype.getHighcharts = function () {

};

/**
 * Check if the DataSource is valid for querying
 * @returns {boolean}
 */
DataSource.prototype.isValid = function () {
    var isValid = true;
    if (!this.location) isValid = false;
    if (!this.indicator) isValid = false;
    if (!this.start_date) isValid = false;
    if (!this.end_date) isValid = false;
    if (!this.interval) isValid = false;

    return isValid;
};

/**
 * Retrieve the title for the series
 */
DataSource.prototype.getTitle = function () {

};

module.exports = DataSource;