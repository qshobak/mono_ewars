import Moment from "moment";

var UUID_REGEX = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);

function _isInt(n) {
    return Number(n) === n && n % 1 === 0;
}

function _isFloat(n) {
    return n === Number(n) && n % 1 !== 0;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

const isObject = (obj) => {
    return typeof obj === 'object' && obj !== null;
}

var VALIDATORS = {
    required: function (val, field) {
        if (val === null) return false;
        if (val === undefined) return false;
        if (val === "") return false;
        return true;
    },
    number: function (val, field) {
        if (!isNumeric(val)) {
            return "Please enter a valid number.";
        }

        if (!field.decimal_allowed && _isFloat(val)) {
            return "Decimal numbers are not allowed, please only provide whole numbers.";
        }

        if (field.max) {
            if (parseFloat(val) > parseFloat(field.max)) return "Maximum allowed value is " + field.max;
        }

        if (field.min) {
            if (parseFloat(val) < parseFloat(field.min)) return "Mininum allowed value is " + field.min;
        }

        if (!field.allow_negative) {
            if (val < 0) {
                return "Negative values are not allowed, please use only positive numbers";
            }
        }

        return null;
    },
    language_string: function (val, field) {
        if (field.required) {
            var passed = [];
            for (let key in val) {
                let string = val[key];
                var result = true;
                if (!string || string == "" || string === null || string === undefined) result = false;
                passed.push(result);
            }

            if (passed.indexOf(true) >= 0) return null;
            return "You must specify at least one value in at least one language";
        }
    }

};

/**
 * Locate a field in the definition by it's field name
 * @param definition
 * @param keyName
 * @returns {*}
 * @private
 */
function _findFieldByKeyName(definition, keyName) {
    var result;

    if (definition[keyName]) {
        result = definition[keyName];
    } else {
        for (let key in definition) {
            let item = definition[key];
            var subResult;
            if (item.fields) {
                subResult = _findFieldByKeyName(item.fields, keyName);
            }
            if (subResult) result = subResult;
        }
    }

    return result;
}

/**
 * Evaluate a fields conditions
 * @param definition
 * @param data
 * @param condition
 * @returns {boolean}
 * @private
 */
function _evaluateConditions(data, condition) {
    var value = data[condition[0]];

    if (value == undefined || value == null) return false;

    var result = false;
    switch (condition[1]) {
        case "gt":
            if (parseFloat(value) > parseFloat(condition[2])) result = true;
            break;
        case "gte":
            if (parseFloat(value) >= parseFloat(condition[2])) result = true;
            break;
        case "lt":
            if (parseFloat(value) < parseFloat(condition[2])) result = true;
            break;
        case "lte":
            if (parseFloat(value) <= parseFloat(condition[2])) result = true;
            break;
        case "eq":
            if (value == condition[2]) result = true;
            break;
        case "ne":
            if (value != condition[2]) result = true;
            break;
        default:
            break
    }

    return result;
}

/**
 * Checks if a field is available and should be parse by validation
 * @param data
 * @param conditional
 * @returns {boolean}
 */
function _isFieldAvailable(data, conditional) {
    var result = false;
    var results = [];

    conditional.rules.forEach(condition => {
        results.push(_evaluateConditions(data, condition));
    });

    switch (conditional.application.toUpperCase()) {
        case "ALL":
            var uni = [...new Set(results)];
            if (uni.length == 1 && uni[0] === true) result = true;
            break;
        case "SOME":
            result = results.indexOf(true >= 0);
            break;
        default:
            break;
    }

    return result;
}

function _iterateForm(fields, data, path) {
    var result;

    Object.keys(fields).forEach(fieldName => {
        let field = fields[fieldName];
        var fieldPath;
        if (!path) {
            fieldPath = fieldName;
        } else {
            fieldPath = path + "." + fieldName;
        }

        // JDUTODO: Need to available whether the field is visible if it has conditions
        var isVisible = true;
        if (field.conditions && field.conditional_bool == true) {
            // Check the fields conditions
            isVisible = _isFieldAvailable(data, field.conditions);
        }

        if (!field.fields && isVisible) {
            // Get the value for the field if present
            var requiredResult = "PASS";
            var value = null;
            if (data[fieldName]) value = data[fieldName];

            if (field.required && !isObject(value) && ["matrix", "row", "header", "display"].indexOf(field.type) < 0) {
                // Check if the field has a value
                var res = VALIDATORS.required(value, field);
                if (res === false) {
                    requiredResult = "FAIL";
                    if (!result) result = {};
                    result[fieldName] = ewars.formatters.I18N_FORMATTER(field.label) + " is a required field."
                }

            } else if (field.required && field.type == "age") {
                // Special validation for age field
                var isNumPresent = false;
                var isUnitPresent = false;
                if (value.value && value.value != 0) isNumPresent = true;
                if (value.units && ["", 0, "0"].indexOf(value.units) < 0) isUnitPresent = true;
                if (isNumeric(value.value)) {
                    isNumPresent = true;
                } else {
                    isNumPresent = false;
                }

                if (value.units == "UNKNOWN") {
                    isNumPresent = true;
                    isUnitPresent = true;
                }

                if (isNumPresent && isUnitPresent) requiredResult = "PASS";

                if (!isNumPresent || !isUnitPresent) {
                    if (!result) result = {};
                    result[fieldName] = ewars.formatters.I18N_FORMATTER(field.label) + " is a required field.";
                }
            }

            if (VALIDATORS[field.type] && requiredResult == "PASS" && field.required) {
                var res = VALIDATORS[field.type](value, field);
                if (res) {
                    if (!result) result = {};
                    result[fieldName] = res;
                }
            }
        }

        if (field.fields) {
            var subResults = _iterateForm(field.fields, data[fieldName] || {}, fieldPath);
            if (subResults) {
                if (!result) result = {};
                result[fieldName] = subResults;
            }
        }
    }, this);

    return result;
}

const exported = (formDefinition, data) => {
    let result = _iterateForm(formDefinition, data);
    return result
}

export default exported;
