const Moment = require("moment");

const OFFSET_REG = /([-|+][0-9]*[DWMY])/g;
const DATE_REG = /([0-9]{4}-[0-9]{2}-[0-9]{2})/g;
const CODE_REG = /([{][A-Z_]*[}])/g;

const NUMERALS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const CODES = {
    "{D_DATE}": "Document date",
    "{NOW}": "Today",
    "{END}": "End date",
    "{D_W_S}": "Document week start",
    "{D_W_E}": "Document week end",
    "{D_M_S}": "Document month start",
    "{D_M_E}": "Document month end",
    "{D_Y_S}": "Document year start",
    "{D_Y_E}": "Document year end",
    "{N_W_S}": "Current week start",
    "{N_W_E}": "Current week end",
    "{N_M_S}": "Current month start",
    "{N_M_E}": "Current month end",
    "{N_Y_S}": "Current year start",
    "{N_Y_E}": "Current year end"
};

const PRESETS_DEFAULT = {
    "-30D": ["{NOW}-30D", "{NOW}"],
    "-60D": ["{NOW}-60D", "{NOW}"],
    "-6M": ["{NOW}-6M", "{NOW}"],
    "-1Y": ["{NOW}-1Y", "{NOW}"],
    "-2Y": ["{NOW}-2Y", "{NOW}"],
    "-5Y": ["{NOW}-5Y", "{NOW}"],
    "-1D": ["{NOW}-1D", "{NOW}-1D"],
    "-2D": ["{NOW}-2D", "{NOW}-2D"],
    "-7D": ["{NOW}-7D", "{NOW}-7D"],
    "PREV_WEEK": ["{N_W_S}-1W", "{N_W_E}-1W"],
    "PREV_MONTH": ["{N_M_S}-1M", "{N_M_E}-1M"],
    "PREV_YEAR": ["{N_Y_S}-1Y", "{N_Y_E}-1Y"],
    "NOW": ["{NOW}", "{NOW}"],
    "CUR_WEEK": ["{N_W_S}", "{N_W_E}"],
    "CUR_MONTH": ["{N_M_S}", "{N_M_E}"],
    "CUR_YEAR": ["{N_Y_S}", "{N_Y_E}"]
};

const PRESETS_TEMPLATE = {
    "-30D": ["{D_DATE}-30D", "{D_DATE}"],
    "-60D": ["{D_DATE}-60D", "{D_DATE}"],
    "-6M": ["{D_DATE}-6M", "{D_DATE}"],
    "-1Y": ["{D_DATE}-1Y", "{D_DATE}"],
    "-2Y": ["{D_DATE}-2Y", "{D_DATE}"],
    "-5Y": ["{D_DATE}-5Y", "{D_DATE}"],
    "-1D": ["{D_DATE}-1D", "{D_DATE}"],
    "-2D": ["{D_DATE}-2D", "{D_DATE}"],
    "-7D": ["{D_DATE}-7D", "{D_DATE}"],
    "PREV_WEEK": ["{D_W_S}-7D", "{D_W_E}-7D"],
    "PREV_MONTH": ["{D_M_S}-1M", "{D_M_E}-1M"],
    "PREV_YEAR": ["{D_Y_S}-1Y", "{D_Y_E}-1Y"],
    "NOW": ["{D_DATE}", "{D_DATE}"],
    "CUR_WEEK": ["{D_W_S}", "{D_W_E}"],
    "CUR_MONTH": ["{D_M_S}", "{D_M_E}"],
    "CUR_YEAR": ["{D_Y_S}", "{D_Y_E}"]
};

const DIGIT_STRINGS = {
    D: "day(s)",
    W: "week(s)",
    M: "month(s)",
    Y: "year(s)"
};

const INT_MARKERS = ["D", "W", "M", "Y"];
const DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

class Range {
    constructor() {}

    /**
     * Process a date specification
     * @param mode
     * @param preset
     * @returns {*}
     */
    static processPreset(mode, preset) {
        if (mode == "TEMPLATE") return PRESETS_TEMPLATE[preset];
        if (mode == "DEFAULT") return PRESETS_DEFAULT[preset];
        return PRESETS_DEFAULT.NOW;
    }

    /**
     * Format a date spec
     * @param spec
     * @returns {*}
     */
    static formatDateSpec(spec) {
        let result = "";

        if (DATE_REG.test(spec)) {
            let datePortion = spec.match(DATE_REG)[0];
            let offset = this.getOffset(spec);

            if (offset == "") {
                return datePortion;
            }

            result = datePortion;

            if (offset.indexOf("-") >= 0) {
                result = result + " - ";
                offset = offset.replace("-", "");
            }

            if (offset.indexOf("+") >= 0) {
                result = result + " + ";
                offset = offset.replace("+", "");
            }

            let lastChar = offset.substr(offset.length - 1);
            let period;
            if (INT_MARKERS.indexOf(lastChar) >= 0) {
                period = DIGIT_STRINGS[lastChar];
                offset = offset.replace(lastChar, "");
            } else {
                period = "day(s)";
            }

            result = result + " " + offset + " " + period;
            return result;

        } else if (CODE_REG.test(spec)) {
            let codePortion = spec.match(CODE_REG)[0];
            let offset = this.getOffset(spec);

            result = CODES[codePortion];

            if (offset == "") {
                return result;
            }

            if (offset.indexOf("-") >= 0) {
                result = result + " - ";
                offset = offset.replace("-", "");
            }

            if (offset.indexOf("+") >= 0) {
                result = result + " + ";
                offset = offset.replace("+", "");
            }

            let lastChar = offset.substr(offset.length - 1);
            let period;
            if (INT_MARKERS.indexOf(lastChar) >= 0) {
                period = DIGIT_STRINGS[lastChar];
                offset = offset.replace(lastChar, "");
            } else {
                period = "day(s)";
            }

            result = result + " " + offset + " " + period;
            return result;
        }

        return spec;
    }

    /**
     * Check if a string is date or contains a date
     * @param spec
     * @returns {boolean}
     */
    static isDate(spec) {
        return DATE_REG.test(spec);
    }

    /**
     * Get the date from specification string
     * @param spec
     */
    static getDate(spec) {
        return spec.match(DATE_REG)[0];
    }

    /**
     * Check if there's a code in the specification string
     * @param spec
     * @returns {boolean}
     */
    static hasCode(spec) {
        let hasCode = false;

        for (let i in CODES) {
            if (spec.indexOf(i) >= 0) hasCode = true;
        }

        return hasCode;
    }

    /**
     * Get the code from a specification string
     * @param spec
     * @returns {string|XML|*}
     */
    static getCode(spec) {
        let code;

        for (let i in CODES) {
            if (spec.indexOf(i) >= 0) code = i;
        }

        code = code.replace("{", "");
        code = code.replace("}", "");

        return code;
    }

    static hasOffset(spec) {
        let result = OFFSET_REG.test(spec); // We don't have a full offset, but we might have a partial

        return result;
    }

    static getOffset(spec) {

    }

}