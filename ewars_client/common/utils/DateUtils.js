var Moment = require('moment');

var CONSTANTS = require("../constants");

/**
 * Get most recently completed interval date
 * @param specDate
 * @param interval
 * @returns {*}
 */
var getActualDate = function (specDate, interval, no_future) {
    if (interval == CONSTANTS.DAY) return specDate;

    if (interval == CONSTANTS.WEEK) {
        var actualEndWeek = specDate.clone().endOf("isoweek");

        if (no_future) {
            if (actualEndWeek.isAfter(specDate, "d")) {
                return actualEndWeek.clone().subtract(7, "d");
            } else {
                return actualEndWeek;
            }
        } else {
            return actualEndWeek;
        }
    }

    if (interval == CONSTANTS.MONTH) {
        var actualEndMonth = specDate.clone().endOf("month");

        if (specDate.isSame(actualEndMonth, "day")) return actualEndMonth;
        if (specDate.isBefore(actualEndMonth, "day")) return actualEndMonth.clone()
            .subtract(1, "M")
            .endOf("month");
    }

    if (interval == CONSTANTS.YEAR) {
        var actualEndYear = specDate.clone().endOf("year");

        if (specDate.isSame(actualEndYear, "day")) return actualEndYear;
        if (specDate.isBefore(actualEndYear, "day"))
            return actualEndYear.clone().subtract(1, "y").endOf("year");
    }
};

var getDaysBetween = function (startDate, endDate) {
    var resultantDates = [startDate];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, CONSTANTS.DAY)) {
        var newDate = cur_date.clone().add(1, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;

};

var getWeeksBetween = function (startDate, endDate) {
    var resultantDates = [startDate.clone()];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, "day")) {
        var newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }


    return resultantDates;

};

var getMonthsBetween = function (startDate, endDate) {

};

var getYearsBetween = function (startDate, endDate) {

};

var INTERVAL_MAP = {
    "DAY": "d",
    "WEEK": "d",
    "MONTH": "M",
    "YEAR": "Y"
};

module.exports = {
    /**
     * Returns a list of dates occurring between two dates
     * of a specific interval type
     * @param start_date
     * @param end_date
     * @param interval
     */
    getDatesInInterval: function (start_date, end_date, interval) {

        var startDate = Moment(start_date),
            endDate;

        if (end_date) endDate = Moment(end_date);
        if (!end_date) endDate = Moment();

        var actualEnd = getActualDate(endDate, interval, true);
        var actualStart = getActualDate(startDate, interval);

        if (interval == CONSTANTS.DAY) return getDaysBetween(actualStart, actualEnd);
        if (interval == CONSTANTS.WEEK) return getWeeksBetween(actualStart, actualEnd);
        if (interval == CONSTANTS.MONTH) return getMonthsBetween(actualStart, actualEnd);
        if (interval == CONSTNATS.YEAR) return getYearsBetween(actualStart, actualEnd);

        return []
    },

    getMostRecentlyEndedInterval: function (reportDate, interval) {
        return getActualDate(reportDate, interval);
    },

    getStartDate: function (specDate, interval) {
        if (interval == CONSTANTS.DAY) return Moment(specDate).clone();
        if (interval == CONSTANTS.WEEK) return Moment(specDate).clone().startOf("isoweek");
        if (interval == CONSTANTS.MONTH) return Moment(specDate).clone().startOf("month");
        if (interval == CONSTANTS.YEAR) return Moment(specDate).clone().startOf("year");
    },

    getEndDate: function (specDate, interval) {
        if (interval == CONSTANTS.DAY) return Moment(specDate).clone();
        if (interval == CONSTANTS.WEEK) return Moment(specDate).clone().endOf("isoweek");
        if (interval == CONSTANTS.MONTH) return Moment(specDate).clone().endOf("month");
        if (interval == CONSTANTS.YEAR) return Moment(specDate).clone().endOf("year");
    },

    processDateSpec: function (definition, reportDate) {
        var startDate,
            endDate;

        if (definition.filter) {
            if (!reportDate) endDate = Moment();
            var filterSpec = definition.filter;

            if (filterSpec == "1M") startDate = endDate.clone().subtract(1, 'M');
            if (filterSpec == "3M") startDate = endDate.clone().subtract(3, "M");
            if (filterSpec == "6M") startDate = endDate.clone().subtract(6, "M");
            if (filterSpec == "1Y") startDate = endDate.clone().subtract(1, "Y");
            if (filterSpec == "YTD") {
                startDate = endDate.clone().startOf("year");
            }

        } else {
            if (definition.end_date_spec == "MANUAL") {
                endDate = Moment(definition.end_date);
            }

            if (definition.end_date_spec == "REPORT_DATE") {
                endDate = Moment(reportDate);
            }

            if (definition.end_date_spec == "REPORT_DATE_SUBTRACTION") {
                var units = parseInt(definition.end_intervals);
                // If the time interval is weeks, we use days for the subtraction as
                // isoweeks isn't very reliable
                if (definition.timeInterval == "WEEK") {
                    units = 7 * units;
                }
                endDate = Moment(reportDate).clone().subtract(units, INTERVAL_MAP[definition.timeInterval]);
            }

            if (definition.start_date_spec == "MANUAL") {
                startDate = Moment(definition.start_date);
            }

            if (definition.start_date_spec == "END_SUBTRACTION") {
                var units = parseInt(definition.start_intervals);
                // If the time interval is weeks, we use days for the subtraction as
                // isoweeks isn't very reliable
                if (definition.timeInterval == "WEEK") {
                    units = 7 * units;
                }
                startDate = endDate.clone().subtract(units, INTERVAL_MAP[definition.timeInterval]);
            }

            if (definition.start_date_spec == "REPORT_DATE") {
                startDate = Moment(reportDate).startOf(INTERVAL_MAP[definition.timeInterval]);
            }
        }

        return [startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD")];

    },

    getNextPeriod: function (startDate, interval) {
        if ([CONSTANTS.NONE, CONSTANTS.DAY].indexOf(interval) >= 0) return startDate.clone().add(1, 'd');
        if (interval == CONSTANTS.WEEK) return startDate.clone().add(7, 'd');
        if (interval == CONSTANTS.MONTH) return null;
        if (interval == CONSTANTS.YEAR) return startDate.clone().add(365, "d");
    },

    getPreviousPeriod: function (startDate, interval) {
        if ([CONSTANTS.NONE, CONSTANTS.DAY].indexOf(interval) >= 0) return startDate.clone().subtract(1, "d");
        if (interval == CONSTANTS.WEEK) return startDate.clone().subtract(7, 'd');
        if (interval == CONSTANTS.MONTH) return null;
        if (interval == CONSTANTS.YEAR) return startDate.clone().subtract(365, 'd');
    }

};