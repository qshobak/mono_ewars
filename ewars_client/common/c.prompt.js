import Button from "./c.button";

class Prompt extends React.Component {
    static defaultProps = {
        confirmText: "OK",
        cancelText: "Cancel"
    };

    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.data);

        let actions = [
            {label: this.props.confirmText, action: "OK"},
            {label: this.props.cancelText, action: "CANCEL"}
        ];

        if (this.props.data.actions) actions = this.props.data.actions;

        return (
            <div className="prompt-wrapper">
                <div className="prompt-container">
                    <div className="prompt-header">
                        <div className="prompt-icon"><i className={"fal " + this.props.data.icon}></i></div>
                        <div className="prompt-title">{this.props.data.title}</div>
                    </div>
                    <div className="prompt-body">{this.props.data.content}</div>
                    <div className="footer">
                        <div className="btn-group pull-right">
                            {actions.map(item => {
                                return (
                                    <ewars.d.Button
                                        onClick={this._onAction}
                                        data={item}
                                        icon={item.icon}
                                        label={item.label}/>
                                )
                            })}
                        </div>
                        <div className="clearer"></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Prompt;
