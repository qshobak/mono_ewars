var IDELayout = React.createClass({
    render: function () {
        var className = "ide-layout";

        if (this.props.light) className += " light";
        var id = null;
        if (this.props.id) id = this.props.id;

        return (
            <div className={className} id={id}>
                {this.props.children}
            </div>
        )
    }
});

export default IDELayout;
