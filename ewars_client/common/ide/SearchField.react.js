var IDESelect = React.createClass({
    _el: null,

    getInitialState: function () {
        return {
            showOptions: false
        }
    },

    _toggleShow: function () {
        var itemPos = $(this.refs.selectNode).offset();
        var itemSize = $(this.refs.selectNode).height();

        var id = _.uniqueId("SELECT_");
        var options = '<div id="' + id + '" class="ide-select-options">';

        _.each(this.props.options, function (option) {
            options += '<div class="ide-select-option" data-value="' + option[0] + '">' + option[1] + '</div>';
        });
        options += '</div>';


        this._el = $('body').append(options);
        $('#' + id).css({
            left: itemPos.left + "px",
            top: itemPos.top + itemSize + "px"
        });

        document.addEventListener("click", function () {
            this._remove(id);
        }.bind(this));

        $('.ide-select-option').on('click', function (e) {
            this.props.onUpdate(this.props.name, e.currentTarget.dataset.value);
            this._remove(id);
        }.bind(this))

    },

    _remove: function (id) {
        $('#' + id).remove();
    },

    render: function () {
        var options = _.map(this.props.options, function (optionSet) {
            return (
                <div className="ide-select-option" data-value={optionSet[0]}>{optionSet[1]}</div>
            )
        });

        var activeLabel = _.find(this.props.options, function (option) {
            if (option[0] == this.props.searchParam) return option[1];
        }, this)[1];

        return (
            <div className="ide-select" ref="selectNode">
                <div className="ide-select-current" onClick={this._toggleShow}>
                    <div className="value">{activeLabel}&nbsp;<i className="fal fa-caret-down"></i></div>
                </div>
                {this.state.showOptions ?
                    <div className="ide-select-options">
                        {options}
                    </div>
                    : null}
            </div>
        )
    }
});

var SearchField = React.createClass({
    getDefaultProps: function () {
        return {
            options: [],
            activeOption: null,
            value: null
        }
    },

    getInitialState: function () {
        return {}
    },

    componentWillMount: function () {
        this.state.value = this.props.value;

    },

    componentWillReceiveProps: function (nextProps) {
        this.state.value = nextProps.value;
    },

    _termChange: function (e) {
        var value = e.target.value;
        this.state.value = value;
        this.props.onChange(this.props.searchParam, value);
    },

    _propChange: function (prop, value) {
        this.props.onChange(value, this.state.value);
    },

    _clear: function () {
        this.props.onChange(this.props.searchParam, null);
    },

    render: function () {
        return (
            <div className="ide-search has-options">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col" style={{minWidth: 200, flex: 2}}>
                            <input type="text" value={this.props.value} onChange={this._termChange} placeholder="Search..."/>
                        </div>
                        {this.state.value ?
                            <div className="ide-col" style={{maxWidth: 25, flex: 1}}>
                                <div className="ide-search-clear" onClick={this._clear}><i className="fal fa-times-circle"></i></div>
                            </div>
                            : null}
                        <div className="ide-col border-left">
                            <IDESelect onUpdate={this._propChange}
                                       name="prop"
                                       value={this.state.value}
                                       searchParam={this.props.searchParam}
                                       options={this.props.options}/>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
});

export default SearchField;
