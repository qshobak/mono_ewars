var IDEColumn = React.createClass({
    render: function () {
        var style = {};

        if (this.props.maxHeight) style.maxHeight = this.props.maxHeight;
        if (this.props.maxWidth) style.maxWidth = this.props.maxWidth;

        return (
            <div className="ide-col" style={style}>
                {this.props.children}
            </div>
        )
    }
});

export default IDEColumn;
