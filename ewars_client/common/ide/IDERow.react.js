var IDERow = React.createClass({
    render: function () {

        var style = {};

        if (this.props.maxHeight) style.maxHeight = this.props.maxHeight;
        if (this.props.maxWidth) style.maxwidth = this.props.maxWidth;

        return (
            <div className="ide-row" style={style}>
                {this.props.children}
            </div>
        );
    }
});

export default IDERow;
