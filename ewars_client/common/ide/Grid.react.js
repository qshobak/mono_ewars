'use strict';
const Button = require("../ButtonComponent.react");
const SelectField = require("../fields/SelectField.react");
//const IndicatorSelector
//const LocationSelector
//const DatePicker

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header"></div>
        )
    }
}

class Cell extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="grid-cell"></div>
        )
    }
}

class Row extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="grid-row">

            </div>
        )
    }
}

class FilterControl extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="grid-filter-control"></div>
        )
    }
}

class Pagination extends React.Component {
    constructor(props) {
        super(props);
    }

    changePage = (data) => {
        this.props.onChange(data);
    };

    render() {
        let paginationString;
        if (this.props.count) {
            let startShow, endShow;
            if (this.props.page == 0) {
                startShow = 1;
                endShow = this.props.limit;
            } else {
                startShow = ((this.props.limit) * this.props.page) + 1;
                endShow = startShow + 9;
            }
            paginationString = `Showing ${startShow} to ${endShow} of ${this.props.count}`;
        } else {
            paginationString = "Loading...";
        }

        return (
            <div className="ide-tbar">
                <div className="ide-tbar-text">{paginationString}</div>
                <div className="btn-group pull-right">
                    <ewars.d.Button
                        icon="fa-step-backward"
                        onClick={this.changePage}
                        data={{page: "B"}}/>
                    <ewars.d.Button
                        icon="fa-fast-backward"
                        onClick={this.changePage}
                        data={{page: "BB"}}/>
                    <ewars.d.Button
                        icon="fa-fast-forward"
                        onClick={this.changePage}
                        data={{page: "FF"}}/>
                    <ewars.d.Button
                        icon="fa-step-forward"
                        onClick={this.changePage}
                        data={{page: "F"}}/>
                </div>
            </div>
        )
    }
}

class Grid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            filters: ewars.copy(this.props.filters || {}),
            order: ewars.copy(this.props.order || {}),
            page: 0,
            limit: 0,
            offset: 0,
            count: 0,
            searchEnabled: false,
            searchTerm: null,
            searchParam: this.props.search ? this.props.search[0][0] : null
        }
    }

    static defaultProps = {
        resource: "default",
        rowButtons: [],
        joins: null,
        searchOptions: null,
        columns: null
    };

    componentDidMount() {
        let height = this.refs.grid.clientHeight;
        let numRows = (height - (height % 40)) / 40;
        this.state.limit = numRows + 5;
        this.query();
    }

    query = () => {
        let blocker = new ewars.Blocker(null, "Loading data...");
        ewars.tx("com.ewars.query", [this.state.resource, null, this.state.filters, this.state.order, this.state.limit, this.state.offset, this.props.joins, true])
            .then(function (resp) {
                this.setState({
                    data: resp.data,
                    count: resp.count
                });
                blocker.destroy();
            }.bind(this))
    };

    onSearchChange = (param, value) => {
        this.state.searchTerm = value;
        this.state.searchParam = param;

        if (!value || value == null) {
            var wasFilled = false;
            if (this.state.filters[param]) {
                wasFilled = true;
                delete this.state.filters[param];
            }

            if (wasFilled) this.query();
        } else {
            this.state.filters[param] = {LIKE: value};
        }

        if (value != null && value.length >= 3) {
            this.query();
        } else {
            this.forceUpdate();
        }
    };

    changePage = (data) => {
        switch (data.page) {
            case "FF":
                if (this.state.offset < this.state.count) {
                    var pages = this.state.count / this.state.limit;
                    this.state.page = Math.ceil(pages);
                    this.state.offset = (this.state.page - 1) * this.state.limit;
                }
                break;
            case "F":
                if (this.state.offset < this.state.count) {
                    this.state.offset = (this.state.page + 1) * this.state.limit;
                    this.state.page++;
                }
                break;
            case "B":
                this.state.offset = (this.state.page - 1) * this.state.limit;
                this.state.page--;
                break;
            case "BB":
                this.state.offset = 0;
                this.state.page = 0;
                break;
            default:
                break;
        }

        if (this.state.page < 0) this.state.page = 0;
        if (this.state.page <= 0) this.state.offset = 0;

        this.forceUpdate();
        this.query();
    };

    onSortChange = (sorting) => {
        this.state.ordering = sorting;
        if (this.isMounted()) this.forceUpdate();
        this.query();
    };

    onFilterChange = (filters) => {
        this.state.filters = filters;
        if (this.isMounted()) this.forceUpdate();
        this.query();
    };

    render() {

        let items = this.state.data.forEach(function (item) {
            return (
                <Row
                    data={item}
                    columns={this.props.columns}
                    buttons={this.props.buttons}/>
            )
        }.bind(this));

        return (
            <div className="ide-inline-grid ide-grid-fill">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div style={{width: 350}}>
                                    <SearchField
                                        options={SEARCH_OPTIONS}
                                        onChange={this._onSearchChange}
                                        searchParam={this.state.searchParam}
                                        value={this.state.search}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            <div className="ide-layout">
                                <div className="ide-row" style={{maxHeight: 20}}>
                                    <div className="ide-col">
                                        <div className="ide-grid">
                                            <div className="headers">
                                                <div className="header">Name</div>
                                                <div className="header">Type</div>
                                                <div className="header">Last Modified</div>
                                                <div className="header">Status</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="ide-row">
                                    <div className="ide-col">
                                        <div className="ide-panel ide-scroll ide-panel-v"
                                             ref="grid">
                                            <div className="grid-rows">
                                                {items}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <Pagination
                                limit={this.state.limit}
                                offset={this.state.offset}
                                page={this.state.page}
                                count={this.state.count}
                                onChange={this.changePage}/>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Grid;
