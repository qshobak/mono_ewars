var IDEPanel = React.createClass({
    render: function () {
        return (
            <div className="ide-panel">
                {this.props.children}
            </div>
        )
    }
});

export default IDEPanel;
