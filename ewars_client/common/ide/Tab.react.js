class Tab extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = (e) => {
        this.props.onClick(this.props.data);
    };

    render() {
        let className = "panelsbar-btn";
        if (this.props.active) className += " panelsbar-btn-active";
        let icon;
        if (this.props.icon) {
            icon = <i className={"fal " + this.props.icon}></i>
        }

        return (
            <div xmlns="http://www.w3.org/1999/xhtml"
                 onClick={this.onClick}
                 className={className}
                 style={{padding: 0}}>
                <div className="ide-row">
                    {icon ?
                        <div className="ide-col"
                             style={{maxWidth: 20, flexBasis: 0, flexGrow: 1, padding: "8px 5px", textAlign: "center", borderTopLeftRadius: "3px"}}>{icon}</div>
                        : null}
                    {this.props.label ?
                        <div className="ide-col"
                             style={{wordBreak: "keep-all", flex: "0 0 auto", padding: 8}}>
                            {this.props.label}
                        </div>
                        : null}
                </div>
            </div>
        )
    }
}

export default Tab;
