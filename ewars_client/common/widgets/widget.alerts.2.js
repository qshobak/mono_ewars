import mapboxgl from "mapbox-gl";
import Button from "../c.button";
import CONSTANTS from "../constants";
import Modal from "../c.modal";
import turf from "turf";

import Tab from "../c.tab";

import Spinner from "../c.spinner";


var STAGES = {
    VERIFICATION: "Verification",
    RISK_ASSESS: "Risk Assessment",
    RISK_CHAR: "Risk Characterisation",
    OUTCOME: "Outcome"
};

var markerTemplate = _.template([
    '<div class="map-popup-wrapper">',
    '<table width="100%">',
    '<tr><th>Name:</th><td><%= alarm_name %></td></tr>',
    '<tr><th>Location:</th><td><%= location_name %></td></tr>',
    '<tr><th>Date:</th><td><%= trigger_period %></td></tr>',
    '</table>',
    '<a target="_blank" href="/alert#?uuid=<%= alert_uuid %>">More info</a>',
    '</div>'
].join(""));

class AlertsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            data: []
        }
    }

    componentWillMount() {
        this._query(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this._query(nextProps);
    }

    _query = (props) => {
        ewars.tx("com.ewars.query", ["alert_full", null, {
            location_id: {under: this.props.location_id},
            state: {eq: this.props.status}
        }, null, null, null, null])
            .then((resp) => {
                this.setState({
                    loaded: true,
                    data: resp
                })
            })
    };

    render() {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="block-tree" style={{position: "relative"}}>
                    {this.state.data.map((item) => {
                        return <AlertListItem data={item}/>
                    })}
                </div>
            </div>
        )
    }
}

var AlertListItem = React.createClass({
    _onClick: function () {
        document.location = "/alert#?uuid=" + this.props.data.uuid;
    },

    render: function () {
        var alertPeriod = ewars.formatters.DATE_FORMATTER(this.props.data.trigger_end, "DAY");

        var alarmName = ewars.formatters.I18N_FORMATTER(this.props.data.alarm_name);
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.location_name);
        return (
            <div className="block" onClick={this._onClick}>
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            {alarmName}&nbsp;{locationName}&nbsp;{alertPeriod}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
});

var AlertDate = React.createClass({
    _onClick: function () {
        document.location = "/alert#?uuid=" + this.props.data.alert.uuid;
    },

    render: function () {

        var alertPeriod = ewars.formatters.DATE_FORMATTER(this.props.data.alert.trigger_end, this.props.data.alert.interval_type);

        var alarmName = ewars.formatters.I18N_FORMATTER(this.props.data.alert.alarm_name);
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.alert.location_name);
        return (
            <div className="alert-list-item" onClick={this._onClick}>
                <div className="period">{alertPeriod}</div>
                <div className="clearer"></div>
                <div className="period">Stage: {STAGES[this.props.data.alert.stage]}</div>
            </div>
        )
    }
});

var AlertGroup = React.createClass({
    _onClick: function () {

    },

    render: function () {

        var locationName = ewars.I18N(this.props.data[0].location.name);

        var alerts = _.sortBy(this.props.data, function (item) {
            return item.alert.trigger_end;
        }).reverse();

        alerts = _.map(alerts, function (alert) {
            return <AlertDate data={alert} key={alert.uuid}/>
        });

        return (
            <div className="alert-location-group">
                <h3>{locationName}</h3>
                <div className="alerts-list">
                    {alerts}
                </div>
            </div>
        );
        return null;
    }
});


var AlarmGroup = React.createClass({
    render: function () {
        var alarmName = ewars.I18N(this.props.data[0].alert.alarm_name);

        var alerts = _.sortBy(this.props.data, function (alert) {
            return alert.alert.trigger_end;
        }, this);

        var grouped = _.groupBy(alerts, function (alert) {
            return alert.alert.location_id;
        }, this);

        alerts = _.map(grouped, function (group) {
            return <AlertGroup data={group} key={group}/>;
        }, this);

        return (
            <div className="alert-list-group">
                <h3>{alarmName}</h3>
                <div className="alerts-list">
                    {alerts}
                </div>
            </div>
        )
    }
});

class Alert extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = () => {
        window.open("/alert#?uuid=" + this.props.data.properties.alert_uuid);
    };

    render() {
        return (
            <div className="block" onClick={this.onClick}>
                <div className="block-content">
                    <div className="ide-row">
                        <div
                            className="ide-col">{this.props.data.properties.alarm_name}
                            - {this.props.data.properties.location_name}</div>
                        <div className="ide-col" style={{
                            maxWidth: 150,
                            textAlign: "right"
                        }}>{this.props.data.properties.trigger_period}</div>
                    </div>
                </div>
            </div>
        )
    }
}

const MODAL_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CLOSE"}
];

class AlertsGen2 extends React.Component {
    _markers = {
        type: "geojson",
        cluster: true,
        clusterMaxZoom: 14,
        clusterRadius: 50,
        data: {
            type: "FeatureCollection",
            features: []
        }
    };

    _geoms = {
        type: "geojson",
        data: {
            type: "FeatureCollection",
            features: []
        }
    };

    _unclustered = {
        type: "geojson",
        data: {
            type: "FeatureCollection",
            features: []
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            confinement: null,
            filter: "OPEN",
            highlighted: null
        }
    }

    componentWillMount() {
        this.query(this.state.filter);
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.filter != this.props.filter) {
            this.query(this.state.filter);
        }
    }

    query(filter) {
        ewars.tx('com.ewars.alerts.query', [filter, 1000, 0], {lt: window.config.ALARM_DEFAULT_SITE_TYPE || 10})
            .then(resp => {
                this.setState({
                    data: resp,
                    filter: filter
                }, () => {
                    if (this._mapLoaded) this._run();
                    this._dataLoaded = true;
                    blocker.destroy();
                })
            })
            .catch(err => {
                this.refs.map.innerHTML = '';
                let errIcon = document.createElement('i');
                errIcon.setAttribute('class', 'fal fa-exclamation-triangle');
                errIcon.style.fontSize = '20px';
                errIcon.style.position = 'absolute';
                errIcon.style.top = '50%';
                errIcon.style.left = '50%';
                errIcon.style.color = 'red';

                this.refs.map.appendChild(errIcon);
            })
    }

    componentDidMount() {
        this._map = null;
        this._mapLoaded = false;
        this._hasNaved = false;
        this._dataLoaded = false;

        this.mounted = true;
        this._render();
    }

    _run = () => {
        if (window.config.ALARM_DEFAULT_BOUNDS) {
            let bounds = JSON.parse(window.config.ALARM_DEFAULT_BOUNDS);
            let bbox = turf.bbox(bounds);
            this._map.fitBounds(bbox);
        } else {
            if (this.state.confinement && !this._hasNaved) {
                var centerLngLat = new mapboxgl.LngLat(parseFloat(this.state.confinement.center.coordinates[0]), parseFloat(this.state.confinement.center.coordinates[1]));
                this._map.setZoom(this.state.confinement.default_zoom);
                this._map.setCenter(centerLngLat);
                this._hasNaved = true;
            }
        }

        var data = {
            type: "geojson",
            cluster: true,
            clusterRadius: 50,
            clusterMaxZoom: 14,
            data: {
                type: "FeatureCollection",
                features: []
            }
        };

        let admin_data = {
            type: "geojson",
            data: {
                type: "FeatureCollection",
                features: []
            }
        };

        let withGeom = this.state.data.d.filter(node => {
            return node.location.geometry != null;
        })

        withGeom.forEach(node => {
            if (node.location.geometry_type == 'POINT') {
                let geom = node.location.geometry ? node.location.geometry : {type: 'Point', coordinates: [0, 0]};
                if (geom.coordinates != [0, 0]) {
                    console.log(geom);
                    data.data.features.push({
                        type: 'Feature',
                        geometry: geom,
                        properties: {
                            location_id: node.location.uuid,
                            location_name: __(node.location.name),
                            dataValue: node.data
                        }

                    })
                }

            } else {
                let geom = node.location.geometry;
                if (geom.features && geom.features.length > 0) {
                    geom = geom.features[0].geometry || null;
                    admin_data.data.features.push({
                        type: 'Feature',
                        geometry: geom,
                        properties: {
                            location_id: node.location.uuid,
                            location_name: __(node.location.name),
                            dataValue: node.data
                        }
                    })
                }
            }
        });

        this._markers = data;

        if (this._map) {
            this._map.getSource("markers")
                .setData(data.data);
            this._map.getSource("unclustered")
                .setData(data.data);
            this._map.getSource("geoms")
                .setData(admin_data.data);
        }

    };

    _render = () => {
        if (!this._map) {
            mapboxgl.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
            this._map = new mapboxgl.Map({
                container: this._el,
                style: 'mapbox://styles/jduren/cim4smtop0114ccm3nf2wz0ya',
                width: "100%",
                height: "100%",
                center: [-15.240329, -0.000000],
                zoom: 1.31,
                attributionControl: false
            });

            this._map.on("style.load", function () {
                this._map.addSource("markers", this._markers);
                this._map.addSource("geoms", this._geoms);
                this._map.addSource("unclustered", this._unclustered);


                this._map.addLayer({
                    "id": "unclustered-points",
                    "type": "circle",
                    "source": "markers",
                    "filter": ["!has", "point_count"],
                    "paint": {
                        "circle-color": "orange",
                        "circle-radius": 25
                    }
                });

                var layers = [
                    [150, 'orange'],
                    [20, 'orange'],
                    [0, 'orange']
                ];

                layers.forEach((layer, i) => {
                    this._map.addLayer({
                        "id": "cluster-",
                        "type": "circle",
                        "source": "markers",
                        "paint": {
                            "circle-color": layer[1],
                            "circle-radius": 18
                        },
                        "filter": i === 0 ?
                            [">=", "point_count", layer[0]] :
                            ["all",
                                [">=", "point_count", layer[0]],
                                ["<", "point_count", layers[i - 1][0]]]
                    });
                });

                this._map.addLayer({
                    "id": "geoms",
                    "type": "fill",
                    "source": "geoms",
                    "layout": {},
                    "paint": {
                        "fill-color": "orange",
                        "fill-opacity": 0.8,
                        "fill-outline-color": "#CCCCCC"
                    }
                });

                this._map.addLayer({
                    "id": "cluster-count",
                    "type": "symbol",
                    "source": "markers",
                    "layout": {
                        "text-field": "{location_name}\n{dataValue}",
                        "text-font": [
                            "DIN Offc Pro Medium",
                            "Arial Unicode MS Bold"
                        ],
                        "text-size": 12
                    }
                });

                this._map.addLayer({
                    "id": "geom-count",
                    "type": "symbol",
                    "source": "geoms",
                    "layout": {
                        "text-field": "{location_name}\n{dataValue}",
                        "text-font": [
                            "DIN Offc Pro Medium",
                            "Arial Unicode MS Bold"
                        ],
                        "text-size": 12
                    }
                });

                this._map.addLayer({
                    "id": "cluster-counter",
                    "type": "symbol",
                    "source": "markers",
                    "layout": {
                        "text-field": "{location_name}\n{dataValue}",
                        "text-font": [
                            "DIN Offc Pro Medium",
                            "Arial Unicode MS Bold"
                        ],
                        "text-size": 12
                    }
                });

                this._mapLoaded = true;

                if (this._dataLoaded) {
                    this._run();
                }

                this._map.on("click", function (e) {
                    var features = this._map.queryRenderedFeatures(e.point, {
                        layers: ["unclustered-points", "geoms"],
                        radius: 100,
                        includeGeometry: true
                    });

                    if (features.length) {
                        this.setState({
                            location_id: features[0].properties.location_id,
                            highlighted: true
                        })
                    }

                }.bind(this));


                this._map.on("mousemove", function (e) {
                    var features = this._map.queryRenderedFeatures(e.point, {
                        radius: 20
                    });

                    this._map.getCanvas().style.cursor = (features.length) ? "pointer" : "";
                }.bind(this));

            }.bind(this));

        }
    };

    _changeFilter = (data) => {
        this.query(data.filter);
    };

    _onModalAction = (action) => {
        this.setState({
            highlighted: null
        })
    };

    render() {
        let alerts;
        if (!this._dataLoaded) alerts = <Spinner/>;

        return (
            <div className="ide-layout" style={{minHeight: "490px", maxHeight: "490px"}}>
                <ewars.d.Toolbar label="Alerts">

                </ewars.d.Toolbar>
                <div className="ide-row">
                    <div className="ide-col border-right">
                        <div className="ide-panel ide-panel-absolute">

                            <div className="alert-dashboard">
                                <div className="map" id="map" ref={(el) => this._el = el}></div>
                            </div>

                        </div>
                    </div>
                </div>
                <ewars.d.Shade
                    shown={this.state.highlighted}
                    title="Alerts"
                    buttons={MODAL_ACTIONS}
                    onAction={this._onModalAction}
                    icon="fa-bell">
                    {this.state.highlighted ?
                        <AlertsList
                            status={this.state.filter}
                            location_id={this.state.location_id}/>
                        : null}
                </ewars.d.Shade>
            </div>
        )
    }
}

export default AlertsGen2;
