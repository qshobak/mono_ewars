class Image extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.data.image_url && this.props.data.image_url != "") {
            return (
                <a href={this.props.data.image_url}>
                    <img width="100%" src={this.props.data.image_url}/>
                </a>
            )
        } else {
            return (
                <img width="100%" src={this.props.data.image_url}/>
            )
        }

    }
}

export default Image;