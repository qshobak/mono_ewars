import Spinner from "../c.spinner";
import Modal from "../c.modal";

const MODAL_ACTIONS = [
    {action: "CLOSE", label: "Close", icon: "fa-times"}
];

class DateItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="missed" onClick={this._onClick}>{ewars.DATE(this.props.data, this.props.interval)}</div>
        )
    }

}

class Item extends React.Component {
    constructor(props) {
        super(props);

        this.state = {show: false}
    }

    componentWillMount() {
        this.props.data.dates.reverse();
    }

    _missing = () => {
        this.setState({
            show: true
        })
    };

    _onModalAction = (action) => {
        this.setState({show: false})
    };

    _select = (dt) => {
        window.open(`/reporting#?location_id=${this.props.data.location_id}&form_id=${this.props.data.form_id}&data_date=${dt}`)
    };

    render() {
        let formName = ewars.I18N(this.props.data.name);
        let locName = ewars.I18N(this.props.data.location_name);

        let sample = this.props.data.dates.slice(0, 3);
        let rest = this.props.data.dates.slice(3);

        let showMore = false;
        if (rest.length > 0) showMore = true;

        return (
            <li className="overdue-item">
                <div className="header">
                    <div className="title">{formName}</div>
                    <div className="content">{locName}</div>
                </div>
                <div className="missing">
                    {sample.map(dt => {
                        return (
                            <DateItem data={dt} interval={this.props.data.interval} onClick={this._select}/>
                        )
                    })}
                    {showMore ?
                        <div className="missed" onClick={this._missing}>{rest.length} more...</div>
                        : null}
                    <div className="clearer"></div>
                </div>
                {this.state.show ?
                    <Modal
                        height={300}
                        width={800}
                        buttons={MODAL_ACTIONS}
                        onAction={this._onModalAction}
                        title={`Missing Submissions: ${formName} - ${locName}`}
                        visible={this.state.show}>
                        <div className="ide-panel ide-panel-absolute ide-scroll"
                             style={{paddingTop: 8, paddingBottom: 8}}>
                            <div className="missing">
                                {this.props.data.dates.map(dt => {
                                    return <DateItem data={dt} interval={this.props.data.interval}
                                                     onClick={this._select}/>
                                })}
                            </div>
                        </div>
                    </Modal>
                    : null}
            </li>
        )
    }
}

class Overdue extends React.Component {
    _isLoaded = false;

    static defaultProps = {
        fitted: false
    }

    constructor(props) {
        super(props);

        this.state = {data: []}
    }

    componentWillMount() {
        ewars.tx("com.ewars.user.overdue")
            .then(resp => {
                this._isLoaded = true;
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        if (window.user.role != "USER") {
            return (
                <div className="placeholder">Overdue reports are not available for your role.</div>
            )
        }

        let style = {maxHeight: "250px"};
        if (this.props.fitted) {
            style = {
                display: "block"
            }
        }

        if (!this._isLoaded) return <Spinner/>;
        return (
            <div className="overdue-list" style={style}>
                {this.state.data.map(item => {
                    return (
                        <Item data={item}/>
                    )
                })}
            </div>
        )
    }
}

export default Overdue;
