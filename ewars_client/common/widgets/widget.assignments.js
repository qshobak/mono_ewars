import AssignRequest from "../c.assignment_requestor";
import Shade from "../c.shade";

import Lang from "../lang/Lang";

const ASSIGN_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CANCEL"}
];

var AssignmentLink = React.createClass({
    _go: function () {
        let uri = "http://" + ewars.domain + "/reporting#?";
        uri += "form_id=" + this.props.assignment.form_id;
        if (this.props.location) uri += "&location_id=" + this.props.location.location_id;
        document.location = uri;
    },

    render: function () {
        var formName = ewars.I18N(this.props.assignment.form_name);

        let location;
        if (this.props.location.location_id) {
            location = ewars.I18N(this.props.location.name[1]);
        }

        return (
            <div className="iw-list-item" onClick={this._go}>
                <div className="title">{formName}</div>
                {location ?
                    <div className="content">{location}</div>
                    : null}
            </div>
        )
    }
});

class LocationProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentWillMount() {
        this._loadLocation();
    }

    _loadLocation = () => {
        ewars.tx("com.ewars.location.profile", [window.user.location_id])
            .then((resp) => {
                this.setState({data: resp})
            })
    };

    render() {
        return (
            <div></div>
        )
    }
}

var AssignmentsWidgetComponent = React.createClass({
    getDefaultProps: function () {
        return {
            data: {}
        }
    },

    getInitialState: function () {
        return {
            assignments: [],
            assignmentRequestVis: false
        }
    },

    onClick: function (node) {
        document.location = "/reporting#/form/" + node.data.form_id;
    },

    _showAssignmentRequest: function () {
        this.setState({
            assignmentRequestVis: true
        })
    },

    componentWillMount: function () {
        if (window.user.role == "USER") {
            this._getAssignments();
        }
    },

    _getAssignments: function () {
        ewars.tx("com.ewars.user.assignments")
            .then((resp) => {
                this.setState({
                    assignments: resp
                })
            })
    },

    _hideAssignmentCreator: function () {
        this.setState({
            assignmentRequestVis: false
        })
    },

    _onModalAction: function (action) {
        if (action == "CANCEL") this.setState({assignmentRequestVis: false});
        if (action == "CLOSE") this.setState({assignmentRequestVis: false});

    },

    render: function () {
        if (window.user.role == "REGIONAL_ADMIN") {
            return <LocationProfile/>;
        }

        if (window.user.role !== "USER") {
            return (
                <div className="placeholder">Assignments are not applicable to your user type.</div>
            )
        }
        let assigns = [];

        this.state.assignments.forEach(assignment => {
            assignment.locations.forEach(location => {
                assigns.push(
                    <AssignmentLink
                        key={location.uuid + "_" + assignment.uuid}
                        assignment={assignment}
                        location={location}/>
                )
            })
        });

        if (assigns.length <= 0) assigns = <div className="placeholder">No Assignments</div>;

        return (
            <div style={{flex: 1, position: "relative", minHeight: '300px', maxHeight: '300px', overflowY: 'auto'}}>
                <div
                    className="assign-request-button"
                    onClick={this._showAssignmentRequest}>
                    <i className="fal fa-plus"></i>&nbsp;Request new assignment
                </div>
                <ul className="iw-list" style={{borderTop: "1px solid #CCCCCC"}}>
                    {assigns}
                </ul>

                <Shade
                    title="Assignments"
                    icon="fa-clipboard"
                    onAction={this._onModalAction}
                    actions={ASSIGN_ACTIONS}
                    shown={this.state.assignmentRequestVis}>
                    <AssignRequest/>
                </Shade>
            </div>
        )
    }
});

export default AssignmentsWidgetComponent;
