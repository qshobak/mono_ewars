var numeral = require('numeral');
var Moment = require("moment");

var Button = require("../../../common/components/ButtonComponent.react");

var COLOURS = [
    "orange", "red", "purple", "pink", "yellow", "green", "magenta", "grey", "black", "aque", "blue", "chocolate", "cyan"
];

var CountryReports = React.createClass({
    _series: null,
    _chart: null,

    getInitialState: function () {
        return {
            data: null
        };
    },

    componentWillMount: function () {
        this._init(this.props);
    },

    componentWillReceiveProps: function (nextProps) {
        this._init(nextProps);
    },

    _init: function (props, btnRef) {
        ewars.tx("com.ewars.account.reporting.frequency", [props.timeFilter, props.formId])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
                this._renderChart();
            }.bind(this))
    },

    componentDidMount: function () {
        this._loadChart();
    },

    _renderChart: function () {
        while (this._chart.series.length > 0)
            this._chart.series[0].remove(true);

        var minDate = null;
        var maxDate = null;

        var colCount = 0;
        _.each(this.state.data, function (data_item) {
            var graph = {
                type: "line",
                name: ewars.formatters.I18N_FORMATTER(data_item.form_name)
            };

            graph.data = _.sortBy(data_item.data, function (node) {
                return node[0]
            }, this);

            if (!minDate) {
                if (graph.data[0]) minDate = graph.data[0][0];
            } else {
                if (graph.data[0]) {
                    if (Moment(graph.data[0][0]).isBefore(Moment(minDate), "day")) minDate = graph.data[0][0];
                }
            }
            if (!maxDate) {
                if (graph.data[graph.data.length - 1]) maxDate = graph.data[graph.data.length - 1][0];
            } else {
                if (graph.data[graph.data.length - 1]) {
                    if (Moment(graph.data[graph.data.length - 1][0]).isAfter(Moment(maxDate), "day")) maxDate = graph.data[graph.data.length][0];
                }
            }

            graph.data = _.map(graph.data, function (node) {
                return [Moment.utc(node[0], "YYYY-MM-DD").valueOf(), parseFloat(node[1])];
            }, this);

            graph.color = COLOURS[colCount];
            colCount++;

            this._chart.addSeries(graph);
        }, this);

        this._chart.xAxis[0].setExtremes(
            Moment.utc(minDate, "YYYY-MM-DD").valueOf(),
            Moment.utc(maxDate, "YYYY-MM-DD").valueOf()
        );

        this._chart.redraw();
    },

    _loadChart: function () {
        // Map in options
        var options = {
            chart: {
                renderTo: this.refs.chart.getDOMNode(),
                zoomType: "x"
            },
            credits: {enabled: false},
            title: {
                text: null,
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            navigator: {
                enabled: true
            },
            yAxis: {
                title: {
                    text: "Reports Submitted"
                },
                labels: {
                    formatter: function () {
                        return numeral(this.value).format("0,0.0")
                    }
                },
                min: 0
            },
            xAxis: {
                type: "datetime"
            }
        };

        if (!this._chart) {
            this._chart = new Highcharts.Chart(options);
        }
    },

    render: function () {
        return (
            <div className="widget">
                <div className="body">
                    <div className="chart" ref="chart">

                    </div>
                </div>
            </div>
        )
    }
});

module.exports = CountryReports;