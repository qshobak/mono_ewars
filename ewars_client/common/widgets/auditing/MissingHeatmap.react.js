var Moment = require("moment");

var MissingHeatmap = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentDidMount: function () {
        this._init(this.props);
        this._render();
    },

    componentWillReceiveProps: function (nextProps) {
        this._init(nextProps);
    },

    _init: function (props) {
        ewars.tx("com.ewars.analysis.missing", [props.timeFilter, props.formId])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this._updateChart();
            }.bind(this))
    },

    _updateChart: function () {
        while (this._chart.series.length > 0)
            this._chart.series[0].remove(true);

        while (this._chart.xAxis.length > 0)
            this._chart.get(this._chart.xAxis[0].id).remove();

        while (this._chart.yAxis.length > 0)
            this._chart.get(this._chart.yAxis[0].id).remove();


        // Add Y Axis
        var locids = _.map(this.state.data, function (dataSeries, locationUUID) {
            return ewars.formatters.I18N_FORMATTER(dataSeries.location_name);
        }, this);

        this._chart.addAxis({
            categories: locids,
            type: "category",
            title: {
                text: "Location"
            }
        }, false, true, false);

        // Add X Axis
        var dates = [];
        _.each(this.state.data, function (dataSeries) {
            var subDates = _.map(dataSeries.data, function (dp) {
                return Moment(dp.data_date).format("YYYY-MM-DD");
            }, this);
            dates.push.apply(dates, subDates);
        }, this);

        dates = _.uniq(dates);

        this._chart.addAxis({
            categories: dates,
            type: "category",
            title: {
                text: "Report Dates"
            },
            opposite: true
        }, true, true, false);

        var graph = {
            borderWidth: 1,
            data: [],
            dataLabels: {
                enabled: true,
                color: "#333333",
                formatter: function (point) {
                    return this.point.available + "/" + this.point.expected;
                }
            }
        };
        graph.data = [];

        var counter = 0;
        _.each(this.state.data, function (dataSeries, locationUUID) {
            var sortedData = _.sortBy(dataSeries.data, function (dp) {
                return dp.data_date;
            }, this);

            var dateCounter = -1;
            var series = _.map(sortedData, function (dp) {
                dateCounter++;
                var color = "#FFFFFF";
                if (parseFloat(dp.a) >= parseFloat(dp.e)) color = "#B3FF99";
                if (parseFloat(dp.a) < parseFloat(dp.e) && parseFloat(dp.a) > 0) color = "#FFE6D0";
                if (parseFloat(dp.a) == 0 && parseFloat(dp.e) > 0) color = "#FFF2F2";
                return {
                    y: counter,
                    x: dateCounter,
                    value: parseFloat(dp.a),
                    color: color,
                    available: parseFloat(dp.a),
                    expected: parseFloat(dp.e),
                    report_date: dp.data_date,
                    location_name: dataSeries.location_name
                }
            }, this);

            graph.data.push.apply(graph.data, series);
            counter++;

        }, this);

        this._chart.addSeries(graph);
        this._chart.redraw();

        var height = locids.length * 30 + 60;
        this._chart.setSize(this.refs.chartNode.getDOMNode().offsetWidth, height, false);
    },

    _render: function () {
        // Map in options
        var options = {
            chart: {
                type: "heatmap",
                renderTo: this.refs.chartNode.getDOMNode(),
                zoomType: "x",
                plotBorderWidth: 1,
                plotBorderColor: "#333333"
            },
            credits: {enabled: false},
            title: {
                text: null,
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    var tip = "";
                    tip += '<b>Location:</b> ' + ewars.formatters.I18N_FORMATTER(this.point.location_name) + '<br />';
                    tip += '<b>Report Date:</b> ' + Moment(this.point.data_date).format("YYYY-MM-DD") + '<br/><br/>';
                    tip += '<b>' + this.point.available + '</b> (Submitted) / <b>' + this.point.expected + '</b> (Expected)';
                    return tip;
                }
            },
            plotOptions: {
                borderColor: "#333333"
            },
            exporting: {
                enabled: true,
                scale: 1,
                width: 1200,
                chartOptions: {
                    width: 1200
                }
            },
            legend: {
                enabled: false,
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 280
            },
            series: []
        };

        if (!this._chart) {
            this._chart = new Highcharts.Chart(options);
        }
    },

    render: function () {
        return (
            <div className="widget">
                <div className="widget-header"></div>
                <div className="body" ref="chartNode">

                </div>
            </div>
        )
    }
});

module.exports = MissingHeatmap;