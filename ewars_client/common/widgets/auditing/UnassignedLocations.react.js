var ReportSet = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var formName = ewars.formatters.I18N_FORMATTER(this.props.data.form_name);

        var missing = _.map(this.props.data.no_assign, function (missing) {
            var locationName = ewars.formatters.I18N_FORMATTER(missing[1]);
            return locationName;
        });

        missing = missing.join(", ");

        return (
            <div className="unassigned-set">
                <h3>{formName}</h3>
                <div className="unassigned">
                    {missing}
                </div>
            </div>
        )
    }
});

var UnassignedLocations = React.createClass({
    getInitialState: function () {
        return {
            data: {}
        }
    },

    componentWillMount: function () {
        this._init(this.props);
    },

    componentWillReceiveProps: function (nextProps) {
        this._init(nextProps);
    },

    _init: function (props) {
        ewars.tx("com.ewars.account.unassigned", [props.formId])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {

        var missingSets = _.map(this.state.data, function (setItem) {
            return <ReportSet data={setItem}/>
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"></div>
                <div className="body">
                    {missingSets}
                </div>
            </div>
        )
    }
});

module.exports = UnassignedLocations;