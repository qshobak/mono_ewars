import TasksWidget from "./widget.tasks";
import AssignmentsWidget from "./widget.assignments";
import Workload from "./widget.workload";
import Documents from "./widget.documents";
import Alerts from "./widget.alerts";
import RecentReports from "./widget.records.recent";
import OverallMetrics from "./widget.metrics.overall";
import Table from "../analysis/TableWidget";
import Calendar from "../analysis/Calendar";
import Gauge from "../analysis/Gauge";
import Stream from "../analysis/Stream";
import Notifications from "./widget.notifications";
import Feed from "./widget.activity";
import Deletions from "./widget.deletions";
import Amendments from "./widget.amendments";
import Text from "./widget.text";
import Overdue from "./widget.overdue";
import Upcoming from "./widget.upcoming";
import Image from "./widget.image";
import Metrics from "./widget.metrics";
import InfoBar from "./widget.info_bar";
import OutbreaksWidget from "./widget.outbreaks";

import SeriesChart from "../analysis/SeriesChart";
import PieChart from "../analysis/PieChart";
import RawValue from "../analysis/RawValue";
import Map2 from "../analysis/Map2";


import Chart from "./chart";

export default {
    OUTBREAKS: OutbreaksWidget,
    ALERTS: Alerts,
    OVERALL_METRICS: null,
    DOCUMENTS: Documents,
    TASKS: TasksWidget,
    ASSIGNMENTS: AssignmentsWidget,
    METRICS: Metrics,
    WORKLOAD: Workload,
    RECENT_REPORTS: RecentReports,
    RECENT: RecentReports,
    SERIES: SeriesChart,
    CATEGORY: PieChart,
    RAW: RawValue,
    TABLE: Table,
    METRICS_OVERALL: OverallMetrics,
    UPCOMING: Upcoming,
    OVERDUE: Overdue,
    MAP: Map2,
    CALENDAR: Calendar,
    GAUGE: Gauge,
    STREAM: Stream,
    TEXT: Text,
    IMAGE: Image,
    DELETIONS: Deletions,
    AMENDMENTS: Amendments,
    ACTIVITY: Feed,
    INFO: InfoBar

}
