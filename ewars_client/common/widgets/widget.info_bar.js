import Moment from "moment";

const STYLE = {
    padding: "8px"
};

export default class InfoBar extends React.Component {
    render() {
        let isoW = Moment().isoWeek();
        return (
            <div className="info-bar" style={STYLE}>
                {Moment().format("LLLL")} - W{isoW} {Moment().year()}
            </div>
        )
    }
}