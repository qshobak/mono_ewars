class DataWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            limit: 5,
            offset: 0
        }
    }

    static defaultProps = {
        limit: 5,
        display: "list",
        itemTmpl: null,
        resource: null,
        query: {},
        displayName: {},
        events: {}
    };

    render() {
        let name = ewars.I18N(this.props.name);

        return (
            <div className="widget">
                <div className="widget-header"><span>{name}</span></div>
                <div className="body">

                </div>
            </div>
        )
    }
}
export default DataWidget;