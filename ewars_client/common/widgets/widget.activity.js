import Moment from "moment";

var A_WEEK_OLD = Moment().utc().clone().subtract(7, 'days').startOf('day');

function isWithinAWeek(momentDate) {
    return momentDate.isAfter(A_WEEK_OLD);
}

const CONTENT_STRINGS = {
    "NEW_USER": "has joined ewars",
    "REPORT_SUBMISSION": "submitted a new report"
};

class MetaReportItem extends React.Component {
    _onClick = () => {
        document.location = "http://" + ewars.domain + "/reporting#?uuid=" + this.props.data.meta.uuid;
    };

    render() {
        return (
            <div className="meta-item" onClick={this._onClick}>
                <div className="meta-graphic">
                    <div className="meta-icon">
                        <i className="fal fa-clipboard"></i>
                    </div>
                </div>
                <div className="meta-content">
                    <div className="meta-title">{this.props.data.meta.form_name}</div>
                    <div className="meta-subtitle">{this.props.data.meta.location_name}</div>
                    <div
                        className="meta-additional">{ewars.DATE(this.props.data.meta.data_date, this.props.data.meta.reporting_interval)}</div>
                </div>
            </div>
        )
    }
}

class MetaItemNewUser extends React.Component {
    _onClick = () => {
        document.location = "http://" + ewars.domain + "/user/" + this.props.data.attachments[0][1];
    };

    render() {
        return (
            <div className="meta-item" onClick={this._onClick}>
                <div className="meta-graphic">
                    <div className="meta-icon">
                        <i className="fal fa-user"></i>
                    </div>
                </div>
                <div className="meta-content">
                    <div className="meta-title">{this.props.data.name}</div>
                    <div className="meta-subtitle">{ewars.I18N(this.props.data.meta.org_name)}</div>
                    <div className="meta-additional">{_l(this.props.data.meta.role)}</div>
                </div>
            </div>
        )
    }
}

class FeedItem extends React.Component {
    render() {
        let content = ewars.I18N(this.props.data.content);

        let fromNow;
        let today = Moment.utc();
        if (isWithinAWeek(Moment.utc(this.props.data.created))) {
            fromNow = Moment.utc(this.props.data.created).from(today);
        } else {
            fromNow = Moment.utc(this.props.data.created).format("dddd, MMMM Do YYYY, h:mm a")
        }

        let meta;
        if (this.props.data.meta) {
            let view;
            if (this.props.data.meta.type == 'REPORT') view = <MetaReportItem data={this.props.data}/>;
            if (this.props.data.meta.type == "USER") view = <MetaItemNewUser data={this.props.data}/>;

            meta = (
                <div className="event-attachments">
                    {view}
                </div>
            )
        }

        return (
            <div className="event">
                <div className="event-content">
                    <div className="event-summary">
                        <div className="event-user"><a href={`/user/${this.props.data.triggered_by}`}
                                                       target="_blank">{this.props.data.name}</a></div>
                        &nbsp;{CONTENT_STRINGS[this.props.data.event_type]}
                        <div className="event-date">{fromNow}</div>
                    </div>
                    {this.props.data.content ?
                        <div className="event-text-extra">{content}</div>
                        : null}
                    {meta}
                </div>
            </div>
        )
    }
}

class Feed extends React.Component {
    _hasLoaded = false;

    static defaultProps = {
        user_id: null,
        noMax: false
    };

    constructor(props) {
        super(props);

        this.state = {
            feed: [],
            limit: 10,
            skip: 0,
            loadingItems: false
        }
    }

    componentDidMount() {
        this._getData(0, this.state.feed, this.props.user_id);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user_id) {
            if (nextProps.user_id != this.props.user_id) {
                this._getData(0, [], nextProps.user_id);
            }
        }
    }

    _getData = (skip, feed, user_id) => {
        if (user_id) {
            // Get a specific users list of activity items

            ewars.tx("com.ewars.profile.activity", [user_id, skip])
                .then((resp) => {
                    feed.push.apply(feed, resp.activity);
                    this._hasLoaded = true;
                    this.setState({
                        feed: feed,
                        skip: skip,
                        count: resp.count
                    })
                })
        } else {


            ewars.tx("com.ewars.activity", [skip])
                .then((resp) => {
                    feed.push.apply(feed, resp.activity);
                    this._hasLoaded = true;
                    this.setState({
                        feed: feed,
                        skip: skip,
                        count: resp.count
                    })
                })
        }
    };

    _loadMore = () => {
        this._getData(this.state.skip + 10, this.state.feed);
    };

    render() {
        if (!this._hasLoaded) {
            return <div className="placeholder">Loading...</div>
        }

        if (this._hasLoaded && this.state.feed.length <= 0) {
            return <div className="placeholder">No Activity</div>
        }

        let items = this.state.feed.map((item) => {
            return <FeedItem key={"FEED_" + item.uuid} data={item}/>
        });

        let style = {
            position: 'relative',
            width: "100%"
        };

        if (!this.props.noMax) {
            style.minHeight = "300px";
            style.maxHeight = "300px";
        } else {
            style.height = "100%";
            style.width = "100%";
            style.maxHeight = "none";
        }

        let feedInner = {
            position: 'absolute',
            display: 'block',
            top: 0,
            left: 0,
            width: '100%',
            minHeight: '100%'
        }

        return (
            <div className="feed-stream" style={style}>
                <div className="feed-inner" style={feedInner}>
                    {items}
                    <div className="feed-load-more" onClick={this._loadMore}>Load more</div>
                </div>
            </div>
        )
    }
}

export default Feed;
