import numeral from "numeral";

var CountryMetrics = React.createClass({
    getInitialState: function () {
        return {
            metrics: {}
        };
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.metrics.country", []).then(function (resp) {
            this.state.metrics = resp.d;
            if (this.isMounted()) this.forceUpdate();
        }.bind(this))
    },

    render: function () {

        var alerts;
        if (this.state.metrics.active_alerts > 0) {
            alerts = <i className="fal fa-exclamation-circle red"></i>;
        }

        var submitted_reports = numeral(this.state.metrics.submitted_reports).format("0,0");
        var total_users = numeral(this.state.metrics.total_users).format("0,0");

        return (
            <div className="widget">
                <div className="widget-header"><span>Country Metrics</span></div>
                <div className="body no-pad">
                    <table className="metrics-table">
                        <tr>
                            <th><i className="fal fa-share-square"></i></th>
                            <th>Submitted reports</th>
                            <td>{submitted_reports}</td>
                        </tr>
                        <tr>
                            <th><i className="fal fa-bell"></i></th>
                            <th>Active alerts</th>
                            <td>{this.state.metrics.active_alerts} {alerts}</td>
                        </tr>
                        <tr>
                            <th><i className="fal fa-ambulance"></i></th>
                            <th>Active responses</th>
                            <td>{this.state.metrics.responses}</td>
                        </tr>
                        <tr>
                            <th><i className="fal fa-users"></i></th>
                            <th>Total users</th>
                            <td>{total_users}</td>
                        </tr>
                        <tr>
                            <th><i className="fal fa-map-pin"></i></th>
                            <th>Reporting locations</th>
                            <td>{this.state.metrics.total_sites}</td>
                        </tr>
                        <tr>
                            <th><i className="fal fa-area-chart"></i></th>
                            <th>Analysis notebooks</th>
                            <td>{this.state.metrics.notebooks}</td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
});

export default CountryMetrics;
