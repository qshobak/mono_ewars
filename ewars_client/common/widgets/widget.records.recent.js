import Spinner from "../c.spinner";

var ReportItem = React.createClass({
    onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var reportName = ewars.formatters.I18N_FORMATTER(this.props.data.form_name);
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.location_name);
        var reportDate = ewars.formatters.DATE_FORMATTER(this.props.data.data_date, this.props.data.time_interval);
        var submittedDate = ewars.formatters.DATE_FORMATTER(this.props.data.submitted_date);

        return (
            <div className="iw-list-item" onClick={this.onClick}>
                <div className="title"><strong>{reportName}:</strong>&nbsp;{reportDate}&nbsp;{locationName}</div>
                <div className="content">Submitted: {submittedDate}&nbsp;by {this.props.data.user_name}</div>
            </div>
        )
    }
});

var RecentlySubmittedReports = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            offset: 0
        }
    },

    onReportClick: function (report) {
        document.location = "/reporting#?uuid=" + report.uuid;
    },

    componentWillMount: function () {
        this._load(this.state.offset);
    },

    _load: function (offset) {
        let blocker = new ewars.Blocker(this.refs.widget, "Loading...");
        ewars.tx("com.ewars.collections.recent", [offset])
            .then(function (resp) {
                blocker.destroy();
                this._hasLoaded = true;
                this.state.data = resp;
                this.state.offset = offset;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _next: function () {
        this._load(this.state.offset + 10);
    },

    _prev: function () {
        this._load(this.state.offset - 10);
    },


    render: function () {
        var items = _.map(this.state.data, function (item) {
            return <ReportItem key={item.uuid} data={item} onClick={this.onReportClick}/>
        }, this);

        var view;
        if (items.length <= 0 && this._hasLoaded) {
            view = <div className="placeholder">No reports have been submitted recently</div>
        } else if (!this._hasLoaded) {
            view = <Spinner/>;
        } else {
            view = (
                <div ref="widget">
                    <div className="iw-list">
                        {items}
                    </div>
                    <ewars.d.Toolbar>
                        <div className="btn-group pull-right">
                            {this.state.offset > 0 ?
                                <ewars.d.Button
                                    onClick={this._prev}
                                    icon="fa-caret-left"/>
                                : null}
                            <ewars.d.Button
                                onClick={this._next}
                                icon="fa-caret-right"/>
                        </div>
                    </ewars.d.Toolbar>
                </div>
            )
        }


        return view;
    }
});

export default RecentlySubmittedReports;
