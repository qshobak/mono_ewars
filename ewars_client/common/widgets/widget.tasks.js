import Form from "../c.form";
import Validate from "../../common/utils/Validator";
import Spinner from "../c.spinner";

import RedactionRequest from "../tasks/RedactionRequest.react";
import AmendmentRequest from "../tasks/AmendmentRequest.react";
import AssignmentRequest from "../tasks/AssignmentRequest.react";
import RegistrationRequest from "../tasks/RegistrationRequest.react";

var priorities = {
    "HIGH": <div className="priority high">High</div>,
    "MID": <div className="priority mid">Moderate</div>,
    "MODERATE": <div className="priority mid">Moderate</div>,
    "LOW": <div className="priority low">Low</div>
};

var TASK_DATA = {
    REDACTION_REQUEST: {
        icon: "fa-trash",
        Cmp: RedactionRequest,
        format: function (data) {
            var str = "<strong>Deletion Request:</strong> " + ewars.I18N(data.form_name) + " " + ewars.formatters.DATE_FORMATTER(data.report_date, data.form_interval) + " in " + ewars.I18N(data.location_name);
            return str;
        }
    },
    AMENDMENT_REQUEST: {
        icon: "fa-pencil",
        Cmp: AmendmentRequest,
        format: function (data) {
            var str = "<strong>Amendment Request:</strong> " + ewars.I18N(data.form_name) + " " + ewars.formatters.DATE_FORMATTER(data.report_date, data.form_interval) + " in " + ewars.I18N(data.location_name);
            return str;
        }
    },
    REGISTRATION_REQUEST: {
        icon: "fa-user-plus",
        Cmp: RegistrationRequest,
        format: function (data) {
            var str = "<strong>User Account Request:</strong> " + data.user_name + " [" + data.email + "]";
            return str;
        }
    },
    ASSIGNMENT_REQUEST: {
        icon: "fa-clipboard",
        Cmp: AssignmentRequest,
        format: function (data) {
            var str = "<strong>Assignment Request:</strong> " + ewars.I18N(data.form_name) + " - " + ewars.I18N(data.location_name) + " - " + data.user_name + " [" + data.user_email + "]";
            return str;
        }
    }
};

var TaskItem = React.createClass({
    getInitialState: function () {
        return {
            view: "tasks"
        };
    },

    onClick: function (e) {
        e.preventDefault();
        this.props.onClick(this.props.data);
    },

    render: function () {
        var label = TASK_DATA[this.props.data.task_type].format(this.props.data.data);

        var priority = priorities[this.props.data.priority];

        var icon = 'fal fa-thumb-tack';
        if (TASK_DATA[this.props.data.task_type]) icon = "fal " + TASK_DATA[this.props.data.task_type].icon;

        return (
            <div className="block hoverable" onClick={this.onClick}>
                <div className="block-content" style={{padding: 0}}>
                    <div className="ide-row">
                        <div className="ide-col" style={{padding: 8}}>
                            <div className="content" dangerouslySetInnerHTML={{__html: label}}></div>
                        </div>
                        <div className="ide-col" style={{maxWidth: 100, padding: 8}}>
                            {priority}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var TaskOption = React.createClass({
    getInitialState: function () {
        return {};
    },

    componentWillMount: function () {
        ewars.subscribe("RESET_BUTTONS", this._reset)
    },

    _reset: function () {
        this.refs.taskBtn.unsetProcessing();
    },

    _onClick: function () {
        this.refs.taskBtn.setProcessing("Processing...");

        this.props.onClick(this.props.data);
    },

    render: function () {

        var label = ewars.formatters.I18N_FORMATTER(this.props.data.label);
        return (
            <ewars.d.Button ref="taskBtn" onClick={this._onClick} label={label}/>
        )
    }
});

var TasksWidgetComponent = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            tasks: [],
            unclaimed: [],
            showTask: false,
            task: null,
            taskFormData: {},
            formErrors: null,
            view: "TASKS",
            taskButtonsEnabled: true
        }
    },

    componentWillMount: function () {
        this._getUserTasks();
        this._hasLoaded = false;
        ewars.subscribe("TASKS_CHANGED", this._reload);
    },

    _reload: function () {
        this._getUserTasks();
    },

    _getUserTasks: function () {
        var self = this;
        ewars.tx("com.ewars.tasks")
            .then(function (resp) {
                this.state.tasks = resp;
                this._hasLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _processTasks: function () {
        var tasks = [];
        var taskEls = [];
        var unclaimed = [];

        tasks.push.apply(tasks, this.state.tasks);
        tasks.push.apply(tasks, this.state.unclaimed);

        tasks = tasks.sort((a, b) => {
            if (a.priority == "HIGH") return 1;
            if (a.priority == "MID") return 0;
            if (a.priority == "LOW") return -1;
        });

        tasks.forEach(task => {
            taskEls.push(
                <TaskItem
                    data={task}
                    onClick={this._onTaskClick}/>
            )
        });

        return taskEls;
    },

    _onTaskClick: function (task) {
        this.state.task = task;
        this.forceUpdate();
    },

    _onTaskOptionSelect: function (option) {
        var self = this;
        var formData;

        this.setState({
            taskButtonsEnabled: false
        });

        if (option.validate_form) {
            if (Validate(this.state.task.form.fields, this.state.taskFormData)) {
                this.setState({
                    formErrors: Validate(this.state.task.form.fields, this.state.taskFormData),
                    taskButtonsEnabled: true
                });
                ewars.emit("RESET_BUTTONS");
                return;
            } else {
                formData = this.state.taskFormData;
            }
        }

        ewars.tx("com.ewars.task.action", [self.state.task.id, option.actions, formData])
            .then(function (resp) {
                self._hasLoaded = false;
                self.setState({
                    task: null,
                    showTask: null,
                    taskButtonsEnabled: true
                });
                ewars.emit("TASKS_CHANGED");
            }.bind(this))
    },

    _onTaskFormUpdate: function (data, prop, value) {
        var copy = this.state.taskFormData;
        copy[prop] = value;
        this.setState({
            taskFormData: copy
        })
    },

    _cancelTask: function () {
        this.setState({
            showTask: false,
            task: null,
            taskFormData: {},
            formErrors: null
        })
    },

    _closeTask: function () {
        this.state.task = null;
        this._getUserTasks();
    },

    render: function () {
        // Set active tab class
        var tasks = this._processTasks();

        var view;
        if (!this._hasLoaded) {
            view = <Spinner/>
        } else {
            view = <div className="overflow-wrap">
                {tasks.length > 0 ?
                    <div className="iw-list">
                        {tasks}
                    </div>
                    :
                    <p className="placeholder">You currently do not have any tasks available</p>
                }
            </div>;
        }

        var title = "Tasks (" + this.state.tasks.length + ")";

        var task;

        if (this.state.task) {
            var Cmp = TASK_DATA[this.state.task.task_type].Cmp;
            task = (
                <Cmp
                    onChange={this._onChange}
                    onClose={this._closeTask}
                    data={this.state.task}/>
            )
        }

        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">Tasks</div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {view}
                            </div>

                        </div>
                    </div>
                </div>
                {task}
            </div>
        )
    }
});

export default TasksWidgetComponent;
