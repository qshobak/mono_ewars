const CONSTANTS = {
    UPCOMING: "UPCOMING",
    OVERDUE: "OVERDUE"
};

var Tab = React.createClass({
    onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "panelsbar-btn";
        if (this.props.active) className += " panelsbar-btn-active";

        return (
            <div xmlns="http://www.w3.org/1999/xhtml" className={className} style={{padding: 0}} onClick={this.onClick}>
                <div className="ide-row">
                    <div className="ide-col"
                         style={{maxWidth: 20, flexBasis: 0, flexGrow: 1, padding: "8px 5px", textAlign: "center", borderTopLeftRadius: "3px"}}>
                        <i className="fal fa-map"></i></div>
                    <div className="ide-col" style={{wordBreak: 'keep-all', flex: "0 0 auto", padding: "8px"}}>
                        {this.props.label}
                    </div>
                </div>
            </div>
        )
    }
});

var ReportItem = React.createClass({
    _onClick: function () {
        document.location = "/reporting#/assignment/" + this.props.data.uuid + "?data_date=" + this.props.due_date + "&location_id=" + this.props.data.location_uuid;
    },

    render: function () {
        return (
            <div className="report-item" onClick={this._onClick}>
                <div className="flex-row">
                    <div className="flex-col" style={{maxWidth: 4}}>
                        <div className="report-color-bar"></div>
                    </div>
                    <div className="flex-col">
                        <div className="report-details">
                            <div className="form-title">{this.props.formName}</div>
                            <div className="location">{this.props.locationName}</div>
                            <div className="details">{this.props.date_string}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var UpcomingReports = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
            ewars.tx("com.ewars.user.upcoming", [])
                .then(function (resp) {
                    this.state.data = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
    },

    render: function () {

        var reports = _.map(this.state.data, function (report) {
            var formName = ewars.formatters.I18N_FORMATTER(report.form_name);
            var locationName = ewars.formatters.I18N_FORMATTER(report.location_name);

            var date_string = ewars.formatters.DATE_FORMATTER(report.due_date, report.reporting_interval);
            var exp_string = "Interval: " + INTERVALS[report.reporting_interval] + " " + date_string;

            return (
                <ReportItem
                    formName={formName}
                    locationName={locationName}
                    data={report}
                    due_date={report.due_date}
                    date_string={exp_string}/>
            )
        });

        if (this.state.data.length <= 0) {
            reports = <div className="placeholder">You currently do not have any upcoming reports.</div>;
        }

        return (
            <div className="report-list">
                {reports}
            </div>
        )
    }
});

var INTERVALS = {
    WEEK: "Weekly",
    DAY: "Daily",
    MONTH: "Monthly",
    YEAR: "Annual"
};

var OverdueReports = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
            ewars.tx("com.ewars.user.overdue", [])
                .then(function (resp) {
                    this.state.data = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
    },

    render: function () {

        var reports = [];
        _.each(this.state.data, function (report) {
            var formName = ewars.formatters.I18N_FORMATTER(report.form_name);
            var locationName = ewars.formatters.I18N_FORMATTER(report.location_name);

            _.each(report.due_dates, function (due_date) {
                var date_string = ewars.formatters.DATE_FORMATTER(due_date, report.reporting_interval);
                var exp_string = "Interval: " + INTERVALS[report.reporting_interval] + " " + date_string;
                reports.push(
                    <ReportItem
                        formName={formName}
                        locationName={locationName}
                        data={report}
                        due_date={due_date}
                        date_string={exp_string}/>
                )
            }, this);
        }, this);

        if (this.state.data.length <= 0) {
            reports = <div className="placeholder">No upcoming reports</div>;
        }

        return (
            <div className="report-list">
                {reports}
            </div>
        )
    }
});


var WorkloadWidget = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            view: CONSTANTS.UPCOMING
        }
    },

    _swapTab: function (data) {
        this.state.view = data.view;
        this.forceUpdate();
    },

    render: function () {
        var view;

        if (this.state.view == CONSTANTS.UPCOMING) view = <UpcomingReports/>;
        if (this.state.view == CONSTANTS.OVERDUE) view = <OverdueReports/>;

        return (
            <div className="widget">
                <div className="widget-header">
                    <span>Workload</span>
                </div>
                <div className="body no-pad">
                    <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs" style={{display: "block"}}>
                        <Tab
                            label="Upcoming"
                            onClick={this._swapTab}
                            data={{view: CONSTANTS.UPCOMING}}
                            active={this.state.view == CONSTANTS.UPCOMING}/>
                        <Tab
                            label="Overdue"
                            onClick={this._swapTab}
                            data={{view: CONSTANTS.OVERDUE}}
                            active={this.state.view == CONSTANTS.OVERDUE}/>
                    </div>
                    <div className="clearer"></div>
                    {view}
                </div>
            </div>
        )
    }
});

export default WorkloadWidget;
