import { html, css, LitElement } from "lit-element";

class DataGridRow extends LitElement {
    static get properties() {
        return {
            data: {type: Object},
            columns: {type: Array}
        }
    }

    static get styles() {
        return css`
            :host {
                display: table-row;
            }

            :host(:hover) {
                background: #000000;
            }
        `;
    }

    constructor() {
        super();
        this.data = {};
        this.columns = [];
    }

    render() {
        let cells = this.columns.map(col => {
            return html`
                <data-grid-cell .column="${col}" data="${this.data}"></data-grid-cell>
            `;
        });
        return html`
            ${cells}
        `;
    }
}

window.customElements.define("data-grid-row", DataGridRow);

class DataGridCell extends LitElement {
    static get styles() {
        return css`
            :host {
                display: table-cell;
                border-right: 1px solid rgba(0,0,0,0.1);
                border-bottom: 1px solid rgba(0,0,0,0.1);
            }
        `;
    }

    render() {
        return html`
        `;
    }
}

window.customElements.define("data-grid-cell", DataGridCell);

class DataGridRowActions extends LitElement {
    static get styles() {
        return css`
            :host {
                display: table-cell;
            }
        `;
    }

    render() {
        return html`
        `;
    }
}

window.customElements.define('data-grid-row-actions', DataGridRowActions);

class DataHeaderCell extends LitElement {
    static get properties() {
        return {
            data: {type: Object}
        }
    }

    static get styles() {
        return css`
            :host {
                display: table-cell;
                background: rgba(0,0,0,0.1);
                padding: 5px;
                color: #F2F2F2;
            }
        `;
    }

    constructor() {
        super();
        this.data = {};
    }

    render() {
        console.log(this.data);
        let label = this.data.label.en || this.data.label;
        return html`
            ${label}
        `;
    }
}

window.customElements.define("data-grid-header-cell", DataHeaderCell);

class DataGridButton extends LitElement {
    static get properties() {
        return {
            icon: {type: String},
            action: {type: String}
        }
    }

    constructor() {
        super();
        this.icon = "fa-cog";
        this.action = "UNKNOWN";
    }

    render() {
        let className = "fal " + this.icon;
        return html`
            <i class="${className}"></i>
        `;
    }
}
window.customElements.define("data-grid-button", DataGridButton);

class DataGrid extends LitElement {
    static get properties() {
        return {
            columns: {type: Object},
            resource: {type: String},
            rowActions: {type: Array},
            search: {type: Boolean},
            defaultSort: {type: Array},
            defaultFilter: {type: Object},
            data: {type: Array},
            limit: {type: Number},
            offset: {type: Number},
            searchParam: {type: String},
            title: {type: String}
        }
    }

    static get styles() {
        return css`
            :host {
                display: flex;
                flex: 2;
                flex-direction: column;
                box-sizing: border-box;
            }

            .dg-header {
                display: block;
                flex: 1;
                max-height: 30px;
                min-height: 30px;
                background: #313131;
                border-bottom: 1px solid #262626;
                border-top: 1px solid #434343;
            }

            .dg-records {
                flex: 2;
                display: block;
                overflow: auto;
                background: #3c3f41;
            }

            .dg-footer {
                display: flex;
                flex-direction: row;
                flex: 1;
                max-height: 30px;
                min-height: 30px;
                background: #313131;
                border-bottom: 1px solid #262626;
                border-top: 1px solid #434343;
            }

            table {
                width: 100%;
            }

            table > tr > td {

            }

            table > tr > th {
                background: #333333;
                color: #F2F2F2;
            }

            .dg-controls {
                float: right;
            }

            .dg-info {
                float: left;
            }
        `;
    }

    constructor() {
        super()
        this.title = "";
        this.data = [];
        this.limit = 100;
        this.offset = 0;
        this.searchParam = "";
    }

    connectedCallback() {
        super.connectedCallback();
        this._query();

    }

    firstUpdated() {
        this._query()
    }

    _onRecordAction = (e) => {

    };

    _query = () => {
        ewars.tx("com.ewars.query", ["form", null, {}, {}, this.limit, this.offset, []], {count: true})
            .then(res => {
                this.data = res.results;
                this.count = res.count;
            })
            .catch(err => {
                window.dispatchEvent(new CustomEvent("error", {detail: "ERROR_LOADING_RECORDS"}));
                console.log(err)
            });
    };

    updated(changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log(`${propName} changed, oldValue: ${oldValue}`);
        });
    }


    render() {
        console.log(this.data);

        let rows = this.data.map(item => {
            return html`
                <data-grid-row .columns="${this.columns}" .data="${item}"></data-grid-row>
            `;
        });

        console.log(this.columns);

        let headers = this.columns.map(item => {
            console.log(item);
            return html`
                <data-grid-header-cell .data="${item}"></data-header-cell>
            `;
        });

        return html`
            <div class="dg-header">
                <div class="dg-header-title">${this.title}</div>
            </div>
            <div class="dg-records">
                <table width="100%">
                    <thead>
                        <tr>
                            ${headers}
                        </tr>
                    </thead>
                    <tbody>
                        ${rows}
                    </tbody>
                </table>
            </div>
            <div class="dg-footer">
                <div class="dg-info">

                </div>

                <div class="dg-controls">
                    <data-grid-button/>
                    <data-grid-button/>
                    <data-grid-button/>
                </div>
            </div>
        `;
    }
}

window.customElements.define("data-grid", DataGrid);
export default DataGrid;
