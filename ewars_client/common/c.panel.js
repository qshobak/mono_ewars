class Panel extends React.Component {
    render() {
        let style = {};
        if (this.props.style) {
            for (let i in this.props.style) {
                style[i] = this.props.style[i];
            }
        }
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll" style={style}>
                {this.props.children}
            </div>
        )
    }
}

export default Panel;
