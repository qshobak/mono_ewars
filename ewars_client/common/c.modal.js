import Button from "./c.button";
import { SwitchField } from "./fields";

var ModalComponent = React.createClass({
    getDefaultProps: function () {
        return {
            visible: false,
            wizard: false,
            height: null,
            light: false,
            toggle: null,
            footer: true
        }
    },

    componentWillMount: function () {
    },

    getInitialState: function () {
        return {
            visible: false
        }
    },

    _onButtonAction: function (data) {
        this.props.onAction(data.action);
    },

    _processButtons: function () {
        var buttons = [];

        (this.props.buttons || []).forEach(button => {
            let key = ewars.utils.uuid();
            buttons.push(<ewars.d.Button
                key={key}
                data={button}
                enabled={button.enabled}
                colour={button.colour || null}
                icon={button.icon || null}
                onClick={button.onClick || this._onButtonAction}
                label={button.label}/>)
        });

        return buttons;
    },

    _toggle: function () {
        this.props.onToggle(this.props.toggleState ? false : true)
    },

    _processToggle: function () {
        return (
            <div className="modal-toggle-wrapper">
                <table>
                    <tr>
                        <td>
                            <div className="modal-toggle-label">{this.props.toggle.label}</div>
                        </td>
                        <td>
                            <div className="modal-switch-wrapper">
                                <SwitchField
                                    name="toggle"
                                    onUpdate={this._toggle}
                                    value={this.props.toggleState}/>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        )
    },

    render: function () {
        var modalClass = "modal-wrapper";
        if (this.props.visible) modalClass += " show";
        if (!this.props.visible) modalClass += " hide";
        if (this.props.wizard) modalClass += " wizard";

        var buttons = this._processButtons();

        var hasButtons = false;
        if (buttons.length > 0) hasButtons = true;

        var style = {};
        if (this.props.width) {
            style.width = this.props.width + "px";
        }

        var bodyStyle = {};
        if (this.props.height) {
            bodyStyle.height = this.props.height;
        }
        var icon;
        if (this.props.icon) icon = "fal " + this.props.icon;

        var toggle;
        if (this.props.toggle) toggle = this._processToggle();

        return (
            <div className={modalClass}>
                <div className="modal-content" style={style}>
                    <div className="ide-layout">
                        {this.props.title ?
                            <div className="ide-row" style={{display: "block"}}>
                                <div className="modal-title">
                                    <div>
                                        {icon ?
                                            <div className="modal-icon">
                                                <i className={icon}></i>
                                            </div>
                                            : null}
                                        <div className="modal-title-content">{this.props.title}</div>
                                    </div>
                                </div>
                            </div>
                            : null}

                        <div className="ide-row" style={{display: "block", overflowY: "auto"}}>
                            <div className="modal-body" style={bodyStyle}>
                                {this.props.children}
                            </div>
                        </div>
                        {this.props.footer ?
                            <div className="ide-row" style={{display: "block"}}>
                                <div className="modal-footer">
                                    {toggle}

                                    {hasButtons ?
                                        <div className="btn-group pull-right">
                                            {buttons}
                                        </div>
                                        : null}
                                </div>
                            </div>
                            : null}
                    </div>


                </div>
            </div>
        )
    }
});

export default ModalComponent;
