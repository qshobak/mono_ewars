class AnalysisWidget {
    constructor(el, definition, reportDate, location_id, template_id, is_public, index) {
        this.el = el;
        this.definition = definition;
        this.reportDate = reportDate;
        this.location_id = location_id;
        this.template_id = template_id;
        this.is_public = is_public;
        this.index = index;

        this._data = null;
        this._chart = null;

        this._id = ewars.utils.uuid();

        if (this.setup) this.setup();
        if (this.render) this.render();
    }

    get chart() {
        return this._chart;
    }

    redraw() {
        if (this._chart.redraw) this._chart.redraw();
    }

    getData() {
        return this._data;
    }

    query(query, callback, errCallback) {
        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                if (resp) {
                    callback(resp);
                } else {
                    errCallback();
                }
            })
    }
}

export default AnalysisWidget;