import Source from '../source';
import Moment from 'moment';
import aQueue from '../../api';

import CONTROLS from '../features';

import Base from '../base';

Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
})


const _getData = (data, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

const TYPE_MAP = {
    'DATE': 'datetime',
    'NUM': null,
    'PERC': null,
    'BOOl': 'category'
};

let isX = (val) => {
    return (val == 'true' || val == true);
};

const isNodeADate = (node) => {
    if (node._dt == 'DATE') return true;
    if (node._t == 'date') return true;
    if (node.n) {
        if (node.n.indexOf('DATE') >= 0) return truel
    }

    return false;
};

class HeatMap extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();
    }

    _createSeries = () => {

    };

    _renderLabel = (chart) => {
        console.log(chart);
    };

    render = () => {
        if (!this._config.n || this._config.n.length <= 0) {
            return;
        }
        let groups = this._config.g;
        let controls = this._config.n.filter(node => {
            return node.t == 'X';
        });

        this.options.chart.type = 'heatmap';
        this.options.chart.plotBorderWidth = 1;

        let showInNavigator = false;
        let addedControls = [];
        controls.forEach(control => {
            if (CONTROLS[control._t]) {
                if (control._t == 'NAVIGATOR') showInNavigator = true;
                let newControl = new CONTROLS[control._t](this.options, control)
                this.options = newControl.getOptions();
                addedControls.push(newControl);
            }
        });

        this.series = [];
        this._totalGroups = groups.length;
        this._loadedGroups = 0;

        let axisDict = {};
        let axis = this._config.n.filter(node => {
            return node.t == 'A';
        })
        axis.forEach(item => {
            axisDict[item._] = item;
        });


        this.options.colorAxis = {
            min: 0,
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        };

        this.options.legend = {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 25,
            symbolHeight: 280
        }


        let nodes = this._config.n.filter(node => {
            return ['M', 'D', 'C'].indexOf(node.t) >= 0;
        });

        aQueue.get(nodes, axis)
            .then(resp => {
                // Need to break up the data
                // get dimensions that are on the same axis as the measure
                let disaggs = this._config.n.filter(node => {
                    return node.x == 'DAGG';
                })

                let xItems = this._config.n.filter(node => {
                    return node.x == 'x';
                })

                let yItems = this._config.n.filter(node => {
                    return node.x == 'y';
                });

                let nMap = this._config.n;

                let values = [];

                let getXLabel, getYLabel;
                let xLabels  = {}, yLabels = {};

                let xAxis = {
                    type: 'category',
                    id: 'x',
                    title: null,
                    categories: [],
                    labels: {
                        formatter: function() {
                            return xLabels[this.value] || this.value;
                        }
                    }
                };

                let yAxis = {
                    type: 'category',
                    id: 'y',
                    title: null,
                    categories: [],
                    labels: {
                        formatter: function () {
                            return yLabels[this.value] || this.value;
                        }
                    }
                };

                let seriesData = [];

                if (disaggs.length <= 0) {
                    // no disaggregations here, we're plotting measures against a dimension
                    let x, y;
                    xItems.forEach(item => {
                        if (item.t == 'M') {
                            x = 'M';
                            xAxis.categories.push(item._);
                        } else {
                            x = 'D';
                            values.push.apply(values, resp.m[item._].values);
                            xAxis.categories.push.apply(xAxis.categories, resp.m[item._].values);
                        }
                    });

                    yItems.forEach(item => {
                        if (item.t == 'M') {
                            y = 'M';
                            yAxis.categories.push(item._);
                        } else {
                            y = 'D';
                            values.push.apply(values, resp.m[item._].values);
                            yAxis.categories.push.apply(yAxis.categories, resp.m[item._].values);
                        }
                    });

                    let self = this;
                    let series = [];
                    if (x == 'M') {
                        if (yItems[0].n == 'LOCATION') {
                            for (let i in resp.m[yItems[0]._].dict) {
                                yLabels[i] = resp.m[yItems[0]._].dict[i].name;
                            }
                        }

                        console.log(yItems, yLabels);

                        resp.d.forEach(node => {
                            xItems.forEach(meas => {
                                xLabels[meas._] = meas.label || meas.n;
                                let measIndex = resp.i[meas._]; // POsition of measure value in data
                                let measCatPos = xAxis.categories.indexOf(meas._); // position of category in axis
                                let dimIndex = resp.i[yItems[0]._]; // position of dim value in data
                                let dimCatPos = yAxis.categories.indexOf(node[dimIndex]);

                                series.push([
                                    measCatPos,
                                    dimCatPos,
                                    parseFloat(node[measIndex] || 0)
                                ])

                            })
                        })
                    } else if (x == 'D') {
                    }

                    console.log(series);

                    this.options.series = [{
                        name: 'Value',
                        data: series,
                        borderWidth: 1,
                        dataLabels: {
                            enabled: true,
                            color: '#000000'
                        }
                    }]
                }


                this.options.yAxis = [yAxis];
                this.options.xAxis = [xAxis];


                // let measures = nodes.filter(node => {
                //     return ['M', 'C'].indexOf(node.t) >= 0 && !node.p;
                // });
                //
                // let dimensions = nodes.filter(node => {
                //     return node.t == 'D';
                // });
                //
                // let groupings = [];
                // let dimAggs = {},
                //     hasDims = false;
                //
                // // There can only be one of these per group
                // let plotDim = dimensions.filter(dim => {
                //     return dim.x != 'AGG';
                // })[0];
                // let dimIndex = resp.i[plotDim._];
                //
                // measures.forEach(meas => {
                //     let result = {
                //         m: meas,
                //         mi: resp.i[meas._],
                //
                //         dims: [],
                //         props: []
                //     };
                //
                //     dimensions.forEach(node => {
                //         if (node.x == 'DAGG') {
                //             result.dims.push({
                //                 i: resp.i[dim._],
                //                 dim: node
                //             })
                //         }
                //
                //         if (['D', 'C', 'S'].indexOf(node._t) >= 0) {
                //             result.props.push({
                //                 i: resp.i[dim._],
                //                 dim: node
                //             })
                //         }
                //     });
                //     groupings.push(result);
                // });
                //
                //
                // groupings.forEach(sub_group => {
                //     if (sub_group.dims.length <= 0) {
                //         // no disaggregation, so we're just creating a single series
                //         let measIndex = sub_group.mi;
                //
                //
                //         let xId, yId, axisPlot = 'y';
                //         let mPrimAxis = axisDict[sub_group.m.x];
                //         if (isX(mPrimAxis)) {
                //             // plotting along the x a
                //             axisPlot = 'x';
                //             xId = sub_group.m.x;
                //             axis.forEach(node => {
                //                 if (!isX(node.x)) {
                //                     yId = node._;
                //                 }
                //             })
                //         } else {
                //             yId = sub_group.m.x;
                //             // need to find x
                //             axis.forEach(node => {
                //                 if (isX(node.x)) {
                //                     xId = node._;
                //                 }
                //             })
                //         }
                //
                //         let graph = {
                //             data: [],
                //             xAxis: xId,
                //             yAxis: yId,
                //             borderWidth: 1,
                //             dataLabels: {
                //                 enabled: true,
                //                 color: '#000000'
                //             }
                //
                //         };
                //         let subSet = resp.d.map(row => {
                //             return [row[dimIndex], parseFloat(row[measIndex])]
                //         })
                //         graph.data = subSet.sort((a, b) => {
                //             if (a[0] > b[0]) return 1;
                //             if (a[0] < b[0]) return -1;
                //             return 0;
                //         })
                //
                //         this.options.series.push(graph);
                //     }
                // })
                //
                //
                // this._loadedGroups++;
                // if (this._loadedGroups >= this._totalGroups) this._complete();
                this._complete();
            })
            .catch(err => {
                // Catch any errors on the client-side
                console.log(err);
            })

    };

    _complete = () => {
        console.log(this.options);
        this._chart = Highcharts.chart(this.options);
    };

    _recvGroupData = (group, data) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };

    exportImage = () => {

    };
}

export default HeatMap;
