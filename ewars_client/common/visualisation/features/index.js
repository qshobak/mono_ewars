import ControlNavigator from './navigator';
import ControlTitle from './title';
import ControlSubTitle from './subtitle'

export default {
    NAVIGATOR: ControlNavigator,
    TITLE: ControlTitle,
    SUBTITLE: ControlSubTitle
}

