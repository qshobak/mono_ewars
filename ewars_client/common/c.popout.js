class Popout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            data: null
        }
    }
    static defaultProps = {
        type: "USER"
    };

    render() {
        return (
            <div>
                <a href="#">{this.props.label}</a>
            </div>
        )
    }
}