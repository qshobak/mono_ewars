class LinkSelect extends React.Component {
    _onClick = (e) => {
        e.preventDefault();
        this.props.onClick();
    }

    render() {
        let view;
        if (this.props.icon && this.props.label) {
            view = (
                <a onClick={this._onClick}>
                    <i className={this.props.icon}></i>
                    <span>&nbsp;{this.props.label}</span>
                </a>
            )
        } else if (this.props.icon && !this.props.label) {
            view = (
                <a onClick={this._onClick}>
                    <i clasName={this.props.icon}></i>
                </a>
            )
        } else {
            view = (
                <a onClick={this._onClick}>{this.props.label}</a>
            )
        }
        return view;
    }
}


export default LinkSelect;
