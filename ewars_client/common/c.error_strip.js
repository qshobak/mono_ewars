class ErrorItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="error-item"></div>
        )
    }
}

class ErrorStrip extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let errs = [];

        for (var i in this.props.errors) {
            errs.push([i, this.props.errors[i]]);
        }


        return (
            <div className="error-strip">
                {errs.map(err => {
                    return <ErrorItem data={err}/>
                })}
            </div>
        )
    }
}

export default ErrorStrip;
