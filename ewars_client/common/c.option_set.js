import TextField from "./c.cmp.text";
import { Layout, Row, Cell } from "./layout";
import Panel from "./c.panel";
import Shade from "./c.shade";
import { ActionGroup } from "./c.action_button";
import Toolbar from "./c.toolbar";
import ButtonGroup from "./c.button_group";

const TYPE_SELECT = {
    options: [
        ["STATIC", "STATIC"],
        ["DYNAMIC", "DYNAMIC"]
    ]
};

const ACTIONS = [,
    ["fa-trash", 'CLEAR'],
    ["fa-plus", "ADD"]
];

const ACTIONS_OPTION = [
    ["fa-caret-up", "UP"],
    ["fa-caret-down", "DOWN"],
    ["fa-trash", "REMOVE"]
];

const CONFIG_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CLOSE"}
];

class OptionConfig extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Layout>
                <Row>
                    <Cell>
                        <Panel>
                        </Panel>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

class StaticOption extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showOptions: false
        }
    }

    _value = (prop, value) => {
        this.props.onChange(this.props.index, 0, value);
    };

    _display = (prop, value) => {
        this.props.onChange(this.props.index, 1, value);
    };

    _action = (action) => {
        if (action == "REMOVE") {
            this.props.onRemove(this.props.index);
        }

        if (action == "UP") {
            if (this.props.index > 0) {
                this.props.onUp(this.props.index);
            }
        }

        if (action == "DOWN") {
            this.props.onDown(this.props.index);
        }

        if (action == "CONFIG") {
            this.setState({
                showOptions: true
            })
        }

        if (action == "CLOSE") {
            this.setState({
                showOptions: false
            })
        }

    };

    _onConfigChange = (config) => {

    };

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <Row>
                        <Cell style={{flex: 2}}>
                            <TextField
                                config={{n: "VALUE"}}
                                onChange={this._value}
                                value={this.props.data[0]}/>
                        </Cell>
                        <Cell width="20px">
                        </Cell>
                        <Cell style={{flex: 2}}>
                            <TextField
                                data={{i18: this.props.i18, n: "LABEL"}}
                                onChange={this._display}
                                value={this.props.data[1]}/>
                        </Cell>
                        <Cell style={{display: "block"}} width="110px">
                            <ActionGroup
                                height="31px"
                                actions={ACTIONS_OPTION}
                                onAction={this._action}
                                right={true}/>
                        </Cell>
                    </Row>
                </div>
                <Shade
                    title="Options"
                    icon="fa-wrench"
                    actions={CONFIG_ACTIONS}
                    onAction={this._action}
                    shown={this.state.showOptions}>
                    {this.state.showOptions ?
                        <OptionConfig
                            onChange={this._onConfigChange}
                            data={this.props.data}/>
                        : null}

                </Shade>
            </div>
        )
    }
}

export default class OptionsSet extends React.Component {
    static defaultProps = {
        showTypes: true,
        title: "Options Editor",
        data: {
            i18: true
        },
        onChange: function () {
        }
    };

    constructor(props) {
        super(props);
    }

    _onFileChange = (e) => {
        console.log(e);
        console.log(e.target.files[0]);
        if (e.target.files[0]) {
            if (e.target.files[0].name.indexOf(".csv") < 0) {
                ewars.growl("Please upload CSV files only");
                return;
            }
        }
        let reader = new FileReader(e.target);

        reader.onloadend = function() {
            if (reader.error) {
                console.log(reader.error.message);
            }
        }

        reader.onload = () => {
            let dataURL = reader.result;

            let lines = dataURL.split("\n");
            let options = [];
            lines.forEach(item => {
                let nodes = item.split(',');
                options.push([nodes[0] || "", {en: nodes[1] || ""}]);
            });

            this.props.onChange(this.props.data.n, options);

        }

        let res = reader.readAsText(e.target.files[0]);
    };

    _action = (action, index) => {
        if (action == "CLEAR") {
            this.props.onChange(this.props.data.n, []);
            return;
        }

        if (action == "ADD") {
            let cp = ewars.copy(this.props.value || []);
            if (this.props.data.i18) {
                cp.push(["", {en: ""}])
            } else {
                cp.push(["", ""])
            }

            this.props.onChange(this.props.data.n, cp);
            return;
        }

        if (action == "UP") {
            let cp = ewars.copy(this.props.value || []);
            if (index > 0) {
                let tmp = cp[index - 1];
                cp[index - 1] = cp[index];
                cp[index] = tmp;
                this.props.onChange(this.props.data.n, cp);
            }
            return;
        }

        if (action == "DOWN") {
            let cp = ewars.copy(this.props.value || []);
            if (index < cp.length - 1) {
                let tmp = cp[index + 1];
                cp[index + 1] = cp[index];
                cp[index] = tmp;
                this.props.onChange(this.props.data.n, cp);
            }
            return;
        }

    };

    _onValueChange = (index, prop, value) => {
        let cp = ewars.copy(this.props.value || []);
        cp[index][prop] = value;

        this.props.onChange(this.props.data.n, cp);
    };

    _remove = (index) => {
        let cp = ewars.copy(this.props.value || []);
        cp.splice(index, 1);
        this.props.onChange(this.props.data.n, cp);

    };

    render() {
        let type_value = "STATIC";

        let options = (this.props.value || []).map((item, index) => {
            return <StaticOption
                onChange={this._onValueChange}
                index={index}
                i18={this.props.data.i18}
                onRemove={this._remove}
                onUp={(index) => {
                    this._action("UP", index)
                }}
                onDown={(index) => {
                    this._action("DOWN", index)
                }}
                data={item}/>;
        });

        return (
            <div>
                <Layout style={{border: "1px solid #262626"}}>
                    <Row height="30px">
                        <Cell
                            style={{
                                width: "130px",
                                padding: "6px",
                                maxWidth: "130px"
                            }}
                            borderRight={true}><label>Upload CSV Options</label></Cell>
                        <Cell style={{flex: 1, padding: "5px"}}>
                            <input
                                onChange={this._onFileChange}
                                style={{
                                    display: "block",
                                    width: "auto",
                                    flex: 1,
                                    height: "25px"
                                }}
                                type="file"/>
                        </Cell>
                    </Row>
                    <Toolbar label={this.props.title}>
                        <Row>
                            <Cell style={{display: "block"}}>
                                <ActionGroup
                                    right={true}
                                    onAction={this._action}
                                    actions={ACTIONS}/>

                            </Cell>
                        </Row>
                    </Toolbar>
                    <Row>
                        <Cell>
                            <div className="block-tree"
                                 style={{position: "relative", height: "auto", maxHeight: "300px", overflowY: "show"}}>
                                <div className="block">
                                    <div className="block-content">
                                        <Row>
                                            <Cell style={{flex: 2}}>Stored Value</Cell>
                                            <Cell width="20px">
                                            </Cell>
                                            <Cell style={{flex: 2}}>Display Value</Cell>
                                            <Cell style={{display: "block"}} width="110px">
                                            </Cell>
                                        </Row>
                                    </div>
                                </div>
                                {options}
                            </div>
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
}
