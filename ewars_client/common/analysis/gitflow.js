class Gitflow {
    constructor(options) {
        this.ref = options.ref;

        if ((typeof options.template === "string") || _isObject(options.template)) {
            this.template = this.newTemplate(options.template);
        } else if (options.template instanceof Template) {
            this.template = options.template;
        } else {
            this.template = this.newTemplate("metro");
        }

        // Orientations
        switch (options.orientation) {
            case "vertical-reverse":
                this.template.commit.spacingY *= -1;
                this.orientation = "vertical-reverse";
                this.template.branch.labelRotation = _isNullOrUndefined(options, "template.branch.labelRotation") ? 0 : options.template.branch.labelRotation;
                this.template.commit.tag.spacingY *= -1;
                break;
            case "horizontal":
                this.template.commit.message.display = false;
                this.template.commit.spacingX = this.template.commit.spacingY;
                this.template.branch.spacingY = this.template.branch.spacingX;
                this.template.commit.spacingY = 0;
                this.template.branch.spacingX = 0;
                this.orientation = "horizontal";
                this.template.branch.labelRotation = _isNullOrUndefined(options, "template.branch.labelRotation") ?
                    -90 : options.template.branch.labelRotation;
                this.template.commit.tag.spacingX = -this.template.commit.spacingX;
                this.template.commit.tag.spacingY = this.template.branch.spacingY;
                break;
            case "horizontal-reverse":
                this.template.commit.message.display = false;
                this.template.commit.spacingX = -this.template.commit.spacingY;
                this.template.branch.spacingY = this.template.branch.spacingX;
                this.template.commit.spacingY = 0;
                this.template.branch.spacingX = 0;
                this.orientation = "horizontal-reverse";
                this.template.branch.labelRotation = _isNullOrUndefined(options, "template.branch.labelRotation") ?
                    90 : options.template.branch.labelRotation;
                this.template.commit.tag.spacingX = -this.template.commit.spacingY;
                this.template.commit.tag.spacingY = this.template.branch.spacingY;
                break;
            default:
                this.orientation = "vertical";
                this.template.branch.labelRotation = _isNullOrUndefined(options, "template.branch.labelRotation") ?
                    0 : options.template.branch.labelRotation;
                break;
        }

        this.marginX = this.template.branch.spacingX + this.template.commit.dot.size * 2;
        this.marginY = this.template.branch.spacingY + this.template.commit.dot.size * 2;
        this.offsetX = 0;
        this.offsetY = 0;

        // Canvas init
        this.canvas = document.getElementById(this.elementId) || options.canvas;
        this.context = this.canvas.getContext("2d");
        this.context.textBaseline = "center";

        // Tooltip layer
        this.tooltip = document.createElement("div");
        this.tooltip.className = "gitgraph-tooltip";
        this.tooltip.style.position = "fixed";
        this.tooltip.style.display = "none";
        var tooltipContainer = options.tooltipContainer || document.body;
        tooltipContainer.appendChild(this.tooltip);

        // Navigation vars
        this.HEAD = null;
        this.branches = [];
        this.commits = [];

        // Utilities
        this.columnMax = 0; // nb of column for message position
        this.commitOffsetX = options.initCommitOffsetX || 0;
        this.commitOffsetY = options.initCommitOffsetY || 0;

        // Bindings
        this.mouseMoveOptions = {
            handleEvent: this.hover,
            gitgraph: this
        };
        this.canvas.addEventListener("mousemove", this.mouseMoveOptions, false);

        this.mouseDownOptions = {
            handleEvent: this.click,
            gitgraph: this
        };
        this.canvas.addEventListener("mousedown", this.mouseDownOptions, false);

        // Render on window resize
        window.onresize = this.render.bind(this);
    }

    dispose = () => {
        this.canvas.removeEventListener("mousemove", this.mouseMoveOptions, false);
        this.canvas.removeEventListener("mousedown", this.mouseDownOptions, false);
    }

    branch = (options) => {
        // Options
        if (typeof options === "string") {
            var name = options;
            options = {};
            options.name = name;
        }

        options = _isObject(options) ? options : {};
        options.parent = this;
        options.parentBranch = options.parentBranch || this.HEAD;

        // Add branch
        var branch = new Branch(options);
        this.branches.push(branch);

        // Return
        return branch;
    }

    orphanBranch = (options) => {
        // Options
        if (typeof options === "string") {
            var name = options;
            options = {};
            options.name = name;
        }

        options = _isObject(options) ? options : {};
        options.parent = this;

        // Add branch
        var branch = new Branch(options);
        this.branches.push(branch);

        // Return
        return branch;
    }

    commit = (options) => {
        this.HEAD.commit(options);

        // Return the main object so we can chain
        return this;
    }

    tag = (options) => {
        his.HEAD.tag(options);

        // Return the main object so we can chain
        return this;
    }

    newTemplate = (options) => {
        if (typeof options === "string") {
            return new Template().get(options);
        }
        return new Template(options);
    }

    render = () => {
        this.scalingFactor = _getScale(this.context);

        // Resize canvas
        var unscaledResolution = {
            x: Math.abs((this.columnMax + 1) * this.template.branch.spacingX) +
            Math.abs(this.commitOffsetX) +
            this.marginX * 2,
            y: Math.abs((this.columnMax + 1) * this.template.branch.spacingY) +
            Math.abs(this.commitOffsetY) +
            this.marginY * 2
        };

        if (this.template.commit.message.display) {
            unscaledResolution.x += 800;
        }

        unscaledResolution.x += this.template.commit.widthExtension;

        this.canvas.style.width = unscaledResolution.x + "px";
        this.canvas.style.height = unscaledResolution.y + "px";

        this.canvas.width = unscaledResolution.x * this.scalingFactor;
        this.canvas.height = unscaledResolution.y * this.scalingFactor;

        // Clear All
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // Add some margin
        this.context.translate(this.marginX, this.marginY);

        // Translate for inverse orientation
        if (this.template.commit.spacingY > 0) {
            this.context.translate(0, this.canvas.height - this.marginY * 2);
            this.offsetY = this.canvas.height - this.marginY * 2;
        }
        if (this.template.commit.spacingX > 0) {
            this.context.translate(this.canvas.width - this.marginX * 2, 0);
            this.offsetX = this.canvas.width - this.marginX * 2;
        }

        // Scale the context when every transformations have been made.
        this.context.scale(this.scalingFactor, this.scalingFactor);

        // Render branches
        for (var i = this.branches.length - 1, branch; !!(branch = this.branches[i]); i--) {
            branch.render();
        }

        // Render commits after to put them on the foreground
        for (var j = 0, commit; !!(commit = this.commits[j]); j++) {
            commit.render();
        }

        _emitEvent(this.canvas, "graph:render", {
            id: this.elementId
        });

    };

    applyCommits = (event, callbackFn) => {
        // Fallback onto layerX/layerY for older versions of Firefox.
        function getOffsetById(id) {
            var el = document.getElementById(id);
            var rect = el.getBoundingClientRect();

            return {
                top: rect.top + document.body.scrollTop,
                left: rect.left + document.body.scrollLeft
            };
        }

        var offsetX = event.offsetX || (event.pageX - getOffsetById(this.elementId).left);
        var offsetY = event.offsetY || (event.pageY - getOffsetById(this.elementId).top);

        for (var i = 0, commit; !!(commit = this.commits[i]); i++) {
            var distanceX = (commit.x + (this.offsetX + this.marginX) / this.scalingFactor - offsetX);
            var distanceY = (commit.y + (this.offsetY + this.marginY) / this.scalingFactor - offsetY);
            var distanceBetweenCommitCenterAndMouse = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));
            var isOverCommit = distanceBetweenCommitCenterAndMouse < this.template.commit.dot.size;

            callbackFn(commit, isOverCommit, event);
        }
    }
}