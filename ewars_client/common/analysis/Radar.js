var Moment = require("moment");
const d3 = require("d3");

var DateUtils = require("../../documents/utils/DateUtils");
var DataSource = require("../models/DataSource");
var AnalysisUtils = require("../utils/AnalysisUtils");
var defaults = {};



module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,

        getData: function () {
            return this._value;
        },

        render() {
            var width = 960,
                height = 700,
                radius = Math.min(width, height) / 2,
                color = d3.scaleOrdinal(d3.schemeCategory20c);

            var g = d3.select(this._el)
                .append('svg')
                .attr('width', width)
                .attr('height', height)
                .append('g')
                .attr('transform', 'translate(' + width/2 + ',' + height/2 + ')');

            var partition = d3.partition()
                .size([360, radius])
                .padding(0);

            var root = d3.hierarchy(data, function (d) {
                    return d.children
                })
                .sum(function (d) {
                    if (d.children) {
                        return 0
                    } else {
                        return 1
                    }
                });

            partition(root);

            var xScale = d3.scaleLinear()
                .domain([0, radius])
                .range([0, Math.PI * 2])
                .clamp(true);

            var arc = d3.arc()
                .startAngle(function(d) { return xScale(d.x0) })
                .endAngle(function(d) { return xScale(d.x1) })
                .innerRadius(function(d) { return d.y0 })
                .outerRadius(function(d) { return d.y1 });

            var path = g.selectAll('path')
                .data(root.descendants())
                .enter().append('path')
                .attr("display", function(d) { return d.depth ? null : "none"; })
                .attr("d", arc)
                .attr("fill-rule", "evenodd")
                .style('stroke', '#fff')
                .style("fill", function(d) { return color((d.children ? d : d.parent).data.name); })
        },

        destroy: function () {
            //this._chart.destroy();
            //this._chart = null;
        },

        updateDefinition(newDef) {
            this.init(newDef);
        },

        init: function () {
            this.render();
            return;
            this._el.removeClass("ewarschart");

            this._el.html('<i class="fal fa-spinner fa-spin"></i>');

            // Parse definition
            var date_range = DateUtils.processDateSpec(this._config, this._reportDate);
            var options = {start_date: date_range[0], end_date: date_range[1]};
            options.location = location_uuid;
            options.template_id = template_id;
            var query = AnalysisUtils.buildQuery(this._config, options);

            // Create data source
            var ds = new DataSource(query, isPublic);

            // Load data
            ds.load(function (source) {
                    this._el.replaceWith("<span>" + ewars.NUM(source.data, this._config.format || "0.0,00") + "</span>");
                    ewars.emit("WIDGET_LOADED");
                }.bind(this),
                function (errType, data) {
                    this._el.replaceWith('<i class="fal fa-exclamation-triangle red"></i>');
                    ewars.emit("WIDGET_LOADED");
                }.bind(this))

        }

    };

    instance.init();

    return instance;
};