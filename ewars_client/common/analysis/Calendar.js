import Moment from "moment";

import DateUtils from "../../documents/utils/DateUtils";
import DataSource from "../models/DataSource";
import AnalysisUtils from "../utils/AnalysisUtils";
var defaults = {};

var src_data = [
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000}
];


class Calendar {
    constructor(el, definition, reportDate, location_uuid, template_id, isPublic, idx) {
        this.definition = definition;
        this._el = el;
        this._reportDate = reportDate;
        this._locationUUID = location_uuid;
        this._tid = template_id;
        this._public = isPublic;
        this._idx = idx;

        this.render();
        return;
        this._el.removeClass("ewarschart");

        this._el.html('<i class="fal fa-spinner fa-spin"></i>');

        // Parse definition
        var date_range = DateUtils.processDateSpec(this._config, this._reportDate);
        var options = {start_date: date_range[0], end_date: date_range[1]};
        options.location = location_uuid;
        options.template_id = template_id;
        var query = AnalysisUtils.buildQuery(this._config, options);

        // Create data source
        var ds = new DataSource(query, isPublic);

        // Load data
        ds.load(function (source) {
                this._el.replaceWith("<span>" + ewars.NUM(source.data, this._config.format || "0.0,00") + "</span>");
                ewars.emit("WIDGET_LOADED");
            }.bind(this),
            function (errType, data) {
                this._el.replaceWith('<i class="fal fa-exclamation-triangle red"></i>');
                ewars.emit("WIDGET_LOADED");
            }.bind(this));

        this.query(definition);
    }

    query(definition) {
        this.render();
    }

    updateDefinition() {

    }

    render() {
        var width = 960,
            height = 136,
            cellSize = 17; // cell size

        var percent = d3.format(".1%"),
            format = d3.timeFormat("%Y-%m-%d");

        var color = d3.scaleQuantize()
            .domain([-.05, .05])
            .range(d3.range(11).map(function (d) {
                return "q" + d + "-11";
            }));

        var svg = d3.select(this._el).selectAll("svg")
            .data(d3.range(2013, 2017))
            .enter().append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("class", "RdYlGn")
            .append("g")
            .attr("transform", "translate(" + ((width - cellSize * 53) / 2) + "," + (height - cellSize * 7 - 1) + ")");

        svg.append("text")
            .attr("transform", "translate(-6," + cellSize * 3.5 + ")rotate(-90)")
            .style("text-anchor", "middle")
            .text(function (d) {
                return d;
            });

        var rect = svg.selectAll(".day")
            .data(function (d) {
                return d3.timeDays(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter().append("rect")
            .attr("class", "day")
            .attr("width", cellSize)
            .attr("height", cellSize)
            .attr("x", function (d) {
                return d3.timeWeek.count(d3.timeYear(d), d) * cellSize;
            })
            .attr("y", function (d) {
                return d.getDay() * cellSize;
            })
            .datum(format);

        rect.append("title")
            .text(function (d) {
                return d;
            });

        svg.selectAll(".month")
            .data(function (d) {
                return d3.timeMonths(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter().append("path")
            .attr("class", "month")
            .attr("d", monthPath);

        var data = d3.nest()
            .key(function (d) {
                return d.date;
            })
            .rollup(function (d) {
                return d.value;
            })
            .map(src_data);

        rect.filter(function (d) {
            return d in data;
        })
            .attr("class", function (d) {
                return "day " + color(data[d]);
            })
            .select("title")
            .text(function (d) {
                return d + ": " + percent(data[d]);
            });

        function monthPath(t0) {
            var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
                d0 = t0.getDay(), w0 = d3.timeWeek.count(d3.timeYear(t0), t0),
                d1 = t1.getDay(), w1 = d3.timeWeek.count(d3.timeYear(t1), t1);
            return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
                + "H" + w0 * cellSize + "V" + 7 * cellSize
                + "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
                + "H" + (w1 + 1) * cellSize + "V" + 0
                + "H" + (w0 + 1) * cellSize + "Z";
        }
    }
}

export default Calendar;
