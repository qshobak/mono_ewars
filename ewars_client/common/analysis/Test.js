var Moment = require("moment");
const d3 = require("d3");

var DateUtils = require("../../documents/utils/DateUtils");
var DataSource = require("../models/DataSource");
var AnalysisUtils = require("../utils/AnalysisUtils");
var defaults = {};


module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,
        _series: {},

        getData: function () {
            return this._value;
        },

        render(dt) {

        },

        destroy: function () {
            //this._chart.destroy();
            //this._chart = null;
        },

        updateDefinition(newDef) {
            this.init(newDef);
        },

        init: function () {

            var arc = d3.arc()
                .innerRadius(0)
                .outerRadius(100)
                .startAngle(0)
                .endAngle(Math.PI / 2);

            arc();

        }
    };

    instance.init();

    return instance;
};