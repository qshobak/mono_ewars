const d3 = require("d3");

var DateUtils = require("../../documents/utils/DateUtils");
var AnalysisUtils = require("../utils/AnalysisUtils");

const required = [
    "location",
    "indicator",
    "start_date",
    "end_date",
    "reduction"
];

const FILTERS = {
    "EQ": "==",
    "GT": ">",
    "GTE": ">=",
    "LT": "<",
    "LTE": "<="
};

function isOk(geom) {
    if (geom == "null") return false;
    if (geom == null) return false;
    if (geom == undefined) return false;

    let tmp = geom;
    if (typeof geom == 'string') tmp = JSON.parse(geom);

    if (tmp.type == "Point") {
        if (tmp.coordinates == [0.0]) return false;
    }

    if (tmp.type == "FeatureCollection") {
        if (!tmp.features) return false;
        if (tmp.features.length <= 0) return false;
    }

    return true;
}

const labels = {
    GT: ">",
    GTE: ">=",
    LT: "<",
    LTE: "<=",
    EQ: "=",
    NEQ: "!="
};

module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic, idx) {
    var instance = {
        _el: el,
        _map: null,
        _isPanned: false,
        _bbox: null,
        _config: definition,
        _layerIds: [],

        getData: function () {
            return this._value;
        },

        render(callback) {
            // this.buildLegend();
        },

        destroy: function () {
        },

        updateDefinition: function (newDef) {
            this._bl = new ewars.Blocker(this._el, "Loading...");
            this._config = newDef;

            let isValid = true;

            required.forEach(function (item) {
                if (!newDef[item]) isValid = false;
            });

            if (!isValid) return;

            this.query(newDef);
        },

        buildLegend: function () {
            if (this._legendId) {
                let original = document.getElementById(this._legendId);
                original.parentNode.removeChild(original);
            }
            var container = document.createElement("div");
            container.setAttribute("class", "map-legend");
            this._legendId = ewars.utils.uuid();
            container.setAttribute("id", this._legendId);

            let scales = this._config.thresholds || this._config.scales;
            scales.forEach(item => {
                let scaleEl = document.createElement("div");
                let dot = document.createElement("i");
                dot.setAttribute("class", "fal fa-circle");
                dot.style.color = item[2];
                let labelText = document.createTextNode(` ${labels[item[0]]} ${item[1]}`);

                scaleEl.appendChild(dot);
                scaleEl.appendChild(labelText);

                container.appendChild(scaleEl);
            });

            this._el.appendChild(container);
        },

        updateData: function (data) {

            let w = 960;
            let h = 500;
            let proj = d3.geoMercator();
            let path = d3.geoPath().projection(proj);
            let t = proj.translate();
            let s = proj.scale();

            let map = d3.select(this._el)
                .append("svg:svg")
                .attr("width", w)
                .attr("height", h)
                .call(d3.zoom().on("zoom", function() {
                    let transform = d3.event.transform;

                    map.attr("transform", transform);

                }));

            var axes = map.append("svg:g").attr("id", "axes");

            let xAxis = axes.append("svg:line")
                .attr("x1", t[0])
                .attr("y1", 0)
                .attr("x2", w)
                .attr("y2", h);

            let yAxis = axes.append("svg:line")
                .attr("x1", 0)
                .attr("y1", t[1])
                .attr("x1", w)
                .attr("y2", t[1]);

            let uk = map.append("svg:g")
                .attr("id", "uk")
                .attr("class", ".country");

            let geoJson = JSON.parse(data.location.geometry);

            uk.selectAll("path")
                .data(geoJson.features)
                .enter()
                .append("svg:path")
                .attr('d', path);


            if (this._bl) this._bl.destroy();
            ewars.emit("WIDGET_LOADED");
        },

        query: function (config) {
            if (idx) {
                if (ewars.g[idx]) {
                    this.updateData(ewars.g[idx]);
                    return;
                }
            }

            var date_range = DateUtils.processDateSpec(config, this._report_date);

            let query = {
                type: config.source_type || "SLICE",
                reduction: "SUM",
                interval: "DAY",
                indicator: config.indicator,
                start_date: date_range[0],
                end_date: date_range[1],
                location: null,
                geometry: true,
                series: config.series || null,
                formula: config.formula || null,
                centroid: true
            };

            if (config.loc_spec) {

                if (config.loc_spec == "SPECIFIC") {
                    query.location = config.location;
                } else if (config.loc_spec == "REPORT_LOCATION") {
                    query.location = location_uuid;
                } else if (config.loc_spec == "GENERATOR") {
                    query.location = {
                        parent_id: config.location,
                        site_type_id: config.site_type_id,
                        status: config.location_status || "ACTIVE"
                    }
                }
            } else {
                query.location = {
                    parent_id: config.location,
                    site_type_id: config.site_type_id,
                    status: config.location_status || "ACTIVE"
                }
            }

            ewars._queue.push("/arc/analysis", query)
                .then(resp => {
                    if (idx) ewars.g[idx] = resp;
                    this.updateData(resp);
                })
                .catch(err => {
                    this._emitLoaded = true;
                    ewars.emit('WIDGET_LOADED')
                })

        },

        init: function () {
            this._bl = new ewars.Blocker(this._el, "Loading...");
            if (this._config) {
                this._el.style.height = this._config.height || "100%";
                this._el.style.width = this._config.width || "100%";
            }

            this.query(this._config)

        }

    };

    instance.init();

    return instance;
}
;