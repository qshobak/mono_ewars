var Moment = require("moment");
var DateUtils = require("../../documents/utils/DateUtils");

var DataSource = require("../../common/models/DataSource");

var defaults = {};

var COLOURS = [
    "orange", "red", "purple", "pink", "yellow", "green", "magenta", "grey", "black", "aque", "blue", "chocolate", "cyan"
];

var DEFAULTS = require("../components/widget_editors/series/defaults_series");

module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _report_date: reportDate,
        _location_uuid: location_uuid,
        _id: null,
        _el: null,
        _chart: null,
        _validSeries: [],
        _definition: {},
        _data: {},


        renderChart: function (props) {
            // remove graphs
            if (this._validSeries.length <= 0) {
                this._chart.redraw();
                return;
            }

            while (this._chart.series.length > 0)
                this._chart.series[0].remove(true);

            var config = _.extend({}, defaults, this._definition);
            var colourCount = 0;
            _.each(this._data, function (dataSeries, index) {
                var dataSource = _.extend(_.extend({}, DEFAULTS), dataSeries);
                var graph = {};

                graph.data = _.sortBy(dataSeries.data, function (node) {
                    return node[0];
                }, this);

                graph.data = _.map(graph.data, function (node) {
                    return [Moment.utc(node[0], "YYYY-MM-DD").valueOf(), parseFloat(node[1])];
                });

                var series = this._validSeries[index];

                if (series.colour && series.colour != "") {
                    graph.color = series.colour;
                } else {
                    graph.color = COLOURS[colourCount];
                    colourCount++;
                }

                graph.type = series.type;
                if (series.type == "area") {
                    graph.fillOpacity = 0.6;
                }

                if (series.type == "bar") {
                    graph.type = "column";
                    graph.fillOpacity = 0.6;
                }

                if (graph.type == "line") {
                    graph.marker = {
                        enabled: true
                    };
                    graph.lineWidth = 1;
                    graph.stickyTracking = true;
                }

                if (series.title && series.title != "") {
                    graph.name = "<strong>" + ewars.formatters.I18N_FORMATTER(series.title) + "</strong>";
                } else {
                    graph.name = "<strong>" + ewars.formatters.I18N_FORMATTER(dataSeries.indicator.name) + "</strong><i> " + ewars.formatters.I18N_FORMATTER(dataSeries.location.name) + "</i>";
                }

                this._chart.addSeries(graph);
            }, this);

            this._chart.hideLoading();
            this._chart.redraw();
        },

        _setDataSeries: function (index, data) {
            this._data[index] = data;
        },

        getData: function () {
            return _.map(this._data, function (dp) {
                return dp;
            }, this)
        },

        init: function () {
            this._id = _.uniqueId("SERIES_");
            this._el = el;

            if (this._chart) {
                this._chart.destroy();
                this._chart = null;
            }

            var config = JSON.parse(definition);
            this._definition = config;

            var title = false;
            if (config.show_title) {
                if (config.title) {
                    title = ewars.formatters.I18N_FORMATTER(config.title);
                }
            }

            // Map in options
            var options = {
                chart: {
                    renderTo: el,
                    animation: true,
                    height: 300,
                    backgroundColor: 'rgba(255,255,255,0)'
                },
                credits: {enabled: false},
                title: {
                    text: title,
                    align: 'center'
                },
                noData: "",
                exporting: {
                    enabled: config.tools
                },
                navigator: {
                    enabled: config.navigator
                },
                legend: {
                    enabled: config.show_legend
                },
                yAxis: {
                    labels: {
                        formatter: function () {
                            return ewars.NUM(this.value, config.number_format)
                        }
                    },
                    min: 0,
                    alternateBandColor: "#F2F2F2"
                },
                xAxis: {
                    type: "datetime",
                    units: [['week', [1]]],
                    labels: {
                        autoRotation: [-45],
                        rotation: -90,
                        formatter: function () {
                            return Moment(this.value).format("[W]WW YYYY")
                        }
                    }
                }
            };

            if (config.zoom) options.chart.zoomType = "x";

            if (!this._chart) {
                this._chart = new Highcharts.Chart(options);
            }

            this._validSeries = [];
            this._data = {};

            var options = {};
            var date_range = DateUtils.processDateSpec(config, this._report_date);
            options.start_date = date_range[0];
            options.end_date = date_range[1];

            this._series = [];
            _.each(config.series, function (series, index) {
                var ds = new DataSource(series, isPublic);
                ds.start_date = options.start_date;
                ds.end_date = options.end_date;

                if (series.loc_spec && series.loc_spec == "REPORT_LOCATION") {
                    ds.location = location_uuid;
                }

                ds.interval = config.timeInterval || series.interval;
                ds._templateId = template_id;

                if (ds.isValid()) this._series.push(ds);
            }, this);


            this._seriesLoaded = 0;
            _.each(this._series, function (sourceResult) {
                sourceResult.load(function (source) {
                    source.render(this._chart);
                    this._seriesLoaded++;

                    if (this._seriesLoaded >= this._series.length) ewars.emit("WIDGET_LOADED");
                }.bind(this))
            }, this);


            //_.each(this._validSeries, function (series, index) {
            //
            //    var data;
            //    if (!series.dataType || series.dataType == "SERIES") {
            //        data = {
            //            dataType: "SERIES",
            //            target_interval: config.timeInterval || series.interval,
            //            target_location: series.locationUUID,
            //            target_indicator: series.indicatorUUID,
            //            options: series.options
            //        }
            //    } else if (series.dataType == "AGG") {
            //        data = series;
            //    } else if (series.dataType == "COMPLEX") {
            //        data = series;
            //    }
            //
            //    var uri = "http://" + ewars.domain + "/api/Analysis()";
            //    if (isPublic) {
            //        uri = "http://" + ewars.domain + "/api/dms/Analysis()";
            //        data.template_id = template_id;
            //    }
            //
            //    $.ajax({
            //        url: uri,
            //        contentType: "application/json",
            //        dataType: "json",
            //        type: "POST",
            //        context: this,
            //        data: JSON.stringify(data),
            //        success: function (resp) {
            //            this._setDataSeries(index, resp);
            //            loaded++;
            //            if (loaded == toBeLoaded) this.renderChart(config);
            //        }
            //    })
            //}, this);
        },

        _getData: function () {

        }
    };

    instance.init();

    return instance;
};