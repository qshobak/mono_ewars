import Moment from "moment";
import RangeUtils from "../utils/RangeUtils";

import DateUtils from "../../documents/utils/DateUtils";
import DataSource from "../models/DataSource";
import AnalysisUtils from "../utils/AnalysisUtils";
var defaults = {};


const RawValue = function (el, definition, reportDate, location_uuid, isPublic, idx) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,
        _data: null,
        _emitLoaded: false,

        getData: function () {
            return ["RAW", this._data];
        },

        updateDefinition: function (config) {
            this._config = config;

            this.init();
        },

        _renderDocument: function (value) {
            let valMap;
            if (this._config.o_val_mapping) valMap = this._config.value_mapping;

            let rawVal = value.data;
            let displayVal = ewars.NUM(value.data, this._config.format || "0,0.00");

            // Get value mapping
            if (valMap) {
                let res;
                valMap.forEach((thresh) => {
                    if (thresh[1] == "INF") {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                    } else {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                            res = thresh;
                        }
                    }
                });

                if (res) {
                    if (res[2].indexOf("{value}") >= 0) {
                        displayVal = res[2].replace("{value}", displayVal)
                    } else {
                        displayVal = res[2];
                    }
                }
            }

            let txtNode = document.createTextNode(displayVal);
            this._el.innerHTML = "";
            this._el.appendChild(txtNode);
        },

        _render: function (value) {
            if (ewars.IS_DOCUMENT) {
                // if this is a document,
                this._renderDocument(value);
                return;
            }

            let text = '<span class="raw-value">';

            // get font size
            let fontSize = "30px";
            if (ewars.isSet(this._config.font_size)) fontSize = this._config.font_size;
            if (fontSize.indexOf("px") < 0) fontSize += "px";

            let prefix = "";
            if (ewars.isSet(this._config.prefix)) prefix = this._config.prefix;
            let suffix = "";
            if (ewars.isSet(this._config.suffix)) suffix = this._config.suffix;

            let colMap;
            if (this._config.o_val_colour) colMap = this._config.value_colouring;
            let valMap;
            if (this._config.o_val_mapping) valMap = this._config.value_mapping;

            let rawVal = value.data;
            let displayVal = ewars.NUM(value.data, this._config.format || "0,0.00");

            // Get value mapping
            if (valMap) {
                let res;
                valMap.forEach((thresh) => {
                    if (thresh[1] == "INF") {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                    } else {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                            res = thresh;
                        }
                    }
                });

                if (res) {
                    if (res[2].indexOf("{value}") >= 0) {
                        displayVal = res[2].replace("{value}", displayVal)
                    } else {
                        displayVal = res[2];
                    }
                }
            }

            // Get colour mapping
            let displayColour = "#333";
            if (colMap) {
                let res;
                colMap.forEach((thresh) => {
                    if (thresh[1] == "INF") {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                    } else {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                            res = thresh;
                        }
                    }
                });

                if (res) {
                    displayColour = res[2];
                }
            }

            let ixSize = "12px";

            let template = `
                <span class="raw-value" style="font-size: ${fontSize};">
                    <span class="raw-prefix" style="font-size: ${ixSize};">${prefix}</span>
                    <span class="raw-inner" style="color: ${displayColour};">${displayVal}</span>
                    <span class="raw-suffix" style="font-size: ${ixSize};">${suffix}</span>
                </span>
            `;

            this._el.innerHTML = template;

        },

        init: function () {
            this._el.setAttribute("class", this._el.className.replace("ewarschart", ""));

            this._el.innerHTML = '<i class="fal fa-spin fa-circle-o-notch raw-error"></i>';

            let config = this._config;

            // Parse definition
            let options = {};
            if (config.period) {
                options.end_date = RangeUtils.process(config.period[1], this._reportDate);
                options.start_date = RangeUtils.process(config.period[0], this._reportDate, options.end_date);
            } else {
                let dateRange = DateUtils.processDateSpec(this._config, this._reportDate);
                options.start_date = dateRange[0];
                options.end_date = dateRange[1];
            }
            options.location = location_uuid;

            if (idx) {
                if (ewars.g[idx]) {
                    this._el.innertHTML = `<span>${ewars.NUM(ewars.g[idx].data, config.format || "0,0.00")}</span>`;
                    return;
                }
            }

            if (config.source_type == "SLICE") {
                let query = {
                    start_date: options.start_date,
                    end_date: options.end_date,
                    reduction: config.reduction || "SUM",
                    indicator: config.indicator,
                    interval: config.interval || "DAY",
                    location: null,
                    type: "SLICE",
                    meta: false
                };

                if (config.loc_spec == "SPECIFIC") query.location = config.location;
                if (config.loc_spec == "REPORT_LOCATION") query.location = location_uuid;
                if (config.loc_spec == "GROUP") query.location = {
                    groups: config.group_ids,
                    agg: "AGGREGATE"
                };

                // Something wrong with the update on these widgets
                if (!query.location && config.series) {
                    if (config.series.length > 0) {
                        query.location = config.series[0].location || null;
                    }
                }
                ewars._queue.push("/arc/analysis", query)
                    .then(resp => {
                        if (idx) ewars.g[idx] = resp;
                        if (resp) {
                            this._data = [config, resp];
                            this._render(resp);
                            if (!this._emitLoaded) {
                                this._emitLoaded = true;
                                ewars.emit("WIDGET_LOADED");
                            }
                        } else {
                            this._el.innerHTML = '<i class="fal fa-exclamation-triangle red"></i>';
                            if (!this._emitLoaded) {
                                this._emitLoaded = true;
                                ewars.emit("WIDGET_LOADED");
                            }
                        }
                    })
                    .catch((err) => {
                        this._el.innerHTML = '<i class="fal fa-exclamation-triangle red"></i>';
                        if (!this._emitLoaded) {
                            this._emitLoaded = true;
                            ewars.emit("WIDGET_LOADED");
                        }
                    })
            } else {
                let query = {
                    start_date: options.start_date,
                    end_date: options.end_date,
                    reduction: config.reduction || "SUM",
                    interval: config.interval || "DAY",
                    location: null,
                    formula: config.formula,
                    type: "SLICE_COMPLEX",
                    series: config.series,
                    meta: false
                };

                if (config.loc_spec == "SPECIFIC") query.location = config.location;
                if (config.loc_spec == "REPORT_LOCATION") query.location = location_uuid;
                if (config.loc_spec == "GROUP") query.location = {
                    groups: config.group_ids,
                    agg: "AGGREGATE"
                };

                // Something wrong with the update on these widets
                if (!query.location && config.series) {
                    if (config.series.length > 0) {
                        query.location = config.series[0].location || null;
                    }
                }
                ewars._queue.push("/arc/analysis", query)
                    .then(resp => {
                        if (idx) ewars.g[idx] = resp;
                        if (resp) {
                            this._data = [config, resp];
                            this._render(resp);
                            if (!this._emitLoaded) {
                                this._emitLoaded = true;
                                ewars.emit("WIDGET_LOADED");
                            }
                        } else {
                            this._el.innertHTML = '<i class="raw-error fal fa-exclamation-triangle red"></i>';
                            if (!this._emitLoaded) {
                                this._emitLoaded = true;
                                ewars.emit("WIDGET_LOADED");
                            }
                        }
                    })
                    .catch((err) => {
                        this._el.innerHTML = '<i class="raw-error fal fa-exclamation-triangle red"></i>';
                        if (!this._emitLoaded) {
                            this._emitLoaded = true;
                            ewars.emit("WIDGET_LOADED");
                        }
                    })


            }

        }

    };

    instance.init();

    return instance;
};

export default RawValue;
