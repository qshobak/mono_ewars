var Moment = require("moment");
const mapboxgl = require("mapbox-gl");

var DateUtils = require("../../documents/utils/DateUtils");
var DataSource = require("../models/DataSource");
var AnalysisUtils = require("../utils/AnalysisUtils");

const REQUIRED = [
    'indicator',
    'start_date',
    'end_date',
    'location',
    'site_type_id',
    'reduction'
];


const required = [
    "location",
    "indicator",
    "start_date",
    "end_date",
    "reduction"
];

const FILTERS = {
    "EQ": "==",
    "GT": ">",
    "GTE": ">=",
    "LT": "<",
    "LTE": "<="
};

function isOk(geom) {
    if (geom == "null") return false;
    if (geom == null) return false;
    if (geom == undefined) return false;

    let tmp = geom;
    if (typeof geom == 'string') tmp = JSON.parse(geom);

    if (tmp.type == "Point") {
        if (tmp.coordinates == [0.0]) return false;
    }

    if (tmp.type == "FeatureCollection") {
        if (!tmp.features) return false;
        if (tmp.features.length <= 0) return false;
    }

    return true;
}

const labels = {
    GT: ">",
    GTE: ">=",
    LT: "<",
    LTE: "<=",
    EQ: "=",
    NEQ: "!="
};

module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic, idx) {
    var instance = {
        _el: el,
        _map: null,
        _isPanned: false,
        _bbox: null,
        _config: definition,
        _layerIds: [],

        getData: function () {
            return this._value;
        },

        render(callback) {
            console.log("HERE")
            mapboxgl.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
            this.map = new mapboxgl.Map({
                container: this._el,
                style: 'mapbox://styles/jduren/cim4smtop0114ccm3nf2wz0ya',
                center: [0, 0],
                attributionControl: false
            });

            this.map.on("load", function () {
                this.map.addSource("data", {
                    type: "geojson",
                    data: {
                        type: "FeatureCollection",
                        features: []
                    }
                });

                this.map.addSource("markers", {
                    type: "geojson",
                    data: {
                        type: "FeatureCollection",
                        features: []
                    }
                });

                if (callback) callback();

                this.buildLegend();

            }.bind(this))

        },

        destroy: function () {
        },

        updateDefinition: function (newDef) {
            this._bl = new ewars.Blocker(this._el, "Loading...");
            this._config = newDef;

            this.query(newDef);
        },

        buildLegend: function () {
            if (this._legendId) {
                let original = document.getElementById(this._legendId);
                original.parentNode.removeChild(original);
            }
            var container = document.createElement("div");
            container.setAttribute("class", "map-legend");
            this._legendId = ewars.utils.uuid();
            container.setAttribute("id", this._legendId);

            let scales = this._config.thresholds || this._config.scales;
            scales.forEach(item => {
                let scaleEl = document.createElement("div");
                let dot = document.createElement("i");
                dot.setAttribute("class", "fal fa-circle");
                dot.style.color = item[2];

                let labelText;
                if (["", NaN, null, undefined].indexOf(item[1]) >= 0) {
                    labelText = document.createTextNode(` ${item[0] || 0}+`);
                } else {
                    labelText = document.createTextNode(` ${item[0] || 0} to ${item[1]}`);
                }

                scaleEl.appendChild(dot);
                scaleEl.appendChild(labelText);

                container.appendChild(scaleEl);
            });

            this._el.appendChild(container);
        },

        updateData: function (data) {
            console.log(data);

            let geoms = {
                type: "FeatureCollection",
                features: []
            };

            let markers = {
                type: "FeatureCollection",
                features: []
            };

            if (data) {

                data.forEach(dataSet => {
                    if (dataSet.location.geometry_type == "ADMIN") {
                        let geo = JSON.parse(dataSet.location.geometry);

                        if (isOk(geo)) {
                            geoms.features.push({
                                type: "Feature",
                                geometry: geo.features[0].geometry || null,
                                properties: {
                                    value: dataSet.data,
                                    value_fmt: ewars.NUM(dataSet.data, this._config.format || "0"),
                                    indicator: dataSet.indicator,
                                    location: dataSet.location,
                                    location_name: ewars.I18N(dataSet.location.name)
                                }
                            })
                        }
                    } else {
                        let point = JSON.parse(dataSet.location.geometry);

                        if (isOk(point)) {
                            markers.features.push({
                                type: "Feature",
                                geometry: JSON.parse(point),
                                properties: {
                                    geometry_type: "POINT",
                                    value: dataSet.data,
                                    value_fmt: ewars.NUM(dataSet.data, this._config.format || "0" || "0"),
                                    indicator: dataSet.indicator,
                                    location: dataSet.location,
                                    location_name: ewars.I18N(dataSet.location.name)
                                }
                            })
                        }
                    }
                })
            }

            let joined = {
                type: "FeatureCollection",
                features: []
            };
            joined.features = geoms.features;
            joined.features.push.apply(joined.features, markers.features);


            this.map.getSource("data").setData(geoms);
            this.map.getSource("markers").setData(markers);

            let defaultCenter = JSON.parse(data[0].centroid.default_center).coordinates;
            this.map.jumpTo({
                center: new mapboxgl.LngLat(defaultCenter[0], defaultCenter[1]),
                zoom: data[0].centroid.default_zoom
            });
            //if (bbox[0] != Infinity) {
            //    this.map.fitBounds(bbox, {
            //        padding: 20,
            //        duration: 0
            //    });
            //}

            this._layerIds.forEach(function (item) {
                this.map.removeLayer(item);
            }.bind(this));

            this._layerIds = [];

            let scales = this._config.thresholds || this._config.scales;
            let scale = 0;
            scales.forEach(function (item) {
                scale++;
                this._layerIds.push("SCALE_" + scale);

                let filter = ["all"];

                if (["", NaN, null, undefined].indexOf(item[1]) >= 0) {
                    // upper limit
                    filter = [
                        ">=", "value", parseFloat(item[0])
                    ]
                } else {
                    filter = [
                        "all",
                        [">=", "value", parseFloat(item[0]) || 0],
                        ["<=", "value", parseFloat(item[1])]
                    ]
                }


                this.map.addLayer({
                    "id": "SCALE_" + scale,
                    "type": "fill",
                    "source": "data",
                    "layout": {},
                    "paint": {
                        "fill-color": item[2],
                        "fill-opacity": 0.8,
                        "fill-outline-color": "#CCCCCC"
                    },
                    filter
                });

                this._layerIds.push("SCALE_MARKER_" + scale);
                this.map.addLayer({
                    "id": "SCALE_MARKER_" + scale,
                    "type": "circle",
                    "source": "markers",
                    "layout": {},
                    "paint": {
                        "circle-color": item[2],
                        "circle-radius": 10
                    },
                    filter
                })


            }.bind(this));

            //this.map.addLayer({
            //    id: "labels_geom",
            //    type: "symbol",
            //    source: "data",
            //    layout: {
            //        "text-field": "{location_name}\n{value_fmt}t",
            //        "text-font": [
            //            "DIN Offc Pro Medium",
            //            "Arial Unicode MS Bold"
            //        ],
            //        "text-size": 12
            //    }
            //})

            this.map.addLayer({
                id: "labels",
                type: "symbol",
                source: "data",
                layout: {
                    "text-field": "{location_name}\n{value_fmt}",
                    "text-font": [
                        "DIN Offc Pro Medium",
                        "Arial Unicode MS Bold"
                    ],
                    "text-size": 11,
                    'text-offset': [0, -0],
                    'text-line-height': 3
                }
            });

            if (this._bl) this._bl.destroy();
            ewars.emit("WIDGET_LOADED");
        },

        query: function (config) {
            let isComplete = true;
            REQUIRED.forEach(req => {
                if (!config[req]) {
                    isComplete = false;
                }
            });

            if (!isComplete) {
                if (this._bl) this._bl.destroy();
                return;
            }

            if (idx) {
                if (ewars.g[idx]) {
                    this.updateData(ewars.g[idx]);
                    return;
                }
            }

            var date_range = DateUtils.processDateSpec(config, this._report_date);

            let query = {
                type: config.source_type,
                reduction: "SUM",
                interval: "DAY",
                indicator: config.indicator,
                start_date: date_range[0],
                end_date: date_range[1],
                location: null,
                geometry: true,
                series: config.series || null,
                formula: config.formula || null,
                centroid: true
            };

            if (config.loc_spec) {

                if (config.loc_spec == "SPECIFIC") {
                    query.location = config.location;
                } else if (config.loc_spec == "REPORT_LOCATION") {
                    query.location = location_uuid;
                } else if (config.loc_spec == "GENERATOR") {
                    query.location = {
                        parent_id: config.location,
                        site_type_id: config.site_type_id,
                        status: config.location_status || "ACTIVE"
                    }
                }
            } else {
                query.location = {
                    parent_id: config.location,
                    site_type_id: config.site_type_id,
                    status: config.location_status || "ACTIVE"
                }
            }

            ewars._queue.push("/arc/analysis", query)
                .then(resp => {
                    if (idx) ewars.g[idx] = resp;
                    this.updateData(resp);
                })
                .catch(err => {
                    this._emitLoaded = true;
                    ewars.emit('WIDGET_LOADED')
                })

        },

        init: function () {
            this._bl = new ewars.Blocker(this._el, "Loading...");
            if (this._config) {
                this._el.style.height = this._config.height || "100%";
                this._el.style.width = this._config.width || "100%";
            }

            this.render(function () {
                this.query(this._config)
            }.bind(this));

        }

    };

    instance.init();

    return instance;
}
;
