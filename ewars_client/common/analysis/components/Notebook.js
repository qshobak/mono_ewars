const SeriesChart = require("../../common/analysis/SeriesChart");
import Map2 from "../../common/analysis/Map2";

const PieChart = require("../../common/analysis/PieChart");
const Table = require("../../common/analysis/TableWidget");
const Calendar = require("../../common/analysis/Calendar");

import Utils from "../../documents/utils";

// Widgets
import Text from "../../common/widgets/Text";


const TYPES = {
    SERIES: SeriesChart,
    MAP: Map2,
    PIE: PieChart,
    CATEGORY: PieChart,
    TABLE: Table,
    CALENDAR: Calendar
};

const WIDGETS = {
    TEXT: Text
};

// TODO: This is temporary
ewars.g.TABLE_MODE = "NOTEBOOK";

class NotebookRow extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let typeMethod = TYPES[this.props.data.type];

        if (this.props.data.type == "MAP") {
            this._chart = new typeMethod(this.refs.chart, this.props.data);
        } else {

            if (typeMethod) {
                this._chart = typeMethod(this.refs.chart, this.props.data);
            }
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillUnmount() {

    };

    getData = () => {
        return this._chart.getData() || null;
    };

    render() {
        let content;

        let Widget = WIDGETS[this.props.data.type];
        if (Widget) {
            content = <Widget data={this.props.data}/>
        } else {
            content = <div className="chart" ref="chart"></div>;
        }

        return (
            <div className="row" style={{marginBottom: "30px"}}>
                <div className="col-12">
                    {content}
                </div>
            </div>
        )
    }
}

class Notebook extends React.Component {
    static defaultProps = {
        editor: false
    };

    constructor(props) {
        super(props);

        this.state = {
            _loaded: 0,
            canDownload: false,
            data: null
        };
        this._els = [];
        this._nodes = [];
        this._loaded = 0;

        this._totalWidgets = this.props.data.definition.length || 0;
        ewars.subscribe("WIDGET_LOADED", this._loadedWidget);

    }

    componentWillReceiveProps(props) {
        if (props.data.uuid != this.props.data.uuid) {
            // reload this chart
            this.state = {
                _loaded: 0,
                canDownload: false,
                data: null
            }
            this._els = [];
            this._loaded = 0;
            this._totalWidgets = props.data.definition.length || 0;
        }
    }

    _loadedWidget = () => {
        this._loaded++;

        if (this._loaded >= this._totalWidgets) {
            this._prep();
        } else {
        }

    };

    _prep = () => {
        let data = [];
        this._els.forEach((item) => {
            if (item) {
                if (item.getData) data.push(item.getData());
            }
        })


        this._data = Utils.formatData(data);

        this.setState({canDownload: true, data: Utils.formatData(data)})

    };

    _download = () => {
        let bl = new ewars.Blocker(null, "Generating excel...");

        ewars.tx("com.ewars.document.data", ["Notebook Export", this.state.data[0] || [], this.state.data[1]])
            .then((resp) => {
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.n);
            })
    };

    componentWillUnmount() {
        ewars._queue.clear();
        this._els = [];
        this._nodes = [];
        ewars.unsubscribe("WIDGET_LOADED", this._loadedWidget);
    }

    render() {
        let nodes = this.props.data.definition.map((row, index) => {
            return <NotebookRow
                data={row}
                ref={(el) => {
                    this._els.push(el);
                }}
                onDataLoaded={this._receiveData}
                key={"ROW_" + this.props.data.uuid + index}/>
        });

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={this.props.data.name}>
                    <div className="btn-group pull-right">
                        {(this.props.data.created_by == window.user.id && !this.props.editor) ?
                            <ewars.d.Button
                                icon="fa-pencil"
                                onClick={() => {
                                    this.props.onEdit(this.props.data);
                                }}
                            />
                            : null}
                        {this.state.canDownload ?
                            <ewars.d.Button
                                icon="fa-download"
                                onClick={this._download}/>
                            : null}
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="grid" style={{margin: "0 auto", maxWidth: 980, padding: 16}}>
                                    {nodes}
                                </div>
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Notebook;