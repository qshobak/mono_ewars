import Tab from '../../common/components/ide/Tab.react';
import NotebookEditor from './NotebookEditor';
import Notebook from "./Notebook";


const STYLES = {
    new: {
        position: "absolute",
        top: "50%",
        marginTop: "-200px",
        textAlign: "center",
        width: "100%"
    },
    newIcon: {
        fontSize: "60px",
        color: "#CCC",
        marginBottom: "20px"
    }
};

const MODAL_ACTIONS = [
    {icon: "fa-times", label: "Close", action: "CLOSE"}
];


class NewCreator extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    onClick = () => {
        this.props.onCreateNew();
    };

    render() {
        return (
            <div className="map-new" style={STYLES.new}>
                <div className="icon" style={STYLES.newIcon}><i className="fal fa-book"></i></div>
                <div className="text" style={{marginBottom: 20}}>"Open" to view an existing notebook or click "Create"
                    below to create your own.
                </div>
                <div className="button">
                    <ewars.d.Button
                        icon="fa-plus"
                        onClick={this.onClick}
                        label="Create Notebook"/>
                </div>
            </div>
        )
    }
}

class NotebookListItem extends React.Component {
    static defualtProps = {
        shared: false,
        data: {}
    };

    constructor(props) {
        super(props);
    }

    _select = () => {
        this.props.onClick(this.props.data);
    };

    _edit = (action) => {
        this.props.onAction(action, this.props.data);
    };

    render() {
        let icon;
        if (!this.shared) {
            if (this.props.data.shared) icon = <i style={{marginRight: '5px'}} className="fal fa-users"></i>;
        }

        if (this.props.data.created_by == window.user.id) {
            return (
                <div className="block hoverable clickable" onClick={this._select} style={{cursor: 'pointer'}}>
                    <ewars.d.Layout style={{border: '1px solid rgba(0,0,0,0.2)'}}>
                        <ewars.d.Row>
                            <ewars.d.Cell style={{padding: '8px', display: 'block'}}>
                                {icon}<span>{this.props.data.name}</span>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <ewars.d.Row style={{background: 'rgba(0,0,0,0.1)'}} height="23px">
                            {this.props.shared ?
                                <ewars.d.Cell>
                                    {this.props.data.user_name}
                                </ewars.d.Cell>
                                : null}
                            <ewars.d.Cell style={{display: 'block'}}>
                                <ewars.d.ActionGroup
                                    height="23px"
                                    right={true}
                                    actions={[['fa-pencil', 'EDIT'], ['fa-trash', 'DELETE']]}
                                    onAction={this._edit}/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>

                </div>
            )
        }
        return (
            <div className="block hoverable clickable" onClick={this._select} style={{cursor: 'pointer'}}>
                <ewars.d.Layout style={{border: '1px solid rgba(0,0,0,0.1)'}}>
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: '8px'}}>
                            {this.props.data.name}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row style={{background: 'rgba(0,0,0,0.1)'}} height="23px">
                        <ewars.d.Cell style={{paddingTop: '5px', paddingLeft: '8px'}}>
                            {this.props.data.user_name}
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: 'block'}} width="20px">
                            <ewars.d.ActionGroup
                                height="23px"
                                right={true}
                                actions={[['fa-copy', 'DUPE']]}
                                onAction={this._edit}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>

            </div>
        )
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: [],
            view: 'DEFAULT',
            notebook: null
        }

        this._query();

    }

    _query = (data) => {
        // TODO: Add filter for shared
        let isShared = false;
        if (data && data.view) isShared = data.view == 'SHARED' ? true : false;
        let com = 'com.ewars.notebooks';
        if (isShared) com = 'com.ewars.notebooks.shared';
        ewars.tx(com, [])
            .then(resp => {
                if (data) {
                    this.setState({
                        ...data,
                        data: resp
                    })
                } else {
                    this.setState({
                        data: resp
                    })
                }
            })
    };

    _createNew = () => {
        let data = {
            title: "",
            description: "",
            definition: [],
            created_by: window.user.id,
            shared: false
        }

        this.setState({
            editing: data,
            notebook: null
        })

    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({show: false})
    };

    _showBrowse = () => {
        this.setState({
            show: true
        })
    };

    _onChangeView = (view) => {
        this._query({view: view.view});
    };

    _select = (data) => {
        if (this.state.notebook) {
            if (this.state.notebook.uuid == data.uuid) return;
        }
        ewars._queue.clear();
        this.setState({
            notebook: data
        })
    };

    _onEdit = (data) => {
        this.setState({
            editing: data,
            notebook: null
        })
    };

    _onClose = () => {
        this._query({editing: null, notebook: null})
    };

    _onDelete = (data) => {
        ewars.prompt("fa-trash", "Delete notebook?", "Are you sure you want to delete this notebook?", () => {
            let bl = new ewars.Blocker(null, "Deleting notebook");
            ewars.tx('com.ewars.notebook.delete', [data.uuid])
                .then((resp) => {
                    bl.destroy();
                    this._query();
                })
        })
    };

    /**
     * Duplicate a notebook
     * @param data
     * @private
     */
    _dupeNotebook = (data) => {
        let newNotebook = ewars.copy(data);
        newNotebook.uuid = null;
        newNotebook.created_by = window.user.id;
        newNotebook.shared = false;
        newNotebook.name += ' (copy)';
        newNotebook.created = null;
        newNotebook.last_modified = null;

        this.setState({
            editing: newNotebook,
            notebook: null
        })
    };

    _action = (action, data) => {
        switch (action) {
            case 'DELETE':
                this._onDelete(data);
                break;
            case 'EDIT':
                this._onEdit(data);
                break;
            case 'DUPE':
                this._dupeNotebook(data);
                break;
            case 'CREATE':
                this._createNew();
                break;
            default:
                return;
        }
    };

    render() {
        let books = [];
        let notebook;


        if (this.state.view == 'DEFAULT') {
            books = this.state.data.filter(item => {
                return item.created_by == window.user.id;
            })
        } else {
            books = this.state.data.filter(item => {
                return item.created_by != window.user.id && item.created_by != window.user.id;
            })
        }

        books = books.sort((a, b) => {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
        });

        if (books.length <= 0) {
            books = (
                <p className="placeholder">No notebooks available at this time.</p>
            )
        } else {
            books = books.map(item => {
                return (
                    <NotebookListItem
                        data={item}
                        shared={this.state.view == 'SHARED'}
                        onAction={this._action}
                        onClick={this._select}/>
                )
            })
        }
        // we're editing a notebook

        if (this.state.notebook) {
            notebook = (
                <Notebook
                    onEdit={this._onEdit}
                    data={this.state.notebook}/>
            )
        } else if (this.state.editing) {
            return (
                <NotebookEditor
                    onClose={this._onClose}
                    data={this.state.editing}/>
            )
        }


        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="30%" borderRight={true}>
                        <ewars.d.Row height="36px">
                            <ewars.d.Cell>
                                <div xmlns="http://www.w3.org/1999/xhtml"
                                     className="iw-tabs"
                                     style={{display: "block", float: 'left'}}>
                                    <Tab
                                        label="My Notebooks"
                                        icon="fa-book"
                                        active={this.state.view == "DEFAULT"}
                                        onClick={this._onChangeView}
                                        data={{view: "DEFAULT"}}/>
                                    <Tab
                                        label="Shared"
                                        icon="fa-users"
                                        active={this.state.view == "SHARED"}
                                        onClick={this._onChangeView}
                                        data={{view: "SHARED"}}/>
                                </div>

                            </ewars.d.Cell>
                        </ewars.d.Row>
                        {this.state.view == 'DEFAULT' ?
                            <ewars.d.Toolbar>
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        onClick={() => {
                                            this._action("CREATE");
                                        }}
                                        label="Create Notebook"
                                        icon="fa-plus"/>
                                </div>

                            </ewars.d.Toolbar>
                            : null}
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <ewars.d.Panel>
                                    <div className="block-tree" style={{position: 'relative'}}>
                                        {books}
                                    </div>
                                </ewars.d.Panel>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true}>
                        {notebook ? notebook : <NewCreator onCreateNew={this._createNew}/>}
                    </ewars.d.Cell>

                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Dashboard;