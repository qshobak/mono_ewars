import Moment from "moment";

import DateUtils from "../../documents/utils/DateUtils";
import RangeUtils from "../utils/RangeUtils";

import DataSource from "../models/DataSource";
import AnalysisUtils from "../utils/AnalysisUtils";

var defaults = {};

const TableWidget = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,
        _data: null,
        _templateId: template_id,
        _locationUUID: location_uuid,
        _largeTable: false,

        _cells: {},
        _completed: 0,

        _columns: {},

        getData: function () {
            return ['TABLE', [this._config, this._queries]];
        },

        _query: function () {

            // Get locations firrst
            var uri = "http://" + ewars.domain + "/api/Analysis()";
            if (this._isPublic) uri = "http://" + ewars.domain + "/api/dms/Analysis()";


            let tki;
            if (window.dt) {
                if (window.dt.template) {
                    tki = JSON.parse(window.dt.template).tki;
                }
            }


            if (this._config.loc_spec == 'GENERATOR') {
                let parentId;
                if (this._config.generator_parent_source == "INHERIT") {
                    parentId = this._locationUUID;
                } else {
                    parentId = this._config.generator_locations_parent;
                }
                ewars.tx('com.ewars.table.locations', [{
                    type: 'GENERATOR',
                    tki: tki,
                    parent: parentId,
                    sti: this._config.generator_locations_type,
                    status: this._config.generator_location_status
                }]).then(resp => {
                    this._setup(resp);
                }).catch(err => {
                    console.log(err);
                })
            } else if (this._config.loc_spec == 'SPECIFIC') {
                ewars.tx('com.ewars.table.locations', [{
                    type: 'SPECIFIC',
                    tki: tki,
                    lid: this._config.location_id || this._config.location
                }]).then(resp => {
                    this._setup(resp);
                }).catch(err => {
                    console.log(err);
                })
            } else if (this._config.loc_spec == 'GROUP') {
                ewars.tx('com.ewars.table.locations', [{
                    type: 'GROUP',
                    tki: tki,
                    groups: this._config.group_ids
                }]).then(resp => {
                    this._setup(resp);
                }).catch(err => {
                    console.log(err);
                })
            }

        },

        /**
         * Handle figuring out the data needed to be loaded for each location in question
         * @param locations
         * @private
         */
        _setup: function (locations) {
            this._totalItems = locations.length * this._config.columns.length;

            this._queries = locations.map(loc => {
                return {
                    name: loc.name,
                    parent_name: loc.parent_name || null,
                    uuid: loc.uuid,
                    rid: ewars.utils.uuid(),
                    pcode: loc.pcode,
                    columns: this._config.columns.map(col => {
                        return {
                            cid: ewars.utils.uuid(),
                            ...col
                        }
                    })
                }
            });

            this._setupTempTable();
        },

        _setupFinalTable: function () {
            let table = document.createElement("table");

            if (ewars.g.TABLE_MODE)
                table.setAttribute("class", "nb-table");

            let headers = [];
            // If we're showing the parent column
            if (this._config.b_parent_col) {
                let header = document.createElement("th");
                header.appendChild(document.createTextNode(" "));
                headers.push(header);
            }

            let locHeader = document.createElement("th");
            locHeader.appendChild(document.createTextNode("Location"));
            headers.push(locHeader);

            this._config.columns.forEach(col => {
                let newHeader = document.createElement("th");
                newHeader.appendChild(document.createTextNode(__(col.title)));
                headers.push(newHeader);
            });

            let thead = document.createElement("thead");
            let trow = document.createElement("tr");
            headers.forEach(item => {
                trow.appendChild(item);
            });
            thead.appendChild(trow);

            if (this._config.show_column_header || this._config.show_column_header == null) {
                table.appendChild(thead);
            }

            this._queries.forEach(location => {
                location.data = {};
                location.columns.forEach(cell => {
                    location.data[cell.uuid] = cell.data.data;
                })
            });

            let sorted = this._queries.sort((a, b) => {
                if (a.name > b.name) return 1;
                if (a.name < b.name) return -1;
                return 0;
            });

            // If there's a limit imposed, respect it

            if (["", null, undefined].indexOf(this._config.t_sort_column) < 0) {
                switch (this._config.t_sort_column) {
                    case "LOC_ASC":
                        sorted = sorted.sort((a, b) => {
                            if (__(a.name) > __(b.name)) return 1;
                            if (__(a.name) < __(b.name)) return -1;
                            return 0;
                        });
                        break;
                    case "LOC_DESC":
                        sorted = sorted.sort((a, b) => {
                            if (__(a.name) > __(b.name)) return -1;
                            if (__(a.name) < __(b.name)) return 1;
                            return 0;
                        });
                        break;
                    default:
                        let [key, dir] = this._config.t_sort_column.split(".");
                        if (dir == "DESC") {
                            sorted = sorted.sort((a, b) => {
                                if (parseFloat(a.data[key] || 0) > parseFloat(b.data[key] || 0)) return -1;
                                if (parseFloat(a.data[key] || 0) < parseFloat(b.data[key] || 0)) return 1;
                                return 0;
                            })
                        } else {
                            sorted = sorted.sort((a, b) => {
                                if (parseFloat(a.data[key] || 0) > parseFloat(b.data[key] || 0)) return 1;
                                if (parseFloat(a.data[key] || 0) < parseFloat(b.data[key] || 0)) return -1;
                                return 0;
                            })
                        }
                        break;
                }
            } else {
                sorted = sorted.sort((a, b) => {
                    if (__(a.name) > __(b.name)) return 1;
                    if (__(a.name) < __(b.name)) return -1;
                    return 0;
                })
            }

            if (this._config.t_limit) sorted = sorted.slice(0, this._config.t_limit);

            let rowContainer = document.createElement("tbody");

            sorted.forEach(item => {
                let row = document.createElement("tr");

                if (this._config.b_parent_col) {
                    let pCell = document.createElement("td");
                    pCell.appendChild(document.createTextNode(item.parent_name));
                    row.appendChild(pCell);
                }

                let locCell = document.createElement("td");
                locCell.appendChild(document.createTextNode(item.name));
                row.appendChild(locCell);

                item.columns.forEach(col => {
                    var cell = document.createElement("td");
                    cell.setAttribute("id", col.cid);
                    var iconCell = document.createElement("i");
                    iconCell.setAttribute("class", "fal fa-cog fa-spin");
                    cell.appendChild(iconCell);
                    row.appendChild(cell);
                });

                rowContainer.appendChild(row);

            });
            //
            // // Filter out columns with no data if this is set
            // if (this._config.hide_null_rows) {
            //     let colIds = this._config.columns.map(item => {
            //         return item.uuid;
            //     });
            //     sorted = sorted.filter(item => {
            //         let hasValue = false;
            //         colIds.forEach(col => {
            //             if (item.data[col.uuid] != null && item.data[col.uuid] != undefined && item.data[col.uuid] > 0) hasValue = true
            //         });
            //
            //         return hasValue;
            //     })
            // }


            table.appendChild(rowContainer);
            this._el.innerHTML = "";
            this._el.appendChild(table);

            sorted.forEach(row => {
                row.columns.forEach(cell => {
                    this._renderCell(cell);
                })
            })
        },

        _setupTempTable: function() {
            let table = document.createElement("table");

            if (ewars.g.TABLE_MODE)
                table.setAttribute("class", "nb-table");

            let headers = [];
            // If we're showing the parent column
            if (this._config.b_parent_col) {
                let header = document.createElement("th");
                header.appendChild(document.createTextNode(" "));
                headers.push(header);
            }

            let locHeader = document.createElement("th");
            locHeader.appendChild(document.createTextNode("Location"));
            headers.push(locHeader);

            this._config.columns.forEach(col => {
                let newHeader = document.createElement("th");
                newHeader.appendChild(document.createTextNode(__(col.title)));
                headers.push(newHeader);
            });

            let thead = document.createElement("thead");
            let trow = document.createElement("tr");
            headers.forEach(item => {
                trow.appendChild(item);
            });
            thead.appendChild(trow);

            if (this._config.show_column_header || this._config.show_column_header == null) {
                table.appendChild(thead);
            }
            // Done with headers

            let rows = [];

            let sorted = this._queries.sort((a, b) => {
                if (a.name > b.name) return 1;
                if (a.name < b.name) return -1;
                return 0;
            })

            if (["", null, undefined].indexOf(this._config.t_sort_column) < 0) {
                switch (this._config.t_sort_column) {
                    case "LOC_ASC":
                        sorted = sorted.sort((a, b) => {
                            if (__(a.name) > __(b.name)) return 1;
                            if (__(a.name) < __(b.name)) return -1;
                            return 0;
                        });
                        break;
                    case "LOC_DESC":
                    default:
                        sorted = sorted.sort((a, b) => {
                            if (__(a.name) > __(b.name)) return -1;
                            if (__(a.name) < __(b.name)) return 1;
                            return 0;
                        });
                        break;
                }
            } else {
                sorted = sorted.sort((a, b) => {
                    if (__(a.name) > __(b.name)) return 1;
                    if (__(a.name) < __(b.name)) return -1;
                    return 0;
                })
            }

            if (this._config.t_limit) sorted = sorted.slice(0, this._config.t_limit);

            // if this is a larger table we show progress as an indicator versus individual rows loading
            // if (sorted.length > 40) this._largeTable = true;
            this._largeTable = true; // Force large table for now

            let rowContainer = document.createElement("tbody");

            if (!this._largeTable) {
                sorted.forEach(item => {
                    let row = document.createElement("tr");

                    if (this._config.b_parent_col) {
                        let pCell = document.createElement("td");
                        pCell.appendChild(document.createTextNode(item.parent_name));
                        row.appendChild(pCell);
                    }

                    let locCell = document.createElement("td");
                    locCell.appendChild(document.createTextNode(item.name));
                    row.appendChild(locCell);

                    item.columns.forEach(cell => {
                        var cell = document.createElement("td");
                        cell.setAttribute("id", cell.cid);
                        var iconCell = document.createElement("i");
                        iconCell.setAttribute("class", "fal fa-cog fa-spin");
                        cell.appendChild(iconCell);
                        row.appendChild(cell);
                    });

                    rowContainer.appendChild(row);
                });
            } else {
                let colLength = sorted[0].columns.length + 1;
                if (this._config.b_parent_col) colLength += 1;
                let c = document.createElement("td");
                c.setAttribute('colspan', colLength);
                c.style.height = "30px";

                let row = document.createElement("tr");
                row.appendChild(c);
                rowContainer.appendChild(row);
            }

            table.appendChild(rowContainer);

            let overlay;
            if (!this._largeTable) {
                let overlay = document.createElement("div");
                overlay.setAttribute("class", "table-loading-overlay");
                let innerOverlay = document.createElement('div');
                innerOverlay.setAttribute("class", "l-text");
                innerOverlay.appendChild(document.createTextNode("Loading..."));
                overlay.appendChild(innerOverlay);
                this._el.appendChild(overlay);
            } else {
                this._overlayId = ewars.utils.uuid();
                let overlay = document.createElement("div");
                overlay.setAttribute("class", "table-loading-overlay");
                overlay.setAttribute("id", this._overlayId);
                let innerOverlay = document.createElement('div');
                innerOverlay.setAttribute("class", "l-text");
                innerOverlay.appendChild(document.createTextNode("Preparing..."));
                overlay.appendChild(innerOverlay);
                this._el.appendChild(overlay);
            }

            this._el.style.position = "relative";
            this._el.appendChild(table);
            if (this._overlayId) {
                this.overlayEl = document.getElementById(this._overlayId);
            }

            this._execute();
        },

        _renderCell: function (cell) {
            let elC = document.getElementById(cell.cid);

            let label = '';

            let rawVal = cell.data.data;
            let displayVal = ewars.NUM(rawVal, cell.format || "0,0.00");

            // Get value mapping
            let valMap;
            if (cell.o_val_mapping) valMap = cell.value_mapping;
            if (valMap) {
                let res;
                valMap.forEach((thresh) => {
                    if (thresh[1] == "INF") {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                    } else {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                            res = thresh;
                        }
                    }
                });

                if (res) {
                    if (res[2].indexOf("{value}") >= 0) {
                        displayVal = res[2].replace("{value}", displayVal)
                    } else {
                        displayVal = res[2];
                    }
                }
            }

            label += displayVal;

            if (cell.suffix) label += ` ${cell.suffix}`;

            let colMap;
            if (cell.o_val_colour) colMap = cell.value_colouring;

            // Get colour mapping
            let displayColour;
            if (colMap) {
                let res;
                colMap.forEach((thresh) => {
                    if (thresh[1] == "INF") {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                    } else {
                        if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                            res = thresh;
                        }
                    }
                });

                if (res) {
                    displayColour = res[2];
                }
            }

            elC.innerText = label;
            if (displayColour) {
                elC.style.color = displayColour;
            }

        },

        _execute: function () {
            this._queries.forEach(location => {
                location.columns.forEach(cell => {

                    let endDate, startDate;
                    if (cell.period) {
                        endDate = RangeUtils.process(cell.period[1], this._reportDate);
                        startDate = RangeUtils.process(cell.period[0], this._reportDate, endDate);
                    } else {
                        let dateRange = DateUtils.processDateSpec(cell, this._reportDate);
                        startDate = dateRange[0];
                        endDate = dateRange[1];
                    }
                    let query = {
                        type: cell.source_type,
                        start_date: startDate,
                        end_date: endDate,
                        reduction: cell.reduction || 'SUM',
                        location: location.uuid,
                        interval: 'DAY',
                        meta: false
                    }

                    if (cell.source_type == 'SLICE_COMPLEX') {
                        query.formula = cell.formula;
                        query.series = cell.series;
                    } else {
                        query.indicator = cell.indicator;
                    }

                    ewars._queue.push("/arc/analysis", query)
                        .then(resp => {
                            if (resp) {
                                cell.data = resp;
                                if (!this._largeTable) {
                                    this._renderCell(cell);
                                } else {
                                    this.overlayEl.innerText = `Loaded ${this._completed + 1} of ${this._totalItems}`;
                                }
                                this._completed++;
                                this._checkComplete()
                            } else {
                                this._completed++;
                                this._checkComplete();

                                if (!this._largeTable) {
                                    document.getElementById(cell.cid).innerHTML = '<i className="fal fa-exclamation-triangle"></i>';
                                } else {
                                    // increment counter
                                    this.overlayEl.innerText = `Loaded ${this._completed + 1} of ${this._totalItems}`;
                                }
                            }
                        })
                        .catch(err => {
                            if (!this._largeTable) {
                                document.getElementById(cell.cid).innerHTML = '<i className="fal fa-exclamation-triangle"></i>';
                            } else {
                                // increment counter
                                this.overlayEl.innerText = `Loaded ${this._completed + 1} of ${this._totalItems}`;
                            }
                            this._completed++;
                            this._checkComplete();
                        })


                })
            })
        },

        /**
         * Check whether the table has completed loading
         * @private
         */
        _checkComplete: function () {
            if (this._completed >= this._totalItems) {
                // We're done loading, we need to perform some
                // work on the result set

                if (!this._emitLoaded) {
                    this._emitLoaded = true;
                    this._setupFinalTable();
                    ewars.emit('WIDGET_LOADED');
                }
            }
        },

        _render: function () {

            let table = document.createElement("table");
            if (ewars.g.TABLE_MODE) {
                table.setAttribute("class", "nb-table");
            }

            let headers = [];

            if (this._config.b_parent_col) {
                let pHeader = document.createElement("th");
                pHeader.appendChild(document.createTextNode(" "));
                headers.push(pHeader);
            }

            let lHeader = document.createElement("th");
            lHeader.appendChild(document.createTextNode("Location"));
            headers.push(lHeader);

            for (let uuid in this._config.columns) {
                let col = this._config.columns[uui];
                let header = document.createElement("th");
                header.appendChild(document.createTextNode(__(col.title)));
                headers.push(header);
            }

            let thead = document.createElement("thead");
            let trow = document.createElement("tr");

            headers.forEach(item => {
                trow.appendChild(item);
            });
            thead.appendChild(trow);

            let sortedData = [];
            for (var n in this._data) {
                sortedData.push({
                    ...this._data[n],
                    uuid: n
                })
            }
            if (["", null, undefined].indexOf(this._config.t_sort_column) < 0) {
                switch (this._config.t_sort_column) {
                    case "LOC_ASC":
                        sortedData = sortedData.sort((a, b) => {
                            if (__(a.location.name) > __(b.location.name)) return 1;
                            if (__(a.location.name) < __(b.location.name)) return -1;
                            return 0;
                        });
                        break;
                    case "LOC_DESC":
                        sortedData = sortedData.sort((a, b) => {
                            if (__(a.location.name) > __(b.location.name)) return -1;
                            if (__(a.location.name) < __(b.location.name)) return 1;
                            return 0;
                        });
                        break;
                    default:
                        let [key, dir] = this._config.t_sort_column.split(".");
                        if (dir == "DESC") {
                            sortedData = sortedData.sort((a, b) => {
                                if (a.data[key] > b.data[key]) return -1;
                                if (a.data[key] < b.data[key]) return 1;
                                return 0;
                            })
                        } else {
                            sortedData = sortedData.sort((a, b) => {
                                if (a.data[key] > b.data[key]) return 1;
                                if (a.data[key] < b.data[key]) return -1;
                                return 0;
                            })
                        }
                        break;
                }
            } else {
                sortedData = sortedData.sort((a, b) => {
                    if (__(a.location.name) > __(b.location.name)) return 1;
                    if (__(a.location.name) < __(b.location.name)) return -1;
                    return 0;
                })
            }

            if (this._config.t_limit) {
                // Limit the number of results
                sortedData = sortedData.slice(0, this._config.t_limit);
            }

            let rows = [];

            sortedData.forEach((row) => {

                let hasValue = false;
                if (this._config.hide_null_rows) {
                    for (let i in this._config.columns) {
                        let cDef = this._config.columns[i];
                        if (row.data[cDef.uuid] > 0 && row.data[cDef.uuid] != null) {
                            hasValue = true;
                        }
                    }
                } else {
                    hasValue = true;
                }

                if (hasValue) {
                    let row = document.createElement("tr");
                    if (this._config.b_parent_col) {
                        let pRHeader = document.createElement("th");
                        pRHeader.appendChild(document.createTextNode(row.location.parent_name));
                        row.appendChild(pRHeader);
                    }

                    let lRHeader = document.createElement("th");
                    lRHeader.appendChild(document.createTextNode(__(row.location.name)));
                    row.appendChild(lRHeader);

                    for (let uuid in this._config.columns) {
                        let col = this._config.columns[uuid];
                        let value = row.data[col.uuid];
                        let colDef = this._columns[col.uuid];
                        let label = "";
                        if (colDef.prefix) label += `${colDef.prefix} `;

                        let rawVal = value;
                        let displayVal = ewars.NUM(value, col.format || "0,0.00");

                        // Get value mapping
                        let valMap;
                        if (colDef.o_val_mapping) valMap = colDef.value_mapping;
                        if (valMap) {
                            let res;
                            valMap.forEach((thresh) => {
                                if (thresh[1] == "INF") {
                                    if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                                } else {
                                    if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                                        res = thresh;
                                    }
                                }
                            });

                            if (res) {
                                if (res[2].indexOf("{value}") >= 0) {
                                    displayVal = res[2].replace("{value}", displayVal)
                                } else {
                                    displayVal = res[2];
                                }
                            }
                        }

                        label += displayVal;


                        if (colDef.suffix) label += ` ${colDef.suffix}`;

                        let colMap;
                        if (col.o_val_colour) colMap = col.value_colouring;

                        // Get colour mapping
                        let displayColour;
                        if (colMap) {
                            let res;
                            colMap.forEach((thresh) => {
                                if (thresh[1] == "INF") {
                                    if (parseFloat(rawVal) >= parseFloat(thresh[0])) res = thresh;
                                } else {
                                    if (parseFloat(rawVal) >= parseFloat(thresh[0]) && parseFloat(rawVal) <= parseFloat(thresh[1])) {
                                        res = thresh;
                                    }
                                }
                            });

                            if (res) {
                                displayColour = res[2];
                            }
                        }

                        let cell = document.createElement("td");
                        cell.appendChild(document.createTextNode(label));
                        if (displayColour)  cell.style.color = displayColour;
                        row.push(cell);
                    }
                }
            });

            let tbody = document.createElement("tbody");
            rows.forEach(item => {
                tbody.appendChild(item);
            });


            table.appendChild(thead);
            table.appendChild(tbody);

            this._el.innerHTML = "";
            this._el.appendChild(table);
        },

        init: function () {

            this._query();

        }
    };

    instance.init();

    return instance;
};

export default TableWidget;
