import Form from "./Form.react";
import Button from "./ButtonComponent.react";
import FieldMapper from "../utils/FieldMapper";

const _statusMap = [
    ["DISABLED", "Disabled"],
    ["ENABLED", "Enabled"]
];

const _statusDict = {
    "DISABLED": "Disabled",
    "ENABLED": "Enabled"
};

var FieldDefinition = React.createClass({
    displayName: "Form Definition Field Component",

    getInitialState: function () {
        return {
            detailShown: false
        };
    },

    _defaultType: "text",

    propTypes: {
        name: React.PropTypes.string.isRequired,
        definition: React.PropTypes.object.isRequired
    },

    componentWillMount: function () {
        if (this.props.exposed) {
            this.state.detailShown = true
        }
    },

    _baseOptions: {
        label: {
            "type": "text",
            "label": "Field Label",
            "required": true
        },
        name: {
            type: "text",
            "label": "Field Name",
            required: true
        },
        type: {
            type: "select",
            label: "Field Type",
            required: true,
            defaultValue: "text",
            options: FieldMapper._optionsList
        },
        status: {
            type: "select",
            label: "Field Status",
            required: true,
            defaultValue: "DISABLED",
            options: _statusMap
        },
        instructions: {
            type: "textarea",
            label: "Field Instructions",
            help: "Instructions for authors. Shown when submitting data"
        },
        required: {
            type: "boolean",
            label: "Required?",
            required: false,
            defaultValue: false
        },
        default_value: {
            type: "text",
            label: "Default Value"
        },
        indexed: {
            type: "switch",
            label: {
                en: "Indexed Field"
            },
            help: {
                en: "An indexed field is made available for search and sorting in subsequent UIs"
            }
        },
        conditional_bool: {
            type: "boolean",
            label: "Conditional Logic",
            defaultValue: false
        },
        conditions: {
            type: "conditions",
            label: "Conditional Rules",
            conditions: {
                application: "all",
                rules: [
                    ["conditional_bool", "eq", true]
                ]
            }
        }
    },

    /**
     * Show/Hide the expanded definition for this field *
     * @private
     */
    _showDetails: function () {
        this.setState({
            detailShown: (this.state.detailShown ? false : true)
        });
        this.props.onShowProps(this.props.index);
    },

    _handleDestroy: function () {
        this.props.onDelete(this.props.name);
    },

    formatLabel: function (labelValue) {
        if (!labelValue) return "Untitled Field";
        if (labelValue.length >= 70) {
            return labelValue.substring(0, 70) + "...";
        }
        return labelValue;
    },

    onPropChange: function (data, name, value) {
        this.props.onChange(this.props.definition.name, this.props.index, name, value);
    },

    _getConditionalFieldOptions: function () {
        var options = [];
        for (var i in this.props.form) {
            var field = this.props.form[i];
            if (field.name != this.props.name) {
                options.push(
                    <option value={i}>{field.label}</option>
                )
            }
        }
        return options;
    },

    _onUp: function (e) {
        e.preventDefault();
        this.props.onMove(this.props.index, parseInt(this.props.index) - 1);
    },

    _onDown: function (e) {
        e.preventDefault();
        this.props.onMove(this.props.index, parseInt(this.props.index) + 1);
    },

    _hasChildren: function () {
        var hasChildren = false;
        if (this.props.children.length > 0) hasChildren = true;
        if (this.props.definition.type == "row") return true;
        if (this.props.definition.type == "matrix") return true;

        return hasChildren;
    },

    addChild: function () {
        this.props.onAddChild(this.props.index);
    },

    render: function () {

        var hasChildren = this._hasChildren();
        var conditionalFieldOptions = this._getConditionalFieldOptions();

        var statusString = "Disabled";
        if (this.props.definition.status) statusString = _statusDict[this.props.definition.status];

        var labelShort = this.formatLabel(this.props.definition.label);

        var settings = null;
        if (this.props.definition.type) {
            var type = FieldMapper._typeMap[this.props.definition.type];

            var typeOptions = FieldMapper._typeOptions[this.props.definition.type] ? FieldMapper._typeOptions[this.props.definition.type] : {};
            var options = {};
            $.extend(options, this._baseOptions, typeOptions);
            if (type) {
                settings = (
                    <Form
                        readOnly={false}
                        definition={options}
                        data={this.props.definition}
                        preview={true}
                        conditionalFieldOptions={conditionalFieldOptions}
                        updateAction={this.onPropChange}/>
                )
            }
        } else {
            settings = (
                <div className="box-placeholder">
                    <p>Options have not been configured for this field type at this time</p>
                </div>
            )
        }

        var fieldTypeName = FieldMapper._types[this.props.definition.type];

        return (
            <div data-index={this.props.index} className="form-item">
                <div className="form-item-row-header">
                    <table width="100%">
                        <tr>
                            <td width="32px">
                                <div className="form-item-no">
                                    <span>{this.props.order}</span>
                                </div>
                            </td>
                            <td width="32px">
                                <a href="#" onClick={this._onUp}>
                                    <i className="fal fa-caret-up"></i>
                                </a>
                            </td>
                            <td width="32px">
                                <a href="#" onClick={this._onDown}>
                                    <i className="fal fa-caret-down"></i>
                                </a>
                            </td>
                            <td width="1024px">
                                <div className="form-item-name">{labelShort} - {fieldTypeName}</div>
                            </td>
                            <td width="32px">
                                <div className="form-item-collapse" onClick={this._showDetails}>
                                    <i className="fal fa-chevron-down"></i>
                                </div>
                            </td>
                            <td width="32px">
                                <div className="form-item-collapse" onClick={this._handleDestroy}>
                                    <i className="fal fa-trash"></i>
                                </div>
                            </td>

                            <td width="32px">
                                <div className="form-item-status">{statusString}</div>
                            </td>
                        </tr>
                    </table>
                </div>
                {this.state.detailShown ?
                    <div className="field-definition">
                        {settings}
                    </div>
                    : null }
                {hasChildren ?
                    <div className="field-children">
                        {this.props.children}

                        {this.props.canAddChilds ?
                            <div className="btn-group">
                                <ewars.d.Button
                                    icon="fa-plus"
                                    colour="green"
                                    onClick={this.addChild}/>
                            </div>
                            : null}
                    </div>
                    : null }
                <div className="clearer"></div>
            </div>

        )

    },

    _onSubFieldChange: function () {

    },

    _onSubFieldDelete: function () {

    }
});

export default FieldDefinition;
