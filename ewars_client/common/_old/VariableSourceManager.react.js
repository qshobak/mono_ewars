var VariableSourceManager = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        let style = {};
        if (this.props.anchor) style.position = "relative";

        return (
            <div className="data-manager" id="dm">
                <div className="base-panel">
                    <div className="header">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td>
                                    <div className="title">Variable
                                        Sources
                                    </div>
                                </td>
                                <td>
                                    <div className="btn-group pull-right">
                                        <button rel="tooltip" className="btn default">
                                            <i className="fal fa-plus"></i><span>Variable</span></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="body">
                        <div className="series-list"></div>
                    </div>
                </div>
            </div>
        )
    }
});

export default VariableSourceManager;
