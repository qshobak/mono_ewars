class SettingsSection extends React.Component {
    render() {
        return (
            <div className="ide-section-basic">
                <div className="header">{this.props.title}</div>
                {this.props.children}
            </div>
        )
    }
}

export default SettingsSection;
