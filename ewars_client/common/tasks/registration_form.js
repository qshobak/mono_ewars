export default {
    user_name: {
        type: "display",
        label: {
            en: "Name"
        }
    },
    email: {
        type: "display",
        label: {
            en: "Email"
        }
    },
    user_type: {
        type: "select",
        label: {
            en: "User Type"
        },
        required: true,
        options: [
            ["USER", _l("USER")],
            ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")],
            ["REGIONAL_ADMIN", _l("REGIONAL_ADMIN")]
        ]
    },
    location_id: {
        type: "location",
        label: {
            en: "Location"
        },
        conditions: {
            application: "all",
            rules: [
                ["user_type", "eq", "REGIONAL_ADMIN"]
            ]
        }
    },
    org_id: {
        type: "select",
        label: {
            en: "Organization"
        },
        optionsSource: {
            resource: "organization",
            query: {},
            valSource: "uuid",
            labelSource: "name"
        }
    }
};
