class HTMLWidget extends React.Component {
    render() {
        let style = {
            width: "100%",
            height: "100%",
            overflow: "hidden"
        };

        return (
            <div style={style} dangerouslySetInnerHTML={{__html: this.props.data.c.html}}>

            </div>
        )
    }
}

export default HTMLWidget;