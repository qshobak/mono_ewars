import {Layout, Row, Cell} from "../../../common/cmp/Layout";
import Toolbar from "../../../common/cmp/Toolbar";
const TextField = require("../../../common/components/fields/TextInputField.react");
import Button from "../../../common/cmp/Button";

class UserGridRow extends React.Component {
    constructor(props) {
        super(props)
    }

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let iconClass = "fal fa-square";
        if (this.props.selected.indexOf(this.props.data.id) >= 0) {
            iconClass = "fal fa-check-square";
        }

        return (
            <tr onClick={this._select}>
                <td><i className={iconClass}></i></td>
                <td>{this.props.data.name}</td>
                <td>{this.props.data.email}</td>
                <td>{_l(this.props.data.user_type)}</td>
            </tr>
        )
    }
}

class UserGrid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            limit: 10,
            offset: 0,
            data: [],
            selected: [],
            selSource: []
        }
    }

    componentWillMount() {
        this._query(0);
    }

    _query = (offset) => {
        let query = {
            aid: {eq: window.user.account_id},
            status: {eq: "ACTIVE"},
            role: {neq: "BOT"}
        };
        ewars.tx("com.ewars.query", ['sso', ['id', 'name', 'email'], query, null, this.state.limit, offset, null])
            .then((resp) => {
                this.setState({
                    data: resp,
                    offset: offset
                })
            })
    };

    _onChange = (prop, value) => {
        this.setState({
            [prop]: value
        })
    };

    _next = () => {
        this._query(this.state.offset + this.state.limit);
    };

    _prev = () => {
        this._query(this.state.offset - this.state.limit);
    };

    _onSelect = (user) => {
        console.log(user);

        let isIn = false;

        let selected = this.state.selected;
        if (selected.indexOf(user.id) >= 0) {
            isIn = true;
            selected.splice(selected.indexOf(user.id), 1);
        } else {
            selected.push(user.id);
        }

        let newSelect = [];
        if (isIn) {
            this.state.selSource.forEach((item) => {
                if (item.id != user.id) {
                    newSelect.push(item);
                }
            })
        } else {
            newSelect = ewars.copy(this.state.selSource);
            newSelect.push(user);
        }

        this.setState({
            selected: selected,
            selSource: newSelect
        })

    };

    _setSelected = () => {
        this.props.onSelected(this.state.selSource)
    };

    render() {
        let label = "Select";
        if (this.state.selected.length > 0) {
            label = `Add ${this.state.selected.length} users`;
        }
        return (
            <Layout>
                <Row>
                    <Cell style={{padding: 8}}>
                        <TextField
                            name="search"
                            placeholder="Search..."
                            value={this.state.search}
                            onUpdate={this._onChange}/>
                    </Cell>
                </Row>
                <Row>
                    <Cell>
                        <table width="100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>User Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.data.map((item) => {
                                return (
                                    <UserGridRow
                                        selected={this.state.selected}
                                        onSelect={this._onSelect}
                                        data={item}/>
                                )
                            })}
                            </tbody>
                        </table>
                    </Cell>
                </Row>
                <Toolbar>
                    <div className="btn-group">
                        <ewars.d.Button
                            icon="fa-save"
                            color="green"
                            onClick={this._setSelected}
                            label={label}/>
                        <ewars.d.Button
                            icon="fa-times"
                            color="red"
                            onClick={this.props.onCancel}
                            label="Close"/>
                    </div>

                    <div className="btn-group pull-right" style={{marginRight: 8}}>
                        <ewars.d.Button
                            onClick={this._prev}
                            icon="fa-chevron-left"/>
                        <ewars.d.Button
                            onClick={this._next}
                            icon="fa-chevron-right"/>
                    </div>
                </Toolbar>
            </Layout>
        )
    }
}

export default UserGrid;