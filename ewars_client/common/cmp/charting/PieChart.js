import * as d3 from "d3";

import RangeUtils from "../../utils/RangeUtils";


const SORT_MAP = {
    CAT_NAME: "name",
    CAT_NAME_DESC: "name",
    VALUE_ASC: "value",
    VALUE_DESC: "value"
}

class PieChart {
    constructor(el, config, additions) {
        this._id = ewars.utils.uuid();
        this._config = config;
        this._el = el;

        this._el.style.position = 'relative';

        this._reportDate = additions.report_date || null;
        this._lid = additions.location_id || null;
        this._token = additions.token || null;

        this._totalItems = 0;
        this._loadedItems = 0;
        this._margins = {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
        };

        // Legend settings
        this._legendRectSize = 18;
        this._legendSpacing = 4;

        // D3 properties
        this._svg = null;

        this._margin = 10;

        let bounds = el.getBoundingClientRect();
        this._width = bounds.width - (this._margins.left + this._margins.right);
        this._height = bounds.height - (this._margins.top + this._margins.bottom);

        this._radius = Math.min(this._width, this._height) / 2;
        this._innerRadius = 0;

        this._data = [];

        this._setup();
    }

    _setup = () => {

        this._color = d3.scaleOrdinal(d3.schemeCategory20b);

        this._svg = d3.select(this._el)
            .append("svg")
            .attr("width", this._width)
            .attr("height", this._height)
            .style("margin-top", this._margins.top)
            .style("margin-left", this._margins.left)
            .append("g")
            .attr("transform", "translate(" + (this._width / 2) + "," + (this._height / 2) + ")");

        this.tooltip = d3.select(this._el)
            .append("div")
            .attr("class", "d3-tooltip");

        this.tooltip.append("div").attr("class", "label");
        this.tooltip.append("div").attr("class", "count");
        this.tooltip.append("div").attr("class", "percent");

        this._arc = d3.arc()
            .innerRadius(0)
            .outerRadius(this._radius);

        this._pie = d3.pie()
            .value(function (d) {
                return d[1].data || 0;
            })
            .sort(null);

        this._path = this._svg.selectAll("path")
            .data(this._pie(this._data))
            .enter()
            .append("path")
            .attr('d', this._arc)
            .attr("stroke", "#FFFFFF")
            .attr("stroke-width", 2)
            .attr('fill', (d, i) => {
                return d.data[0].colour || this._color(i);
            });

        this._legend = d3.select(this._el)
            .select("svg")
            .append("g")
            .attr("transform", () => {
                let w = 0, h = 0;
                switch(this._config.legend_position) {
                    case "TC":
                        w = this._width / 2;
                        h = (this._legendRectSize * this._config.series.length);
                        break;
                    case "TL":
                        h = (this._legendRectSize * this._config.series.length);
                        break;
                    case "TR":
                        h = (this._legendRectSize * this._config.series.length);
                        w = this._width - this._legendRectSize;
                        break;
                    case "ML":
                        h = this._height / 2;
                        break;
                    case "MC":
                        h = this._height / 2;
                        w = this._width / 2;
                        break;
                    case "MR":
                        h = this._height / 2;
                        w = this._width;
                        break;
                    case "BL":
                        h = this._height;
                        break;
                    case "BC":
                        w = this._width / 2;
                        h = this._height;
                        break;
                    case "BR":
                        w = this._width;
                        h = this._height;
                        break;
                    default:
                        break;
                }
                return `translate(${w},${h})`;
            })

        this._legendItems = this._legend
            .selectAll(".legend")
            .data(this._config.series)
            .enter();

        this._legendItems
            .append("g")
            .attr("class", "legend")
            .attr("transform", (d, i) => {
                let height = this._legendRectSize + this._legendSpacing;
                let offset = height * this._config.series.length / 2;
                let horz = -2 * this._legendRectSize;
                let vert = i * height - offset;
                return 'translate(' + horz + "," + vert + ")";
            });

        this._legendItems
            .selectAll("g")
            .append("rect")
            .attr("width", this._legendRectSize)
            .attr("height", this._legendRectSize)
            .style("fill", function(d) {
                return d.colour || "#333333";
            })
            .style("stroke", function(d) {
                return d.colour || "#333333";
            });

        this._legendItems
            .selectAll("g")
            .append("text")
            .attr('x', this._legendRectSize + this._legendSpacing)
            .attr("y", this._legendRectSize - this._legendSpacing)
            .text(function (d) {
                return __(d.title)
            })


        this._query();
        // this._render();
    }

    _query = () => {

        (this._config.sources || this._config.series).forEach((src, index) => {
            let endDate = RangeUtils.process(src.period[1], this._reportDate),
                startDate = RangeUtils.process(src.period[0], this._reportDate, endDate);

            let query = {
                uuid: ewars.utils.uuid(),
                type: src.sources.length > 1 ? "SLICE_COMPLEX" : "SLICE",
                indicator: src.indicator,
                interval: "DAY",
                start_date: startDate,
                end_date: endDate,
                formula: src.formula || null,
                ...this._getSeries(src),
                location: this._configLocation(src),
                reduction: src.reduction || "SUM"
            };


            ewars._queue.push("/arc/analysis", query, this._token)
                .then((resp) => {
                    this._data.push([src, resp]);

                    this._renderSlice(src, resp);
                })

        })

    };

    _renderSlice = (slice, data) => {


        this._path.data(this._pie(this._data))
            .enter()
            .append("path")
            .attr('d', this._arc)
            .attr('fill', (d, i) => {
                return d.data[0].colour || this._color(i);
            })
            .on("mouseover", (d) => {
                let total = d3.sum(this._data.map((item) => {
                    return item[0].data;
                }))
                let percent = Math.round(1000 * d.data[0].data / total) / 10;
                this.tooltip.select(".label").html(__(d.data[0].title));
                this.tooltip.select(".value").html(d.data[0].data);
                this.tooltip.select(".percent").html(percent + "%");
                this.tooltip.style("display", "Block");
            })
            .on("mousemove", (d) => {
                this.tooltip.style("top", (d3.event.layerY + 10) + "px")
                    .style("left", (d3.event.layerX + 10) + "px");
            })
            .on("mouseout", (d) => {
                this.tooltip.style("display", "none");
            })


    };

    _configLocation = (src) => {
        switch (src.loc_spec) {
            case "REPORT_LOCATION":
                return this._locationId;
            case "GENERATOR":
                return {
                    parent_id: src.generator_locations_parent,
                    site_type_id: src.generator_location_type,
                    status: src.generator_location_status || "ACTIVE"
                };
            case "GROUP":
                return {
                    agg: src.group_output,
                    groups: src.group_ids
                };
            default:
                return src.location;
        }
    };

    _getSeries = (src) => {
        if (src.sources.length <= 1) {
            let item = src.sources[0];
            switch (item[0]) {
                case "I":
                    if (item[3].dimension) {
                        return {
                            indicator: {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            }
                        }
                    } else {
                        return {
                            indicator: {
                                uuid: item[1]
                            }
                        };
                    }
                case "F":
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    }
                default:
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    };
            }
        } else {
            return {
                series: src.sources.map((item) => {
                    switch (item[0]) {
                        case "I":
                            if (item[3].dimension) {
                                return {
                                    ...item[3],
                                    variable_name: item[2],
                                    uuid: item[1]
                                }
                            } else {
                                return {
                                    ...item[3],
                                    uuid: item[1],
                                    variable_name: item[2]
                                }
                            }
                        case "F":
                            return {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            };
                        default:
                            return {
                                ...item[3],
                                uuid: item[1]
                            };
                    }
                })
            }
        }
    };

    _render = () => {

        let path = this._svg.selectAll("path")
            .data(pie(dataset))
            .enter()
            .append("path")
            .attr('d', arc)
            .attr('fill', function (d, i) {
                return color(d.data.label);
            });

    };
}

export default PieChart;