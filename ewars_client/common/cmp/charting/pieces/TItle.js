import * as d3 from "d3";

class Title {
    constructor(props) {
        this._position = props.position;
        this._title = props.title;
        this._subtitle = props.subtitle;
        this._svg = props.svg;
        this._height = props.height;
        this._width = props.width;
        this._style = props.style || {};

        this.render();
    }

    render() {
        let title = d3.select(this._svg)
            .append("g");


        let titleText = title.append("text")
            .text(this._title);

        let subtitleText = title.append("text")
            .text(this._subtitle);

        switch(this._position) {
            case "TL":
                title.attr("transform", `translate(0,0)`);
                break;
            case "TC":
                title.attr("transform", `translate(${this._width / 2},0)`);
                break;
            case "TR":
                title.attr('transform', `translate(${this._width},0)`);
                break;
            case "ML":
                title.attr("transform", `translate(0, ${this._height / 2})`);
                break;
            case "MR":
                title.attr("transform", `translate(${this._width},${this._height / 2})`);
                break;
            case "BL":
                title.attr("transform", `translate(0, ${this._height})`);
                break;
            case "BC":
                title.attr("transform", `translate(${this._width / 2},${this._height})`);
                break;
            case "BR":
                title.attr('transform', `translate(${this._width},${this._height})`);
                break;
            default:
                title.attr("transform", `translate(${this._width / 2},0)`);
                break;
        }
    }
}

export default Title;