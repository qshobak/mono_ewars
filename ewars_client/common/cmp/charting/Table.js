const STYLES = {
    HEADER: {
        background: "#333333",
        color: "#F2F2F2",
        padding: "8px",
        textAlign: "left"
    },
    CELL: {
        background: "#FFFFFF",
        color: "#333333",
        padding: "8px",
        textAlign: "left"
    },
    ROW_HEADER: {
        background: "#333333",
        color: "#F2F2F2",
        padding: "8px",
        textAlign: "right"
    }
}

class Header extends React.Component {
    render() {
        return (
            <th style={STYLES.HEADER}>Test</th>
        )
    }
}

class Cell extends React.Component {
    render() {
        return (
            <td style={STYLES.CELL}>VALUE</td>
        )
    }
}

class RowHeader extends React.Component {
    render() {
        return (
            <th style={STYLES.ROW_HEADER}>RowHeader</th>
        )
    }
}


class Table extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    copmonentWillMount() {
        this._query();
    }

    _query = () => {

    };

    render() {
        console.log(this.props.data);
        return (
            <table width="100%">
                <thead>
                <tr>
                    <td></td>
                    <Header/>
                    <Header/>
                    <Header/>
                    <Header/>
                    <Header/>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <RowHeader/>
                    <Cell/>
                    <Cell/>
                    <Cell/>
                    <Cell/>
                    <Cell/>
                </tr>
                </tbody>

            </table>
        )
    }
}

export default Table;