import * as d3 from "d3";

/**
 * Represents a range of dates
 */
class DateRange {
    constructor(start_date, end_date, interval) {
        this._start_date = start_date;
        this._end_date = end_date;
        this._interval = interval;
    }
}

const _computeHash = (data) => {
    let hash = 0, i, chr;
    if (data.length == 0) return hash;
    for (let i = 0; i < data.length; i++) {
        let chr = data.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32 bit integer
    }
    return hash;
}


/**
 * Represents a row of data
 */
class Row {
    constructor(props) {
        this.type = props[0];
        this.id = props[1];
        this.name = props[2];
        this.config = props[3] || {};
        this._index = 0;

        this._location = null;
        this._indicator = null;
        this._sum = null;
        this._mean = null;
        this._high = -Infinity;
        this._low = Infinity;

        this._start_date  = this.config.start_date || new Date().toISOString();
        this._end_date = this.config.end_date || new Date().toISOString();

        this._loaded = false;
        this._data = {};

        this._id = props[props.length - 1];

        this._load();
    }

    _toString = (data) => {
        let dt = this.config;
        if (data) dt = data;
        let hash = "";
        Object.keys(dt).forEach((item) => {
            hash += item + ":" + JSON.stringify(dt[item]) + ",";
        })
        return hash;
    }

    update = (data) => {
        let newHash = this._toString(data);
        console.log(newHash, this._toString());
        if (newHash != this._toString()) {
            console.log("DIFFED");
        }
    }

    high = () => {
        return this._high;
    };

    low = () => {
        return this._low;
    };

    /**
     * if value is undefined, return prop, otherwise set
     * @param prop
     * @param value
     */
    attr = (prop, value) => {
        if (prop == "locationName") {
            return __(this._location.name || "Unknown");
        }

        if (prop == "sourceName") {
            return __(this._indicator.name || "Unknown");
        }

        if (prop == "sum") {
            return this._sum;
        }

    };

    getSum = () => {
        return this._sum;
    };

    getMean = () => {
        return this._mean;
    };

    /**
     * Get as time series
     * @returns {Array}
     */
    getSeries = (targets) => {
        let series = [];
        for (var n in this._data) {
            series.push([
                n,
                this._data[n]
            ])
        }

        series = series.sort((a, b) => {
            if (a[0] > b[0]) return 1;
            if (a[0] < b[0]) return -1;
            return 0;
        })

        return series;
    };

    _load = () => {
        let query = {
            interval: "DAY",
            start_date: new Date().toISOString(),
            end_date: new Date().toISOString(),
            type: "SERIES",
            location: window.user.clid,
            indicator: this.id
        };

        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                this._indicator = resp.indicator;
                this._location = resp.location;
                this._process(resp.data);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _process = (data) => {
        this._sum = 0;
        data.forEach((item) => {
            let val = parseFloat(item[1] || 0.0);
            this._sum += val;
            this._data[item[0]] = val;
            if (val > this._high) this._high = val;
            if (val < this._low) this._low = val;
        })

        this._mean = this._sum / data.length;
        ewars.emit("RENDER");
    };

    getId = () => {
        return this._id;
    };

    getName = () => {
        switch (this.type) {
            case "I":
                if (ewars.g.indicators[this.id])
                    return __(ewars.g.indicators[this.id].name);
                break;
            default:
                return "Unknown";
                break;
        }
    };

    setOrder = (index) => {
        this._index = index;
    }
}

/**
 * Represents a column of data
 */
class Column {
    constructor(props) {
        this.type = props[0];
        this.id = props[1];
        this.name = props[2];
        this.config = props[3] || {};
        this._index = 0;

        this._id = props[props.length - 1];
    }

    getId = () => {
        return this._id;
    };

    getName = () => {
        switch (this.type) {
            case "I":
                if (ewars.g.indicators[this.id])
                    return __(ewars.g.indicators[this.id].name);
                break;
            default:
                return "Unknown";
                break;
        }
    };

    setOrder = (index) => {
        this._index = index;
    }
}


class Omni {
    constructor(el) {
        this._el = el;
        this._rows = {};
        this._cols = {};

        this._x_axes = [];
        this._y_axes = [];

        this._period = [];

        this.width = 0;
        this.height = 0;

        this._reduction = "SUM";
        this._style = "COLUMN";
        this._multiple = false;
        this._multiDir = null;
        this._multiLocation = false;

        // x Axis
        this._xTicks = [];
        this._xTickFormat = null;

        // y Axis
        this._yTicks = [];
        this._yTickFormat = null;

        // Analysis
        this._lines = [];
        this._bands = [];
        this._trends = [];
        this._forecasts = [];
        this._distributions = [];
        this._boxes = [];

        this._getSize();
        // TODO: Add window resize event listener to reset size

        this._svg = d3.select(el)
            .append("svg")
            .attr("transform", "translate(0,0)")
            .attr("width", this.width)
            .attr("height", this.height);

        this._root = this._svg
            .append("g")
            .attr("transform", `translate(0,0)`);

        ewars.subscribe("RENDER", this._render);

    }

    _getSize = () => {
        let bounds = this._el.getBoundingClientRect();
        this.width = bounds.width;
        this.height = bounds.height;
    };

    _kind = () => {
        if (Object.keys(this._cols).length <= 0 && Object.keys(this._rows).length > 0) {
            // We don't have any colums here. So we're going to default out to a bar chart
            this._reduction = "SUM";
            this._style = "COLUMN";
            this._multiple = false;
            this._multiLocation = false;
        }

        if (Object.keys(this._cols).length > 0 && Object.keys(this._rows).length > 0) {
            // WE have columsn and rows, default to seeries
            this._reduction = "SUM";
            this._style = "SERIES";
            this._multiple = false;
            this._multiLocation = false;

        }
    };

    _render = () => {
        if (this._style == "COLUMN") {
            let y = d3.scaleLinear()
                .range([this.height - 40, 0]);

            let max = -Infinity, min = Infinity;
            let names = [];
            for (let n in this._rows) {
                if (this._rows[n].high() > max) max = this._rows[n].high();
                if (this._rows[n].low() < min) min = this._rows[n].low();
                names.push(this._rows[n].attr("sourceName"));
            }

            if (max <= 0) max = 1;
            if (min <= 0) min = 0;
            y.domain([min, max]).clamp(true);
            let yAxis = d3.axisLeft(y);

            if (this._yAxis) this._yAxis.remove();
            this._yAxis = this._root
                .append("g")
                .attr("transform", "translate(30,10)")
                .call(yAxis);


            let x = d3.scaleBand()
                .range([0, this.width - 30])
                .domain(names);

            let xAxis = d3.axisBottom(x);

            if (this._xAxis) this._xAxis.remove();
            this._xAxis = this._root
                .append("g")
                .attr("transform", `translate(30, ${this.height - 30})`)
                .call(xAxis);

            let rows = Object.keys(this._rows).map((item) => {
                return [this._rows[item].attr("sourceName"), Math.random(0.1,1)];
            })

            if (this._columns) this._columns.remove();

            this._columns = this._svg
                .append("g")
                .attr("transform", `translate(30,10)`);

            this._columns
                .selectAll("rect")
                .data(rows)
                .enter()
                .append("rect")
                .attr("x", function (d) {
                    console.log("HERE")
                    return x(d[0]) + 10;
                })
                .attr("width", function(d) {
                    return x.bandwidth() - 10;
                })
                .attr("y", function (d) {
                    return y(d[1]);
                })
                .attr("height", (d) => {
                    return this.height - y(d[1]) - 40
                })
        }

    };

    startDate = (newDate) => {

    };

    endDate = (endDate) => {

    };

    update = (rows, cols, settings) => {
        if (!settings.dd) {
            this._period = [new Date(), new Date()];
        }

        if (rows) {
            rows.forEach((row, index) => {
                let uuid = row[row.length - 1];
                if (this._rows[uuid]) {
                    this._rows[uuid].update(row[row.length - 2])
                } else {
                    let r = new Row(row);
                    r.setOrder(index);
                    this._rows[r.getId()] = r;
                }
            })
        }

        if (cols) {
            cols.forEach((col, index) => {
                let uuid = col[col.length - 1];
                if (this._cols[uuid]) {
                    this._cols[uuid].update(col[col.length - 2]);
                } else {
                    let c = new Column(col);
                    c.setOrder(index);
                    this._cols[c.getId()] = c;
                }
            })
        }

        this._kind();
    }


}

export default Omni;