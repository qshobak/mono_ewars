import Moment from "moment";
import * as d3 from "d3";

import Title from "./pieces/Title";
import Legend from "./pieces/Legend";
import Scale from "./pieces/Scale";

import RangeUtils from "../../utils/RangeUtils";


class SeriesChart {
    constructor(el, config, additions) {
        this._el = el;
        this._config = config;

        this._margins = {
            top: 20,
            right: 50,
            bottom: 30,
            left: 50
        }

        let bounds = this._el.getBoundingClientRect();
        this._width = bounds.width - (this._margins.left + this._margins.right);
        this._height = bounds.height - (this._margins.top + this._margins.bottom);

        this._totalSeries = this._config.series.length;
        this._loaded = 0;

        // Create root svg element
        this._svg = d3.select(this._el)
            .append("svg")
            .attr("width", bounds.width)
            .attr("height", bounds.height);


        // Create g to contain line series
        this._series = this._svg
            .append("g")
            .attr("transform", `translate(${this._margins.left},${this._margins.top})`)
            .attr("height", this._height)
            .attr("width", this._width);


        this._data = [];
        this._sources = [];
        this._query();
        // this._setup();

    }

    _getSeries = (src) => {
        if (src.sources.length <= 1) {
            let item = src.sources[0];
            switch (item[0]) {
                case "I":
                    if (item[3].dimension) {
                        return {
                            indicator: {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            }
                        }
                    } else {
                        return {
                            indicator: {
                                uuid: item[1]
                            }
                        };
                    }
                case "F":
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    }
                default:
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    };
            }
        } else {
            return {
                series: src.sources.map((item) => {
                    switch (item[0]) {
                        case "I":
                            if (item[3].dimension) {
                                return {
                                    ...item[3],
                                    variable_name: item[2],
                                    uuid: item[1]
                                }
                            } else {
                                return {
                                    ...item[3],
                                    uuid: item[1],
                                    variable_name: item[2]
                                }
                            }
                        case "F":
                            return {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            };
                        default:
                            return {
                                ...item[3],
                                uuid: item[1]
                            };
                    }
                })
            }
        }
    };

    _configLocation = (src) => {
        switch (src.loc_spec || src.location_spec) {
            case "REPORT_LOCATION":
                return this._locationId;
            case "GENERATOR":
                return {
                    parent_id: src.location,
                    site_type_id: src.site_type_id,
                    status: src.generator_location_status || "ACTIVE"
                };
            case "GROUP":
                return {
                    agg: src.group_output,
                    groups: src.group_ids
                };
            default:
                return src.location;
        }
    };

    _checkComplete = () => {
        if (this._loaded >= this._totalSeries) {
            this._parse();
            this._render();
            ewars.emit("WIDGET_LOADED");
        }
    };

    _parseData = (data) => {
        if (data.data && !data.location) {
            data.data.forEach((loc) => {
                loc.data = loc.data.map((item) => {
                    return [Moment.utc(item[0]).toDate(), item[1]];
                })
            })
            return data;
        } else {
            return {
                ...data,
                data: data.data.map((item) => {
                    return [Moment.utc(item[0]).toDate(), item[1]]
                })
            }
        }
    };

    _query = () => {
        let seriesIndex = 0;
        let totalIndexes = 0;
        this._config.series.forEach((series, index) => {
            let end_date = RangeUtils.process(this._config.period[1], this._reportDate),
                start_date = RangeUtils.process(this._config.period[0], this._reportDate, end_date),
                targetAxis = 0;


            if (series.override == "OVERRIDE") {
                end_date = RangeUtils.process(series.period[1], this._report_date);
                start_date = RangeUtils.process(series.period[0], this._report_date, options.end_date);
            }

            let query = {
                start_date: start_date,
                end_date: end_date,
                type: series.sources.length > 1 ? "SERIES_COMPLEX" : "SERIES",
                interval: this._config.interval,
                formula: series.formula,
                series: series.series,
                location: this._configLocation(series),
                fill: true,
                ...this._getSeries(series)
            };

            series.index = index;

            ewars._queue.push("/arc/analysis", query)
                .then(resp => {
                    if (resp) {
                        this._loaded++;
                        resp.uuid = series.uuid;
                        this._data.push(this._parseData(resp));

                        this._sources[series.uuid] = series;
                        this._checkComplete();

                    } else {

                        this._loaded++;
                        this._checkComplete();
                    }
                })
                .catch((err) => {
                    this._loaded++;
                    this._checkComplete();
                })

        }, this);
    };

    _parse = () => {

        this._results = [];
        this._dates = [];
        this._locations = {};
        this._values = [];

        this._data.forEach((item) => {
            if (item.data && !item.location) {
                // multiple locations
                item.data.forEach((subItem) => {
                    this._locations[subItem.location.uuid] = subItem.location;
                    subItem.uuid = item.uuid;
                    this._results.push(subItem);
                })
            } else {
                this._locations[item.location.uuid] = item.location;
                this._results.push(item);
            }
        })
    };

    _render = () => {
        // Parse x axis


        let xType, yType;

        xType = this._config.x[0].dimension;
        yType = this._config.y[0].dimension;

        const PLOT_TYPES = {
            "DEFAULT_VALUE": ["DEFAULT", null],
            "DEFAULT_LOCATION": ["SCATTER", null],
            "DEFAULT_DEFAULT": ["SCATTER", null],
            "VALUE_DEFAULT": ["BAR", "DATE"],
            "VALUE_LOCATION": ["BAR", "LOCATION"],
            "VALUE_VALUE": ["SCATTER", null],
            "LOCATION_DEFAULT": ["SCATTER", null],
            "LOCATION_VALUE": ["COLUMN", "LOCATION"],
            "LOCATION_LOCATION": ["SCATTER", null]
        }
        let [plotType, aggType] = PLOT_TYPES[`${xType}_${yType}`];


        switch (plotType) {
            case "DEFAULT":
                this._plotDefault(xType, yType);
                break;
            case "BAR":
                this._plotBar(aggType);
                break;
            case "COLUMN":
                this._plotColumn(aggType);
                break;
            case "SCATTER":
                this._plotScatter(aggType);
                break;
        }


    };



    _parseAxis = (conf, axes, type, aggType) => {
        let axis, scale;

        if (conf.dimension == "DEFAULT") {
            scale = d3.scaleTime();

            let low = new Date(9999, 1, 1), high = new Date(1970, 12, 31);
            this._results.forEach((dt) => {
                dt.data.forEach((item) => {
                    if (item[0] > high) high = item[0];
                    if (item[0] < low) low = item[0];
                })
            });
            scale.domain([low, high]);
        }

        if (conf.dimension == "LOCATION") {
            scale = d3.scaleBand()
                .paddingInner(0.05);

            let locs = [];
            this._results.forEach((item) => {
                locs.push(item.location.uuid);
            })

            scale.domain(locs);
        }

        if (conf.dimension == "VALUE") {
            scale = d3.scaleLinear();

            let low = Infinity, high = -Infinity;
            if (type == "DEFAULT") {

                this._results.forEach((dt) => {
                    dt.data.forEach((item) => {
                        if (item[1]) {
                            if (item[1] < low) low = item[1];
                            if (item[1] > high) high = item[1];
                        }
                    })
                });
            }

            if (type == "BAR") {
                if (aggType == "LOCATION") {
                    this._results.forEach((item) => {
                        let value = 0;
                        item.data.forEach((dt) => {
                            if (dt[1]) value += dt[1];
                        })
                        if (value < low) low = value;
                        if (value > high) high = value;
                    })
                } else {
                    let data = {};
                    this._results.forEach((dt) => {
                        dt.data.forEach((item) => {
                            if (item[1]) {
                                if (data[item[0]]) {
                                    data[item[0]] += item[1];
                                } else {
                                    data[item[0]] = item[1]
                                }
                            }
                        })
                    })

                    for (let i in data) {
                        if (data[i] > high) high = data[i];
                        if (data[i] < low) low = data[i];
                    }

                }
            }

            scale.domain([0, this._config.max || high]).clamp(true);
        }

        if (axes == "x") {
            scale.rangeRound([0, this._width]);
        } else {
            scale.rangeRound([this._height, 0]);
        }

        if (conf.pos == "BOTTOM") {
            axis = d3.axisBottom(scale);
        } else if (conf.pos == "TOP") {
            axis = d3.axisTop(scale);
        } else if (conf.pos == "LEFT") {
            axis = d3.axisLeft(scale);
        } else {
            axis = d3.axisRight(scale);
        }

        if (conf.dimension == "LOCATION") {
            axis.tickFormat((d) => {
                return __(this._locations[d].name)
            })
        }

        if (conf.dimension == "DEFAULT") {
            if (this._config.interval == "WEEK") {
                axis.tickFormat((d) => {
                    return Moment(d).format("[W]W GGGG")
                })
            }

            if (this._config.interval == "DAY") {
                axis.tickFormat((d) => {
                    return d.toISOString();
                })
            }

            if (this._config.interval == "YEAR") {
                axis.tickFormat((d) => {
                    return Moment(d).format("YYYY")
                })
            }

            if (this._config.interval == "MONTH") {
                axis.tickFormat((d) => {
                    return Moment(d).format("YYYY-MM-DD")
                })
            }
        }

        return [scale, axis, conf.pos, conf.dimension];

    };

    _plotScatter = (aggType) => {
        let data = [];
        let results = [];

        if (aggType == "LOCATION") {
            this._results.forEach((item) => {
                let value = 0;
                item.data.forEach((dt) => {
                    if (dt[1]) value += dt[1];
                })
                data.push([item.location.uuid, value]);
            })
            results = data;
        } else {
            data = {};
            this._results.forEach((result) => {
                result.data.forEach((dt) => {
                    if (dt[1]) {
                        results.push([dt[0], dt[1], result.location.uuid]);
                    }
                })
            })
        }

        let [xScale, xAxis, xPos, xType] = this._parseAxis(this._config.x[0], "x", "SCATTER", aggType);
        let [yScale, yAxis, yPos, yType] = this._parseAxis(this._config.y[0], "y", "SCATTER", aggType);

        this._yAxis = this._series
            .append("g")
            .call(yAxis);

        if (yPos == "RIGHT") {
            this._yAxis.attr("transform", `translate(${this._width},0)`)
        }

        this._xAxis = this._series
            .append("g")
            .call(xAxis);

        if (xPos == "BOTTOM") {
            this._xAxis.attr("transform", `translate(0, ${this._height})`)
        }

        let dScale = d3.scaleSqrt()
            .domain([0, d3.max(results, d => d[1])])
            .range([0, 10])

        this._series.append("g")
            .selectAll("circle")
            .data(results)
            .enter()
            .append("circle")
            .attr("cx", (d) => {
                if (xType == "DEFAULT") {
                    let dt = Moment(d[0]);
                    return xScale(dt.toDate());
                } else if (xType == "LOCATION") {
                    return xScale(d[2]) + xScale.bandwidth() / 2;
                } else {
                    return xScale(d[1]);
                }
            })
            .attr("cy", (d) => {
                if (yType == "DEFAULT") {
                    let dt = Moment(d[0]);
                    return yScale(dt.toDate());
                } else if (yType == "LOCATION") {
                    return yScale(d[2]) + yScale.bandwidth() / 2;
                } else {
                    return yScale(d[1]);
                }
            })
            .attr("r", (d) => {
                return dScale(d[1]);
            })
            .attr("fill", "steelblue")
    };

    _plotBar = (aggType) => {

        let data = [];
        let results = [];

        if (aggType == "LOCATION") {
            this._results.forEach((item) => {
                let value = 0;
                item.data.forEach((dt) => {
                    if (dt[1]) value += dt[1];
                })
                data.push([item.location.uuid, value]);
            })
            results = data;
        } else {
            data = {};
            this._results.forEach((dt) => {
                dt.data.forEach((item) => {
                    if (item[1]) {
                        if (data[item[0]]) {
                            data[item[0]] += item[1];
                        } else {
                            data[item[0]] = item[1]
                        }
                    }
                })
            })

            for (let i in data) {
                results.push([i, data[i]])
            }

        }

        let [xScale, xAxis, xPos, xType] = this._parseAxis(this._config.x[0], "x", "BAR", aggType);
        let [yScale, yAxis, yPos, yType] = this._parseAxis(this._config.y[0], "y", "BAR", aggType);

        this._yAxis = this._series
            .append("g")
            .call(yAxis);

        if (yPos == "RIGHT") {
            this._yAxis.attr("transform", `translate(${this._width},0)`)
        }

        this._xAxis = this._series
            .append("g")
            .call(xAxis);

        if (xPos == "BOTTOM") {
            this._xAxis.attr("transform", `translate(0, ${this._height})`)
        }

        this._series.append("g")
            .selectAll("rect")
            .data(results)
            .enter()
            .append("rect")
            .attr("x", 0)
            .attr("y", (d) => {
                if (yType == "DEFAULT") {
                    let dt = Moment(d[0]);
                    return yScale(dt.toDate());
                } else {
                    return yScale(d[0]);
                }
            })
            .attr("width", (d) => {
                return xScale(d[1]);
            })
            .attr("height", (d) => {
                if (yType == "DEFAULT") {
                    return 1;
                } else {
                    return yScale.bandwidth();
                }
            })
            .attr("fill", (d) => {
                return "steelblue";
            })
    };

    _plotColumn = (aggType) => {
        let data = [];
        let results = [];

        if (aggType == "LOCATION") {
            this._results.forEach((item) => {
                let value = 0;
                item.data.forEach((dt) => {
                    if (dt[1]) value += dt[1];
                })
                data.push([item.location.uuid, value]);
            })
            results = data;
        } else {
            data = {};
            this._results.forEach((dt) => {
                dt.data.forEach((item) => {
                    if (item[1]) {
                        if (data[item[0]]) {
                            data[item[0]] += item[1];
                        } else {
                            data[item[0]] = item[1]
                        }
                    }
                })
            })

            for (let i in data) {
                results.push([i, data[i]])
            }

        }

        let [xScale, xAxis, xPos, xType] = this._parseAxis(this._config.x[0], "x", "BAR", aggType);
        let [yScale, yAxis, yPos, yType] = this._parseAxis(this._config.y[0], "y", "BAR", aggType);

        this._yAxis = this._series
            .append("g")
            .call(yAxis);

        if (yPos == "RIGHT") {
            this._yAxis.attr("transform", `translate(${this._width},0)`)
        }

        this._xAxis = this._series
            .append("g")
            .call(xAxis);

        if (xPos == "BOTTOM") {
            this._xAxis.attr("transform", `translate(0, ${this._height})`)
        }


        this._series.append("g")
            .selectAll("rect")
            .data(results)
            .enter()
            .append("rect")
            .attr("x", (d) => {
                if (xType == "DEFAULT") {
                    let dt = Moment(d[0]);
                    return xScale(dt.toDate());
                } else {
                    return xScale(d[0]);
                }
            })
            .attr("y", (d) => {
                return yScale(d[1]);
            })
            .attr("width", (d) => {
                return xScale.bandwidth();
            })
            .attr("height", (d) => {
                if (yType == "DEFAULT") {
                    let dt = Moment(d[0]);
                    return yScale(dt.toDate());
                } else {
                    return this._height - yScale(d[1]);
                }
            })
            .attr("fill", (d) => {
                return "steelblue";
            })
    };

    _plotDefault = () => {
        let dates = [];
        this._results.forEach((item) => {
            item.data.forEach((dt) => {
                if (dates.indexOf(dt[0]) < 0) dates.push(dt[0])
            })
        })

        let [xScale, xAxis, xPos, xType] = this._parseAxis(this._config.x[0], "x", "DEFAULT");
        let [yScale, yAxis, yPos, yType] = this._parseAxis(this._config.y[0], "y", "DEFAULT");

        this._yAxis = this._series
            .append("g")
            .call(yAxis);

        if (yPos == "RIGHT") {
            this._yAxis.attr("transform", `translate(${this._width},0)`)
        }

        this._yAxis
            .append("text")
            .attr("fill", "#000")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("Number");

        this._xAxis = this._series
            .append("g")
            .call(xAxis);

        if (xPos == "BOTTOM") {
            this._xAxis.attr("transform", `translate(0, ${this._height})`)
        }


        let line = d3.line()
            .x((d) => {
                return xScale(d[0]);
            })
            .y((d) => {
                return yScale(parseFloat(d[1] || 0))

            });

        // Calculate box width;
        let perSize = this._width / dates.length;

        this._series
            .append("g")
            .selectAll("path")
            .data(this._results)
            .enter()
            .append("path")
            .attr("class", "d3-line")
            .attr("fill", "none")
            .attr("stroke", (d, i) => {
                let source = this._sources[d.uuid];
                return source.colour || "steelblue";
            })
            .attr("stroke-width", 0.5)
            .attr("d", (d) => {
                return line(d.data);
            });

        this._hlGroup = this._series.append("g");
        this._hlLine = this._hlGroup.append("line")
            .attr("x1", 100).attr("x2", 100)
            .attr("y1", 0).attr("y2", this._height)
            .attr("stroke", "#333333")
            .attr("stroke-width", 0.5)
            .attr("fill", "#000000");


        // Create markers
        this._results.forEach((series, index) => {
            this._series
                .append("g")
                .selectAll("circle")
                .data(series.data)
                .enter()
                .append("circle")
                .attr("cx", (d) => {
                    return xScale(d[0]);
                })
                .attr("cy", (d) => {
                    return yScale(parseFloat(d[1] || 0));
                })
                .attr("fill", () => {
                    let source = this._sources[series.uuid];
                    return source.colour || "steelblue";
                })
                .attr("r", 3);
        });


        // Create a "ghost" set of bars to act as the anchor for the vertical line,
        // hove
        this._series.append("g")
            .selectAll("rect")
            .data(dates)
            .enter()
            .append("rect")
            .attr("x", (d) => {
                return xScale(d) - perSize / 2;
            })
            .attr("y", 0)
            .attr("height", this._height)
            .attr("width", (d) => {
                return perSize;
            })
            .attr("fill", "transparent")
            .on("mouseenter", (d) => {
                let point = xScale(d);
                this._hlLine
                    .style("opacity", 1)
                    .attr("x1", point)
                    .attr("x2", point);

                let html = `<div class="popover">`;

                let results = [];
                let displayDate = Moment.utc(d).format("YYYY-MM-DD");
                this._results.forEach((item) => {
                    let points = item.data.filter((node) => {
                        return node[0] == d
                    })
                    if (points.length > 0) {
                        results.push([
                            __(item.location.name),
                            points[0][1]
                        ])
                    }

                });

                let dom = `<table><thead><tr><th colSpan="2">${__(this._config.title)}</th></tr></thead></table>`;
                this._hlGroup
                    .selectAll(".popover")
                    .append("div")
                    .html(dom)
                    .attr("class", "popover")
                    .style("left", 0)
                    .style("top", 0)
                    .style("position", "absolute")
            })
            .on("mousemove", (d) => {
                let point = xScale(d);
                this._hlLine
                    .style("opacity", 1)
                    .attr("x1", point)
                    .attr("x2", point);

            })
            .on("mouseleave", (d) => {
                this._hlLine.style("opacity", 0);

                this._hlGroup.selectAll(".popover").remove();
            })
            .on("click", (d) => {
            })


    }
}

export default SeriesChart;