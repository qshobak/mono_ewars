class Heatmap {
    constructor(el, definition, reportDate, locationId) {
        this._el = el;
        this.definition = definition;
        this.reportDate = reportDate;
        this.locationId = locationId;

        this._setup();
    }

    _setup = () => {
        this.bounds = this._el.getBoundingClientRect();
        this._width = this.bounds.width;
        this._height = this.bounds.height;

        let color = d3.scale.linear()
            .domain([0, 100])
            .range(["green", "blue", "orange", "red"]);


    }
}