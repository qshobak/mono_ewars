import * as d3 from "d3";
const DATA = [
    ["2017-01-01", 12],
    ["2017-01-02", 12],
    ["2017-01-03", 19],
    ["2017-01-04", 222],
    ["2017-01-05", 12],
    ["2017-01-06", 1],
    ["2017-01-07", 0],
    ["2018-01-01", 1]
]


class BulletChart {
    constructor(el, definition, reportDate, locationId) {
        this.definition = definition;
        this._el = el;
        this.reportDate = reportDate;
        this.locationId = locationId;



        this._setup();
    }

    _setup = () => {
        this.bounds = this._el.getBoundingClientRect();
        this._width = this.bounds.width;
        this._height = this.bounds.height;

        let margin = {
            top: 5,
            right: 40,
            bottom: 20,
            left: 120
        };

        let realWidth = this._width - margin.left - margin.right;
        let realHeight = this._height - margin.top - margin.bottom;

        let chart = d3.bullet()
            .width(realWidth)
            .height(realHeight);

        this._svg = d3.select(this._el)
            .append("svg")
            .attr("class", "bullet")
            .attr("width", this._width)
            .attr("height", this._height);

        this._group = this._svg.append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`)
            .call(chart);


    }
}

export default BulletChart;