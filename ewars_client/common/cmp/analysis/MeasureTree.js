const LABELS = {
    COMPLETENESS: 'Completeness',
    TIMELINESS: 'Timeliness',
    RECORDS: 'Number of records',
    RECORDS_EXPECTED: 'Records expected',
    RECORDS_TIMELY: 'Records on-time',
    RECORDS_LATE: 'Records late'
}

class FormNode extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false,
        root: false
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    componentWillMount() {
        if (this.props.root) {
            ewars.tx("com.ewars.query", ["form", ["id", "name"], {"status": {eq: "ACTIVE"}}, null, null, null, null])
                .then((resp) => {
                    ewars.g.FORMS = {};
                    resp.forEach(item => {
                        ewars.g.FORMS[item.id] = item.name;
                    })
                    this.setState({
                        data: resp,
                        show: true
                    })
                })
        }
    }


    _toggle = () => {
        if (this.state.data) {
            this.setState({
                show: !this.state.show
            })
        } else {
            // this.refs.icon.className = "fal fa-cog fa-spin";
            ewars.tx("com.ewars.form.measures", [this.props.data.id])
                .then((resp) => {
                    let sorted = resp.sort((a, b) => {
                        if (a[0] > b[0]) return 1;
                        if (a[0] < b[0]) return -1;
                        return 0;
                    })
                    resp.forEach((item) => {
                        ewars.g.measures[item[0] + "." + item[2]] = item;
                    })
                    this.setState({
                        data: resp,
                        show: true
                    })
                })
        }
    };

    render() {
        if (!this.state.data && this.props.root) return <div></div>;

        if (this.props.root) {
            let items;
            items = this.state.data.map(item => {
                return <FormNode data={item}/>;
            })

            return (
                <div className="section">
                    {items}
                </div>
            )
        }


        let label, icon = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
        if (this.props.root) label = "Form Dimensions";
        if (!this.props.root) label = __(this.props.data.name);

        let items;
        if (this.state.show) {
            items = this.state.data.map(item => {
                return <Node type="FIELD" data={item}/>
            })
        }

        if (this.state.show) {
            items.push(<Node type='COMPLETENESS' data={{fid: this.props.data.id}}/>);
            items.push(<Node type='RECORDS' data={{fid: this.props.data.id}}/>);
            items.push(<Node type="TIMELINESS" data={{fid: this.props.data.id}}/>);
            items.push(<Node type="RECORDS_LATE" data={{fid: this.props.data.id}}/>);
            items.push(<Node type="RECORDS_TIMELY" data={{fid: this.props.data.id}}/>);
            items.push(<Node type="RECORDS_EXPECTED" data={{fid: this.props.data.id}}/>);
        }

        return (
            <div className="node">
                <div className="node-handle" onClick={this._toggle}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="20px" style={{padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: "6px 5px"}}>{label}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.show ?
                    <div className="node-children">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }
}

class Node extends React.Component {
    static defaultProps = {
        type: null
    };

    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        let code, config = {};
        switch (this.props.type) {
            case "FIELD":
                code = "FIELD." + this.props.data[0] + "." + this.props.data[2];
                break;
            case "LOCATION":
                code = this.props.type;
                config.sti = this.props.data.id;
                break;
            default:
                code = this.props.type;
                config.id = null;
                break;
        }
        ewars.emit("SHOW_DROPS", "M");

        e.dataTransfer.setData('n', JSON.stringify({
            t: 'M',
            n: code,
            c: config,
            fid: this.props.data.fid || undefined
        }))
    };

    _onDragEnd = () => {
        ewars.emit('HIDE_DROPS');
    };

    render() {
        let label, icon;

        switch (this.props.type) {
            case "LOCATION":
                icon = "fal fa-map-marker";
                label = __(this.props.data.name);
                break;
            case "DATE":
                icon = "fal fa-calendar";
                label = __(this.props.data.name);
                break;
            case "FIELD":
                icon = "fal fa-list";
                label = __(this.props.data[4]);

                if (this.props.data[3] == "date") icon = "fal fa-calendar";
                if (this.props.data[3] == "text") icon = "fal fa-font";
                if (this.props.data[3] == "select") icon = "fal fa-list-alt";
                if (this.props.data[3] == "location") icon = "fal fa-map-marker";
                if (this.props.data[3] == "number") icon = "fal fa-hashtag";
                break;
            case "USER_TYPE":
                icon = "fal fa-group";
                label = __(this.props.data.name);
                break;
            case "SOURCE":
                icon = "fal fa-laptop";
                label = "Source Type";
                break;
            case "STATUS":
                icon = "fal fa-toggle-on";
                label = "Status";
                break;
            case "ORGANIZATION":
                icon = "fal fa-building";
                label = "Organization";
                break;
            case "USER":
                icon = "fal fa-user";
                label = "User";
                break;
            case 'RECORDS_LATE':
            case 'RECORDS_TIMELY':
            case 'RECORDS_EXPECTED':
            case 'RECORDS':
            case 'COMPLETENESS':
            case 'TIMELINESS':
                icon = 'fal fa-hashtag';
                label = LABELS[this.props.type];
                break;
            default:
                icon = "fal fa-hashtag";
                label = this.props.data.name;
                break;
        }

        return (
            <div className="node">
                <div className="node-handle">
                    <div className="node-control" draggable={true} onDragStart={this._onDragStart}
                         onDragEnd={this._onDragEnd}>
                        <ewars.d.Row>
                            <ewars.d.Cell width="20px">
                                <i className={icon}></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell>{label}</ewars.d.Cell>
                            <ewars.d.Cell width="20px" style={{display: "block"}}>
                                <div className="node-ctx">
                                    <i className="fal fa-caret-down"></i>
                                </div>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </div>
            </div>
        )
    }
}

class LocationTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            show: false
        }
    }

    _toggle = () => {
        if (this.state.data) {
            this.setState({
                show: !this.state.show
            })
        } else {
            ewars.tx("com.ewars.query", ["location_type", null, {status: {eq: "ACTIVE"}}, null, null, null, null, null])
                .then(resp => {
                    this.setState({
                        show: true,
                        data: resp
                    })
                })
        }
    };

    render() {
        let icon = "fal fa-caret-right";
        if (this.state.show) icon = "fal fa-caret-down";

        return (
            <div className="node">
                <div className="node-handle">
                    <ewars.d.Row>
                        <ewars.d.Cell width="20px" onClick={this._toggle}
                                      style={{padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            <div className="node-control" draggable={true}>
                                <ewars.d.Row>
                                    <ewars.d.Cell>
                                        Location
                                    </ewars.d.Cell>
                                    <ewars.d.Cell width="20px">
                                        <div className="node-ctx">
                                            <i className="fal fa-caret-down node-drop"></i>
                                        </div>
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.show ?
                    <div className="node-children">
                        {this.state.data.map(item => {
                            return <Node key={"LT" + item.id} data={item} type="LOCATION"/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class MeasureTree extends React.Component {
    static defaultPrpos = {
        draggable: true,
        onClick: null
    };

    constructor(props) {
        super(props)
    }

    _startDrag = (e) => {

    };

    _onVarDrag = (e) => {
        e.dataTransfer.setData("n", JSON.stringify(["V", null, null, {}]));
    };

    _onClick = (data) => {
        this.props.onClick(data);
    };

    _onVarClick = () => {
        this.props.onClick(["V", null, null, {}]);
    };

    render() {
        return (
            <ewars.d.Panel>
                <div className="plot-tree" style={{padding: "5px"}}>
                    <FormNode root={true}/>
                    <Node type="RECORDS" data={{name: "Number of Records"}}/>
                    <Node type="USERS" data={{name: "Number of Users"}}/>
                    <Node type="LOCATIONS" data={{name: "Number of Locations"}}/>
                    <Node type="LOCATION_TYPES" data={{name: "Number of Location Types"}}/>
                    <Node type="ALERTS" data={{name: "Number of Alerts"}}/>
                    <Node type="ALARMS" data={{name: "Number of Alarms"}}/>
                    <Node type="DEVICES" data={{name: "Number of Devices"}}/>
                    <Node type="FORMS" data={{name: "Number of Forms"}}/>
                    <Node type="ORGANIZATIONS" data={{name: "Number of Organizations"}}/>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default MeasureTree;