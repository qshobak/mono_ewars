class Widget extends React.Component {
    static defaultProps = {
        noHeader: false
    };

    constructor(props) {
        super(props)
    }

    render() {

        let icon;
        if (this.props.data.icon) {
            icon = <i className={"fal" + this.props.data.icon}></i>;
        }

        let headerStyle = {};
        if (this.props.data.headerColor) headerStyle.backgroundColor = this.props.data.headerColor;
        if (this.props.data.headerTextColor) headerStyle.color = this.props.data.headerTextColor;

        let bodyStyle = {};
        if (this.props.data.widgetBackgroundColor) bodyStyle.backgroundColor = this.props.data.widgetBackgroundColor;
        if (this.props.data.widgetTextColor) bodyStyle.color = this.props.data.widgetTextColor;

        return (
            <div className="widget">
                {!this.props.noHeader ?
                    <div className="widget-header" style={headerStyle}><span>{icon} {this.props.title}</span></div>
                    : null}
                <div className="body no-pad" style={bodyStyle}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Widget;