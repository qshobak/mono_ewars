class Block extends React.Component {
    static defaultProps = {
        width: null,
        borderTop: false,
        borderRight: false,
        borderBottom: false,
        borderLeft: false,
        addClass: null,
        padding: null,
        onClick: null,
        style: {},
        yO: false
    };

    constructor(props) {
        super(props);
    }

    _onClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (this.props.onClick) this.props.onClick();
    };

    render() {
        let style = {};
        style.maxHeight = this.props.height || null;
        style.maxWidth = this.props.width || null;

        style.padding = this.props.padding || null;

        let className = "block";
        className += this.props.borderRight ? " border-right" : "";
        className += this.props.borderBottom ? " border-bottom" : "";
        className += this.props.borderLeft ? " border-left" : "";
        className += this.props.borderTop ? " border-top" : "";
        className += this.props.addClass ? ` ${this.props.addClass}` : "";
        className += this.props.yO ? " overY " : "";

        if (this.props.onClick) style.cursor = "pointer";

        Object.assign(style, this.props.style || {});

        let click;
        if (this.props.onClick) click = this._onClick;

        return (
            <div className={className} style={style} onClick={click}>
                {this.props.children}
            </div>
        )
    }
}

export default Block;
