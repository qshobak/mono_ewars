export default class ToolbarButton extends React.Component {
    render() {
        let iconClass = "fal";
        if (this.props.icon) iconClass += " " + this.props.icon;

        return (
            <div className="tbar-btn" onClick={this.props.onClick}>
                <i className={iconClass}></i>
            </div>
        )
    }
}

