import * as CONSTANTS from './constants/';
import {state} from '../../../admin_documents/cmp_state';
import * as VIEWS from './views/';

const ACTIONS = [
    ['fa-save', 'SAVE'],
    ['fa-times', 'CLOSE']
]

class LayoutEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        ewars.subscribe(this.props.tid + '_UPDATED', this._onChange);
    }

    _onChange = () => {
        this.setState(this.state);
    };

    render() {
        let view, sideView;

        let node = state.getTab(this.props.tid);

        if (node.view == 'DEFAULT') view = <VIEWS.ViewEditor tid={this.props.tid}/>;
        if (node.view == 'PREVIEW') view = <VIEWS.ViewPreview tid={this.props.tid}/>;
        if (node.view == 'DATA') view = <VIEWS.ViewData tid={this.props.tid}/>;
        if (node.view == 'PERMISSIONS') view = <VIEWS.ViewPermissions tid={this.props.tid}/>;

        if (node.sideView == 'DEFAULT') sideView = <VIEWS.ViewSettings tid={this.props.tid}/>;
        if (node.sideView == 'DROPPABLES') sideView = <VIEWS.ViewDroppables tid={this.props.tid}/>;
        if (node.sideView == 'TREE') sideView = <VIEWS.ViewTree tid={this.props.tid}/>;
        if (node.sideView == 'STYLE') sideView = <VIEWS.ViewStyle tid={this.props.tid}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="300px" borderRight={true}>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar>
                                <div className="btn-group btn-tabs">
                                    {CONSTANTS.TABS_SIDE.map(item => {
                                        return (
                                            <ewars.d.Button
                                                highlight={item[2] == node.sideView}
                                                onClick={() => {
                                                    node.setSideView(item[2])
                                                }}
                                                icon={item[0]}/>
                                        )
                                    })}
                                </div>

                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    {sideView}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>

                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar>
                                <div className="btn-group btn-tabs">
                                    {CONSTANTS.TABS_MAIN.map(item => {
                                        return (
                                            <ewars.d.Button
                                                highlight={item[1] == node.view}
                                                icon={item[0]}
                                                onClick={() => {
                                                    node.setView(item[1]);
                                                }}
                                                label={item[2]}/>
                                        )
                                    })}
                                </div>

                                <ewars.d.ActionGroup
                                    right={true}
                                    actions={ACTIONS}
                                    onAction={this._action}/>

                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    {view}

                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default LayoutEditor;