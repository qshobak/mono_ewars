export default [
    ['fa-cogs', 'DEFAULT', 'Editor'],
    ['fa-eye', 'PREVIEW', 'Preview'],
    ['fa-lock', 'PERMISSIONS', 'Permissions'],
    ['fa-database', 'DATA', 'Data']
]