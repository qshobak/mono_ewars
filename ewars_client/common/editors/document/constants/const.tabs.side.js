export default [
    ['fa-cog', 'Settings', 'DEFAULT'],
    ['fa-cube', 'Droppables', 'DROPS'],
    ['fa-code-branch', 'Tree', 'TREE'],
    ['fa-paint-brush', 'Style', 'STYLE']
]