import Moment from "moment";

const DEFAULTS = {
    type: "MAP",
    indicator: null,
    start_date: Moment.utc(),
    end_date: Moment.utc(),
    reduction: "SUM",
    height: 300,
    width: "100%",
    base_colour: "#FFFFFF",
    bgd_colour: "#F2F2F2",
    chloro_opacity: 0.9,
    source_type: "SLICE",
    loc_source: "SPECIFIC",
    legend: "HIDE",
    pie_overlay: "HIDE",
    thresholds: [],
    format: "0"
};

export default DEFAULTS;
