import Form from "../../../c.form";
import DEFAUTLS from "./defaults_widget";
import formWidget from "./form_widget";
import Thresholds from "../_shared/Thresholds";


var SettingsView = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _update: function (data, prop, value) {
        this.props.onChange(null, prop, value);
    },

    _onThreshChange: function (data) {
        this.props.onChange(null, "thresholds", data);
    },

    render: function () {

        this.props.data.mode = this.props.mode;

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-padded">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <div className="chart-edit">
                            <Form
                                definition={formWidget}
                                data={this.props.data}
                                updateAction={this._update}
                                readOnly={false}
                                errors={this.state.errors}/>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default SettingsView;
