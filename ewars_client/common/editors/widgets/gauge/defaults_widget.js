export default {
    spec: 2,
    type: "RAW",
    title: {en: "New Widget"},
    additionalClass: null,
    source_type: "INDICATOR",
    series: [],
    format: "0"
};
