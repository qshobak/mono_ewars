const IS_DATA_ORIENTED = {
    conditional_bool: true,
    conditions: {
        application: "any",
        rules: [
            ["type", "eq", "TYPE"],
            ["type", "eq", "SINGLE"],
            ["type", "eq", "GROUP"]
        ]
    }
}

const FORM = {
    t_header: {
        type: "header",
        icon: "fa-cog",
        label: "General"
    },
    title: {
        type: "text",
        label: "Layer title"
    },
    type: {
        type: "select",
        label: "Visualization",
        options: [
            ["TYPE", "Locations of type"],
            ['SINGLE', 'Single location'],
            ['FORM_POINT', 'Collected Lat/Lng'],
            ['GROUP', 'Location group'],
            ['GEOJSON', 'Custom geojson']
        ]
    },
    lid: {
        type: "location",
        label: "Location",
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ["type", "eq", "TYPE"],
                ["type", "eq", "SINGLE"],
                ["type", "eq", "FORM_POINT"]
            ]
        }
    },
    loc_type: {
        type: "select",
        label: "Location type",
        optionsSource: {
            resource: "location_type",
            query: {},
            valSource: "id",
            labelSource: "name"
        },
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ["type", "eq", "TYPE"]
            ]
        }
    },
    form_field: {
        type: "form_field",
        label: "Form field",
        required_type: "lat_long",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["type", 'eq', 'FORM_POINT']
            ]
        }
    },
    t_data: {
        type: "header",
        icon: "fa-database",
        label: "Data source",
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ['type', 'eq', 'SINGLE'],
                ['type', 'eq', 'GROUP'],
                ['type', 'eq', 'TYPE'],
            ]
        }
    },
    source: {
        type: "button_group",
        label: "Source type",
        options: [
            ['INDICATOR', "Indicator"],
            ['COMPLEX', 'Calculation'],
            ["NONE", "None"]
        ],
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ['type', 'eq', 'SINGLE'],
                ['type', 'eq', 'GROUP'],
                ['type', 'eq', 'TYPE'],
            ]
        }
    },
    ind_id: {
        type: "indicator",
        label: "Indicator",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source", "eq", "INDICATOR"]
            ]
        }
    },
    formula: {
        type: "text",
        label: "Formula",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source", "eq", "COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {en: "Variables"},
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source", "eq", "COMPLEX"]
            ]
        }
    },
    period: {
        type: "date_range",
        label: "Source period",
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ['type', 'eq', 'SINGLE'],
                ['type', 'eq', 'GROUP'],
                ['type', 'eq', 'TYPE'],
                ['type', 'eq', 'FORM_POINT']
            ]
        }
    },
    t_thresholds: {
        type: "header",
        label: "Thresholds",
        icon: "fa-cog",
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ['source', 'eq', 'INDICATOR'],
                ['source', 'eq', 'COMPLEX']
            ]
        }
    },
    ctrl_scale: {
        type: "button_group",
        label: "Show scale",
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    scale_pos: {
        type: "select",
        label: "Scale pos.",
        options: [
            ["topcenter", 'Top-center'],
            ["topright", "Top-right"],
            ["middleright", "Middle-right"],
            ["bottomright", "Bottom-right"],
            ["bottomcenter", 'Bottom-center'],
            ["bottomleft", "Bottom-left"],
            ["middleleft", "Middle-left"],
            ["topleft", "Top-left"]
        ]
    },
    scales: {
        type: "colour_thresholds",
        label: "Thresholds",
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ['source', 'eq', 'INDICATOR'],
                ['source', 'eq', 'COMPLEX']
            ]
        }
    },
    t_style: {
        type: "header",
        icon: "fa-paint-brush",
        label: "Style"
    },
    stroke_color: {
        type: "color",
        label: "Stroke color"
    },
    stroke_width: {
        type: "text",
        label: "Stroke width"
    },
    stroke_style: {
        type: "button_group",
        label: "Stroke style",
        options: [
            ["SOLID", "Solid"],
            ["DOTTED", 'Dotted'],
            ["DASHED", "Dashed"]
        ]
    },
    fill_color: {
        type: "color",
        label: "Fill color"
    },
    fill_opacity: {
        type: "text",
        max: 1,
        min: 0,
        label: "Fill opacity"
    },
    marker_type: {
        type: "button_group",
        label: "Marker type",
        options: [
            ["CIRCLE", "Circle"],
            ["SQUARE", 'Square'],
            ["TRIANGLE", "Triangle"],
            ["STAR", "Star"]
        ]
    },
    marker_size: {
        type: "text",
        label: "Marker size",
        allowDecimal: true
    },
    t_labels: {
        icon: "fa-tag",
        type: "header",
        label: "Labels"
    },
    lab_type: {
        type: "button_group",
        label: "Label type",
        options: [
            ["IN", "In-place"],
            ["NUM", "Numbered"],
            ["NONE", "No labelling"]
        ]
    },
    label_color: {
        type: "color",
        label: "Label color"
    },
    label_size: {
        type: "number",
        label: "Label size"
    }
}

export default FORM;