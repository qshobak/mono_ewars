export default {
    t_header: {
        type: "header",
        icon: "fa-cog",
        label: "General"
    },
    title: {
        type: "language_string",
        label: {
            en: "Title"
        }
    },
    center: {
        type: "bounds",
        label: "Map bounds"
    },
    t_style: {
        type: "header",
        icon: "fa-paint-brush",
        label: {
            en: "Style"
        }
    },
    bgd_color: {
        type: "color",
        label: "Background color"
    },
    ctrl_time: {
        type: "button_group",
        label: "Time controls",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    ctrl_zoom: {
        type: "button_group",
        label: "Zoom controls",
        options: [
            [true, 'Show'],
            [false, 'Hide']
        ]
    },
    ctrl_layers: {
        type: "button_group",
        label: "Layer controls",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    mapbox: {
        type: "button_group",
        label: "Mapbox layer",
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    }
}