import Form from "../../../c.form";
import DEFAULTS from "./defaults_widget";
import formWidget from "./form_widget";

var SettingsView = React.createClass({
    displayName: "Raw Widget Settings",

    getDefaultProps: function () {
        return {
            mode: "DEFAULT"
        }
    },

    getInitialState: function () {
        return {
            errors: []
        }
    },

    _update: function (data, prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {

        if (window.user.role != "SUPER_ADMIN") {
            formWidget.location.parentId = window.user.clid;
        }

        this.props.data.mode = this.props.mode;

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-padded">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <br />
                        <div className="chart-edit">
                            <Form
                                definition={formWidget}
                                data={this.props.data}
                                updateAction={this._update}
                                readOnly={false}
                                errors={this.state.errors}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default SettingsView;
