export default {
    dataType: {
        type: "hidden"
    },
    t_general: {
        type: "header",
        label: "General"
    },
    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    t_source: {
        type: "header",
        label: "Data Source"
    },
    source_type: {
        type: "button_group",
        label: {
            en: "Source Type"
        },
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator Source"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {
            en: "Variables"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    reduction: {
        type: "button_group",
        label: "Reduction",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Avg."]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    period: {
        type: "date_range",
        label: "Period"
    },
    t_style: {
        type: "header",
        label: "Style"
    },
    format: {
        type: "select",
        label: "Value Formatting",
        options: [
            ["0", "0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0,0.0", "0,0.0"],
            ["0,0.00", "0,0.00"],
            ["0,0", "0,0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]
        ]
    },
    prefix: {
        label: "Prefix",
        type: "text",
        help: "Text shown to the left of the value"
    },
    suffix: {
        label: "Suffix",
        type: "text",
        help: "Text shown after the value in"
    },
    o_val_colour: {
        type: "button_group",
        label: "Value Colouring",
        options: [
            [false, "Disabled"],
            [true, "Enabled"]
        ]
    },
    value_colouring: {
        label: "Value Colouring",
        type: "colour_thresholds",
        output_type: "COLOUR",
        help: "Colour the value based on threshold",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["o_val_colour", 'eq', true]
            ]
        }
    },
    o_val_mapping: {
        type: "button_group",
        label: "Value Mapping",
        options: [
            [false, "Disabled"],
            [true, "Enabled"]
        ]
    },
    value_mapping: {
        label: "Value Mapping",
        type: "colour_thresholds",
        output_type: "VALUE",
        help: "Display text/value based on value thresholds",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["o_val_mapping", "eq", true]
            ]
        }
    }
};
