const modeConditions = {
    conditions: {
        application: "all",
        rules: [
            ["mode", "eq", "DASHBOARD"]
        ]
    }
};

export default {
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "button_group",
        label: "Location Spec.",
        options: [
            ["SPECIFIC", "Specific Location"],
            ["GROUP", "Location Group(s)"]
        ]
    },
    group_ids: {
        type: "location_group",
        label: "Location Group(s)",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    data_source_header: {
        type: "header",
        label: {en: "Data Source"}
    },
    source_type: {
        type: "button_group",
        label: "Source type",
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {en: "Variables"},
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    reduction: {
        type: "button_group",
        label: "Reduction",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Avg."]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    interval: {
        type: "button_group",
        label: "Aggregation",
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ]
    },
    period: {
        type: "date_range",
        label: "Source Period"
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    font_size: {
        type: "number",
        label: "Value Text Size",
        help: "Size is in pixels (px), default 30px",
        conditional_bool: true,
        ...modeConditions
    },
    format: {
        type: "select",
        label: {
            en: "Number Formatting"
        },
        options: [
            ["0", "0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0,0.0", "0,0.0"],
            ["0,0.00", "0,0.00"],
            ["0,0", "0,0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]

        ]
    },
    prefix: {
        label: "Prefix",
        type: "text",
        help: "Text shown to the left of the value",
        conditional_bool: true,
        ...modeConditions
    },
    suffix: {
        label: "Suffix",
        type: "text",
        help: "Text shown after the value in",
        conditional_bool: true,
        ...modeConditions
    },
    o_val_colour: {
        type: "button_group",
        label: "Value Colouring",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ],
        conditional_bool: true,
        ...modeConditions
    },
    value_colouring: {
        label: "Value Colouring",
        type: "colour_thresholds",
        output_type: "COLOUR",
        help: "Colour the value based on threshold",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["o_val_colour", 'eq', true],
                ...modeConditions.conditions.rules
            ]
        }
    },
    o_val_mapping: {
        type: "button_group",
        label: "Value Mapping",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ]
    },
    value_mapping: {
        label: "Value Mapping",
        type: "colour_thresholds",
        output_type: "VALUE",
        help: "Display text/value based on value thresholds",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["o_val_mapping", "eq", true]
            ]
        }
    },
    // t_bgd: {
    //     label: "Background Styling",
    //     type: "header",
    //     ...modeConditions
    // },
    // bgd_shown: {
    //     label: "Background Series",
    //     type: "button_group",
    //     help: "Display a time series of the data as the background for the widget",
    //     options: [
    //         [false, "Hide"],
    //         [true, "Show"]
    //     ],
    //     ...modeConditions
    // },
    // bgd_logarithmic: {
    //     type: "button_group",
    //     label: "Logarithmic",
    //     options: [
    //         [false, "No"],
    //         [true, "Yes"]
    //     ],
    //     conditions: {
    //         application: "all",
    //         rules: [
    //             ["bgd_shown", "eq", true],
    //             ...modeConditions.conditions.rules
    //         ]
    //     }
    // },
    // bgd_fill: {
    //     type: "color",
    //     label: "Series Colour",
    //     conditions: {
    //         application: "all",
    //         rules: [
    //             ["bgd_shown", "eq", true],
    //             ...modeConditions.conditions.rules
    //         ]
    //     }
    // },
    t_widget: {
        type: "header",
        label: "Widget settings",
        conditional_bool: true,
        ...modeConditions
    },
    widgetBaseColor: {
        type: "text",
        label: "Base colour",
        conditional_bool: true,
        ...modeConditions
    },
    widgetHighlightColor: {
        type: "text",
        label: "Highlight colour",
        conditional_bool: true,
        ...modeConditions
    },
    widgetIcon: {
        type: "text",
        label: "Icon",
        conditional_bool: true,
        ...modeConditions
    },
    widgetSubText: {
        type: "text",
        label: "Widget sub-text",
        conditional_bool: true,
        ...modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "Widget footer text",
        conditional_bool: true,
        ...modeConditions
    }
};
