export default {
    spec: 2,
    title: {en: "New Widget"},
    type: "CATEGORY",
    interval: "WEEK",
    style: "DEFAULT",
    slices: [],
    show_legend: false,
    slice_order: "NONE",
    width: null,
    height: null,
    hide_no_data: false,
    y_axis_type: "NUMERIC",
    y_axis_label: "Number",
    y_axis_format: "0",
    y_axis_allow_decimals: false,
    start_date: new Date(),
    end_date: new Date()
};
