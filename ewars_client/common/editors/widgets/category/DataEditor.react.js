import { Layout, Row, Cell } from "../../../layout";
import Toolbar from "../../../c.toolbar";
import Button from "../../../c.button";
import DEFAULTS_SLICE from "./defaults_slice";
import FORM_SLICE from "./form_slice";
import Form from "../../../c.form";

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

var Column = React.createClass({
    getInitialState: function () {
        return {
            isExpanded: false,
            errors: []
        }
    },

    _moveUp: function () {
        this.props.moveUp(this.props.data, this.props.index);
    },

    _moveDown: function () {
        this.props.moveDown(this.props.data, this.props.index);
    },

    _edit: function () {
        this.setState({
            isExpanded: this.state.isExpanded ? false : true
        })
    },

    _delete: function () {
        this.props.onRemove(this.props.index);
    },

    _update: function (data, prop, value) {
        this.props.onChange(this.props.index, prop, value);
    },

    render: function () {
        var colTitle = ewars.I18N(this.props.data.title);

        var editIcon = "fal fa-pencil";
        if (this.state.isExpanded) editIcon = "fal fa-times";

        if (window.user.role != "SUPER_ADMIN") {
            FORM_SLICE.generator_locations_parent.parentId = window.user.clid;
            FORM_SLICE.location.parentId = window.user.clid;
        }

        if (ewars.TEMPLATE_MODE == "TEMPLATE") {
            FORM_SLICE.loc_spec.options = [
                ["SPECIFIC", "Specific Location"],
                ["REPORT_LOCATION", "Document Location"],
                ["GENERATOR", "Generator"],
                ["GROUP", "Location Group(s)"]
            ]
        }

        return (
            <div className="data-item-wrapper">
                <div className="series-list-item">
                    <div className="name">{colTitle}</div>
                    <div className="control-wrapper">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-caret-up"
                                onClick={this._moveUp}/>
                            <ewars.d.Button
                                icon="fa-caret-down"
                                onClick={this._moveDown}/>
                        </div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon={editIcon}
                                onClick={this._edit}/>
                            <ewars.d.Button
                                icon="fa-trash"
                                colour="red"
                                onClick={this._delete}/>
                        </div>
                    </div>
                </div>
                {this.state.isExpanded ?
                    <div className="data-item-settings">
                        <div className="ide-settings-content" style={{paddingBottom: 16}}>
                            <div className="ide-settings-form">
                                <div className="chart-edit">
                                    <Form
                                        definition={FORM_SLICE}
                                        data={this.props.data}
                                        updateAction={this._update}
                                        readOnly={false}
                                        errors={this.state.errors}/>

                                </div>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
});

var DataEditor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _addSeries: function () {
        var cols = ewars.copy(this.props.data.slices || []);
        cols.push({...{}, ...DEFAULTS_SLICE});
        this.props.onChange("slices", cols);
    },

    _onChange: function (index, prop, value) {
        var cols = ewars.copy(this.props.data.slices || []);
        cols[index][prop] = value;
        this.props.onChange("slices", cols);
    },

    _moveUp: function (col, index) {
        if (index <= 0) return;

        var targetIndex = index - 1;
        var cols = ewars.copy(this.props.data.slices || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("slices", cols);
    },

    _moveDown: function (col, index) {
        if (index >= this.props.data.slices.length - 1) return;

        var targetIndex = index + 1;
        var cols = ewars.copy(this.props.data.slices || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("slices", cols);
    },

    _onRemove: function (index) {
        let cols = ewars.copy(this.props.data.slices || []);
        cols.splice(index, 1);
        this.props.onChange("slices", cols);
    },

    render: function () {

        let cols = this.props.data.slices.map((col, index) => {
            return <Column
                onChange={this._onChange}
                data={col}
                moveUp={this._moveUp}
                moveDown={this._moveDown}
                onRemove={this._onRemove}
                index={index}/>;
        });

        return (
            <div className="data-manager" id="dm">
                <Layout>
                    <Toolbar>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-plus"
                                onClick={this._addSeries}
                                label="Add Category"/>
                        </div>
                    </Toolbar>
                    <Row>
                        <Cell>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="series-list">{cols}</div>
                            </div>
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
});

export default DataEditor;
