import CONSTANTS from "../../../constants";
import Settings from "./SettingsView.react";
import DataEditor from "./DataEditor.react";
import JSONEditor from "../_shared/JSONEditor.react";
import VertTab from "../../../c.tab.vertical";

const VIEWS = {
    SETTINGS: "SETTINGS",
    DATA: "DATA",
    SOURCE: "SOURCE",
    PREVIEW: "PREVIEW"
};


var SeriesWidgetEditor = React.createClass({
    displayName: "Series Widget Editor",

    getDefaultProps: function () {
        return {
            mode: "DEFAULT"
        }
    },

    getInitialState: function () {
        return {
            view: VIEWS.SETTINGS
        }
    },

    _onChange: function (prop, value) {
        this.props.onChange(prop, value);
    },

    _changeShoulder: function(view) {
        this.setState({
            view: view
        })
    },

    _onJSONSave: function (data) {
        this.props.onJSONSave(data);
    },

    render: function() {

        var view;
        if (this.state.view == VIEWS.SETTINGS) view = <Settings mode={this.props.mode} data={this.props.data} onChange={this._onChange}/>;
        if (this.state.view == VIEWS.DATA) view = <DataEditor mode={this.props.mode} data={this.props.data} onChange={this._onChange}/>;
        if (this.state.view == VIEWS.PREVIEW) view = null;
        if (this.state.view == VIEWS.SOURCE) view = <JSONEditor onSave={this._onJSONSave} mode={this.props.mode} data={this.props.data}/>;

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col ide-relative" style={{maxWidth: "34px"}}>
                        <div className="ide-panel">
                            <div className="ide-tabs">
                                <VertTab
                                    view={VIEWS.SETTINGS}
                                    label="Settings"
                                    icon="fa-globe"
                                    active={this.state.view == VIEWS.SETTINGS}
                                    onClick={this._changeShoulder}/>
                                <VertTab
                                    view={VIEWS.DATA}
                                    label="Data"
                                    icon="fa-tags"
                                    active={this.state.view == VIEWS.DATA}
                                    onClick={this._changeShoulder}/>
                                <VertTab
                                    view={VIEWS.SOURCE}
                                    label="Source"
                                    icon="fa-code"
                                    active={this.state.view == VIEWS.SOURCE}
                                    onClick={this._changeShoulder}/>
                            </div>
                        </div>
                    </div>

                    <div className="ide-col">
                        {view}
                    </div>
                </div>
            </div>
        )
    }
});

export default SeriesWidgetEditor;
