export default [
    {
        t: 'H',
        l: 'General Settings'
    },
    {
        t: 'BUTTONSET',
        l: 'Allow request',
        n: 'request',
        o: [
            [false, 'No'],
            [true, 'Yes']
        ]
    }
];