export default [
    {
        t: 'H',
        l: 'General Settings'
    },
    {
        t: 'FILE',
        l: 'Image file',
        n: 'url'
    },
    {
        t: 'TEXT',
        l: 'Link',
        n: 'link'
    }
];