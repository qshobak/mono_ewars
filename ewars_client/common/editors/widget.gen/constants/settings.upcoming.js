export default [
    {
        t: 'H',
        l: 'General Settings'
    },
    {
        t: 'NUMERIC',
        l: 'Limit',
        n: 'limit'
    },
    {
        t: 'NUMERIC',
        l: 'Age (more than days)'
    }
];