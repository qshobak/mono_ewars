import StyleEditor from './StyleEditor';
import WidgetInstance from './widget_instance';
import * as EDITORS from './editors/'

const EDITOR = {}

const OVERLAY_STYLE = {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 999,
    background: "rgba(0,0,0,0.5)"
};

const OVERLAY_INNER = {
    position: 'fixed',
    top: '20px',
    right: '20px',
    bottom: '20px',
    left: '20px',
    background: '#333333',
    border: '1px solid rgba(0,0,0,0.1)'
}

class WidgetEditor extends React.Component {
    constructor(props) {
        super(props);

        let instance = new WidgetInstance(props.data);

        this.state = {
            view: 'DEFAULT',
            data: instance
        }

        ewars.subscribe('WIDGET_UPDATED', this._onUpdate);
    }

    componentWillUnmount() {
        ewars.unsubscribe('WIDGET_UPDATED', this._onUpdate);
    }

    _onUpdate = () => {
        this.setState({instance: this.state.instance})
    };

    _close = () => {
        this.props.onClose();
    };

    _save = () => {
        this.props.onSave(this.state.data.getFinal());
    };

    render() {

        let widgetView;

        if (this.props.data.type == 'HTML') widgetView = <EDITORS.HTMLEditor/>;
        if (this.props.data.type == 'ALERTS') widgetView = <EDITORS.AlertsListEditor/>;
        if (this.props.data.type == 'MARKDOWN') widgetView = <EDITORS.MarkdownEditor/>;
        if (this.props.data.type == 'QUICKLINKS') widgetView = <EDITORS.QuickLinksEditor/>;
        if (this.props.data.type == 'ACTIVITY') widgetView = <EDITORS.ActivityEditor/>;

        return (
            <div style={OVERLAY_STYLE}>
                <div style={OVERLAY_INNER}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label="Widget Editor">
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    icon="fa-save"
                                    onClick={this._save}/>
                                <ewars.d.Button
                                    icon="fa-times"
                                    onClick={this._close}/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell borderRight={true} width="30%">
                                <StyleEditor
                                    definition={{}}
                                    data={this.props.data}/>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                {widgetView}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

export default WidgetEditor;