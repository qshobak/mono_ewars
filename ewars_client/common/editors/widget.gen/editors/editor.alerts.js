import ControlField from '../components/ui.control_field';

const FORM = [
    {
        t: 'H',
        l: 'General Settings'
    },
    {
        t: 'SELECT',
        l: 'Display mode',
        n: 'disp',
        o: [
            ['DEFAULT', 'List'],
            ['MAP', 'Map']
        ]
    },
    {
        t: 'SELECT',
        l: 'Alarm',
        n: 'al',
        o: [
            ['*', 'All'],
            ['ACTIVE', 'Active alarms'],
            ['INACTIVE', 'Inactive alarms'],
            ['SPECIFIC', 'Specific alarm']
        ]
    },
    {
        t: 'SELECT',
        l: 'Alarm',
        n: 'alid',
        os: {
            resource: 'alarm'
        }
    },
    {
        t: 'SELECT',
        n: 'al_state',
        l: 'Alert state',
        o: [
            ['*', 'Any'],
            ['OPEN', 'Open'],
            ['CLOSED', 'Closed']
        ]
    },
    {
        t: 'SELECT',
        n: 'al_stage',
        l: 'Alert stage',
        o: [
            ['*', 'Any'],
            ['VERIFICATION', 'Verification'],
            ['RISK_ASSESS', 'Risk assessment'],
            ['RISK_CHAR', 'Risk characterization'],
            ['OUTCOME', 'Outcome']
        ]
    },
    {
        t: 'SELECT',
        n: 'alert_outcome',
        l: 'Alert outcome',
        o: [
            ['*', 'Any']
        ]
    },
    {
        t: 'SELECT',
        n: 'al_loc',
        l: 'Alert location',
        o: [
            ['*', 'Any'],
            ['SPECIFIC', 'Location'],
            ['OF_TYPE', 'Of type'],
            ['UNDER', 'Under parent']
        ]
    },
    {
        t: 'SELECT',
        n: 'al_risk',
        l: 'Alert risk',
        o: [
            ['*', 'Any'],
            ['LOW', 'Low'],
            ['MODERATE', 'Moderate'],
            ['HIGH', 'High'],
            ['SEVERE', 'Severe']
        ]
    }
];

class AlertsListEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="admin-control-editor">
                <ewars.d.SystemForm
                    data={{}}
                    definition={FORM}/>
            </div>
        )
    }
}

export default AlertsListEditor;