import TextField from '../../cmp/fields/TextField';
import ColourThresholds from '../../cmp/fields/ColourThresholds';

import {instance} from './widget_instance';

const DEFINITION = {
    t_general: {
        l: 'General Settings',
        t: 'H'
    },
    title: {
        l: 'title',
        t: 'bool',
        d: false,
        __: {
            titleText: {l: 'text', t: 'text', d: 'Chart'},
            titleColor: {l: 'colour', t: 'color', d: '#333333'},
            titleSize: {l: 'size (px)', t: 'number', d: 12},
            titleWeight: {l: 'weight', t: 'select', d: 'normal', o: ['normal', 'bold']},
            titleAlign: {l: 'align', t: 'select', d: 'center', o: ['center', 'left', 'right']},
            titleVAlign: {l: 'v. align', t: 'select', d: 'top', o: ['top', 'middle', 'bottom']},
            titleX: {l: 'x', t: 'number'},
            titleY: {l: 'y', t: 'number'}
        }
    },
    subTitle: {
        l: 'Sub-Title',
        t: 'bool',
        d: false,
        __: {
            sTitleText: {l: 'text', t: 'text', d: 'Chart'},
            sTitleColor: {l: 'colour', t: 'color', d: '#333333'},
            sTitleSize: {l: 'size (px)', t: 'number', d: 12},
            sTitleWeight: {l: 'weight', t: 'select', d: 'normal', o: ['normal', 'bold']},
            sTitleAlign: {l: 'align', t: 'select', d: 'center', o: ['center', 'left', 'right']},
            sTitleVAlign: {l: 'v. align', t: 'select', d: 'top', o: ['top', 'middle', 'bottom']},
            sTitleX: {l: 'x', t: 'number'},
            sTitleY: {l: 'y', t: 'number'}
        }
    },
    t_container_style: {
        l: "Container Styling",
        t: "H"
    },
    border: {
        l: "border",
        t: 'bool',
        d: false,
        // This property can break out into other properties
        __: {
            borderColor: {l: 'color', t: 'color', d: 'transparent'},
            borderWidth: {l: 'width', t: 'number', d: 0},
            borderRadius: {l: 'radius', t: 'number', d: 0}
        }
    },
    margin: {
        l: "margin",
        t: 'text',
        __: {
            marginTop: {l: 'top', t: 'number', d: 0},
            marginRight: {l: 'right', t: 'number', d: 0},
            marginBottom: {l: 'bottom', t: 'number', d: 0},
            marginLeft: {l: 'left', t: 'number', d: 0}
        }
    },
    padding: {
        l: "padding",
        t: 'text',
        __: {
            paddingTop: {l: 'top', t: 'number', d: 0},
            paddingRight: {l: 'right', t: 'number', d: 0},
            paddingBottom: {l: 'bottom', t: 'number', d: 0},
            paddingLeft: {l: 'left', t: 'number', d: 0}
        }
    },
    boxShadow: {
        l: "box shadow",
        t: 'bool',
        __: {
            bsXOffset: {l: 'x', t: 'number', d: 0},
            bsYOffset: {l: 'y', t: 'number', d: 0},
            bsBlur: {l: 'blur', t: 'number', d: 0},
            bdColor: {l: 'color', t: 'number', d: 0}
        }
    },
    backgroundColor: {
        l: "background",
        t: 'color'
    },
    t_chart: {
        t: 'H',
        l: 'chart settings'
    },
    showGrid: {
        l: 'show grid',
        t: 'bool',
        d: false
    },
    reload: {
        l: 'reloading',
        t: 'bool',
        d: false,
        __: {
            reloadInterval: {l: 'Interval (secs)', t: 'number', d: 60}
        }
    },
    widgetBox: {
        l: 'boxed',
        t: 'bool',
        d: false,
        __: {
            boxHeaderBackground: {l: 'Header bgd.', t: 'color', d: 'transaprent'},
            boxBorder: {l: 'Border', t: 'text', d: '1px solid #333333'},
            boxTitle: {l: 'Title', t: 'text'},
            boxBackground: {l: 'Background', t: 'color'}
        }
    }
};

const TABLE_STYLE = {
    borderBottom: "1px solid rgba(0,0,0,0.2)"
}

const LABEL_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    borderRight: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "right",
    width: "25%",
    background: "rgba(0,0,0,0.2)"
};

const INPUT_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "left"
};

const SUB_LABEL_STYLE = {
    borderLeft: "12px solid rgba(0,0,0,0.2)",
    ...LABEL_STYLE
}

const INPUT_TXT_STYLE = {
    display: "block",
    border: "none",
    padding: "0px",
    margin: "0px",
    background: "transparent",
    borderRadius: "0px"
}

const HEADER_SECTION_STYLE = {
    padding: "8px 5px 5px 5px",
    fontWeight: "bold",
    textAlign: "left",
    textTransform: "uppercase",
    background: "rgba(0,0,0,0.1)",
    margin: '8px',
    borderRadius: '8px'
};

const SELECT_STYLE = {
    border: "none",
    width: "100%",
    height: "100%",
    margin: 0,
    padding: 0,
    fontSize: "12px",
    background: "#333333",
    color: "#FFFFFF"
}

class Styler extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            edit: false,
            value: "",
            childs: false,
            showEditor: false
        }

        ewars.subscribe("CLOSE_EDITS", this._closeEdit)
    }

    componentWillUnmount() {
        ewars.unsubscribe("CLOSE_EDITS", this._closeEdit);
    }


    _edit = () => {
        ewars.emit("CLOSE_EDITS");
        this.setState({
            edit: true
        })
        // this._ref.focus();
    };

    _closeEdit = () => {
        this.setState({
            edit: false
        })
    };

    _onChange = (e) => {
        instance.updateProp(this.props.prop, e.target.value);
    };

    _expand = () => {
        this.setState({
            childs: !this.state.childs
        })
    };

    _action = (action) => {
        if (action == "CLOSE") {
            this.setState({
                showEditor: false
            })
        }
    };

    render() {
        let editor;

        switch (this.props.data.t) {
            case 'text':
            default:
                editor = (
                    <input
                        value={instance.config[this.props.prop] || null}
                        onChange={this._onChange}
                        type="text"/>
                );
                break;
            case 'select':
                editor = (
                    <select onChange={this._onChange}
                            value={instance.config[this.props.prop] || null}>
                        {this.props.data.o.map(item => {
                            return (
                                <option value={item}>{item}</option>
                            )
                        })}
                    </select>
                );
                break;
            case 'number':
                editor = (
                    <input type="number"
                           value={instance.config[this.props.prop] || null}
                           onChange={this._onChange}/>
                );
                break;
            case 'bool':
                editor = (
                    <select value={instance.config[this.props.prop] || null}
                            style={SELECT_STYLE}
                            onChange={this._onChange}>
                        <option value={true}>True</option>
                        <option value={false}>False</option>
                    </select>
                );
                break;
            case 'color':
                editor = (
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <input type="color"
                                   onChange={this._onChange}
                                   value={instance.config[this.props.prop] || null}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                );
                break;
        }


        let children = [];

        if (this.props.data.__) {
            for (let i in this.props.data.__) {
                children.push(
                    <Styler
                        isChild={true}
                        prop={i}
                        onChange={this.props.onChange}
                        data={this.props.data.__[i]}/>
                )
            }
        }

        return (
            <div style={{display: 'block'}} className="style-control">
                <ewars.d.Layout>
                    <ewars.d.Row>
                        {this.props.data.__ ?
                            <ewars.d.Cell width="20px" onClick={this._expand} style={{textAlign: "center", padding: '5px', background: 'rgba(0,0,0,0.2)', lineHeight: '26px'}} borderBottom={true}>
                                <i className={"fal " + (this.state.childs ? "fa-caret-down" : "fa-caret-right")} style={{lineHeight: '26px'}}></i>
                            </ewars.d.Cell>
                            : null}
                        {this.props.isChild ?
                            <ewars.d.Cell width="5px" style={{background: 'rgba(0,0,0,0.2)'}} borderBottom={true}>&nbsp;</ewars.d.Cell>
                        : null}
                        <ewars.d.Cell style={{padding: '5px', lineHeight: '26px'}} borderBottom={true}>
                            {this.props.data.l}
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: '5px'}} borderBottom={true}>
                            {editor}
                        </ewars.d.Cell>

                    </ewars.d.Row>
                    {this.state.childs ? children : null}
                </ewars.d.Layout>
            </div>
        )
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={HEADER_SECTION_STYLE} colSpan="2">{__(this.props.data.l)}</div>
        )
    }
}


class StyleEditor extends React.Component {
    static defaultProps = {
        data: {}, // style settings
        definition: {} // definition for the form
    };

    constructor(props) {
        super(props);

        this.state = {
            search: ""
        }
    }

    _onSearchChange = (e) => {
        this.setState({
            search: e.target.value
        })
    };

    onChange = (prop, value) => {
        instance.updateProp(prop, value);
    };

    render() {
        let items = [];

        for (let prop in DEFINITION) {
            if (DEFINITION[prop].t == "H") {
                items.push(
                    <Header data={DEFINITION[prop]}/>
                )
            } else {
                items.push(
                    <Styler
                        prop={prop}
                        onChange={this.onChange}
                        data={DEFINITION[prop]}
                        style={this.props.data}/>
                )
            }
        }


        return (
            <ewars.d.Panel>
                {items}
            </ewars.d.Panel>
        )

    }
}

export default StyleEditor;