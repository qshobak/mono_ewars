let instance;

class WidgetInstance {
    constructor(props) {
        this.config = props;
        instance = this;
    }

    updateProp = (prop, value) => {
        if ([null, undefined, ''].indexOf(value) >= 0) {
            delete this.config[prop];
        } else {
            this.config[prop] = value;
        }
        ewars.emit('WIDGET_UPDATED');
    };

    getProp = (prop, fb) => {
        if (!fb) fb = undefined;
        return this.config[prop] ? this.config[prop] : fb;
    };

    silentUpdateProp = (prop, value) => {
        if ([null, undefined, ''].indexOf(value) >= 0) {
            delete this.config[prop];
        } else {
            this.config[prop] = value;
        }
    };

    getFinal = () => {
        return this.config;
    }
}

export {instance}

export default WidgetInstance;