const toArray = (etl) => {
    if (!etl) return [];
    let asArray = Object.keys(etl).map(key => {
        return {
            ...etl[key],
            i: key
        }
    });

    let counter = 0;
    asArray.sort((a, b) => {
        if (a.o > b.o) return 1;
        if (a.o < b.o) return -1;
        return 0;
    });

    asArray.forEach(item => {
        item.o = counter;
        counter++;
    });

    return asArray;
}

const utils = {};
utils.logicToArray = toArray;

/**
 * Remove a root-level indicator from the etl
 **/
utils.removeInd = (etl, id) => {
    delete etl[id];
    return etl;
};

/**
 * Move a root-level indicator down one row
 */
utils.moveIndDown = (etl, id) => {
    let asArray = toArray(etl);
    let curIndex = asArray.findIndex(x => x.i == id);
    if (curIndex < asArray.length - 1) {
        asArray[curIndex].o++;
        asArray[curIndex+1].o--;
    }

    let obj = {};
    asArray.forEach(item => {
        obj[item.i] = item;
    });
    return obj;
};

/**
 * Move a root-level indicator up one level
 **/
utils.moveIndUp = (etl, id) => {
    let asArray = toArray(etl);

    let curIndex = asArray.findIndex(x => x.i == id);
    if (curIndex > 0) {
        asArray[curIndex].o--;
        asArray[curIndex-1].o++;
    }

    let obj = {};
    asArray.forEach(item => {
        obj[item.i] = item;
    });
    return obj;
};

utils.duplicateInd = (etl, id) => {
    let new_uuid = ewars.utils.uuid();
    etl[`NEW_${new_uuid}`] = {
        ...ewars.copy(etl[id]),
        key: ewars.utils.uuid(),
        o: toArray(etl).length + 1
    }

    return etl;
};

utils.swapIndicator = (etl, original, newInd) => {
    etl[newInd] = {
        ...etl[original],
        i: newInd
    };
    delete etl[original];

    return etl;
}

utils.updateIndicator = (etl, id, data) => {
    etl[id] = data;
    return etl;
}

utils.setAddField = (etl, data) => {
    etl[data.id].n[data.path].push(":EQ:");
    return etl;
}

utils.addSet = (etl, id, path) => {
    return etl;
}

utils.removeSet = (etl, id, path) => {
    return etl;
}

utils.changeSetRule = (etl, id, path, value) => {
    return etl;
}

utils.removeFieldFromInd = (etl, data) => {
    etl[data.id].n.splice(data.index, 1);
    return etl;
}

utils.updateIndicatorField = (etl, data) => {
    let cur = etl[data.id].n[data.index];
    let parts = (cur || "").split(":");

    switch (data.prop) {
        case "operator":
            parts[1] = data.value;
            break;
        case "value":
            parts[2] = data.value;
            break;
        default:
            break;
    }

    etl[data.id].n[data.index] = parts.join(":");

    return etl;
};

utils.upcastIndicatorField = (etl, data) => {
    return etl;
}

utils.addRootIndicator = (etl) => {
    let new_uuid = ewars.utils.uuid();
    let id = `NEW_${new_uuid}`;
    if (!etl) etl = {};
    etl[id] = {
        i: id,
        key: new_uuid,
        o: toArray(etl).length + 1,
        n: []
    }
    return etl;
}

utils.removeComplexFromIndicator = (etl, data) => {
    etl[data.id].n.splice(data.index, 1);
    return etl;
}

utils.handleEvent = (_event, etl, data) => {
    switch (_event) {
        case "etl-set-change-provision":
            etl[data.id].n[data.path][0] = data.value;
            return etl;
            break;
        case "remove-ind":
            return utils.removeInd(etl, data.id);
            break;
        case "ind-move-down":
            return utils.moveIndDown(etl, data.id);
            break;
        case "ind-move-up":
            return utils.moveIndUp(etl, data.id);
            break;
        case "dupe-ind":
            return utils.duplicateInd(etl, data.id);
            break;
        case "ind-rule-change":
            return utils.updateIndicator(etl, data.id, data.data);
            break;
        case "ind-remove-complex":
            return utils.rmeoveComplexFromIndicator(etl, data);
            break;
        case "ind-swap":
            if (etl[data.new]) {
                ewars.prompt("Existing Mapping", "THere is already a mapping for the selected indicator");
                return etl;
            } else {
                return utils.swapIndicator(etl, data.id, data.new);
            }
            break;
        case "etl-add-field":
            return utils.setAddField(etl,data);
            break;
        case "etl-add-set":
            return utils.addSet(etl, data.id, data.path);
            break;
        case "etl-remove-set":
            return utils.removeSet(etl, data.id, data.path);
            break;
        case "etl-change-rule":
            return utils.changeSetRule(etl, data.id, data.path, data.value);
            break;
        case "ind-remove-field":
            return utils.removeFieldFromInd(etl, data);
            break;
        case "ind-update-field":
            return utils.updateIndicatorField(etl, data);
            break;
        case "etl-set-update-rule":
            let changeNode = etl[data.id];
            let changePath = data.path.split(".");
            let cp = changePath.map(item => parseInt(item));

            let rule;

            switch (cp.length) {
                case 1:
                    rule = changeNode.n[cp[0]];
                    break;
                case 2:
                    rule = changeNode.n[cp[0]][cp[1]];
                    break;
                case 3:
                    rule = changeNode.n[cp[0]][cp[1]][cp[2]];
                    break;
                default:
                    break;
            }

            let split = rule.split(":");

            switch (data.prop) {
                case "field":
                    split[0] = data.value;
                    break;
                case "operator":
                    split[1] = data.value;
                    break;
                case "value":
                    split[2] = data.value;
                    break;
                default:
                    break;
            }

            if (cp.length == 1) {
                changeNode.n[cp[0]] = split.join(":");
            } else if (cp.length == 2) {
                changeNode.n[cp[0]][cp[1]] = split.join(":");
            } else if (cp.length == 3) {
                changeNode.n[cp[0]][cp[1]][cp[2]] = split.join(":");
            } else {}

            etl[data.id] = changeNode;

            return etl;
            break;
        case "etl-set-remove-rule":
            let node = etl[data.id];
            let path = data.path.split(".");
            path = path.map(item => parseInt(item));

            if (path.length == 1) {
                node.n.splice(path[0], 1);
            } else if (path.length == 2) {
                node.n[path[0]].splice(path[1], 1);
            } else if (path.legnth == 3) {
                node.n[path[0]][path[1]].splice(path[2],1);
            }

            etl[data.id] = node;
            return etl;
            break;
        default:
            break;
    }
}

export default utils;

