import SetItem from "./SetItem";

class Set extends React.Component {
    constructor(props) {
        super(props);
    }

    _emit = (event, data) => {
        window.dispatchEvent(new CustomEvent(event, {
            detail: {
                ...data,
                _event: event
            }
        }));
    };

    _add = () => {
        this._emit("etl-add-field", {
            id: this.props.indicator,
            path: this.props.path
        });
    };

    _addSet = () => {
        this._emit("etl-add-set", {
            id: this.props.indicator,
            path: this.props.path
        })
    };

    _onRemove = () => {
        this._emit("etl-remove-set", {
            id: this.props.indicator,
            path: this.props.path
        });
    };

    _onChangeRule = (e) => {
        this._emit("etl-set-change-provision", {
            id: this.props.indicator,
            path: this.props.path,
            value: e.target.value
        });
    };

    render() {

        let items = this.props.data.map((item, index) => {
            if (["ANY", "ALL"].indexOf(item) >= 0) {
                return (
                    <div className="application">
                        <div className="ide-row">
                            <div className="ide-tbar" style={{maxWidth: 55}}>
                                <select value={this.props.data[0]} onChange={this._onChangeRule}>
                                    <option value="ALL">All</option>
                                    <option value="ANY">Any</option>
                                </select>
                            </div>
                            <div className="ide-col border-right" style={{padding: 15}}> of the following are TRUE</div>
                            <div className="ide-col icon border-right" style={{maxWidth: 30, padding: 15}} onClick={this._add}>
                                <i className="fal fa-plus"/>
                            </div>
                            <div className="ide-col icon" style={{maxWidth: 22, padding: 15}} onClick={this._onRemove}>
                                <i className="fal fa-trash"/>
                            </div>
                        </div>
                    </div>
                )
            } else if (typeof item == "string") {
                return <SetItem
                    data={item}
                    onRemove={this._removeSetItem}
                    onChange={this._onSetChange}
                    indicator={this.props.indicator}
                    path={`${this.props.path}.${index}`}
                    definition={this.props.definition}
                    index={index}/>
            } else {
                // This is sub-ste
                return <Set
                    data={item}
                    path={`${this.props.path}.${index}`}
                    index={index}
                    indicator={this.props.indicator}
                    definition={this.props.definition}/>
            }
        });

        return (
            <div className="set">
                {items}
            </div>
        )
    }
}

export default Set;
