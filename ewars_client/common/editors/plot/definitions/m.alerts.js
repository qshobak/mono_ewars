export default {
    name: 'Number of Alerts',
    icon: 'fa-hashtag',
    t: 'NUMERIC',
    aggs: [
        'COUNT,Count'
    ]
}