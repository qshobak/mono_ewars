export default {
    name: '# of Expected Records',
    icon: 'fa-hashtag',
    aggs: [
        'SUM,Sum'
    ]
}