export default {
    name: 'Number of Alarms',
    icon: 'fa-hashtag',
    aggs: [
        'COUNT,Count'
    ]
}