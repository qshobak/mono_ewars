export default {
    name: 'Alert Stage',
    icon: 'fa-exclamation-triangle',
    f: ['ALERT_STAGE'],
    t: 'select',
    o: [
        'VERIFICATION,Verification',
        'RISK_CHAR,Risk Characterization',
        'RISK_ASSESS,Risk Assessment',
        'OUTCOME,Outcome'
    ]
}