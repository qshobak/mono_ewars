export default {
    name: 'Location Type',
    icon: 'fa-map-marker',
    f: ['LOCATION_TYPE'],
    m: ['FIELD', 'RECORDS', 'ALERTS']
}