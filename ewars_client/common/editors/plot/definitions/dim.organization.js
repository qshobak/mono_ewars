export default {
    name: 'Organization',
    icon: 'fa-building',
    f: ['ORGANIZATION'],
    m: ['USERS', 'RECORDS', 'FIELD', 'ALERTS']
}