export default {
    name: 'Alert Outcome',
    icon: 'fa-exclamation-triangle',
    f: ['ALERT_OUTCOME'],
    m: ['ALERTS'],
    t: 'select',
    v: [
        'DISCARDED,Discarded',
        'MONITOR,Monitored',
        'RESPONSE,Response'
    ]
}