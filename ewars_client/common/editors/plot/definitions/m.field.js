export default {
    name: 'Field',
    icon: 'fa-hashtag',
    aggs: [
        'SUM,Sum',
        'MED,Median'
    ]
}