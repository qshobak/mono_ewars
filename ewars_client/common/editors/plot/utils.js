const generateId = () => {
    let id = ewars.utils.uuid();
    id = id.split('-');
    return id.pop();
};

const isX = (val) => {
    if (['true', true].indexOf(val) >= 0) return true;
    return false;
}

// const extrapolateColourRange = (range) => {
//     let steps = range.steps;
//     let colourNodes = range.rangeColours || {};
//     let rangeValues = range.rangeValues || {};
//
//     let range = [
//         [0,null]
//     ]
//
//     for (let i = 1; i <= steps; i++) {
//         if (i = 1) {
//             range[i] = [0, rangeValues[i], colour]
//         }
//     }
// }

export {
    generateId,
    isX
}