import DataMainDrop from './data/data.drop.main';
import ControlGroup from './data/data.control';
import CalcGroup from './data/data.calculation';
import Group from './data/data.group';
import ControlAxis from './data/data.axis';
import ColourAxis from './data/data.axis.colour';
import SizeAxis from './data/data.axis.size';

import {instance} from './plot_instance';

const MAIN_ACTIONS = [
    ['fa-plus', 'ADD_GROUP']
]

class DataManager extends React.Component {
    static defaultProps = {
        controls: "*",
        groups: [],
        allowMultipleGroups: true,
        allowedAxisTypes: ['x', 'y', 'custom', 'colour', 'size']
    };

    constructor(props) {
        super(props)

        this.state = {
            cmp: null
        }
    }

    _onChange = () => {
        this.props.onChange();
    };

    _addGroup = () => {
        instance.addGroup()
    };

    _action = (action) => {
        switch (action) {
            case 'ADD_GROUP':
                this._addGroup();
                return;
            default:
                return
        }
    };

    render() {
        // Pull out any calculations
        let calcs = [], controls = [], axis = [], groups = [];

        instance.groups.forEach(group => {
            groups.push(
                <Group nodeId={group._} data={group}/>
            );
        })

        instance.nodes.forEach(node => {
            if (node.t == 'C') {
                calcs.push(
                    <CalcGroup
                        nodeId={node._}
                        key={node._}/>
                )
            } else if (node.t == 'X') {
                controls.push(
                    <ControlGroup
                        key={node._}
                        nodeId={node._}/>
                )
            } else if (node.t == 'A') {
                // axis
                axis.push(
                    <ControlAxis
                        nodeId={node._}
                        key={node._}/>
                )
            } else if (node._t == 'S') {
                axis.push(
                    <SizeAxis
                        nodeId={node._}
                        key={node._}/>
                )

            } else if (node._t == 'C') {
                axis.push(
                    <ColourAxis
                        nodeId={node._}
                        key={node._}/>
                )

            }

        });

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Data">

                    {instance.canHaveMultipleGroups() ?
                        <ewars.d.ActionGroup
                            right={true}
                            onAction={this._action}
                            actions={MAIN_ACTIONS}/>
                        : null}

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            {groups}
                            {calcs}
                            <ewars.d.Row style={{flexWrap: 'wrap', alignContent: 'flex-start'}}>
                                {axis}
                                {controls}
                            </ewars.d.Row>
                            <DataMainDrop/>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.cmp}
            </ewars.d.Layout>
        )
    }

}

export default DataManager;
