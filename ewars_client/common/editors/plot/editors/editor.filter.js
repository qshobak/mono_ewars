import {instance} from '../plot_instance';

const STYLES = {
    MODAL: {
        position: "absolute",
        top: "30px",
        left: "50%",
        width: "800px",
        marginLeft: "-400px",
        maxHeight: "400px",
        boxShadow: "1px 0px 3px #000000"
    },
    WRAPPER: {
        position: "fixed",
        display: "block",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: "rgba(0,0,0,0.7)",
        zIndex: 99
    }
};

const FILTER_ACTIONS = [
    ['fa-square', 'UNCHECK'],
    ['fa-check-square', 'CHECK_ALL']
];

const MODAL_ACTIONS = [
    ['fa-save', 'SAVE'],
    ['fa-times', 'CLOSE']
];

const ADV_CONTROLS = [
    ['fa-plus', 'ADD'],
    ['fa-object-group', 'GROUP'],
    ['fa-times', 'CLEAR']
]

const DATA_GREPS = {
    LOCATION_TYPE: {
        val: 'id',
        disp: 'name',
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.query', ['location_type', null, {}, null, null, null, null])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        }
    },
    LOCATION_GROUP: {
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.location.groups', [])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        },
        val: null,
        disp: null
    },
    ALERT_STATE: [
        ['OPEN', 'Open'],
        ['CLOSED', 'Closed']
    ],
    ALERT_STAGE: [
        ['VERIFICATION', 'Verification'],
        ['RISK_ASSESS', 'Risk Assessment'],
        ['RISK_CHAR', 'Risk Characterization'],
        ['OUTCOME', 'Outcome']
    ],
    ALERT_STAGE_STATE: [
        ['PENDING', 'Pending'],
        ['COMPLETED', 'Completed']
    ],
    ALERT_OUTCOME: [
        ['DISCARDED', 'Discarded'],
        ['RESPONSE', 'Response'],
        ['MONITOR', 'Monitor']
    ],
    LOCATION_STATUS: [
        ['ACTIVE', 'Active'],
        ['DISABLED', 'Disabled']
    ],
    ALERT_RISK: [
        ['LOW', 'Low'],
        ['MODERATE', 'Moderate'],
        ['HIGH', 'High'],
        ['SEVERE', 'Severe']
    ],
    LOCATION_GEOM: [
        ['POINT', 'Point'],
        ['ADMIN', 'Admin']
    ],
    SOURCE: [
        ['ANDROID', 'Android'],
        ['DESKTOP', 'Desktop'],
        ['SYSTEM', 'System'],
        ['IMPORT', 'Import']
    ],
    USER_TYPE: [
        ['USER', 'User'],
        ['ACCOUNT_ADMIN', 'Account Administrator'],
        ['REGIONAL_ADMIN', 'Regional Administrator']
    ],
    FORM: {
        disp: 'name',
        val: 'id',
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.query', ['form', null, {}, null, null, null, null])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        }
    },
    ALARM: {
        disp: 'name',
        val: 'uuid',
        fetch: () => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.query', ['alarm', null, {}, null, null, null, null])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        }
    },
    FIELD: {
        fetch: (fieldname) => {
            return new Promise((resolve, reject) => {
                ewars.tx('com.ewars.form.field', [fieldname])
                    .then(resp => {
                        resolve(resp);
                    })
            })
        },
        val: null,
        disp: null
    }
};


// Singleton, local context for state outside of PlotInstance
let f_instance;

class FilterInstance {
    constructor(data, filters, disp, val) {
        this.data = data;
        this.filters = filters;
        this.disp = disp;
        this.val = val;

        f_instance = this;
    }

    updateComparator = (index, cmp) => {
        this.filters.forEach((item, i) => {
            if (index == i) {
                this.filters[i] = [cmp, this.filters[i].split(':')[1]].join(':');
            }
        })

    };

    toggle = (val) => {
        let rIndex;
        this.filters.forEach((item, i) => {
            if (item.split(':')[1] == val) rIndex = i;
        })

        if (rIndex == undefined) {
            this.filters.push(`EQ:val`);
        } else {
            this.filters.splice(rIndex, 1);
        }
    };

    updateValue = (index, val) => {
        this.filters.forEach((item, i) => {
            if (index == i) {
                this.filters[i] = [this.filters[i].split(':')[0], val].join(':');
            }
        })
    };

    clear = () => {
        this.filters = [];
    };

    checkAll = () => {
        let options = this.data;
        if (this.disp) {
            options = this.data.map(item => {
                return [item[this.state.val], this.state.disp]
            })
        }

        this.filters = options.map(item => {
            return `EQ:${item[0]}`;
        })
    };

    uncheckAll = () => {
        this.filters = [];
    };

    addFilter = () => {
        this.filters.push('EQ:');
    };

    addGroup = () => {
        this.filters.push([
            'ALL',
            'EQ:'
        ])
    };

    addSubRule = (index) => {
        this.filters.forEach((item, i) => {
            if (i == index) {
                item.push(`eq:`);
            }
        })
    };
}

/**
 * All items should be checked if filter is in place
 */

class FilterItem extends React.Component {
    static defaultProps = {
        disp: null,
        val: null,
        data: null,
        nid: null
    };

    constructor(props) {
        super(props);
    }

    _toggle = () => {
        let val = this.props.data[0];

        let filters = ewars.copy(this.props.filters);
        let rIndex;
        filters.forEach((item, index) => {
            if (item.split(':')[1] == val) {
                rIndex = index;
            }
        })

        if (rIndex == undefined) {
            // item doesn't exist in filters
            filters.push(`EQ:${val}`);
        } else {
            filters.splice(rIndex, 1);
        }

        this.props.onChange(filters);
    };

    render() {
        let node = instance.getNode(this.props.nid);

        let isChecked = false;
        if (this.props.filters.length > 0) {
            this.props.filters.forEach(item => {
                if (item.split(':')[1] == this.props.data[0]) isChecked = true;
            })
        }

        let icon = isChecked ? 'fal fa-check-square' : 'fal fa-square';
        let style = {padding: '8px'};
        let iconStyle = {};
        if (isChecked) iconStyle.color = 'green';
        if (this.props.nth) style.background = 'rgba(0,0,0,0.1)';

        return (
            <div className="filter-row" style={style} onClick={this._toggle}>
                <ewars.d.Row>
                    <ewars.d.Cell style={{padding: '5px'}} width="20px">
                        <i className={icon} style={iconStyle}></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{padding: '5px'}}>
                        {this.props.data[1]}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class AdvancedFilterItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        let prop = e.target.name;
        let filters = ewars.copy(this.props.filters);
        switch(prop) {
            case 'cmp':
                filters[this.props.index] = [
                    e.target.value,
                    filters[this.props.index].split(':')[1]
                ].join(':')
                this.props.onChange(filters);
                break;
            case 'val':
                filters[this.props.index] = [
                    filters[this.props.index].split(':')[0],
                    e.target.value
                ].join(':');
                this.props.onChange(filters);
                break;
            default:
                break;

        }
    }

    render() {
        let style = {
            background: 'rgba(0,0,0,0.1)',
            borderRadius: '16px',
            padding: '5px',
            margin: '5px 8px'
        };

        return (
            <div className="filter-row" style={style}>
                <ewars.d.Row>
                    <ewars.d.Cell style={{padding: '5px'}} width="20px">
                        <select
                            onChange={this._onChange}
                            value={this.props.fNode.split(':')[0]}>
                            <option value="EQ">EQ</option>
                            <option value="NEQ">NEQ</option>
                            <option value="GT">GT</option>
                            <option value="GTE">GTE</option>
                            <option value="LT">LT</option>
                            <option value="LTE">LTE</option>
                            <option value="UNDER">Under</option>
                            <option value="CONTAINS">Contains</option>
                        </select>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{padding: '5px'}}>

                        [input]
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class AdvancedFilterView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let node = instance.getNode(this.props.nid);
        return (
            <ewars.d.Panel>
                {this.props.filters.map((item, index) => {
                    return <AdvancedFilterItem
                        disp={this.props.disp}
                        onChange={this.props.onChange}
                        filters={this.props.filters}
                        val={this.props.val}
                        index={index}
                        data={this.props.data}
                        fNode={item}/>
                })}
            </ewars.d.Panel>
        )
    }
}

class DefaultFilterView extends React.Component {
    static defaultProps = {
        disp: null,
        val: null,
        data: null,
        nid: null
    }

    constructor(props) {
        super(props);
    }

    render() {
        let view;

        let node = instance.getNode(this.props.nid);

        if (this.props.options) {
            view = this.props.options.map((item, index) => {
                let isNth = index % 2 == 0;
                return (
                    <FilterItem
                        filters={this.props.filters}
                        onChange={this.props.onChange}
                        data={item}
                        nth={isNth}
                        nid={this.props.nid}/>

                )
            })
        }

        return (
            <ewars.d.Panel>
                {view}
            </ewars.d.Panel>
        )
    }
}

class RangeFilterView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="placeholder">

            </div>
        )
    }
};

class FilterEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'DEFAULT',
            data: null,
            filters: [],
            grep: null
        };

        let node = instance.getNode(this.props.nid);
        this.state.filters = node.f || [];

        if (DATA_GREPS[node.n]) {
            if (DATA_GREPS[node.n].fetch != undefined) {
                this.state.grep = DATA_GREPS[node.n];
                DATA_GREPS[node.n].fetch()
                    .then(resp => {
                        this.setState({data: resp})
                    })
            } else {
                this.state.data = DATA_GREPS[node.n];
            }
        } else {
            // This is a field
            let fieldName = node.n.split('.');
            fieldName = fieldName.slice(1, fieldName.length).join('.');
            DATA_GREPS.FIELD.fetch(fieldName)
                .then(resp => {
                    this.setState({data: resp});
                })
        }
    }

    _action = (action) => {
        let node = instance.getNode(this.props.nid);
        let filters = ewars.copy(this.state.filters || []);
        switch (action) {
            case 'UNCHECK':
                this.setState({filters: []});
                break;
            case 'CHECK_ALL':
                if (this.state.grep.disp) {
                    filters = this.state.data.map(item => {
                        return `EQ:${item[this.state.grep.val]}`;
                    })
                } else {
                    filters = this.state.data.map(item => {
                        return `EQ:${item[0]}`;
                    })
                }
                this.setState({filters: filters});
                break;
            case 'SAVE':
                let filters = this.state.filters;
                this.props.onSave(filters);
                break;
            case 'CLOSE':
                this.props.onClose();
                break;
            case 'ADD':
                filters.push('EQ:');
                this.setState({filters: filters});
                break;
            case 'DELETE':
                break;
            case 'ADD_GROUP':
                filters.push([
                    'ALL',
                    ['EQ:']
                ])
                filters.push([
                    'ALL',
                    'EQ:'
                ])
                this.setState({filters: filters});
                break;
            case 'CLEAR':
                this.setState({filters: []});
                break;
            default:
                break;
        }
    };

    _onFilterChange = (data) => {
        this.setState({
            filters: data
        })
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let view, footerControls;


        if (!this.state.data) {
            view = <div className="placeholder">Loading...</div>
        }

        let options = [];
        if (node.n.indexOf('FIELD.') >= 0) {
            if (this.state.data) {
                options = this.state.data[4].options;
            }
        } else if (this.state.grep) {
            if (this.state.data) {
                options = this.state.data.map(item => {
                    return [item[this.state.grep.val], __(item[this.state.grep.disp])];
                })
            }
        } else {
            options = this.state.data;
        }



        if (this.state.view == 'DEFAULT') {
            view = <DefaultFilterView
                options={options}
                filters={this.state.filters}
                onChange={this._onFilterChange}
                nid={this.props.nid}/>;
            footerControls = FILTER_ACTIONS;
        } else {
            view = <AdvancedFilterView
                data={this.state.data}
                onChange={this._onFilterChange}
                filters={this.state.filters}
                disp={this.state.grep.disp || null}
                val={this.state.grep.val || null}
                nid={this.props.nid}/>;
            footerControls = ADV_CONTROLS;
        }

        return (
            <div style={STYLES.WRAPPER}>
                <div style={STYLES.MODAL}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label="Filters" icon="fa-filter">
                            {/*<div className="btn-group btn-tabs">*/}
                                {/*<ewars.d.Button*/}
                                    {/*highlight={this.state.view == 'DEFAULT'}*/}
                                    {/*onClick={() => {*/}
                                        {/*this.setState({view: 'DEFAULT'})*/}
                                    {/*}}*/}
                                    {/*label="Default"/>*/}
                                {/*<ewars.d.Button*/}
                                    {/*onClick={() => {*/}
                                        {/*this.setState({view: 'ADVANCED'})*/}
                                    {/*}}*/}
                                    {/*highlight={this.state.view == 'ADVANCED'}*/}
                                    {/*label="Advanced"/>*/}
                            {/*</div>*/}
                            <ewars.d.ActionGroup
                                right={true}
                                height="35px"
                                onAction={this._action}
                                actions={MODAL_ACTIONS}/>

                        </ewars.d.Toolbar>
                        <ewars.d.Row height="300px" style={{minHeight: '300px', background: '#333333'}}>
                            <ewars.d.Cell>
                                {view}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <ewars.d.Toolbar>
                            <ewars.d.ActionGroup
                                right={true}
                                height="35px"
                                actions={footerControls}
                                onAction={this._action}/>

                        </ewars.d.Toolbar>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

export default FilterEditor;