const DIMENSIONS = [
    ["D", "DATE", "Date", {}],
    ["D", "LOCATION", "Location", {}]
]

class Dimension extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="block" draggable={true}>
                <div className="block-content">
                    {this.props.data[2]}
                </div>
            </div>
        )
    }
}

class Dimensions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel>
                <div className="block-tree" style={{position: "relative"}}>
                    {DIMENSIONS.map((item) => {
                        return <Dimension
                            key={item[1]}
                            data={item}/>
                    })}
                </div>
            </ewars.d.Panel>
        )
    }
}

export default Dimensions;