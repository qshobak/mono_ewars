import {instance} from '../plot_instance';

class NavigatorEditor extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (e) => {
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        return (
            <div className="control-editor">
                <div className="control-field">
                    <ewars.d.Row>
                        <ewars.d.Cell width="30%">Height</ewars.d.Cell>
                        <ewars.d.Cell>
                            <input
                                type="number"
                                name="height"
                                onChange={this._onChange}
                                value={node.height || '0'}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

export default NavigatorEditor;