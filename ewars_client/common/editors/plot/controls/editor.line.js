import {instance} from '../plot_instance';
import {ControlField} from '../components'


const DEFAULTS = {
    type: 'LINE',
    _: null,
    c: {
        axis: '<id>', // id of axis to target
        pos: 1, // end position along axis,
        line: 'red', // line colour for the band
        name: 'Threshold' // Label to plot along the band
    }
};

const FIELDS = [
    ['color', 'color', 'Colour'],
    ['dashStyle', 'select', 'Dash style'],
    ['label', 'text', 'Label'],
    ['value', 'number', 'Value'],
    ['width', 'number', 'width']
]

class LineEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let fromControl, toControl;

        let axis = instance.getAxis();

        let selectedAxis = axis.filter(item => {
            if (item._ == node.node.axis) return true;
        })[0] || null;


        if (selectedAxis) {
            switch (selectedAxis._t) {
                case 'DATE':
                case 'date':
                    fromControl = <input
                        onChange={this._onChange}
                        type="date"
                        value={node.node.from}
                        name="from"/>;
                    break;
                case 'numeric':
                case 'NUMERIC':
                case 'NUM':
                    fromControl = <input
                        onChange={this._onChange}
                        type="number"
                        value={node.node.from}
                        name="from"/>;
                    break;
                default:
                    break;

            }
        }

        return (
            <div className="control-editor">
                <ControlField label="Label">
                    <input
                        onChange={this._onChange}
                        type="text"
                        name="label"
                        value={node.node.label || undefined}/>
                </ControlField>
                <ControlField label="Stroke color" key="borderColor">
                    <input
                        onChange={this._onChange}
                        type="color"
                        name="strokeColour"
                        value={node.node.strokeColour || undefined}/>
                </ControlField>
                <ControlField label="Line style">
                    <select
                        name="dashStyle"
                        value={node.node.dashStyle || 'Solid'}
                        onChange={this._onChange}>
                        <option value="Solid">Solid</option>
                        <option value="ShortDash">Short Dash</option>
                        <option value="ShortDot">Short Dot</option>
                        <option value="ShortDashDot">Short Dash Dot</option>
                        <option value="ShortDashDotDash">Short Dash Dot Dash</option>
                        <option value="Dot">Dot</option>
                        <option value="Dash">Dash</option>
                        <option value="LongDash">Long Dash</option>
                        <option value="DashDot">Dash Dot</option>
                        <option value="LongDashDot">Long Dash Dot</option>
                        <option value="LongDashDotDash">Long Dash Dot Dash</option>
                    </select>
                </ControlField>
                <ControlField label="Stroke width" key="borderWidth">
                    <input
                        onChange={this._onChange}
                        type="number"
                        name="strokeWidth"
                        value={node.node.strokeWidth || undefined}/>
                </ControlField>
                <ControlField label="Axis">
                    <select
                        onChange={this._onChange}
                        name="axis"
                        value={node.node.axis || undefined}>
                        <option value="">No selection</option>
                        {instance.getAxis().map(item => {
                            return <option value={item._}>{item.node.label}</option>;
                        })}
                    </select>
                </ControlField>
                <ControlField label="From">
                    {fromControl}
                </ControlField>
            </div>
        )
    }
}

export default LineEditor;