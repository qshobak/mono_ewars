import {ControlField} from '../components'

import {instance} from '../plot_instance';

const DEFAULTS = {
    type: 'BAND',
    _: null,
    c: {
        orientation: 'v', // v = vertical, h = horizontal
        start: 0, // start position along axis
        end: 1, // end position along axis,
        fill: 'transparent', // fill colour for the band
        line: 'red', // line colour for the band
        name: 'Threshold' // Label to plot along the band
    }
};

class BandEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let fromControl, toControl;

        let axis = instance.getAxis();

        let selectedAxis = axis.filter(item => {
            if (item._ == node.node.axis) return true;
        })[0] || null;


        console.log(selectedAxis);
        if (selectedAxis) {
            switch (selectedAxis._t) {
                case 'DATE':
                case 'date':
                    fromControl = <input
                        onChange={this._onChange}
                        type="date"
                        value={node.node.from}
                        name="from"/>;
                    toControl = <input
                        onChange={this._onChange}
                        type="date"
                        value={node.node.to}
                        name="to"/>;
                    break;
                case 'numeric':
                case 'NUMERIC':
                case 'NUM':
                    fromControl = <input
                        onChange={this._onChange}
                        type="number"
                        value={node.node.from}
                        name="from"/>;
                    toControl = <input
                        onChange={this._onChange}
                        type="number"
                        value={node.node.to}
                        name="to"/>;
                    break;
                default:
                    break;

            }
        }

        return (
            <div className="control-editor">
                <ControlField label="Label">
                    <input
                        onChange={this._onChange}
                        type="text"
                        name="label"
                        value={node.node.label || undefined}/>
                </ControlField>
                <ControlField label="Border Color" key="borderColor">
                    <input
                        onChange={this._onChange}
                        type="color"
                        name="borderColor"
                        value={node.node.borderColor || undefined}/>
                </ControlField>
                <ControlField label="Border Width" key="borderWidth">
                    <input
                        onChange={this._onChange}
                        type="number"
                        name="borderWidth"
                        value={node.node.borderWidth || undefined}/>
                </ControlField>
                <ControlField label="Color" key="color">
                    <input
                        onChange={this._onChange}
                        type="color"
                        name="color"
                        value={node.node.color || undefined}/>
                </ControlField>
                <ControlField label="Axis">
                    <select
                        onChange={this._onChange}
                        name="axis"
                        value={node.node.axis || undefined}>
                        <option value="">No selection</option>
                        {instance.getAxis().map(item => {
                            return <option value={item._}>{item.node.label}</option>;
                        })}
                    </select>
                </ControlField>
                <ControlField label="From">
                    {fromControl}
                </ControlField>
                <ControlField label="To">
                    {toControl}
                </ControlField>
            </div>
        )
    }
}

export default BandEditor;