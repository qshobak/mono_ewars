import ControlField from '../components/ui.control_field';

import {instance} from '../plot_instance';

const POSITIONS = [
    ['TL', 'Top-left'],
    ['TC', 'Top-Center'],
    ['TR', 'Top-Right'],
    ['ML', 'Mid-Left'],
    ['MR', 'Mid-right'],
    ['BL', 'Bottom-left'],
    ['BC', 'Bottom-center'],
    ['BR', 'Bottom-right']
];

class MeasureSelect extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        if (this.props.value.indexOf(this.props.data._) >= 0) {
            let result = ewars.copy(this.props.value);
            let removalIndex = this.props.value.indexOf(this.props.data._);
            result.splice(removalIndex, 1);
            this.props.onChange(result);
        } else {
            let newList = ewars.copy(this.props.value);
            newList.push(this.props.data._);
            this.props.onChange(newList);
        }
    };

    render() {
        let checked = false;
        if (this.props.value.indexOf(this.props.data._) >= 0) checked = true;
        return (
            <div className="tick-item">
                <input
                    checked={checked}
                    onChange={this._onChange}
                    type="checkbox"/>&nbsp;{this.props.data.node.label || this.props.data.n}
            </div>
        )
    }
}

class StackEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    _onMeasuresChange = (data) => {
        instance.updateNodeProp(this.props.nid, 'targets', data);
    };

    render() {
        let node = instance.getNode(this.props.nid);

        let measures = instance.nodes.filter(n => {
            return ['C', 'M'].indexOf(n.t) >= 0 && !n.p;
        });

        let stackItemOptions = [];
        measures.forEach(item => {
            stackItemOptions.push(
                <option value={item._}>{item.node.label || item.n}</option>
            )
        });

        return (
            <div className="control-editor">
                <ControlField label="Mode">
                    <select
                        name="mode"
                        onChange={this._onChange}
                        value={node.node.mode || 'normal'}>
                        <option value="normal">Normal</option>
                        <option value="percentage">Percentage</option>
                    </select>
                </ControlField>
                <ControlField label="Measures">
                    {measures.map(item => {
                        return <MeasureSelect
                            onChange={this._onMeasuresChange}
                            data={item}
                            value={node.node.targets || []}/>
                    })}
                </ControlField>
            </div>
        )
    }
}

export default StackEditor;