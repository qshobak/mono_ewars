import Preview from "../Preview";

const STYLE = {
    border: "1px solid #000000",
    position: "absolute",
    top: 10,
    right: 10,
    bottom: 10,
    left: 10,
    background: "#F2F2F2"
}

class Chart extends React.Component {
    _width = null;
    _height = null;

    constructor(props) {
        super(props);

        this.state = {
            width: null,
            height: null
        }
    }

    componentDidMount() {
        this._chart = new Preview(this._el, this.props.data, this.props.ds);
    }

    componentWillReceiveProps(nextProps) {
        if (this._chart) {
            this._chart.update(this.props.data, this.props.ds);
        } else {
            this._chart = new Preview(this._el, this.props.data, this.props.ds);
        }
    }

    render() {


        return (
            <div
                ref={(el) => {
                    this._el = el
                }}
                className="plot-chart"
                style={STYLE}>
            </div>
        )
    }
}

export default Chart;

