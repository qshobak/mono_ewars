import {instance} from '../plot_instance';

import {ICON_MAP, X_COLOUR_MAP} from '../constants/'


class NuggetDetail extends React.Component {

    _label = 'Unknown';

    constructor(props) {
        super(props);

        let node = instance.getNode(props.nid);

        // Set up standard properties for all nuggets
        if (node._t == 'D') this._icon = ICON_MAP.detail;

        this._label = node.getShortLabel();

    }

    render() {
        return (
            <div className="nugget">
                <ewars.d.Row>
                    <ewars.d.Cell width="20px">
                        <i className={this._icon}></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {this._label}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default NuggetDetail;
