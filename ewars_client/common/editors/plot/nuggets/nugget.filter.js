import {ModalDialog} from '../editors';
import ContextMenu from '../ContextMenu';

import {preset_names, presets} from '../constants/';
import * as ctx_menus from '../menus'

import Base from './base';

class NuggetFilter extends Base {
    constructor(props) {
        super(props);

        this.state = {
            showFilterEditor: false
        }
    }

    render() {
        return (
            <div className="nugget" style={this._style} onClick={this._toggle}>
                <ewars.d.Row>
                    <ewars.d.Cell width="20px">
                        <i className={this._icon}></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell>{this._label}</ewars.d.Cell>
                    <ewars.d.Cell width="20px" style={{textAlign: "center", display: "block"}}>
                        <i className="fal fa-caret-down"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.show ?
                    <ContextMenu
                        actions={ctx_menu}
                        onClick={this._action}/>
                    : null}
                {this.state.editor ?
                    <ModalDialog
                        type="FILTER"
                        gid={this.props.gid}
                        nid={this.props.nid}
                        filters={this.props.filters[this.props.nid] || []}
                        onSave={this._onFilterSave}
                        data={this.props.data}
                        onClose={() => this._action("CLOSE")}/>
                    : null}
            </div>
        )
    }
}

export default NuggetFilter;
