import NuggetDetail from './nugget.detail';
import NuggetFilter from './nugget.filter';
import NuggetColour from './nugget.colour';
import NuggetSize from './nugget.size';

export {
    NuggetDetail,
    NuggetFilter,
    NuggetColour,
    NuggetSize
}