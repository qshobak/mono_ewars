import {instance} from '../plot_instance';

import {ICON_MAP, X_COLOUR_MAP} from '../constants/'


class NuggetColour extends React.Component {

    _label = 'Unknown';

    constructor(props) {
        super(props);

        let node = instance.getNode(props.nid);

        // Set up standard properties for all nuggets
        if (node._t == 'F') this._icon = ICON_MAP.filter;
        if (node._t == 'C') this._icon = ICON_MAP.colour;
        if (node._t == 'S') this._icon = ICON_MAP.size;
        if (node._t == 'D') this._icon = ICON_MAP.detail;

        this._label = node.getShortLabel();

        if (node.t == 'D') {
            if (node.n.indexOf('FIELD.') >= 0) {

            }
        }
    }

    render() {
        return (
            <div className="nugget">
                <ewars.d.Row>
                    <ewars.d.Cell width="20px">
                        <i className={this._icon}></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {this._label}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default NuggetColour;
