const PERC_MENU = [
    ["R_PERC_5", "5", null, null],
    ["R_PERC_10", "10", null, null],
    ["R_PERC_25", "25", null, null],
    ["R_PERC_50", "50", null, null],
    ["R_PERC_75", "75", null, null],
    ["R_PERC_90", "90", null, null],
    ["R_PERC_95", "95", null, null]
]

const REDUCTION_MENU = [
    ["R_SUM", "Sum", null, null],
    ["R_AVERAGE", "Average", null, null],
    ["R_MEDIAN", "Median", null, null],
    ["R_COUNT", "Count", null, null],
    ["R_COUNT_D", "Count (Distinct)", null, null],
    "-",
    ["R_MIN", "Minimum", null, null],
    ["R_MAX", "Maximum", null, null],
    [null, "Percentile", null, PERC_MENU],
    ["R_STD_DEV", "Std. Dev.", null, null],
    ["R_STD_DEV_P", "Std. Dev. (Pop.)", null, null],
    ["R_VAR", "Variance", null, null],
    ["R_VAR_P", "Varians (Pop.)", null, null]
];


const RENDER_MENU = [
    ["LT_LINE", "Line", null, null],
    ["LT_COL", "Column", null, null],
    ["LT_BAR", "Bar", null, null],
    ["LT_SCATTER", "Scatter", null, null],
    ["LT_AREA", "Area", null, null]
];

const CALCULATIONS = [
    ["CALC_RUN_TOTAL", "Running Total", null, null],
    ["CALC_DIFFERENCE", "Difference", null, null],
    ["CALC_PERC_DIFF", "% Difference", null, null],
    ["CALC_PERC_OF_TOTAL", "% of Total", null, null],
    ["CALC_RANK", "Rank", null, null],
    ["CALC_PERCENTILE", "Percentile", null, null],
    ["CALC_MOV_AVG", "Moving Average", null, null],
    ["CALC_YTD_TOTAL", "YTD Total", null, null],
    ["CALC_YTD_GROWTH", "YTD Growth", null, null]
]

const TEST_MENU = [
    ["FILTER", "Filter...", "fa-filter", null],
    "-",
    [null, "Reduction", null, REDUCTION_MENU],
    [null, "Quick Calculation", null, CALCULATIONS],
    "-",
    [null, "Rendering", null, RENDER_MENU],
    "-",
    ["TOOLTIP", "Tooltip...", "fa-comment", null],
    ["STYLE", "Style...", "fa-paint-brush", null],
    "-",
    ["REMOVE", "Remove", "fa-trash", null]
]


const STYLES = {
    MENU: {
        maxWidth: "200px",
        minWidth: "120px",
        display: 'block',
        position: 'fixed',
        top: 0,
        left: 0,
        background: '#F2F2F2',
        borderRadius: '3px',
        border: '1px solid rgba(0,0,0,0.1)'
    },
    SUB_MENU: {
        position: "absolute",
        top: "-3px",
        left: "100%",
        marginLeft: "1px",
        maxWidth: "200px",
        minWidth: "120px",
        background: "#F2F2F2",
        minHeight: "100px",
        border: "1px solid #333333",
        borderRadius: "3px"
    },
    MENU_INNER: {
        display: "block",
    },
    MENU_ITEM: {
        position: "relative",
        display: "block",
        padding: "5px",
        color: "#333333",
        cursor: "pointer"
    },
    LINE: {
        height: "1px",
        background: "#333333",
        marginTop: "3px",
        marginBottom: "3px",
        display: "block"
    }
}


class Separator extends React.Component {
    render() {
        return (
            <div style={STYLES.LINE}></div>
        )
    }
}

class MenuItem extends React.Component {
    static defaultProps = {
        onClick: null,
        _el: null
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            show: !this.state.show
        })
    };

    _onClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        this.props.onClick(this.props.data[0]);
    };

    render() {

        if (this.props.data[3]) {
            return (
                <div style={STYLES.MENU_ITEM}>
                    <ewars.d.Row onClick={this._toggle}>
                        {this.props.data[2] ?
                            <ewars.d.Cell width="20px">
                                <i className={"fal " + this.props.data[2]}></i>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell>{this.props.data[1]}</ewars.d.Cell>
                        <ewars.d.Cell width="20px">
                            <i className="fal fa-caret-right"></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    {this.state.show ?
                        <div style={STYLES.SUB_MENU}>
                            <div style={STYLES.MENU_INNER}>
                                {this.props.data[3].map((item) => {
                                    if (item == "-") {
                                        return <Separator/>;
                                    }
                                    return <MenuItem onClick={this.props.onClick} data={item}/>
                                })}
                            </div>
                        </div>
                        : null}
                </div>
            )
        }

        return (
            <div style={STYLES.MENU_ITEM} onClick={this._onClick}>
                <ewars.d.Row>
                    {this.props.data[2] ?
                        <ewars.d.Cell width="20px">
                            <i className={"fal " + this.props.data[2]}></i>
                        </ewars.d.Cell>
                        : null}
                    <ewars.d.Cell>
                        {this.props.data[1]}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )

    }
}

class ContextMenu extends React.Component {
    static defaultProps = {
        actions: TEST_MENU
    };

    constructor(props) {
        super(props);
    }

    _onClick = (action) => {
        this.props.onClick(action);
    };

    componentDidMount() {
        if (this._el) {
            let parent = this._el.parentNode.getBoundingClientRect();
            this._el.style.left = (parent.x + 2) + 'px';
            this._el.style.top = (parent.y + 25) + 'px';
        }
    }

    render() {
        return (
            <div
                ref={(el) => {
                    this._el = el;
                }}
                className="context-menu"
                style={STYLES.MENU}>
                <div style={STYLES.MENU_INNER}>
                    {this.props.actions.map((item) => {
                        if (item == "-") {
                            return <Separator/>
                        }

                        return <MenuItem onClick={this._onClick} data={item}/>
                    })}

                </div>
            </div>
        )
    }
}
;

export default ContextMenu;



