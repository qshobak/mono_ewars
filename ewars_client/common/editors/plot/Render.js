class Render extends React.Component {
    _node = null;

    constructor(props) {
        super(props);

        ewars.subscribe("DATA_UPDATED", this._dataUpdated);
    }

    _dataUpdated = (data) => {
        if (this._el && data) {
            if (this._node && this._el) {
                this._el.innerHTML = "";
            }

            this._node = document.createElement("div");
            this._node.setAttribute("class", "bk-root")
            let innerChild = document.createElement("div");
            innerChild.setAttribute("class", "bk-plotdiv");
            innerChild.setAttribute("id", data.d.elementid);
            this._node.appendChild(innerChild);
            this._el.appendChild(this._node);

            eval(data.s);
        }
    };

    componentDidMount() {
        let data = this.props.ds.getData();
        this._node = document.createElement("div");
        this._el.appendChild(this._node);
    }

    componentWillUnmount() {
        if (this._node) {
            if (this._el) this._el.removeChild(this._node);
            if (this._el) this._el.innerHTML = "";
            this._node = null;
        }

        if (this._el) this._el.innerHTML = "";

        ewars.unsubscribe("DATA_UPDATED", this._dataUpdated);
    }

    render() {
        return (
            <div
                ref={el => this._el = el}
                style={{
                    position: "absolute",
                    top: "8px",
                    left: "8px",
                    right: "8px",
                    bottom: "8px",
                    display: "block"
                }}>

            </div>
        )
    }
}

export default Render;
