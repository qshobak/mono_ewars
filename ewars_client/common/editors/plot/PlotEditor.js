import ControlList from "./ControlList";
import StyleEditor from './StyleEditor'

import {PIE_STYLE, MAP_STYLE, STYLE, DP_STYLE, HEATMAP_STYLE, TABLE_STYLE} from './constants/';
import {getDefaults} from './defaults';
import {instance, PlotInstance} from './plot_instance';


import DimensionTree from '../../cmp/analysis/DimensionTree';
import MeasureTree from '../../cmp/analysis/MeasureTree';
import SourceView from './view.source';

import Preview from './Preview';

import DataManager from './DataManager';

const OVERLAY_STYLE = {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 999,
    background: "#333333"
};

ewars.g.FORMS = {};


const ANALYSIS_DROP = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "rgba(0,0,0,0.1)",
    display: "none",
    textAlign: "center",
    fontSize: "30px",
    color: "#FFFFFF",
    zIndex: 9
};

class AnalysisDrop extends React.Component {
    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._onShowDrop);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    _onShowDrop = (type) => {
        if (type == "A") {
            if (this._el) this._el.style.display = "block";
        }
    };

    _hideDrops = () => {
        if (this._el) this._el.style.display = "none";
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        ewars.emit("HIDE_DROPS");
    };

    render() {
        return (
            <div
                onDragOver={this._onDragOver}
                onDrop={this._onDrop}
                ref={el => {
                    this._el = el
                }}
                style={ANALYSIS_DROP}>
                <i className="fal fa-download"></i>
            </div>
        )
    }
}

class PlotEditor extends React.Component {
    _plot = null;

    constructor(props) {
        super(props);

        let data = props.data.g ? props.data : getDefaults(props.data.type);
        let instance = new PlotInstance(data);

        this.state = {
            mainView: 'DATA',
            view: 'DATA',
            sidebar: true,
            data: instance
        };

        ewars.subscribe('PLOT_UPDATED', this._onChange);
    }

    componentWillUnmount() {
        ewars.unsubscribe('PLOT_UPDATED', this._onChange);
    }

    _onChange = () => {
        this.setState({data: this.state.data});
    };

    _onSave = () => {
        let final = instance.getFinal();
        this.props.onSave(final);
    };

    _action = (action) => {
        ewars.z.dispatch("PE", "UNEDIT_FILTER");
    };

    _onStyleChange = (data) => {
        ewars.z.dispatch('PE', 'UPDATE_CONFIG', data);
    };

    _toggleSidebar = () => {
        this.setState({
            sidebar: !this.state.sidebar
        })
    };


    render() {
        let leftView;

        if (this.state.view == "DATA") {
            leftView = (
                <ewars.d.Layout>
                    <ewars.d.Toolbar label="Dimensions"/>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <DimensionTree/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Toolbar label="Measures"/>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <MeasureTree/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            )
        }
        if (this.state.view == "ANALYSIS") leftView = <AnalysisTools/>;
        if (this.state.view == "CONTROLS") leftView = <ControlList/>;
        if (this.state.view == "STYLE") {
            let styler;
            if (instance.type != 'DATAPOINT') styler = STYLE;
            if (instance.type == 'DATAPOINT') styler = DP_STYLE;
            if (instance.type == 'HEATMAP') styler = HEATMAP_STYLE;
            if (instance.type == 'MAP') styler = MAP_STYLE;
            if (instance.type == 'PIE') styler = PIE_STYLE;
            if (instance.type == 'TABLE') styler = TABLE_STYLE;
            leftView = <StyleEditor
                definition={styler}
                data={this.state.config}
                onChange={this._onStyleChange}/>

        }

        let modalView;
        if (this.state.modal) {

        }

        let availTypes = [];
        let items = availTypes.map((item) => {
            return (
                <ewars.d.Button
                    label={item}
                    onClick={() => {
                        ewars.z.dispatch("PE", "SET_TYPE", item);
                    }}/>
            )
        })

        let view;
        let render_type;

        let mainView;
        if (this.state.mainView == 'DEFAULT') mainView = <Preview data={instance.getFinal()}/>;
        if (this.state.mainView == 'DATA') mainView = <DataManager data={this.state}/>;
        if (this.state.mainView == 'SOURCE') mainView = <SourceView data={this.state}/>;


        return (
            <div style={OVERLAY_STYLE}>
                <ewars.d.Layout>
                    <ewars.d.Toolbar label="Plot Editor">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this._onSave}
                                icon="fa-save"/>
                            <ewars.d.Button
                                onClick={this.props.onClose}
                                icon="fa-times"/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        {this.state.sidebar ?
                            <ewars.d.Cell width="350px" borderRight={true}>
                                <ewars.d.Layout>
                                    <ewars.d.Toolbar>
                                        <div className="btn-group btn-tabs">
                                            <ewars.d.Button
                                                highlight={this.state.view == "DATA"}
                                                onClick={() => {
                                                    this.setState({view: 'DATA'})
                                                }}
                                                icon="fa-database"/>
                                            <ewars.d.Button
                                                highlight={this.state.view == "CONTROLS"}
                                                onClick={() => {
                                                    this.setState({view: 'CONTROLS'});
                                                }}
                                                icon="fa-gamepad"/>
                                            <ewars.d.Button
                                                highlight={this.state.view == "STYLE"}
                                                onClick={() => {
                                                    this.setState({view: 'STYLE'});
                                                }}
                                                icon="fa-paint-brush"/>
                                        </div>

                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                onClick={this._toggleSidebar}
                                                icon="fa-angle-double-left"/>
                                        </div>

                                    </ewars.d.Toolbar>
                                    <ewars.d.Row>
                                        <ewars.d.Cell>
                                            {leftView}
                                        </ewars.d.Cell>
                                    </ewars.d.Row>
                                </ewars.d.Layout>
                            </ewars.d.Cell>
                            :
                            <ewars.d.Cell width="45px" borderRight={true}>
                                <ewars.d.Layout>
                                    <ewars.d.Toolbar>
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                icon="fa-angle-double-right"
                                                onClick={this._toggleSidebar}/>
                                        </div>
                                    </ewars.d.Toolbar>
                                </ewars.d.Layout>
                            </ewars.d.Cell>
                        }
                        <ewars.d.Cell style={{background: "rgb(218,218,218)"}}>
                            <ewars.d.Layout>
                                <ewars.d.Toolbar>
                                    <div className="btn-group btn-tabs">
                                        <ewars.d.Button
                                            highlight={this.state.mainView == "DATA"}
                                            onClick={() => {
                                                this.setState({mainView: 'DATA'});
                                            }}
                                            label="Data"
                                            icon="fa-database"/>
                                        <ewars.d.Button
                                            highlight={this.state.mainView == "DEFAULT"}
                                            onClick={() => {
                                                this.setState({mainView: 'DEFAULT'});
                                            }}
                                            label="Preview"
                                            icon="fa-eye"/>
                                        <ewars.d.Button
                                            highligh={this.state.mainView == 'SOURCE'}
                                            onClick={() => {
                                                this.setState({mainView: 'SOURCE'});
                                            }}
                                            label="Source"
                                            icon="fa-code"/>
                                    </div>
                                </ewars.d.Toolbar>
                                <ewars.d.Row>
                                    <ewars.d.Cell>
                                        <AnalysisDrop/>
                                        {mainView}
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                            </ewars.d.Layout>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

export default PlotEditor;
