class BoolButton extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = () => {
        if (this.props.value == true || this.props.value == 'true') {
            this.props.onChange({
                target: {
                    name: this.props.name || null,
                    value: false
                }
            })
        } else {
            this.props.onChange({
                target: {
                    name: this.props.name || null,
                    value: true
                }
            })
        }
    };

    render() {
        let iconClass = 'fal fa-toggle-off';
        let style = {
            fontSize: '18px',
            color: '#F2F2F2',
            paddingTop: '3px',
            cursor: 'pointer'
        }
        if (['true', true].indexOf(this.props.value) >= 0) {
            iconClass = 'fal fa-toggle-on';
            style.color = 'green';
        }
        return (
            <div
                className="bool-button"
                style={style}
                onClick={this._click}>
                <i className={iconClass}></i>
            </div>
        )
    }

}

export default BoolButton;