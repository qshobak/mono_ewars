class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <ewars.d.Row>
                    <ewars.d.Cell width="30%">{this.props.label}</ewars.d.Cell>
                    <ewars.d.Cell>
                        {this.props.children}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

export default ControlField;