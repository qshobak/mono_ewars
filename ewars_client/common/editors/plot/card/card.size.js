import {instance} from '../plot_instance';

class CardSize extends React.Component {
    constructor(props) {
        super(props);

        ewars.subscribe('SHOW_DROPS', this._showDrops);
        ewars.subscribe('HIDE_DROPS', this._hideDrops);
    }

    componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    };

    componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);

        ewars.unsubscribe('SHOW_DROPS', this._showDrops);
        ewars.unsubscribe('HIDE_DROPS', this._hideDrops);
    };

    _showDrops = (type) => {
        if (['M', 'D'].indexOf(type) >= 0) {
            if (this._ael) this._ael.style.border = '1px solid #F2F2F2';
        }
    };

    _hideDrops = () => {
        if (this._ael) this._ael.style.border = '1px solid #505152';
    };

    _onToggle = () => {
        if (this._el) {
            if (this._shown) {
                this._shown = false;
                this._el.style.display = "none";
            } else {
                this._shown = true;
                this._el.style.display = "block";
            }
        }
    };

    handleBodyClick = (evt) => {
        if (this._el) {
            const area = this._el;

            if (!area.contains(evt.target)) {
                this._shown = false;
                this._el.style.display = "none";
            }
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData('n'));

        ewars.emit('HIDE_DROPS');
        instance.addSizeNode(this.props.nid, data);
    };

    _set = (style) => {
        this._shown = false;
        if (this._el) this._el.style.display = "none";
    };

    render() {
        return (
            <div
                onDrop={this._onDrop}
                onDragOver={this._onDragOver}
                ref={(el) => {this._ael = el;}}
                className="plot-settings">
                <div className="inner" onClick={this._onToggle}>
                    <div className="icon"><i className="fal fa-signal"></i></div>
                    <div className="text">Size</div>
                </div>
                <div
                    ref={el => this._el = el}
                    className="axis-drop-options">
                    <div className="axis-drop-inner">
                        <label htmlFor="lineWidth">Line width</label>
                        <div className="padder">
                            <input
                                min={0.1}
                                max={5}
                                step={0.1}
                                list="widths"
                                onChange={this._setWidth}
                                type="range"/>
                            <datalist id="widths">
                                <option value="0.1" label="0.1">0.1</option>
                                <option value="0.2">0.2</option>
                                <option value="0.3">0.3</option>
                                <option value="0.4">0.4</option>
                                <option value="0.5">0.5</option>
                                <option value="0.6">0.6</option>
                                <option value="0.7">0.7</option>
                                <option value="0.8">0.8</option>
                                <option value="0.9">0.9</option>
                                <option value="1" label="1">1.0</option>
                                <option value="1.5">1.5</option>
                                <option value="2">2</option>
                                <option value="2.5">2.5</option>
                                <option value="3">3</option>
                                <option value="3.5">3.5</option>
                                <option value="4">4</option>
                                <option value="4.5">4.5</option>
                                <option value="5" label="5">5</option>
                            </datalist>
                        </div>


                    </div>
                </div>
            </div>
        )
    }
}

export default CardSize;
