import {instance} from '../plot_instance';


class CardDetail extends React.Component {
    defaultBorder = "1px solid #505152";

    constructor(props) {
        super(props)

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentWillUnmount() {
        ewars.unsubscribe("SHOW_DROPS", this._showDrops);
        ewars.unsubscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = () => {
        if (this._el) this._el.style.border = "1px solid #F2F2F2";
    };

    _hideDrops = () => {
        if (this._el) this._el.style.border = this.defaultBorder;
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        ewars.emit('HIDE_DROPS');
        let data = JSON.parse(e.dataTransfer.getData('n'));
        instance.addDetailNode(this.props.nid, data);
    };

    render() {
        return (
            <div
                ref={el => this._el = el}
                onDragOver={this._onDragOver}
                onDrop={this._onDrop}
                className="plot-settings">
                <div className="icon"><i className="fal fa-info"></i></div>
                <div className="text">Detail</div>
            </div>
        )
    }
}

export default CardDetail;