import LocationSelectField from "../../../common/components/fields/LocationSelectField.react";

const ACTIONS = [
    ["fa-plus", "ADD"]
];

const STYLE = {
    FILTER: {
        display: "block",
        borderRadius: "3px",
        background: "steelblue",
        padding: "5px",
        marginBottom: "8px",
        color: "#333333"
    }
};

const FILTERS = {
    STI: [],
    GROUP: [],
    STATUS: [],
    PARENT: []
};

class Filter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={STYLE.FILTER}>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        Filter name
                    </ewars.d.Cell>
                    <ewars.d.Cell width="20px">
                        <i className="fal fa-caret-down"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class LocationFiltering extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Filter/>


            </div>
        )
    }
}

class CardLocation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT"
        }
    }

    _onChange = (prop, value) => {
        let uuid = this.props.data[0];
        let config = this.props.data[3];
        config.period = value;
        ewars.z.dispatch("PE", "UPDATE_DIM", [uuid, config]);
    };


    render() {
        let conf = this.props.data[3];

        return <LocationSelectField
            name="lid"
            onUpdate={this._onChange}
            value={conf.lid}/>;
    }
}

export default CardLocation;