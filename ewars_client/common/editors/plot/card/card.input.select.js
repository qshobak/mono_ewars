import {instance} from '../plot_instance';

class InlineSelectField extends React.Component {
    static defaultProps = {
        options: [],
        name: null,
        gid: null,
        nid: null
    };

    constructor(props) {
        super(props);

    }

    componentWillMount() {
        window.__hack__.addEventListener('click', this._handleBodyClick);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener('click', this._handleBodyClick);
    }

    _handleBodyClick = (e) => {

    }

    _onToggle = () => {
        if (this._el) {
            if (this._shown) {
                this._shown = false;
                this._el.style.display = "none";
            } else {
                this._shown = true;
                this._el.style.display = "block";
            }
        }
    };

    _handleBodyClick = (evt) => {
        if (this._el) {
            const area = this._el;

            if (!area.contains(evt.target)) {
                this._shown = false;
                this._el.style.display = "none";
            }
        }
    };

    _set = (value) => {
        console.log(value, this.props.nid, this.props.name);
        this._shown = false;
        if (this._el) this._el.style.display = 'none';

        instance.updateNodeProp(this.props.nid, this.props.name, value);
    };

    render() {

        let label = 'Unset';
        if (this.props.value) {
            this.props.options.forEach(item => {
                if (item[0] == this.props.value) label = item[1];
            })
        }

        return (
            <div className="style-selector">
                <div className="style-handle" onClick={this._onToggle}>
                    <ewars.d.Row>
                        <ewars.d.Cell borderRight={true}>{label}</ewars.d.Cell>
                        <ewars.d.Cell width="20px" style={{textAlign: "center"}}>
                            <i className="fal fa-caret-down"></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                <div
                    ref={el => this._el = el}
                    className="style-options">
                    <div className="style-opts-inner">
                        {this.props.options.map(item => {
                            return (
                                <div
                                    key={item[0]}
                                    onClick={() => {
                                        this._set(item[0])
                                    }}
                                    className="style-opt">
                                    {item[1]}
                                </div>
                            )
                        })}
                    </div>

                </div>
            </div>
        )
    }

}

export default InlineSelectField;