import Dimension from '../Dimension';
import Measure from '../Measure';

import {
    COLORS, INTERVALS,
    REDUCTIONS, PRESET_NAMES,
    LINE_STYLES, DASH_STYLES,
    NUM_FORMATS
} from '../constants/';

import Card from './card';

import CardColor from './card.colour';
import CardSize from './card.size';
import CardLabel from './card.label';
import CardMarker from './card.marks';
import CardDetail from './card.details';
import CardTooltip from './card.tooltip';
import CardFilters from './card.filters';

import InlineSelectField from './card.input.select';
import VarNameTextField from './card.input.var';

import {instance} from '../plot_instance';

import {NuggetColour, NuggetDetail, NuggetSize} from '../nuggets';

const CARD_ACTIONS = [
    ["fa-paint-brush", "STYLE"],
    ["fa-text-width", "LABELS"],
    ["fa-comment", "TOOLTIP"],
    ["fa-square", "MARKER"]
];

const CONTROL_TYPES = {
    COLOR: CardColor,
    SIZE: CardSize,
    LABEL: CardLabel,
    MARKERS: CardMarker,
    DETAIL: CardDetail,
    TOOLTIP: CardTooltip
}

const AVAILABLES = {
    DEFAULT: ['COLOR', 'SIZE', 'LABEL', 'DETAIL', 'TOOLTIP', 'MARKERS'],
    MAP: ['COLOR', 'LABEL', 'DETAIL', 'TOOLTIP'],
    PIE: ['COLOR', 'LABEL', 'DETAIL', 'TOOLTIP'],
    DATAPOINT: [],
    HEATMAP: ['COLOR', 'LABEL', 'DETAIL', 'TOOLTIP'],
    FUNNEL: ['COLOR', 'LABEL', 'DETAIL', 'TOOLTIP'],
    PYRAMID: ['COLOR', 'LABEL', 'DETAIL', 'TOOLTIP'],
    TABLE: ['COLOR', 'LABEL'],
    SPARK: ['COLOR', 'SIZE', 'LABEL', 'DETAIL', 'TOOLTIP']
};




class CardItem extends React.Component {
    static defaultProps = {
        nodeId: null
    };

    constructor(props) {
        super(props);

        this.state = {
            view: "FILTER",
            showShade: false,
            shadeView: "DEFAULT",
            cmp: null
        }

    }

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("n"));

        instance.addFilterToNode(this.props.nodeId, data);
    };

    _action = (action) => {
        if (action == "CLOSE") this.setState({showShade: false});
        if (action == "STYLE") this.setState({showShade: true, shadeView: "STYLE"})
    };

    render() {

        let node = instance.getNode(this.props.nodeId);
        let isDate = node.isDate(),
            isDimension = node.isDimension(),
            isMeasure = node.isMeasure(),
            isCalculation = node.isCalculation(),
            isLocation = node.isLocation(),
            label = node.getShortLabel();

        if (isCalculation) label = 'Calculation';

        let style = {};
        if (isDimension) style.background = 'rgba(40,49,66,1)';
        if (isCalculation) style.background = 'rgba(58,32,1,1)';

        let showStyleSelect = true,
            showAggSelect = true,
            showVarNameSelect = this.props.isCalcVariable;


        showStyleSelect = ((isMeasure || isCalculation) && !this.props.isCalcVariable && instance.type == 'DEFAULT');
        let showDashSelect = showStyleSelect && ['line', 'area'].indexOf(node.node.render) >= 0;
        let showControls = isMeasure || isCalculation;

        if (node._t == 'CX') {
            showStyleSelect = false;
            showDashSelect = false;
            showControls = false;
            showVarNameSelect = true;
        }

        let colours = instance.getNodeColours(node._);
        colours = colours.map(subNode => {
            return <NuggetColour nid={subNode._}/>
        });

        let details = instance.getNodeDetails(node._);
        details = details.map(subNode => {
            return <NuggetDetail nid={subNode._}/>;
        })

        let sizers = instance.getNodeSizers(node._);
        sizers = sizers.map(subNode => {
            return <NuggetSize nid={subNode._}/>;
        });

        if (instance.type == 'DATAPOINT') {
            showDashSelect = false;
            showStyleSelect = false;
        }


        let controlNodes = [];
        if (AVAILABLES[instance.type]) {
            AVAILABLES[instance.type].forEach(controlType => {
                let Cmp = CONTROL_TYPES[controlType];
                controlNodes.push(
                    <ewars.d.Cell key={node._ +  '_' + controlType + '_NCX'} width="33.333%" style={{minWidth: '30%'}}>
                        <Cmp nid={node._}/>
                    </ewars.d.Cell>
                )
            })
        }

        let showNumFormat = false;
        if (['M', 'C'].indexOf(node.t) >= 0 && instance.type == 'TABLE' && !node.p) showNumFormat = true;
        let showDimLabel = false;
        if (node.t == 'D' && instance.type == 'TABLE') showDimLabel = true;

        return (
            <Card
                style={style}
                nid={this.props.nodeId}
                title={label}>
                {showDimLabel ?
                    <VarNameTextField
                        name="label"
                        nid={node._}
                        value={node.node.label || ''}/>
                : null}
                {showVarNameSelect ?
                    <VarNameTextField
                        name="var"
                        nid={node._}
                        value={node.node.var || ''}/>
                    : null}
                {showStyleSelect ?
                    <InlineSelectField
                        name="render"
                        nid={node._}
                        value={node.node.render || ''}
                        options={LINE_STYLES}/>
                    : null}
                {showDashSelect ?
                    <InlineSelectField
                        name="dashStyle"
                        nid={node._}
                        value={node.node.dashStyle || undefined}
                        options={DASH_STYLES}/>
                    : null}
                {isMeasure || isCalculation ?
                    <InlineSelectField
                        name="agg"
                        nid={node._}
                        options={REDUCTIONS}
                        value={node.node.agg || ''}/>
                    : null}
                {isDimension && isDate ?
                    <InlineSelectField
                        name="interval"
                        nid={node._}
                        options={INTERVALS}
                        value={node.node.interval || 'DAY'}/>
                    : null}
                {showNumFormat ?
                    <InlineSelectField
                        name="format"
                        nid={node._}
                        options={NUM_FORMATS}
                        value={node.node.format || '0'}/>
                : null}
                {isLocation ?
                    <div className="placeholder">Location Settings</div>
                    : null}
                {showControls ?
                    <div className="plot-marks">
                        <ewars.d.Row style={{flexWrap: 'wrap', justifyContent: 'flex-start'}}>
                            {controlNodes}
                        </ewars.d.Row>
                    </div>
                    : null}
                <div style={{minHeight: "100px"}}
                     onDrop={this._onDrop}
                     onDragOver={this._onDragOver}>
                    <CardFilters nid={node._}/>
                    {colours}
                    {details}
                    {sizers}
                </div>
                {this.state.cmp}
            </Card>
        )

    }
}

export default CardItem;
