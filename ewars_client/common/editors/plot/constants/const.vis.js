const BASE = {
    hasAxis: true, // Does the chart have an axis
    allowCalcs: true, // Does the chart allow calculations
    allowMultiGroups: true, // Does the chart allow multiple groups
    allowMultiAxis: true, // Does the chart allow multiple axis
    allowStacking: true, // Does the chart allow stacking series
    allowSize: true, // Can measures in the series be sized?
    allowLabel: true, // Can measures in the series have custom labelling
    allowMarkers: true, // can measures have custom markers
    allowDetails: true, // Can details be set for a measure in this chart
    allowTooltip: true, // Can tooltip be customized for measures in this chart
    allowStyle: true, // Allow selecting the style for a measure
    allowColor: true, // Allow setting a measure as the color for a measure
    allowBulletTarget: false, // Whether to show the bullet target
    allowValueFormat: true, // Allow formatting the value
    controls: [
        'LINE',
        'BAND',
        'TEXT',
        'LEGEND',
        'SCALE',
        'TITLE',
        'SUBTITLE',
        'TREND',
        'ANNOTATION',
        'NAVIGATOR',
        'POLYGON',
        'MAP_LAYER',
        'MAP_POINT',
        'MAP_POLY',
        'LOG',
        'BELL'
    ]
}

export default {
    DEFAULT: {
        ...BASE,
        controls: [
            'LINE',
            'BAND',
            'TEXT',
            'LEGEND',
            'TITLE',
            'SCALE',
            'SUBTITLE',
            'ANNOTATION',
            'NAVIGATOR',
            'LOG',
            'BELL'
        ]
    },
    PIE: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowMarkers: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'LEGEND',
            'SCALE',
            'COLOUR',
            'TITLE',
            'SUBTITLE'
        ]
    },
    SPARK: {
        ...BASE
    },
    MAP: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowMarkers: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'LEGEND',
            'SCALE',
            'TITLE',
            'COLOUR',
            'SUBTITLE',
            'MAP_LAYER',
            'MAP_POINT',
            'MAP_POLY'
        ]
    },
    HEATMAP: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowMarkers: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'LEGEND',
            'TITLE',
            'SUBTITLE',
            'COLOUR'
        ]
    },
    FUNNEL: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowMarkers: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    STREAM: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    GAUGE: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    PYRAMID: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowStyle: fasle,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    TREEMAP: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowSize: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    BULLET: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowMultiGroups: false,
        allowSize: false,
        allowBulletTarget: true,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'COLOUR',
            'LEGEND'
        ]
    },
    DATAPOINT: {
        ...BASE,
        allowMultiAxis: false,
        allowStacking: false,
        allowMultiGroups: false,
        allowSize: false,
        allowStyle: false,
        controls: [
            'TEXT',
            'TITLE',
            'SUBTITLE',
            'ICON',
            'PREFIX',
            'SUFFIX'
        ]
    }
}