// LINE STYLES
export default [
    ["bar", "Bar"],
    ["column", "Column"],
    ["line", "Line"],
    ["area", "Area"],
    ["scatter", "Scatter"],
    ['step', 'Step']
];
