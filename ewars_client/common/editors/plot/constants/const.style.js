export default {
    t_chart: {
        t: 'H',
        l: 'chart settings'
    },
    showGrid: {
        l: 'show grid',
        t: 'bool',
        d: false
    },
    reload: {
        l: 'reloading',
        t: 'bool',
        d: false,
        __: {
            reloadInterval: {l: 'Interval (secs)', t: 'number', d: 60}
        }
    },
    widgetBox: {
        l: 'boxed',
        t: 'bool',
        d: false,
        __: {
            boxHeaderBackground: {l: 'Header bgd.', t: 'color', d: 'transaprent'},
            boxBorder: {l: 'Border', t: 'text', d: '1px solid #333333'}
        }
    },
    exportTools: {
        l: 'exporting',
        t: 'bool',
        d: false
    },
    zoomable: {
        l: 'zoomable',
        t: 'bool',
        d: false
    },
    plotBackgroundColor: {
        l: 'plot bgd',
        t: 'color',
        d: 'transparent'
    }
};