// INTERVALS
export default [
    ['DAY', 'Daily (Default)'],
    ['WEEK', 'Weekly'],
    ['MONTH', 'Monthly'],
    ['YEAR', 'Annually']
];
