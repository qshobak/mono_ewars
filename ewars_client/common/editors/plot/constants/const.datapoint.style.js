export default {
    t_value: {
        t: 'H',
        l: 'Widget'
    },
    fontSize: {
        t: 'number',
        l: 'Font Size'
    },
    bold: {
        t: 'bool',
        l: 'Bold'
    },
    fontUnderline: {
        t: 'bool',
        l: 'Underline'
    },
    fontColor: {
        t: 'color',
        l: 'Font colour'
    },
    valueFormat: {
        t: 'select',
        l: 'Formatting',
        o: [
            ['0', '0'],
            ['0.0', '0.0'],
            ['0.00', '0.00'],
            ['0,0.0', '0,0.0'],
            ['0,0.00', '0,0.00'],
            ['0,0', '0,0'],
            ['0%', '0%'],
            ['0.0%', '0.0%'],
            ['0.00%', '0.00%']
        ]
    },
    prefix: {
        t: 'text',
        l: 'Prefix'
    },
    suffix: {
        t: 'text',
        l: 'Suffix'
    }
};