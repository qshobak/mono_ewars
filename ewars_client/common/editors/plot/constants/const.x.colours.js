export default {
    'D': '#333333',
    'M': 'rgba(0,0,0,0.8)', // dark grey
    'C': 'rgb(58, 32, 1)' // umber/burnt orange
};