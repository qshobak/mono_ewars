export default {
    t_chart: {
        t: 'H',
        l: 'chart settings'
    },
    showLabels: {
        t: 'bool',
        l: 'Show labels',
        o: [
            [false, 'No'],
            [true, 'Yes']
        ]
    },
    pBorder: {
        t: 'bool',
        l: 'Border'
    },
    borderColor: {
        t: 'color',
        l: 'Border color'
    },
    borderWidth: {
        t: 'number',
        l: 'Border width'
    },
    explode: {
        t: 'bool',
        l: 'Explode'
    },
    donut: {
        t: 'bool',
        l: 'Donut',
        o: [
            [false, 'No'],
            [true, 'Yes']
        ]
    },
    reload: {
        l: 'reloading',
        t: 'bool',
        d: false,
        __: {
            reloadInterval: {l: 'Interval (secs)', t: 'number', d: 60}
        }
    },
    exportTools: {
        l: 'exporting',
        t: 'bool',
        d: false
    }
};