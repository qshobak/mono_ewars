import DataGrid from "../../../lib/datagrid/c.datagrid";

class VizEditor extends HTMLElement {
    constructor() {
        super();

        const shadow = this.attachShadow({mode: "open"});

        const text = document.createElement("span");
        text.textContent = "Hello";

        const grid = document.createElement("data-grid");

        shadow.appendChild(grid);
    }

}

window.customElements.define("viz-editor", VizEditor);
export default VizEditor;
