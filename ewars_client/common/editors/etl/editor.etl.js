import { Layout, Row, Cell } from "../../layout";
import Button from "../../c.button";
import Toolbar from "../../c.toolbar";

const LogicUtils = {

}

const getIndicators = (etl) => {
    return new Promise((resolve, reject) => {
        if (Object.keys((etl || {})).length > 0) {
            let uuids = Object.keys(etl).filter(item => {
                return item.indexOf("NEW") < 0;
            });

            if (uuids.length > 0) {
                let query = {uuid: {in: uuids}};

                ewars.tx("com.ewars.query", ["indicator", ["uuid", "name"], query, null, null, null, null, null])
                    .then(res => {
                        let inds = {};
                        res.forEach(item => {
                            inds[item.uuid] = items;
                        });
                        resolve(inds);
                    })
                    .catch(err => {
                        reject(err);
                    })
            } else {
                resolve({});
            }
        } else {
            resolve({});
        }
    })
}

const renderRules = (version, indicators, actionFn) => {
    let asArray = Object.keys(version.etl).map(key => {
        return {
            ...version.etl[key],
            i: key
        };
    });

    asArray.forEach(item => {
        if (item.o == undefined || item.o == null) {
            item.o = -1;
        }
    });

    asArray  = asArray.sort((a, b) => {
        if (a.o > b.o) return 1;
        if (a.o < b.o) return -1;
        return 0;
    });

    asArray.map((item, index) => {
        return (
            <Rule
                key={`IND_${item.i}`}
                indicator={item.i}
                index={index}
                indicators={indicators}
                definition={version.definition}
                onAction={actionFn}/>
        );
    });
}

const addNewRule = (etl) => {
    let uuid = ewars.utils.uuid();
    etl[`NEW_${uuid}`] = {
        i: null,
        o: -1,
        r: "SUM",
        n: []
    }

    return etl;
}

const removeRuleAtIndex = (etl, index) => {
    return etl;
}

class Mapping extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

    }
}

class Rule extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}

class ETLEditor extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            indicators: {}
        }
    }

    componentDidMount() {
        getIndicators(this.props.form.version.etl)
            .then(res => {
                this.setState({
                    indicators: res
                });
            })
    }

    _action = (action, data) => {
        switch(action) {
            case "ADD":
                this.props.onChange(addRule(this.props.form.version.etl));
                break;
            case "REMOVE":
                this.props.onChange(removeRuleAtIndex(this.props.form.version.etl, index));
                break;
            case "RULE_CHANGE":
                let data = ewars.copy(this.props.form.version.etl);
                data[uuid] = data;
                this.props.onChange(data);
                break;
            case "INDICATOR_CHANGE":
                let data = ewars.copy(this.props.form.version.etl);
                data[replace] = ewars.copy(data[orig]);
                data[replace].i = replace;
                delete data[orig];
                this.props.onChage(data);
                break;
            default:
                break;
        }

    };

    render() {
        let definition = ewars.copy(this.props.form.version.definition || {});

        let indicators = renderRules(
            this.props.form.version,
            this.state.indicators,
            this._action
        );

        if (indicators.length < 0) indicators = <Spinner/>;


        return (
            <Layout style={{flexDirection: "row"}}>
                <Cell width="200px" borderRight={true}>

                </Cell>
                <Cell>
                    <Toolbar>
                        <div className="btn-group pull-right">
                            <Button
                                label="Add Rule"
                                icon="fa-plus"
                                onClick={() => this._action("ADD")}/>
                        </div>
                    </Toolbar>
                    <Row style={{overflowY: "auto"}}>
                        {indicators}
                    </Row>
                </Cell>
            </Layout>
        )

    }
}

export default ETLEditor;
