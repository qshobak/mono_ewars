import Dispatcher from "../../dispatchers/ClientDispatcher";
import Constants from "./FormDefinitionConstants";

export default {
    setDefinition: function (def) {
        Dispatcher.handleViewAction({
            actionType: Constants.SET_DEFINITION,
            data: def
        })
    },
    setExposedFieldByUUID: function (uuid) {
        Dispatcher.handleViewAction({
            actionType: Constants.SET_EXPOSED_FIELD,
            data: uuid
        })
    },
    moveField: function (uuid, curIndex, newIndex) {
        Dispatcher.handleViewAction({
            actionType: Constants.MOVE_FIELD,
            data: {
                uuid: uuid,
                curIndex: curIndex,
                newIndex: newIndex
            }
        })
    },
    addFieldChild: function (uuid) {
        Dispatcher.handleViewAction({
            actionType: Constants.ADD_FIELD_CHILD,
            data: uuid
        })
    },
    setFieldProp: function (uuid, prop, value) {
        Dispatcher.handleViewAction({
            actionType: Constants.RECEIVE_FIELD_PROP,
            data: {
                uuid: uuid,
                prop: prop,
                value: value
            }

        })
    },
    deleteField: function (uuid) {
        Dispatcher.handleViewAction({
            actionType: Constants.DELETE_FIELD,
            data: uuid
        })

    },
    addField: function(field) {
        Dispatcher.handleViewAction({
            actionType: Constants.ADD_FIELD,
            data: field
        })
    }
};
