import FieldDefinition from "./FieldDefinition.react";
import FormUtils from "../../utils/FormUtils";


function _iterateChildren(childs, fn, basePath, parent) {
    if (!basePath) basePath = "";
    for (var i in childs) {
        var path = basePath + "." + i;
        var child = childs[i];
        fn(i, child, childs, path.slice(1, path.length), parent);

        if (child.fields) {
            _iterateChildren(child.fields, fn, path, child);
        }
    }
}

function _deleteField(uuid, sourceData) {
    _iterateChildren(sourceData, function (name, child, items) {
        if (child.uuid == uuid) {
            delete items[name];
        }
    })
}

function _duplicateField(uuid, sourceData) {
    let field;

    _iterateChildren(sourceData, function (name, child, items) {
        if (child.uuid == uuid) {
            let newField = ewars.copy(items[name]);
            let uuid = ewars.utils.uuid();
            let newName = "NEW_" + ewars.utils.uuid().split("-")[0];
            newField.uuid = uuid;
            newField.label = newField.label || {en: ""};
            newField.order = 99999999;
            items[newName] = newField;

            // Iterate nested cells
            if (newField.type == "row") {
                for (var i in newField.fields) {
                    newField.fields[i].uuid = ewars.utils.uuid();
                }
            }

            // iterate matrix children down to cells and update uuids
            if (newField.type == "matrix") {
                for (var n in newField.fields) {
                    let subField = newField.fields[n];
                    newField.fields[n].uuid = ewars.utils.uuid();

                    for (var t in subField.fields) {
                        subField.fields[t].uuid = ewars.utils.uuid();
                    }
                }
            }


            var arrayed = _getFieldsAsArray(items);
            arrayed.sort(_comparator);
            items = _getArrayAsObject(arrayed);
        }
    })
}

function _findParentNode(parentName, childObj) {
    var testObj = childObj.parentNode;
    var count = 1;
    while (testObj.getAttribute('name') != parentName) {
        alert("My name is " + testObj.getAttribute("name") + ". Lets try moving up one level");
        testObj = testObj.parentNode;
        count++;
    }
}

function _normalize(sourceFields) {
    Object.keys(sourceFields).forEach(key => {
        if (!sourceFields[key].uuid) sourceFields[key].uuid = ewars.utils.uuid();

        if (sourceFields[key].fields) _normalize(sourceFields[key].fields);
    });
}

function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields, parentId) {
    var fields = [];
    for (let token in sourceFields) {
        let fieldCopy = ewars.copy(sourceFields[token]);
        fieldCopy.name = token;
        fieldCopy.parentId = parentId;
        if (!fieldCopy.uuid) fieldCopy.uuid = ewars.utils.uuid();

        if (fieldCopy.fields) {
            var children = _getFieldsAsArray(fieldCopy.fields, fieldCopy.uuid);
            fields.push.apply(fields, children);
        }

        delete fieldCopy.fields;

        fields.push(fieldCopy);
    }

    return fields.sort(_comparator);
}

function _getArrayAsObject(sourceArray) {
    var fieldDict = {};
    var resultDict = {};

    sourceArray.forEach(item => {
        if (fieldDict[item.uuid]) {
            item.uuid = ewars.utils.uuid();
        }
        fieldDict[item.uuid] = item;
    });

    for (let i in fieldDict) {
        let item = fieldDict[i];
        var parent = fieldDict[item.parentId];
        if (parent) {
            parent.fields = parent.fields || {};
            parent.fields[item.name] = item;
        } else {
            resultDict[item.name] = item;
        }
    }

    return resultDict;
}

function _resort(parentId, sourceData) {
    var toSort = [];
    var fields = _getFieldsAsArray(sourceData);

    var fieldDict = {};
    fields.forEach(item => {
        fieldDict[item.uuid] = item;
    });

    let items = fields.forEach(item => {
        if (item.parentId == parentId) toSort.push(item);
    });

    var sortedResult = toSort.sort(_comparator);

    var count = 0;
    sortedResult.forEach(item => {
        item.order = count;
        fieldDict[item.uuid] = item;
        count++;
    });

    let result = _getArrayAsObject(Object.keys(fieldDict).map(item => {
        return fieldDict[item];
    }));

    return result;
}

const groupBy = function(xs, key) {
    return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
}

/**
 * Fixes numbering issues
 * @param items {array}
 * @returns asArray {array}
 */
function _fixNumbering(items) {
    var asArray = _getFieldsAsArray(items);

    let groups = groupBy(asArray, "parentId");
    var result = [];

    Object.keys(groups).forEach(key => {
        groups[key].sort(_comparator);
        let count = 0;
        groups[key].forEach(item => {
            item.order = count;
            result.push(item);
            count++;
        });
    });

    return _getArrayAsObject(result)
}

const DROPPABLE_TYPES = [
    ["text", "Text field"],
    ['date', 'Date field'],
    ['location', 'Location field'],
    ['number', 'Numeric field'],
    ['header', 'Header field'],
    ['textarea', 'Textarea field'],
    ['select', 'Select field'],
    ['matrix', 'Table'],
    ['display', 'Display field'],
    ['lat_long', 'Lat/Lng field'],
    ['calculated', 'Calculated'],
    ['time', 'Time Input']
    // ["repeater", "Repeater"],
    // ['progress', "Progress bar"],
    // ['image', 'Image upload'],
    // ['file', 'File upload']
];

const ICONS = {
    text: "fa-font",
    textarea: "fa-align-left",
    date: "fa-calendar",
    display: "fa-square",
    number: "fa-hashtag",
    location: 'fa-map-marker',
    lat_long: "fa-location-arrow",
    header: "fa-heading",
    matrix: "fa-th",
    row: "fa-rectangle-wide",
    select: "fa-list-alt",
    calculated: "fa-calculator",
    time: "fa-clock"
    // repeater: "fa-copy",
    // image: "fa-image",
    // progress: "fa-spinner",
    // file: "fa-file"
};

var FormDefinitionComponent = React.createClass({
    displayName: "Form Definition Component",

    _tmpNumber: 0,

    getInitialState: function () {
        return {
            fields: {},
            definition: {},
            view: "edit",
            dummyForm: {
                data: {}
            }
        }
    },

    componentWillMount: function () {
        this.state.definition = _fixNumbering(ewars.copy(this.props.definition));
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.definition) {
            this.state.definition = _fixNumbering(ewars.copy(nextProps.definition));
        }
    },

    shouldComponentUpdate: function () {
        return true;
    },

    propTypes: {
        onChange: React.PropTypes.func.isRequired,
        definition: React.PropTypes.object.isRequired
    },

    /**
     * Handles adding a new field to the definition
     * @private
     */
    _addField: function () {
        var id = ewars.utils.uuid();
        var name = "untitled_" + id.split("-")[0];

        var high = -1;

        Object.keys(this.state.definition).forEach(key => {
            if (this.state.definition[key].order > high) high = this.state.definition[key].order;
        });

        var field = {
            name: name,
            uuid: id,
            label: {en: "New Field"},
            type: "text",
            order: high + 1,
            required: false
        };

        this.state.definition[field.name] = field;
        _normalize(this.state.definition);
        this.props.onChange(this.state.definition);
    },

    _showPreview: function () {
        this.setState({
            view: "preview"
        })
    },

    _hidePreview: function () {
        this.setState({
            view: "edit"
        })
    },

    _onFieldChange: function (fieldUUID, prop, value) {
        var arrFields = _getFieldsAsArray(this.state.definition);

        let collision = false;

        if (prop == "name") {
            let editing, conflict;
            // Find the field we're editing
            arrFields.forEach((field) => {
                if (field.uuid == fieldUUID) {
                    editing = field;
                    field.name = value;
                }
            });

            // Find any matches
            arrFields.forEach((field) => {
                if (field.name == value && field.uuid != fieldUUID) conflict = field;
            });

            if (conflict) {
                // there's a conflict, how do we deal with it
                // Prepend DUPE_; this is hidden in the editor
                editing.name = "DUPE_" + value;
            }
        } else {
            arrFields.forEach((field) => {
                if (field.uuid == fieldUUID) {
                    field[prop] = value;
                }
            })
        }
        var data = _getArrayAsObject(arrFields);
        _normalize(data);
        this.props.onChange(data);
    },

    _addChild: function (uuid) {
        var arrFields = _getFieldsAsArray(this.state.definition);

        let parent = arrFields.find(item => {
            return item.uuid == uuid
        });

        var newType = 'text';
        if (parent.type == "matrix") newType = "row";

        var id = ewars.utils.uuid();
        var name = "untitled_" + id.split("-")[0];

        // Get the highest ordered child
        var high = -1;
        arrFields.forEach(arrField => {
            if (arrField.parentId == uuid) {
                if (parseInt(arrField.order) > high) high = parseInt(arrField.order);
            }
        });

        arrFields.push({
            name: name,
            label: {en: "New Field"},
            uuid: id,
            required: false,
            type: newType,
            order: high + 1,
            parentId: uuid
        });

        var data = _getArrayAsObject(arrFields);
        _normalize(data);
        this.props.onChange(data);
    },

    _onDeleteField: function (uuid) {
        _deleteField(uuid, this.state.definition);
        this.state.definition = _fixNumbering(this.state.definition);
        this.props.onChange(this.state.definition);
    },

    _onDuplicate: function (uuid) {
        _duplicateField(uuid, this.state.definition);
        this.state.definition = _fixNumbering(this.state.definition);
        this.props.onChange(this.state.definition);
    },

    _bigMove: function (moving, target) {
        let fields = _getFieldsAsArray(this.state.definition);
        let movingIndex,
            targetIndex;

        fields.forEach((item, index) => {
            if (item.uuid == moving) {
                movingIndex = index;
            }
            if (item.uuid == target) {
                targetIndex = index;
            }
        });

        let movingItem = fields.filter(item => {
            return item.uuid == moving;
        })[0] || null;

        fields = fields.sort(_comparator);

        let result = [];
        fields.forEach(item => {
            if (item.uuid != moving) {
                if (item.uuid == target) {
                    result.push(item);
                    result.push(movingItem);
                } else {
                    result.push(item);
                }
            }
        })

        let counter = 0;
        result.forEach(item => {
            item.order = counter;
            counter++;
        })

        this.state.definition = _getArrayAsObject(result);
        this.props.onChange(this.state.definition);
    },

    _dropNew: function (data, target) {
        let fields = _getFieldsAsArray(this.state.definition);

        fields = fields.sort(_comparator);

        data.order = 0;
        let _s = data.uuid.split("-");
        data.name = "untitled_" + _s[_s.length - 1];

        let result = [];

        if (target) {
            fields.forEach(item => {
                result.push(item);
                if (item.uuid == target) {
                    result.push(data);
                }
            });
        } else {
            result = fields;
            result.push(data);
        }

        let counter = 0;
        result.forEach(item => {
            item.order = counter;
            counter++;
        });

        this.state.definition = _getArrayAsObject(result);
        this.props.onChange(this.state.definition);
    },

    _moveFieldUp: function (field, direction) {
        var fields = _getFieldsAsArray(this.state.definition);
        let targetField = fields.find(f => { return f.uuid == field });
        let groups = groupBy(fields, "parentId");

        var targetGroup = groups[targetField.parentId];
        targetGroup.sort(_comparator);

        // Get its current indev
        var curIndex = null;
        targetGroup.forEach((item, index) => {
            if (item.uuid == field) curIndex = index;
        });

        if (curIndex <= 0) return;

        var targetIndex = curIndex - 1;
        var tmp = targetGroup[targetIndex];
        targetGroup[targetIndex] = targetField;
        targetGroup[curIndex] = tmp;

        targetGroup.forEach((item, index) => {
            item.order = index;

            fields.forEach(field => {
                if (field.uuid == item.uuid) field.order = index;
            });
        });

        this.state.definition = _getArrayAsObject(fields);
        this.props.onChange(this.state.definition);
    },

    _moveFieldDown: function (field, direction) {
        var fields = _getFieldsAsArray(this.state.definition);
        let targetField = fields.find(f => { return f.uuid == field; });
        let groups = groupBy(fields, "parentId");

        var targetGroup = groups[targetField.parentId];
        targetGroup.sort(_comparator);

        // Get its current indev
        var curIndex = null;
        targetGroup.forEach((item, index) => {
            if (item.uuid == field) curIndex = index;
        });

        if (curIndex >= targetGroup.length - 1) return;

        var targetIndex = curIndex + 1;
        var tmp = targetGroup[targetIndex];
        targetGroup[targetIndex] = targetField;
        targetGroup[curIndex] = tmp;

        targetGroup.forEach((item, index) => {
            item.order = index;

            fields.forEach(field => {
                if (field.uuid == item.uuid) field.order = index;
            });
        });

        this.state.definition = _getArrayAsObject(fields);
        this.props.onChange(this.state.definition);
    },

    /**
     * Works reursively to resolve a hierarchical structure for the form definition in order
     * to display the content in a meaningful way.
     * @returns {*}
     * @private
     * @param sourceFields {Array} Fields to process
     */
    _processFields: function (sourceFields, optionFields) {

    },

    _handleDummyFormUpdate: function (data, prop, value) {
        var data = this.state.dummyForm;
        ewars.setKeyPath(data, prop, value);
        this.setState({
            dummyForm: data
        })
    },

    _onDragOver: function (e) {
        e.preventDefault();
    },

    _onDrop: function (e) {
        let data = e.dataTransfer.getData("e");
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New field",
                required: false,
                uuid: ewars.utils.uuid()
            }
            this._dropNew(newField, null);
        }
    },

    render: function () {
        var formFields = FormUtils.asOptions(this.state.definition);

        let fields = [];
        let fieldSet = [];

        for (let i in this.state.definition) {
            fieldSet.push({
                ...this.state.definition[i],
                name: i
            })
        }
        fieldSet = fieldSet.sort(_comparator);

        let _dragOver, _onDrop;
        if (fieldSet.length > 0) {
            fields = fieldSet.map((field, index) => {
                return (
                    <FieldDefinition
                        fieldOptions={formFields}
                        data={field}
                        order={index + 1}
                        isRoot={true}
                        bigMove={this._bigMove}
                        dropNew={this._dropNew}
                        moveFieldUp={this._moveFieldUp}
                        moveFieldDown={this._moveFieldDown}
                        onAddChild={this._addChild}
                        onChange={this._onFieldChange}
                        onDelete={this._onDeleteField}
                        onDuplicate={this._onDuplicate}
                        key={field.uuid}/>
                )
            })
        } else {
            _dragOver = this._onDragOver;
            _onDrop = this._onDrop;
            fields = (
                <div style={{textAlign: "center", marginTop: "100px", display: "block"}}>
                    <i style={{fontSize: "90px"}} className="fal fa-caret-circle-down"></i>
                    <div style={{fontSize: "14px", padding: "20px 0"}}>Drag a field here to start</div>
                </div>
            )
        }


        if (this.state.view == "edit") {
            return (
                <ewars.d.Layout>
                    <ewars.d.Row>
                        <ewars.d.Cell borderRight={true} style={{overflowY: "auto"}} width="300px">
                            <div className="block-tree">
                                {DROPPABLE_TYPES.map(item => {
                                    return (
                                        <div
                                            onDragStart={(e) => {
                                                e.dataTransfer.setData("e", JSON.stringify({
                                                    new: true,
                                                    type: item[0]
                                                }))
                                                setTimeout(() => {
                                                    window.dispatchEvent(new CustomEvent("show-drops"));
                                                    ewars.emit("SHOW_DROPS", null);
                                                })
                                            }}
                                            onDragEnd={function () {
                                                window.dispatchEvent(new CustomEvent("hide-drops"));
                                                ewars.emit("HIDE_DROPS");
                                            }}
                                            draggable={true}
                                            className="block">
                                            <div className="block-content" style={{display: "flex"}}>
                                                <ewars.d.Cell width="20px">
                                                    <i className={"fal " + ICONS[item[0]] || "fa-list"}></i>
                                                </ewars.d.Cell>
                                                <ewars.d.Cell>
                                                    {item[1]}
                                                </ewars.d.Cell>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            <ewars.d.Toolbar label="Definition">
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        icon="fa-plus"
                                        label="Add Field"
                                        colour="green"
                                        onClick={this._addField}/>
                                    <ewars.d.Button
                                        icon="fa-eye"
                                        label="Preview"
                                        onClick={this._showPreview}/>
                                </div>
                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <div style={{display: "block", overflowY: "auto", flex: 2}} onDragOver={_dragOver}
                                     onDrop={_onDrop}>
                                    <div className="block-tree" style={{position: "relative"}}>
                                        {fields}
                                    </div>
                                </div>
                            </ewars.d.Row>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            )
        }

        if (this.state.view == "preview") {
            return (
                <ewars.d.Layout>
                    <ewars.d.Toolbar label="Preview">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-times"
                                label="Close Preview"
                                onClick={this._hidePreview}/>

                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <ewars.d.Panel style={{padding: 16}}>
                                <ewars.d.Form
                                    readOnly={false}
                                    updateAction={this._handleDummyFormUpdate}
                                    definition={this.props.definition}
                                    data={this.state.dummyForm}
                                    onCancel={this._hidePreview}/>
                            </ewars.d.Panel>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            )
        }

        return view;
    }
});

export default FormDefinitionComponent;
