const FORM = [
    {
        _o: 0,
        l: "General",
        t: "H",
        i: "fa-cog"
    },
    {
        _o: 1,
        n: "title",
        t: "TEXT",
        l: "DATA_SOURCE_NAME"
    },
    {
        _o: 2,
        m: "color",
        n: "colour",
        t: "COLOUR",
        l: "Colour"
    },
    {
        _o: 3,
        t: "H",
        i: "fa-code-branch",
        l: "Data Source"
    },
    {
        _o: 4,
        t: "DATASOURCEDROP",
        n: "sources",
        l: "Source(s)",
        r: true
    },
    {
        _o: 5,
        t: "H",
        i: "fa-map-marker",
        l: "Source location"
    },
    {
        _o: 6,
        n: "location_spec",
        t: "BUTTONSET",
        l: "Source location",
        o: [
            ["SPECIFIC", "Specific location"],
            ["GENERATOR", "Location(s) of type"],
            ["GROUPS", "Location group(s)"]
        ]
    },
    {
        _o: 7,
        n: "location",
        t: "LOCATION",
        l: "Location",
        c: [
            "ANY",
            "location_spec:=:SPECIFIC",
            "location_spec:=:GENERATOR"
        ]
    },
    {
        _o: 8,
        n: "site_type_id",
        l: "Location type",
        t: "SELECT",
        os: [
            "location_type",
            "id",
            "name",
            {}
        ],
        c: [
            "ALL",
            "location_spec:=:GENERATOR"
        ]
    },
    {
        _o: 9,
        n: "groups",
        l: "Location group(s)",
        t: "LOCGROUP",
        c: [
            "ALL",
            "location_spec:=:GROUPS"
        ]
    },
    {
        _o: 10,
        l: "Source time period",
        t: "H",
        i: "fa-calendar"
    },
    {
        _o: 11,
        n: "period_source",
        l: "Period",
        t: "BUTTONSET",
        o: [
            ["INHERIT", "Inherit"],
            ["OVERRIDE", "Override"]
        ]
    },
    {
        _o: 12,
        n: "period",
        t: "PERIOD",
        l: "Date range",
        c: [
            "ALL",
            "period_source:=:OVERRIDE"
        ]
    },
    {
        _o: 13,
        n: "reduction",
        l: "Reduction",
        t: "BUTTONSET",
        o: [
            ["SUM", "Sum/Cumulative"],
            ["AVG", "Average/Median"]
        ]
    }
];

export default FORM;
