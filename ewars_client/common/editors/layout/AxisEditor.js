const STYLES = {
    HEADER: {
        display: "block",
        position: "relative",
        padding: "8px",
        textTransform: "uppercase",
        height: "30px"
    },
    AXIS: {
        display: "block",
        margin: "8px"
    },
    HEADER_TEXT: {
        color: "#FFFFFF",
        display: "block",
        float: "left",
        padding: "3px",
        fontWeight: "bold",
        background: "rgb(51,51,51)"
    },
    HEADER_LINE: {
        position: "absolute",
        height: "1px",
        left: 0,
        right: 0,
        top: "50%",
        borderBottom: "1px solid #F2F2F2",
        zIndex: "-1"
    },
    SETTINGS: {
        display: "block"
    }
};

const AXIS_ACTIONS = [
    ["fa-plus", "ADD"]
];

const FORM = [
    {
        _o: 0,
        t: "SELECT",
        l: "Dimension",
        n: "dimension",
        o: [
            ["DEFAULT", "Date"],
            ["LOCATION", "Location"],
            ["VALUE", "Value"]
        ]
    },
    {
        _o: 1,
        t: "SELECT",
        l: "Position",
        n: "pos",
        o: [
            ["LEFT", "Left"],
            ["RIGHT", "Right"],
            ["BOTTOM", "Bottom"],
            ["TOP", "Top"]
        ]
    },
    {
        _o: 2,
        t: "SELECT",
        l: "Value type",
        n: "vt",
        o: [
            ["NUMERIC", "Numeric"],
            ["PERC", "Percentage"]
        ]
    }
];

class Axis extends React.Component {
    static defaultProps = {
        axisName: "x"
    };

    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        this.props.onChange(this.props.index, prop, value);
    };

    render() {
        let label = "Default";
        if (this.props.index > 0) {
            label = "Custom " + this.props.index;
        }
        return (
            <div style={STYLES.AXIS}>
                <div style={STYLES.HEADER}>
                    <div style={STYLES.HEADER_TEXT}>{label}</div>
                    <div style={STYLES.HEADER_LINE}></div>
                </div>

                <div style={STYLES.SETTINGS}>
                    <ewars.d.SystemForm
                        vertical={true}
                        definition={FORM}
                        onChange={this._onChange}
                        data={this.props.data}/>

                </div>
            </div>
        )
    }
}

let DEFAULT = {
    _: 0,
    dimension: "DEFAULT"
}

class AxisEditor extends React.Component {
    static defaultProps = {
        ext: null,
        data: {}
    };

    constructor(props) {
        super(props);
    }

    _action = (action) => {
        if (action == "ADD") {
            let data = this.props.data[this.props.ext.sp] || [DEFAULT];
            data.push({
                _o: 1,
                dimension: "DEFAULT"
            })
            ewars.z.dispatch("E", "WIDGET_PROP_CHANGE", [this.props.id, this.props.ext.sp, data])
        }
    };

    _onChange = (index, prop, value) => {
        let data = this.props.data[this.props.ext.sp] || [DEFAULT];
        data[index][prop] = value;

        ewars.z.dispatch("E", "WIDGET_PROP_CHANGE", [this.props.id, this.props.ext.sp, data]);
    };

    render() {
        let data = this.props.data[this.props.ext.sp] || [DEFAULT];
        data = data.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        });

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <ewars.d.ActionGroup
                        right={true}
                        actions={AXIS_ACTIONS}
                        onAction={this._action}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            {data.map((item, index) => {
                                return <Axis
                                    onChange={this._onChange}
                                    data={item}
                                    index={index}/>
                            })}

                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default AxisEditor;