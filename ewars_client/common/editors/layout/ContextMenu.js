import STYLES from "./styles";

class ContextMenu extends React.Component {
    static defaultProps = {
        type: "W",
        root: false
    };

    constructor(props) {
        super(props)
    }


    render() {
        return (
            <div
                ref={el => this._el = el}
                style={STYLES.CONTEXT_MENU}
                className="context-menu">
                {this.props.children}
            </div>
        )
    }
}

export default ContextMenu;