import * as analysis from '../../visualisation';
import * as WIDGETS from '../../wdgts/';

import * as viz from '../../visualisation';

class HtmlNode extends React.Component {
    render() {
        return (
            <div dangerouslySetInnerHTML={{__html: this.props.data.html}}></div>
        )
    }
}

const WIDGET_TYPES = {
    ALERTS: WIDGETS.WidgetAlerts,
    ACTIVITY: WIDGETS.WidgetActivity
}

const CHARTS = ["SERIES", "CATEGORY", "CALENDAR", "RAW"];

class NullWidget extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {
            width: "100%",
            height: "100%",
            background: "#000000"
        }

        return (
            <div style={style}>

            </div>
        )
    }
}


const _f = (val, fb) => {
    if (val == undefined) return fb;
    if (val == null) return fb;
    if (val == '') return fb;
    return val;
}

class HBox extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD"
    };

    render() {
        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        children = children.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        })
        return (
            <div className="ide-row"
                 style={{
                     height: this.props.data.h + "%",
                     maxHeight: this.props.data.h + "%"
                 }}>
                {children.map((item) => {
                    return <Node
                        data={item}
                        definition={this.props.definition}
                        parentType={this.props.data.t}
                        key={"PREV" + item._}/>
                })}

            </div>
        )
    }
}

class VBox extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD"
    };

    render() {
        let style = {height: "100%", maxHeight: "100%"};

        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        children = children.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        })

        return (
            <div className="ide-col"
                 style={{
                     height: this.props.data.h + "%",
                     maxHeight: this.props.data.h + "%",
                     width: (this.props.data.w || 100) + "%",
                     maxWidth: (this.props.data.w || 100) + "%"
                 }}>
                {children.map((item) => {
                    return <Node
                        data={item}
                        definition={this.props.definition}
                        parentType={this.props.data.t}
                        key={"PREV" + item._}/>
                })}

            </div>
        )
    }
}

const VIZ_TYPES = ['DEFAULT', 'PIE', 'COLUMN', 'FUNNEL', 'PYRAMID', 'HEATMAP', 'DATAPOINT', 'MAP', 'TABLE', 'HTML'];
class Widget extends React.Component {
    static defaultProps = {
        data: {},
        parentType: null,
        mode: "DASHBOARD"
    };

    componentDidMount() {
        let renderClass;
        switch (this.props.data.c.type) {
            case 'DEFAULT':
                renderClass = viz.DefaultChart;
                break;
            case 'PIE':
                renderClass = viz.PieChart;
                break;
            case 'COLUMN':
                renderClass = viz.ColumnChart;
                break;
            case 'FUNNEL':
                renderClass = viz.FunnelChart;
                break;
            case 'PYRAMID':
                renderClass = viz.PyramidChart;
                break;
            case 'HEATMAP':
                renderClass = viz.HeatMap;
                break;
            case 'DATAPOINT':
                renderClass = viz.Default;
                break;
            case 'MAP':
                renderClass = viz.DefaultMap;
                break;
            case 'TABLE':
                renderClass = viz.TableDefault;
                break;
            case 'HTML':
                renderClass = viz.OtherHTML;
                break;
            default:
                break;
        }


        if (renderClass) {
            this._viz = new renderClass(
                this.props.data.c,
                null,
                this._el
            )
        }
    }

    render() {
        let className = "ide-widget";
        if (this.props.parentType == "V") className += " ide-row";
        if (this.props.parentType == "H") className += " ide-col";

        let cmp;
        if (VIZ_TYPES.indexOf(this.props.data.c.type) < 0) {
            let Cmp;
            if (WIDGET_TYPES[this.props.data.c.type]) {
                Cmp = WIDGET_TYPES[this.props.data.c.type];
                cmp = <Cmp data={this.props.data.c}/>
            }
        }

        let outerStyle = {
            height: _f(this.props.data.h, 100) + '%',
            maxHeight: _f(this.props.data.h, 100) + '%',
            width: _f(this.props.data.w, 100) + '%',
            maxWidth: _f(this.props.data.w, 100) + '%',
            position: "relative",
            padding: _f(this.props.data.c.margin, undefined),
            paddingTop: _f(this.props.data.c.marginTop, undefined),
            paddingRight: _f(this.props.data.c.marginRight, undefined),
            paddingBottom: _f(this.props.data.c.marginBottom, undefined),
            paddingLeft: _f(this.props.data.c.marginLeft, undefined),
            display: 'block'
        };

        return (
            <div
                className={className}
                style={outerStyle}>
                <div
                    style={{display: 'block', width: '100%', height: '100%'}}
                    ref={(el) => {
                        this._el = el;
                    }}>
                    {cmp}
                </div>
            </div>
        )
    }
}

const TYPES = {
    V: VBox,
    H: HBox,
    W: Widget
}

class Node extends React.Component {
    static defaultProps = {
        data: {},
        parentType: null,
        mode: "DASHBOARD"
    };

    constructor(props) {
        super(props)
    }

    render() {
        let Cmp = TYPES[this.props.data.t];

        return (
            <Cmp
                definition={this.props.definition}
                parentType={this.props.parentType}
                data={this.props.data}/>
        )
    }
}

class Preview extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD",
        definition: [],
        settings: {}
    };

    constructor(props) {
        super(props)
    }

    render() {
        console.log('HUD', this.props);
        let style = {...this.props.settings.settings.style || {}};
        if (this.props.mode == "HUD") style.height = "100%";

        let roots = this.props.data.filter((item) => {
            return !item.p;
        })

        return (
            <ewars.d.Layout style={style}>
                {roots.map((item) => {
                    return <Node
                        definition={this.props.data}
                        mode={this.props.mode} data={item}
                        key={"PREV" + item._}/>
                })}

            </ewars.d.Layout>
        )
    }
}

export default Preview;