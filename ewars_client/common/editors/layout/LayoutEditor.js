import Droppables from "./Droppables";
import Tree from "./Tree";
import TYPE_DEFINITIONS from '../../constants/type_definitions';
import {PLOT_TYPES} from '../../constants/type_definitions';

import {Responsive, WidthProvider} from 'react-grid-layout';

const ResponsiveGridLayout = WidthProvider(Responsive);


import Page from "./Page";
import WidgetSettings from "./WidgetSettings";
import Preview from "./Preview";
import HTMLEditor from "./HTMLEditor";
import StyleEditor from "./StyleEditor";
import CenterDrop from "./CenterDrop";
import AxisEditor from "./AxisEditor";
import DataSources from "./DataSources";
import SingleSourceSelector from "./SingleSourceSelector";
import Widget from './Widget';

import PlotEditor from '../plot/PlotEditor';
import WidgetEditor from '../widget/WidgetEditor';

import reducer from "./reducer";


// Forms
import FORM_LAYOUT_TEMPLATE_THEME from '../../forms/form.layout.template.theme';
import FORM_LAYOUT_DASHBOARD_THEME from '../../forms/form.layout.dashboard.theme';
import FORM_LAYOUT_HUD_THEME from '../../forms/form.layout.hud.theme';

const THEME_OPTIONS = {
    TEMPLATE: FORM_LAYOUT_TEMPLATE_THEME,
    DASHBOARD: FORM_LAYOUT_DASHBOARD_THEME,
    HUD: FORM_LAYOUT_HUD_THEME
}

const CONTAINER = {
    height: "200px",
    background: "#333"
}


const data = [
    {
        t: "H",
        _: "0",
        h: 200,
        i: []
    }

];

class SettingsPage extends React.Component {
    _onChange = (prop, value) => {
        ewars.z.dispatch("E", "SET_LAYOUT_PROP", [prop, value]);
    };

    render() {
        return (
            <ewars.d.Panel style={{overflow: "visible"}}>
                <div style={{padding: "8px"}}>
                    <ewars.d.SystemForm
                        vertical={true}
                        definition={this.props.settings}
                        data={this.props.data}
                        onChange={this._onChange}
                        enabled={true}/>
                </div>
            </ewars.d.Panel>
        )
    }
}

const DEFAULTS = {
    name: "New HUD",
    status: "DRAFT",
    widgets: {},
    definition: [],
    settings: {},
    created_by: window.user.id,
    permissions: {}
};

class LayoutEditor extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD",
        aspect: null,
        onSave: null,
        onClose: null,
        permissions: false,
        settings: [],
        data: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            data: ewars.copy(this.props.data || DEFAULTS),
            view: "SETTINGS",
            widget: null,
            mainView: "EDITOR"
        }
    }

    _save = () => {
        if (this.props.onSave) {
            this.props.onSave(this.state.data)
                .then(res => {
                    console.log(res);
                    this.setState({
                        ...this.state,
                        data: res
                    });
                })
                .catch(err => {
                    ewars.error(err);
                });
        }
    };

    _closeEdit = () => {
        this.setState({
            widget: null
        })
    };

    _onRootDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("e", '{}'));
        let layout = this.state.data.definition || [];
        let uuid = ewars.utils.uuid();
        layout.push({
            i: uuid,
            x: 0,
            y: Infinity,
            h: 1,
            w: 1,
            minW: 2,
            minH: 2
        })

        let widgets = this.state.data.widgets || {};
        widgets[uuid] = {
            _: uuid,
            type: data.t
        };
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout,
                widgets: widgets
            }
        })
    };

    _addPage = () => {
    };

    _onChangeSetting = (prop, value) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                settings: {
                    ...this.state.settings,
                    [prop]: value
                }
            }
        });
    };

    _onStyleChange = (prop, value) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                style: {
                    ...this.state.style,
                    [prop]: value
                }
            }
        })
    };

    _onClosePlot = () => {
        this.setState({
            widget: null
        })
    };

    _onPlotSave = (data) => {
        console.log(data);
        let widgets = this.state.data.widgets;
        widgets[this.state.widget._] = data;
        this.setState({
            ...this.state,
            widget: null,
            data: {
                ...this.state.data,
                widgets: widgets
            }
        })
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onLayoutChange = (layout) => {
        // this.props.onLayoutChange(layout);
        console.log(layout);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout
            }
        })
    };

    onBreakpointChange = (breakpoint, cols) => {
    };

    _onEdit = (data) => {
        console.log(data);
        this.setState({
            widget: ewars.copy(data)
        })
    };

    _onRemoveItem = (data) => {
        let rIndex;
        let layout = this.state.data.definition;
        let widgets = this.state.data.widgets;
        delete widgets[data._];
        layout.forEach((item, index) => {
            if (item.i == data._) {
                rIndex = index;
            }
        })
        layout.splice(rIndex, 1);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout,
                widgets: widgets
            }
        })
    };

    _changeView = (data) => {
        console.log(data);
        this.setState({
            view: data
        })
    };

    render() {

        let view;


        if (this.state.view == "DEFAULT") view = <Droppables/>;
        if (this.state.view == "TREE") view = <Tree definition={this.state.definition}/>;
        if (this.state.view == "THEME") {
            view = <StyleEditor
                mode="main"
                data={this.state.data.settings || {style: {}}}
                onChange={this._onStyleChange}/>;
        }
        if (this.state.view == "SETTINGS") view = <SettingsPage data={this.state.data} settings={this.props.settings}/>;
        if (this.state.view == "DATA") view = <DataSources data={[]}/>;

        let curView;
        if (this.state.widget) {
            if (PLOT_TYPES.indexOf(this.state.widget.type) >= 0) {
                view = <PlotEditor
                    onSave={this._onPlotSave}
                    onClose={this._onClosePlot}
                    data={this.state.widget}/>

            } else {
                view = (
                    <WidgetEditor
                        onSave={this._onPlotSave}
                        onClose={this._onClosePlot}
                        data={this.state.widget}/>
                )
            }
        }

        let mainView,
            wrapperStyle = {};

        if (this.state.mainView == "PREVIEW") {
            mainView =
                <Preview
                    mode={this.props.mode}
                    data={this.state.definition}
                    settings={this.state.data}/>;
        } else {


            if (this.props.mode === "TEMPLATE") {
                // change to paging mode
                wrapperStyle.background = "#CCCCCC";
                mainView = (
                    <ewars.d.Panel>
                        {this.state.definition.map((item) => {
                            return (
                                <Page data={item}/>
                            )
                        })}
                    </ewars.d.Panel>
                )
            } else if (this.props.mode === "HUD") {
                wrapperStyle.background = "transparent";
                mainView = (
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        onDrop={this._onRootDrop}
                        onDragOver={this._onDragOver}
                        style={{
                            height: "100%",
                            overflow: "hidden",
                            border: "1px solid rgb(242, 242, 242)"
                        }}>
                        <ResponsiveGridLayout
                            className="layout"
                            layouts={{lg: this.state.data.definition || []}}
                            rowHeight={30}
                            autoSize={false}
                            verticalCompact={true}
                            onBreakPointChange={this.onBreakpointChange}
                            onLayoutChange={this._onLayoutChange}
                            breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                            cols={{lg: 36, md: 36, sm: 36, xs: 4, xxs: 2}}>
                            {(this.state.data.definition || []).map(item => {
                                return (
                                    <div key={item.i}>
                                        <Widget
                                            key={"WIDG_" + item.i}
                                            onEdit={this._onEdit}
                                            onDelete={this._onRemoveItem}
                                            data={this.state.data.widgets[item.i]}/>
                                    </div>
                                )
                            })}
                        </ResponsiveGridLayout>
                    </div>
                )
            } else {
                mainView = (
                    <ewars.d.Panel>
                        <div className="ide-layout" style={{position: "relative", padding: "16px"}}
                             ref={(el) => {
                                 this._el = el;
                             }}>

                        </div>
                    </ewars.d.Panel>
                )
            }
        }

        wrapperStyle.background = "#F2F2F2";

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell
                        width="250px"
                        style={{display: "flex", flexDirection: "column"}}
                        borderRight={true}
                        borderTop={true}>
                        <ewars.d.Toolbar>

                            {!this.state.widget ?
                                <div className="btn-group btn-tabs">
                                    <ewars.d.Button
                                        onClick={() => {
                                            this._changeView("SETTINGS");
                                        }}
                                        highlight={this.state.view == "SETTINGS"}
                                        icon="fa-cog"/>
                                    <ewars.d.Button
                                        onClick={() => {
                                            this._changeView("DEFAULT")
                                        }}
                                        highlight={this.state.view == "DEFAULT"}
                                        icon="fa-cube"/>
                                    {this.props.mode == "TEMPLATE" ?
                                        <ewars.d.Button
                                            onClick={() => {
                                                this._changeView("PAGE_TEMPLATES");
                                            }}
                                            highlight={this.state.view == "PAGE_TEMPLATES"}
                                            icon="fa-file"/>
                                        : null}
                                    <ewars.d.Button
                                        onClick={() => {
                                            this._changeView("THEME");
                                        }}
                                        highlight={this.state.view == "THEME"}
                                        icon="fa-paint-brush"/>
                                </div>
                                : null}
                            {this.state.widget ?
                                <div className="btn-group btn-tabs">

                                    <ewars.d.Button
                                        icon="fa-close"
                                        onClick={this._closeEdit}/>
                                </div>
                                : null}
                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "block", position: "relative"}}>
                            {view}
                        </ewars.d.Row>

                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true}>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar>
                                <div className="btn-group btn-tabs">
                                    <ewars.d.Button
                                        highlight={!this.state.mainView == "EDITOR"}
                                        onClick={() => {
                                            this.setState({
                                                mainView: "EDITOR"
                                            })
                                        }}
                                        icon="fa-cogs"
                                        label="Editor"/>
                                    <ewars.d.Button
                                        highlight={this.state.mainView == "PREVIEW"}
                                        icon="fa-eye"
                                        onClick={() => {
                                            this.setState({
                                                mainView: "PREVIEW"
                                            })
                                        }}
                                        label="Preview"/>
                                    <ewars.d.Button
                                        highlight={this.state.mainView == "VARS"}
                                        icon="fa-cog"
                                        onClick={() => {
                                            this.setState({mainView: "VARS"})
                                        }}
                                        label="Variables"/>
                                    <ewars.d.Button
                                        highlight={this.state.mainView == "PERMISSIONS"}
                                        icon="fa-lock"
                                        onClick={() => {
                                            this.setState({mainView: "PERMISSIONS"})
                                        }}
                                        label="Permissions"/>

                                </div>
                                <div className="btn-group pull-right">
                                    {this.props.mode == "TEMPLATE" ?
                                        <ewars.d.Button
                                            icon="fa-plus"
                                            label="Add Page"
                                            onClick={this._addPage}/>
                                        : null}
                                    {this.props.onSave ?
                                        <ewars.d.Button
                                            onClick={this._save}
                                            icon="fa-save"/>
                                        : null}
                                    {this.props.onClose ?
                                        <ewars.d.Button
                                            onClick={this.props.onClose}
                                            icon="fa-times"/>
                                        : null}
                                </div>
                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell style={wrapperStyle}>
                                    {mainView}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default LayoutEditor;