var USER_TYPES = require("../../../../constants/user_types");

var COUNTRY_ALLOWED = ["ACCOUNT_ADMIN", "USER"];


var UserType = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.role);
    },

    render: function () {
        var userTypeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);
        var description = ewars.formatters.I18N_FORMATTER(this.props.data.description);

        var className = "user-type";
        if (this.props.selected) className += " selected";

        return (
            <div className={className} onClick={this._onClick}>
                <div className="name">{userTypeName}</div>
                <div className="description">{description}</div>
            </div>
        )

    }

});

var Type = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _onClick: function (key) {
        this.state.errors = [];
        this.props.onChange("role", key);
    },

    canAdvance: function () {
        var isComplete = true;

        if (!this.props.data.role || this.props.data.role == ""){
            isComplete = false;
            this.state.errors.push("Please select a valid user type");
        }

        return isComplete;
    },

    render: function () {

        var user_types = _.map(USER_TYPES, function (userType, KEY) {
            if (window.user.group_id == 5) {
                if (COUNTRY_ALLOWED.indexOf(KEY) >= 0) {
                    return <UserType
                        data={userType}
                        user_type={KEY}
                        onClick={this._onClick}
                        selected={this.props.data.role == KEY}/>;
                }
            } else {
                return <UserType
                    data={userType}
                    user_type={KEY}
                    onClick={this._onClick}
                    selected={this.props.data.role == KEY}/>;
            }
        }, this);


        var errors = _.map(this.state.errors, function (err) {
            return <li>{err}</li>
        });

        return (
            <div className="wizard-page">
                <h3>User Type</h3>

                <p>Select the user type that is appropriate for the new user</p>

                {this.state.errors.length > 0 ?
                    <ul className="errors">
                        {errors}
                    </ul>
                    : null}

                <div className="padded">
                    <div className="user-type-list">
                        {user_types}
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Type;