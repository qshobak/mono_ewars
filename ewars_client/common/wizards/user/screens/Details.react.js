var TextInput = require("../../../fields/TextInputField.react");
var SelectField = require("../../../fields/SelectField.react");


var Details = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _onChange: function (prop, value) {
        this.state.errors = [];
        this.props.onChange(prop, value);
    },

    canAdvance: function () {
        var isComplete = true;

        this.state.errors = [];

        if (!this.props.data.name || this.props.data.name == "") {
            isComplete = false;
            this.state.errors.push("Please provide a valid name");
        }
        if (!this.props.data.email || this.props.data.email == "") {
            isComplete = false;
            this.state.errors.push("Please provide a valid email address");
        }
        if (!this.props.data.status || this.props.data.status == "") {
            isComplete = false;
            this.state.errors.push("Please provide a valid status");
        }
        if (!this.props.data.lang || this.props.data.lang == "") {
            isComplete = false;
            this.state.errors.push("Please provide a valid default language");
        }
        this.forceUpdate();

        return isComplete;
    },

    render: function () {

        var errors = _.map(this.state.errors, function (err) {
            return <li>{err}</li>;
        }, this);

        return (
            <div className="wizard-page">
                <h3>User Details</h3>

                <p>Enter the general details for the user</p>

                {this.state.errors.length > 0 ?
                    <ul className="errors">
                        {errors}
                    </ul>
                    : null}

                <div className="padded">
                    <label htmlFor="">Name*</label>
                    <TextInput
                        name="name"
                        onUpdate={this._onChange}
                        value={this.props.data.name}/>
                    <br />
                    <label htmlFor="">Email*</label>
                    <TextInput
                        name="email"
                        onUpdate={this._onChange}
                        value={this.props.data.email}/>
                    <br />
                    <label htmlFor="">Status*</label>
                    <SelectField
                        name="status"
                        onUpdate={this._onChange}
                        value={this.props.data.status}
                        config={{options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"]]}}/>
                    <br />
                    <label htmlFor="">Default Language*</label>
                    <SelectField
                        name="lang"
                        onUpdate={this._onChange}
                        value={this.props.data.lang}
                        config={{options: [["en", "en - English"], ["fr", "fr - French"]]}}/>
                    <br />
                    <label htmlFor="">Mobile #</label>
                    <TextInput
                        name="phone"
                        onUpdate={this._onChange}
                        value={this.props.data.phone}/>
                </div>
            </div>
        )

    }
});

module.exports = Details;