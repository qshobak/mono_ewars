var SelectField = require("../../../fields/SelectField.react");
var TextInput = require("../../../fields/TextInputField.react");

var Password = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _onChange: function (prop, value) {
        this.state.errors = [];
        this.props.onChange(prop, value);
    },

    canAdvance: function () {
        var isComplete = true;

        if (!this.props.data.password_set || this.props.data.password_set == "") {
            isComplete = false;
            this.state.errors.push("Please select a valid password source");
        }

        if (this.props.data.password_set == "SET") {

            if (!this.props.data.password) {
                isComplete = false;
                this.state.errors.push("Please provide a valid password");
            }

            if (!this.props.data.password_confirm || this.props.data.password_confirm == "") {
                isComplete = false;
                this.state.errors.push("Please confirm the password");
            }
        }

        return isComplete;
    },

    render: function () {
        return (
            <div className="wizard-page">
                <h3>User Password</h3>

                <p>Tell EWARS what to do for the users password</p>

                <div className="padded">
                    <label htmlFor="">How would you like to set the users password?</label>
                    <SelectField
                        name="password_set"
                        onUpdate={this._onChange}
                        value={this.props.data.password_set}
                        config={{options: [["SET", "Set Myself"], ["GENERATE", "Auto-Generate"]]}}/>
                    <br />
                    {this.props.data.password_set == "SET" ?
                        <div>
                            <label htmlFor="">Password</label>
                            <TextInput
                                name="password"
                                onUpdate={this._onChange}
                                value={this.props.data.password}/>
                            <br/>
                            <label>Confirm Password</label>
                            <TextInput
                                name="password_confirm"
                                onUpdate={this._onChange}
                                value={this.props.data.password_confirm}/>
                        </div>
                        : null}

                    {this.props.data.password_set == "GENERATE" ?
                        <p>A random password will be generated and provided to the user in their registration email.</p>
                    : null}

                </div>
            </div>
        )
    }
});

module.exports = Password;