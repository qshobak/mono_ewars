var USER_TYPES = require("../../../../constants/USER_TYPES");

var Review = React.createClass({
    render: function () {
        var typeName = ewars.I18N(USER_TYPES[this.props.data.role].name);

        var locationName;
        if (this.props.data.location) locationName = ewars.I18N(this.props.data.location.name);

        var orgName;
        if (this.props.data.organization) orgName = ewars.I18N(this.props.data.organization.name);

        return (
            <div className="wizard-page">
                <h3>Review</h3>

                <p>Review the users details below and click "Submit" to create the user.</p>

                <p>

                </p>

                <div className="padded">
                    <table className="details">
                        <tr>
                            <th>Name</th>
                            <td>{this.props.data.name}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{this.props.data.email}</td>
                        </tr>
                        <tr>
                            <th>User Type</th>
                            <td>{typeName}</td>
                        </tr>
                        <tr>
                            <th>Mobile #</th>
                            <td>{this.props.data.phone}</td>
                        </tr>

                        {this.props.data.account.id ?
                            <tr>
                                <th>Account</th>
                                <td>{this.props.data.account.name}</td>
                            </tr>
                            : null}

                        {this.props.data.laboratory.uuid ?
                            <tr>
                                <th>Laboratory</th>
                                <td>{this.props.data.laboratory.name}</td>
                            </tr>
                            : null}

                        {this.props.data.organization.uuid ?
                            <tr>
                                <th>Organization</th>
                                <td>{orgName}</td>
                            </tr>
                            : null}

                        {this.props.data.location.uuid ?
                            <tr>
                                <th>Location</th>
                                <td>{locationName}</td>
                            </tr>
                        : null}
                    </table>
                </div>


            </div>
        )
    }
});

module.exports = Review;