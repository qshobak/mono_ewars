var KeyMirror = require("keymirror");
var Moment = require("moment");

var Button = require("../../ButtonComponent.react");
var Modal = require("../../ModalComponent.react");

var Details = require("./screens/Details.react");
var Type = require("./screens/Type.react");
var Password = require("./screens/Password.react");
var Review = require("./screens/Review.react");
var Account = require("./screens/Account.react");
var Location = require("./screens/Location.react");
var Organization = require("./screens/Organization.react");
var Laboratory = require('./screens/Laboratory.react');

var CONSTANTS = KeyMirror({
    DETAILS: null,
    TYPE: null,
    PASSWORD: null,
    REVIEW: null,
    ACCOUNT: null,
    LOCATION: null,
    ORGANIZATION: null,
    LAB: null
});

var PROGRESS = {
    "SUPER_ADMIN": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "GLOBAL_ADMIN": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "ACCOUNT_ADMIN": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    REGIONAL_ADMIN: [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.LOCATION, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "USER": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.ORGANIZATION, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "ORG_ADMIN": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ORGANIZATION, CONSTANTS.LOCATION, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "LAB_USER": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.LAB, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "COUNTRY_SUPERVISOR": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.LOCATION, CONSTANTS.PASSWORD, CONSTANTS.REVIEW],
    "BOT": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.REVIEW],
    "API_USER": [CONSTANTS.DETAILS, CONSTANTS.TYPE, CONSTANTS.ACCOUNT, CONSTANTS.PASSWORD, CONSTANTS.REVIEW]
};


var getNextItem = function (items, curItem) {
    var curIndex = items.indexOf(curItem);
    return items[curIndex + 1];
};

var getPrevItem = function (items, curItem) {
    var curIndex = items.indexOf(curItem);
    return items[curIndex - 1];
};

var UserWidget = React.createClass({
    getInitialState: function () {
        return {
            view: CONSTANTS.DETAILS,
            data: {
                account: {id: null},
                laboratory: {uuid: null},
                organization: {uuid: null},
                location: {uuid: null}
            },
            location: null,
            organization: null
        }
    },

    _onChange: function (prop, value) {
        this.state.data[prop] = value;
        this.forceUpdate();
    },

    _cancel: function () {
        this.state.data = {
            account: {id: null},
            laboratory: {uuid: null},
            organization: {uuid: null},
            location: {uuid: null}
        };
        this.state.view = CONSTANTS.DETAILS;
        this.props.onCancel();
    },

    _progress: function () {

        if (!this.refs.view.canAdvance()) {
            return;
        }

        if (this.state.view == CONSTANTS.DETAILS) {
            // Need to check if the user email exists already
            var blocker = new ewars.Blocker(null, "Checking details...");
            var isTaken = false;
            ewars.tx("com.ewars.query", ["user", ["id"], {email: {eq: this.state.data.email}}, null, null, null, null])
                .then(function (resp) {
                    if (resp.length > 0) isTaken = true;
                    blocker.destroy();

                    if (isTaken) {
                        ewars.growl("The email specified is already in use, please use another email");

                    } else {
                        this.state.view = CONSTANTS.TYPE;
                        this.forceUpdate();

                    }
                }.bind(this))

        } else {
            if (this.state.view == CONSTANTS.REVIEW) return;

            this.state.view = getNextItem(PROGRESS[this.state.data.role], this.state.view);
            this.forceUpdate();
        }
    },

    _back: function () {
        if (this.state.view == CONSTANTS.TYPE) {
            this.state.view = CONSTANTS.DETAILS;
            this.forceUpdate();
            return;
        }

        this.state.view = getPrevItem(PROGRESS[this.state.data.role], this.state.view);
        this.forceUpdate();
    },

    _onClose: function () {
        this.state.view = CONSTANTS.DETAILS;
        this.state.data = {
            account: {id: null},
            laboratory: {uuid: null},
            organization: {uuid: null},
            location: {uuid: null}
        };

        this.props.onCancel();
    },

    _create: function () {
        var data = {
            role: this.state.data.role,
            name: this.state.data.name,
            email: this.state.data.email,
            password_set: this.state.data.password_set,
            password: this.state.data.password,
            status: this.state.data.status,
            phone: this.state.data.phone,
            org_id: (this.state.data.organization.uuid || null),
            lab_id: (this.state.data.laboratory.uuid || null),
            acc_id: (this.state.data.account.id || null),
            location_id: (this.state.data.location.uuid || null)
        };

        ewars.tx("com.ewars.user.create", [data])
            .then(function (resp) {
                if (resp.result == true) {
                    this._onClose();
                    ewars.growl("The user was created successfully");
                } else {
                    this.state.error = resp.msg;
                    if (this.isMounted()) this.forceUpdate();
                }
            }.bind(this))
    },

    render: function () {
        var buttons = [];

        buttons = [
            {label: "Previous", onClick: this._back},
            {label: "Next", onClick: this._progress, disabled: this.state.data.location_uuid == null},
            {label: "Cancel", onClick: this._cancel, colour: "red"}
        ];

        if (this.state.view == CONSTANTS.DETAILS) {
            buttons = [
                {label: "Next", onClick: this._progress, disabled: this.state.data.form_id == null},
                {label: "Cancel", onClick: this._cancel, colour: "red"}
            ];
        }

        if (this.state.view == CONSTANTS.REVIEW) {
            buttons = [
                {label: "Previous", onClick: this._back},
                {label: "Create User", onClick: this._create, colour: "green"},
                {label: "Cancel", onClick: this._cancel, colour: "red"}
            ]
        }

        var error;

        if (this.state.error) {
            error = <p className="error">{this.state.error}</p>;
        }

        if (this.state.view == CONSTANTS.DETAILS) this._view =
            <Details ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.TYPE) this._view =
            <Type ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.PASSWORD) this._view =
            <Password ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.ACCOUNT) this._view =
            <Account ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.LOCATION) this._view =
            <Location ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.ORGANIZATION) this._view =
            <Organization ref="view" data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.LAB) this._view =
            <Laboratory ref="view" data={this.state.data} onChange={this._onChange}/>;

        if (this.state.view == CONSTANTS.REVIEW) this._view =
            <Review ref="view" data={this.state.data} location={this.state.location}
                    organization={this.state.organization}/>;

        return (
            <Modal
                wizard={true}
                buttons={buttons}
                width={780}
                visible={this.props.visible}>
                {error}
                {this._view}
            </Modal>
        )
    }
});

module.exports = UserWidget;