import chroma from "chroma-js";

// need to extend leaflet with TopoJSON support
if (window.L) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function (jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            }
            else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });

    var _redIcon = L.icon({
        iconUrl: 'static/icons/map-markers/marker-red.png',

        iconSize: [21, 34], // size of the icon
        shadowSize: [26, 26], // size of the shadow
        iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
        shadowAnchor: [10.5, 24],  // the same for the shadow
        popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
    });
}

var _colorScale = chroma.scale(['#D5E3FF', '#003171']).domain([0, 1]);

var MapComponent = React.createClass({
    _map: null,
    _markers: [],
    _marker: null,

    propTypes: {
        layer: React.PropTypes.object,
        showTiles: React.PropTypes.bool,
        height: React.PropTypes.number,
        lat: React.PropTypes.number,
        lng: React.PropTypes.number,
        pointEditMode: React.PropTypes.bool,
        defaultLat: React.PropTypes.number,
        defaultLng: React.PropTypes.number
    },

    getDefaultProps: function () {
        return {
            showTiles: true,
            height: 300,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        }
    },

    getInitialState: function () {
        return {};
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.height != nextProps.height) {
            return true;
        }

        if (nextProps.data) {
            if (nextProps.data.length > 0) {
                this._renderMarkers(nextProps.data);
            }
        }

        if (nextProps.lat != this.props.lat || nextProps.lng != this.props.lng) {
            if (this._marker) {
                if (nextProps.lat != "-" && nextProps.lat != "" && nextProps.lng != "-" && nextProps.lng != "") {
                    this._map.setView([nextProps.lat, nextProps.lng]);
                    this._marker.setLatLng([nextProps.lat, nextProps.lng]);
                }
            }
        }

        if (nextProps.defaultLat && nextProps.defaultLng) {
            this._map.setView([nextProps.defaultLat, nextProps.defaultLng]);
        }
        return false;
    },

    componentDidMount: function () {
        if (!this._map) this.renderEditor();
    },

    componentDidUpdate: function () {
        if (!this._map) this.renderEditor();
        if (this._map) this._map.invalidateSize();
        this._map.fitBounds(this._topoLayer.getBounds());
    },

    componentWillUnmount: function () {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    onChange: function (name, value) {
        this.props.onChange(name, value);
    },

    render: function () {
        if (this.props.height) {
            var style = {
                height: this.props.height + "px"
            }
        }

        return (
            <div className="map" style={style}></div>
        )
    },

    _renderMarkers: function (data) {
        for (var i in this._markers) {
            var item = this._markers[i];

            this._map.removeLayer(item);
        }

        this._markers = [];

        var redIcon = L.icon({
            iconUrl: 'static/icons/map-markers/marker-red.png',

            iconSize: [21, 34], // size of the icon
            shadowSize: [26, 26], // size of the shadow
            iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
            shadowAnchor: [10.5, 24],  // the same for the shadow
            popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
        });

        for (i in data) {
            var item = data[i];

            var lat = ewars.getKeyPath(this.props.latProp, item);
            var lng = ewars.getKeyPath(this.props.lngProp, item);

            if ((lat && lng) && (lat != "NULL")) {
                var marker = L.marker([lat, lng], {
                    icon: redIcon,
                    zIndexOffset: 10000
                });

                this._markers.push(marker);
                this._map.addLayer(marker);
            }

        }

    },

    _onPointChange: function (lat, lng) {
        this.props.onPointChange(lat, lng);
    },

    renderEditor: function () {
        this._map = L.map(this.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            attributionControl: false,
            scrollWheelZoom: (this.props.pointEditMode ? false : true)
        });

        if (this.props.lat && this.props.lng) {
            this._map.setView([this.props.lat, this.props.lng], 1);
        } else {
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 1);
        }

        var tiles = L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'});
        tiles.addTo(this._map);

        if (this.props.pointEditMode && this.props.defaultLat) {
            this._marker = L.marker([this.props.defaultLat, this.props.defaultLng], {
                icon: _redIcon,
                draggable: true,
                riseOnHover: true
            });

            this._map.addLayer(this._marker);
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 10)
        }

        if (this.props.pointEditMode && this.props.lat) {
            this._marker = L.marker([this.props.lat, this.props.lng], {
                icon: _redIcon,
                draggable: true,
                riseOnHover: true
            });

            this._map.addLayer(this._marker);
            this._map.setView([this.props.lat, this.props.lng], 10)
        }

        if (this.props.data) {

            for (i in this.state.data) {
                var item = this.state.data[i];

                var lat = ewars.getKeyPath(this.props.latProp, item);
                var lng = ewars.getKeyPath(this.props.lngProp, item);

                if (lat && lng) {
                    var marker = L.marker([lat, lng], {
                        icon: _redIcon,
                        zIndexOffset: 10000
                    });

                    this._markers.push(marker);
                    this._map.addLayer(marker);
                }

            }

        }

        // Point editing features
        if (this.props.pointEditMode) {
            var self = this;
            this._map.on("click", function (e) {
                if (self._marker) self._marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                self._onPointChange(e.latlng.lat, e.latlng.lng);
            });

            if (this._marker) {
                this._marker.on("dragend", function (e) {
                    self._onPointChange(e.target.latlng.lat, e.target.latlng.lng);
                })
            }
        }

        if (this.props.layer) {
            this._topoLayer = new L.geoJson();

            this._topoLayer.addData(this.props.layer);
            this._topoLayer.setStyle({
                fillColor: _colorScale(Math.random()).hex(),
                fillOpacity: 0.1,
                color: "#555",
                weight: 1,
                opacity: 0.1
            });
            this._topoLayer.addTo(this._map);
            this._map.fitBounds(this._topoLayer.getBounds());
        }

    }

});

export default MapComponent;
