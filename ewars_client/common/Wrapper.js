import Prompt from "./cmp/Prompt";

class Wrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            notifications: [],
            prompt: null
        }
    }

    componentWillMount() {
        ewars.subscribe("ADD_NOTIFICATION", this._addNotification);
        ewars.subscribe("PROMPT", this._prompt);
        ewars.subscribe("GROWL", this._growl);
    }

    _prompt = (data) => {
        this.setState({
            prompt: data
        })
    };

    _addNotification = () => {

    };

    _onPromptAction = (action) => {

    };

    render() {

        let prompt;
        if (this.state.prompt) {
            prompt = <Prompt data={this.state.prompt}/>
        }

        return (
            <div className="ide-wrapper">
                {this.props.children}
                {prompt}
            </div>
        )
    }
}

export default Wrapper;