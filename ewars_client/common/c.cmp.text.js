import LanguageEditor from "./c.editor.language";
import { Layout, Row, Cell } from "./layout";
import Shade from "./c.shade";
import { ActionGroup } from "./c.action_button";

const LANG_OPTIONS = [
    ["fa-language", "EDIT"]
];

const MODAL_ACTIONS = [
    {label: "CLOSE", icon: "fa-times", action: "CLOSE"}
];

class TextField extends React.Component {
    static defaultProps = {
        data: {
            n: null, // name
            ml: false, // multi-line
            v: [], // Validations
            r: false, // required
            l: null, // Label,
            mkdwn: false,
            i18: false
        },
        value: "",
        ro: false

    };

    constructor(props) {
        super(props);

        this.state = {
            showLang: false
        }
    }

    componentDidMount() {
        window.addEventListener("close-shade", this._closeShade);
    }

    componentWillUnmount() {
        window.removeEventListener("close-shade", this._closeShade);
    }

    _closeShade = () => {
        this.setState({
            showLang: false
        });
    };

    _onChange = (e) => {
        this.props.onChange(this.props.data.n, e.target.value);
    };

    _action = (action) => {
        if (action == "EDIT") {
            this.setState({
                showLang: true
            })
        }

        if (action == "CLOSE") {
            this.setState({
                showLang: false
            })
        }

    };

    _onLangChange = (prop, value) => {
        this.props.onChange(this.props.data.n, value);
    };

    render() {
        console.log('TF', this.props.ro);
        let value = this.props.value;

        if (value) {
            if (JSON.stringify(value).indexOf("{") >= 0) {
                value = value.en;
            }
        }

        if (this.props.data.ml) {
            return (
                <div style={{display: "block"}}>
                    <Row>
                        <Cell>
                <textarea
                    disabled={this.props.ro ? 'DISABLED' : ''}
                    onChange={this._onChange}
                    value={value}/>
                        </Cell>
                        {this.props.data.i18 ?
                            <Cell style={{display: "block", flexGrow: 0}}>
                                <ActionGroup
                                    right={true}
                                    height="31px"
                                    onAction={this._action}
                                    actions={LANG_OPTIONS}/>
                            </Cell>
                            : null}
                    </Row>
                    <Shade
                        title="Translations"
                        actions={MODAL_ACTIONS}
                        onAction={this._action}
                        toolbar={true}
                        shown={this.state.showLang}>
                        {this.state.showLang ?
                            <LanguageEditor
                                data={{ml: true, n: "lang"}}
                                value={this.props.value}
                                onChange={this._onLangChange}/>
                            : null}
                    </Shade>
                </div>
            )
        }


        if (this.props.data.mkdwn) {
            return (
                <p>{value}</p>
            )
        }

        return (
            <div style={{display: "block"}}>
                <Row>
                    <Cell>
                        <input
                            disabled={this.props.ro ? 'DISABLED' : ""}
                            value={value}
                            onChange={this._onChange}
                            type="text"/>
                    </Cell>
                    {this.props.data.i18 ?
                        <Cell style={{display: "block", flexGrow: 0}}>
                            <ActionGroup
                                right={true}
                                height="31px"
                                onAction={this._action}
                                actions={LANG_OPTIONS}/>
                        </Cell>
                        : null}
                </Row>
                <Shade
                    toolbar={false}
                    onAction={this._action}
                    shown={this.state.showLang}>
                    {this.state.showLang ?
                        <LanguageEditor
                            value={this.props.value}
                            onChange={this._onLangChange}/>
                        : null}
                </Shade>
            </div>
        )

    }
}

export default TextField;
