import TextField from "./f.text";
import SelectField from "./f.select";

const ACTIONS = [
    ['fa-plus', 'ADD_ITEM', 'ADD_ITEM']
];

const ACTIONS_OPTION = [
    ['fa-trash', 'DELETE', 'DELETE']
];

class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        this.props.onRemove(this.props.index);
    };

    _value = (prop, value) => {
        this.props.onChange(this.props.index, value);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{flex: 2}}>
                            <SelectField
                                data={{n: "VALUE", o: this.props.fields || []}}
                                onChange={this._value}
                                value={this.props.data}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: "block"}} width="30px">
                            <ewars.d.ActionGroup
                                height="31px"
                                actions={ACTIONS_OPTION}
                                onAction={this._action}
                                right={true}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

class CalculatedFieldDefinition extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case "ADD_ITEM":
                let items = this.props.value || [];
                items.push("");
                this.props.onChange(this.props.data.n, items);
                break;
            default:
                break;
        }

    };

    _onChange = (data) => {
        this.props.onChange(this.props.data.n, data);
    };

    _remove = (index) => {
        let items = this.props.value || [];
        items.splice(index, 1);
        this.props.onChange(this.props.data.n, items);
    };

    _change = (index, value) => {
        let items = this.props.value || [];
        items[index] = value;
        console.log(this.props.data.n);
        this.props.onChange(this.props.data.n, items);
    };

    render() {

        let fields = (this.props.data.fo || []).filter(item => {
            return item[2].type == "number";
        });
        let fieldOptions = fields.map(item => {
            return [item[2].path, item[1]];
        })
        let options = (this.props.value || []).map((item, index) => {
            return <Item data={item}
                         index={index}
                         onChange={this._change}
                         onRemove={this._remove}
                         fields={fieldOptions}/>
        });

        return (
            <ewars.d.Layout style={{border: "1px solid #262626"}}>
                <ewars.d.Toolbar label="Source Fields">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{display: "block"}}>
                            <ewars.d.ActionGroup
                                right={true}
                                onAction={this._action}
                                actions={ACTIONS}/>

                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="block-tree"
                             style={{position: "relative", height: "auto", overflowY: "inherit"}}>
                            {options}
                        </div>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default CalculatedFieldDefinition;
