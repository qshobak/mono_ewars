import CodeMirror from "codemirror";

const ACTIONS = [
    {icon: "fa-save", action: "SAVE", label: "SAVE_CHANGES"},
    {icon: "fa-times", action: "CLOSE", label: "CANCEL"}
];

class Editor extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let height = this._el.getBoundingClientRect().height;
        this._editor = CodeMirror(this._el, {
            value: this.props.value,
            lineNumbers: true,
            mode: "html"
        })
        this._editor.setSize("100%", height);
    }

    getValue = () => {
        return this._editor.getValue();
    };

    render() {
        return (
            <div
                ref={(el) => {
                    this._el = el
                }}
                style={{height: "100%", width: "100%"}}>


            </div>
        )
    }
}


class HTMLField extends React.Component {
    static defaultProps = {
        value: ""
    };

    constructor(props) {
        super(props);

        this.state = {
            edit: false,
            html: ""
        }
    }


    componentDidMount() {
        console.log(this._el);
        this._editor = CodeMirror(this._el, {
            lineNumbers: true,
            mode: "html"
        })
    };

    _edit = () => {
        this.setState({
            edit: true,
            html: this.props.value || ""
        })
    };

    _action = (action) => {
        if (action == "CLOSE") {
            this.setState({
                edit: false,
                html: ""
            })
        }

        if (action == "SAVE") {
            let value = this._editor.getValue();
            this.props.onChange(this.props.name, value);
            this.setState({
                edit: false,
                html: ""
            })
        }
    };

    _onChange = (value) => {
        this.setState({html: value});
    };

    render() {
        return (
            <Editor
                ref={(el) => {
                    this._editor = el
                }}
                value={this.state.html}
                onChange={this._onChange}/>
        )
    }
}

export default HTMLField;
