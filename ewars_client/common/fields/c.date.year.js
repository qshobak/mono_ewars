import Moment from "moment";

class YearPicker extends React.Component {
    constructor(props) {
        super(props);
    }

    _movePrevious = () => {
        let newDate;
        if (Moment.isMoment(this.props.value)) {
            newDate = this.props.value.clone().subtract(1, "y").endOf("Y");
        } else {
            newDate = Moment.utc(this.props.value || new Date()).subtract(1, 'y').endOf("Y");
        }
        this.props.onChange(newDate);
    };

    _moveNext = () => {
        let newDate;
        if (Moment.isMoment(this.props.value)) {
            newDate = this.props.value.clone().add(1, "y").endOf("Y");
        } else {
            newDate = Moment.utc(this.props.value || new Date()).add(1, 'y').endOf("Y");
        }
        this.props.onChange(newDate);
    };

    render() {
        let value = Moment.utc(this.props.value || new Date());
        let year = value.year();

        return (
            <div className="date-picker">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col cal-left" style={{maxWidth: 25}} onClick={this._movePrevious}>
                            <i className="fal fa-caret-left"></i>
                        </div>
                        <div className="ide-col cal-header">{year}</div>
                        <div className="ide-col cal-right" style={{maxWidth: 25}} onClick={this._moveNext}>
                            <i className="fal fa-caret-right"></i>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default YearPicker;
