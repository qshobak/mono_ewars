
// need to extend leaflet with TopoJSON support
if (window.L != undefined && window.L != null) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function (jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            }
            else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });
}

var TopoJSONField = React.createClass({
    getInitialState: function () {
        return {};
    },

    _render: function () {
    },

    componentWillReceiveProps: function () {

    },

    shouldComponentUpdate: function (nextProps, nextState) {
        return false;
    },

    componentDidMount: function () {
        this.renderEditor();
    },

    componentDidUpdate: function () {
        this.renderEditor();
    },

    componentWillUnmount: function () {
        this.map.off("click", this.onMapClick);
        this.map = null;
    },

    onChange: function (name, value) {
        this.props.onChange(name, value);
    },

    render: function () {
        return (
            <div className="map"></div>
        )
    },

    renderEditor: function () {

        var map = this.map = L.map(this.getDOMNode(), {
            minZoom: 2,
            maxZoom: 20,
            attributionControl: false
        });

        var topoLayer = new L.TopoJSON();

        topoLayer.addData(this.props.value);
        topoLayer.addTo(map);

        map.fitWorld();
    }

});

export default TopoJSONField;
