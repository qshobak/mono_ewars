import Button from "../c.button";

var GeometryField = React.createClass({
    getInitialState: function () {
       return {}
    },

    _editMode: function () {
        ewars.prompt("fa-cogs", "Feature Unavailable", "This feature is under active development and will be available soon", function () {});
    },

    render: function () {
        return (
            <div className="geometry-editor">
                <ewars.d.Button
                    icon="fa-pencil"
                    label="Edit Geometry"
                    onClick={this._editMode}/>
            </div>
        )
    }
});

export default GeometryField;
