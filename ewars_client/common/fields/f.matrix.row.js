var MatrixRow = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {

        var label;

        if (this.props.data.show_row_label) {
            var labelContent = ewars.I18N(this.props.data.label);
            label = (
                <th>{labelContent}</th>
            )
        }

        var children = this.props.children;
        if (!Array.isArray(children)) {
            children = [children];
        }

        return (
            <tr>
                {label}
                {children}
            </tr>
        )
    }
});

export default MatrixRow;
