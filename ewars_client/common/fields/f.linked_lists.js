const STYLE = {
    item: {
        padding: 8,
        border: "1px solid #CCCCCC",
        marginBottom: 5,
        cursor: "pointer"
    },
    active: {
        background: "#CCCCCC",
        color: "#FFFFFF"
    },
    cell: {
        padding: 8
    },
    itemRaw: {
        border: "1px solid #CCCCCC",
        marginBottom: 5
    }

};

class ListItem extends React.Component {
    static defaultProps = {
        active: false,
        index: null,
        name: null
    };

    _click = () => {
        this.props.click(this.props.index);
    };

    render() {
        let style = STYLE.item;
        if (this.props.active) style = {...style, ...STYLE.active};
        return (
            <div style={style} onClick={this._click}>{ewars.I18N(this.props.name)}</div>
        )
    }

}

class OrderableListItem extends React.Component {
    static defaultProps = {
        active: false,
        index: null,
        name: null
    };

    _remove = () => {
        console.log("HERE");
        this.props.remove(this.props.index);
    };

    _moveUp = () => {
        this.props.moveUp(this.props.index);
    };

    _moveDown = () => {
        this.props.moveDown(this.props.index);
    };

    render() {
        return (
            <div style={STYLE.itemRaw}>
                <ewars.d.Row>
                    <ewars.d.Cell style={STYLE.cell}>
                        {ewars.I18N(this.props.name)}
                    </ewars.d.Cell>
                    {this.props.index > 0 ?
                        <ewars.d.Cell onClick={this._moveUp} width={25} style={STYLE.cell}>
                            <i className="fal fa-caret-up"></i>
                        </ewars.d.Cell>
                        : null}
                    <ewars.d.Cell onClick={this._moveDown} width={25} style={STYLE.cell}>
                        <i className="fal fa-caret-down"></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell onClick={this._remove} width={25} style={STYLE.cell}>
                        <i className="fal fa-trash"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }

}

class Source extends React.Component {
    static defaultProps = {
        correlary: "id",
        target: [],
        data: [],
        selected: null
    };

    constructor(props) {
        super(props);

        this.state = {
            selected: null
        }
    }

    _select = (index) => {
        this.props.select(index);
    };

    render() {

        let items = [];

        this.props.data.forEach((item, index) => {

            let hasItem = false;
            this.props.target.forEach((tgt) => {
                if (tgt[0] == item[this.props.correlary]) {
                    hasItem = true;
                }
            });

            if (!hasItem) {
                items.push(
                    <ListItem
                        key={item[this.props.correlary]}
                        name={item.name}
                        index={index}
                        active={this.props.selected == index}
                        click={this._select}/>
                )
            }
        });

        return (
            <ewars.d.Cell>
                <div className="iw-edit-list">
                    <div className="iw-edit-list-items">
                        {items}
                    </div>
                </div>
            </ewars.d.Cell>
        )
    }
}

class Target extends React.Component {
    static defaultProps = {
        correlary: "id",
        source: [],
        target: []
    };

    constructor(props) {
        super(props);

        this.state = {
            selected: null,
            sourceSelected: null,
            targetSelected: null
        }
    }


    render() {

        let items = [];

        let data = [];

        this.props.target.map((item, index) => {
            let sourceI;
            this.props.source.forEach((src) => {
                if (src[this.props.correlary] == item[0]) {
                    sourceI = src;
                }
            });

            if (sourceI) {
                items.push(
                    <OrderableListItem
                        key={item[0]}
                        name={sourceI.name}
                        index={index}
                        moveUp={this.props.moveUp}
                        moveDown={this.props.moveDown}
                        remove={this.props.remove}
                        active={this.props.selected}
                        order={item[1]}
                        click={this.props.select}/>
                )
            }
        });

        return (
            <ewars.d.Cell>
                <div className="iw-edit-list">
                    <div className="iw-edit-list-items">
                        {items}
                    </div>
                </div>
            </ewars.d.Cell>
        )
    }
}


class LinkedListsField extends React.Component {
    static defaultProps = {
        source: [],
        target: [],
        correlary: "id"
    };

    constructor(props) {
        super(props);

        this.state = {
            sourceSelected: null,
            targetSelected: null
        }
    }

    _sourceSelect = (index) => {
        this.setState({
            sourceSelected: index,
            targetSelected: null
        })
    };

    _targetSelect = (index) => {
        this.setState({
            sourceSelected: null,
            targetSelected: index
        })
    };

    _moveRight = () => {
        let items = ewars.copy(this.props.target || []);
        items.push([this.props.source[this.state.sourceSelected][this.props.correlary], 999]);
        items.sort((a, b) => {
            if (a[1] > b[1]) return 1;
            if (a[1] < b[1]) return -1;
            return 0;
        });
        let cur = 0;
        items.forEach((item) => {
            item[1] = cur;
            cur++;
        });

        this.props.onUpdate(this.props.name, items)
    };

    _remove = (index) => {
        let items = ewars.copy(this.props.target || []);
        items.splice(index, 1);

        let cur = 0;
        items.forEach((item) => {
            item[1] = cur;
            cur++;
        });

        this.props.onUpdate(this.props.name, items);
    };

    _moveUp = (index) => {
        if (index <= 0) return;
        if (!this.props.target[index - 1]) return;

        let items = ewars.copy(this.props.target || []);
        let tmp = items[index - 1];
        items[index - 1] = items[index];
        items[index] = tmp;

        let cur = 0;
        items.forEach((item) => {
            item[1] = cur;
            cur++;
        });

        this.props.onUpdate(this.props.name, items);
    };

    _moveDown = (index) => {
        if (!this.props.target[index + 1]) return;

        let items = ewars.copy(this.props.target || []);
        let tmp = this.props.target[index + 1];
        items[index + 1] = items[index];
        items[index] = tmp;

        let cur = 0;
        items.forEach((item) => {
            item[1] = cur;
            cur++;
        });

        this.props.onUpdate(this.props.name, items);
    };

    render() {
        return (
            <div style={{height: 200}}>
                <ewars.d.Layout>
                    <ewars.d.Row>
                        <Source
                            selected={this.state.sourceSelected}
                            correlary={this.props.correlary}
                            target={this.props.target}
                            select={this._sourceSelect}
                            data={this.props.source}/>
                        <ewars.d.Cell width={60} style={{padding: 16}}>
                            <ewars.d.Button
                                onClick={this._moveRight}
                                icon="fa-caret-right"/>
                        </ewars.d.Cell>
                        <Target
                            moveUp={this._moveUp}
                            moveDown={this._moveDown}
                            remove={this._remove}
                            correlary={this.props.correlary}
                            target={this.props.target}
                            source={this.props.source}/>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

export default LinkedListsField;
