class DisplayCalculatedField extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        console.log(this.props);
        let value = 0;
        this.props.config.calc_def.forEach(item => {
            let fieldVal = this.props.data[item] || '0';
            value += parseFloat(fieldVal);
        });

        return (
            <input
                ref="inputField"
                disabled={true}
                className="form-control input"
                type="text"
                value={value}
                placeholder={this.props.placeholder}/>
        )
    }
}

export default DisplayCalculatedField;