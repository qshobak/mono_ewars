class Field extends React.Component {
    render() {
        return (
            <div className="block">
                <div className="block-content"></div>
            </div>
        )
    }
}

class FormField extends React.Component {
    static defaultProps = {
        config: {
            form_id: null
        }
    };

    constructor(prop) {
        super(props);

        this.state = {
            fields: null
        }
    }

    componentDidMount() {
        if (this.props.config.form_id) {
            ewars.tx("com.ewars.fields", ["QUERY", this.props.config.form_id, null])
                .then((resp) => {
                    this.setState({
                        fields: resp
                    })
                })
        }
    }

    render() {
        return (
            <div className="iw-select">
                <div className="handle"></div>
            </div>
        )
    }
}

export default FormField;