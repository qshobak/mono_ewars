import MapComponent from "../c.map";
import Button from "../c.button";

var LocationInputField = React.createClass({
    propTypes: {
        value: React.PropTypes.object,
        name: React.PropTypes.string
    },

    getInitialState: function () {
        return {
            lat: 51.505,
            lng: -0.09
        }
    },

    componentDidMount: function () {
        this._getUsersPosition();
    },

    getDefaultProps: function () {
        return {
            value: {
                lat: 12.0000,
                lng: 8.33333
            }
        }
    },

    _getUsersPosition: function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this._handleGeoLocate);
        }
    },

    _handleGeoLocate: function (data) {
        //this._handleChange(data.coords.latitude, data.coords.longitude);
    },

    _handleChange: function (lat, lng) {
        if (this.props.readOnly) return;
        this.props.onUpdate(this.props.name, {lat: lat, lng: lng}, this.props.config.path)
    },

    _handleSingleChange: function (e) {
        var copy = JSON.parse(JSON.stringify({lat: this.props.value.lat, lng: this.props.value.lng}));
        copy[e.target.name] = e.target.value;
        this.props.onUpdate(this.props.name, copy, this.props.config.path);
    },

    render: function () {

        var pointEditMode = true;
        if (this.props.readOnly) pointEditMode = false;

        return (
            <div className="location-selector-field">
                <div className="map-wrapper">
                    <MapComponent
                        lat={this.props.value.lat}
                        lng={this.props.value.lng}
                        defaultZoom={10}
                        pointEditMode={pointEditMode}
                        onPointChange={this._handleChange}
                        showTiles={true}/>
                </div>
            {!this.props.readOnly ?
                <div className="controls-wrapper">
                    <div className="grid">
                        <div className="col-6">
                            <span>Latitude</span>
                            <input type="text" name="lat" onChange={this._handleSingleChange} value={this.props.value.lat}/>
                        </div>
                        <div className="col-6">
                            <span>Longitude</span>
                            <input type="text" name="lng" onChange={this._handleSingleChange} value={this.props.value.lng}/>
                        </div>
                    </div>
                </div>
                : null}
            </div>
        )
    }
});

export default LocationInputField;
