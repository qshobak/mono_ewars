import TreeNode from "./LocationTreeNode.react";

var LocationSelectorComponent = React.createClass({
    _locations: [],
    _hasLoaded: false,

    getInitialProps: function () {
        return {
            allowCheck: false,
            checked: [],
            fillHeight: false,
            locationTypeLimit: null,
            location_type: null,
            hideInactive: false
        }
    },

    getInitialState: function () {
        return {
            showTree: false,
            locations: [],
            search: "",
            nodeName: "Select Location",
            nodeUUID: null,
            steps: [],
            rootElements: [],
            checked: [],
            checkedNames: []
        };
    },

    componentWillMount: function () {
        this._init(this.props);
    },

    componentWillReceiveProps: function (nextProps) {
        this._init(nextProps);
    },

    _handleResults: function (locations) {
        this.state.rootElements = [];
        this.state.locations = locations;
        if (this.isMounted()) this.forceUpdate();
    },

    _init: function (props) {
        if (!props.value || props.value == "None") {
            this.state.nodeName = "Select Location";
            this.state.nodeUUID = null;
            if (this.isMounted()) this.forceUpdate();
        }

        if (props.value && !props.allowCheck && props.value != "None") {
            if (props.value != this.state.nodeUUID) {
                ewars.tx("com.ewars.resource", ["location", props.value, ["uuid", "name"], null])
                    .then(function (resp) {
                        this.state.nodeName = ewars.formatters.I18N_FORMATTER(resp.name);
                        this.state.nodeUUID = resp.uuid;
                        if (this.isMounted()) this.forceUpdate();
                    }.bind(this))
            }
        } else if (props.value && _.isArray(props.value) && props.allowCheck && !this.props.disabled) {
            if (props.value.length > 0) {

                var filter = {
                    uuid: {in: props.value}
                };

                if (this.props.hideInactive) filter.status = {eq: "ACTIVE"};

                ewars.tx("com.ewars.query", ["location", ["uuid", "name", "parent_id", 'location_type'], filter, {"name.en": "ASC"}, null, null, null])
                    .then(function (resp) {
                        this.state.checkedNames = _.map(resp, function (item) {
                            return ewars.formatters.I18N_FORMATTER(item.name);
                        }, this);
                        if (this.isMounted()) this.forceUpdate();
                    }.bind(this))
            }
        }

        this._resetView(props);
    },

    _resetView: function (props) {
        var query = {};
        if (props.parent_id) query.uuid = {eq: props.parent_id};
        if (this.props.config.parentId) query.parent_id = {eq: this.props.config.parentId};
        if (!props.parent_id) query.parent_id = {eq: "NULL"};
        if (props.hideInactive) query.status = {eq: 'ACTIVE'};
        console.log(query);
        // ewars.tx("com.ewars.query", ["location", ['uuid', 'name', 'location_type'], query, {"name.en": "ASC"}, null, null, null])
        //     .then(function (resp) {
        //             this._handleResults(resp);
        //         }.bind(self)
        //     )
    },

    _onCheck: function (node) {
        var checked = this.props.value || [];

        if (checked.indexOf(node.uuid) < 0) {
            checked.push(node.uuid)
        } else {
            checked = _.without(checked, node.uuid);
        }

        this.setState({
            checked: checked
        });

        this.props.onUpdate(this.props.name, checked, node.name);
    },

    _processLocationTree: function () {
        var nodes = [];

        for (var i in this.state.locations) {
            var location = this.state.locations[i];
            var key = _.uniqueId("LOCATION_");

            nodes.push(
                <TreeNode
                    hideInactive={this.props.hideInactive}
                    checked={this.props.value}
                    location_type={this.props.location_type}
                    allowCheck={this.props.allowCheck}
                    onCheck={this._onCheck}
                    taxonomy_id={1}
                    index={i}
                    key={key}
                    onNodeSelect={this._onNodeSelection}
                    data={location}/>
            )
        }

        return nodes;
    },

    _handleSearchChange: function (e) {
        this.setState({
            search: e.target.value
        });

        if (e.target.value.length >= 3) {
            this._searchLocations(e.target.value);
        }

        if (e.target.value.length < 3) {
            this._resetView(this.props);
        }
    },

    _searchLocations: function (searchTerm) {
        var query = {
            "name.en": {like: searchTerm}
        };
        if (this.props.hideInactive) query.status = {eq: "ACTIVE"};

        if (window.user.role != "SUPER_ADMIN") {
            if (window.user.role == "ACCOUNT_ADMIN") {
                query.lineage = {under: window.user.clid};
            } else if (window.user.role == "REGIONAL_ADMIN") {
                query.lineage = {under: window.user.location_id};
            } else {
                query.lineage = {under: window.user.clid};
            }
        }

        ewars.tx("com.ewars.query", ["location", ["uuid", "name"], query, {"name.en": "DESC"}, null, null, null])
            .then(function (resp) {
                this._handleResults(resp);
            }.bind(this))
    },

    _hideTree: function (e) {
        this.setState({
            showTree: false
        })
    },

    _toggleTree: function () {
        this.setState({
            showTree: this.state.showTree ? false : true
        })
    },

    _onNodeSelection: function (node) {
        this.setState({
            nodeName: ewars.formatters.I18N_FORMATTER(node.name),
            nodeUUID: node.uuid,
            showTree: false,
            showLocationCreator: false
        });

        this.props.onUpdate(this.props.name, node.uuid, node.name);
    },

    _onLocationUpdate: function (data, prop, value, path) {
        var copy = this.state.newLocationData;
        copy[prop] = value;
        this.setState({
            newLocationData: copy
        })
    },

    _saveLocation: function () {
        ewars._connect().then(function (session) {
            session.call("com.ewars.location.create", [this.state.newLocationData])
                .then(function (resp) {
                    if (resp.d.active) {
                        ewars.notifications.notification("fa-bell", "Location Created", "The requested location has been created, you can now select this location and submit your report.");
                    } else {
                        ewars.notifications.notification("fa-bell", "Location Creation Requested", "A request for approval has been submitted for the specified location, you will be able to submit your report upon administrator approval of the location.");
                    }

                    this._onNodeSelection(resp.d.location);
                }.bind(this))
        }.bind(this))
    },

    render: function () {
        var nodes = this._processLocationTree();

        var selectedName = "Select Location";
        if (this.props.allowCheck) {
            if (_.isArray(this.props.value)) {
                selectedName = this.state.checkedNames.join(", ") || selectedName;
            }
        } else {
            if (this.state.nodeName) selectedName = this.state.nodeName;
        }

        var handleIcon = "fal fa-globe";
        if (this.state.showTree) handleIcon = "fal fa-times";

        return (
            <div className="location-selector">
                <div className="handle">
                    <div className="current">
                        <div className="current-location">
                            {selectedName}
                        </div>
                    </div>
                    {!this.props.disabled ?
                        <div className="grip" rel="tooltip" title="Select Location" onClick={this._toggleTree}><i
                            className={handleIcon}></i>
                        </div>
                        : null}
                </div>
                {this.state.showTree ?
                    <div className="location-tree-wrapper">
                        <div className="search">
                            <input placeholder="search" className="form-field" type="text" value={this.state.search}
                                   onChange={this._handleSearchChange}/>
                        </div>

                        <div className="location-tree">

                            <ul>
                                {nodes}
                            </ul>

                        </div>
                    </div>
                    : null}

            </div>
        )
    }
});

export default LocationSelectorComponent;
