const TextField = React.createClass({
    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool
    },

    _options: {

    },

    componentDidMount: function () {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getDefaultProps: function () {
        return {
            path: null,
            name: null,
            config: {}
        }
    },

    getInitialState: function () {
        return {};
    },

    _handleKeyDown: function (e) {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    },

    _onChange: function (event) {
        if (!this.props.config.useBlur) {
            this.props.onUpdate(this.props.name, event.target.value, this.props.path);
        }
    },

    _onBlur: function (e) {
        // Perform validation here
        if (this.props.config.useBlur) {
            this.props.onUpdate(this.props.name, e.target.value, this.props.path);
        }
    },

    render: function() {

        let value = this.props.value;
        if (value) {
            let strValue = String(value);
            if (strValue.indexOf("DUPE_") >= 0) {
                value = strValue.replace("DUPE_", "");
            }
        }

        return (
            <input
                ref="inputField"
                disabled={this.props.readOnly}
                className="form-control input"
                onBlur={this._onBlur}
                onChange={this._onChange}
                type="text"
                value={value}
                placeholder={this.props.placeholder}
                name={this.props.name}
                onKeyDown={this._handleKeyDown}/>
        )
    }

});

export default TextField;
