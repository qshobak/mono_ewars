import DayPicker from "./c.date.day";
import WeekPicker from "./c.date.week";
import MonthPicker from "./c.date.month";
import YearPicker from "./c.date.year";
import DisplayPicker from "./c.date.display";
import Moment from "moment";

const PICKERS = {
    NONE: DayPicker,
    DAY: DayPicker,
    WEEK: WeekPicker,
    MONTH: MonthPicker,
    YEAR: YearPicker,
    DISPLAY: DisplayPicker
};

class Handle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let name = "No date selected",
            showClear = false;

        if (this.props.value) {
            name = ewars.DATE(this.props.value, this.props.format);
            showClear = true;
        }

        return (
            <div className="handle" onClick={this.props.onClick}>
                <div className="ide-row">
                    <div className="ide-col">{name}</div>
                    {showClear ?
                        <div className="ide-col icon" style={{maxWidth: "31px"}} onClick={this.props.onClear}>
                            <i className="fal fa-ban"></i>
                        </div>
                    : null}
                    <div className="ide-col icon" style={{maxWidth: 31}}>
                        <i className="fal fa-calendar"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class DateField extends React.Component {
    constructor(props) {
        super(props);

        let val = this.props.value;
        if (!val) val = null;

        this.state = {
            showOptions: false,
            value: val || Moment()
        }
    }

    static defaultProps = {
        value: null,
        dispValue: null,
        name: null,
        config: {
            block_future: false,
            date_type: "DAY"
        }
    };

    componentWillMount() {
        window.__hack__.addEventListener("click", this._handleBodyClick);
        if (!this.props.value) {
            this.state.value = Date.now()
        } else {
            let val = this.props.value;
            if (!val) val = null;
            this.state.value = val;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.value) {
            this.state.value = Date.now()
        } else {
            let val = nextProps.value;
            if (!val) val = null;
            this.state.value = val;
        }
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._handleBodyClick);
    }

    _handleBodyClick = (evt) => {
        if (this.refs.selector) {
            const area = this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    };

    _onValueChange = (newValue) => {
        let name = this.props.name;
        if (this.props.config.nameOverride) name = this.props.config.nameOverride;
        this.state.showOptions = false;
        this.props.onUpdate(name, newValue.clone().format("YYYY-MM-DD"), this.props.config.path || null);
    };

    _toggle = () => {
        this.setState({
            showOptions: !this.state.showOptions
        })
    };

    _clear = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let name = this.props.name;
        if (this.props.config.nameOverride) name = this.props.config.nameOverride;
        this.props.onUpdate(name, null, this.props.config.path || null);
    };

    render() {
        let view;
        let date_type = this.props.config.date_type || this.props.date_type;
        let ViewCmp = PICKERS[date_type || "DAY"];

        if (this.props.readOnly) {
            return (
                <input type="text" value={ewars.DATE(this.state.value, date_type)}/>
            )
        }

        view = <ViewCmp
            onChange={this._onValueChange}
            format={date_type}
            offsetAvailability={this.props.config.offsetAvailability}
            block_future={this.props.config.block_future}
            value={this.state.value}/>;

        if (date_type == "YEAR") return view;


        return (
            <div ref="selector" className="ew-select" onClick={this._handleBodyClick}>
                <Handle
                    onClick={this._toggle}
                    value={this.props.value}
                    onClear={this._clear}
                    format={date_type}/>
                {this.state.showOptions ?
                    <div className="ew-select-data" style={{maxWidth: 300, right: 0, left: "initial"}}>
                        {view}
                    </div>
                    : null}
            </div>
        )
    }
}

export default DateField;
