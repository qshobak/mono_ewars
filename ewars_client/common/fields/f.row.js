import Button from "../c.button";

var RowField = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {
        var childs = [];
        if (this.props.children) {

            var childLength = this.props.children.length;
            var colWidth = 12 / childLength;
            var colString = "col-" + colWidth + " column";

            childs = [];
            for (var childIndex in this.props.children) {
                var child = this.props.children[childIndex];
                childs.push(
                    <div className={colString}>
                {child}
                    </div>
                )
            }
        }

        return (
            <div className="grid">
                <div className="row">
                    {childs}

                    <div className="clearer"></div>

                </div>
                <div className="clearer"></div>
            </div>
        )
    }
});

export default RowField;
