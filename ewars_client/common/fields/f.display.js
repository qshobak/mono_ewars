var DisplayField = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {
        return (
            <div className="display-field">
            {this.props.config.defaultValue || this.props.value}
            </div>
        )
    }
});

export default DisplayField;
