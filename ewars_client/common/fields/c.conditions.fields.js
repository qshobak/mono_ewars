import CheckboxField from "./f.checkbox";
import DateField from "./f.date";
import SelectField from "./f.select";
import TextAreaField from "./f.textarea";
import TextField from "./f.text";
import LocationField from "./f.location";
import NumericField from "./f.numeric";
import SwitchField from "./f.switch";
import IndicatorField from "./f.indicator";

export default = {
    select: SelectField,
    text: TextField,
    textarea: TextAreaField,
    checkbox: CheckboxField,
    date: DateField,
    number: NumberField,
    location: LocationField,
    switch: SwitchField,
    indicator: IndicatorField
};
