import Button from "../c.button";
import TextField from "./f.text";

var OptionRow = React.createClass({
    getInitialState: function () {
        return {};
    },

    _onUpdate: function () {

    },

    _addRow: function () {
        this.props.onAddRow();
    },

    _deleteRow: function () {
        this.props.onRemoveRow(this.props.index);
    },

    _updateLabel: function (prop, value) {
        this.props.onUpdate(this.props.index, "LABEL", value);
    },

    _updateValue: function (prop, value) {
        this.props.onUpdate(this.props.index, "VALUE", value);
    },

    render: function () {
        return (
            <tr>
                <td>
                    <TextField
                        name="value"
                        onUpdate={this._updateValue}
                        value={this.props.data[0]}
                        data={{index: this.props.index}}/>
                </td>
                <td>
                    <TextField
                        name="label"
                        onUpdate={this._updateLabel}
                        value={this.props.data[1]}
                        data={{index: this.props.index}}/>
                </td>
                <td width="70px">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            colour="green"
                            onClick={this._addRow}/>
                        <ewars.d.Button
                            icon="fa-trash"
                            colour="red"
                            data={{index: this.props.index}}
                            onClick={this._deleteRow}/>
                    </div>
                </td>
            </tr>
        )
    }
});

var SelectFieldOptionsField = React.createClass({
    getInitialState: function () {
        return {
            options: [
                ["EXAMPLE", "Example"]
            ]
        }
    },

    componentWillMount: function () {
        if (this.props.value) {
            var value = JSON.parse(JSON.stringify(this.props.value));
            this.state.options = value;
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.value) {
            var value = JSON.parse(JSON.stringify(nextProps.value));
            this.state.options = value;
        }
    },

    _addRow: function () {
        this.state.options.push(["", ""]);
        this.props.onUpdate(this.props.name, this.state.options, this.props.config.path || null);
    },

    _removeRow: function (index) {
        var value = JSON.parse(JSON.stringify(this.state.options));
        value.splice(index, 1);
        this.props.onUpdate(this.props.name, value, this.props.config.path || null);
    },

    _onUpdate: function (index, type, value) {
        if (type == "LABEL") this.state.options[index][1] = value;
        if (type == "VALUE") this.state.options[index][0] = value;
        this.props.onUpdate(this.props.name, this.state.options, this.props.config.path || null);
    },

    render: function () {

        var options = this.state.options.map(function (item, index) {
            return <OptionRow
                data={item}
                onAddRow={this._addRow}
                onRemoveRow={this._removeRow}
                onUpdate={this._onUpdate}
                index={index}/>
        }.bind(this));

        return (
            <div style={{padding: "5px"}}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <th style={{textAlign: "left", fontWeight: "bold"}}>Value</th>
                        <th style={{textAlign: "left", fontWeight: "bold"}}>Label</th>
                        <th></th>
                    </tr>
                    {options}
                    </tbody>
                </table>
                <div className="btn-group">

                </div>
            </div>
        )
    }
});

export default SelectFieldOptionsField;
