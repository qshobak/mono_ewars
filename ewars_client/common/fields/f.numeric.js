const reg = /^\d+$/;

var NumericField = React.createClass({
    propTypes: {
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        config: React.PropTypes.object
    },

    getDefaultProps: function () {
        return {
            placeholder: "Enter number",
            name: null,
            readOnly: false,
            path: null,
            config: {
                allow_negative: true,
                decimal_allowed: true,
                style: {}
            }
        }
    },

    _options: {},

    componentDidMount: function () {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getInitialState: function () {
        return {};
    },

    _onChange: function (e) {
        var value = e.target.value;

        if (!this.props.config.decimal_allowed) value = value.split(".")[0];
        if (!this.props.config.allow_negative) value = value.replace("-", "");

        value = value.replace(/[^\d.-]/g, '');

        let name = this.props.config.nameOverride || this.props.name;
        let path = this.props.config.path || this.props.name;

        //value = value.replace(/^0+/, '');
        this.props.onUpdate(name, value, path);
    },

    render: function () {
        var value = this.props.value || "";

        let style = {paddingRight: "5px"};
        if (this.props.style) Object.assign(style, this.props.style);

        return (
            <input ref="inputField"
                   disabled={this.props.readOnly}
                   className="form-control number-field"
                   style={style}
                   onChange={this._onChange}
                   type="text"
                   value={value}
                   placeholder={this.props.placeholder || 0}
                   name={this.props.name}/>
        )

    }

});

export default NumericField;
