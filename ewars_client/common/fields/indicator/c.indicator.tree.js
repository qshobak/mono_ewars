import ContextMenu from "../../c.context_menu";

class IndicatorNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showContext: false
        }
    }

    componentWillMount() {
        window.__hack__.addEventListener("click", this._onBodyClick);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._onBodyClick);
    }

    _onBodyClick = (evt) => {
        if (!this.refs.item.contains(evt.target)) {
            this.setState({
                showContext: false
            })
        }
    };

    _onContext = (e) => {
        e.preventDefault();
        this.setState({
            showContext: true
        })
    };

    _onContextAction = (action) => {
        this.setState({
            showContext: false
        });
        this.props.onAction(action, this.props.data);
    };

    _onClick = () => {
        this.props.onAction("CHECK", this.props.data);
    };

    _onCheck = () => {
        this.props.onAction("CHECK", this.props.data);
    };

    _onSelect = () => {
        this.props.onAction("INDICATOR_SELECT", this.props.data);
    };

    _dragStart = (e) => {
        e.dataTransfer.setData("item", JSON.stringify(this.props.data));
    };

    render() {
        let label = ewars.I18N(this.props.data.name);

        let icon;
        if (this.props.allowCheck) {
            icon = "fal fa-square";
            if (this.props.checked.indexOf(this.props.data.uuid) >= 0) icon = "fal fa-check-square";
        }

        let className = "block-content";
        className = className + (this.props.data.status == "ACTIVE" ? " status-green" : " status-red");

        return (
            <div className="block" onDragStart={this._dragStart} draggable={true} ref="item"
                 onContextMenu={this._onContext} onClick={this._onSelect}>
                <div className={className}>
                    <div className="ide-row">
                        {this.props.allowCheck ?
                            <div className="ide-col" style={{maxWidth: 25}}>
                                <i className={icon}></i>
                            </div>
                            : null}
                        <div className="ide-col">{label}</div>
                    </div>
                    {this.props.actions && this.state.showContext ?
                        <ContextMenu
                            absolute={true}
                            onClick={this._onContextAction}
                            actions={this.props.actions}/>
                        : null}
                </div>
            </div>
        )
    }
}

class IndicatorFolderNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = ewars.g["FOLDER_" + this.props.data.id] || {
            showChildren: false,
            data: null,
            showContext: false
        }
    }

    componentWillMount() {
        window.__hack__.addEventListener("click", this._onBodyClick);
        ewars.subscribe("RELOAD_INDICATORS", this._reload);
    }

    _onBodyClick = (evt) => {
        if (!this.refs.item.contains(evt.target)) {
            this.setState({
                showContext: false
            })
        }
    };

    _reload = () => {
        ewars.tx("com.ewars.indicators", [this.props.data.id])
            .then(function (resp) {
                this.setState({
                    data: resp
                })
            }.bind(this))
    };

    getChildren = () => {
        if (this.state.data) {
            this.setState({
                showChildren: !this.state.showChildren
            });
            return;
        }

        this.refs.iconHandle.setAttribute("class", "fa fa-spin fa-gear");

        // Get groups first
        ewars.tx("com.ewars.indicators", [this.props.data.id])
            .then(function (resp) {
                this.setState({
                    data: resp,
                    showChildren: true
                })
            }.bind(this))
    };

    _onCaretClick = () => {
        this.getChildren();
    };

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._onBodyClick);
        ewars.g["FOLDER_" + this.props.data.id] = ewars.copy(this.state);
    };

    _dragStart = (e) => {
        e.dataTransfer.setData("item", JSON.stringify(this.props.data));
    };

    _onContext = (e) => {
        e.preventDefault();
        this.setState({
            showContext: true
        })
    };

    _onContextAction = (action) => {
        this.setState({
            showContext: false
        });

        this.props.onAction(action, this.props.data);
    };

    render() {

        let childs;

        if (this.state.data) {
            childs = this.state.data.map(function (item) {
                if (item.context == "INDICATOR") {
                    return (
                        <IndicatorNode
                            {...this.props}
                            key={"IND_" + item.uuid}
                            data={item}/>
                    )
                }

                if (item.context == "FOLDER") {
                    return (
                        <IndicatorFolderNode
                            {...this.props}
                            key={"FOLDER_" + item.id}
                            data={item}/>
                    )
                }
            }.bind(this))
        }

        let icon = "fal ";
        if (this.state.showChildren) icon += " fa-folder-open";
        if (!this.state.showChildren) icon += " fa-folder";

        let label = ewars.I18N(this.props.data.name);

        return (
            <div className="block" ref="item">
                <div onClick={this._onCaretClick} className="block-content" onDragStart={this._dragStart}
                     draggable={true}
                     onContextMenu={this._onContext}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 20, textAlign: "center", marginRight: 8}}>
                            <i className={icon} ref="iconHandle"></i>
                        </div>
                        <div className="ide-col">{label}</div>
                    </div>
                    {this.props.actions && this.state.showContext ?
                        <ContextMenu
                            absolute={true}
                            onClick={this._onContextAction}
                            actions={this.props.actions}/>
                        : null}
                </div>
                {this.state.showChildren ?
                    <div className="block-children">
                        {childs}
                    </div>
                    : null}
            </div>
        )
    }
}

var TreeNodeComponent = React.createClass({
    _hasLoaded: false,

    getDefaultProps: function () {
        return {
            dark: false,
            hideInactive: true
        }
    },

    getInitialState: function () {
        return {
            children: [],
            showChildren: false,
            showContext: false
        };
    },

    componentWillMount: function () {
        window.__hack__.addEventListener("click", this._onBodyClick);
        ewars.subscribe("RELOAD_INDICATORS", this._reload);
    },

    componentWillUnmount: function () {
        window.__hack__.removeEventListener("click", this._onBodyClick);
    },

    _onBodyClick: function (evt) {
        if (!evt.contains(this.refs.item)) {
            this.setState({
                showContext: false
            })
        }
    },

    _onContext: function (e) {
        e.preventDefault();
        this.setState({
            showContext: true
        })
    },

    _onContextAction: function (action) {
        this.props.onAction(action, this.props.data);
    },

    _reload: function () {
        if (this._hasLoaded && this.props.type != "INDICATOR") {
            // Get groups first

            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then(function (resp) {
                    this._hasLoaded = true;
                    this.setState({
                        children: resp,
                        showChildren: true
                    })
                }.bind(this))
        }
    },

    _onClick: function () {
        if (this.state.showChildren) {
            this.setState({
                showChildren: false
            });
            return;
        }

        if (!this._hasLoaded) {
            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then(function (resp) {
                    this._hasLoaded = true;
                    this.setState({
                        children: resp,
                        showChildren: true
                    })
                }.bind(this))
        } else {

            this.setState({
                showChildren: true
            })
        }
    },

    _hasChildren: function () {
        return true;
    },

    _onEdit: function () {
        this.props.onEdit(null, this.props.data);
    },

    render: function () {
        let className = "block";

        let label = ewars.I18N(this.props.data.name);

        let showSelect = false;

        let caret;
        if (["SYSTEM", "FOLDER"].indexOf(this.props.data.context) >= 0) {
            if (this.state.showChildren) caret = "fal fa-folder-open";
            if (!this.state.showChildren) caret = "fal fa-folder";
        }

        let children;

        if (this.state.showChildren && this._hasLoaded) {
            children = [];
            this.state.children.forEach(function (item) {
                children.push(<TreeNodeComponent
                    key={item.uuid || `FOLDER-${item.id}`}
                    actions={this.props.actions}
                    onSelect={this.props.onSelect}
                    data={item}/>);
            }.bind(this))
        }

        if (this.state.showChildren && !this._hasLoaded) {
            children = <Spinner/>;
        }

        if (this.props.data.context == "INDICATOR") showSelect = true;

        let handleStyle = {flexGrow: 1, flexBasis: 0};

        return (
            <div className={className} ref="item">
                <div className="block-content" onClick={this._onClick} onContextMenu={this._onContext}>
                    <div className="ide-row">
                        {caret ?
                            <div className="ide-col" style={{maxWidth: 25}}>
                                <i className={caret}></i>
                            </div>
                            : null}
                        <div className="ide-col" style={handleStyle}>{label}</div>
                    </div>
                    {this.props.actions && this.state.showContext ?
                        <ContextMenu
                            absolute={true}
                            onClick={this._onContextAction}
                            actions={this.props.actions}/>
                        : null}
                </div>
                {this.state.showChildren ?
                    <div className="block-children">
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
});

function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

class SearchInput extends React.Component {
    static defaultProps = {
        value: ''
    };

    constructor(props) {
        super(props);

        this.sendForSearch = debounce(this.sendForSearch, 300);
    }

    sendForUpdate = (e) => {
        this.props.onChange(e.target.value);

        if (e.target.value.length > 2) {
            this.sendForSearch(e.target.value);
        }

        if (e.target.value == "") this.sendForSearch(null);
    };

    sendForSearch = (searchTerm) => {
        this.props.onSearch(searchTerm);
    };

    render() {
        return (
            <input
                type="text"
                onChange={this.sendForUpdate}
                value={this.props.value}/>
        )
    }
}

class IndicatorTreeView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            data: []
        }

        this._handleSearchChange = debounce(this._handleSearchChange, 500);
    }

    static defaultProps = {
        allowCheck: false,
        checked: [],
        value: null,
        hideInactive: true
    };

    componentWillMount() {
        this._init();
        ewars.subscribe("RELOAD_INDICATORS", this._init);
    }

    _init = () => {
        ewars.tx("com.ewars.indicators", [null])
            .then(function (resp) {
                this.setState({
                    data: resp
                });
                ewars.emit("RELOAD_CHILDS");
            }.bind(this))
    };

    _onSearchChange = (value) => {
        this.setState({
            search: value
        })
    };

    _onSearch = (val) => {
        if (val == "" || val == null) {
            this._init();
        } else {
            ewars.tx("com.ewars.query", ["indicator", null, {"name.en": {like: val}}, null, null, null, null])
                .then(resp => {
                    this.setState({
                        data: resp
                    })
                })
        }
    };

    _clearSearch = () => {
        this.setState({
            search: ""
        });

        this._init();
    };

    _onItemAction = (action, data) => {
        this.props.onAction(action, data);
    };


    render() {
        let nodes = this.state.data.map((item) => {
            if (this.state.search != "" && this.state.search != null) {
                return (
                    <IndicatorNode
                        key={item.uuid || `FOLDER-${item.id}`}
                        actions={this.props.actions}
                        onEdit={this.props.onEdit}
                        onAction={this._onItemAction}
                        data={item}/>
                );
            } else {
                return (

                    <IndicatorFolderNode
                        key={"FOLDER_" + item.id}
                        {...this.props}
                        onAction={this._onItemAction}
                        data={item}/>
                )
            }
        });

        return (
            <ewars.d.Layout>
                <ewars.d.Row height={45}>
                    <ewars.d.Cell>
                        <div className="search">
                            <div className="search-inner">
                                <ewars.d.Row>
                                    <ewars.d.Cell addClass="search-left" width={25}>
                                        <i className="fal fa-search"></i>
                                    </ewars.d.Cell>
                                    <ewars.d.Cell addClass="search-mid">
                                        <SearchInput
                                            value={this.state.search}
                                            onChange={this._onSearchChange}
                                            onSearch={this._onSearch}/>
                                    </ewars.d.Cell>
                                    {this.state.search != "" ?
                                        <ewars.d.Cell
                                            addClass="search-right"
                                            width={25}
                                            onClick={this._clearSearch}>
                                            <i className="fal fa-times"></i>
                                        </ewars.d.Cell>
                                        : null}
                                </ewars.d.Row>
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {nodes}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>

        )
    }
}

export default IndicatorTreeView;
