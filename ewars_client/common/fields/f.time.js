const STYLE = {
    fontSize: "12px",
    border: "1px solid #e3e7e9",
    padding: "7px 5px 7px 5px",
    margin: "0px",
    color: "#525252"
}

class TimeField extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        if (this.props.onUpdate) this.props.onUpdate(this.props.name, e.target.value);
        if (this.props.onChange) this.props.onChange(this.props.n, e.target.value);

    };

    render() {

        return (
            <input
                onChange={this._onChange}
                value={this.props.value}
                style={STYLE}
                type="time"/>
        )

    }
}

export default TimeField;
