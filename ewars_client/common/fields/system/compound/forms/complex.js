exprt default {
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        required: true
    },
    value_type: {
        type: "select",
        label: {
            en: "Value Type"
        },
        options: [
            ["NUMERIC", "Numeric"],
            ["PERCENT", "Percentage"]
        ]
    }
};
