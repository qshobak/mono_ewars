export default {
    // General
    title: null,
    show_title: true,
    type: "SERIES",
    dataType: "SERIES",
    tools: false,
    zoom: false,
    animate: false,
    navigator: false,
    legend_enabled: false,
    series: []
};
