export default {
    indicatorUUID: null,
    modifier: null,
    interpolate_missing: false,
    polyfill: false,
    dataType: "AGG_SERIES"
}
