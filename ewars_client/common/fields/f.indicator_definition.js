import Button from "../c.button";
import TextField from "./f.text";
import SelectField from "./f.select";

const REDUCTION = {
    options: [
        ["SUM", "Sum"],
        ["MEDIAN", "Median"]
    ]
};

class CompoundManager extends React.Component {
    constructor(props) {
        super(props);
    }

    _onUpdate = (prop, value) => {
        this.props.onUpdate(prop, value);
    };

    render() {
        return (
            <div className="basic">
                <div className="hsplit-box">
                    <div className="ide-setting-label">Formula</div>
                    <div className="ide-setting-control">
                        <TextField
                            name="formula"
                            value={this.props.data.definition.formula}
                            onUpdate={this._onCompoundChange}/>
                    </div>
                </div>
                <div className="vsplit-box">
                    <div className="ide-setting-label">Indicators</div>
                    <div className="ide-setting-control">
                        <IndicatorList data={this.props.data} onUpdate={this._onUpdate}/>
                    </div>
                </div>
            </div>
        )

    }
}


class IndicatorItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            indicator: {}
        }

    }

    componentWillMount() {
        ewars.tx("com.ewars.resource", ["indicator", this.props.data, ["uuid", "name", "definition", "itype", "icode"], null])
            .then(function (resp) {
                this.setState({
                    indicator: resp
                })
            }.bind(this))
    }

    _remove = () => {
        this.props.onRemove(this.props.index);
    };

    render() {
        let indName = this.state.indicator.icode || "" + " " + ewars.I18N(this.state.indicator.name || "Loading");

        return (
            <div className="block" style={{padding: 0}}>
                <div className="block-content" style={{padding: 0}}>
                    <div className="ide-layout">
                        <div className="ide-row">
                            <div className="ide-col" style={{padding: 8}}>{indName}</div>
                            <div className="ide-col" style={{padding: 8}}>
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        onClick={this._remove}
                                        icon="fa-times"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


class IndicatorList extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDragEnter = (e) => {
        e.preventDefault();
    };

    _onDragLeave = (e) => {
    };

    _onDropped = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let data = JSON.parse(e.dataTransfer.getData("item"));

        let definition = ewars.copy(this.props.data.definition);
        definition.series.push(data.uuid);

        this.props.onUpdate("series", definition.series);
    };

    _remove = (index) => {
        let definition = ewars.copy(this.props.data.definition);
        definition.series.splice(index, 1);
        this.props.onUpdate("series", definition.series);
    };

    render() {
        let view;
        if (this.props.data.definition.series.length <= 0) {
            view = <div className="placeholder">Drag 'n' Drop indicators here</div>;
        } else {
            view = this.props.data.definition.series.map(function (item, index) {
                return <IndicatorItem
                    data={item}
                    key={item.uuid || item}
                    onRemove={this._remove}
                    index={index}/>;
            }.bind(this))
        }

        return (
            <div className="ind-drop-zone" onDragOver={this._onDragEnter} onDragLeave={this._onDragLeave}
                 onDrop={this._onDropped}>
                <div className="block-tree">
                    {view}
                </div>
            </div>
        )
    }
}

class AggregateManager extends React.Component {
    constructor(props) {
        super(props);
    }

    _onUpdate = (prop, value) => {
        this.props.onUpdate(prop, value);
    };

    render() {
        return (
            <div className="basic">
                <div className="hsplit-box">
                    <div className="ide-setting-label">Aggregation Type</div>
                    <div className="ide-setting-control">
                        <SelectField
                            name="reduction"
                            onUpdate={this._onUpdate}
                            config={REDUCTION}
                            value={this.props.data.definition.reduction}/>
                    </div>
                </div>
                <div className="vsplit-box">
                    <div className="ide-setting-label">Indicators</div>
                    <div className="ide-setting-control">
                        <IndicatorList data={this.props.data} onUpdate={this._onUpdate}/>
                    </div>
                </div>
            </div>
        )
    }
}


var IndicatorDefinition = React.createClass({
    getInitialState: function () {
        return {
            definition: {}
        }
    },

    _updateDefinition: function (prop, value) {
        let data = ewars.copy(this.props.value.definition);
        data[prop] = value;


        this.props.onUpdate("definition", data);
    },

    render: function () {

        let edit;
        if (this.props.value) {
            if (this.props.value.itype == "COMPOUND") edit =
                <CompoundManager data={this.props.value} onUpdate={this._updateDefinition}/>;
            if (this.props.value.itype == "AGGREGATE") edit =
                <AggregateManager data={this.props.value} onUpdate={this._updateDefinition}/>;
        }

        return <div></div>;
    }
});

export default IndicatorDefinition;

