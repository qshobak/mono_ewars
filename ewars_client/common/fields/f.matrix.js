import MatrixRow from "./f.matrix.row";
import MatrixCell from "./f.matrix.cell";

const SORT_FIELDS = (a, b) => {
    let aO = parseInt(a.order),
        bO = parseInt(b.order);
    if (aO > bO) return 1;
    if (aO < bO) return -1;
    return 0;
}

/**
 * Retrieve the form definition fields as a flat array
 * @param sourceFields
 * @returns {Array.<T>}
 * @private
 */
function _getFieldsAsArray(sourceFields) {
    var fields = [];
    for (var token in sourceFields) {
        var field = sourceFields[token];
        if (typeof field == 'object') {
            field.name = token;
            fields.push(field);

        }
    }

    return fields.sort(SORT_FIELDS);
}

var MatrixField = React.createClass({
    getInitialState: function () {
        return {};
    },

    _handleCellChange: function (prop, value, path) {
        this.props.onUpdate(prop, value, path);
    },

    _processheaders: function () {
        if (this.props.config.headers) {
            var headers = [];
            for (var i in this.props.config.headers) {
                headers.push(
                    <th className="matrix-header-cell">&nbsp;</th>
                )
            }

            return headers;
        }
    },
    _getRuleResult: function (rule, data) {
        var result;

        var sourceValue = ewars.getKeyPath(rule[0], data);
        if (sourceValue == null || sourceValue == undefined) return false;

        const isNumber = (val) => {
            try {
                parseFloat(val);
                return true;
            } catch (e) {
                return false;
            }
        };

        switch (rule[1]) {
            case "eq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) >= 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue == rule[2];
                }
                break;
            case "ne":
            case "neq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) < 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue != rule[2];
                }
                break;
            case "gt":
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) > parseFloat(rule[2]);
                }
                break;
            case "lt":
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) < parseFloat(rule[2]);
                }
                break;
            case 'gte':
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) >= parseFloat(rule[2]);
                }
                break;
            case 'lte':
                if (!isNumber(sourceValue)) {
                    result = false;
                } else {
                    result = parseFloat(sourceValue) <= parseFloat(rule[2]);
                }
                break;
            default:
                result = false;
                break
        }

        return result;
    },

    _checkFieldVisible: function (field, data) {
        if (field.conditional_bool == true) {
            var result = false;

            if (!field.conditions) return true;
            if (!field.conditions.rules) return true;

            if (["ANY", "any"].indexOf(field.conditions.application) >= 0) {
                // Only one of the rules has to pass

                for (var conIdx in field.conditions.rules) {
                    var rule = field.conditions.rules[conIdx];

                    var tmpResult = this._getRuleResult(rule, data);
                    if (result != true && tmpResult == true) result = true;
                }

            } else {
                var ruleCount = field.conditions.rules.length;
                var rulePassCount = 0;

                for (var ruleIdx in field.conditions.rules) {
                    var rule = field.conditions.rules[ruleIdx];
                    var ruleResult = this._getRuleResult(rule, data);
                    if (ruleResult) rulePassCount++;
                }

                if (ruleCount == rulePassCount) result = true;

            }
            return result;
        } else {
            return true;
        }
    },


    _processDefinition: function () {
        let result = []

        let sorted = this.props.config.fields.sort((a, b) => {
            if (parseFloat(a.order) > parseFloat(b.order)) return 1;
            if (parseFloat(a.order) < parseFloat(b.order)) return -1;
            return 0;
        });

        sorted.forEach((row, index) => {

            let visible = true;
            if (row.conditional_bool) {
                visible = this._checkFieldVisible(row, this.props.data);
            }

            if (visible) {

                let cells = [];
                let cellDefinition = [];
                row.fields.forEach(field => {
                    cellDefinition.push({
                        ...field,
                        name: `${this.props.name}.${row.name}.${field.name}`
                    })
                })

                cellDefinition = cellDefinition.sort(SORT_FIELDS);

                cellDefinition.forEach((cell, index) => {
                    let isVisible = true;
                    if (cell.conditional_bool) {
                        isVisible = this._checkFieldVisible(cell, this.props.data);
                    }

                    if (this.props.map[cell.type] && isVisible) {
                        var FieldControl = this.props.map[cell.type];
                        let value = this.props.data[cell.name] || "";
                        cells.push(
                            <MatrixCell
                                key={"CELL_" + cell.uuid}
                                errors={this.props.errors}
                                field={cell}>
                                <FieldControl
                                    data={this.props.data}
                                    name={cell.name}
                                    value={value}
                                    key={"FIELD_" + cell.uuid}
                                    options={cell.options ? cell.options : null}
                                    config={cell}
                                    readOnly={this.props.readOnly}
                                    onUpdate={this._handleCellChange}/>

                            </MatrixCell>
                        )
                    } else {
                        var Field = this.props.map._typeMap.none;
                        cells.push(
                            <MatrixCell>
                                <Field/>
                            </MatrixCell>
                        )
                    }
                })

                result.push(<MatrixRow
                    key={row.uuid}
                    data={row}>
                    {cells}
                </MatrixRow>)
            }

        }, this);

        return result;
    },

    _processRow: function (rowDefinition) {
        return row;
    },

    render: function () {
        var content = this._processDefinition();
        return (
            <div className="matrix">
                <table className="matrix-table">
                    <tbody>
                    {content}
                    </tbody>
                </table>
            </div>
        )
    }
});

export default MatrixField;
