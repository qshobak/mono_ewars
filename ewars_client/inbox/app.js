import InboxComponent from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    Form,
    Panel
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Button,
    Modal,
    Toolbar,
    Form,
    Panel
}

ReactDOM.render(
    <InboxComponent/>,
    document.getElementById("application")
);
