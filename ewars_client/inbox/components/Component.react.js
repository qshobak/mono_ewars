import {
    Button,
    Tab,
    Layout,
    Row,
    Cell,
    Toolbar
} from "../../common";

import InboxView from "./views/InboxView.react";
import Activity from "./views/Activity.react";
import Tasks from "./views/Tasks.react";

const PAGER_STYLE = {
    display: "block",
    textAlign: "right",
    paddingTop: 12
};

class Section extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick = (e) => {
        this.props.onClick(this.props.view);
    };

    render() {
        return (
            <div className="block" onClick={this._onClick}>
                <div className="block-content">
                    <i className={"fal " + this.props.icon}></i>&nbsp;{this.props.label}
                </div>
            </div>
        )
    }
}

class InboxComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showEditor: false,
            view: "NOTIFICATIONS"
        }
    }

    createMessage = (e) => {
        this.setState({
            showEditor: true
        })
    };

    _changeView = (view) => {
        this.setState({
            view: view
        })
    };

    render() {

        let view;

        if (this.state.view == "NOTIFICATIONS") view = <InboxView/>;
        if (this.state.view == "ACTIVITY") view = <Activity/>;
        if (this.state.view == "TASKS") view = <Tasks/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width={250} borderTop={true}>
                        <div className="block-tree">
                            <Section
                                label="Inbox"
                                icon="fa-envelope"
                                onClick={this._changeView}
                                view="NOTIFICATIONS"/>
                            <Section
                                label="Tasks"
                                icon="fa-tasks"
                                onClick={this._changeView}
                                view="TASKS"/>
                            <Section
                                label="Activity"
                                icon="fa-bullhorn"
                                onClick={this._changeView}
                                view="ACTIVITY"/>
                        </div>

                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true}>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }

}


export default InboxComponent;
