import TasksWidget from "../../../common/widgets/widget.tasks";

class Tasks extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TasksWidget/>
        )
    }
}

export default Tasks;
