import Moment from "moment";

import {
    Button,
    Layout, Row, Cell,
    Toolbar
} from "../../../common";

const PAGER_STYLE = {
    display: "inline-block",
    textAlign: "right",
    paddingTop: 6,
    paddingRight: 8
};

class Thread extends React.Component {
    constructor(props) {
        super(props);
    }

    _delete = () => {
        let bl = new ewars.Blocker(null, "Deleting notification...");
        ewars.tx("com.ewars.notification.delete", [this.props.data.uuid])
            .then((resp) => {
                bl.destroy();
                ewars.emit("RELOAD_INBOX");
            });
    };

    render() {
        let blockClass = "block-content";
        if (!this.props.data.read) blockClass += " status-green";

        return (
            <div className="block" onClick={() => this.props.onClick(this.props.data)}>
                <div className={blockClass}>
                    <Row>
                        <Cell style={{padding: 6}}>
                            {this.props.data.subject}
                        </Cell>
                        <Cell width={80} style={{padding: 6}}>
                            {ewars.DATE(this.props.data.sent, "DAY")}
                        </Cell>
                        <Cell width={30}>
                            <ewars.d.Button
                                icon="fa-trash"
                                onClick={this._delete}/>

                        </Cell>
                    </Row>
                </div>
            </div>
        )
    }
}

class ThreadItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="widget">
                <div className="widget-header">
                    <span>{Moment(this.props.data.created).format()}</span>
                </div>
                <div className="body">
                    {this.props.data.content}
                </div>
            </div>
        )
    }
}

class MessageView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let threads = [];
        if (this.props.data.message_type == "NOTIFICATION") {
            threads = (
                <div className="article">
                    {this.props.data.thread.content}
                </div>
            )
        } else {
            threads = this.props.data.thread.map(function (item) {
                return <ThreadItem
                    data={item}/>
            }.bind(this))
        }

        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">{this.props.data.subject}</div>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    icon="fa-times"
                                    onClick={this.props.closeThread}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll" style={{padding: 14}}>
                            <div className="article" dangerouslySetInnerHTML={{__html: this.props.data.thread.content}}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class InboxView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            activeMessage: null,
            offset: 0,
            limit: 20,
            total: null
        }
    }

    _next = () => {
        this._query(this.state.offset + this.state.limit);
    };

    _prev = () => {
        this._query(this.state.offset - this.state.limit);
    };

    componentWillMount() {
        this._query(0);
        ewars.subscribe("RELOAD_INBOX", this._query);
    }

    _query = (offset) => {
        ewars.tx("com.ewars.notifications", null, {offset: offset, limit: this.state.limit})
            .then((resp) => {
                this.setState({
                    messages: resp.items,
                    total: resp.total,
                    offset: offset
                })
            });
    };

    /**
     * Select a message to view/edit
     * @param data
     * @private
     */
    _selectMessage = (data) => {
        ewars.tx("com.ewars.notification.read", [data.uuid])
            .then((resp) => {
                this.setState({
                    activeMessage: data
                })
            });
    };

    _closeThread = () => {
        this.setState({
            activeMessage: null
        })
    };

    _remove = (index) => {
        let copy = ewars.copy(this.state.messages);
        copy.splice(index, 1);
        this.setState({
            messages: copy
        })
    };

    _clear = () => {
        let bl = new ewars.Blocker(null, "Clearing notifications...");

        ewars.tx("com.ewars.notifications.clear")
            .then((resp) => {
                bl.destroy();
                this.setState({
                    messages: []
                })
            })
    };

    render() {

        let messages = this.state.messages.map(function (item, index) {
            return <Thread
                onClick={this._selectMessage}
                index={index}
                remove={this._remove}
                data={item}/>
        }.bind(this));

        if (this.state.messages.length <= 0) {
            messages = <div className="placeholder">You currently do not have any notifications.</div>
        }

        let startPag = 1;
        let endPag = this.state.offset + this.state.limit;
        if (this.state.offset > 0) startPag = this.state.offset + 1;
        let pagination = `${startPag} to ${endPag} of ${this.state.total}`;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Notifications">

                    <div style={{display: "block", float: "right"}}>
                        <span style={PAGER_STYLE}>{pagination}</span>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-trash"
                                onClick={this._clear}
                                label="Delete All"/>
                        </div>
                        <div className="btn-group pull-right">
                            {this.state.offset != 0 ?
                                <ewars.d.Button
                                    onClick={this._prev}
                                    icon="fa-caret-left"/>
                                : null}
                            <ewars.d.Button
                                onClick={this._next}
                                icon="fa-caret-right"/>
                        </div>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: "relative"}}>
                                {messages}
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.activeMessage ?
                    <div className="message-view">
                        <MessageView
                            closeThread={this._closeThread}
                            data={this.state.activeMessage}/>
                    </div>
                    : null}
            </ewars.d.Layout>
        )
    }
}

export default InboxView;
