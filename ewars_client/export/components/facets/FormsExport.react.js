import { Form } from "../../../common";
import Moment from "moment";
import LocationTreeView from "../../../locations/components/LocationTreeView.react";
import {
    AssignmentField as AssignmentLocationSelect
} from "../../../common/fields";

var EXPORT_FORM = {
    start_date: {
        type: "date",
        label: "Start Date",
        required: true
    },
    end_date: {
        type: "date",
        label: "End Date",
        required: true
    },
    location_id: {
        type: "location",
        label: "Location",
        required: true
    },
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"},
                exportable: {eq: true}
            }
        },
        required: true
    }
};


class FormsExport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "LOCATIONS",
            export: {
                start_date: Moment.utc().format("YYYY-MM-DD"),
                end_date: Moment.utc().format("YYYY-MM-DD"),
                form_id: null,
                location_id: null
            },
            errors: null
        }
    }

    _onChange = (data, prop, value, additional) => {
        if (window.user.role == "USER" && prop == "location_id") {
            this.setState({
                ...this.state,
                export: {
                    ...this.state.export,
                    form_id: additional[0].form_id,
                    location_id: additional[1].location_id
                }
            })
        } else {

            this.setState({
                ...this.state,
                errors: null,
                export: {
                    ...this.state.export,
                    [prop]: value
                }
            })
        }
    };

    _export = () => {
        let valid = true,
            errors = {};


        if (!this.state.export.location_id) {
            valid = false;
            errors.location_id = "Please specify a valid location";
        }

        if (!this.state.export.start_date) {
            valid = false;
            errors.start_date = "Please specify a valid start date";
        }

        if (!this.state.export.end_date) {
            valid = false;
            errors.end_date = "Please specify a valid end date";
        }

        if (!this.state.export.form_id) {
            valid = false;
            errors.form_id = "Please specify a valid form";
        }

        if (!valid) {
            this.setState({
                errors: errors
            });
            return;
        }

        let blocker = new ewars.Blocker(null, "Building data...");
        let data = {
            location_id: this.state.export.location_id,
            start_date: this.state.export.start_date,
            end_date: this.state.export.end_date,
            form_id: this.state.export.form_id
        };

        ewars.tx("com.ewars.export", ["FORM", data])
            .then(function (resp) {
                blocker.destroy();
                if (resp.err) {
                    ewars.growl("No data available for the specified parameters");
                } else {
                    this._promptDownload(resp);
                }
            }.bind(this));
    };

    _promptDownload = (def) => {
        window.location.href = "http://" + ewars.domain + "/download/" + def.fn;
    };

    render() {
        let data = ewars.copy(this.state.export);
        if (window.user.role == "USER") {
            EXPORT_FORM.location_id.type = "assignment";
            EXPORT_FORM.location_id.label = "Assignment";
            data.location_id = {
                location_id: this.state.export.location_id,
                form_id: this.state.export.form_id
            };
            delete EXPORT_FORM.form_id;
        }


        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                label="Export"
                                                icon="fa-download"
                                                onClick={this._export}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="widget">
                                            <div className="widget-header"><span>Export Options</span></div>
                                            <div className="body">
                                                <Form
                                                    definition={EXPORT_FORM}
                                                    updateAction={this._onChange}
                                                    data={data}
                                                    errors={this.state.errors}
                                                    readOnly={false}/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormsExport;
