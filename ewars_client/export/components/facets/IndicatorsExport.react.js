import {
    SelectField,
    DateField,
    SwitchField
} from "../../../common/fields";

import { Tab } from "../../../common";
import LocationTreeView from "../../../locations/components/LocationTreeView.react";
import IndicatorTreeComponent from "../../../indicators/components/IndicatorTreeComponent.react";

function* entries(obj) {
    for (let key of Object.keys(obj)) {
        yield [key, obj[key]];
    }
}

const FIELDS = {
    location_type: {
        optionsSource: {
            resource: "location_type",
            query: {},
            valSource: "id",
            labelSource: "name"
        }
    },
    select_type: {
        options: [
            ["CHILD_TYPE", "Children of type"],
            ["SELECTED", "Selected"]
        ],
        required: true
    },
    interval: {
        options: [
            ["NONE", "None"],
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Annual"]
        ],
        required: true
    },
    startDate: {
        date_type: "DAY",
        require: true
    },
    endDate: {
        date_type: "DAY",
        required: true
    }
};

class IndicatorItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showOptions: false
        }
    }

    render() {
        let name = ewars.I18N(this.props.data.name);

        return (
            <div className="block">
                <div className="block-content">
                    {name}
                </div>
                {this.state.showOptions ?
                    <div className="block-children">

                    </div>
                    : null}
            </div>
        )
    }
}


class IndicatorsExport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "LOCATIONS",
            location: [],
            indicator: [],
            indDefs: {},
            select_type: "SELECTED",
            interval: null,
            startDate: new Date(),
            endDate: new Date(),
            transpose: false,
            errors: {}
        }

    }

    _onChangeView = (data) => {
        this.setState({
            view: data.view
        })
    };

    _onLocationAction = (action, data) => {
        let location = this.state.location;

        if (location.indexOf(data.uuid) >= 0) {
            let index = location.indexOf(data.uuid);
            location.splice(index, 1);
        } else {
            location.push.apply(location, [data.uuid]);
        }
        this.setState({
            location: location
        })
    };

    _onFieldChange = (prop, value, path) => {
        this.setState({
            ...this.state,
            [prop]: value
        })
    };

    _onIndicatorAction = (action, data) => {
        let indicator = this.state.indicator;
        let indDefs = this.state.indDefs;

        if (indicator.indexOf(data.uuid) >= 0) {
            let index = indicator.indexOf(data.uuid);
            indicator.splice(index, 1);
            if (indDefs[data.uuid]) delete indDefs[data.uuid];
        } else {
            indicator.push.apply(indicator, [data.uuid]);
            indDefs[data.uuid] = data;
        }

        this.setState({
            indicator: indicator,
            indDefs: indDefs
        })
    };

    _export = () => {
        let valid = true,
            errors = {};


        if (!this.state.indicator) {
            valid = false;
            errors.indicator = "Please specify an indicator to export";
        } else if (this.state.indicator.length <= 0) {
            valid = false;
            errors.indicator = "Please specify an indicator to export";
        }

        if (!this.state.interval) {
            valid = false;
            errors.interval = "Please specify a valid interval";
        }

        if (!this.state.location) {
            valid = false;
            errors.location = "Please specify a location";
        } else if (this.state.location.length <= 0) {
            valid = false;
            errors.location = "Please specify a location";
        }


        if (!this.state.select_type) {
            valid = false;
            errors.location = "Please select a location specification";
        }

        if (!this.state.endDate) {
            valid = false;
            errors.startDate = "Please specify a valid start date";
        }

        if (!this.state.startDate) {
            valid = false;
            errors.startDate = "Please specify a valid start date"
        }

        if (this.state.select_type == "CHILD_TYPE") {
            if (!this.state.location_type) {
                valid = false;
                errors.location_type = "Please specify a location type to export"
            }
        }

        if (!valid) {
            this.setState({
                ...this.state,
                errors: errors
            });
            return;
        }


            let blocker = new ewars.Blocker(null, "Building data...");
        let data = {
            indicator: this.state.indicator,
            location: this.state.location,
            start_date: this.state.startDate,
            end_date: this.state.endDate,
            interval: this.state.interval,
            location_type: this.state.location_type,
            select_type: this.state.select_type,
            transpose: this.state.transpose
        };

        ewars.tx("com.ewars.export", ["INDICATOR", data])
            .then(function (resp) {
                blocker.destroy();
                this._promptDownload(resp);
            }.bind(this));
    };

    _promptDownload = (def) => {
        window.location.href = "http://" + ewars.domain + "/download/" + def.fn;
    };

    render() {

        let view;
        let parentId;
        if (window.user.role == "REGIONAL_ADMIN") parentId = window.user.location_id;

        if (this.state.view == "LOCATIONS") view = <LocationTreeView
            hideInactive={true}
            onAction={this._onLocationAction}
            parentId={parentId}
            checked={this.state.location}
            allowCheck={true}/>;

        if (this.state.view == "INDICATORS")
            view = <IndicatorTreeComponent
                allowCheck={true}
                onAction={this._onIndicatorAction}
                checked={this.state.indicator}
                hideInactive={true}/>;

        FIELDS.startDate.date_type = this.state.interval;
        FIELDS.endDate.date_type = this.state.interval;

        let indicators = [];
        for (let [key, value] of entries(this.state.indDefs)) {
            indicators.push(
                <IndicatorItem
                    data={value}/>
            )
        }

        let numIndicators = indicators.length;

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col border-right" style={{maxWidth: 350}}>
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 36}}>
                                <div className="ide-col">
                                    <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs"
                                         style={{display: "block"}}>
                                        <Tab
                                            label="Locations"
                                            icon="fa-map-marker"
                                            active={this.state.view == "LOCATIONS"}
                                            onClick={this._onChangeView}
                                            data={{view: "LOCATIONS"}}/>
                                        <Tab
                                            label="Indicators"
                                            icon="fa-chart-line"
                                            active={this.state.view == "INDICATORS"}
                                            onClick={this._onChangeView}
                                            data={{view: "INDICATORS"}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    {view}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                label="Export"
                                                icon="fa-download"
                                                onClick={this._export}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        {this.state.errors.location ?
                                            <p className="error" style={{margin: 8}}>{this.state.errors.location}</p>
                                        : null}

                                        {this.state.errors.indicator ?
                                            <p className="error" style={{margin: 8}}>{this.state.errors.indicator}</p>
                                        : null}
                                        <div className="widget">
                                            <div className="widget-header"><span>Export Options</span></div>
                                            <div className="body">

                                                <div className="hsplit-box">
                                                    <div className="ide-setting-label">Aggregation Interval*</div>
                                                    <div className="ide-setting-control">
                                                        <SelectField
                                                            name="interval"
                                                            onUpdate={this._onFieldChange}
                                                            config={FIELDS.interval}
                                                            value={this.state.interval}/>
                                                    </div>
                                                </div>
                                                    {this.state.errors.interval ?
                                                        <p className="error">Please select a valid aggregation
                                                            interval</p>
                                                        : null}

                                                <div className="hsplit-box">
                                                    <div className="ide-setting-label">Target Location Selection*</div>
                                                    <div className="ide-setting-control">
                                                        <SelectField
                                                            name="select_type"
                                                            value={this.state.select_type}
                                                            onUpdate={this._onFieldChange}
                                                            config={FIELDS.select_type}/>
                                                    </div>
                                                </div>
                                                    {this.state.errors.select_type ?
                                                        <p className="error">{this.state.errors.select_type}</p>
                                                        : null}

                                                {this.state.select_type == "CHILD_TYPE" ?
                                                    <div className="hsplit-box">
                                                        <div className="ide-setting-label">Location Type*</div>
                                                        <div className="ide-setting-control">
                                                            <SelectField
                                                                name="location_type"
                                                                config={FIELDS.location_type}
                                                                value={this.state.location_type}
                                                                onUpdate={this._onFieldChange}/>
                                                        </div>
                                                        {this.state.errors.location_type ?
                                                            <p className="error">Please select a valid location type</p>
                                                            : null}
                                                    </div>
                                                    : null}

                                                <div className="hsplit-box">
                                                    <div className="ide-setting-label">Transpose</div>
                                                    <div className="ide-setting-control">
                                                        <SwitchField
                                                            name="transpose"
                                                            value={this.state.transpose}
                                                            onUpdate={this._onFieldChange}/>
                                                    </div>
                                                </div>


                                                <div className="hsplit-box">
                                                    <div className="ide-setting-label">Start Date*</div>
                                                    <div className="ide-setting-control">
                                                        <DateField
                                                            name="startDate"
                                                            onUpdate={this._onFieldChange}
                                                            value={this.state.startDate}
                                                            config={FIELDS.startDate}/>
                                                    </div>
                                                </div>
                                                    {this.state.errors.startDate ?
                                                        <p className="error">Please select a valid start date</p>
                                                    : null}

                                                <div className="hsplit-box">
                                                    <div className="ide-setting-label">End Date*</div>
                                                    <div className="ide-setting-control">
                                                        <DateField
                                                            name="endDate"
                                                            onUpdate={this._onFieldChange}
                                                            value={this.state.endDate}
                                                            config={FIELDS.endDate}/>
                                                    </div>
                                                </div>
                                                {this.state.errors.endDate ?
                                                    <p className="error">Please select a valid end date</p>
                                                    : null}
                                            </div>
                                        </div>

                                        <div className="widget">
                                            <div className="widget-header"><span>Indicators ({numIndicators})</span>
                                            </div>
                                            <div className="body no-pad">
                                                <div className="block-tree" style={{position: "relative"}}>
                                                    {indicators}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        )
    }
}

export default IndicatorsExport;
