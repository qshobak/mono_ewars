import LocationTreeView from "../../../locations/components/LocationTreeView.react";

var EXPORT_FORM = {
    location_id: {
        type: "location",
        label: "Location",
        required: true
    },
    site_type_id: {
        type: "select",
        label: "Location Type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    },
    status: {
        type: "select",
        label: "Location Status",
        options: [
            ["ANY", "Any"],
            ["ACTIVE", "Active"],
            ["DISABLED", "Disabled"]
        ],
        required: true
    }
};

class LocationsExport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "LOCATIONS",
            location: [],
            export: {
                location_id: null,
                site_type_id: null,
                status: 'ACTIVE'
            },
            errors: null
        }
    }

    _onLocationAction = (action, data) => {
        let location = this.state.location;

        if (location.indexOf(data.uuid) >= 0) {
            let index = location.indexOf(data.uuid);
            location.splice(index, 1);
        } else {
            location.push.apply(location, [data.uuid]);
        }
        this.setState({
            location: location
        })
    };

    _export = () => {
        let valid = true,
            errors = {};

        if (!this.state.export.location_id) {
            valid = false;
            errors.location_id = "Please specify a valid location";
        }

        if (!this.state.export.status) {
            valid = false;
            errors.status = "Please select a valid location status";
        }

        if (!valid) {
            this.setState({
                errors: errors
            });
            return;
        }

        let bl = new ewars.Blocker(null, "Building data...");
        ewars.tx("com.ewars.export", ["LOCATIONS", this.state.export])
            .then(resp => {
                bl.destroy();
                if (resp.err) {
                    ewars.growl("No data available for the specified parameters");
                } else {
                    this._download(resp.fn);
                }
            })
    };

    _download = (uri) => {
        window.location.href = "http://" + ewars.domain + "/download/" + uri;
    };

    _onChange = (data, prop, value) => {
        this.setState({
            ...this.state,
            errors: null,
            export: {
                ...this.state.export,
                [prop]: value
            }
        })
    };

    render() {

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                label="Export"
                                                icon="fa-download"
                                                onClick={this._export}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="widget">
                                            <div className="widget-header"><span>Export Options</span></div>
                                            <div className="body">
                                                <ewars.d.Form
                                                    definition={EXPORT_FORM}
                                                    data={this.state.export}
                                                    readOnly={false}
                                                    errors={this.state.errors}
                                                    updateAction={this._onChange}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default LocationsExport;
