import AlertsExport from "./facets/AlertsExport.react";
import IndicatorsExport from "./facets/IndicatorsExport.react";
import FormsExport from "./facets/FormsExport.react";
import LocationsExport from "./facets/LocationsExport";

import { Tab } from "../../common";

var EXPORT_FORM = require("../forms/ExportForm");

var ExportComponent = React.createClass({
    getInitialState: function () {
        return {
            view: "FORMS",
            errors: []
        }
    },

    _export: function () {
        let mapper = {
            "INDICATOR": this._handleINDICATOR,
            "FORM": this._handleFORM,
            "ALERTS": this._handleALERTS
        };

        var errors = Validator(EXPORT_FORM, this.state.export);

        if (errors) {
            this.setState({
                errors: errors
            });
            return;
        }

        this.setState({errors: null});

        mapper[this.state.export.export_type]();
    },

    _handleALERTS: function () {
        let blocker = new ewars.Blocker(null, "Building data...");
        let data = {
            alert_ids: this.state.export.alert_id,
            location_id: this.state.export.location_uuid,
            start_date: ewars.DATE(this.state.export.start_date, "DAY"),
            end_date: ewars.DATE(this.state.export.end_date, "DAY"),
            options: {}
        };

        ewars.tx("com.ewars.export", [this.state.export_type, data])
            .then(function (resp) {
                blocker.destroy();
                this._promptDownload(resp);
            }.bind(this))
    },

    _handleFORM: function () {
        let blocker = new ewars.Blocker(null, "Building data...");
        let data = {
            form_id: this.state.export.form,
            location_id: this.state.export.location_uuid,
            start_date: ewars.DATE(this.state.export.start_date, "DAY"),
            end_date: ewars.DATE(this.state.export.end_date, "DAY"),
            options: {}
        };

        ewars.tx("com.ewars.export", [this.state.export.export_type, data])
            .then(function (resp) {
                blocker.destroy();
                this._promptDownload(resp);
            }.bind(this))
    },

    _promptDownload: function (def) {
        window.location.assign("http://" + ewars.domain + "/report/download/" + def.fn);
    },

    _handleINDICATOR: function () {
        var options = {};
        options.start_date = this.state.export.start_date;
        options.end_date = this.state.export.end_date;

        let ds = new DataSource({
            type: "SERIES",
            indicator: this.state.export.indicator,
            location: this.state.export.location_uuid,
            interval: this.state.export.time_interval,
            start_date: this.state.export.start_date,
            end_date: this.state.export.end_date
        }, false);

        ds.load(function (source) {
            this._buildIndicatorExport(ds);
            this.refs.exportBtn.unsetProcessing();
        }.bind(this));
    },

    _buildIndicatorExport: function (ds) {
        var export_data = "data:text/csv;filename=export.csv;charset=utf-8,";
        var cols = [];
        var row = [];

        cols.push("Location");

        var joiner = ",";

        var sorted = _.sortBy(ds.data, function (data) {
            return data[0]
        }, this);

        // Add Columns
        _.each(sorted, function (item) {
            cols.push(Moment(item[0]).format(this.state.export.date_format));
        }, this);

        row.push(ewars.formatters.I18N_FORMATTER(ds.locationDefinition.name));

        _.each(sorted, function (item) {
            row.push(item[1]);
        }, this);

        export_data += cols.join(joiner) + "\n";
        export_data += row.join(joiner);

        var encoded = encodeURI(export_data);
        window.open(encoded);
    },

    _onChangeView: function (data) {
        this.setState({
            view: data.view
        })
    },

    render: function () {

        let view;

        if (this.state.view == "FORMS") view = <FormsExport/>;
        if (this.state.view == "INDICATORS") view = <IndicatorsExport/>;
        if (this.state.view == "ALERTS") view = <AlertsExport/>;
        if (this.state.view == "LOCATIONS") view = <LocationsExport/>;

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs" style={{display: "block"}}>
                                <Tab
                                    label="Form Submissions"
                                    icon="fa-clipboard"
                                    active={this.state.view == "FORMS"}
                                    onClick={this._onChangeView}
                                    data={{view: "FORMS"}}/>
                                {window.user.role != "USER" ?
                                    <Tab
                                        label="Indicator Data"
                                        icon="fa-chart-line"
                                        active={this.state.view == "INDICATORS"}
                                        onClick={this._onChangeView}
                                        data={{view: "INDICATORS"}}/>
                                    : null}
                                <Tab
                                    label="Alerts"
                                    icon="fa-exclamation-triangle"
                                    active={this.state.view == "ALERTS"}
                                    onClick={this._onChangeView}
                                    data={{view: "ALERTS"}}/>
                                {window.user.role != "USER" ?
                                    <Tab
                                        label="Locations"
                                        icon="fa-map-marker"
                                        active={this.state.view == "LOCATIONS"}
                                        onClick={this._onChangeView}
                                        data={{view: "LOCATIONS"}}/>
                                    : null}
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});

export default ExportComponent;
