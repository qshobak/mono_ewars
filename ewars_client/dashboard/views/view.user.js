class ViewUser extends React.Component {
    state = {
        sideView: "LOCATIONS",
        view: "ACTIVITY"
    }
    constructor(props) {
        super(props);
    }

    render() {
        console.log(window.user);
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="50%" borderRight={true} style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Row height="150px" borderBottom={true} style={{background: "#F2F2F2"}}>
                            <ewars.d.Cell width="60px">
                                <i className="fal fa-user"></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell style={{display: "flex", flexDirection: "column"}}>
                                {window.user.name} ({window.user.email})<br/>
                                {window.user.role}<br/>
                                {window.user.org_name ?
                                    <ewars.d.Cell>{window.user.org_name}</ewars.d.Cell>
                                    : null}
                                {window.user.location_name ?
                                    <ewars.d.Cell>{window.user.location_name.en || window.user.location_name}</ewars.d.Cell>
                                : null}
                                <a href="/profile">Edit profile</a> | <a href="#">Change password</a>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <ewars.d.Toolbar style={{padding: "0"}}>
                            <div className="ux-tabs">
                                <div className="ux-tab active">
                                    <i className="fal fa-map-marker"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-bell"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-users"></i>
                                </div>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "flex", overflowY: "auto"}}>

                        </ewars.d.Row>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar style={{padding: "0"}}>
                            <div className="ux-tabs">
                                <div className="ux-tab active">
                                    <i className="fal fa-bullhorn"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-clock"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-clipboard"></i>
                                </div>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "block", overflowY: "auto"}}>

                        </ewars.d.Row>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewUser;