class ViewRoles extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="250px" borderRight={true} style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar>

                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "block", overflowY: "auto"}}>
                            <div className="dash-handle">
                                <i className="fal fa-lock"></i>&nbsp;Account Administrator
                            </div>
                            <div className="dash-handle">
                                <i className="fal fa-lock"></i>&nbsp;Geographic Administrator
                            </div>
                            <div className="dash-handle">
                                <i className="fal fa-lock"></i>&nbsp;Reporting User
                            </div>
                        </ewars.d.Row>

                    </ewars.d.Cell>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewRoles;