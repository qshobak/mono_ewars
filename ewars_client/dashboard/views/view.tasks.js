import { Modal } from "../../common";
import RedactionRequest from "../../common/tasks/RedactionRequest.react";
import AmendmentRequest from "../../common/tasks/AmendmentRequest.react";
import AssignmentRequest from "../../common/tasks/AssignmentRequest.react";
import RegistrationRequest from "../../common/tasks/RegistrationRequest.react";

const TASK_TYPES = {
    REDACTION_REQUEST: RedactionRequest,
    AMENDMENT_REQUEST: AmendmentRequest,
    ASSIGNMENT_REQUEST: AssignmentRequest,
    REGISTRATION_REQUEST: RegistrationRequest
};


const priorities = {
    "HIGH": <div className="priority high">High</div>,
    "MID": <div className="priority mid">Moderate</div>,
    "MODERATE": <div className="priority mid">Moderate</div>,
    "LOW": <div className="priority low">Low</div>
};

const TASK_DATA = {
    REDACTION_REQUEST: {
        icon: "fa-trash",
        Cmp: RedactionRequest,
        format: function (data) {
            var str = "<strong>Deletion Request:</strong> " + ewars.I18N(data.form_name) + " " + ewars.formatters.DATE_FORMATTER(data.report_date, data.form_interval) + " in " + ewars.I18N(data.location_name);
            return str;
        }
    },
    AMENDMENT_REQUEST: {
        icon: "fa-pencil",
        Cmp: AmendmentRequest,
        format: function (data) {
            var str = "<strong>Amendment Request:</strong> " + ewars.I18N(data.form_name) + " " + ewars.formatters.DATE_FORMATTER(data.report_date, data.form_interval) + " in " + ewars.I18N(data.location_name);
            return str;
        }
    },
    REGISTRATION_REQUEST: {
        icon: "fa-user-plus",
        Cmp: RegistrationRequest,
        format: function (data) {
            var str = "<strong>User Account Request:</strong> " + data.user_name + " [" + data.email + "]";
            return str;
        }
    },
    ASSIGNMENT_REQUEST: {
        icon: "fa-clipboard",
        Cmp: AssignmentRequest,
        format: function (data) {
            var str = "<strong>Assignment Request:</strong> " + ewars.I18N(data.form_name) + " - " + ewars.I18N(data.location_name) + " - " + data.user_name + " [" + data.user_email + "]";
            return str;
        }
    }
};

class TaskItem extends React.Component {
    state = {
        modal: false
    }

    _onClick = () => {
        this.props.onSelect(this.props.data);
    };


    render() {
        var label = TASK_DATA[this.props.data.task_type].format(this.props.data.data);

        var priority = priorities[this.props.data.priority];

        var icon = 'fal fa-thumb-tack';
        if (TASK_DATA[this.props.data.task_type]) icon = "fal " + TASK_DATA[this.props.data.task_type].icon;

        return (
            <div className="block-content task-item" style={{padding: 0}} onClick={this._onClick}>
                <div className="ide-row">
                    <div className="ide-col" style={{padding: 8}}>
                        <div className="content" dangerouslySetInnerHTML={{__html: label}}></div>
                    </div>
                    <div className="ide-col" style={{maxWidth: 100, padding: 8}}>
                        {priority}
                    </div>
                </div>
            </div>
        )
    }
}

class ViewTasks extends React.Component {
    state = {
        tasks: [],
        filter: null,
        modal: false,
        data: null
    };

    constructor(props) {
        super(props);

        this._load();
    }

    _load = () => {
        ewars.tx("com.ewars.user.tasks", [])
            .then(res => {
                this.setState({
                    tasks: res
                })
            })
            .catch(err => {
                console.log(err);
                ewars.error("Error loading your tasks");
            })
    };

    _select = (data) => {
        this.setState({
            data: data
        })
    };

    _closeTask = () => {
        this.setState({
            data: null
        }, () => {
            this._load();
        })
    };

    render() {
        let tasks = [];

        if (this.state.filter) {
            let _tasks = this.state.tasks.filter(item => {
                return item.task_type == this.state.filter;
            })
            tasks = _tasks.map(item => {
                return (
                    <TaskItem
                        filter={this.state.filter}
                        onSelect={this._select}
                        data={item}/>
                )

            })
        } else {
            tasks = this.state.tasks.map(item => {
                return (
                    <TaskItem
                        filter={this.state.filter}
                        onSelect={this._select}
                        data={item}/>
                )
            })
        }

        let counts = {
            ALL: this.state.tasks.length,
            REGISTRATION_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "REGISTRATION_REQUEST"
            }).length,
            ASSIGNMENT_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "ASSIGNMENT_REQUEST";
            }).length,
            AMENDMENT_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "AMENDMENT_REQUEST";
            }).length,
            RETRACTION_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "REDACTION_REQUEST";
            }).length,
            ACCESS_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "ACCESS_REQUEST";
            }).length,
            ORG_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "ORG_REQUEST";
            }).length,
            LOC_REQUEST: this.state.tasks.filter(item => {
                return item.task_type == "LOC_REQUEST";
            }).length
        };

        let taskView;
        if (this.state.data) {
            let Cmp = TASK_TYPES[this.state.data.task_type];
            console.log(Cmp, this.state.data.task_type);
            taskView = <Cmp
                onClose={this._closeTask}
                data={this.state.data}/>
        }


        return (
            <ewars.d.Layout style={{minHeight: 0}}>
                <ewars.d.Row style={{minHeight: 0}}>
                    <ewars.d.Cell width="250px" borderRight={true} style={{overflowY: "auto", background: "#333333"}}>
                        <div
                            onClick={() => this.setState({filter: null})}
                            className="dash-handle">
                            All Tasks ({counts.ALL})
                        </div>

                        <div className="dash-section">
                            <i className="fal fa-clipboard"></i>&nbsp;Admin Tasks
                        </div>

                        <div
                            onClick={() => this.setState({filter: "REGISTRATION_REQUEST"})}
                            className="dash-handle sub-item">
                            Registration requests ({counts.REGISTRATION_REQUEST})
                        </div>
                        <div onClick={() => this.setState({filter: "ACCESS_REQUEST"})}
                             className="dash-handle sub-item">
                            Access requests ({counts.ACCESS_REQUEST})
                        </div>
                        <div
                            onClick={() => this.setState({filter: "ASSIGNMENT_REQUEST"})}
                            className="dash-handle sub-item">
                            Assignment requests ({counts.ASSIGNMENT_REQUEST})
                        </div>
                        <div
                            onClick={() => this.setState({filter: "AMENDMENT_REQUEST"})}
                            className="dash-handle sub-item">
                            Amendment requests ({counts.AMENDMENT_REQUEST})
                        </div>
                        <div
                            onClick={() => this.setState({filter: "ORG_REQUEST"})}
                            className="dash-handle sub-item">
                            Organization requests ({counts.ORG_REQUEST})
                        </div>
                        <div
                            onClick={() => this.setState({filter: "LOC_REQUEST"})}
                            className="dash-handle sub-item">
                            Location requests ({counts.LOC_REQUEST})
                        </div>
                        <div
                            onClick={() => this.setState({filter: "REDACTION_REQUEST"})}
                            className="dash-handle sub-item">
                            Record deletion requests ({counts.RETRACTION_REQUEST})
                        </div>

                        <div className="dash-section">
                            <i className="fal fa-clipboard"></i>&nbsp;Reporting
                        </div>

                        <div className="dash-section">
                            <i className="fal fa-bell"></i>&nbsp;Alerts
                        </div>


                    </ewars.d.Cell>
                    <ewars.d.Cell style={{overflowY: "auto", display: "block"}}>
                        {tasks}
                    </ewars.d.Cell>
                </ewars.d.Row>
                {taskView}
            </ewars.d.Layout>
        )
    }
}

export default ViewTasks;
