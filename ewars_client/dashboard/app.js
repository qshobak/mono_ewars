import Component from "./components/LegacyComponent";

import {
    Layout,
    Cell,
    Row,
    Toolbar,
    Button,
} from "../common";

ewars.d = {
    Layout,
    Cell,
    Row,
    Toolbar,
    Button
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


