import WIDGETS from "../../common/widgets/widgets";
import Widget from "../../common";
import WidgetEditor from "../../common/editors/widgets/c.widget_editor";

import DateUtils from "../../documents/utils/DateUtils";
import RangeUtils from "../../common/utils/RangeUtils";


const CHARTS = [
    "MAP",
    "SERIES",
    "CATEGORY",
    "GAUGE"
];

function _isSet(value) {
    if (!value) return null;
    if (value == undefined) return null;
    if (value == "") return null;
    return value;
}

class Chart extends React.Component {
    componentDidMount() {
        let chart = WIDGETS[this.props.data.type];

        if (this.props.data.type == "MAP") {
            this._chart = new chart(this.refs.chart, this.props.data, null, null, null, null, this.props.idx);
        } else {
            this._chart = chart(this.refs.chart, this.props.data, null, null, null, null, this.props.idx);
        }
    }

    render() {
        let label;
        if (this.props.data.title) {
            let labelString = ewars.I18N(this.props.data.title);
            label = <span rel={labelString}>{labelString}</span>
        }

        let icon = this.props.data.icon ? <i className={`fal ${this.props.data.icon}`}></i> : null;

        let titleStyle = {};

        let dateRange = DateUtils.processDateSpec(this.props.data, null);

        let start = ewars.DATE(dateRange[0], this.props.data.interval);
        let end = ewars.DATE(dateRange[1], this.props.data.interval);

        let sourceDate = start + " to " + end;
        if (start === end) sourceDate = end;

        let headerStyle = {
            backgroundColor: this.props.data.widgetBaseColor || null,
            paddingRight: this.props.data.widgetHighlightColor ? null : 8
        };

        if (!this.props.data.widgetIcon) {
            titleStyle.paddingLeft = 8;
        }


        let iconBgd = {}, titleBorder = {};
        if (this.props.data.widgetHighlightColor) {
            iconBgd = {
                backgroundColor: this.props.data.widgetHighlightColor ? this.props.data.widgetHighlightColor : null,
                lineHeight: "41px"
            };
            titleBorder = {
                borderTop: `6px solid ${this.props.data.widgetHighlightColor}`
            };
            titleStyle.paddingLeft = 8;
            titleStyle.lineHeight = "28px"
        }

        let wrapperStyle = {};
        let chartStyle = {};
        if (this.props.data.type === "MAP") {
            wrapperStyle.paddingTop = 0;
            wrapperStyle.paddingBottom = 0;
            wrapperStyle.height = "100%";
            chartStyle.height = "100%";
        }

        return (
            <div className={"ide-col " + this.props.width + " widget-cell raw-cell"}>
                <div className="dash-val">
                    <table style={{height: "100%", width: "100%"}} height="100%">
                        <tbody>
                        <tr style={{height: 35}}>
                            <td>
                                <div className="dash-val-header" style={headerStyle}>
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            {icon ?
                                                <td style={{paddingLeft: 8, width: 25, ...iconBgd}}>
                                                    {icon}
                                                </td>
                                                : null}
                                            <td>
                                                <div className="dash-title" style={{...titleStyle, ...titleBorder}}>
                                                    {label}
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="dash-val-content" style={wrapperStyle}>
                                    <div className="chart" ref="chart"></div>
                                </div>
                            </td>
                        </tr>
                        {this.props.data.type != "MAP" ?
                            <tr style={{height: 23}}>
                                <td>
                                    <div className="dash-val-footer">
                                        <div className="controls">
                                            <WidgetDateSource data={sourceDate}/>
                                            {_isSet(this.props.data.widgetFooterText) ?
                                                <WidgetSource data={this.props.data.widgetFooterText}/>
                                                : null}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            : null}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

class RawWidget extends React.Component {
    componentDidMount() {
        this._chart = WIDGETS.RAW(
            this.refs.chart,
            this.props.data,
            null, null, null, null,
            this.props.idx
        )
    }

    componentWillUnmount() {
        this._chart = null;
    }

    componentWillReceiveProps(nextProps) {
        this._chart.updateDefinition(nextProps.data);
    }

    render() {
        let label;
        if (this.props.data.title) {
            let labelString = ewars.I18N(this.props.data.title);
            label = <span rel={labelString}>{labelString}</span>
        }

        let icon = this.props.data.widgetIcon ? <i className={`fal ${this.props.data.widgetIcon}`}></i> : null;

        let titleStyle = {};

        let subTitle = this.props.data.widgetSubText || null;

        let end_date, start_date;
        if (this.props.data.period) {
            end_date = RangeUtils.process(this.props.data.period[1], null);
            start_date = RangeUtils.process(this.props.data.period[0], null, end_date);
        } else {
            let dateCat = DateUtils.processDateSpec(this.props.data);
            end_date = dateCat.end_date;
            start_date = dateCat.start_date;
        }

        let start = ewars.DATE(start_date, this.props.data.interval);
        let end = ewars.DATE(end_date, this.props.data.interval);

        let sourceDate = start + " to " + end;
        if (start === end) sourceDate = end;

        let headerStyle = {
            backgroundColor: this.props.data.widgetBaseColor || null,
            paddingRight: this.props.data.widgetHighlightColor ? null : 8
        };

        if (!this.props.data.widgetIcon) {
            titleStyle.paddingLeft = 8;
        }

        let iconBgd = {}, titleBorder = {};
        if (this.props.data.widgetHighlightColor) {
            iconBgd = {
                backgroundColor: this.props.data.widgetHighlightColor ? this.props.data.widgetHighlightColor : null,
                lineHeight: "41px"
            };
            titleBorder = {
                borderTop: `6px solid ${this.props.data.widgetHighlightColor}`
            };
            titleStyle.paddingLeft = 8;
            titleStyle.lineHeight = "28px"
        }

        return (
            <div className={"ide-col " + this.props.width + " widget-cell raw-cell"}>
                <div className="dash-val">
                    <table style={{height: "100%", width: "100%"}} height="100%">
                        <tbody>
                        <tr style={{height: 35}}>
                            <td>
                                <div className="dash-val-header" style={headerStyle}>
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            {icon ?
                                                <td style={{paddingLeft: 8, width: 25, ...iconBgd}}>
                                                    {icon}
                                                </td>
                                                : null}
                                            <td>
                                                <div className="dash-title" style={{...titleStyle, ...titleBorder}}>
                                                    {label}
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style={{verticalAlign: "middle"}}>
                                <div className="dash-val-content">
                                    <div className="chart" ref="chart"></div>
                                    <div className="dash-val-title" rel={subTitle}>{subTitle}</div>
                                    <div className="dash-val-sub">{sourceDate}</div>
                                </div>
                            </td>
                        </tr>
                        <tr style={{height: 23}}>
                            <td>
                                <div className="dash-val-footer">
                                    <div className="controls">
                                        <WidgetDateSource data={sourceDate}/>
                                        {_isSet(this.props.data.widgetFooterText) ?
                                            <WidgetSource data={this.props.data.widgetFooterText}/>
                                            : null}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

class WidgetDateSource extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="sub-source">
                <div className="sub-handle"><i className="fal fa-calendar"></i></div>
                <div className="content">{this.props.data}</div>
            </div>
        )
    }
}

class WidgetSource extends React.Component {
    render() {
        return (
            <div className="sub-date">
                <div className="sub-handle"><i className="fal fa-compass"></i></div>
                <div className="content">{this.props.data}</div>
            </div>
        )
    }
}

class WidgetEditorControls extends React.Component {
    render() {
        return (
            <div className="sub-edit">
                <div className="handle"><i className="fal fa-pencil"></i></div>
            </div>
        )
    }
}

const MISSING_STYLE = {
    textAlign: "center",
    fontSize: 35,
    position: "absolute",
    top: "50%",
    width: "100%",
    marginTop: -19
};


class NoConfig extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={"ide-col " + this.props.width + " widget-cell"}>
                <div style={MISSING_STYLE}>
                    <i className="fal fa-exclamation-triangle"></i>
                </div>
            </div>
        )
    }
}

class Cell extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {

        if (this.props.data.type == "RAW") {
            let cmp = <RawWidget
                idx={`${this.props.uuid}_${this.props.rowIndex}_${this.props.cellIndex}`}
                width={this.props.width}
                data={this.props.data}/>;
            return cmp;
        }

        if (CHARTS.indexOf(this.props.data.type) >= 0) {
            let cmp = <Chart
                idx={`${this.props.uuid}_${this.props.rowIndex}_${this.props.cellIndex}`}
                width={this.props.width}
                data={this.props.data}/>;

            return cmp;
        }

        let widget;
        if (WIDGETS[this.props.data.type]) {
            let Cmp = WIDGETS[this.props.data.type];
            if (Cmp) {
                widget = <Cmp
                    idx={`${this.props.uuid}_${this.props.rowIndex}_${this.props.cellIndex}`}
                    data={this.props.data}/>;
            } else {
                widget = <NoConfig/>
            }
        }

        if (widget) {
            let headerStyle = {};
            if (this.props.data.headerColor) headerStyle.backgroundColor = this.props.data.headerColor;
            if (this.props.data.headerTextColor) headerStyle.color = this.props.data.headerTextColor;

            let icon;
            if (this.props.data.icon) {
                icon = <i style={headerStyle} className={"fal " + this.props.data.icon}></i>;
            }

            let bodyStyle = {};
            if (this.props.data.widgetBackgroundColor) bodyStyle.backgroundColor = this.props.data.widgetBackgroundColor;
            if (this.props.data.widgetTextColor) bodyStyle.color = this.props.data.widgetTextColor;

            return (
                <div className={"ide-col " + this.props.width + " widget-cell"}>
                    {this.props.data.title ?
                        <div className="ide-tbar" style={headerStyle}>
                            <div className="ide-tbar-text" style={headerStyle}>{icon} {this.props.data.title}</div>
                        </div>
                        : null}
                    {widget}
                </div>
            )
        } else {
            return <NoConfig/>
        }

    }
}

class Row extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let width = 12 / this.props.data.length;
        let counter = 0;
        return (
            <div className="ide-row">
                {this.props.data.map((cell, index) => {
                    counter++;
                    let key = `${cell.type}_${counter}`;
                    return <Cell
                        width={width}
                        data={cell}
                        uuid={this.props.uuid}
                        key={key}
                        rowIndex={this.props.index}
                        cellIndex={index}/>
                })}
            </div>
        )
    }
}

class DashNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showEditor: false,
            showMenu: false
        }
    }

    _menu = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            showMenu: !this.state.showMenu
        })
    };

    _edit = () => {
        this.props.onEdit(this.props.data);
    };

    render() {
        return (
            <div className="inner">
                <div className="title">
                    <a onClick={this._menu} href="#">{this.props.data.title || "New widget"}&nbsp;<i
                        className="fal fa-caret-down"></i></a>
                    {this.state.showMenu ?
                        <div className="node-menu">
                            <div className="menu-inner">
                                <a href="#"><i className="fal fa-eye"></i>&nbsp;View</a>
                                <a href="#" onClick={this._edit}><i className="fal fa-pencil"></i>&nbsp;Edit</a>
                                <a href="#"><i className="fal fa-trash"></i>&nbsp;Remove</a>
                            </div>
                        </div>
                        : null}
                </div>
                <div className="body">

                </div>
            </div>
        )
    }
}

class EditingDashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: null,
            data: {
                layout: [
                    {i: ewars.utils.uuid(), x: 0, y: 0, w: 1, h: 2},
                    {i: ewars.utils.uuid(), x: 1, y: 0, w: 3, h: 2},
                    {i: ewars.utils.uuid(), x: 4, y: 0, w: 1, h: 2}
                ]
            }
        }
    }

    _addItem = () => {
        let layout = this.state.data.layout;
        layout.push({
            i: ewars.utils.uuid(),
            x: 0,
            y: Infinity,
            w: 1,
            h: 1
        })

        this.setState({
            data: {
                ...this.state.data,
                layout: layout
            }
        })
    };

    _onLayoutChange = (layout) => {
        // this.props.onLayoutChange(layout);
        this.setState({
            data: {
                ...this.state.data,
                layout: layout
            }
        })
    };

    onBreakpointChange = (breakpoint, cols) => {
        this.setState({
            breakpoint: breakpoint,
            cols: cols
        });
    };


    _edit = (data) => {
        this.setState({
            editing: data
        })
    };

    render() {
        return (
            <div className="ide-layout">
                <ewars.d.Row height="30px">
                    <div className="dash-title">{this.props.data.name}</div>

                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-cog"/>
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={this._addItem}/>
                    </div>

                </ewars.d.Row>
                <ewars.d.Cell style={{display: "block", overflowY: "auto"}}>
                    <ResponsiveGridLayout className="layout"
                                          cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}
                                          rowHeight={30}
                                          onBreakPointChange={this.onBreakpointChange}
                                          onLayoutChange={this._onLayoutChange}
                                          layout={this.state.data.layout}>
                        {this.state.data.layout.map(item => {
                            return (
                                <div key={item.i} className="dash-node">
                                    <DashNode
                                        onEdit={this._edit}
                                        data={item}/>
                                </div>
                            )
                        })}
                    </ResponsiveGridLayout>
                </ewars.d.Cell>
                {this.state.editing ?
                    <WidgetEditor
                        data={this.state.editing}/>

                    : null}
            </div>
        )
    }
}

class Dashboard extends React.Component {
    static defaultProps = {
        editing: false
    };



    constructor(props) {
        super(props)

    }


    render() {


        let counter = 0;
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={this.props.data.name}>

                </ewars.d.Toolbar>
                <ewars.d.Cell style={{overflowY: "scroll", display: "block", padding: "8px"}}>
                    {this.props.data.definition.map((row, index) => {
                        counter++;
                        let key = `${this.props.data.uuid}_${counter}`;
                        return <Row
                            data={row}
                            uuid={this.props.data.uuid}
                            key={key}
                            index={index}/>
                    })}

                </ewars.d.Cell>
            </ewars.d.Layout>
        )
    }
}

export default Dashboard;
