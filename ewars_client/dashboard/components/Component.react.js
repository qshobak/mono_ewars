import { Spinner } from "../../common";

const style = {
    section: {
        color: "#CCC",
        paddingTop: 8,
        paddingRight: 8,
        paddingBottom: 8,
        paddingLeft: 29,
        borderTop: "1px solid rgba(0,0,0,0.2)",
        borderBottom: "1px solid rgba(0,0,0,0.2)"
    },
    childs: {
        paddingTop: 8,
        background: "rgba(0,0,0,0.2)",
        paddingBottom: 16
    }
};

class DashHandle extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let className = "dash-handle";

        let iconClass, label;
        if (!_.isArray(this.props.data)) {
            if (this.props.activeId == this.props.data.uuid) className += " active";

            iconClass = `fa ${this.props.data.icon || ""}`;
            label = this.props.data.name.replace(" (Default)", "");
        }

        if (_.isArray(this.props.data)) {
            return (
                <div className={className}>
                    <div style={style.section}>{this.props.data[0]}</div>

                    <div style={style.childs}>
                    {this.props.data[2].map((item) => {
                        return (
                            <DashHandle
                                key={item.uuid}
                                child={true}
                                activeId={this.props.activeId}
                                onClick={this.props.onClick}
                                data={item}/>
                        )
                    })}
                    </div>
                </div>
            )
        } else {
            let style = {padding: 8};
            if (this.props.child) style.padding = "8px 8px 8px 27px";
            return (
                <div className={className} onClick={() => {
                    this.props.onClick(this.props.data.uuid)
                }}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={22} style={style}>
                            <i className={iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: 8}}>{label}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            )
        }
    }
}

class NoDashes extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col border-top">
                            <p className="placeholder">There are currently no dashboards configured for your
                                account.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Component extends React.Component {
    static defaultProps = {
        actions: [] // Actions that the system thinks this user needs to undertake
    };

    isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            dashboards: [],
            activeDashboard: null,
            activeDashboardId: null,
            dashConfigs: {},
            dashColor: null
        }
    }

    componentWillMount() {
        if (window.user.user_type != "SUPER_ADMIN") {
            let bl = new ewars.Blocker(null, "Loading dashboards...");

            ewars.tx("com.ewars.user.dashboards", [window.user.id], null)
                .then((resp) => {
                    bl.destroy();
                    this.isLoaded = true;

                    this.setState({
                        dashboards: resp.dashboards,
                        activeDashboardId: resp.dashboards[0].uuid,
                        activeDashboard: resp.initial,
                        dashConfigs: {
                            ...this.state.dashConfigs,
                            [resp.initial.uuid]: resp.initial
                        }

                    })
                })
        }
    }

    _onSelectDashboard = (dashboardId) => {
        // Check if we have the dashboard already
        if (this.state.dashConfigs[dashboardId]) {
            this.setState({
                activeDashboard: this.state.dashConfigs[dashboardId],
                activeDashboardId: dashboardId,
                dashColor: this.state.dashConfigs[dashboardId].color || null
            })
        } else {
            let bl = new ewars.Blocker(null, "Loading dashboard...");
            ewars.tx("com.ewars.user.dashboard", [dashboardId])
                .then((resp) => {
                    bl.destroy();
                    this.setState({
                        activeDashboardId: dashboardId,
                        activeDashboard: resp,
                        dashColor: resp.color || null,
                        dashConfigs: {
                            ...this.state.dashConfigs,
                            [dashboardId]: resp
                        }
                    })
                })
        }
    };

    render() {
        if (this.state.dashboards.length <= 0 && !this.isLoaded) {
            return <Spinner/>
        }

        if (this.state.dashboards.length <= 0) {
            return <NoDashes/>
        }

        let dashboard;

        if (this.state.activeDashboard) {
            dashboard = <Dashboard data={this.state.activeDashboard}/>
        }

        let shoulderStyle = {maxWidth: 250, background: "rgb(134, 137, 143)"};
        if (this.state.dashColor) shoulderStyle.background = this.state.dashColor;

        let actions = [];
        if (this.props.actions.length > 0) {
            actions.push(
                <div className="sys-action" onClick={this._updateTimeZone}>
                    Your profile timezone differs from the your browsers timezone, click here to update your profile time zone
                </div>
            )
        }

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col border-right border-top"
                             style={shoulderStyle}>

                            <div className="ide-panel ide-panel-absolute ide-scroll dashes">
                                {this.state.dashboards.map((dash) => {
                                    return <DashHandle
                                        activeId={this.state.activeDashboardId}
                                        data={dash}
                                        onClick={this._onSelectDashboard}
                                        key={dash.uuid || null}/>
                                })}
                            </div>

                        </div>
                        <div className="ide-col border-top">
                            {dashboard}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Component;
