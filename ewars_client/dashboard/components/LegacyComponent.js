import Dashboard from "./Dashboard.react";
import TasksWidget from "../../common/widgets/widget.tasks"
import ViewUser from "../views/view.user";
import ViewTasks from '../views/view.tasks';

import MainMenu from "../menus/menu.main";
import ReportingMenu from "../menus/menu.reporting";
import AnalysisMenu from "../menus/menu.analysis";
import TeamMenu from "../menus/menu.teams";
import AdminMenu from "../menus/menu.admin";
import AlertsMenu from "../menus/menu.alerts";

import ViewTeam from "../views/view.team";
import AlertsManagerComponent from "../../alarms/components/AlertsManagerComponent.react";
import LocationManager from "../../locations/components/Main";
import ViewRoles from "../views/view.roles";

import {
    Modal,
    Spinner
} from "../../common";



const VIEWS = {
    USER: ViewUser,
    TASKS: ViewTasks,
    DASHBOARD: Dashboard,
    TEAM: ViewTeam,
    ALARMS: AlertsManagerComponent,
    LOCATIONS: LocationManager,
    ROLES: ViewRoles
}

const style = {
    section: {
        color: "#CCC",
        paddingTop: 8,
        paddingRight: 8,
        paddingBottom: 8,
        paddingLeft: 29,
        borderTop: "1px solid rgba(0,0,0,0.2)",
        borderBottom: "1px solid rgba(0,0,0,0.2)"
    },
    childs: {
        paddingTop: 8,
        background: "rgba(0,0,0,0.2)",
        paddingBottom: 16
    }
};


class NoDashes extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col border-top">
                            <p className="placeholder">There are currently no dashboards configured for your
                                account.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


class LegacyComponent extends React.Component {
    static defaultProps = {
        actions: [] // Actions that the system thinks this user needs to undertake
    };

    isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            dashboards: [],
            activeDashboard: null,
            activeDashboardId: null,
            dashConfigs: {},
            dashColor: null,
            editing: null,
            showModal: false,
            menu: "DEFAULT",
            view: "DEFAULT",
            viewConfig: null
        }

        ewars.tx("com.ewars.user.dashboards", [window.user.id])
            .then(res => {
                this.setState({
                    dashboards: res.dashboards,
                    activeDashboard: res.initial,
                    activeDashboardId: res.initial.uuid,
                    view: "DASHBOARD",
                    viewConfig: res.initial
                })
            })
            .catch(err => {
                console.log("HERE", err);
            })
    }

    _setTasks = () => {
        this.setState({
            view: "TASKS",
            activeDashboard: null,
            activeDashboardId: null
        })
    };

    _addDash = () => {
        let newDash = {
            uuid: ewars.utils.uuid(),
            unsaved: true,
            name: "New Dashboard",
            layout: [],
            permissions: {}
        };

        let dashboards = this.state.dashboards;
        dashboards.push(newDash);

        this.setState({
            dashboards: dashboards,
            activeDashboard: newDash,
            activeDashboardId: newDash.uuid,
            showModal: false
        });
    };

    _action = (action) => {
        switch (action) {
            case 'ADD':
                this.setState({
                    showModal: true
                })
                break;
            default:
                break;
        }
    };

    _hideModal = () => {
        this.setState({
            showModal: false
        })
    };

    componentWillMount() {
        if (window.user.user_type != "SUPER_ADMIN") {
            let bl = new ewars.Blocker(null, "Loading dashboards...");

            ewars.tx("com.ewars.user.dashboards", [window.user.id], null)
                .then((resp) => {
                    bl.destroy();
                    this.isLoaded = true;

                    this.setState({
                        dashboards: resp.dashboards,
                        activeDashboardId: resp.dashboards[0].uuid,
                        activeDashboard: resp.initial,
                        dashConfigs: {
                            ...this.state.dashConfigs,
                            [resp.initial.uuid]: resp.initial
                        },
                        view: "DASHBOARD"

                    })
                })
        }
    }

    _onSelectDashboard = (dashboardId) => {
        // Check if we have the dashboard already
        if (this.state.dashConfigs[dashboardId]) {
            this.setState({
                activeDashboard: this.state.dashConfigs[dashboardId],
                activeDashboardId: dashboardId,
                dashColor: this.state.dashConfigs[dashboardId].color || null,
                view: "DASHBOARD"
            })
        } else {
            let bl = new ewars.Blocker(null, "Loading dashboard...");
            ewars.tx("com.ewars.user.dashboard", [dashboardId])
                .then((resp) => {
                    bl.destroy();
                    this.setState({
                        activeDashboardId: dashboardId,
                        activeDashboard: resp,
                        dashColor: resp.color || null,
                        dashConfigs: {
                            ...this.state.dashConfigs,
                            [dashboardId]: resp
                        },
                        view: "DASHBOARD"
                    })
                })
        }
    };

    _setUser = () => {
        this.setState({
            view: "USER",
            activeDashboard: null,
            activeDashboardId: null
        })
    };

    _viewChange = (view, data) => {
        if (view == "DASHBOARD") {
            this._onSelectDashboard(data.uuid);
        } else {
            this.setState({
                view: view,
                viewConfig: data
            })
        }
    };

    render() {
        if (this.state.dashboards.length <= 0 && !this.isLoaded) {
            return <Spinner/>
        }

        if (this.state.dashboards.length <= 0) {
            return <NoDashes/>
        }

        let dashboard;

        if (this.state.activeDashboard) {
            dashboard = <Dashboard data={this.state.activeDashboard}/>
        }

        let shoulderStyle = {
            paddingTop: "8px",
            overflowY: "auto",
            display: "block",
            background: "rgb(134, 137, 143)"
        };
        if (this.state.dashColor) shoulderStyle.background = this.state.dashColor;

        let actions = [];
        if (this.props.actions.length > 0) {
            actions.push(
                <div className="sys-action" onClick={this._updateTimeZone}>
                    Your profile timezone differs from the your browsers timezone, click here to update your profile
                    time zone
                </div>
            )
        }

        let view;

        if (VIEWS[this.state.view]) {
            let Cmp = VIEWS[this.state.view];
            if (this.state.view == "DASHBOARD") {
                view = (
                    <Dashboard
                        data={this.state.activeDashboard}/>
                )
            } else {
                view = (
                    <Cmp data={this.state.viewConfig}/>
                )
            }
        }

        let menu;

        switch (this.state.menu) {
            case "DEFAULT":
            default:
                menu = <MainMenu
                    dashboards={this.state.dashboards}
                    onViewChange={this._viewChange}/>;
                break;
            case "REPORTING":
                menu = <ReportingMenu
                    onViewChange={this._viewChange}/>;
                break;
            case "ANALYSIS":
                menu = <AnalysisMenu
                    onViewChange={this._viewChange}/>;
                break;
            case "TEAMS":
                menu = <TeamMenu
                    onViewChange={this._viewChange}/>;
                break;
            case "ALERTS":
                menu = <AlertsMenu
                    onViewChange={this._viewChange}/>;
                break;
            case "ADMIN":
                menu = <AdminMenu
                    onViewChange={this._viewChange}/>;
                break;
        }


        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <ewars.d.Cell style={{display: "flex", flexDirection: "column"}} width="250px" borderRight={true}>
                        {/*
                        <ewars.d.Toolbar style={{padding: "0"}}>
                            <div className="ux-tabs">
                                <div
                                    onClick={() => this.setState({menu: "DEFAULT"})}
                                    className={"ux-tab" + (this.state.menu == "DEFAULT" ? " active" : "")}>
                                    <i className="fal fa-tachometer"></i>
                                </div>
                                <div
                                    onClick={() => this.setState({menu: "REPORTING"})}
                                    className={"ux-tab" + (this.state.menu == "REPORTING" ? " active" : "")}>
                                    <i className="fal fa-clipboard"></i>
                                </div>
                                <div
                                    onClick={() => this.setState({menu: "ALERTS"})}
                                    className={"ux-tab" + (this.state.menu == "ALERTS" ? " active" : "")}>
                                    <i className="fal fa-bell"></i>
                                </div>
                                <div
                                    onClick={() => this.setState({menu: "ANALYSIS"})}
                                    className={"ux-tab" + (this.state.menu == "ANALYSIS" ? " active" : "")}>
                                    <i className="fal fa-chart-bar"></i>
                                </div>
                                {window.user.role == "ACCOUNT_ADMIN" ?
                                    <div
                                        onClick={() => this.setState({menu: "ADMIN"})}
                                        className={"ux-tab" + (this.state.menu == "ADMIN" ? " active" : "")}>
                                        <i className="fal fa-cogs"></i>
                                    </div>
                                    : null}
                            </div>
                        </ewars.d.Toolbar>
                        */}
                        {menu}

                    </ewars.d.Cell>
                    <div className="ide-col border-top">
                        {view}
                    </div>
                    <Modal
                        title="Add component"
                        icon="fa-cube"
                        visible={this.state.showModal}
                        footer={false}>
                        <ewars.d.Layout>
                            <ewars.d.Row>
                                <ewars.d.Cell style={{padding: "16px"}}>
                                    <div className="addon">
                                        <i className="fal fa-map-marker"></i>
                                        <div className="clearer"></div>
                                        <span>Map</span>
                                    </div>

                                    <div className="addon">
                                        <i className="fal fa-tachometer"></i>
                                        <div className="clearer"></div>
                                        <span>Dashboard</span>
                                    </div>

                                    <div className="addon">
                                        <i className="fal fa-box"></i>
                                        <div className="clearer"></div>
                                        <span>Component</span>
                                    </div>

                                </ewars.d.Cell>
                            </ewars.d.Row>
                            <ewars.d.Toolbar>
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        label="Add"
                                        onClick={this._addDash}
                                        icon="fa-plus"/>
                                    <ewars.d.Button
                                        label="Cancel"
                                        icon="fa-times"/>
                                </div>
                            </ewars.d.Toolbar>
                        </ewars.d.Layout>
                    </Modal>
                </div>
            </div>
        )
    }
}

export default LegacyComponent;
