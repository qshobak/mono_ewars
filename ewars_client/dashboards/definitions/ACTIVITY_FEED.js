const ACTIVITY_FEED = {
    title: {
        type: "text",
        label: {en: "Widget Title"}
    },
    restrict: {
        type: "switch",
        label: {en: "Restrict content to user"}
    }
};

export default ACTIVITY_FEED;