const TEXT = {
    text: {
        type: "textarea",
        label: {en: "Content"},
        i18n: false
    }
};

export default TEXT;