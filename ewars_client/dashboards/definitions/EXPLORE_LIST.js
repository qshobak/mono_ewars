export default {
    title: {
        type: "text",
        label: {en: "Title"}
    },
    spec: {
        type: "select",
        label: {en: "Spec."},
        options: [
            ["ALL", "All Active"],
            ["MANUAL", "Manually Specify"]
        ]
    },
    layouts: {
        type: "select",
        multiple: true,
        label: {en: "Items"},
        optionsSource: {
            resource: "layout",
            valSource: "uuid",
            labelSource: "name",
            query: {}
         },
        conditions: {
            application: "all",
            rules: [
                ["spec", "eq", "MANUAL"]
            ]
        }
    }
}