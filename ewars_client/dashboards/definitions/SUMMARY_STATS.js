export default {
    title: {
        type: "text",
        label: {en: "Widget Title"}
    },
    metrics: {
        type: "select",
        label: {en: "Metrics"},
        multiple: true,
        options: [
            ["COMPLETENESS", "Completeness"],
            ["TASKS", "Tasks"],
            ["TIMELINESS", "Timeliness"],
            ["REPORTS", "Report Count"],
            ["ALERTS", "Active Alerts"],
            ["REPORTING_LOCATIONS", "Reporting Locations"],
            ["ASSIGNMENTS", "Assignments"],
            ["USERS", "Users"],
            ["PARTNERS", "Partners"],
            ["ALARMS", "Alarms"]
        ]
    },
    style: {
        type: "select",
        label: {en: "Display Style"},
        options: [
            ["GRID", "Grid"],
            ["LIST", "List"]
        ]
    }
}