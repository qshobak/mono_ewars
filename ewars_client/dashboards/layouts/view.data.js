import {LitElement, html, css} from "lit-element";

import Toolbar from "../../lib/c.tbar";

class ViewData extends LitElement {
    static get properties() {
        return {}
    }

    static get styles() {
        return css`
            :host {
                display: flex;
                flex-direction: column;
                height: 100%;
                background: #333;
            }

            .view {
                display: block;
                flex: 2;
            }
        `;
    }

    constructor() {
        super();
    }

    _newDashboard() {
        console.log("HERE");
        this.dispatchEvent(new CustomEvent("new-dashboard"));
    }

    _showImport() {
        this.dispatchEvent(new CustomEvent("ShowImport"));
    }

    render() {
        return html`
            <ide-tbar label="Dashboards">
                <div class="btn-group pull-right">
                    <basic-button @click="${this._newDashboard}" label="New Dashboard" icon="fa-plus"></basic-button>
                    <basic-button @click="${this._showImport}" label="Import Dashboard" icon="fa-upload"></basic-button>
                </div>
            </ide-tbar>
            <div class="view">
            </div>
        `;
    }
}

window.customElements.define("view-data", ViewData);
export default ViewData;
