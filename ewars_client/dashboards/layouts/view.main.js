import {css, html, LitElement} from "lit-element";

import ViewData from "./view.data";
import DashboardEditor from "./view.editor";

import DataGrid from "../../lib/datagrid/c.datagrid";
import Btn from "../../lib/buttons/b.button";

class ViewAdminDashboards extends LitElement {
    static get properties() {
        return {
            records: {type: Array},
            name: {type: String},
            view: {type: String},
            dashboard: {type: Object}
        };
    }

    constructor() {
        super();

        this.records = [];
        this.name = "Dashboards";
        this.dashboard = null;

    }

    handleNewDashboard() {
        this.dashboard = {
            name: {en: "New Dashboard"},
            status: "ACTIVE",
            layout: {
                "TEST": {
                    title: {en: "New Widget"},
                    grid: {
                        w: 6,
                        h: 6,
                        x: 0,
                        y: 0
                    }
                }
            },
            variables: {},
            data: {}
        };
    }

    closeDashboard() {
        this.dashboard = {}
    }

    render() {
        let view;

        if (this.dashboard) {
            view = html`
            <dashboard-editor
                @close-dashboard="${() => { this.closeDashboard() }}"
                .data="${this.dashboard}">
            </dashboard-editor>`;
        } else {
            view = html`<view-data @new-dashboard="${() => { this.handleNewDashboard() }}"></view-data>`;
        }

        return view;
    }
}

window.customElements.define("view-admin-dashboards", ViewAdminDashboards);
export default ViewAdminDashboards;
