import { LitElement, css, html } from "lit-element";

import Btn from "../../lib/buttons/b.button";
import Toolbar from "../../lib/c.tbar";
import DashboardPanel from "../lib/controls/c.panel";
import DragAndDropLayout from "../lib/controls/c.dd";

import {setTopLeft} from "../utils";

const WIDGETS = [

];

class DashboardSettings extends LitElement {
    static get properties() {
        return {
            data: {type: Object}
        };
    }

    render() {
        return html`
            <ide-modal>

            </ide-modal>
        `;
    }
}
window.customElements.define("dashboard-settings", DashboardSettings);

//**
// 24 column width layout
// n rows where each row is 30px high for snapping
// **

class DashboardEditor extends LitElement {
    static get properties() {
        return {
            data: {type: Object, attribute: true},
            config: {type: Object, attribute: true},
            showSettings: {type: Boolean}
        };
    }

    static get styles() {
        return css`
            :host {
                box-sizing: border-box;
            }

            .view {
                box-sizing: border-box;
                height: 100%;
                width: 100%;
                position: relative;
                padding: 16px;
                overflow-y: auto;
            }

        `;
    }

    constructor() {
        super();

        this.data = {
            layout: {
                "TEST1": {
                    title: {en: "New Widget"},
                    colStart: 0,
                    colEnd: 4,
                    rowStart: 0,
                    rowEnd: 2
                }
            }
        }
    }

    cancelEdit() {
        this.dispatchEvent(new CustomEvent("close-dashboard"));
    }

    save() {

    }

    itemDragStart(e) {
        e.stopPropagation();
        console.log("ITEM_DRAG_START");
        e.dataTransfer.setData("e", "TEST");
        console.log(e);
    }

    itemDrag(e) {
        console.log("ITEM DRAGGING");
    }

    onItemDrop(e) {
        e.preventDefault();
    }

    addItem() {
        let newId = ewars.utils.uuid();
        this.data.layout[newId] = {
            title: {en: "Another Widget"},
            grid: {
                x: 0,
                y: -1,
                w: 26,
                h: 4
            }
        };
        this.requestUpdate();
    }

    hitTest(e) {
        console.log(e);
    }

    connectedCallback() {
        super.connectedCallback();
        console.log(this.clientHeight);
    }

    render() {
        const layout = [
            {x: 0, y: 0, w: 10, h: 1, i: "0.0.1"},
            {x: 0, y: 1, w: 5, h: 1, i: "0.0.2"},
            {x: 5, y: 1, w: 5, h: 1, i: "0.0.3"},

            {x: 10, y: 0, w: 16, h: 1, i: "0.0.4"}
        ];
        const data = {
            layout,
            cols: 26
        };
        let dashTitle = __(this.data.name);

        return html`
            <ide-tbar label="${dashTitle}">
                <basic-button label="Add Item" icon="fa-plus" @click="${this.addItem}"></basic-button>
                <basic-button label="Settings" icon="fa-cog" @click="${this.showSettings}"></basic-button>
                <basic-button label="Variables" icon="fa-tag" @click="${this.showVariables}"></basic-button>

                <basic-button label="Save Change(s)" icon="fa-save" @click="${this.save}"></basic-button>
                <basic-button label="Cancel" icon="fa-ban" @click="${this.cancel}"></basic-button>
            </ide-tbar>
            <div class="view" ondrop="${this.onItemDrop}">
                <drag-drop-layout
                    .margin="${[10, 10]}"
                    .width="${2000}"
                    .containerPadding="${[10, 10]}"
                    .cols="${26}"
                    .layout="${layout}"
                    .rowHeight="${100}"
                ></drag-drop-layout>
            </div>
        `;
    }
}


window.customElements.define("dashboard-editor", DashboardEditor);
export default DashboardEditor;

