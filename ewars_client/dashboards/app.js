import Component from "./components/Component.react";
import ReactDOM from "react-dom";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Toolbar,
    Panel,
    Button,
    Form
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Toolbar,
    Panel,
    Button,
    Form
};

ReactDOM.render(
    <Component/>,
    document.getElementById("application")
);


/**
import ViewAdminDashboards from "./layouts/view.main";


ewars.tx("com.ewars.conf", ["*"])
    .then(res => {
        window.ewars.config = res;
        let el = document.getElementById("application");
        el.innerHTML = "";
        let root = document.createElement("view-admin-dashboards");
        el.insertBefore(root, el.firstChild);

    })
    .catch(err => {
        console.log(err);
    });
**/
