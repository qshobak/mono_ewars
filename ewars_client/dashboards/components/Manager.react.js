import { DataTable } from "../../common";
import COLUMNS from "../constants/columns";

const JOINS = [
    "user:created_by:id:name,email"
];

const actions = [
    {icon: "fa-pencil", label: "Edit", action: "EDIT"},
    {icon: "fa-trash", label: "Delete", action: "DELETE"},
    {icon: "fa-copy", label: "Duplicate", action: "DUPE"}
];

class LayoutManager extends React.Component {
    constructor(props) {
        super(props);
    }

    _onAction = (action, cell, node) => {
        if (action == "CLICK") this.props.onSelect(node);
        if (action == "EDIT") this.props.onSelect(node);
        if (action == "DELETE") this._delete(node);
        if (action == "DUPE") this._dupe(node);
    };

    _dupe = (node) => {
        let copy = ewars.copy(node);
        copy.uuid = null;

        this.props.onSelect(copy)
    };

    _delete = (node) => {
        ewars.prompt("fa-trash", "Delete dashboard?", "Are you sure you want to delete this dashboard? It will be completely removed from the system.", function () {
            let bl = new ewars.Blocker(null, "Deleting preset");
            ewars.tx("com.ewars.dashboard.delete", [node.uuid])
                .then(resp => {
                    bl.destroy();
                    ewars.emit("RELOAD_DT");
                })
        })
    };

    render() {

        let filter;

        return <div className="ide-layout">
            <div className="ide-row" style={{maxHeight: 35}}>
                <div className="ide-col">
                    <div className="ide-tbar">
                        <div className="ide-tbar-text"><i className="fal fa-tachometer"></i>&nbsp;Dashboards</div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this.props.createNew}
                                label="Create New"
                                icon="fa-plus"/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="ide-row">
                <div className="ide-col">
                    <DataTable
                        join={JOINS}
                        filter={filter}
                        actions={actions}
                        onCellAction={this._onAction}
                        resource="layout"
                        columns={COLUMNS}/>
                </div>
            </div>
        </div>
    }
}

export default LayoutManager;
