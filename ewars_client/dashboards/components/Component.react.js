import Navigator from "../layouts/Navigator.react";
import Editor from "../layouts/Editor.react";

import Manager from "./Manager.react";

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: null,
            view: "DEFAULT"
        }
    }

    _onSelect = (node) => {
        if (!node.definition) node.definition = [];
        this.setState({
            editing: node
        })
    };

    _onChange = (prop, value) => {
        this.setState({
            editing: {
                ...this.state.editing,
                [prop]: value
            }
        })
    };

    _onCloseEdit = () => {
        this.setState({
            editing: null
        })
    };

    _create = () => {
        this.setState({
            editing: {
                name: "",
                description: {en: ""},
                definition: []
            }
        })
    };

    render() {

        let view = <Manager onSelect={this._onSelect} createNew={this._create}/>;
        if (this.state.editing) view = <Editor
            data={this.state.editing}
            onChange={this._onChange}
            onClose={this._onCloseEdit}
            createNew={this._create}/>;


        return (
            <div className="ide">
                {view}
            </div>
        )
    }
}

export default Component;

