import {LitElement, html } from "lit-element";
import d3 from "d3";

class DashboardChart extends LitElement {
    render() {
        return html`
            <p>The Chart</p>
        `;
    }
}

class ChartDataSources extends LitElement {
    render() {
        return html`
            <div class="adm-chart-data-sources">

            </div>
        `;
    }
}

class ChartDataSource extends LitElement {
    render() {
        return html`
            <div class="adm-chart-data-source">

            </div>
        `;
    }
}

class ChartDataSourceSeries extends LitElement {
    render() {
        return html`
            <div class="adm-chart-data-source-series">

            </div>
        `;
    }
}

class ChartDataSourceFunctions extends LitElement {
    render() {
        return html`
            <div class="adm-chart-data-source-functions">

            </div>
        `;
    }
}


customElements.define("dashboard-chart", DashboardChart);

class ChartDataSource extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

    }
}

class ChartDataSourceSeries extends React.Component {

}

class ChartDataSourceFunctions extends React.Component {

}

class QueryInspector extends React.Component {

}

class ChartDataSources extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (

        )
    }
}

class ChartGraphStyle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (

        )
    }
}

class ChartSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

        )
    }
}

class ChartThresholds extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

        )
    }
}

class ChartEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row height="40%" borderBottom={true}>

                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell width="34px" borderRight={true} style={{position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs">
                            <div className="ide-tab"><i className="fal fa-database"></i></div>
                            <div className="ide-tab"><i className="fal fa-chart-line"></i></div>
                            <div className="ide-tab"><i className="fal fa-cog"></i></div>
                            <div className="ide-tab"><i className="fal fa-bell"></i></div>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ChartEditor;
