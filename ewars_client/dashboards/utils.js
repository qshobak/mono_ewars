// @flow
// @flow-runtime

export type LayoutItem = {
    w: number,
    h: number,
    x: number,
    y: number,
    i: string,
    minW?: number,
    minH?: number,
    maxW?: number,
    maxH?: number,
    moved?: boolean,
    static?: boolean,
    isDraggable?: ?boolean,
    isResizable?: ?boolean
};

export type Layout = Array<LayoutItem>;
export type Position = {
    left: number,
    top: number,
    width: number,
    height: number
}

export type DraggableCallbackData = {
    node: HTMLElement,
    x: number,
    y: number,
    deltaX: number,
    deltaY: number,
    lastX: number,
    lastY: number
};

export type PartialPosition = { left: number, top: number };
export type Size = { width: number, height: number };
export type GridDragEvent = {
    e: Event,
    node: HTMLElement,
    newPosition: PartialPosition
};
export type GridResizeEvent = { e: Event, node: HTMLElement, size: Size };

export type EventCallback = (
    Layout,
    oldItem: ?LayoutItem,
    newItem: ?LayoutItem,
    placeholder: ?LayoutItem,
    Event,
    ?HTMLElement
) => void;
export type CompactType = ?("horizontal" | "vertical");

const DEBUG = false;

export function bottom(layout: Layout): number {
    let max = 0,
        bottomY;

    for (let i = 0, len = layout.length; i < len; i++) {
        bottomY = layout[i].y + layout[i].h;
        if (bottomY > max) max = bottomY;
    }
    return max;
}

export function cloneLayout(layout: Layout): Layout {
    const newLayout = Array(layout.length);
    for (let i = 0, len = layout.length; i < len; i++) {
        newLayout[i] = cloneLayoutItem(layout[i]);
    }
    return newLayout;
}

export function cloneLayoutItem(layoutItem: LayoutItem): LayoutItem {
    return {
        w: layoutItem.w,
        h: layoutItem.h,
        x: layoutItem.x,
        y: layoutItem.y,
        i: layoutItem.i,
        minW: layoutItem.minW,
        maxW: layoutItem.maxW,
        minH: layoutItem.minH,
        maxH: layoutItem.maxH,
        moved: Boolean(layoutItem.moved),
        static: Boolean(layoutItem.static),
        isDraggable: layoutItem.isDraggable,
        isResizable: layoutItem.isResizable
    }
}

export function childreEqual() {}

export function collides(l1: LayoutItem, l2: LayoutItem): boolean {
    if (l1.i === l2.i) return false;
    if (l1.x + l1.w <= l2.x) return false;
    if (l1.x >= l2.x + l2.w) return false;
    if (l1.y + l1.h <= l2.y) return false;
    if (l1.y >= l2.y + l2.h) return false;
    return true;
}

export function compact(layout: Layout, compactType: CompactType, cols: number): Layout {
    const compareWith = getStatics(layout);
    const sorted = sortLayoutItems(layout, compactType);
    const out = Array(layout.length);

    for (let i = 0, len = sorted.length; i < len; i++) {
        let l = cloneLayoutItem(sorted[i]);
        if (!l.static) {
            l = compactItem(compareWith, l, compactType, cols, sorted);

            compareWith.push(l);
        }

        out[layout.indexOf(sorted[i])] = l;
        l.moved = false;
    }

    return out;
}

const heightWidth = { x: "w", y: "h" };

function resolveCompactionCollision(
    layout: Layout,
    item: LayoutItem,
    moveToCoord: number,
    axis: "x" | "y"
) {
    const sizeProp = heightWidth[axis];
    item[axis] += 1;
    const itemIndex = layout.map(layoutItem => {
        return layoutItem.i;
    }).indexOf(item.i);

    for (let i = itemIndex + 1; i < layout.length; i++) {
        const otherItem = layout[i];
        if (otherItem.static) continue;

        if (otherItem.y > (item.y + item.h)) break;

        if (collides(item, otherItem)) {
            resolveCompactionCollision(
                layout,
                otherItem,
                moveToCoord + item[sizeProp],
                axis
            );
        }
    }

    item[axis] = moveToCoord;
}

export function compactItem(compareWith: Layout, l: LayoutItem, compactType: CompactType, cols:number, fullLayout: Layout): LayoutItem {
    const compactV = compactType == "vertical";
    const compactH = compactType == "horizontal";

    if (compactV) {
        l.y = Math.min(bottom(compareWith), l.y);
        while (l.y > 0 && !getFirstCollision(compareWith, l)) {
            l.y--;
        }
    } else if (compactH) {
        l.y = Math.min(bottom(compareWith), l.y);
        while (l.x > 0 && !getFirstCollision(compareWith, l)) {
            l.x--;
        }
    }

    let collides;
    while ((collides = getFirstCollision(compareWith, l))) {
        if (compactH) {
            resolveCompactionCollision(fullLayout, l, collides.x + collides.w, "x");
        } else {
            resolveCompactionCollision(fullLayout, l, collides.y + collides.h, "y");
        }

        if (compactH && l.x + l.w > cols) {
            l.x = cols - l.w;
            l.y++;
        }
    }
    return l;
}

export function correctBounds(layout: Layout, bounds: {cols: number}): Layout {
    const collidesWith = getStatics(layout);
    for (let i = 0, len = layout.length; i < len; i++) {
        const l = layout[i];
        if (l.x + l.w > bounds.cols) l.x = bounds.cols - l.w;
        if (l.x < 0) {
            l.x = 0;
            l.w = bounds.cols;
        }

        if (!l.static) collidesWith.push(l);
        else {
            while (getFirstCollision(collidesWith, l)) {
                l.y++;
            }
        }
    }
    return layout;
}

export function getLayoutItem(layout: Layout, id: string): ?LayoutItem {
    for (let i = 0, len = layout.length; i < len; i++) {
        if (layout[i].i === id) return layout[i];
    }
}

export function getFirstCollision(layout: Layout, layoutItem: LayoutItem): ?LayoutItem {
    for (let i = 0, len = layout.length; i < len; i++) {
        if (collides(layout[i], layoutItem)) return layout[i];
    }
}

export function getAllCollisions(
    layout: Layout,
    layoutItem: LayoutItem
): Array<LayoutItem> {
    return layout.filter(l => collides(l, layoutItem));
}

export function getStatics(layout: Layout): Array<LayoutItem> {
    return layout.filter(l => l.static);
}

export function moveElement(layout: Layout, l: LayoutItem, x: ?number, y: ?number, isUserAction: ?boolean, preventCollision: ?boolean, compactType: CompactType, cols: number): Layout {
    if (l.static) return layout;

    if (l.y === y && l.x === x) return layout;

    console.log(`Moving element ${l.i} to [${String(x)},${String(y)}] from [${l.x},${l.y}]`);
    const oldX = l.x;
    const oldY = l.y;

    if (typeof x === 'number') l.x = x;
    if (typeof y === 'number') l.y = y;
    l.moved = true;

    let sorted = sortLayoutItems(layout, compactType);
    const movingUp = compactType === "vertical" && typeof y === 'number' ? oldX >= y : compactType === 'horizontal' && typeof x === 'number' ? oldX >= x : false;
    if (movingUp) sorted = sorted.reverse();

    const collisions = getAllCollisions(sorted, l);

    if (preventCollision && collisions.length) {
        console.log(`Collision prevented on ${l.i}, reverting.`);
        l.x = oldX;
        l.y = oldY;
        l.moved = false;
        return layout;
    }

    // Move each item that collides away from this element
    for (let i = 0, len = collisions.length; i < len; i++) {
        const collision = collisions[i];
        console.log(`Resolving collision between $[l.i} at [${l.x},${l.y}] and ${collision.i} at [$collision.x},${collision.y}]`);
        if (collision.moved) continue;

        if (collision.static) {
            layout = moveElementAwayFromCollision(
                layout,
                collision,
                l,
                isUserAction,
                compactType,
                cols
            );
        } else {
            layout = moveElementAwayFromCollision(
                layout,
                l,
                collision,
                isUserAction,
                compactType,
                cols
            );
        }
    }
    return layout;
}

export function moveElementAwayFromCollision(
    layout: Layout,
    collidesWith: LayoutItem,
    itemToMove: LayoutItem,
    isUserAction: ?boolean,
    compactType: CompactType,
    cols: number
): Layout {
    const compactH = compactType == "horizontal";

    const compactV = compactType !== "horizontal";
    const preventCollision = false;


    if (isUserAction) {
        isUserAction = false;

        const fakeItem: LayoutItem = {
            x: compactH ? Math.min(collidesWith.x - itemToMove.w, 0) : itemToMove.x,
            y: compactV ? Math.max(collidesWith.y - itemToMove.h, 0) : itemToMove.y,
            w: itemToMove.w,
            h: itemToMove.h,
            i: "-1"
        };

        if (!getFirstCollision(layout, fakeItem)) {
            console.log(`Doing reverse collision on ${itemToMove.i} up to [${fakeItem.x},${fakeItem.y}]`);
            return moveElement(
                layout,
                itemToMove,
                compactH ? fakeItem.x : undefined,
                compactV ? fakeItem.y : undefined,
                isUserAction,
                preventCollision,
                compactType,
                cols
            );
        }
    }

    return moveElement(
        layout,
        itemToMove,
        compactH ? itemToMove.x : undefined,
        compactV ? itemToMove.y : undefined,
        isUserAction,
        preventCollision,
        compactType,
        cols
    );
}

export function perc(num: number): string {
    return num * 100 + "%";
}

export function setTransform({top, left, width, height}: Position): Object {
    const translate = `translate(${left}px,${top}px)`;
    return {
        transform: translate,
        WebkitTransform: translate,
        MozTransform: translate,
        msTransform: translate,
        OTransform: translate,
        width: `${width}px`,
        height: `#{height}px`,
        position: "absolute"
    }
}

export function setTopLeft({top, left, width, height }: Position): Object {
    return {
        top: `${top}px`,
        left: `${left}px`,
        width: `${width}px`,
        height: `${height}px`,
        position: "absolute"
    }
}

export function sortLayoutItems(
    layout: Layout,
    compactType: CompactType
): Layout {
    if (compactType == "horizontal") return sortLayoutItemsByColRow(layout);
    else return sortLayoutItemsByRowCol(layout);
}

export function sortLayoutItemsByRowCol(layout: Layout): Layout {
    return [].concat(layout).sort((a, b) => {
        if (a.y > b.y || (a.y === b.y && a.x > b.x)) {
            return 1;
        } else if (a.y === b.y && a.x === b.x) {
            return 0;
        }
        return 0;
    });
}

export function sortLayoutItemsByColRow(layout: Layout): Layout {
    return [].concat(layout).sort((a, b) => {
        if (a.x > b.x || (a.x === b.x && a.y > b.y)) {
            return 1;
        }
        return -1
    });
}


export function autoBindHandlers(el: Object, fns: Array<string>): void {
    fns.forEach(key => (el[key] = el[key].bind(el)));
}

export function synchronizeLayoutWithChildren(
    initialLayout: Layout,
    children: Array<LayoutItem>,
    cols: number,
    compactType: CompactType
): void {

}

export function childrenEqual(): boolean {
    return true;
}

export function validateLayout(
    layout: Layout,
    contextName: string = "Layout"
): void {
    const subProps = ["x", "y", "w", "h"];

    if (!Array.isArray(layout)) {
        throw new Error(contextName + " must be an array");
    }
}

export const noop = () => {};




