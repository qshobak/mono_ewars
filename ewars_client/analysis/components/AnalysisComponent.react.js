var Button = require("../../common/components/ButtonComponent.react");
const Modal = require("../../common/components/ModalComponent.react");

import NotebookEditor from "./NotebookEditor";
import NotebookBrowser from "./NotebookBrowser";
import Notebook from "./Notebook";

var IndicatorUtils = require("../../common/indicators");

const MODAL_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CANCEL"}
];

const STYLES = {
    new: {
        position: "absolute",
        top: "50%",
        marginTop: "-200px",
        textAlign: "center",
        width: "100%"
    },
    newIcon: {
        fontSize: "60px",
        color: "#CCC",
        marginBottom: "20px"
    }
};

class NewCreator extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="map-new" style={STYLES.new}>
                <div className="icon" style={STYLES.newIcon}><i className="fal fa-clipboard"></i></div>
                <div className="text" style={{marginBottom: 20}}>"Open" to view an existing notebook or click "Create"
                    below to create your own.
                </div>
                <div className="button">
                    <Button
                        icon="fa-plus"
                        onClick={this.props.onClick}
                        label="Create Notebook"/>
                </div>
            </div>
        )
    }
}

class AnalysisComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }

    }

    _browse = () => {
        this.setState({
            show: true
        })
    };

    _create = () => {
        this.setState({
            data: {
                uuid: null,
                name: "",
                description: "",
                definition: [],
                created_by: window.user.id,
                shared: false
            }
        })
    };

    onModalAction = (action) => {
        if (action == "CANCEL") this.setState({show: false})
    };

    _onSelect = (data) => {
        this.setState({data: data});
        this.setState({
            show: false
        })
    };

    _onNBChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]:value
            }
        })
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Saving changes...");
        if (this.state.data.uuid) {
            ewars.tx("com.ewars.notebook", ["UPDATE", this.state.data.uuid, this.state.data])
                .then(function (resp) {
                    bl.destroy();
                    this.setState({
                        data: resp
                    })
                }.bind(this))
        } else {
            ewars.tx("com.ewars.notebook", ["CREATE", null, this.state.data])
                .then(function(resp) {
                    bl.destroy();
                    this.setState({
                        data: resp
                    })
                }.bind(this));
        }
    };

    render() {

        let view = <NewCreator onClick={this._create}/>;
        if (this.state.data) {
            if (this.state.data.created_by == window.user.id) {
                view = <NotebookEditor data={this.state.data} onChange={this._onNBChange}/>;
            }
            if (this.state.data.created_by != window.user.id) view = <Notebook data={this.state.data}/>;
        }

        let canEdit = false;
        if (this.state.data) {
            if (this.state.data.created_by == window.user.id) canEdit = true;
        }

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">Analysis Notebooks</div>

                                <div className="btn-group pull-right">
                                    <Button
                                        icon="fa-folder-open"
                                        label="Open"
                                        onClick={this._browse}/>
                                    <Button
                                        icon="fa-plus"
                                        label="Create New"
                                        onClick={this._create}/>
                                    <Button
                                        icon="fa-save"
                                        label="Save Change(s)"
                                        onClick={this._save}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            {view}
                        </div>
                    </div>
                </div>
                <Modal
                    title="Notebook Browser"
                    icon="fa-clipboard"
                    buttons={MODAL_ACTIONS}
                    onAction={this.onModalAction}
                    visible={this.state.show}>
                    <NotebookBrowser onSelect={this._onSelect}/>
                </Modal>
            </div>
        )
    }
}

export default AnalysisComponent;
