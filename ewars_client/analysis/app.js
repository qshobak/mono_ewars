import Component from "./components/Component";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Toolbar,
    Button,
    Panel,
    Modal,
    ActionGroup
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Toolbar,
    Button,
    Panel,
    Modal,
    ActionGroup
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


