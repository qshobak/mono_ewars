class Organization extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: ""
        }
    }

    _search = (e) => {
        this.setState({
            search: e.target.value
        })
    };

    render() {
        return (
            <div className="reg-account">
                <div className="search-wrapper">

                </div>
                <div className="items">
                    <div className="item">Some Organization</div>
                </div>
            </div>
        )
    }
}

export default Organization;