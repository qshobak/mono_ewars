import Selector from "./Selector";

const CODES = {};

const ROLES = {
    USER: "Reporting User",
    ACCOUNT_ADMIN: _l("ACCOUNT_ADMIN"),
    REGIONAL_ADMIN: _l("REGIONAL_ADMIN")
};

class UserApproval extends React.Component {
    render() {
        return (
            <div className="article" style={{paddingTop:8, paddingBottom: 16}}>
                <center>
                    <p>You can accept the invitation by clicking "Complete registration" below.</p>
                    <ewars.d.Button
                        label="Complete registration"
                        color="green"
                        onClick={this.props.doGrant}/>
                </center>
            </div>
        )
    }
}

class Completed extends React.Component {
    _goLogin = () => {
        document.location = "/login";
    };

    render() {
        return (
            <div className="registration-wizard">
                <h1 className="register">Welcome to EWARS</h1>
                <div className="widget">
                    <div className="body">
                        <center>
                            <p className="register">Welcome to EWARS, you can now log in to your account at <a
                                href="http://ewars.ws/login">http://ewars.ws./login</a></p>
                            <ewars.d.Button
                                label="Sign In"
                                onClick={this._goLogin}/>
                        </center>
                    </div>
                </div>
            </div>
        )
    }
}

class NewUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            err: null,
            show: false
        }
    }

    _onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    _selectOrg = (org) => {
        this.setState({show: true});
    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({show: false})
    };

    _onSelect = (org) => {
        this.props.onChange("org_id", org.uuid);
        this.setState({
            show: false,
            organization: org
        })
    };

    _clearOrg = () => {
        this.setState({
            organization: null
        });
        this.props.onChange("org_id", null);
    };

    render() {
        return (
            <div>
                <div className="article"
                     style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30, marginBottom: 40}}>
                    <div className="ide-layout">
                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 150}}></div>
                            <div className="ide-col">

                                <p>Please provide us with the details below to complete your
                                    registration.</p>
                            </div>

                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Your Name *
                            </div>
                            <div className="ide-col">
                                <input type="text"
                                       name="name"
                                       onChange={this._onChange}
                                       value={this.props.data.name}
                                       placeholder="e.g. John Smith..."/>
                            </div>
                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Your Email *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="email"
                                    value={this.props.data.email}
                                    name="email"
                                    placeholder="e.g. j.smith@ewars.ws..."/>
                            </div>
                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Confirm Email *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="email"
                                    value={this.props.data.email_confirm}
                                    name="email_confirm"
                                    placeholder="e.g. j.smith@ewars.ws..."/>
                            </div>
                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Password *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="password"
                                    value={this.props.data.password}
                                    name="password"/>
                            </div>
                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Confirm Password *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="password"
                                    value={this.props.data.password_confirm}
                                    name="password_confirm"/>
                            </div>
                        </div>

                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Organization *
                            </div>
                            <div className="ide-col">
                                {!this.state.organization ?
                                    <ewars.d.Button label="Select Organization" onClick={this._selectOrg}/>
                                    :
                                    <div className="reg-item">
                                        <div className="ide-row">
                                            <div
                                                className="ide-col result-name">{ewars.I18N(this.state.organization.name)}</div>
                                            <div className="ide-col other" style={{maxWidth: 30}}>
                                                <ewars.d.Button icon="fa-close" onClick={this._clearOrg}/>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.show ?
                        <ewars.d.Shade
                            title="Select Item"
                            icon="fa-search"
                            onAction={this._onModalAction}
                            shown={this.state.show}>
                            <Selector
                                resource={"organization"}
                                onSelect={this._onSelect}/>
                        </ewars.d.Shade>
                        : null
                }
            </div>
        )
    }
}

var Component = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        let email = "";
        if (window.user) email = window.user.email;

        return {
            error: null,
            data: {
                name: "",
                email: window.invitation.email || "",
                email_confirm: window.invitation.email || "",
                password: "",
                password_confirm: "",
                org_id: null
            },
            exist: {
                email: window.invitation.user_id
            },
            completed: false,
            view: "NEW"
        }
    },

    componentWillMount() {
        if (window.user) this.state.view = "EXIST";
    },

    _onChange: function (prop, value) {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                [prop]: value
            }
        });
        this.state.data[prop] = value;
        this.forceUpdate();
    },

    _validate: function () {
        console.log("HERE");
        if (!this.state.data.email || this.state.data.email == "") {
            this.setState({error: "Please provide a valid email"});
            return;
        }

        if (this.state.data.email != this.state.data.email_confirm) {
            this.setState({error: "Emails do not match"});
            return;
        }

        if (!this.state.data.password || this.state.data.password == "") {
            this.setState({error: "Please provide a valid password"});
            return;
        }

        if (!this.state.data.password_confirm || this.state.data.password_confirm == "") {
            this.setState({error: "Please confirm the password."});
            return;
        }

        if (this.state.data.password != this.state.data.password_confirm) {
            this.setState({error: "Passwords do not match"});
            return;
        }

        if (!this.state.data.org_id || this.state.data.org_id == "") {
            this.setState({error: "Please select a valid organization or 'Other' if you're unsure."});
            return;
        }

        if (!this.state.data.name || this.state.data.name == "") {
            this.setState({error: "Please provide your full name."});
            return;
        }

        this._process();

    },

    _process: function () {
        let bl = new ewars.Blocker(null, "Validating invitation...");

        let data = JSON.stringify({
            email: this.state.data.email,
            password: this.state.data.password,
            name: this.state.data.name,
            org_id: this.state.data.org_id,
            uuid: window.invitation.uuid
        });

        console.log("HERE");
        let r = new XMLHttpRequest();
        r.open("POST", "/invitation/" + window.invitation.uuid, true);
        r.onreadystatechange = () => {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                this.setState({
                    error: ERROR_CODES[3]
                })
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.code) {
                        this.setState({
                            error: ERROR_CODES[resp.code]
                        })
                    } else {
                        this.setState({completed: true});
                    }
                }
            }
        };

        r.send(data);

    },

    _doGrant: function() {
        let bl = new ewars.Blocker(null, "Processing invitation...");
        let data = JSON.stringify({});

        let r = new XMLHttpRequest();
        r.open("POST", "/invitation/" + window.invitation.uuid, true);

        r.onreadystatechange = () => {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                this.setState({
                    error: ERROR_CODES[3]
                })
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.code) {
                        this.setState({
                            error: ERROR_CODES[resp.code]
                        })
                    } else {
                        this.setState({completed: true});
                    }
                }
            }
        };

        r.send(data);

    },

    render: function () {
        let view,
        showComplete = false;

        if (window.invitation.user_id) {
            // This invite has a real user attached
            view = <UserApproval doGrant={this._doGrant}/>
        } else {
            // This is a complete registration, user needs to fill out form
            showComplete = true;
            view = <NewUser
                error={this.state.error}
                onChange={this._onChange}
                data={this.state.data}/>
        }


        if (this.state.completed) return <Completed/>;

        return (
            <div className="registration-wizard">
                <h1 className="register">EWARS Invitation</h1>
                <p className="register">You've been invited to join the <strong>{ewars.I18N(window.invitation.account_name)}</strong> account as a <strong>{ROLES[window.invitation.details.role]}</strong>, please complete registration below to start using EWARS.</p>

                <div className="widget">
                    <div className="body no-pad">
                        {view}
                    </div>
                    {showComplete ?
                    <div className="widget-footer">
                        {!this.state.completed ?
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Complete registration"
                                    color="green"
                                    icon="fa-submit"
                                    onClick={this._validate}/>
                            </div>
                            : null}
                    </div>
                        : null}
                </div>

                <p className="register">If you've received an invite from us by mistake, or you're unsure of
                    whether you need this account, just close this page, we will not send you any other emails. For
                    additional information please contact <a href="mailto:support@ewars.ws">support@ewars.ws</a></p>
            </div>
        )
    }
});

export default Component;
