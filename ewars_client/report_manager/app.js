import ReportingComponent from "./components/Component.react";

import {
    Layout, Row, Cell,
    Button,
    Toolbar,
    Form,
    Shade,
    Modal,
    Panel
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Toolbar,
    Button,
    Form,
    Shade,
    Modal,
    Panel
};


//// Speed-ups
// Holds tab state
ewars.g.tab_cache = {};
// Holds retrieved form version definition
ewars.g.version_cache = {};
// Holds retrieved form data
ewars.g.forms = {};
// Holds retrieved select lists
ewars.g.select_cache = {};

var routes = {
    form: new RegExp("#\/form\/([0-9]+)"),
    report: new RegExp("#\/report\/([0-9A-Za-z\-]+)"),
    drafts: new RegExp("#\/drafts"),
    entities: new RegExp("#\/entities"),
    assignment: new RegExp("#\/assignment\/([0-9A-Za-z\-]+)")
};

var curLoc = document.location;
var match;

for (var token in routes) {
    var route = routes[token];
    if (route.test(curLoc)) {
        match = {
            token: token,
            reg: route
        };
    }
}

function render(startTabs, startIndex) {
    ReactDOM.render(
        <ReportingComponent
            initialTabs={startTabs}
            initialIndex={startIndex}/>,
        document.getElementById('application')
    );
}

let startTabs = [];

if (match) {
    switch (match.token) {
        case "form":
            var data = match.reg.exec(curLoc);

            ewars.tx("com.ewars.resource", ["form", null, {uuid: {eq: data[1]}}, null, null, null, ["version:version_id:uuid"]])
                .then(function (resp) {
                    startTab = {
                        label: "(DRAFT)" + "-" + ewars.formatters.I18N_FORMATTER(resp.name),
                        nodeType: "form",
                        formId: resp.id,
                        form: resp,
                        data: {
                            created_by: window.user.id,
                            status: "DRAFT",
                            data: {}
                        }
                    };

                    render([startTab], 0);

                }.bind(this));

            break;
        case "report":
            var data = match.reg.exec(curLoc);
            ewars.tx("com.ewars.resource", ["report", data[1], null, ["form:form_id:id", "version:form_version_id:uuid"]])
                .then(function (resp) {
                    resp.form.version = resp.version;
                    startTab = {
                        label: ewars.formatters.DATE_FORMATTER(resp.submitted_date) + "-" + ewars.formatters.I18N_FORMATTER(resp.form.name),
                        nodeType: "REPORT_VIEW",
                        formId: resp.form.id,
                        form: resp.form,
                        report: resp
                    };

                    render([startTab], 0);
                }.bind(this));
            break;
        case "assignment":
            var data = match.reg.exec(curLoc);

            var data_def = {};
            _.each(curLoc.href.split("?")[1].split("&"), function (item) {
                var key = item.split("=")[0];
                var value = item.split("=")[1];
                data_def[key] = value;
            }, this);

            ewars.tx("com.ewars.user.assignment", [data[1]])
                .then(function (resp) {
                    startTab = {
                        label: "Draft: " + ewars.formatters.I18N_FORMATTER(resp.form.name),
                        nodeType: "DRAFT",
                        draft: {
                            created_by: window.user.id,
                            form_id: resp.form.id,
                            form: resp.form,
                            assignment_id: resp.uuid,
                            assignment: resp,
                            status: "DRAFT",
                            data: {},
                            entities: [],
                            form_version_id: resp.form.version.uuid,
                            data_date: data_def.data_date || null,
                            location_id: data_def.location_id || null
                        },
                        form: resp.form,
                        formId: resp.form.id
                    };

                    render([startTab], 0);
                }.bind(this));

            break;
        default:
            render(null, null);
            break;
    }
} else {
    render(null, null);
}



