import {
    DataTable,
    Layout, Row, Cell,
    Toolbar,
    Shade
} from "../../common";
import { SelectField } from "../../common/fields";

import Report from "./Report";

const FORMS = {
    form_id: {
        type: "select",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {}
        },
        label: "Form"
    },
    location_id: {
        type: "location",
        label: "Location"
    },
    source_type: {
        type: "select",
        label: "Source",
        options: [
            ["SYSTEM", _l("SYSTEM")],
            ["IMPORT", _l("IMPORT")],
            ["ANDROID", _l("ANDROID")],
            ["IOS", _l("IOS")],
            ["DESKTOP", _l("DESKTOP")]
        ]
    }
};

const COLUMNS = [
    {name: "form.name", config: FORMS.form_id, filterKey: "form_id"},
    {name: "data_date", config: {label: "Report Date", type: "date"}, fmt: ewars.DATE},
    {name: "user.name", config: {label: "User"}, filterKey: "created_by"},
    {name: "user.email", config: {label: "User Email"}},
    {name: "location.name", config: FORMS.location_id, filterKey: "location_id"},
    {name: "submitted_date", config: {label: "Submitted Date", type: "date"}, fmt: ewars.DATE},
    {name: "source", config: FORMS.source_type, fmt: _l}
];

const JOINS = [
    "user:created_by:id:id,name,email",
    "form:form_id:id:id,name",
    "location:location_id:uuid:uuid,name"
];

const SELECT = [
    "uuid",
    "created_by",
    "form_id",
    "location_id",
    "data_date",
    "account_id",
    "submitted_date",
    "status",
    "source"
];

const OPTIONS_CONFIG = {
    optionsSource: {
        resource: "form",
        labelSource: "name",
        valSource: "id",
        query: {
            status: {eq: "ACTIVE"}
        }
    }
};

const ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

const MODAL_ACTIONS = [
    {label: "Save Change(s)", icon: "fa-save", action: "SAVE"},
    {label: "Cancel", icon: "fa-close", action: "CLOSE"}
];

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collection: null,
            form_id: null,
            showShade: false
        }
    }

    _onCellAction = (action, cell, data) => {
        if (action == "CLICK") this.setState({collection: data, showShade: true});
        if (action == "EDIT") this.setState({collection: data, showShade: true});
        if (action == "DELETE") this._delete(data);
    };

    _delete = (report) => {
        ewars.prompt("fa-trash", "Delete Form Submission?", "Are you sure you want to delete this submission, any associated data will be lost.", () => {
            let bl = new ewars.Blocker(null, "Deleting...");
            ewars.tx("com.ewars.collection.retract", [report.uuid, "Admin deletion"])
                .then(function (resp) {
                    bl.destroy();
                    ewars.emit("RELOAD_DT");
                    ewars.growl("Submission deleted");
                }.bind(this))
        })
    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({collection: null, showShade: false});
    };

    _onFormChange = (prop, value) => {
        let bl = new ewars.Blocker(null, "Loading definition...");
        ewars.tx("com.ewars.form.definition", [value])
            .then(function (resp) {
                bl.destroy();
                console.log(value)
                this.setState({
                    form_id: value,
                    definition: resp
                })
            }.bind(this))
    };

    _onShadeAction = (action) => {
        if (action == "CLOSE") {
            this.setState({collection: null})
        }
    };

    _onCancelEdit = (reload) => {
        this.setState({
            collection: null,
            showShade: false
        });

        if (reload) {
            ewars.emit("RELOAD_DT")
        }
    };

    render() {
        let reportTitle;

        let filter = {status: {eq: "SUBMITTED"}};
        filter.form_id = {eq: this.state.form_id};

        return (
            <Layout>
                <Toolbar label="Form Submission Manager">
                    <div className="ide-tbar-control"
                         style={{width: 350, float: "right", marginTop: -3, marginRight: 3}}>
                        <SelectField
                            name="form_id"
                            value={this.state.form_id}
                            onUpdate={this._onFormChange}
                            config={OPTIONS_CONFIG}/>
                    </div>
                </Toolbar>
                <Row>
                    <Cell>
                        {this.state.form_id ?
                            <DataTable
                                grid={this.state.definition.grid}
                                formId={this.state.form_id}
                                id={"REPORTS_" + this.state.form_id}
                                key={"REPORTS_" + this.state.form_id}
                                header_actions={[]}
                                isReports={true}
                                select={SELECT}
                                paused={this.state.showShade}
                                filter={filter}
                                actions={ACTIONS}
                                onCellAction={this._onCellAction}
                                join={JOINS}/>
                            : null}
                    </Cell>
                </Row>
                <Shade
                    toolbar={false}
                    onAction={this._onShadeAction}
                    shown={this.state.collection != null}>
                    <Report
                        onCancel={this._onCancelEdit}
                        data={this.state.collection}
                        form={this.state.definition}/>
                </Shade>
            </Layout>
        )
    }
}

export default Component;
