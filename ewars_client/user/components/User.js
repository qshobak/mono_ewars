import Moment from "moment";
import Activity from "../../common/widgets/widget.activity";
import Account from "../../register/components/Selector";

class Tab extends React.Component {
    render() {
        let className = "ux-tab";
        if (this.props.active) className += " active";

        return (
            <div
                onClick={() => {
                    this.props.onClick(this.props.id)
                }}
                className={className}>{this.props.label}</div>
        )
    }
}

const TABS = [
    ["Activity", "ACTIVITY"],
    // ["Shared", "SHARED"],
    ["Assignments", "ASSIGNMENTS"]
    // ["Alerts", "ALERTS"]
];

class AssignmentItem extends React.Component {
    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell><strong>{ewars.I18N(this.props.data.form_name)}</strong></ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row>
                        <ewars.d.Cell>{this.props.data.l_full}</ewars.d.Cell>
                    </ewars.d.Row>

                </div>
            </div>
        )
    }
}

class Assignments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentWillMount() {
        if (this.props.user.role === "USER") {
            ewars.tx("com.ewars.profile.assignments", [this.props.user.id])
                .then((resp) => {
                    this.setState({
                        data: resp
                    })
                })
        } else {

        }
    }

    render() {
        if (this.props.user.role !== "USER") {
            return <div className="placeholder">Users with this role type do not have assignments.</div>
        }

        if (!this.state.data) {
            return <div className="placeholder">Loading assignments...</div>
        }

        return (
            <div className="block-tree" style={{position: "relative", textAlign: "left"}}>
                {this.state.data.map((item) => {
                    return <AssignmentItem data={item}/>
                })}
            </div>
        )
    }
}


class User extends React.Component {
    static defaultProps = {
        id: null
    };

    constructor(props) {
        super(props);

        this.state = {
            view: "ACTIVITY",
            user: null
        }
    }

    componentWillMount() {
        if (!this.props.id) {
            let id;

            let hash = document.location.href;
            hash = hash.split("/");
            id = hash[hash.length - 1];
            this._getUser(id);
        } else {
            this._getUser(this.props.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id) {
            this._getUser(nextProps.id);
        }
    }

    _getUser = (id) => {
        ewars.tx("com.ewars.profile", [id])
            .then((resp) => {
                this.setState({
                    user: resp,
                    id: id,
                    view: "ACTIVITY"
                })
            })
    };

    _swapTab = (tab) => {
        this.setState({
            view: tab
        })
    };

    render() {
        if (!this.state.user) {
            return <div className="placeholder">Loading user details...</div>
        }

        let registerDate = Moment.utc(this.state.user.registered).format('MMMM Do YYYY, h:mm:ss a');

        let view;

        if (this.state.view == "ACTIVITY") view = <Activity user_id={this.state.user.id} noMax={true}/>;
        if (this.state.view == "ASSIGNMENTS") view = <Assignments user={this.state.user}/>;

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="user-header">
                    <div className="user-avatar">
                        <img src="/static/image/avatar.mini.png"/>
                    </div>
                    <div className="user-details">
                        <div className="user-name">{this.state.user.name}</div>
                        <div className="user-role">{_l(this.state.user.role)}</div>
                        <div className="user-meta"><a
                            href={`mailto:${this.state.user.email}`}>{this.state.user.email}</a>&nbsp;
                            •&nbsp;{registerDate}
                        </div>
                        <div className="user-connect"><i
                            className="fal fa-building"></i> {ewars.I18N(this.state.user.org_name)}
                        </div>
                    </div>

                    <div className="user-tabs">
                        <div className="ux-tabs">

                            {TABS.map((tab) => {
                                    return <Tab
                                        active={this.state.view === tab[1]}
                                        label={tab[0]}
                                        id={tab[1]}
                                        onClick={this._swapTab}/>;
                                }
                            )}
                        </div>
                    </div>

                    <div className="profile-wrapper">
                        {view}
                    </div>
                </div>
            </div>
        )
    }
}

export default User;
