class TabInstance {
    constructor(props) {
        this.props = props;
    }
}

let state;

class ComponentState {
    constructor() {
        state = this;
        this.tabs = [];
        this.view = 'DEFAULT';
        this.dirty = false;
        this.activeTab = 'DEFAULT';
    }

    addTab = (tab) => {
        let newId = ewars.utils.uuid();
        this.tabs.push({
            _: newId,
            cmp: tab.n,
            n: tab.n,
            i: tab.i || null,
            _id: null
        })
        this.activeTab = newId;

        ewars.emit('VIEW_UPDATED');
    };

    removeTab = (tid) => {
        this.activeTab = 'DEFAULT';
        let rIndex;
        this.tabs.forEach((item, index) => {
            if (item._ == tid) rIndex = index;
        })
        this.tabs.splice(rIndex, 1);
        ewars.emit('VIEW_UPDATED');
    };

    getActiveTab = () => {
        let tab = this.tabs.filter(item => {
            return item._ == this.activeTab;
        })[0] || null;

        return tab;
    };

    setActiveTab = (tid) => {
        this.activeTab = tid;
        ewars.emit('VIEW_UPDATED');
    };

    setView = (view) => {
        this.view = view;
        ewars.emit('VIEW_UPDATED');
    }
}

export default ComponentState;
export {state, ComponentState};