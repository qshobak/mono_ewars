import LocationTree from '../controls/control.locations.tree';

const ACTIONS = [
    ['fa-plus', 'ADD']
];

class TabLocations extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Locations">
                    <ewars.d.ActionGroup
                        right={true}
                        actions={ACTIONS}/>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell width="33.33%" borderRight={true}>
                        <LocationTree/>

                    </ewars.d.Cell>
                    <ewars.d.Cell>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default TabLocations;