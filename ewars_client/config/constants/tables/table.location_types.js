import FIELD_TYPES from '../const.field_types';

export default {
    query: (limit, offset, filters, sorts) => {
        return ewars.tx('com.ewars.users', [limit, offset, filters, sort])
    },
    actions: [
        ['fa-plus', 'CREATE']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT'],
        ['fa-clipboard', 'ASSIGNMENTS'],
        ['fa-ellipsis-v', 'NOOP'],
        ['fa-ban', 'DELETE']
    ],
    columns: [
        {n: 'name', l: 'Name', sort: true},
        {n: 'email', l: 'Email', sort: true},
        {n: 'status', l: 'Status', sort: true},
        {n: 'role', l: 'Role', sort: true},
        {n: 'location_name', l: 'Location', sort: true},
        {n: 'org_name', l: 'Organization', sort: true},
        {n: 'registered', l: 'Registered', sort: true, t: FIELD_TYPES.DATE},
        {n: 'system', l: 'System user', sort: true, t: FIELD_TYPES.SWITCH}

    ],
    initialSort: {
        'registered': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: FIELD_TYPES.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        },
        'role': {
            t: FIELD_TYPES.ROLE
        }
    }
}