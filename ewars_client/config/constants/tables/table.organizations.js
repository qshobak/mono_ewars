import FIELD_TYPES from '../const.field_types';

export default {
    query: (limit, offset, filters, sorts) => {
        return ewars.tx('com.ewars.organizations', [limit, offset, filters, sort])
    },
    actions: [
        ['fa-plus', 'CREATE']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT'],
        ['fa-ellipsis-v', 'NOOP'],
        ['fa-trash', 'DELETE']
    ],
    columns: [
        {n: 'name', l: 'Name', sort: true},
        {n: 'status', l: 'Status', sort: true},
        {n: 'created', l: 'Created', t: FIELD_TYPES.DATE, sort: true},
        {n: 'created_by', l: 'Created by'},
        {n: 'modified', l: 'Modified', t: FIELD_TYPES.DATE, sort: true},
        {n: 'modified_by', l: 'Modified by'},
        {n: 'members', l: 'Members'}
    ],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: FIELD_TYPES.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        }
    }
}