import FIELD_TYPES from '../const.field_types';

export default {
    query: (limit, offset, filters, sorts) => {
        return ewars.tx('com.ewars.assignments', [limit, offset, filters, sort])
    },
    actions: [
        ['fa-plus', 'CREATE']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT'],
        ['fa-ellipsis-v', 'NOOP'],
        ['fa-trash', 'DELETE']
    ],
    columns: [
        {n: 'user_name', l: 'Name', sort: true},
        {n: 'status', l: 'Status', sort: true},
        {n: 'form_name', l: 'Form', sort: true},
        {n: 'location_name', l: 'Location', sort: true},
        {n: 'start_date', l: 'Start date', sort: true, t: FIELD_TYPES.DATE},
        {n: 'end_date', l: 'End date', sort: true, t: FIELD_TYPES.DATE}
    ],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: FIELD_TYPES.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        },
        'location': {
            t: FIELD_TYPES.LOCATION
        },
        'user': {
            t: FIELD_TYPES.USER
        },
        'form': {
            t: FIELD_TYPES.FORM
        }

    }
}