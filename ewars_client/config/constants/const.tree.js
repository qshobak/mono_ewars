const TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
}

export default [
    {
        n: 'Appearance & Behaviour',
        i: 'fa-cog',
        _: 'APPEARANCE',
        t: TYPES.VIEW,
        c: [
            {n: 'Account', i: 'fa-life-ring', _: 'ACCOUNT', t: TYPES.VIEW},
            {n: 'Theme', i: 'fa-paint-brush', _: 'THEME', t: TYPES.VIEW},
            {n: 'Email & Notifications', i: 'fa-envelope', _: 'EMAIL', t: TYPES.VIEW},
            {n: 'Security & Authentication', i: 'fa-key', _: 'SECURITY', t: TYPES.VIEW},
            {n: 'Registration', i: 'fa-list', _: 'REGISTRATION', t: TYPES.VIEW},
            {n: 'Menu', i: 'fa-globe', _: 'MENU', t: TYPES.VIEW},
            {
                n: 'Legal',
                i: 'fa-balance-scale',
                _: 'LEGAL',
                t: null,
                c: [
                    {n: 'Terms of use', i: 'fa-file', _: 'TERMS', t: TYPES.VIEW},
                    {n: 'Privacy policy', i: 'fa-file', _: 'PRIVACY', t: TYPES.VIEW}
                ]
            }
        ]
    },
    {
        n: 'Alarms',
        i: 'fa-bell',
        _: 'ALARMS',
        t: TYPES.TABLE,
        c: [
            {n: 'Settings', i: 'fa-cog', _: 'ALARM_SETTINGS', t: TYPES.VIEW},
            {n: 'Notifications', i: 'fa-envelope', _: 'ALERT_NOTIFICATIONS', t: TYPES.VIEW},
            {n: 'Export', i: 'fa-download', _: 'ALERT_EXPORT', t: TYPES.VIEW},
            {n: 'Integrations', i : 'fa-cubes', _: 'ALERT_INTEGRATIONS', t: TYPES.VIEW},
            {n: 'Admin actions', i: 'fa-bold', _: 'ALARM_ADMIN_ACTIONS', t: TYPES.VIEW}
        ]
    },
    {
        n: 'Locations',
        i: 'fa-map-marker',
        t: TYPES.TAB,
        _: 'LOCATIONS',
        c: [
            {n: 'Location types', i: 'fa-tags', _: 'LOCATION_TYPES', t: TYPES.TABLE},
            {n: 'Location groups', i: 'fa-tags', _: 'LOCATION_GROUPS', t: TYPES.TABLE},
            {n: 'Identification codes', i: 'fa-barcode', _: 'LOCATION_ID_CODES', t: TYPES.TABLE},
            {n: 'Export', i: 'fa-download', _: 'LOCATIONS_EXPORT', t: TYPES.VIEW},
            {n: 'Import', i: 'fa-upload', _: 'LOCATIONS_IMPORT', t: TYPES.VIEW},
            {n: 'Profiles', i: 'fa-map-signs', _: 'LOCATION_PROFILE', t: TYPES.VIEW},
            {n: 'Integrations', i: 'fa-cubes', _: 'LOCATION_INTEGRATIONS', t: TYPES.VIEW},
            {n: 'Reporting', i: 'fa-clipboard', _: 'LOCATION_REPORTING', t: TYPES.TABLE},
            {n: 'Location statuses', _: 'LOCATION_STATUSES', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Users',
        i: 'fa-user',
        _: 'USERS',
        t: TYPES.TABLE,
        c: [
            {n: 'Assignments', i: 'fa-clipboard', _: 'ASSIGNMENTS', t: TYPES.TABLE},
            {n: 'Roles', i: 'fa-users', _: 'ROLES', t: TYPES.TABLE},
            {n: 'Organizations', i: 'fa-building', _: 'ORGANIZATIONS', t: TYPES.TABLE},
            {n: 'Profile extensions', i: 'fa-id-card', _: 'USER_PROFILES', t: TYPES.TABLE},
            {n: 'Teams', i: 'fa-users', _: 'TEAMS', t: TYPES.TABLE},
            {n: 'Distribution lists', i: 'fa-envelope', _: 'DISTRIBUTIONS', t: TYPES.TABLE},
            {n: 'Invitations', i: 'fa-envelope', _: 'INVITATIONS', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Forms',
        i: 'fa-list-alt',
        _: 'FORMS',
        t: TYPES.TABLE,
        c: [
            {n: 'Data import', i: 'fa-upload', _: 'FORMS_IMPORT', t: TYPES.TAB},
            {n: 'Data export', i: 'fa-download', _: 'FORMS_EXPORT', t: TYPES.TAB},
            {n: 'Integrations', i: 'fa-plug', _: 'FORM_INTEGRATIONS', t: TYPES.TABLE},
            {n: 'Push API', i: 'fa-plug', _: 'FORMS_PUSH_API', t: TYPES.TABLE},
            {n: 'Submissions', i: 'fa-clipboard', _: 'SUBMISSIONS', t: TYPES.TAB}
        ]
    },
    {
        n: 'Workflows',
        i: 'fa-code-branch',
        _: 'WORKFLOWS',
        t: TYPES.TABLE,
        c: [
            {n: 'Alert', i: 'fa-bell', _: 'WORKFLOW_ALERTS', t: TYPES.TABLE},
            {n: 'Form', i: 'fa-list-alt', _: 'WORKFLOW_FORMS', t: TYPES.TABLE},
            {n: 'System', i: 'fa-cube', _: 'WORKFLOW_SYSTEM', t: TYPES.TABLE},
            {n: 'Active processes', i: 'fa-wrench', _: 'PROCESSES', t: TYPES.TABLE},
            {n: 'Task auditing', i: 'fa-tasks', _: 'TASK_AUDITOR', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Data',
        i: 'fa-database',
        _: 'DATA',
        t: TYPES.VIEW,
        c: [
            {n: 'Custom datasets', i: 'fa-database', _: 'RELATIONAL', t: TYPES.TABLE},
            {n: 'External data', i: 'fa-server', _: 'CONNECTORS', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Analysis',
        i: 'fa-chart-line',
        _: 'ANALYSIS',
        c: [
            {n: 'Notebooks', i: 'fa-book', _: 'NOTEBOOKS', t: TYPES.TABLE},
            {n: 'HUDs', i: 'fa-browser', _: 'HUDS', t: TYPES.TABLE},
            {n: 'Plots', i: 'fa-chart-pie', _: 'PLOTS', t: TYPES.TABLE},
            {n: 'Maps', i: 'fa-map', _: 'MAPS', t: TYPES.TABLE},
            {n: 'Measures', i: 'fa-tag', _: 'MEASURES', t: TYPES.TAB},
            {n: 'Dimensions', i: 'fa-cog', _: 'DIMENSIONS', t: TYPES.TAB},
            {n: 'Indicators', i: 'fa-tag', _: 'INDICATORS', t: TYPES.TAB}
        ]

    },
    {
        n: 'Content',
        i: 'fa-eraser',
        _: 'CONTENT',
        c: [
            {n: 'Feeds', i: 'fa-file-alt', _: 'FEEDS', t: TYPES.TABLE},
            {n: 'Wikis', i: 'fa-graduation-cap', _: 'WIKIS', t: TYPES.TABLE},
            {n: 'Digest emails', i: 'fa-envelope', _: 'DIGESTS', t: TYPES.TABLE},
            {n: 'Dashboards', i: 'fa-tachometer', _: 'DASHBOARDS', t: TYPES.TABLE},
            {n: 'Dashboards (legacy)', i: 'fa-tachometer', _: 'DASHBOARDS_LEGACY', t: TYPES.TABLE},
            {n: 'Documents', i: 'fa-file', _: 'DOCUMENTS', t: TYPES.TABLE},
            {n: 'Documents (legacy)', i: 'fa-file', _: 'DOCUMENTS_LEGACY', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Devices',
        i: 'fa-mobile',
        _: 'DEVICES',
        t: TYPES.VIEW,
        c: [
            {n: 'Inventory', i: 'fa-barcode', _: 'DEVICE_INVENTORY', t: TYPES.TABLE},
            {n: 'SMS gateways', i: 'fa-server', _: 'GATEWAYS_SMS', t: TYPES.TABLE},
            {n: 'WAP gateway', i: 'fa-server', _: 'GATEWAYS_WAP', t: TYPES.TABLE},
            {n: 'Relay nodes', i: 'fa-server', _: 'RELAY_NODES', t: TYPES.TABLE},
            {n: 'Event log', i: 'fa-sync', _: 'EVENT_LOG', t: TYPES.TABLE},
            {n: 'Import inventory', i: 'fa-upload', _: 'IMPORT_DEVICES', t: TYPES.TABLE}
        ]
    },
    {
        n: 'Hub',
        i: 'fa-dot-circle',
        _: 'HUB',
        t: TYPES.CUSTOM,
        c: [
            {n: 'Subscriptions', i: 'fa-arrow-to-right', _: 'SUBSCRIPTIONS', t: TYPES.TABLE},
            {n: 'Shared data', i: 'fa-exchange', _: 'SHARED_DATA', t: TYPES.TABLE},
            {n: 'Subscribers', i: 'fa-arrow-from-right', _: 'SUBSCRIBERS', t: TYPES.TABLE},
            {n: 'Advertised sets', i: 'fa-eye', _: 'ADVERTISED_SETS', t: TYPES.TABLE}
        ]
    },
    {
        n: '3rd party integrations',
        i: 'fa-cubes',
        _: 'INTEGRATIONS',
        t: TYPES.VIEW,
        c: [
            {n: 'Weather API', i: 'fa-cloud', _: 'INTEGRATION_WEATHER', t: TYPES.VIEW},
            {n: 'Maps', i: 'fa-map', _: 'INTEGRATION_MAPS', t: TYPES.VIEW},
            {n: 'Amazon S3', i: 'fa-database', _: 'INTEGRATION_S3', t: TYPES.VIEW},
            {n: 'Slack', i: 'fa-slack-hash', _: 'INTEGRATION_SLACK', t: TYPES.VIEW},
            {n: 'Skype', i: 'fa-skype', _: 'INTEGRATION_SLACK', t: TYPES.VIEW},
            {n: 'Telegram', i: 'fa-telegram', _: 'INTEGRATION_TELEGRAM', t: TYPES.VIEW},
            {n: 'Open ID', i: 'fa-openid', _: 'INTEGRATION_OPENID', t: TYPES.VIEW},
            {n: 'Dropbox', i: 'fa-dropbox', _: 'INTEGRATION_DROPBOX', t: TYPES.VIEW},
            {n: 'Line', i: 'fa-line', _: 'INTEGRATION_LINE', t: TYPES.VIEW}

        ]
    },
    {
        n: 'Custom components',
        i: 'fa-cubes',
        _: 'COMPONENTS',
        t: TYPES.TABLE
    },
    {
        n: 'Sites',
        i: 'fa-sitemap',
        _: 'SITES',
        t: TYPES.TABLE
    },
    {
        n: 'API',
        i: 'fa-leaf',
        _: 'API',
        t: TYPES.VIEW
    },
    {
        n: 'System administration',
        i: 'fa-cogs',
        _: 'ADMIN',
        t: TYPES.VIEW,
        c: [
            {n: 'Usage', i: 'fa-fire', _: 'USAGE', t: TYPES.VIEW},
            {n: 'Backup(s)', i: 'fa-database', _: 'BACKUPS', t: TYPES.VIEW},
            {n: 'GSM Modems', i: 'fa-server', _: 'GSM', t: TYPES.VIEW},
            {n: 'Performance', i: 'fa-heartbeat', _: 'PERFORMANCE', t: TYPES.VIEW},
            {n: 'System notices', i: 'fa-exclamation-triangle', _: 'NOTICES', t: TYPES.TABLE},
            {n: 'EWARS releases', i: 'fa-thumbs-up', _: 'RELEASES', t: TYPES.TABLE},
            {n: 'Translation(s)', i: 'fa-language', _: 'TRANSLATIONS', t: TYPES.TABLE},
            {n: 'PDF cache', i: 'fa-file-archive', _: 'PDF_CACHE', t: TYPES.TABLE},
            {n: 'Indexing', i: 'fa-list-alt', _: 'INDEXING', t: TYPES.VIEW},
            {n: 'Audit log(s)', i: 'fa-clock', _: 'AUDIT_LOGS', t: TYPES.TABLE},
            {n: 'Authentication log', i: 'fa-shield', _: 'AUTH_LOG', t: TYPES.TABLE},
            {n: 'Admin actions', i: 'fa-bolt', _: 'ADMIN_ACTIONS', t: TYPES.VIEW},
            {n: 'Jobs', i: 'fa-clock', _: 'JOBS', t: TYPES.VIEW}
        ]
    }
]
