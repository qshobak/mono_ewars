import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Assignment',
    fields: [
        {
            _: 0,
            n: 'fid',
            l: 'Form',
            t: FIELD_TYPES.FORM,
            r: true
        },
        {
            _: 1,
            n: 'lid',
            l: 'Location',
            t: FIELD_TYPES.LOCATION,
            r: true
        },
        {
            _: 2,
            n: 'start_date',
            l: 'Start date',
            t: FIELD_TYPES.DATE,
            r: true
        },
        {
            _: 3,
            n: 'end_date',
            l: 'End date',
            t: FIELD_TYPES.DATE
        },
        {
            _: 4,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        }
    ],
    logic: [],
    validations: []
}