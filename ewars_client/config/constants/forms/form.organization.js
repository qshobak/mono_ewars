import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Organization',
    fields: [
        {
            _: 0,
            n: 'name',
            l: 'Name',
            t: FIELD_TYPES.TEXT,
            multilang: true,
            r: true
        },
        {
            _: 1,
            n: 'acronym',
            l: 'Acronym',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 2,
            n: 'site',
            l: 'Web site',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 3,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        }
    ],
    logic: []
}