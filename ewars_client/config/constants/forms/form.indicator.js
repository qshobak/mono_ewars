import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Indicator',
    fields: [
        {
            _: 0,
            n: 'name',
            l: 'Name',
            multilang: true,
            t: FIELD_TYPES.TEXT
        },
        {
            _: 1,
            n: 'type',
            l: 'Indicator type',
            t: FIELD_TYPES.SELECT,
            o: [
                ['DEFAULT', 'Default'],
                ['STATIC', 'Static value']
            ]
        },
        {
            _: 2,
            n: 'value',
            l: 'Static value',
            t: FIELD_TYPES.NUMERIC
        },
        {
            _: 3,
            n: 'icode',
            l: 'ICODE',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 4,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: 5,
            n: 'parent_id',
            l: 'Group/Folder',
            t: FIELD_TYPES.INDICATOR_GROUP
        },
        {
            _: 6,
            n: 'description',
            l: 'Description',
            multilang: true,
            multiline: true,
            t: FIELD_TYPES.TEXT
        }
    ]
}