import FIELD_TYPES from '../const.field_types';


export default {
    name: 'Theme settings',
    fields: [
        {
            _: 0,
            n: 'app_name',
            l: 'Application name',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 1,
            n: 'header_bgd_color',
            l: 'Header bgd color',
            t: FIELD_TYPES.COLOR
        }
    ]
}