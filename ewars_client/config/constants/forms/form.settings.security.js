import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Authentication & Security',
    fields: [
        {
            _: 0,
            n: 'two_factor',
            l: 'Two-factor authentication',
            t: FIELD_TYPES.SELECT,
            o: [
                [false, 'Disabled'],
                [true, 'Enabled']
            ]
        },
        {
            _: 1,
            n: 'enforce_ssl',
            l: 'Enforce SSL',
            t: FIELD_TYPES.SELECT,
            o: [
                [false, 'No'],
                [true, 'Yes']
            ]
        },
        {
            _: 2,
            n: 'ldap',
            l: 'LDAP',
            t: FIELD_TYPES.SWITCH
        }
    ],
    validation: [],
    logic: []
}