import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Location type',
    fields: [
        {
            _: 0,
            n: 'name',
            t: FIELD_TYPES.TEXT,
            l: 'Name',
            multilang: true,
            r: true
        },
        {
            _: 1,
            n: 'status',
            t: FIELD_TYPES.SELECT,
            l: 'Status',
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: 2,
            n: 'description',
            l: 'Description',
            t: FIELD_TYPES.TEXT,
            multiline: true
        }
    ],
    logic: [],
    validations: []
}