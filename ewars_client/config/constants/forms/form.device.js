import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Device',
    fields: [
        {
            _: 0,
            t: FIELD_TYPES.SECTION,
            l: 'Device details'
        },
        {
            _: 0.1,
            n: 'os',
            t: FIELD_TYPES.SELECT,
            l: 'Operating system',
            o: [
                ['ANDROID', 'Android'],
                ['IOS', 'iOS'],
                ['WINDOWS', 'Windows'],
                ['OSX', 'OSX'],
                ['LINUX', 'Linux']
            ]
        },
        {
            _: 0.2,
            n: 'did',
            t: FIELD_TYPES.TEXT,
            l: 'Device id'
        },
        {
            _: 0.3,
            n: 'os_version',
            t: FIELD_TYPES.TEXT,
            l: 'OS version'
        },
        {
            _: 0.4,
            n: 'status',
            t: FIELD_TYPES.SELECT,
            l: 'Status',
            o: [
                ['ACTIVE', 'Active'],
                ['BLOCKED', 'BLocked']
            ]
        },
        {
            _: 0.5,
            n: 'gateway_id',
            t: FIELD_TYPES.GATEWAY,
            l: 'SMS gateway'
        },
        {
            _: 0.6,
            n: 'phone_number',
            t: FIELD_TYPES.TEXT,
            l: 'Device phone #'
        },
        {
            _: 0.7,
            n: 'device_name',
            t: FIELD_TYPES.TEXT,
            l: 'Device name'
        },
        // User detauls
        {
            _: 1,
            t: FIELD_TYPES.SECTION,
            l: 'User details'
        },
        {
            _: '1.1',
            n: 'uid',
            t: FIELD_TYPES.USER,
            l: 'Assigned user'
        },
        {
            _: '1.2',
            n: 'user_email',
            t: FIELD_TYPES.DISPLAY,
            l: 'Assigned user email'
        },
        // Usage details
        {
            _: 2,
            l: 'Usage details',
            t: FIELD_TYPES.SECTION
        },
        {
            _: '2.1',
            n: 'last_seen',
            t: FIELD_TYPES.DISPLAY,
            fmt: 'DATE',
            l: 'Last seen'
        },
        {
            _: '2.2',
            n: 'app_version',
            t: FIELD_TYPES.DISPLAY,
            l: 'Application version'
        },
        {
            _: '2.3',
            n: 'app_client_version',
            t: FIELD_TYPES.DISPLAY,
            l: 'Client version'
        }
    ],
    logic: [],
    validation: []
}