import FIELD_TYPES from '../const.field_types';

export default {
    title: 'Alarm',
    fields: [
        {
            _: 0,
            t: FIELD_TYPES.SECTION,
            l: 'General'
        },
        {
            _: '0.1',
            n: 'name',
            t: FIELD_TYPES.TEXT,
            l: 'Name',
            r: true
        },
        {
            _: '0.2',
            n: 'status',
            t: FIELD_TYPES.SELECT,
            l: 'Status',
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: '0.3',
            n: 'description',
            t: FIELD_TYPES.TEXT,
            multiline: true,
            l: 'Description'
        },
        {
            _: '0.4',
            n: 'eid_prefix',
            t: FIELD_TYPES.TEXT,
            l: 'EID prefix'
        },
        {
            _: '0.5',
            n: 'wid',
            l: 'Workflow',
            t: FIELD_TYPES.WORKFLOW,
            r: true
        },
        // Monitoring
        {
            _: 1,
            t: FIELD_TYPES.SECTION,
            l: 'Monitoring'
        },
        // Monitoring - Location
        {
            _: '1.1',
            t: FIELD_TYPES.SECTION,
            l: 'Target location(s)'
        },
        {
            _: '1.1.1',
            t: FIELD_TYPES.SELECT,
            n: 'restrict_location',
            l: 'Restrict location(s)',
            o: [
                [false, 'No'],
                [true, 'Yes']
            ]
        },
        {
            _: '1.1.2',
            t: FIELD_TYPES.SELECT,
            n: 'restrict_location_type',
            l: 'Restrict location type',
            o: [
                [false, 'No'],
                [true, 'Yes']
            ]
        },
        // Monitoring - Data being monitored
        {
            _: '1.2',
            t: FIELD_TYPES.SECTION,
            l: 'Monitored data'
        },
        {
            _: '1.2.1',
            t: FIELD_TYPES.SELECT,
            n: 'date_spec',
            l: 'Data source interval source',
            o: [
                ['REPORT_PERIOD', 'Report period'],
                ['RANGE', 'Range']
            ]
        },
        {
            _: '1.2.2',
            t: FIELD_TYPES.SELECT,
            n: 'data_source_interval',
            l: 'Dat source interval',
            o: [
                ['DAY', 'Day'],
                ['WEEK', 'Week'],
                ['MONTH', 'Month'],
                ['YEAR', 'Year']
            ]
        },
        {
            _: '1.2.3',
            t: FIELD_TYPES.SELECT,
            n: 'source',
            l: 'Data source',
            o: [
                ['INDICATOR', 'Indicator'],
                ['COMPLEX', 'Complex']
            ]
        },
        {
            _: '1.2.4',
            t: FIELD_TYPES.INDICATOR,
            n: 'indicator',
            l: 'Source indicator'
        },
        {
            _: '1.2.5',
            t: FIELD_TYPES.SELECT,
            n: 'aggregate_lower',
            l: 'Aggregate lower levels',
            o: [
                [false, 'No'],
                [true, 'Yes']
            ]
        },
        {
            _: '1.2.5',
            t: FIELD_TYPES.SELECT,
            n: 'aggregation',
            l: 'Aggregation',
            o: [
                ['SUM', 'Sum'],
                ['MED', 'Average/Median']
            ]
        },
        {
            _: '1.2.6',
            t: FIELD_TYPES.NUMERIC,
            l: 'Modifier',
            n: 'modifier'
        },
        // Alert criteria
        {
            _: '1.3',
            t: FIELD_TYPES.SECTION,
            l: 'Alert criteria'
        },
        {
            _: '1.3.1',
            t: FIELD_TYPES.SELECT,
            n: 'comparator',
            l: 'Comparator',
            o: [
                ['GT', '>'],
                ['GTE', '>='],
                ['LT', '<'],
                ['LTE', '<='],
                ['EQ', '='],
                ['NEQ', '!=']

            ]
        },
        {
            _: '1.3.2',
            t: FIELD_TYPES.SELECT,
            n: 'crit_data_source',
            l: 'Criteria data source',
            o: [
                ['STATIC', 'Static value'],
                ['INDICATOR', 'Indicator']
            ]
        },
        {
            _: '1.3.3',
            t: FIELD_TYPES.NUMERIC,
            n: 'static_value',
            l: 'Static value'
        },
        {
            _: '1.3.4',
            t: FIELD_TYPES.SELECT,
            n: 'crit_start_date_spec',
            l: 'Start date specification',
            o: []
        },
        {
            _: '1.3.5',
            t: FIELD_TYPES.NUMERIC,
            n: 'crit_intervals',
            l: 'Intervals'
        },
        {
            _: '1.3.6',
            t: FIELD_TYPES.SELECT,
            n: 'crit_end_date_spec',
            l: 'End date specification',
            o: []
        },
        {
            _: '1.3.7',
            t: FIELD_TYPES.NUMERIC,
            n: 'crit_end_intervals',
            l: 'Intervals'
        },
        {
            _: '1.3.8',
            t: FIELD_TYPES.INDICATOR,
            n: 'crit_indicator',
            l: 'Indicator source'
        },
        {
            _: '1.3.9',
            t: FIELD_TYPES.SELECT,
            n: 'crit_reduction',
            l: 'Reduction',
            o: [
                ['SUM', 'Sum'],
                ['MED', 'Average/Median']
            ]
        },
        {
            _: '1.3.10',
            t: FIELD_TYPES.NUMERIC,
            n: 'crit_modifier',
            l: 'Modifier'
        },
        {
            _: '1.3.11',
            t: FIELD_TYPES.NUMERIC,
            n: 'crit_floor',
            l: 'Floor'
        },
        // Discarding
        {
            _: '1.4',
            t: FIELD_TYPES.SECTION,
            l: 'Auto-Discard'
        },
        {
            _: '1.4.1',
            t: FIELD_TYPES.SWITCH,
            n: 'ad_enabled',
            l: 'Enabled',
            p: [
                [false, 'Off'],
                [true, 'On']
            ]
        },
        {
            _: '1.4.2',
            t: FIELD_TYPES.SELECT,
            n: 'ad_recovery_interval',
            l: 'Recovery interval type',
            o: [
                ['DAY', 'Day(s)'],
                ['WEEK', 'Week(s)'],
                ['MONTH', 'Month(s)'],
                ['YEAR', 'Year(s)']
            ]
        },
        {
            _: '1.4.3',
            t: FIELD_TYPES.NUMERIC,
            n: 'ad_intervals',
            l: 'Interval periods'
        },
        // Investigations
        {
            _: '1.5',
            t: FIELD_TYPES.SECTION,
            l: 'Investigations'
        },
        {
            _: '1.5.1',
            t: FIELD_TYPES.SWITCH,
            n: 'inv_enabled',
            l: 'Enabled'
        },
        {
            _: '1.5.2',
            t: FIELD_TYPES.FORMS,
            l: 'Forms',
            n: 'inv_forms'
        }
    ],
    logic: {},
    required: [],
    validations: {}
}