import FIELD_TYPES from '../const.field_types';


export default {
    name: 'Custom user',
    fields: [
        {
            _: 0,
            n: 'email',
            l: 'Email',
            t: FIELD_TYPES.TEXT,
            suffix: '@ewars.ws',
            r: true
        },
        {
            _: 1,
            n: 'email_confirm',
            l: 'Confirm email',
            t: FIELD_TYPES.TEXT,
            suffix: '@ewars.ws',
            r: true
        },
        {
            _: 2,
            n: 'password',
            l: 'Password',
            t: FIELD_TYPES.TEXT,
            mask: 'password',
            r: true
        },
        {
            _: 3,
            n: 'password_confirm',
            l: 'Confirm password',
            t: FIELD_TYPES.TEXT,
            mask: 'password',
            r: true
        },
        {
            _: 4,
            n: 'role',
            l: 'Role',
            t: FIELD_TYPES.ROLE,
            r: true
        },
        {
            _: 5,
            n: 'location_id',
            l: 'Location',
            t: FIELD_TYPES.LOCATION
        },
        {
            _: 6,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: 7,
            n: 'org_id',
            l: 'Organization',
            t: FIELD_TYPES.ORGANIZATION,
            r: true
        },
        {
            _: 8,
            n: 'phone',
            l: 'Phone',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 9,
            n: 'occupation',
            l: 'Occupation',
            t: FIELD_TYPES.TEXT
        }
    ],
    logic: [],
    validation: []
}