import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Data set',
    fields: [
        {
            _: 0,
            n: 'name',
            l: 'Name',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 1,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: 2,
            n: 'type',
            l: 'Type',
            t: FIELD_TYPES.SELECT,
            o: [
                ['RELATIONAL', 'Relational'],
                ['KEY', 'Key/Value store']
            ]
        },
        {
            _: 3,
            n: 'description',
            l: 'Description',
            t: FIELD_TYPES.TEXT,
            multiline: true
        }
    ],
    logic: [],
    validation: []
}