import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Alarm settings',
    fields: [
        {
            _: 0,
            n: 'default_bounds',
            l: 'Default bounds',
            t: FIELD_TYPES.GEOM_EDITOR
        },
        {
            _: 1,
            n: 'sti',
            l: 'Default location type',
            t: FIELD_TYPES.LOCATION_TYPE
        },
        {
            _: 2,
            n: 'ad_notify',
            l: 'Notify on auto-discard',
            t: FIELD_TYPES.SWITCH
        }
    ],
    logic: [],
    validation: []
}