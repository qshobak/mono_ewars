import {state} from '../cmp_state';

import ControlTreeMenu from '../controls/control.treemenu';

import ViewTable from './view.table';

import ViewExportAlarms from './view.export.alarms';
import ViewTheme from './view.theme';
import ViewAccount from './view.account';
import ViewNotifications from './view.notifications';
import ViewSecurity from './view.security';
import ViewUsage from './view.usage';

const VIEWS = {
    EXPORT_ALERTS: ViewExportAlarms,
    ACCOUNT: ViewAccount,
    THEME: ViewTheme,
    NOTIFICATIONS: ViewNotifications,
    SECURITY: ViewSecurity,
    USAGE: ViewUsage
};

const TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};



class ViewRoot extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let view;

        if (state.view.t == TYPES.VIEW) {
            if (VIEWS[state.view._]) {
                let Cmp = VIEWS[state.view._];
                view = <Cmp/>;
            }
        } else if (state.view.t == TYPES.TABLE) {
            // this is a table view, need to render it
            view = <ViewTable data={state.view}/>;
        }

        return (
            <ewars.d.Row>
                <ewars.d.Cell width="250px" borderRight={true}>
                    <ControlTreeMenu
                        onSelect={this._selectView}/>

                </ewars.d.Cell>
                <ewars.d.Cell>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            {view}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}


export default ViewRoot;
