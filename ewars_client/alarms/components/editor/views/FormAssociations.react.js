import { Button } from "../../../../common";

var FormSelection = React.createClass({
    render: function () {
        return (
            <div className="row">
                <div className="cell">Form Name</div>
                <div className="cell">Type</div>
                <div className="cell cell block">
                    <div className="btn-group">
                        <Button
                            icon="fa-trash"
                            onClick={this._remove}/>
                    </div>
                </div>
            </div>
        )
    }
});

var FormSelectView = React.createClass({
    getInitialState: function() {
        return {};
    },

    render: function () {
        return (
            <div className="ide-inline-grid">
                <div className="headers">
                    <div className="header">Form Name</div>
                    <div className="header">Type</div>
                    <div className="header">&nbsp;</div>
                </div>
                <div className="rows">

                </div>
            </div>
        )
    }
});

var FormAssociations = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Associated Forms</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Select forms which can be asociated with an alert raised by this alarm</p>
                </div>
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">Associated Forms</div>
                        <div className="btn-group">
                            <Button
                                icon="fa-link"
                                label="Link Form"
                                onClick={this._addForm}/>
                        </div>
                        <FormSelectView
                            onChange={this._formsChange}/>
                    </div>

                </div>
            </div>
        )
    }
});

export default FormAssociations;
