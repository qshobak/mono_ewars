import {
    SelectField,
    TextField
} from "../../../../common/fields";
import { Button } from "../../../../common";

var defaults = {
    uuid: null,
    name: null,
    type: "SUBMIT_HOOK",
    endpoint: null,
    format: "json",
    status: "INACTIVE"
};

var FORMAT_CONFIG = {
    options: [
        ["JSON", "JSON"],
        ["XML", "XML"],
        ["YAML", "YAML"],
        ["PROTOBUF", "ProtoBuf"],
        ["BINARY", "Binary"]
    ]
};

var Integration = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.data.name}</td>
                <td>{this.props.data.status}</td>
                <td>{this.props.data.format}</td>
                <td>{this.props.data.type}</td>
            </tr>
        )
    }
});

var IntegrationsList = React.createClass({
    render: function () {

        var ints = _.map(this.props.integrations, function (item) {
            return <Integration data={item}/>
        }, this);

        return (
            <div className="ide-inline-grid">
                <table width="100%">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Format</th>
                        <th>Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    {ints}
                    </tbody>
                </table>
            </div>
        )
    }
});

var EditIntegration = React.createClass({
    render: function () {
        return (
            <div className="ide-section-basic">
                <div className="header">Integration Settings</div>

                <div className="hsplit-box">
                    <div className="ide-setting-label">Name</div>
                    <div className="ide-setting-control">
                        <TextField
                            name="name"
                            onUpdate={this._onChange}
                            value={this.props.data.name}/>
                    </div>
                </div>

                <div className="hsplit-box">
                    <div className="ide-setting-label">Integration Type</div>
                    <div className="ide-setting-control">
                        <SelectField
                            name="type"
                            onUpdate={this._onChange}
                            value={this.props.data.type}
                            config={{options: [["POST_HOOK", "Submission Hook"]]}}/>
                    </div>
                </div>

                <div className="hsplit-box">
                    <div className="ide-setting-label">Endpoint</div>
                    <div className="ide-setting-control">
                        <TextField
                            name="endpoint"
                            onUpdate={this._onChange}
                            value={this.props.data.endpoint}/>
                    </div>
                </div>

                <div className="hsplit-box">
                    <div className="ide-setting-label">Format</div>
                    <div className="ide-setting-control">
                        <SelectField
                            name="format"
                            onUpdate={this._onChange}
                            value={this.props.data.format}
                            config={FORMAT_CONFIG}/>
                    </div>
                </div>
            </div>
        )
    }
});

var Integrations = React.createClass({
    getInitialState: function () {
        return {
            view: "LIST",
            integrations: [],
            editing: {}
        }
    },

    _addIntegration: function () {
        this.state.view = "EDIT";
        this.state.editing = _.extend({}, defaults);
        this.forceUpdate();
    },

    render: function () {

        var view;

        if (this.state.view == "LIST") view = <IntegrationsList integrations={this.state.integrations}/>;
        if (this.state.view == "EDIT") view = <EditIntegration data={this.state.editing}/>;

        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Integrations</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Configure integrations with other systems for this form.</p>
                </div>
                <div className="ide-tbar">
                    <Button
                        label="Add Integration"
                        onClick={this._addIntegration}/>
                </div>
                <div className="ide-settings-form">

                    {view}

                </div>
            </div>
        )
    }
});

export default Integrations;
