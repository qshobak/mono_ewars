import {
    SwitchField,
    NumericField as NumberField,
    SelectField
} from "../../../../common/fields";
import { ButtonGroup } from "../../../../common";

var INTERVAL_SELECT = {
    options: [
        ["DAY", "Day(s)"],
        ["WEEK", "Week(s)"],
        ["MONTH", "Month(s)"],
        ["YEAR", "Year(s)"]
    ]
};

var Recovery = React.createClass({
    _update: function (prop, value) {
        this.props.onChange(prop, value)
    },

    render: function () {
        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-bell-slash"></i></div>
                    <div className="ide-settings-title">Auto-Discard</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Define how and when alerts generated by this alarm should be auto-discarded.</p>
                </div>
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">Enabled</div>
                        <div className="description">Should automated recovery be enabled for alerts generated by this alarm</div>


                        <div className="hsplit-box">
                            <div className="ide-setting-label">Enabled</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    value={this.props.data.ad_enabled}
                                    name="ad_enabled"
                                    onUpdate={this._update}/>
                            </div>
                        </div>

                    </div>

                    {this.props.data.ad_enabled ?
                        <div className="ide-section-basic">
                            <div className="header">Recovery Settings</div>
                            <div className="description">Define when the alert should be auto-closed</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Recovery Interval Type</div>
                                <div className="ide-setting-control">
                                    <ButtonGroup
                                        name="ad_interval"
                                        value={this.props.data.ad_interval}
                                        onUpdate={this._update}
                                        config={INTERVAL_SELECT}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Interval Periods</div>
                                <div className="ide-setting-control">
                                    <NumberField
                                        name="ad_period"
                                        value={this.props.data.ad_period}
                                        onUpdate={this._update}/>
                                </div>
                            </div>
                        </div>
                        : null}

                </div>
            </div>
        )
    }
});

export default Recovery;
