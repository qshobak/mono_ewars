import ViewAlarmSettings from './view.alarm.settings';

class ViewAlarm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT"
        }
    }

    render() {
        return (
            <ewars.d.Cell style={{display: "flex"}}>
                <ewars.d.Row height="34px" borderBottom={true} style={{background: "#000000"}}>
                    <div className="ux-tabs">
                        <div
                            className={"ux-tab" + (this.state.view == "DEFAULT" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "DEFAULT"})
                            }}>
                            <i className="fal fa-cog"></i>&nbsp;Settings
                        </div>
                        <div
                            className={"ux-tab" + (this.state.view == "WORKFLOW" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "WORKFLOW"});
                            }}>
                            <i className="fal fa-code-branch"></i>&nbsp;Workflow
                        </div>
                        <div
                            className={"ux-tab" + (this.state.view == "NOTIFICATIONS" ? " active" : "")}
                            onClick={() => {
                                this.setState({
                                    view: "NOTIFICATIONS"
                                })
                            }}>
                            <i className="fal fa-email"></i>&nbsp;Notifications
                        </div>
                        <div
                            className={"ux-tab" + (this.state.view == "PERMISSIONS" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "PERMISSIONS"})
                            }}>
                            <i className="fal fa-lock"></i>&nbsp;Permissions
                        </div>
                        <div
                            className={"ux-tab" + (this.state.view == "EXPORT" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "EXPORT"})
                            }}>
                            <i className="fal fa-download"></i>&nbsp;Export &amp; Administration
                        </div>
                        <div
                            style={{color: "red"}}
                            className={"ux-tab" + (this.state.view == "DELETE" ? " active" : "")}
                            onClick={() => {
                                this.setState({view: "DELETE"})
                            }}>
                            <i className="fal fa-trash"></i>&nbsp;Delete
                        </div>
                    </div>
                </ewars.d.Row>
                <ewars.d.Row style={{display: "block", overflowY: "auto"}}>
                    <ViewAlarmSettings/>

                </ewars.d.Row>
            </ewars.d.Cell>
        )
    }
}

export default ViewAlarm;