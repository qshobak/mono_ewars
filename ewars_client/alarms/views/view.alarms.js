import ViewAlarm from './view.alarm';


const VIEW_ACTIONS = [
    ['fa-plus', 'CREATE', 'Create new alarm']
]

class AlarmItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="son-list-item">
                {this.props.data.name.en || this.props.data.name}
            </div>
        )
    }
}


class ViewAlarms extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alarm: null,
            alarms: []
        }

        this._loadAlarms();
    }

    _loadAlarms = () => {
        ewars.tx("com.sonoma.alarms", [])
            .then(res => {
                this.setState({
                    alarms: res
                })
            })
            .catch(err => {
                ewars.error("There was an error loading the alarms")
            })
    }

    render() {
        let view;

        view = <ViewAlarm/>;

        let alarms = this.state.alarms.sort((a, b) => {
            return (a.name.en || a.name).localeCompare((b.name.en || b.name));
        })

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="250px" borderRight={true} style={{overflowY: "auto"}}>
                        {alarms.map(item => {
                            return <AlarmItem data={item} key={item.uuid}/>
                        })}

                    </ewars.d.Cell>
                    {view}
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewAlarms;