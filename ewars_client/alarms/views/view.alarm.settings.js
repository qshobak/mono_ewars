const styles = {
    CHART: {
        display: "block",
        height: "400px",
        margin: "16px",
        border: "1px solid #F2F2F2"
    },
    SETTINGS: {
        display: "block"
    },
    HEADER: {
        display: "block",
        fontSize: "16px",
        textTransform: "uppercase",
        color: "#F2F2F2",
        fontWeight: "bold",
        padding: "0 16px 8px 16px",
        marginBottom: "8px"
    },
    CONDITION: {
        display: "block",
        margin: "0 16px 8px 16px"
    },
    COND_PREFIX: {
        background: "#3e3e3e",
        fontWeight: "bold",
        padding: "8px",
        textAlign: "center",
        float: "left"
    },
    COND_DATA: {
        background: "#757575",
        minWidth: "100px",
        height: "28px",
        float: "left",
        display: "block"
    },
    COND_INPUT: {
        width: "100px",
        background: "#F2F2F2",
        float: "left"
    },
    COND_BUTTON: {
        width: "30px",
        maxWidth: "30px",
        cursor: "pointer",
        float: "left",
        padding: "8px",
        borderRadius: "3px",
        background: "#F2F2F2",
        marginLeft: "8px"
    },
    SERIES_ROW: {
        display: "flex",
        flexDirection: "column",
        margin: "0 16px 8px 16px",
        height: "82px"
    }
};

if (Highcharts) {
    Highcharts.setOptions({
        global: {
            useUTC: true
        }
    });
}

class Condition extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={styles.CONDITION}>
                <div style={styles.COND_PREFIX}>WHEN</div>
                <div style={{...styles.COND_DATA}}>
                    <select name="" id="">
                        <option value="MIN">Minimum value</option>
                        <option value="MAX">Maximum value</option>
                        <option value="SUM">Sum</option>
                        <option value="COUNT">Number of records</option>
                        <option value="LAST">Last recorded value</option>
                        <option value="MEDIAN">Median</option>
                        <option value="PERC_DIFF">Percentage difference</option>
                        <option value="COUNT_NOT_NULL">Count of not-null</option>

                    </select>
                </div>
                <div style={{...styles.COND_PREFIX, width: "35px", maxWidth: "35px"}}>OF</div>
                <div style={styles.COND_DATA}></div>
                <div style={styles.COND_PREFIX}>IS ABOVE</div>
                <div style={styles.COND_INPUT}>
                    <input type="text"/>
                </div>
                <div style={styles.COND_BUTTON}>
                    <i className="fal fa-trash"></i>
                </div>

            </div>
        )
    }
}

class Conditions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div style={styles.HEADER}>Conditions</div>

                <Condition/>
            </div>
        )
    }
}

const DATA_ACTIONS = [
    ['fa-list', 'TOGGLE', 'Menu'],
    ['fa-trash', 'TRASH', 'Delete source']
]

class MetricSelect extends React.Component {
    static defaultProps = {
        variables: []
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            spec: ewars.g.spec || []
        }
    }

    componentWillMount() {
        if (!ewars.g.spec) {
            ewars.tx("com.sonoma.apec.specification", [])
                .then(res => {
                    ewars.g.spec = res.sort((a, b) => {
                        return (a.name.en || a.name).localeCompare(b.name.en || b.name);
                    });
                    this.setState({
                        spec: ewars.g.indicators
                    })
                })
                .catch(err => {
                    ewars.error("Could not load indicators");
                })
        }
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    render() {
        console.log(this.state.spec);
        return (
            <ewars.d.Cell width="100px" style={{background: "#848484", cursor: "pointer", textAlign: "center"}}>
                <div className="handle" onClick={this._toggle}>Metric</div>
                {this.state.show ?
                    <div className="metric-select">
                        <div className="metric-select-inner">
                            <div className="metric">*</div>
                            {this.props.variables.map(item => {
                                return (
                                    <div className="metric">${item}</div>
                                )
                            })}
                            {this.state.indicators.map(item => {
                                return (
                                    <div className="metric">{item.name.en || item.name}</div>
                                )
                            })}
                        </div>
                    </div>
                    : null}
            </ewars.d.Cell>
        )
    }
}

class Metric extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div style={styles.HEADER}>Data</div>

                <div style={styles.SERIES_ROW}>
                    <ewars.d.Row height="40px" style={{background: "#232323"}}>
                        <ewars.d.Cell width="60px" style={{
                            textAlign: "center",
                            display: "block",
                            padding: "8px",
                            borderRight: "3px solid #333333"
                        }}>Series
                        </ewars.d.Cell>
                        <MetricSelect
                            onSelect={this._select}/>

                        <ewars.d.Cell style={{borderRight: "3px solid #333333"}}>

                        </ewars.d.Cell>

                        <ewars.d.Cell width="65px" style={{flex: 1, display: "block", paddingRight: "8px"}}>
                            <ewars.d.ActionGroup
                                height="40px"
                                actions={DATA_ACTIONS}
                                onAction={this._action}
                                right={true}/>
                        </ewars.d.Cell>

                    </ewars.d.Row>
                    <ewars.d.Row height="40px" style={{borderTop: "3px solid #333333", background: "#232323"}}>
                        <ewars.d.Cell width="30px" style={{background: "#333333"}}>&nbsp;</ewars.d.Cell>


                        <ewars.d.Cell width="30px" style={{
                            display: "block",
                            padding: "8px",
                            borderRight: "3px solid #333333",
                            textAlign: "center"
                        }} width="30px">ƒx
                        </ewars.d.Cell>
                        <ewars.d.Cell>

                        </ewars.d.Cell>
                        <ewars.d.Cell borderLeft={true} width="30px">
                            <ewars.d.Button icon="fa-plus"/>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: "block", background: "#333333"}}
                                      width="200px">&nbsp;</ewars.d.Cell>
                    </ewars.d.Row>
                </div>

                <div className="clearer"></div>
                <div style={{margin: "8px 16px"}}>
                    <ewars.d.Button
                        icon="fa-plus"/>
                </div>


            </div>
        )
    }
}

class AlarmChart {
    constructor(el, props) {
        this.el = el;
        this.props = props;

        this._chartOptions = {
            chart: {
                renderTo: this.el
            },
            title: {
                text: null
            }
        }
        this._chart = new Highcharts.Chart(this._chartOptions);
    }

}

class ViewAlarmSettings extends React.Component {
    constructor(props) {
        super(props);

        this._chart = null;

        this.state = {
            data: ewars.copy(this.props.data || {})
        }
    }

    componentDidMount() {
        if (this._el) {
            this._chart = new AlarmChart(this._el, this.state.data);
        }
    }

    componentWillUnmount() {
        if (this._chart) this._chart.destroy();
        this._chart = null;
    }

    render() {
        return (
            <div>
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    style={styles.CHART}>

                </div>

                <div style={styles.SETTINGS}>

                    <div style={{display: "block", margin: "8px 16px"}}>
                        <label>Alarm Name</label>
                        <input type="text"/>

                        <label>Alarm status</label>
                        <select name="" id="">
                            <option value="">No Status Selected</option>
                            <option value="ACTIVE">Active</option>
                            <option value="INACTIVE">Inactive</option>
                            <option value="ARCHIVED">Archived</option>
                            <option value="SILENT">Silent</option>
                        </select>

                        <label htmlFor="">Tags</label>
                        <input type="text"/>
                    </div>

                    <Metric/>
                    <div className="clearer" style={{height: "16px"}}></div>
                    <Conditions/>
                    <div className="clearer"></div>
                    <div style={{margin: "8px 16px"}}>
                        <ewars.d.Button
                            icon="fa-plus"/>
                    </div>

                </div>

            </div>
        )
    }
}

export default ViewAlarmSettings;