export default {
    name: "New alarm",
    context: "GENERAL",
    status: false,
    description: "",

    // Resultant Actions
    actions: {
        generate_alert: true,
        generate_email_admin: true,
        generate_sms_admin: false,
        generate_email_submitter: true,
        generate_sms_submitter: false
    },

    sti_agg_rollup: false,

    // Time Component
    date_spec: "TRIGGER_PERIOD",
    date_spec_interval_type: "WEEK",
    date_spec_intervals: null,
    date_spec_aggregation: "SUM",

    // Location Component
    location_spec: "LOCATION_TYPE",
    location_spec_parent: null,
    location_spec_type: null,
    location_spec_uuid: null,

    // Calculation Component
    data_source_type: "INDICATOR",
    indicator_id: null,
    indicator_definition: null,
    data_aggregation: "SUM",
    data_modifier: null,
    data_formula: null,
    data_sources: [],

    // Threshold Component
    comparator: "GTE",
    comparator_source: "VALUE",
    comparator_value: null,
    comparator_modifier: null,
    comparator_definition: {
        start_date_specification: null,
        start_date: null,
        start_date_intervals: null,
        end_date_specification: "EVALUATION_DATE",
        end_date: null,
        end_date_intervals: null,
        indicator_id: null,
        indicator_definition: null,
        modifier: null,
        reduction: "SUM",
        floor: null
    }
};
