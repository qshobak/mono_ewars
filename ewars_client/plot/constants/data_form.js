const data_form = [
    {
        name: "title",
        label: "Title",
        type: "text"
    },
    {
        name: "series_style",
        label: "Series Style",
        type: "select",
        options: [
            ["LINE", "Line"],
            ["CANDLESTICK", "Candlestick"],
            ["AREA", "Area"],
            ["SCATTER", "Scatter"],
            ["COLUMN", "Column"]
        ]
    },
    {
        name: "color",
        label: "Series Color",
        type: "text"
    },
    {

    }
];

export default data_form;
