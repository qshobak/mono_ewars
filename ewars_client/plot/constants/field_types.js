import {
    DateField,
    SelectField,
    TextField,
    SwitchField
} from "../../common/fields";

const FieldTypes = {
    "date": DateField,
    "select": SelectField,
    "text": TextField,
    "switch": SwitchField
};

export default FieldTypes;
