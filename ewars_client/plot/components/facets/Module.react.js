import { Button } from "../../../common";

class Module extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    render() {
        let iconClass = "fa";

        if (this.state.show) iconClass += " fa-caret-down";
        if (!this.state.show) iconClass += " fa-caret-right";

        let style = {flexBasis: 0, flexGrow: 1};
        if (!this.state.show) style.maxHeight = 35;
        return (
            <div className="ide-row" style={style}>
                <div className="ide-col">
                    <div className="ide-layout">
                        <div className="ide-row" style={{maxHeight: 35}}>
                            <div className="ide-col">
                                <div className="ide-tbar">
                                    <div className="ide-tbar-text">{this.props.label}</div>
                                    <div className="btn-group pull-right">
                                        <Button
                                            icon={iconClass}
                                            onClick={this._toggle}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.show ?
                            <div className="ide-row">
                                <div className="ide-col">
                                    {this.props.children}
                                </div>
                            </div>
                            : null}
                    </div>

                </div>
            </div>
        )
    }
}

export default Module;
