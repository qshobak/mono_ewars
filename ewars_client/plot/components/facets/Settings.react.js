import { Button, Tab, Form } from "../../../common";

import Data from "./Data.react";
import Action from "../../utils/actions";
import Store from "../../utils/Store";
import form from "../../constants/widget_form";

class Settings extends React.Component {
    constructor(props) {
        super(props);
    }

    _onFieldChange = (data, prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll exp-settings" style={{padding: 8}}>
                <ewars.d.Form
                    definition={form}
                    readOnly={false}
                    updateAction={this._onFieldChange}
                    data={this.props.data}/>
            </div>
        )
    }
}

export default Settings;
