import {
    Button,
    Form,
    Modal
} from "../../../common";

import {
    IndicatorField as IndicatorSelector
} from "../../../common/fields";

import IndicatorTreeComponent from "../../../indicators/components/IndicatorTreeComponent.react";
import LocationTreeView from "../../../locations/components/LocationTreeView.react";

import Module from "./Module.react";

import DT_FORM from "../../constants/dt_form";
import DataEditor from "../../../common/editors/widgets/series/DataEditor.react";

import Store from "../../utils/Store";
import Actions from "../../utils/actions";

class DataItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _show = () => {
        this.setState({
            show: !this.state.show
        })
    };

    _onIndChange = (prop, value, path, data) => {
        this.props.onChange(this.props.index, prop, value);
    };

    _remove = () => {
        this.props.onRemove(this.props.index);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content" style={{padding: 0}}>
                    <div className="ide-row">
                        <div className="ide-col border-right" style={{padding: 8}}>
                            <IndicatorSelector
                                name="indicator"
                                onUpdate={this._onIndChange}
                                value={this.props.data.indicator}/>
                        </div>
                        <div className="ide-col" style={{maxWidth: 22, padding: 8}} onClick={this._remove}>
                            <i className="fal fa-trash"></i>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Data extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: null,
            show: false
        }

    }

    _addSeries = () => {
        let series = ewars.copy(this.props.data.series || []);
        series.push({});
        this.props.onChange(series);
    };

    _onItemChange = (index, prop, value) => {
        let series = ewars.copy(this.props.data.series || []);
        series[index].indicator = value;
        this.props.onChange(series);
    };

    _onRemove = (index) => {
        let series = ewars.copy(this.props.data.series || []);
        series.splice(index, 1);
        this.props.onChange(series);
    };

    render() {
        let series = this.props.data.series || [];
        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="btn-group">
                                <Button
                                    icon="fa-plus"
                                    onClick={this._addSeries}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll" style={{padding: 8}}>
                            <div className="block-tree">
                                {series.map(function (series, index) {
                                    return <DataItem
                                        onChange={this._onItemChange}
                                        onRemove={this._onRemove}
                                        data={series}
                                        index={index}/>
                                }.bind(this))}
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    visible={this.state.show}
                    title="Data Source"
                    icon="fa-data">

                </Modal>
            </div>

        )
    }
}

export default Data;
