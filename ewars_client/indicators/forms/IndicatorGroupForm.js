export default {
    name: {
        type: "language_string",
        label: "Group Name",
        required: true
    },
    parent_id: {
        type: "select",
        label: "Parent",
        optionsSource: {
            resource: "IndicatorGroup",
            query: {},
            valSource: "id",
            labelSource: "name",
            additional: [
                ["null", "No Parent"]
            ],
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    }
};
