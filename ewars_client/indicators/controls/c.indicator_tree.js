class IndicatorTree extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            search: ''
        };

        this.handleChange = this._onSearchChange.bind(this);
        this.emitChangeDebounced = _.debounce(this.emitChange, 3);
    }

    _onSearchChange = (e) => {
        this.emitChangeDebounced(e.target.value);
    };

    emitChange = (value) => {
        this.setState({
            search: value
        })
    };

    render() {
        return (
            <div className="vbox">
                    <div className="search">
                        <div className="hbox">
                            <div className="box search-left">
                                <i className="fal fa-search"></i>
                            </div>
                            <div className="box">
                                <input
                                    placeholder="Search indicators..."
                                    value={this.state.search}
                                    onChange={(e) => {
                                        this._action('SEARCH_CHANGE', e.target.value);
                                    }}
                                    type="text"/>
                            </div>
                            {this.state.search != '' ?
                                <div className="box">
                                    <div
                                        onClick={() => {
                                            this._action('CLEAR_SEARCH');
                                        }}
                                        className="search-right">
                                        <i className="fal fa-times"></i>
                                    </div>
                                </div>
                            : null}
                        </div>
                    </div>
                <div className="box">

                </div>
            </div>
        )
    }
}

export default IndicatorTree;