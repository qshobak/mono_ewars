import IndicatorTreeView from "./IndicatorTreeComponent.react";

import {
    Filler,
    DropDownMenu
} from "../../common";

import IndicatorEditor from "./editors/IndicatorEditor.react";
import GroupEditor from "./editors/GroupEditor.react";

const CONSTANTS = {
    EDIT_INDICATOR: "EDIT_INDICATOR",
    EDIT_GROUP: "EDIT_GROUP"
};

const IND_ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

var Component = React.createClass({
    getInitialState: function () {
        return {
            dirty: false,
            editing: null,
            editType: "INDICATOR",
            view: null,
            errors: []
        };
    },

    _onNewGroup: function () {
        this.setState({
            editing: {
                name: {en: "New Indicator"}
            },
            editType: "FOLDER",
            view: "EDIT"
        })
    },

    _onEdit: function (type, indData) {
        if (['DEFAULT', 'INDICATOR', 'STATIC', 'FOLDER', null, undefined, "AGGREGATE", "COMPLEX"].indexOf(indData.itype) < 0) {
            ewars.growl("This indicator or grouping is not editable");
            return;
        }

        if (type == "FOLDER" || !type) {
            ewars.tx("com.ewars.resource", ["indicator_group", indData.id, null, null])
                .then(function (resp) {
                    this.state.view = CONSTANTS.EDIT_GROUP;
                    this.state.editType = "FOLDER";
                    this.state.editing = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }

        if (type == "INDICATOR") {
            ewars.tx("com.ewars.resource", ['indicator', indData.uuid, null, null])
                .then(function (resp) {
                    this.state.view = CONSTANTS.EDIT_INDICATOR;
                    this.state.editType = "INDICATOR";
                    this.state.editing = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    _editIndicator: function (indicator) {
        this.props.onEdit(indicator);
    },

    _onNewIndicator: function () {
        this.setState({
            view: CONSTANTS.EDIT_INDICATOR,
            editType: "INDICATOR",
            editing: {}
        })
    },

    _onNewGroup: function () {
        this.setState({
            view: CONSTANTS.EDIT_GROUP,
            editType: "FOLDER",
            editing: {}
        })
    },

    _closeEdit: function () {
        this.setState({
            view: null,
            editing: null,
            editType: null
        })
    },

    _onAction: function (action, data) {
        this._onEdit("INDICATOR", data);
    },

    _onIndAction: function (action, data) {
        if (action == "EDIT") {
            if (data.id) {
                this._onEdit("FOLDER", data);
            } else {
                this._onEdit("INDICATOR", data);
            }
        }

        if (["INDICATOR_SELECT", "CLICK"].indexOf(action) >= 0) {
            if (data.uuid) {
                this._onEdit("INDICATOR", data);
            } else {
                this._onEdit("FOLDER", data);
            }
        }

        if (action == "DELETE") {
            ewars.prompt("fa-trash", "Delete Item?", _l("DELETE_IND_GROUP"), function () {
                let bl = new ewars.Blocker(null, "Deleting item...");
                let com = "com.ewars.indicator.delete";
                if (data.id) com = "com.ewars.indicator_group.delete";

                ewars.tx(com, [data.id || data.uuid])
                    .then(resp => {
                        bl.destroy();
                        ewars.emit("RELOAD_INDICATORS");
                        ewars.growl("Item deleted");
                    })
            }.bind(this))
        }
    },

    _onSet: function (data) {
        this.setState({
            editing: data
        })
    },

    onPropChange: function (prop, value) {
        this.setState({
            editing: {
                ...this.state.editing,
                [prop]: value
            }
        })
    },

    render: function () {

        var subView = <Filler icon="fa-code-branch"/>, formDefinition;

        if (this.state.view == CONSTANTS.EDIT_INDICATOR) {
            subView = <IndicatorEditor
                onChange={this.onPropChange}
                onSet={this._onSet}
                data={this.state.editing}/>
        }

        if (this.state.view == CONSTANTS.EDIT_GROUP) {
            subView = <GroupEditor
                onChange={this.onPropChange}
                onSet={this._onSet}
                data={this.state.editing}/>
        }

        var create_menu = [
            {label: "New Indicator", icon: "fa-plus", handler: this._onNewIndicator},
            {label: "New Group", icon: "fa-plus", handler: this._onNewGroup}
        ];

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Indicators" icon="fa-code-branch">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="New Indicator"
                            onClick={this._onNewIndicator}
                            icon="fa-plus"/>
                        <ewars.d.Button
                            label="New Group"
                            onClick={this._onNewGroup}
                            icon="fa-plus"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row className="ide-row">
                    <ewars.d.Cell width={300} borderRight={true}>
                        <IndicatorTreeView
                            hideInactive={false}
                            actions={IND_ACTIONS}
                            onAction={this._onIndAction}
                            fillHeight={true}
                            onEdit={this._onEdit}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {subView}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

export default Component;
