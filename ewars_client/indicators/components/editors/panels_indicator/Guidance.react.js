var Guidance = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onUpdate: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                        <div className="ide-settings-title">General Settings</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>Basic information about the indicator</p>
                    </div>
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="placeholder">Guidance editor coming soon</div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
});

export default Guidance;
