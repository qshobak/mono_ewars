import { Button } from "../../../common";
import { LanguageStringField } from "../../../common/fields";
import GroupSelector from "../facets/GroupSelector";

var defaults = {
    id: null,
    name: {
        en: null
    },
    parent_id: null
};

var fields = {
    parent_id: {
        optionsSource: {
            resource: "indicator_group",
            query: {},
            select: ["id", "name", "parent_id"],
            valSource: "id",
            labelSource: "name",
            additional: [
                ["null", "No Parent"]
            ],
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    }
};

var GroupEditor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onChange: function (prop, value) {
        this.props.onChange(prop, value);
    },

    _onSave: function () {
        let bl = new ewars.Blocker(null, "Saving changes...");
        if (this.props.data.id) {
            ewars.tx("com.ewars.indicator_group.update", [this.props.data.id, this.props.data])
                .then(function (resp) {
                    bl.destroy();
                    this.props.onSet(resp);
                    ewars.growl("Indicator group updated");
                    ewars.emit("RELOAD_INDICATORS");
                }.bind(this))
        } else {
            ewars.tx("com.ewars.indicator_group.create", [this.props.data])
                .then(function (resp) {
                    bl.destroy();
                    this.props.onSet(resp);
                    ewars.growl("Indicator group created");
                    ewars.emit("RELOAD_INDICATORS");
                }.bind(this))
        }
    },

    render: function () {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Indicator Group">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            label="Save"
                            color="green"
                            onClick={this._onSave}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="ide-settings-panel">
                                <div className="ide-settings-content">
                                    <div className="ide-settings-header">
                                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                                        <div className="ide-settings-title">Indicator Group</div>
                                    </div>
                                    <div className="ide-settings-intro">
                                        <p>Basic information about the indicator</p>
                                    </div>
                                    <div className="ide-settings-form">

                                        <div className="ide-section-basic">
                                            <div className="header">General</div>

                                            <div className="hsplit-box">
                                                <div className="ide-setting-label">Name</div>
                                                <div className="ide-setting-control">
                                                    <LanguageStringField
                                                        name="name"
                                                        value={this.props.data.name}
                                                        onUpdate={this._onChange}/>
                                                </div>
                                            </div>

                                            <div className="hsplit-box">
                                                <div className="ide-setting-label">Parent</div>
                                                <div className="ide-setting-control">
                                                    <GroupSelector
                                                        name="parent_id"
                                                        value={this.props.data.parent_id}
                                                        onUpdate={this._onChange}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

export default GroupEditor;
