import GeneralSettings from "./panels_indicator/GeneralSettings.react";
import DefinitionSettings from "./panels_indicator/Definition.react";
import Guidance from "./panels_indicator/Guidance.react";

import {
    VertTab
} from "../../../common";

const CONSTANTS = {
    GENERAL: "GENERAL",
    DEFINITION: "DEFINITION",
    GUIDANCE: "GUIDANCE"
}

var defaults = {
    uuid: null,
    group_id: null,
    name: {
        en: ''
    },
    description: {
        en: ''
    },
    status: "INACTIVE",
    protected: false,
    tracks_location: false,
    tracks_date: false,
    value_type: 'NUMERIC',
    history: {},
    created: null,
    created_by: window.user.id,
    last_modified: null,
    ranked: false,
    colour: null,
    itype: "DEFAULT",
    definition: null,
    accounts: []

};

var SettingsSection = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-settings-section";
        if (this.props.active) className += " ide-settings-section-active";
        var iconClass = "fa fa-caret-right";
        if (this.props.active) iconClass = "fa fa-caret-down";

        var subs = [];
        if (this.props.subs) {
            subs = _.map(this.props.subs, function (sub) {
                return (
                    <div className="section-item">{sub}</div>
                )
            }, this);
        }

        var hasSubs = subs.length > 0;

        return (
            <div className={className}>
                <div className="section-header" onClick={this._onClick}>
                    <div className="section-title">{this.props.label}</div>
                </div>
                {hasSubs ?
                    <div className="section-items">
                        {subs}
                    </div>
                    : null}
            </div>
        )
    }
});

var IndicatorEditor = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            view: CONSTANTS.GENERAL
        }
    },


    componentWillUnmount() {
        this.state = {
            data: {},
            view: CONSTANTS.GENERAL
        }
    },

    componentWillMount: function () {
        this.state.data = {
            ...JSON.parse(JSON.stringify(defaults)),
            ...this.props.data
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = {
            ...JSON.parse(JSON.stringify(defaults)),
            ...nextProps.data
        }
    },

    _changeSection: function (section) {
        this.state.view = section;
        this.forceUpdate();
    },

    _onChange: function (prop, value) {
        this.state.data[prop] = value;
        this.forceUpdate();
    },

    _save: function () {
        let bl = new ewars.Blocker(null, "Saving changes...");
        if (this.state.data.uuid) {
            ewars.tx("com.ewars.indicator.update", [this.state.data.uuid, this.state.data])
                .then(function (resp) {
                    bl.destroy();
                    ewars.emit("RELOAD_INDICATORS");
                    ewars.growl("Indicator updated")
                }.bind(this))
        } else {
            ewars.tx("com.ewars.indicator.create", [this.state.data])
                .then(function (resp) {
                    bl.destroy();
                    ewars.emit("RELOAD_INDICATORS");
                    ewars.growl("Indicator created");
                    this.setState({
                        data: resp
                    })
                }.bind(this))
        }
    },

    _onChange: function (prop, value) {
        let data = ewars.copy(this.state.data);
        if (prop == "value") {
            if (!data.definition) data.definition = {};
            data.definition[prop] = value;
        } else {
            data[prop] = value;
        }
        this.setState({
            data: {...data}
        })
    },

    render: function () {

        var view;

        if (this.state.view == CONSTANTS.GENERAL) view =
            <GeneralSettings data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.DEFINITION) view =
            <DefinitionSettings data={this.state.data} onChange={this._onChange}/>;
        if (this.state.view == CONSTANTS.GUIDANCE) view = <Guidance data={this.state.data} onChange={this._onChange}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Indicator">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            label="Save"
                            ref="saveBtn"
                            onClick={this._save}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="34px" style={{position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs">
                            <VertTab label="General Settings"
                                icon="fa-cog"
                                active={this.state.view == CONSTANTS.GENERAL}
                                onClick={() => this._changeSection("GENERAL")}/>
                            <VertTab label="Definition"
                                icon="fa-book"
                                active={this.state.view == CONSTANTS.DEFINITION}
                                onClick={() => this._changeSection("DEFINITION")}/>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>

        )
    }
});

export default IndicatorEditor;
