class Handle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let label = "No Selection";
        if (this.props.value) label = ewars.I18N(this.props.value.name);
        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{label}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

class Item extends React.Component {
    _isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            show: false
        }
    }

    _toggle = () => {
        if (!this._isLoaded) {
            ewars.tx("com.ewars.query", ["indicator_group", null, {parent_id: {eq: this.props.data.id}}, {"name.en": "ASC"}, null, null, null, null])
                .then((resp) => {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        show: true
                    })
                })
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    _onSelect = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let iconClass = "fal fa-folder";
        if (this.state.show) iconClass = "fal fa-folder-open";

        return (
            <div className="block">
                <div className="block-content" style={{padding: 0}}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={22}
                            style={{maxWidth: 22, padding: 8}}
                            onClick={this._toggle}>
                            <i className={iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: 8}}
                                      onClick={this._onSelect}>{ewars.I18N(this.props.data.name)}</ewars.d.Cell>
                    </ewars.d.Row>

                </div>
                {this.state.show ?
                    <div className="block-children">
                        {this.state.data.map(item => {
                            return <Item
                                data={item}
                                onSelect={this.props.onSelect}/>
                        })}
                    </div>
                    : null}
            </div>

        )
    }
}


class GroupSelector extends React.Component {
    isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            show: false
        }
    }

    componentWillMount() {
        window.__hack__.addEventListener("click", this._onBodyClick);
        this._query(this.props);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._onBodyClick);
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.value) {
            if (nextProps.value != this.state.value.id) {
                this._query(nextProps);
            }
        }
    }

    _query = (props) => {
        if (props.value) {
            ewars.tx("com.ewars.resource", ["indicator_group", props.value, null, null])
                .then(resp => {
                    this.setState({
                        value: resp
                    })
                })
        }
    };

    _reload = () => {
        ewars.tx("com.ewars.query", ["indicator_group", null, {parent_id: {eq: "NULL"}}, {"name.en": "ASC"}, null, null, null])
            .then(resp => {
                this._isLoaded = true;
                this.setState({
                    data: resp,
                    show: true
                })
            })
    };

    _onBodyClick = (evt) => {
        if (this.state.show) {
            if (!this._itemsEl.contains(evt.target)) {
                this.setState({
                    show: false
                })
            }
        }
    };

    _toggle = () => {
        if (!this._isLoaded && !this.state.show == true) {
            this._reload();
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    _onSelect = (item) => {
        this.setState({show: false, value: item});
        this.props.onUpdate(this.props.name, item.id);
    };

    render() {
        let items = this.state.data.map((item) => {
            return (
                <Item key={"GROUP_" + item.id} data={item} onSelect={this._onSelect}/>
            )
        });


        return (
            <div ref="selector" className="ew-select" onClick={this._onBodyClick}>
                <Handle
                    onClick={this._toggle}
                    value={this.state.value}/>
                {this.state.show ?
                    <div className="ew-select-data" ref={(el) => {
                        this._itemsEl = el;
                    }}>
                        <div className="block-tree" style={{position: "relative", height: 250, padding: 0}}>
                            {items}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

export default GroupSelector;