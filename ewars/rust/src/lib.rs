extern crate uuid;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;

#[repr(C)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub status: String,
    pub code: String,
    pub created_by: Uuid,
    pub modified_by: Uuid
}

impl Location {
    pub fn new() -> Location {
        Location {
            uuid: Uuid::new_v4(),
            name: HashMap::new(),
            status: "ACTIVE".to_string(),
            code: "ACTIVE".to_string(),
            created_by: Uuid::new_v4(),
            modified_by: Uuid::new_v4()
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn location_new() -> Location {
    Location::new()
}



#[repr(C)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

#[repr(C)]
pub enum Foo {
    A = 1,
    B,
    C
}

#[no_mangle]
pub unsafe extern "C" fn example_get_origin() -> Point {
    Point {x: 0.0, y: 0.0 }
}

#[no_mangle]
pub unsafe extern "C" fn example_print_foo(foo: *const Foo) {
    println!("{}", match *foo {
        Foo::A => "a",
        Foo::B => "b",
        Foo::C => "c",
    });
}
