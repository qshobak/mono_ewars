from .system import alerts as alert_indicators
from .system import assignments as assignment_indicators
from .system import devices as device_indicators
from .system import reporting as reporting_indicators
from .system import tasks as task_indicators
from .system import organizations as org_indicators
from .system import locations as location_indicators


INDICATORS = {}

# Alert/Alarm indicators
INDICATORS['ALERTS_RAISED'] = alert_indicators.get_alerts_raised
INDICATORS['ALERTS_CLOSED'] = alert_indicators.get_alerts_closed
INDICATORS['ALERTS_OPEN'] = alert_indicators.get_alerts_open
INDICATORS['ALERTS_VERIFICATION'] = alert_indicators.get_alerts_verification
INDICATORS['ALERTS_RISK_ASSESS'] = alert_indicators.get_alerts_risk_assess
INDICATORS['ALERTS_RISK_CHAR'] = alert_indicators.get_alerts_risk_char
INDICATORS['ALERTS_OUTCOME'] = alert_indicators.get_alerts_outcome
INDICATORS['ALERTS_DISCARDED'] = alert_indicators.get_alerts_discarded
INDICATORS['ALERTS_DISCARDED_VERIFICATION'] = alert_indicators.get_discarded_verification
INDICATORS['ALERTS_DISCARDED_OUTCOME'] = alert_indicators.get_discarded_outcome
INDICATORS['ALERTS_VERIFIED'] = alert_indicators.get_alerts_verified
INDICATORS['ALERTS_LOW'] = alert_indicators.get_low_alerts
INDICATORS['ALERTS_MOD'] = alert_indicators.get_moderate_alerts
INDICATORS['ALERTS_HIGH'] = alert_indicators.get_high_alerts
INDICATORS['ALERTS_SEVERE'] = alert_indicators.get_severe_alerts
INDICATORS['ALERTS_MONITOR'] = alert_indicators.get_monitored_alerts
INDICATORS['ALERTS_MONITOR_VERIFICATION'] = alert_indicators.get_verification_monitored_alerts
INDICATORS['ALERTS_MONITOR_OUTCOME'] = alert_indicators.get_outcome_monitored_alerts
INDICATORS['ALERTS_RESPONSE'] = alert_indicators.get_response_alerts

# Form/Reporting Indicators
INDICATORS['REPORT_SUBMISSIONS'] = reporting_indicators.get_report_submissions
INDICATORS['REPORT_TIMELINESS'] = reporting_indicators.get_report_timeliness
INDICATORS['REPORT_COMPLETENESS'] = reporting_indicators.get_report_completeness
INDICATORS['REPORTS_EXPECTED'] = reporting_indicators.get_reports_expected
INDICATORS['REPORTS_LATE'] = reporting_indicators.get_reports_late
INDICATORS['REPORTS_ON_TIME'] = reporting_indicators.get_reports_timely
INDICATORS['REPORTS_MOBILE'] = reporting_indicators.get_report_source_mobile
INDICATORS['REPORTS_WEB'] = reporting_indicators.get_report_source_web
INDICATORS['REPORTS_OTHER'] = reporting_indicators.get_report_source_other
INDICATORS['REPORTS_IMPORT'] = reporting_indicators.get_report_source_import
INDICATORS['REPORT_RETRACTIONS'] = reporting_indicators.get_report_retractions
INDICATORS['REPORT_AMENDMENTS'] = reporting_indicators.get_report_amendments
INDICATORS['REPORTS_BY_ADMIN'] = reporting_indicators.get_reports_submitted_by_admin
INDICATORS['REPORTS_BY_USER'] = reporting_indicators.get_reports_submitted_by_user

# Devices
INDICATORS['DEVICES_ANDROID'] = device_indicators.get_devices_android
INDICATORS['DEVICES'] = device_indicators.get_devices
INDICATORS['DEVICE_UN_SYNCED'] = device_indicators.get_unsynced_devices
INDICATORS['DEVICES_IOS'] = device_indicators.get_devices_ios
INDICATORS['DEVICES_DESKTOP'] = device_indicators.get_devices_desktop
# INDICATORS['SMS_GATEWAYS'] = device_indicators.get_sms_gateways
# INDICATORS['SMS_SENT'] = device_indicators.get_sms_sent
# INDICATORS['SMS_RECEIVED'] = device_indicators.get_sms_received

# Assignments
INDICATORS['ASSIGNS_TOTAL'] = assignment_indicators.get_assignments_total
INDICATORS['ASSIGNS_EXPIRED'] = assignment_indicators.get_expired

# Tasks
INDICATORS['TASKS_TOTAL'] = None
INDICATORS['TASKS_COMPLETED'] = None
INDICATORS['TASKS_INCOMPLETE'] = None
INDICATORS['TASK_COMPLETION_TIME_AVG'] = None

# Users
INDICATORS['USERS_TOTAL'] = None
INDICATORS['REGISTRATIONS'] = None
INDICATORS['REGISTRATIONS_APPROVED'] = None
INDICATORS['REGISTRATIONS_REJECTED'] = None
INDICATORS['REG_APPROVE_AVG_TIME'] = None

# Organizations
INDICATORS['ORGS'] = None
INDICATORS['ORG_USERS'] = None
INDICATORS['ORG_REPORTS'] = None
INDICATORS['ORG_REPORTS_RETRACTED'] = None
INDICATORS['ORG_REPORTS_AMENDED'] = None
INDICATORS['ORG_COMPLETENESS'] = None
INDICATORS['ORG_TIMELINESS'] = None
INDICATORS['ORG_REPORTS_EXPECTED'] = None
INDICATORS['ORG_REPORTS_SUBMITTED'] = None

# Investigations
INDICATORS['INV_TOTAL'] = None
INDICATORS['INV_OPEN'] = None
INDICATORS['INV_CLOSED'] = None
INDICATORS['INV_AVG_TIME'] = None

# Locations
INDICATORS['REPORTING_LOCATIONS'] = None

# Response
INDICATORS['AVG_RESPONSE_LENGTH'] = None


def handle_system_indicator(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    """ Handle system indicators
    :param indicator: {dict} The system indicator
    :param target_interval: {str} Target interval
    :param start_date: {date} Start date for query
    :param end_date: {date} End date for query
    :param target_location: {dict} Target location
    :param options: {dict} Additional options for query
    :param user: {dict} Calling user
    :return:
    """
    results = []
    source_reports = None

    method = INDICATORS.get(indicator.get("itype"), None)

    if method is not None:
        results = method(indicator, target_interval, start_date, end_date, target_location, options, user=user)

    return results, source_reports
