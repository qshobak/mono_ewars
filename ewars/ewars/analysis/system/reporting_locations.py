import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from ewars.utils import date_utils

from ewars.models import Location

from psycopg2.extensions import AsIs


async def query_reporting_locations(location_id, start_date, end_date, form_id=None, reduction=None, user=None):
    """ Query for reporting locations under a specific locatino

    Args:
        location_id: The location to query for reporting locations
        start_date: The start date of the series
        end_date:  The end date of the series
        form_id: optional form_id to process against
        reduction: A reduction function to use for reducing the series (optional)
        user: THe calling user

    Returns:
        Time Series || reduced float
    """
    forms = None
    location = None

    if form_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT id, features
                        FROM %s.forms
                        WHERE id = %s
                    """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            res = await cur.fetchone()
            forms = [res]
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                WHERE status = 'ACTIVE'
            """, (
                AsIs(user.get('tki')),
                user.get("aid"),
            ))
            forms = await cur.fetchall()

    period_sets = []

    for form in forms:
        if "LOCATION_REPORTING" in form.get('features', {}).keys():
            site_type_id = form['features']['LOCATION_REPORTING'].get("site_type_id", None)
            rps = await Location.get_rps_locale(
                    location_id,
                    site_type_id,
                    form.get("id"),
                    user=user
            )

            period_sets = period_sets + rps

    reporting_periods = None

    timeline = date_utils.get_date_range(
            start_date,
            end_date,
            "DAY"
    )

    pre_dict = dict((x, []) for x in timeline)

    for item in period_sets:
        for ts, loc_uuids in pre_dict.items():
            if item.get('start_date') <= ts <= item.get("end_date"):
                loc_uuids.append(item.get("location_id"))

    exit_dict = dict((x, len(list(set(y)))) for x, y in pre_dict.items())

    results = []
    if reduction is not None:
        results = max([y for x, y in exit_dict.items()])
    else:
        results = [[x, y] for x, y in exit_dict.items()]

    return results
