from ewars.db import get_db_cursor
from ewars.constants import CONSTANTS

from ewars.core.analysis.aggregate import aggregate

from psycopg2.extensions import AsIs


def get_assignments_total(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    assignments = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.created
            FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.form_id = %s
                AND a.created >= %s
                AND a.created <= %s
                AND a.status = 'ACTIVE'
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        assignments = cur.fetchall()

    series = [[x.get("created"), 1] for x in assignments]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_expired(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    return []
