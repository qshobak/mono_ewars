from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

async def count_submitted(target_date, location_uuids, form_id, user=None):
    """ Get the total number of reports submitted for a set of locations

    Args:
        target_date: The target date
        location_uuids: The locations
        form_id: The form to qiery against

    Returns:
        An aggregated amount of reports submitted
    """
    results = []

    if len(location_uuids) <= 0:
        return 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT data_date
            FROM %s.collections
            WHERE location_id::TEXT = ANY(%s)
            AND (status = 'SUBMITTED' OR status = 'PENDING_AMENDMENT')
            AND form_id = %s
            AND data_date = %s
        """, (
            AsIs(user.get("tki")),
            location_uuids,
            form_id,
            target_date,
        ))
        results = await cur.fetchall()

    return sum([1 for x in results])
