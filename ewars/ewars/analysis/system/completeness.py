import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from ewars.models import Location
from ewars.utils import date_utils

from .submitted import count_submitted
from ewars.analysis.aggregate import aggregate
from psycopg2.extensions import AsIs

async def get_completeness(location_id, start_date, end_date, form_id=None, reduction=None, user=None):
    """ Calculate completeness for a given location

    Args:
        location_id:
        start_date:
        end_date:
        form_id:
        reduction:
        user:

    Returns:

    """
    results = None
    forms = None
    interval = "DAY"

    if form_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT id, features
                        FROM %s.forms
                        WHERE id = %s
                    """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            res = await cur.fetchone()
            forms = [res]
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                WHERE status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

    timeline = date_utils.get_date_range(
            start_date,
            end_date,
            "DAY"
    )

    # Root timeline
    pre_dict = dict((x, dict(e=0, s=0)) for x in timeline)

    loc_sets = []

    for form in forms:
        site_type_id = None
        sub_interval = None

        # Get the interval for the reporting
        if 'INTERVAL_REPORTING' in form.get('features').keys():
            sub_interval = form['features']["INTERVAL_REPORTING"].get("interval", None)
            if sub_interval is not None:
                interval = sub_interval


        # Get the location information for the reporting
        if "LOCATION_REPORTING" in form.get('features').keys():
            site_type_id = form['features']['LOCATION_REPORTING'].get("site_type_id", None)

        # Check if there's an interval, if not, this isn't a report that has interval-based reporting
        if sub_interval is not None:
            sub_time = date_utils.get_date_range(
                    start_date,
                    end_date,
                    sub_interval
            )
            # date: (loc_uuids, expected, submitted)
            sub_dict = dict((x, [[], 0, 0]) for x in sub_time)

            rps = await Location.get_rps_locale(
                location_id,
                site_type_id,
                form.get('id'),
                user=user
            )

            for rp in rps:
                for ts, dt in sub_dict.items():
                    if rp.get('start_date') <= ts <= rp.get("end_date"):
                        dt[0].append(rp.get("location_id"))


            for x, dt in sub_dict.items():
                if x in pre_dict.keys():
                    pre_dict[x]['s'] += await count_submitted(x, dt[0], form.get("id"), user=user)
                    pre_dict[x]['e'] += len(list(set(dt[0])))

            loc_sets.append(sub_dict)

    result = None
    if reduction is not None:
        expected = sum([x.get("e") for x in pre_dict.values()])
        submitted = sum([x.get("s") for x in pre_dict.values()])
        result = 0
        try:
            result = submitted / expected
        except ZeroDivisionError:
            result = 0
    else:
        result = []
        def agg_fn(target, addition):
            if target is None:
                target = dict(
                        e=0,
                        s=0
                )

            target['e'] += addition['e']
            target['s'] += addition['s']

            return target

        raw_data = [[x, y] for x,y in pre_dict.items()]
        agged = aggregate(interval, start_date, end_date, raw_data, agg_fn=agg_fn)

        for item in agged:
            value = 0

            if item[1].get("e") <= 0:
                value = None
            else:
                try:
                    value = item[1].get("s") / item[1].get("e")
                except ZeroDivisionError:
                    value = 0

            result.append((item[0], value))

    return result


async def get_rps_locale_raw(location_id, start_date, end_date, form_id=None, user=None):
    """ Calculate completeness for a given location

    Args:
        location_id:
        start_date:
        end_date:
        form_id:
        user:

    Returns:

    """
    results = None
    forms = None
    interval = "DAY"

    if form_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT id, features
                        FROM %s.forms
                        WHERE id = %s
                    """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            res = await cur.fetchone()
            forms = [res]
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                AND status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

    timeline = date_utils.get_date_range(
            start_date,
            end_date,
            "DAY"
    )

    # Root timeline
    pre_dict = dict((x, dict(e=0, s=0)) for x in timeline)

    loc_sets = []

    for form in forms:
        site_type_id = None
        sub_interval = None

        # Get the interval for the reporting
        if 'INTERVAL_REPORTING' in form.get('features').keys():
            sub_interval = form['features']["INTERVAL_REPORTING"].get("interval", None)
            if sub_interval is not None:
                interval = sub_interval


        # Get the location information for the reporting
        if "LOCATION_REPORTING" in form.get('features').keys():
            site_type_id = form['features']['LOCATION_REPORTING'].get("site_type_id", None)

        # Check if there's an interval, if not, this isn't a report that has interval-based reporting
        if sub_interval is not None:
            sub_time = date_utils.get_date_range(
                    start_date,
                    end_date,
                    sub_interval
            )
            # date: (loc_uuids, expected, submitted)
            sub_dict = dict((x, [[], 0, 0]) for x in sub_time)

            rps = await Location.get_rps_locale(
                    location_id,
                    site_type_id,
                    form.get('id'),
                    user=user
            )

            for rp in rps:
                for ts, dt in sub_dict.items():
                    if rp.get('start_date') <= ts <= rp.get("end_date"):
                        dt[0].append(rp.get("location_id"))


            for x, dt in sub_dict.items():
                if x in pre_dict.keys():
                    pre_dict[x]['s'] += await count_submitted(x, dt[0], form.get("id"), user=user)
                    pre_dict[x]['e'] += len(list(set(dt[0])))

            loc_sets.append(sub_dict)

    result = []

    def agg_fn(target, addition):
        if target is None:
            target = dict(
                    e=0,
                    s=0
            )

        target['e'] += addition['e']
        target['s'] += addition['s']

        return target

    raw_data = [[x, y] for x,y in pre_dict.items()]
    agged = aggregate(interval, start_date, end_date, raw_data, agg_fn=agg_fn)

    return agged















