from ewars.db import get_db_cursor
from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs

from ewars.core.analysis.aggregate import aggregate


def get_alerts_raised(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    """ Get data series for alerts raised
    :param args:
    :param kwargs:
    :param user:
    :return:
    """
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return results

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_closed(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.state = 'CLOSED'
                AND a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_open(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l on l.uuid = a.location_id
            WHERE a.state = 'OPEN'
                AND a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND trigger_end >= %s
                AND trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_verification(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l on l.uuid = a.location_id
            WHERE a.stage = 'VERIFICATION'
                AND a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND trigger_end >= %s
                AND trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_risk_assess(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l on l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.stage = 'RISK_ASSESS'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_risk_char(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.stage = 'RISK_CHAR'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_outcome(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.stage = 'OUTCOME'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_alerts_discarded(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    """
    :param indicator:
    :param target_interval:
    :param start_date:
    :param end_date:
    :param target_location:
    :param options:
    :param user:
    :return:
    """
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'DISCARDED'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_discarded_verification(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []
    alarm_id = indicator.get('alarm_id', None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'DISCARDED'
                AND a.stage = 'VERIFICATION'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get('trigger_end'), 1] for x in alerts]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_discarded_outcome(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'DISCARDED'
                AND a.stage = 'OUTCOME'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    result = aggregate(target_interval, start_date, end_date, series)

    return result


def get_alerts_verified(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND ((a.stage != 'VERIFICATION') OR (a.stage = 'VERIFICATION' AND a.stage_state IN %s))
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            ('DISCARD', 'MONITOR',),
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get('trigger_end'), 1] for x in alerts]
    result = aggregate(target_interval, start_date, end_date, series)

    return result


def get_low_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.risk = 'LOW'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get('trigger_end'), 1] for x in alerts]
    result = aggregate(target_interval, start_date, end_date, series)

    return result


def get_moderate_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.risk = 'MODERATE'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in alerts]
    result = aggregate(target_interval, start_date, end_date, series)

    return result


def get_high_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.risk = 'HIGH'
                AND a.trigger_end <= %s
                AND a.trigger_end >= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    result = aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])
    return result


def get_severe_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.risk = 'SEVERE'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
       """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])


def get_monitored_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    result = []

    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'MONITOR'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])


def get_verification_monitored_alerts(indicator, target_interval, start_date, end_date, target_location, options,
                                      user=None):
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'MONITOR'
                AND a.stage = 'VERIFICATION'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])


def get_outcome_monitored_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
            AND l.lineage::TEXT[] @> %s::TEXT[]
            AND a.outcome = 'MONITOR'
            AND a.stage = 'OUTCOME'
            AND a.trigger_end >= %s
            AND a.trigger_end <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get('uuid')],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])


def get_response_alerts(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    alarm_id = indicator.get("alarm_id", None)

    if alarm_id is None:
        return []

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.outcome = 'RESPONSE'
                AND a.trigger_end <= %s
                AND a.trigger_end >= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        alerts = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("trigger_end"), 1] for x in alerts])
