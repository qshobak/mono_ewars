from ewars.db import get_db_cursor
from ewars.constants import CONSTANTS
from ewars.core.analysis.aggregate import aggregate

from psycopg2.extensions import AsIs

def get_devices_android(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    results = []

    devices = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT d.last_seen
            FROM %s.devices AS d
            WHERE d.last_seen >= %s
                AND d.last_seen <= %s
                AND d.device_type = 'ANDROID'
        """, (
            AsIs(user.get("tki")),
            start_date,
            end_date,
        ))
        devices = cur.fetchall()

    series = [[x.get("last_seen"), 1] for x in devices]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


def get_devices(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    devices = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT d.last_seen
            FROM %s.devices AS d
            WHERE d.last_seen >= %s
                AND d.last_seen <= %s
        """, (
            AsIs(user.get("tki")),
            start_date,
            end_date,
        ))
        devices = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("last_seen"), 1] for x in devices])


def get_unsynced_devices(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    return []


def get_devices_ios(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    devices = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT d.last_seen
            FROM %s.devices AS d
            WHERE d.last_seen > = %s
                AND d.last_seen <= %s
                AND d.device_type = 'IOS'
        """, (
            AsIs(user.get("tki")),
            start_date,
            end_date,
        ))
        devices = cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("last_seen"), 1] for x in devices])


def get_devices_desktop(indicator, target_interval, start_date, end_date, target_location, options, user=None):

    devices = []