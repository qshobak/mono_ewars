import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from ewars.models import Location
from ewars.utils import date_utils

from .submitted import count_submitted
from ewars.analysis.aggregate import aggregate

from psycopg2.extensions import AsIs


async def get_timeliness(location_id, start_date, end_date, form_id=None, reduction=None, user=None):
    """ Calculate completeness for a given location

    Args:
        location_id:
        start_date:
        end_date:
        form_id:
        reduction:
        user:

    Returns:

    """
    results = None
    forms = None
    interval = "DAY"

    if form_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT id, features
                        FROM %s.forms
                        WHERE id = %s
                    """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            res = await cur.fetchone()
            forms = [res]
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                WHERE status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

    timeline = date_utils.get_date_range(
        start_date,
        end_date,
        "DAY"
    )

    # Root timeline
    pre_dict = dict((x, [0, 0, []]) for x in timeline)

    loc_sets = []

    for form in forms:
        site_type_id = None
        sub_interval = None
        overdue = None
        overdue_interval = None
        has_overdue = False

        # Get the interval for the reporting
        if 'INTERVAL_REPORTING' in form.get('features').keys():
            sub_interval = form['features']["INTERVAL_REPORTING"].get("interval", None)
            if sub_interval is not None:
                interval = sub_interval

        # Get the location information for the reporting
        if "LOCATION_REPORTING" in form.get('features').keys():
            site_type_id = form['features']['LOCATION_REPORTING'].get("site_type_id", None)

        if "OVERDUE" in form.get('features').keys():
            has_overdue = True
            overdue = form['features']['OVERDUE']['threshold']
            overdue_interval = form['features']['OVERDUE']['interval']

        if has_overdue and sub_interval is not None:
            overdue_thresh_days = 0
            if overdue_interval == "WEEK":
                overdue_thresh_days = 7 * int(overdue)
            elif overdue_interval == "DAY":
                overdue_thresh_days = int(overdue)
            elif overdue_interval == "MONTH":
                overdue_thresh_days = 30 * int(overdue)

            data = []

            # Get reporting periods under the parent location
            reporting_periods = await Location.get_rps_locale(
                location_id,
                site_type_id,
                form.get('id'),
                user=user
            )

            # Clean down to the interval type
            timeline = date_utils.get_date_range(
                start_date,
                end_date,
                sub_interval
            )

            for ts in timeline:
                for rp in reporting_periods:
                    if rp.get('start_date') <= ts <= rp.get("end_date", datetime.date.today()):
                        if pre_dict.get(ts, None) is not None:
                            pre_dict[ts][1] += 1
                            pre_dict[ts][2].append(rp.get("location_id"))
                        else:
                            pre_dict[ts] = [0, 1, [rp.get("location_id")]]

            async with get_db_cur() as cur:
                for ts, dt in pre_dict.items():
                    if ts in timeline:
                        if len(dt[2]) > 0:
                            on_time = 0
                            await cur.execute("""
                                SELECT COUNT(*) AS total
                                FROM %s.timeliness
                                WHERE form_id = %s 
                                  AND location_id IN %s 
                                  AND data_date = %s
                                  AND delay <= %s;
                            """, (
                                AsIs(user.get("tki")),
                                form.get("id"),
                                tuple(dt[2]),
                                ts,
                                overdue_thresh_days,
                            ))
                            res = await cur.fetchone()
                            on_time = res.get('total')

                            dt[0] = dt[0] + on_time

                        # Reset location ids
                        dt[2] = []

    result = None
    if reduction is not None:
        on_time = sum([x[0] for x in pre_dict.values()])
        expected = sum([x[1] for x in pre_dict.values()])
        result = 1
        try:
            result = on_time / expected
        except ZeroDivisionError:
            result = 1
    else:
        result = []

        def agg_fn(target, addition):
            if target is None:
                target = [0, 0]

            target[0] += addition[0]
            target[1] += addition[1]

            return target

        raw_data = [[x, y] for x, y in pre_dict.items()]
        agged = aggregate(interval, start_date, end_date, raw_data, agg_fn=agg_fn)

        for item in agged:
            value = 1

            try:
                value = item[1][0] / item[1][1]
            except ZeroDivisionError:
                value = 1

            result.append((item[0], value))

    return result
