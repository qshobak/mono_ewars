import collections
import sys
import datetime
import json
import re
import uuid
import time
import functools
from functools import reduce
from statistics import median, mean
from psycopg2.extensions import AsIs
from ewars.db import get_db_cur, get_db_cursor
from ewars.utils import date_utils
from ewars.models import Location, Form
from ewars.core.async_lru import async_lru

from ewars.utils.six import string_types

from .aggregate import aggregate

from ewars.constants import CONSTANTS

from ewars.analysis import system_parser

matcher = re.compile(r'("field_name": "([a-zA-Z0-9:\._]+)")')

DEBUGGED = False
try:
    from ewars.sock.sockv2 import app

    DEBUGGED = True
except Exception:
    pass

ROOT_KEYS = [
    "location_id",
    "reporting_interval",
    "EVENT:SUBMITTED",
    "data_date"
]

CMP = dict(
    EQ='=',
    NEQ='!=',
    GT='>',
    LT='<',
    GTE='>=',
    LTE='<='
)


def ResultIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        for result in results:
            yield result


def debugg(data):
    if DEBUGGED:
        app.logger.debug("DATA {}:".format(data))


class memoized(object):
    '''Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated).
    '''

    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        if not isinstance(args, collections.Hashable):
            # uncacheable. a list, for instance.
            # better to not cache than blow up.
            return self.func(*args)
        if args in self.cache:
            return self.cache[args]
        else:
            value = self.func(*args)
            self.cache[args] = value
            return value

    def __repr__(self):
        '''Return the function's docstring.'''
        return self.func.__doc__

    def __get__(self, obj, objtype):
        '''Support instance methods.'''
        return functools.partial(self.__call__, obj)


def get_nested_default(d, path):
    return reduce(lambda d, k: d.setdefault(k, {}), path, d)


def set_nested(d, path, value):
    get_nested_default(d, path[:-1])[path[-1]] = value


@functools.lru_cache(maxsize=1024, typed=True)
def _add(x, y):
    return x + y


@functools.lru_cache(maxsize=1024, typed=True)
def _subtract(x, y):
    return x - y


@functools.lru_cache(maxsize=1024, typed=True)
def _age_calc(value, modifier):
    return float(value) * modifier


def _cmp_arr(source, target, cmp):
    target_fx = list(map(str, target))
    if isinstance(source, string_types):
        # just check if its in the target
        if cmp == "EQ":
            if source in target_fx:
                return 1
            else:
                return 0
        elif cmp == "NEQ":
            if source not in target_fx:
                return 1
            else:
                return 0
    if isinstance(source, (list,)):
        has_all = True

        if cmp == "EQ":
            for item in source:
                if str(item) not in target_fx:
                    has_all = False

            if has_all:
                return 1
            else:
                return 0
        if cmp == "NEQ":
            for item in source:
                if str(item) in target_fx:
                    has_all = False

            if has_all:
                return 0
            else:
                return 1


def _as_cmp(cmp, val, value):
    if cmp == "EQ":
        return str(val) == str(value)
    elif cmp == "NEQ":
        return str(val) != str(value)
    elif cmp == "GTE":
        return float(value) >= float(val)
    elif cmp == "GT":
        return float(value) > float(val)
    elif cmp == "LT":
        return float(value) < float(val)
    elif cmp == "LTE":
        return float(value) <= float(val)


@functools.lru_cache(maxsize=1024)
def ok_parser(val):
    if isOk(val):
        return float(val)
    return 0.0


@functools.lru_cache(maxsize=1024)
def _eval_circ(*args, circuit):
    result = False

    _, cmp, val = circuit.cplit(':')

    if val in ('', None, 'NULL', 'null'):
        return False

    if len(args) > 1:
        val = list(args)
        return _cmp_arr(val, list(args), cmp) == 1

    if args[0] is not None:
        return _as_cmp(cmp, val, args[0])


def _eval_circuit(value, circuit):
    result = False

    if ":" in circuit:
        cmp = circuit.split(":")[1].upper()
        val = circuit.split(":")[2]

        if "," in val:
            val = val.split(",")

        if value in ("", "NULL", "null", None):
            return False

        if isinstance(value, (list,)):
            # This is a list comparison (probably a multi-select field)
            # use _cmp_arr instead of logic below
            sub_res = _cmp_arr(val, value, cmp)
            if sub_res == 1:
                return True
            else:
                return False
        else:
            return _as_cmp(cmp, val, value)

    return result


@functools.lru_cache(maxsize=2048)
def _cmp_val(val, actual_value, cmp):
    if cmp == "EQ":
        if str(val) == str(actual_value):
            return 1
    elif cmp == "NEQ":
        if str(val) != str(actual_value):
            return 1
    elif cmp == "GTE":
        if actual_value >= val:
            return 1
    elif cmp == "GT":
        if actual_value > val:
            return 1
    elif cmp == "LT":
        if actual_value < val:
            return 1
    elif cmp == "LTE":
        if actual_value <= val:
            return 1

    return 0


def _eval_single_circuit(*args, circuit=None):
    result = []
    raw_values = list(args)
    if ":" in circuit:
        cmp = circuit.split(":")[1].upper()
        val = circuit.split(":")[2]

        if "," in val:
            val = val.split(",")

        for actual_value in raw_values:
            if actual_value in ("", "NULL", "null", None):
                result.append(0)
            else:

                # Is this an array of data
                if actual_value is not None:
                    if "[" in actual_value:
                        actual_value = json.loads(actual_value)

                if isinstance(actual_value, (list,)):
                    # This is a list comparison (probably a multi-select field)
                    # use _cmp_arr instead of logic below
                    result.append(_cmp_arr(val, actual_value, cmp))
                else:
                    result.append(_cmp_val(val, actual_value, cmp))

        return sum(result)

    else:
        return sum([float(x) for x in raw_values if isOk(x)])


def _sum(vals):
    return sum(vals)


def _mean(vals):
    return mean(vals)


_IDS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']


def _parse(report, circuit):
    """ Evaluate a circuit against a report

    Args:
        report:
        circuit:

    Returns:

    """
    reduction = circuit.get("r")

    sources = dict(list(report))

    values = []

    for n in circuit.get("n"):
        if isinstance(n, (string_types,)):
            field = n.split(":")[0]
            n_values = sources.get(field, [])
            values.append(_eval_single_circuit(*n_values, circuit=n))
        else:
            # This is a complex, we need to evaluate to a binary value
            circuits = dict((x, str(uuid.uuid4())) for x in n[1:])

            setters = dict((id, []) for x, id in circuits.items())

            for key, value in circuits.items():
                id = circuits.get(key)
                field = key.split(":")[0]

                raw_values = sources.get(field, None)
                setters[id] = [_eval_circuit(value, key) for value in raw_values]

            result_sets = list(zip(*setters.values()))
            if n[0] == "ALL":
                values = values + [1 for x in result_sets if all(x)]
                # for item in result_sets:
                #     if all(x is True for x in item):
                #         values.append(1)
            else:
                values = values + [1 for x in result_sets if any(x)]
                # for item in result_sets:
                #     if True in item:
                #         values.append(1)

    # end_values = list(map(ok_parser, values))

    if reduction == "SUM":
        return _sum(values)
    elif reduction in ("MID", "AVG", "MED"):
        return _mean(values)


@functools.lru_cache(maxsize=1024)
def isOk(value):
    result = True

    if value is None:
        return False

    if value is None:
        result = False

    if value == "":
        result = False

    if value == ".":
        result = False

    # Test float conversion
    try:
        float(value)
    except ValueError:
        result = False

    return result


AGG_MAP = {
    "DAY": "D",
    "MONTH": "M",
    "WEEK": "W",
    "YEAR": "A"
}


async def run(target_interval=None,
              target_location=None,
              indicator=None,
              options=None,
              user=None):
    data = []

    load_geometry = options.get("geometry", False)
    fill_missing = options.get("fill_missing", False)
    constrain = options.get("constrain", False)

    source_reports = []

    start_date = None
    end_date = None

    if "start_date" in options:
        if isinstance(options.get("start_date"), string_types):
            start_date = date_utils.parse_date(options.get("start_date"))
        else:
            start_date = options.get("start_date")

    if 'end_date' in options:
        if isinstance(options.get("end_date"), string_types):
            end_date = date_utils.parse_date(options.get("end_date"))
        else:
            end_date = options.get("end_date")

    if "filter" in options:
        time_filter = options.get("filter")
        if time_filter == "YTD":
            end_date = datetime.datetime.now()
            start_date = datetime.date(end_date.year, 1, 2)
        elif time_filter == "1Y":
            end_date = datetime.datetime.now()
            start_date = date_utils.monthdelta(end_date, -12)
        elif time_filter == "6M":
            end_date = datetime.datetime.now()
            start_date = date_utils.monthdelta(end_date, -6)
        elif time_filter == "3M":
            end_date = datetime.datetime.now()
            start_date = date_utils.monthdelta(end_date, -3)
        elif time_filter == "1M":
            end_date = datetime.datetime.now()
            start_date = date_utils.monthdelta(end_date, -1)

    indicator_item = indicator
    if isinstance(indicator_item, (string_types,)):
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT AS uuid, name, itype, definition FROM %s.indicators WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                indicator,
            ))
            indicator_item = await cur.fetchone()
    elif isinstance(indicator_item, uuid.UUID):
        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT uuid::TEXT AS uuid, name, itype, definition
                FROM %s.indicators
                WHERE uuid = %s
            ''', (
                AsIs(user.get('tki')),
                indicator_item,
            ))
            indicator_item = await cur.fetchone()
    elif isinstance(indicator_item, (dict,)) and "uuid" in indicator_item.keys():
        temp_item = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT AS uuid, name, itype, definition FROM %s.indicators WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                indicator_item.get("uuid"),
            ))
            temp_item = await cur.fetchone()

        for key, value in indicator_item.items():
            if key != "uuid":
                temp_item[key] = value

        indicator_item = temp_item

    itype = indicator_item.get("itype", "DEFAULT")

    location = None
    if load_geometry:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, lineage, geometry_type, pcode
                FROM %s.locations WHERE uuid =%s
            """, (
                AsIs(user.get("tki")),
                target_location,
            ))
            location = await cur.fetchone()

        location['geometry'] = await Location.geometry(target_location, tki=user.get("tki"))
        if location.get("geometry_type") == "ADMIN":
            location['geometry_type'] = "ADMIN"
        else:
            location['geometry_type'] = "POINT"
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT, name, lineage, pcode FROM %s.locations WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                target_location,
            ))
            location = await cur.fetchone()

    if itype == "STATIC":
        # We're dealing with a static indicator here
        ind_value = indicator_item.get('definition', {}).get("value", 0)

        ep_dates = date_utils.get_date_range(start_date, end_date, target_interval)

        results = 0
        if options.get("reduction", None) is not None:
            results = float(ind_value)
        else:
            results = [[x.strftime("%Y-%m-%d"), float(ind_value)] for x in ep_dates]

        return dict(
            indicator=indicator_item,
            data=results,
            location=location
        )

    resultant_data = []
    if itype == "DEFAULT":
        resultant_data, source_reports = await _handle_default(
            dict(indicator_item),
            indicator,
            target_interval,
            start_date,
            end_date,
            dict(location),
            options,
            user=user
        )
    elif itype == "CUSTOM":
        resultant_data = []
        source_reports = []
    else:
        # Handle system indicators
        resultant_data, source_reports = await handle_system_indicator(
            indicator_item,
            indicator,
            target_interval,
            start_date,
            end_date,
            location,
            options,
            user=user
        )

    if fill_missing:
        ep_dates = date_utils.get_date_range(start_date, end_date, target_interval)

        dates_present = [x[0] for x in resultant_data]
        for ep_date in ep_dates:
            if ep_date not in dates_present:
                resultant_data.append([ep_date, None])

        resultant_data = sorted(resultant_data, key=lambda k: k[0])

    result = dict(
        indicator=indicator_item,
        location=location,
        data=resultant_data
    )

    if options.get("show_sources", False):
        result['source_reports'] = [x['uuid'] for x in source_reports]

    return result


class Query:
    def __init__(self):
        self.aliases = []

    def add_alias(self):
        pass

    def add_grouping(self):
        pass


async def _handle_default(
        indicator,
        indicator_item,
        target_interval,
        start_date,
        end_date,
        target_location,
        options,
        user=None):
    """
    Handle normal indicator querying
    :param indicator:
    :param target_interval:
    :param start_date:
    :param end_date:
    :param target_location:
    :param options:
    :return:
    """
    results = []
    data = []
    sources = dict()
    source_reports = []
    form_versions = []
    roll_up = options.get("roll_up", True)

    lookup_date = date_utils.get_start_of_interval(start_date, target_interval)

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
                v.uuid,
                v.form_id,
                v.etl,
                f.time_interval,
                v.etl->>'%s' AS output
            FROM %s.form_versions AS V
                LEFT JOIN %s.forms AS f ON f.id = v.form_id
            WHERE f.status = 'ACTIVE'
                AND v.etl->>'%s' IS NOT NULL;
        """, (
            AsIs(indicator.get("uuid")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(indicator.get("uuid"))
        ))
        form_versions = await cur.fetchall()

    for form_v in form_versions:
        logic_pointers = []

        if form_v.get("output", None) is not None:
            logic_pointers.append(form_v.get("output"))

        sources[form_v.get("uuid")] = logic_pointers

    fields = []
    # Get the names of the form fields needed
    for v_uuid, source in sources.items():
        for item in source:
            if isinstance(item, (string_types,)):
                item = json.loads(item)
            for rule in item.get("n"):
                if isinstance(rule, (string_types,)):
                    if ":" in rule:
                        fields.append(rule.split(":")[0])
                    else:
                        fields.append(rule)
                else:
                    subs = rule[1:]
                    fields = fields + [x.split(":")[0] for x in subs]

    unique_fields = []
    for x in fields:
        if x not in unique_fields:
            unique_fields.append(x)

    # Pull data from reports using the source maps
    reports = []

    tuple_options = []
    # tuple_options = [list(sources.keys())]

    resultant_data = dict()

    with_smtm = """
        WITH locations AS (
            SELECT uuid
            FROM %s.locations
            WHERE lineage @> ARRAY['{LOCATION}']::TEXT[]
            AND status = 'ACTIVE'
        )
    """ % (
        AsIs(user.get('tki')),
    )

    sql_string = ""
    if len(unique_fields) > 0:
        sql_string = """
            SELECT
                r.data_date AS data_date,
                {CODE}
            FROM %s.collections AS r
        """ % (
            AsIs(user.get("tki")),
        )
    else:
        sql_string = """SELECT r.uuid, r.location_id::TEXT, r.data_date, r.form_version_id
                FROM %s.collections AS r
            """ % (
            AsIs(user.get("tki")),
        )

    sql_string += """
        WHERE r.form_version_id = '{FORM_VERSION}'
        AND r.status = ANY('{SUBMITTED,PENDING_AMENDMENT}'::TEXT[])

    """

    if options.get("constrain_report", None) is None or options.get('constrain_report', False) is False:
        if roll_up is False:
            sql_string += """
                AND r.location_id::TEXT = %s::TEXT
            """
            tuple_options = tuple_options + [target_location.get("uuid")]
        else:
            sql_string = with_smtm + sql_string
            sql_string = sql_string.replace('{LOCATION}', str(target_location.get('uuid')))
            sql_string += """
                AND r.location_id = ANY(SELECT uuid from locations)
            """
    else:
        rec_location = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT location_id
                FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                options.get('constrain_report', None),
            ))
            rec_location = await cur.fetchone()
            rec_location = rec_location.get('location_id')

        tuple_options = tuple_options + [str(rec_location)]
        sql_string += " AND r.location_id::TEXT = %s"

        tuple_options = tuple_options + [str(options.get('constrain_report'))]
        sql_string += " AND r.uuid::TEXT = %s"

    if options.get("constrain_report", None) is None or options.get("constrain_report", False) is False:
        if start_date is not None:
            tuple_options = tuple_options + [start_date]
            sql_string += " AND r.data_date >= %s"

        if end_date is not None:
            tuple_options = tuple_options + [end_date]
            sql_string += " AND r.data_date <= %s"

    prepre = []

    sql_string += " GROUP BY r.data_date"

    items = []
    directed = False
    for fv_uuid, form_v in sources.items():
        # Avoid parsing json over and over in _parse
        circuits = []
        if isinstance(form_v[0], (string_types,)):
            form_v[0] = json.loads(form_v[0])

        # Parse the indicator extrap into a proper extrap
        circuits = form_v[0].get('n', [])

        groups = []

        wheres = []

        has_complexes = False
        for item in circuits:
            group = []
            if isinstance(item, list):
                # Dealing with a list
                has_complexes = True
                for node in item[1:]:
                    key, cmp, val, *_ = node.split(':')
                    field_def = await Form.get_field_fv(fv_uuid, key, user=user)
                    if cmp in ('GT', 'GTE', 'LT', 'LTE',):
                        if field_def[3] == 'number':
                            group.append(
                                "(CASE WHEN r.data->'%s' IS NOT NULL AND r.data->>'%s' != '' THEN CAST(r.data->>'%s' AS NUMERIC) %s %s ELSE FALSE END)" % (
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    CMP.get(cmp),
                                    val,
                                ))
                        else:
                            group.append(
                                "(CASE WHEN r.data->>'%s' IS NOT NULL AND r.data->>'%s' != '' THEN r.data->>'%s' %s %s ELSE FALSE END)" % (
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    CMP.get(cmp),
                                    val,
                                ))
                    elif cmp == 'CNT':
                        group.append(
                            "ARRAY(SELECT jsonb_array_elements_text(COALESCE(CASE WHEN (r.data->>'%s') IS NULL THEN NULL ELSE (r.data->'%s') END, '[]'::jsonb))) @> '{%s}'::TEXT[]" % (
                                '.'.join(key.split('.')[1:]),
                                '.'.join(key.split('.')[1:]),
                                val,
                            ))
                    elif cmp == 'NCNT':
                        group.append(
                            "NOT ARRAY(SELECT jsonb_array_elements_text(COALESCE(CASE WHEN (r.data->>'%s') IS NULL THEN NULL ELSE (r.data->'%s') END, '[]'::jsonb))) @> '{%s}'::TEXT[]" % (
                                '.'.join(key.split('.')[1:]),
                                '.'.join(key.split('.')[1:]),
                                val,
                            ))
                    else:
                        if field_def[3] in ('text', 'select'):
                            if cmp == 'EQ':
                                group.append("r.data->>'%s' = '%s'" % (
                                    '.'.join(key.split('.')[1:]),
                                    val,
                                ))
                            if cmp == 'NEQ':
                                group.append("r.data->>'%s' IS DISTINCT FROM '%s'" % (
                                    '.'.join(key.split('.')[1:]),
                                    val,
                                ))
                        else:
                            group.append("r.data->>'%s' %s '%s'" % (
                                '.'.join(key.split('.')[1:]),
                                CMP.get(cmp, '='),
                                val,
                            ))
                    wheres.append("r.data->>'%s' != ANY(VALUES(NULL),(''))" % (
                        '.'.join(key.split('.')[1:]),
                    ))

                if len(item[1:]) > 1:
                    if item[0] == 'ANY':
                        groups.append(('OR', group))
                    else:
                        groups.append(('AND', group))
                else:
                    key, cmp, val, *_ = item[1].split(':')
                    wheres.append("r.data->>'%s' != ANY(VALUES(NULL),(''))" % (
                        '.'.join(key.split('.')[1:]),
                    ))
                    groups.append(('', group[0]))
            else:
                if ':' in item:
                    key, cmp, val, *_ = item.split(':')
                    field_def = await Form.get_field_fv(fv_uuid, key, user=user)
                    if cmp in ('GT', 'GTE', 'LT', 'LTE',):
                        if field_def[3] == 'number':
                            group.append(
                                "(CASE WHEN r.data->'%s' IS NOT NULL AND r.data->>'%s' != '' THEN CAST(r.data->>'%s' AS NUMERIC) %s %s ELSE FALSE END)" % (
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    CMP.get(cmp),
                                    val,
                                ))
                        else:
                            group.append(
                                "(CASE WHEN r.data->>'%s' IS NOT NULL AND r.data->>'%s' != '' THEN r.data->>'%s' %s %s ELSE FALSE END)" % (
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    '.'.join(key.split('.')[1:]),
                                    CMP.get(cmp),
                                    val,
                                ))
                    elif cmp == 'CNT':
                        group.append(
                            "ARRAY(SELECT jsonb_array_elements_text(COALESCE(CASE WHEN (r.data->>'%s') IS NULL THEN NULL ELSE (r.data->'%s') END, '[]'::jsonb))) @> '{%s}'::TEXT[]" % (
                                '.'.join(key.split('.')[1:]),
                                '.'.join(key.split('.')[1:]),
                                val,
                            ))
                    elif cmp == 'NCNT':
                        group.append(
                            "NOT ARRAY(SELECT jsonb_array_elements_text(COALESCE(CASE WHEN (r.data->>'%s') IS NULL THEN NULL ELSE (r.data->'%s') END, '[]'::jsonb))) @> '{%s}'::TEXT[]" % (
                                '.'.join(key.split('.')[1:]),
                                '.'.join(key.split('.')[1:]),
                                val,
                            ))
                    else:
                        if field_def[3] in ('text', 'select'):
                            if cmp == 'EQ':
                                group.append("r.data->>'%s' = '%s'" % (
                                    '.'.join(key.split('.')[1:]),
                                    val,
                                ))
                            if cmp == 'NEQ':
                                group.append("r.data->>'%s' IS DISTINCT FROM '%s'" % (
                                    '.'.join(key.split('.')[1:]),
                                    val,
                                ))
                        else:
                            group.append("r.data->>'%s' %s '%s'" % (
                                '.'.join(key.split('.')[1:]),
                                CMP.get(cmp, '='),
                                val,
                            ))

                    wheres.append("r.data->>'%s' != ANY(VALUES(NULL),(''))" % (
                        '.'.join(key.split('.')[1:]),
                    ))

                    groups.append(('', group[0]))
                else:
                    directed = True
                    groups.append(
                        "COALESCE(CASE WHEN isnumeric(r.data->>'%s') THEN r.data->>'%s' END, '0.0')::float" % (
                            '.'.join(item.split('.')[1:]),
                            '.'.join(item.split('.')[1:]),
                        ))

        query_sql = ""

        if directed == True:
            print("DIRECTED")
            additions = []
            for g in groups:
                print("#", g)
                if isinstance(g, (list, tuple,)):
                    additions.append(g[1])
                else:
                    additions.append(g)

            for item in additions:
                print(item)

            if form_v[0].get('r', "SUM") == 'SUM':
                query_sql = """
                     SUM(%s) AS result
                """ % (
                    ' + '.join(additions),
                )
        else:
            group_count = 0
            additions = []
            for group in groups:
                print(group)
                group_count += 1
                if group[0] == 'OR':
                    additions.append(" OR ".join(group[1]))
                    #query_sql.append(' OR '.join(group[1]))
                elif group[0] == 'AND':
                    #query_sql.append(' AND '.join(group[1]))
                    additions.append(" AND ".join(group[1]))
                else:
                    #query_sql.append(group[1])
                    additions.append(group[1])

            query_sql = ') AND ('.join(additions)
            query_sql = 'COUNT(((%s)) OR NULL) AS result' % (query_sql,)

        local_sql = sql_string.replace('{CODE}', query_sql)
        print(local_sql)

        # st_time = time.time()
        with get_db_cursor() as cur:
            fv_sql = local_sql.replace("{FORM_VERSION}", str(fv_uuid))

            cur.execute(fv_sql, tuple(tuple_options))

            reports = cur.fetchall()

            for report in reports:
                sub_result = report.get('result', None)

                if sub_result is not None:
                    report_date = date_utils.parse_date(report.get('data_date'))
                    if report_date is not None:
                        actual_date = date_utils.get_end_of_current_interval(report_date, target_interval)
                        string_date = actual_date.strftime('%Y-%m-%d')

                        try:
                            resultant_data[string_date] = _add(resultant_data.get(string_date, 0), (sub_result or 0))
                        except KeyError:
                            resultant_data[string_date] = sub_result

    if len(resultant_data.keys()) <= 0:
        if options.get("reduction", None) is not None:
            return 0, []
        else:
            return [], []

    if options.get('reduction', None) is not None:
        if options.get("reduction") in ("AVG", "MED", "MEDIAN"):
            post_data = mean(list(resultant_data.values()))
        else:
            post_data = sum([y for x, y in resultant_data.items()])
    else:
        prepre = [[key, value] for key, value in resultant_data.items()]
        post_data = aggregate(target_interval, start_date, end_date, prepre)

    source_reports = [x.get("uuid") for x in reports]
    return post_data, source_reports


async def handle_system_indicator(indicator_item, indicator, target_interval, start_date, end_date, location, options,
                                  user=None):
    """ Perform calculation for a system indicator

    Args:
        indicator_item:
        target_interval:
        start_date:
        end_date:
        location:
        options:
        user:

    Returns:

    """
    results = []

    itype = indicator_item.get("itype")
    if itype == "ALERTS":
        results = await system_parser.query_alerts(
            indicator,
            target_interval,
            start_date,
            end_date,
            location,
            options,
            user=user
        )
    elif itype == "ASSIGNMENTS":
        results = await system_parser.query_assignments(
            indicator,
            target_interval,
            start_date,
            end_date,
            location,
            options,
            user=user
        )
    elif itype == "FORMS":
        results = await system_parser.query_forms(
            indicator,
            target_interval,
            start_date,
            end_date,
            location,
            options,
            user=user
        )
    elif itype == "FORM_SUBMISSIONS":
        results = await system_parser.query_submissions(
            indicator,
            target_interval,
            start_date,
            end_date,
            location,
            options,
            user=user
        )
    elif itype == "DEVICES":
        results = await system_parser.query_devices(indicator, target_interval, start_date, end_date, location, options,
                                                    user=user)
    elif itype == "TASKS":
        results = await system_parser.query_tasks(indicator, target_interval, start_date, end_date, location, options,
                                                  user=user)
    elif itype == "USERS":
        results = await system_parser.query_users(indicator, target_interval, start_date, end_date, location, options,
                                                  user=user)
    elif itype == "LOCATIONS":
        results = await system_parser.query_locations(indicator, target_interval, start_date, end_date, location,
                                                      options,
                                                      user=user)
    else:
        results = []

    return results, []
