import json


def recursive_find(data, field_name):
    """ Recursivley find a value
    :param data: {dict} The dict to look up the value in
    :param field_name: {str} The field name to look up
    :return:
    """
    levels = field_name.split(".")

    result = data
    for level in levels:
        if result is not None:
            if level in result:
                result = result[level]
            else:
                result = None

    if result in ("NULL", "null", "Null", ""):
        result = None

    if result is not None:
        try:
            result = float(result)
        except ValueError:
            result = result

    return result


def merge_two_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


def flatten(data, parent):
    """ Flattens a data object to a flat list of keys and values
    :param data: {dict}
    :param parent:
    :return:
    """
    if isinstance(data, basestring) and "{" in data:
        data = json.loads(data)

    result = dict()

    for key, value in data.items():
        path_name = key
        if parent is not None:
            path_name = "%s.%s" % (parent, key,)

        if isinstance(value, (dict,)):
            sub_data = flatten(value, path_name)
            result = merge_two_dicts(result, sub_data)
        else:
            result[path_name] = value

    return result
