from ewars.db import get_db_cur
from psycopg2.extensions import AsIs


async def get_grouped_locations(group_ids, collapse=True, user=None):
    """ get a definitive list of locations to use in a group analysis query where
        no overlaps occur because of lineage
    
    Args:
        group_ids: The group ids to use
        user: The calling user

    Returns:

    """
    locations = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid::TEXT, name, lineage, pcode
            FROM %s.locations 
            WHERE status = 'ACTIVE'
              AND groups::TEXT[] @> %s::TEXT[];
        """, (
            AsIs(user.get("tki")),
            group_ids,
        ))
        locations = await cur.fetchall()

    pre_dict = dict((x.get("uuid"), x) for x in locations)

    if not collapse:
        return locations

    removals = []

    for key, value in pre_dict.items():
        for root_key in pre_dict.keys():
            if root_key in value.get("lineage")[0:-1]:
                removals.append(key)

    result = []

    for key, value in pre_dict.items():
        if key not in removals:
            result.append(value)

    return result
