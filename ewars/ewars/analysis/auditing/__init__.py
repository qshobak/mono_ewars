from .completeness import *
from .timeliness import *
from .account import get_account_stats, get_account_metrics

from ewars.analysis.system import completeness

async def performance(metric, location_id, start_date, end_date, form_id, user=None):
    results = None

    if metric == "COMPLETENESS":
        results = await completeness.get_rps_locale_raw(
            location_id,
            start_date,
            end_date,
            form_id=form_id,
            user=user
        )
    elif metric == "TIMELINESS":
        results = await get_timeliness(
            location_id,
            form_id,
            start_date,
            end_date,
            user=user
        )
    elif metric == "ACCOUNT_STATS":
        results = await get_account_stats(location_id, user=user)

    return results