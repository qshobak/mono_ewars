import datetime

from ewars.constants import CONSTANTS

from ewars.db import get_db_cursor
from ewars.utils import date_utils
from ewars.models import Location
from ewars.analysis import aggregate

from psycopg2.extensions import AsIs



def get_completeness_location(location_id, start_date, end_date, user=None):
    """ Calculates overall completeness for a specific location

    Args:
        location_id: The UUID of the location
        start_date: The start date to inspect from
        end_date: The end date to inspect up to
        user: The calling user

    Returns:

    """
    account = None
    location = None
    result = []
    forms = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT uuid, lineage
            FROM %s.locations
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            location_id,
        ))
        location = cur.fetchone()

        cur.execute("""
            SELECT * from _iw.accounts
            WHERE location_id = %s
        """, (
            location['lineage'][0],
        ))
        account = cur.fetchone()

        cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
        ))
        forms = cur.fetchall()

    data = []
    for form in forms:
        form_result = get_completeness(
                location_id,
                form.get('id'),
                start_date,
                end_date,
                user=user,
        )
        if form_result is not None:
            data = data + form_result

    pre_dict = dict()
    for dp in data:
        if dp[1] is not None:
            if dp[0] not in pre_dict.keys():
                pre_dict[dp[0]] = dict(
                        expected=0,
                        submitted=0
                )

            pre_dict[dp[0]]['expected'] += dp[1].get('expected')
            pre_dict[dp[0]]['submitted'] += dp[1].get('submitted')

    results = [[key, value] for key, value in pre_dict.items()]
    results = sorted(results, key=lambda k: k[0])

    return results


def get_overall_completeness(account_id, start_date, end_date, user=None):
    """ Calculate overall completeness for an account

    Args:
        account_id: The account to check completeness against
        start_date: The start period
        end_date: The ned period
        user: The calling user

    Returns:
        A series of data for the completeness of a location
    """

    account = None
    result = []
    forms = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
        ))
        forms = cur.fetchall()

        cur.execute("""
            SELECT location_id
            FROM _iw.accounts
            WHERE id = %s
        """, (
            account_id,
        ))
        account = cur.fetchone()

    data = []
    for form in forms:
        form_result = get_completeness(
                account.get("location_id"),
                form.get('id'),
                start_date,
                end_date,
                user=user,
        )
        if form_result is not None:
            data = data + form_result

    pre_dict = dict()
    for dp in data:
        if dp[1] is not None:
            if dp[0] not in pre_dict.keys():
                pre_dict[dp[0]] = dict(
                        expected=0,
                        submitted=0
                )

            pre_dict[dp[0]]['expected'] += dp[1].get('expected')
            pre_dict[dp[0]]['submitted'] += dp[1].get('submitted')

    results = [[key, value] for key, value in pre_dict.items()]
    results = sorted(results, key=lambda k: k[0])

    return results


