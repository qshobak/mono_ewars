import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from .completeness import get_overall_completeness
from .timeliness import get_overall_timeliness

class Stats(object):
    USERS = 0
    COLLECTIONS = 0
    FORMS = 0
    TIMELINESS = 0
    COMPLETENESS = 0
    ALERTS_TOTAL = 0
    ALERTS_OPEN = 0
    ORGANIZATIONS = 0

    def _as_dict(self):
        return self.__dict__


async def get_account_stats(account_id, user=None):
    """ Retrieve administrative stats about an account

    Args:
        account_id: The id of the account to retrieve stats about
        user:

    Returns:

    """
    stats = Stats()

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS t FROM %s.users;
            """, (
            AsIs(user.get("tki")),
        ))
        stats.USERS = await cur.fetchone()['t']

        await cur.execute("""
            SELECT COUNT(*) AS t
            FROM %s.collections
                WHERE (status = 'SUBMITTED' OR status = 'PENDING_AMENDMENT')
        """, (
            AsIs(user.get("tki")),
        ))
        stats.COLLECTIONS = await cur.fetchone()['t']

        await cur.execute("""
            SELECT COUNT(*) AS t
            FROM %s.forms
                WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
        ))
        stats.FORMS = await cur.fetchone()['t']

    return stats._as_dict()


def get_account_metrics(account_id, user=None):
    """

    Args:
        account_id: The id of the account to get metrics about
        user: THe calling user

    Returns:
        A dict of metric values for the account in quesiotn
    """
    result = None
    account = None

    with get_db_cursor() as cur:
        cur.execute("""SELECT tki FROM _iw.accounts WHERE id = %s""", (account_id))
        account = cur.fetchone()

        cur.execute("""
            SELECT
                (SELECT created FROM _iw.accounts WHERE id = %s) AS OPENED,
                (SELECT 'DEFAULT') AS ACC_TYPE,
                (SELECT COUNT(*) FROM %s.tasks_v2) AS TASKS,
                (SELECT COUNT(*) FROM %s.tasks_v2 WHERE state = 'OPEN') AS TASKS_UNACTIONED,
                (SELECT COUNT(*) FROM _iw.sso WHERE aid = %s) AS USERS,
                (SELECT COUNT(*) FROM _iw.sso WHERE aid = %s AND role = 'ACCOUNT_ADMIN') AS USERS_ACCOUNT_ADMIN,
                (SELECT COUNT(*) FROM _iw.sso WHERE aid = %s AND role = 'REGIONAL_ADMIN') AS USERS_GEO_ADMIN,
                (SELECT COUNT(*) FROM _iw.sso WHERE aid = %s AND role = 'USER') AS USERS_USER,
                (SELECT COUNT(*) FROM %s.forms WHERE status = 'ACTIVE') AS FORMS,
                (SELECT COUNT(DISTINCT(org_id)) FROM _iw.sso WHERE aid = %s) AS ORGANIZATIONS,
                (SELECT COUNT(*) FROM %s.devices) AS DEVICES,
                (SELECT COUNT(*) FROM %s.sms_gateways) AS GATEWAYS;
        """, (
            account_id,
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            account_id,
            account_id,
            account_id,
            account_id,
            AsIs(account.get("tki")),
            account_id,
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
        ))
        result = cur.fetchone()

        # Open Alerts
        cur.execute("""
            SELECT
              (SELECT COUNT(*) FROM %s.alerts WHERE state != 'DELETED') AS ALERTS,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'OPEN') AS ALERTS_OPEN,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'OPEN' AND stage = 'VERIFICATION') AS ALERTS_OPEN_VERIFICATION,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'OPEN' AND state = 'RISK_ASSESS') AS ALERTS_OPEN_RISK_ASSESS,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'OPEN' AND state = 'RISK_CHAR') AS ALERTS_OPEN_RISK_CHAR,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'OPEN' AND state = 'OUTCOME') AS ALERTS_OPEN_OUTCOME,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'CLOSED') AS ALERTS_CLOSED,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'CLOSED' AND outcome = 'MONITOR') AS ALERTS_CLOSED_MONITOR,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'CLOSED' AND outcome = 'DISCARDED') AS ALERTS_CLOSED_DISCARD,
              (SELECT COUNT(*) FROM %s.alerts WHERE state = 'CLOSED' AND outcome = 'RESPONSE') AS ALERTS_CLOSED_RESPONSE
            FROM %s.alerts AS a;
        """, (
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
            AsIs(account.get("tki")),
        ))
        alert_data = cur.fetchone()

    if alert_data is not None:
        for key, value in alert_data.items():
            result[key] = value

    end_date = datetime.date.today()
    start_date = end_date - datetime.timedelta(days=30)

    completeness = get_overall_completeness(account_id, start_date, end_date, user=user)

    expected_complete = 0
    received_complete = 0
    for item in completeness:
        expected_complete += item[1].get("expected")
        received_complete += item[1].get("submitted")

    try:
        result['completeness'] = (received_complete / expected_complete)
    except ZeroDivisionError:
        result['completeness'] = 0

    timeliness = get_overall_timeliness(account_id, start_date, end_date, user=user)

    expected = 0
    on_time = 0

    for item in timeliness:
        expected += item[1].get("expected")
        on_time += item[1].get("on_time")

    try:
        result['timeliness'] = (on_time / expected)
    except ZeroDivisionError:
        result['timeliness'] = 0


    return result
