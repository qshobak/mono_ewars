import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.models import Location
from ewars.utils import date_utils

from ewars.constants import CONSTANTS


def get_timeliness_location(location_id, start_date, end_date, options, user=None):
    """ Calcualte timeliness for a specific location

    Args:
        location_id: The UUID of thje location
        start_date: The start date to inspect from
        end_date: The end date to inspec tup to
        user: The calling user

    Returns:

    """
    account = None
    location = None
    forms = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT uuid, lineage
            FROM %s.locations
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            location_id,
        ))
        location = cur.fetchone()

        cur.execute("""
            SELECT * from _iw.accounts
            WHERE location_id = %s
        """, (
            location['lineage'][0],
        ))
        account = cur.fetchone()

        cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            account["id"],
        ))
        forms = cur.fetchall()

    data = []
    for form in forms:
        form_result = get_timeliness(
            location_id,
            form.get('id'),
            start_date,
            end_date,
            user=user,
        )
        if form_result is not None:
            data = data + form_result

    pre_dict = dict()
    for dp in data:
        if dp[1] is not None:
            if dp[0] not in pre_dict.keys():
                pre_dict[dp[0]] = dict(
                    submitted=0,
                    on_time=0,
                    late=0,
                    expected=0,
                    missing=0
                )

            if dp[1] is not None:
                pre_dict[dp[0]]['expected'] += dp[1].get('expected')
                pre_dict[dp[0]]['submitted'] += dp[1].get('submitted')
                pre_dict[dp[0]]['missing'] += dp[1].get('missing')
                pre_dict[dp[0]]['on_time'] += dp[1].get('on_time')
                pre_dict[dp[0]]['late'] += dp[1].get('late')

    results = [[key, value] for key, value in pre_dict.items()]
    results = sorted(results, key=lambda k: k[0])

    if options.get("reduction", None) is not None:
        expected = sum([x[1].get("expected") for x in results])
        on_time = sum([x[1].get("on_time") for x in results])

        try:
            results = on_time / expected
        except ZeroDivisionError:
            results = 0

    return results


def get_overall_timeliness(account_id, start_date, end_date, options, user=None):
    """ Get overall timeliness for an account

    Args:
        account_id: THe account to get the timeliness for
        start_date: The start period
        end_date: The end period
        user: THe calling user

    Returns:

    """
    account = None
    forms = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT location_id
            FROM _iw.accounts
            WHERE id = %s
        """, (
            account_id,
        ))
        account = cur.fetchone()

        cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            account_id,
        ))
        forms = cur.fetchall()

    data = []
    for form in forms:
        form_result = get_timeliness(
            account.get("location_id"),
            form.get('id'),
            start_date,
            end_date,
            user=user
        )
        if form_result is not None:
            data = data + form_result

    pre_dict = dict()
    for dp in data:
        if dp[1] is not None:
            if dp[0] not in pre_dict.keys():
                pre_dict[dp[0]] = dict(
                    submitted=0,
                    late=0,
                    expected=0,
                    missing=0,
                    on_time=0
                )

            pre_dict[dp[0]]['submitted'] = dp[1]['submitted']
            pre_dict[dp[0]]['late'] = dp[1]['late']
            pre_dict[dp[0]]['expected'] = dp[1]['expected']
            pre_dict[dp[0]]['missing'] = dp[1]['missing']
            pre_dict[dp[0]]['on_time'] = dp[1]['on_time']

    results = [[key, value] for key, value in pre_dict.items()]
    results = sorted(results, key=lambda k: k[0])

    return results


async def get_timeliness(location_id, form_id, start_date, end_date, user=None):
    """ Get the timeliness of reporting for each time interval for a given report
    :param location_id: {str} The UUID of the location
    :param form_id: {int} THe form id to look at timeliness
    :param start_date: {date} The start of the period
    :param end_date: {date} The end of the period
    :param user:
    :return:
    """
    result = []
    form = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = await cur.fetchone()

    interval = None
    site_type_id = None
    has_location = False
    has_interval = False
    has_overdue = False
    overdue = None
    overdue_interval = None

    if "INTERVAL_REPORTING" in form.get('features').keys():
        has_interval = True
        interval = form['features']['INTERVAL_REPORTING']['interval']

    if "LOCATION_REPORTING" in form.get("features").keys():
        has_location = True
        site_type_id = form['features']['LOCATION_REPORTING']['site_type_id']

    if "OVERDUE" in form.get('features').keys():
        has_overdue = True
        overdue = form['features']['OVERDUE']['threshold']
        overdue_interval = form['features']['OVERDUE']['interval']

    if not has_overdue:
        return []

    overdue_thresh_days = 0

    if overdue_interval == "WEEK":
        overdue_thresh_days = 7 * int(overdue)
    elif overdue_interval == "DAY":
        overdue_thresh_days = int(overdue)
    elif overdue_interval == "MONTH":
        overdue_thresh_days = 30 * int(overdue)

    if not has_location:
        reporting_periods = await Location.get_reporting_periods_for_form(location_id, form_id, user=user)
    else:
        reporting_periods = None
        sub_time = date_utils.get_date_range(
            start_date,
            end_date,
            interval
        )
        # date: (loc_uuids, expected, on_time)
        sub_dict = dict((x, [[], 0, 0]) for x in sub_time)

        # Get reporting periods under the parent location
        reporting_periods = await Location.get_rps_locale(
            location_id,
            site_type_id,
            form.get('id'),
            user=user
        )

        # if reporting periods is null, return []
        if reporting_periods is None:
            return []

        if len(reporting_periods) <= 0:
            return []

        for rp in reporting_periods:
            for ts, dt in sub_dict.items():
                if rp.get('start_date') <= ts <= rp.get("end_date"):
                    dt[0].append(rp.get("location_id"))

        for ts, dt in sub_dict.items():
            dt[2] = len(dt[0])

        async with get_db_cur() as cur:
            for ts, dt in sub_dict.items():
                delays = []
                if len(dt[0]) > 0:
                    await cur.execute("""
                        SELECT delay
                        FROM %s.timeliness
                        WHERE form_id = %s 
                          AND location_id::TEXT = ANY(%s)
                          AND data_date = %s;
                    """, (
                        AsIs(user.get("tki")),
                        form.get("id"),
                        dt[0],
                        ts,
                    ))
                    delays = await cur.fetchall()

                    for delay in delays:
                        if delay.get("delay") <= overdue_thresh_days:
                            dt[1] += 1

        results = [[ts, [dt[1], dt[2]]] for ts, dt in sub_dict.items()]
        results = sorted(results, key=lambda k: k[0])

        return results
