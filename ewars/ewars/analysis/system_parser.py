import time

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from .aggregate import aggregate
from .system import reporting
from ewars.utils import date_utils

from ewars.analysis.system import reporting_locations, completeness, missing, timeliness

ARIT = {
    "EQ": "=",
    "NEQ": "!="
}


async def new_query_alerts(definition, target_interval, start_date, end_date, location, options, user=None):
    is_custom = False

    sql = """
        SELECT a.uuid, a.trigger_end
        FROM %s.alerts AS a
          LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
        {WHERES}
        l.lineage::TEXT[] @> %s::TEXT[]
        AND a.trigger_end >= %s
        AND a.trigger_end <= %s
    """

    wheres = []
    alarm_id = definition.get("alarm_id", None)

    # Map in the alarm id
    if alarm_id is not None:
        wheres.append("a.alarm_id = '%s'" % (definition.get("alarm_id")))

    dimension = definition.get("dimension", None)
    if dimension == "ALERTS_OPEN":
        wheres.append("a.state = 'OPEN'")
    elif dimension == "ALERTS_CLOSED":
        wheres.append("a.state IN ('CLOSED','AUTODISCARDED')")
    elif dimension == "ALERTS_CLOSED_NON_AUTO":
        wheres.append("a.state IN ('CLOSED')")
    elif dimension == "ALERTS_AUTO_DISCARDED":
        wheres.append("a.state = 'AUTODISCARDED'")

    elif dimension == "ALERTS_DISCARDED":
        wheres.append("a.state IN ('AUTODISCARDED')")
        wheres.append("a.outcome = 'DISCARDED'")
    elif dimension == "ALERTS_MONITORED":
        wheres.append("a.outcome = 'MONITOR'")
    elif dimension == "ALERTS_RESPOND":
        wheres.append("a.outcome = 'RESPONSE'")

    # VERIFICATION
    elif dimension == "ALERTS_IN_VERIFICATION":
        wheres.append("a.stage = 'VERIFICATION'")
        wheres.append("a.stage_state IN ('PENDING','ACTIVE')")
    elif dimension == "ALERTS_AWAIT_VERIFICATION":
        wheres.append("a.stage = 'VERIFICATION'")
        wheres.append("a.stage_state IN ('PENDING','ACTIVE')")
    elif dimension == "ALERTS_VERIFIED":
        # Verified alerts
        is_custom = True
        sql = """
            SELECT a.uuid, a.trigger_end
            FROM %s.alerts AS a
              LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
            WHERE ((a.stage = 'VERIFICATION' AND a.stage_state = 'COMPLETED') OR (a.stage IN ('RISK_ASSESS','RISK_CHAR','OUTCOME')))
            AND a.state != 'AUTODISCARDED'
            AND l.lineage::TEXT[] @> %s::TEXT[]
            AND a.trigger_end >= %s
            AND a.trigger_end <= %s
        """

        if alarm_id:
            sql = sql + " AND a.alarm_id = '%s'" % (alarm_id,)

    elif dimension == "ALERTS_DISCARDED_VERIFICATION":
        wheres.append("a.stage = 'VERIFICATION'")
        wheres.append("a.outcome = 'DISCARD'")
        wheres.append("a.stage_state = 'COMPLETED'")
    elif dimension == "ALERTS_MONITORED_OUTCOME":
        wheres.append("a.stage = 'VERIFICATION'")
        wheres.append("a.outcome = 'MONITOR'")
        wheres.append("a.stage_state = 'COMPLETED'")


    # RISK_ASSESS
    elif dimension == "ALERTS_IN_RISK_ASSESS":
        wheres.append("a.stage = 'RISK_ASSESS'")
        wheres.append("a.stage_state IN ('PENDING','ACTIVE')")
    elif dimension == "ALERTS_AWAIT_RISK_ASSESS":
        wheres.append("a.stage IN ('VERIFICATION','RISK_CHAR')")
        wheres.append("a.stage_state = 'PENDING'")
    elif dimension == "ALERTS_RISK_ASSESSED":
        is_custom = True
        sql = """
            SELECT a.uuid, a.trigger_end
            FROM %s.alerts AS a
              LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
            WHERE a.stage IN ('RISK_CHAR','OUTCOME')
            AND l.lineage::TEXT[] @> %s::TEXT[]
            AND a.trigger_end >= %s
            AND a.trigger_end <= %s
        """

        if alarm_id:
            sql = sql + " AND a.alarm_id = '%s'" % (alarm_id,)

    # Risk Characterization
    elif dimension == "ALERTS_IN_RISK_CHAR":
        wheres.append("a.stage = 'RISK_CHAR'")
        wheres.append("a.stage_state IN ('PENDING','ACTIVE')")
    elif dimension == "ALERTS_AWAIT_RISK_CHAR":
        wheres.append("a.stage IN ('VERIFICATION','RISK_ASSESS','RISK_CHAR')")
        wheres.append("a.stage_state = 'PENDING'")
    elif dimension == "ALERTS_RISK_CHAR":
        is_custom = True
        sql = """
            SELECT a.uuid, a.trigger_end
            FROM %s.alerts AS a
              LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
            WHERE a.stage = 'OUTCOME'
            AND a.risk IS NOT NULL
            AND l.lineage::TEXT[] @> %s::TEXT[]
            AND a.trigger_end >= %s
            AND a.trigger_end <= %s
        """

        if alarm_id:
            sql = sql + " AND a.alarm_id = '%s'" % (alarm_id,)

    # Outcome
    elif dimension == "ALERTS_IN_OUTCOME":
        wheres.append("a.stage = 'OUTCOME'")
        wheres.append("a.stage_state IN ('PENDING','ACTIVE')")
    elif dimension == "ALERTS_AWAIT_OUTCOME":
        wheres.append("a.stage IN ('VERIFICATION','RISK_ASSESS','RISK_CHAR','OUTCOME')")
        wheres.append("a.stage_state = 'PENDING'")
    elif dimension == "ALERTS_OUTCOME":
        is_custom = True
        sql = """
            SELECT a.uuid, a.trigger_end
            FROM %s.alerts AS a
              LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
            WHERE a.stage IN ('OUTCOME','VERIFICATION')
            AND a.outcome IS NOT NULL
            AND a.stage_state = 'COMPLETED'
            AND l.lineage::TEXT[] @> %s::TEXT[]
            AND a.trigger_end >= %s
            AND a.trigger_end <= %s
        """

        if alarm_id:
            sql = sql + " AND a.alarm_id = '%s'" % (alarm_id,)


    elif dimension == "ALERTS_DISCARD_OUTCOME":
        wheres.append("a.stage = 'OUTCOME'")
        wheres.append("a.stage_state = 'COMPLETED'")
        wheres.append("a.outcome = 'DISCARD'")
    elif dimension == "ALERTS_MONITORED_OUTCOME":
        wheres.append("a.stage = 'OUTCOME'")
        wheres.append("a.stage_state = 'COMPLETED'")
        wheres.append("a.outcome = 'MONITOR")
    elif dimension == "ALERTS_RESPOND_OUTCOME":
        wheres.append("a.stage = 'OUTCOME'")
        wheres.append("a.stage_state = 'COMPLETED'")
        wheres.append("a.outcome = 'RESPONSE'")

    # Risks
    elif dimension == "ALERTS_RISK_LOW":
        wheres.append("a.risk = 'LOW'")
        wheres.append("a.stage = 'OUTCOME'")
    elif dimension == "ALERTS_RISK_MODERATE":
        wheres.append("a.risk = 'MODERATE'")
        wheres.append("a.stage = 'OUTCOME'")
    elif dimension == "ALERTS_RISK_HIGH":
        wheres.append("a.risk = 'HIGH'")
        wheres.append("a.stage = 'OUTCOME'")
    elif dimension == "ALERTS_RISK_SEVERE":
        wheres.append("a.risk = 'SEVERE'")
        wheres.append("a.stage = 'OUTCOME'")

    if not is_custom:
        if len(wheres) > 0:
            sql = sql.replace("{WHERES}", "WHERE " + " AND ".join(wheres) + " AND ")
        else:
            sql = sql.replace("{WHERES}", " WHERE ")

    results = None

    async with get_db_cur() as cur:
        await cur.execute(sql, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [str(location.get("uuid"))],
            start_date,
            end_date,
        ))
        results = await cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in results]
    result = None
    if options.get("reduction", None) is not None:
        result = sum([x[1] for x in series])
    else:
        result = aggregate(target_interval, start_date, end_date, series)

    return result


async def query_alerts(definition, target_interval, start_date, end_date, location, options, user=None):
    if "dimension" in definition.keys():
        # Support for newer style definitions for alert
        return await new_query_alerts(definition, target_interval, start_date, end_date, location, options, user=user)

    if definition.get("uuid", None) == "a18b5f1f-4775-4bc9-89cf-b0d83abab45a":
        return await new_query_alerts(definition, target_interval, start_date, end_date, location, options, user=user)

    if "dimension" not in definition.keys():
        checks = ["state", "stage", "stage_state", "outcome"]
        has_some = False
        for check in checks:
            if check in definition.keys():
                has_some = True

        if has_some == False:
            return await new_query_alerts(definition, target_interval, start_date, end_date, location, options,
                                          user=user)

    results = []

    sql = """
        SELECT a.uuid, a.trigger_end
        FROM %s.alerts AS a
        LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
        {WHERES}
        l.lineage::TEXT[] @> %s::TEXT[]
        AND a.trigger_end >= %s
        AND a.trigger_end <= %s

    """

    wheres = []
    for key, value in definition.items():
        if key not in ("account_id", "uuid", "definition") and value is not None:
            wheres.append("a.%s = '%s'" % (
                key,
                value,
            ))

    if len(wheres) > 0:
        sql = sql.replace("{WHERES}", "WHERE " + " AND ".join(wheres) + " AND ")
    else:
        sql = sql.replace("{WHERES}", "WHERE ")

    results = None

    async with get_db_cur() as cur:
        await cur.execute(sql, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [location.get("uuid")],
            start_date,
            end_date,
        ))
        results = await cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in results]
    result = None
    if options.get("reduction", None) is not None:
        result = sum([x[1] for x in series])
    else:
        result = aggregate(target_interval, start_date, end_date, series)

    return result


async def query_assignments(definition, target_interval, start_date, end_date, location, options, user=None):
    results = []

    sql = """
        SELECT a.uuid, a.created
        FROM %s.assignments AS a
        LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
        WHERE l.lineage::TEXT[] @> %s::TEXT[]
        AND created >= %s
        AND created <= %s
        AND {WHERES}

    """

    wheres = []
    for definition in definition:
        data = definition.split(":")

        if "null" not in data:
            wheres.append("a.%s %s '%s'" % (
                data[0],
                ARIT[data[1]],
                data[2],
            ))

    sql = sql.replace("{WHERES}", " AND ".join(wheres))

    results = None

    async with get_db_cur() as cur:
        await cur.execute(sql, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [location.get("uuid")],
            start_date,
            end_date,
        ))
        results = await cur.fetchall()

    series = [[x.get("trigger_end"), 1] for x in results]
    result = aggregate(target_interval, start_date, end_date, series)

    if options.get("reduction", None) is not None:
        result = max([x[1] for x in result])

    return result


async def query_submissions(definition, target_interval, start_date, end_date, location, options, user=None):
    """ Handle querying against form submissions, there are some complex items in here including
    customized metrics for calculating completeness, timeliness, submitted, missing and otherwise
    for a give form or for all forms

    Args:
        definition: The indicator definition
        target_interval: The target interval
        start_date: The start date
        end_date: The end date
        location: The location to query against
        reduction: A reducing function for the data series
        user: The calling user

    Returns:

    """
    results = []

    st_time = time.time()
    metric = definition.get("metric", None)
    org_id = definition.get("organization_id", None)
    form_id = definition.get("form_id", None)
    source = definition.get("source", None)

    if metric == "COMPLETENESS":
        results = await completeness.get_completeness(
            location.get("uuid"),
            start_date,
            end_date,
            form_id=form_id,
            reduction=options.get('reduction', None),
            user=user
        )

    elif metric == "TIMELINESS":
        results = await timeliness.get_timeliness(
            location.get("uuid"),
            start_date,
            end_date,
            form_id=form_id,
            reduction=options.get('reduction', None),
            user=user
        )

    elif metric in ("SUBMISSIONS", "SUBMITTED"):
        results = await reporting.get_report_submissions(
            form_id,
            target_interval,
            start_date,
            end_date,
            location,
            source=source,
            tki=user.get('tki')
        )

        st_agg = time.time()
        if options.get("reduction", None) is not None:
            results = sum([x[1] for x in results])

    elif metric == "MISSING":
        results = await missing.get_missing(
            location.get("uuid"),
            start_date,
            end_date,
            form_id=form_id,
            reduction=options.get('reduction', None),
            user=user
        )

        if options.get("reduction", None) is not None:
            results = sum([x[1] for x in results])

    elif metric == "LATE":
        results = await reporting.get_reports_late(form_id, target_interval, start_date, end_date, location, options,
                                                   user=user)
        if options.get('reduction', None) is not None:
            results = sum([x[1] for x in results])
    elif metric == "EXPECTED":
        results = await reporting.get_reports_expected(form_id, target_interval, start_date, end_date, location,
                                                       options, user=user)
        if options.get("reduction", None) is not None:
            results = sum([x[1] for x in results])

    return results


async def query_devices(definition, target_interval, start_date, end_date, location, options, user=None):
    results = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT last_seen
            FROM %s.devices
            WHERE last_seen >= start_date
            AND last_seen <= end_date
        """, (
            AsIs(user.get("tki")),
            start_date,
            end_date,
        ))
        results = await cur.fetchall()

    return results


async def query_tasks(definition, target_interval, start_date, end_date, location, options, user=None):
    results = []

    return results


async def query_locations(definition, target_interval, start_date, end_date, location, options, user=None):
    data = []
    start_value = 0

    status = 'ACTIVE'
    if "status" in definition:
        status = definition.get("status")

    site_type_id = definition.get("site_type_id", None)

    sql = """
            SELECT created_date
            FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
            AND status = %s
            {SITE_TYPE_ID}
    """

    if site_type_id is not None:
        sql = sql.replace("{SITE_TYPE_ID}", "AND site_type_id = %s" % (site_type_id,))
    else:
        sql = sql.replace("{SITE_TYPE_ID}", "")

    async with get_db_cur() as cur:
        await cur.execute(sql, (
            AsIs(user.get("tki")),
            [location.get("uuid")],
            status,
        ))
        data = await cur.fetchall()

        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.locations
            WHERE status = %s
            AND lineage::TEXT[] @> %s::TEXT[]
            AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            status,
            [location.get("uuid")],
            site_type_id,
        ))
        res = await cur.fetchone()
        start_value = res.get('total')

    pre_agg = [[x.get('created_date'), 1] for x in data]

    ep_dates = date_utils.get_date_range(start_date, end_date, "DAY")

    dates_present = [x[0] for x in pre_agg]
    for ep_date in ep_dates:
        if ep_date not in dates_present:
            pre_agg.append([ep_date.strftime("%Y-%m-%d"), start_value])

    def agg_fn(date, value):
        return value

    result = [[x, start_value] for x in ep_dates]

    if options.get("reduction", None) is not None:
        result = max([x[1] for x in result])

    return result


async def query_forms(definition, target_interval, start_date, end_date, location, options, user=None):
    results = []
    metric = None
    if "metric" in definition:
        metric = definition.get("metric", None)
    else:
        metric = definition.get("dimension", None)

    form_id = definition.get("form_id", None)

    if metric in ("REPORTING_LOCATIONS", "LOCATIONS"):
        results = await reporting_locations.query_reporting_locations(
            location.get("uuid"),
            start_date,
            end_date,
            form_id=definition.get("form_id", None),
            reduction=options.get('reduction', None),
            user=user
        )

    elif metric is None:
        results = 0

    return results


async def query_users(definition, target_interval, start_date, end_date, location, options, user=None):
    results = []
    start_value = 0

    users = []

    account_id = definition.get("account_id", user.get("aid"))

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT registered, role, aid
            FROM %s.users
            WHERE aid = %s
                AND status = 'ACTIVE'
                AND registered >= %s
                AND registered <= %s
        """, (
            AsIs(user.get("tki")),
            user.get('aid'),
            start_date,
            end_date,
        ))
        users = await cur.fetchall()

        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.users
            WHERE aid = %s
                AND status = 'ACTIVE'
                AND registered < %s
        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
            start_date,
        ))
        res = await cur.fetchone()
        start_value = res.get('total')

    pre_data = [[x.get("registered"), 1] for x in users]

    if options.get("reduction", None) is not None:
        result = sum([x[1] for x in pre_data])
        result = result + start_value
    else:
        result = aggregate(target_interval, start_date, end_date, pre_data)
        counter = start_value
        for item in result:
            counter = item[1] + counter
            item[1] = counter

    return result
