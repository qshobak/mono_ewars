from ewars.db import get_db_cursor, get_db_cur

from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS


async def get_account_metrics(user=None):
    """Retrieve metrics for a specific account

    Args:
        user: The calling user

    Returns:
        Alerts, Responses and overdue metric values for the specific account
    """
    alerts = 0
    responses = 0
    overdue = 0

    # TODO: Need to extrapolate based on account type

    async with get_db_cur() as cur:
        if user['role'] == CONSTANTS.USER:
            await cur.execute("""
                SELECT COUNT(*) AS total
                    FROM %s.alerts AS a
                        LEFT JOIN %s.alert_users AS au ON au.alert_id = a.uuid
                    WHERE state ='OPEN'
                        AND au.user_id = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user['id'],
            ))
        elif user['role'] == CONSTANTS.ACCOUNT_ADMIN:
            await cur.execute("""
                    SELECT COUNT(a.*) AS total
                        FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        WHERE l.lineage::TEXT[] @> %s::TEXT[]
                            AND a.state = 'OPEN';
                """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user.get("clid")],
            ))
        elif user['role'] == CONSTANTS.REGIONAL_ADMIN:
            await cur.execute("""
                SELECT COUNT(a.*) AS total
                    FROM %s.alerts AS a
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                    WHERE l.lineage::TEXT[] @> %s::TEXT[]
                        AND a.state = 'OPEN';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user['location_id']],
            ))
        else:
            await cur.execute("""
                    SELECT COUNT(*) AS total FROM %s.alerts;
                """, (
                AsIs(user.get("tki")),
            ))
        data = await cur.fetchone()
        alerts = data.get('total')

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
                FROM %s.forms
                WHERE status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
        ))
        data = await cur.fetchone()
        overdue = data.get("total")

    return dict(
        alerts=alerts,
        responses=responses,
        overdue=overdue
    )
