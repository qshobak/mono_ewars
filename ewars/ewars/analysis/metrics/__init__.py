from .overall_metrics import get_account_metrics
from .account_metrics import get_account_summary_metrics
from ewars.core.metrics import retrieval


async def get_metric(metric_name, user=None):
    """ Retrieve a metric value from the system

    Args:
        metric_name: The name of the metric
        user: The calling user

    Returns:

    """
    result = None

    if metric_name in ("FORM_SUBMISSIONS", "SUBMISSIONS"):
        result = await retrieval.get_submissions(user=user)
    elif metric_name == "ASSIGNMENTS":
        result = await retrieval.get_assignments(user=user)
    elif metric_name == "REPORTING_LOCATIONS":
        result = await retrieval.get_reporting_locations(user=user)
    elif metric_name == "USERS":
        result = await retrieval.get_users(user=user)
    elif metric_name == "LOCATIONS":
        result = await retrieval.get_locations(user=user)
    elif metric_name == "ALERTS_OPEN":
        result = await retrieval.get_open_alerts(user=user)
    elif metric_name == "ALERTS_TOTAL":
        result = await retrieval.get_all_alerts(user=user)
    elif metric_name == "ALERTS_CLOSED":
        result = await retrieval.get_alerts_closed(user=user)
    elif metric_name == "FORMS":
        result = await retrieval.get_forms(user=user)
    elif metric_name == "DEVICES":
        result = await retrieval.get_devices(user=user)
    elif metric_name == "PARTNERS":
        result = await retrieval.get_partners(user=user)
    elif metric_name == "TASKS_OPEN":
        result = await retrieval.get_tasks_open(user=user)
    elif metric_name == "TASKS_TOTAL":
        result = await retrieval.get_tasks(user=user)
    elif metric_name == "COMPLETENESS":
        result = await retrieval.get_completeness(user=user)
    elif metric_name == "TIMELINESS":
        result = await retrieval.get_timeliness(user=user)
    elif metric_name == "DOCUMENTS":
        result = await retrieval.get_documents(user=user)
    elif metric_name == "ALARMS":
        result = await retrieval.get_alarms(user=user)
    elif metric_name == "ORGANIZATIONS":
        result = await retrieval.get_organizations(user=user)
    else:
        result = 0

    return result
