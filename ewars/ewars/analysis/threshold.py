from ewars.common.constants import CONSTANTS

from ewars.common.utils import date_utils

def create_threshold_series(data, definition, user=None):
    """ Transforms a data series into a threshold series
    :param data: {list} The original data series
    :param definition: {dict} The definition for the transformation
    :param user: {dict} The calling user
    :return:
    """
    result = []

    options = definition.get('options')
    start_date_intervals = options.get("start_date_intervals", None)
    start_date_spec = options.get("start_date_specification", None)
    end_date_intervals = options.get("end_date_intervals", None)
    end_date_spec = options.get("end_date_specification", None)
    indicator = data.get("indicator", None)
    location_id = data.get("location", None)
    comparator = options.get("comparator", None)
    floor = options.get("floor", None)
    interval = options.get("interval", None)
    reduction = options.get("reduction", None)


    range_set = date_utils.get_range_set(data.get("interval"), data.get("start_date"), data.get("end_date"), default_value=0)

    return data

