import datetime

from ewars.constants import CONSTANTS
from ewars.utils import date_utils
from ewars.utils.six import string_types


def aggregate(interval, start_date, end_date, data, agg_fn=None):
    """ Aggregates a series of data together

    Args:
        interval: The interval to aggregate to
        start_date: The start date of the aggregation
        end_date: The end date of the aggregation
        data: The data to be aggregated
        agg_fn: And optional function to use as the aggregate method for the
        values in the series

    Returns:
        The result of the aggregation.
    """

    for item in data:
        if isinstance(item[0], (datetime.datetime,)):
            item[0] = item[0].date()
        elif isinstance(item[0], (string_types,)):
            item[0] = date_utils.parse_date(item[0])

    if isinstance(start_date, (string_types,)):
        start_date = date_utils.parse_date(start_date)

    if isinstance(end_date, (string_types,)):
        end_date = date_utils.parse_date(end_date)

    range_sets = date_utils.get_range_set(start_date, end_date, interval)

    for ranger in range_sets:
        for item in data:
            if item[0] >= ranger.get("x") and item[0] <= ranger.get("y"):
                if agg_fn is None:
                    try:
                        ranger['z'] += item[1]
                    except ValueError:
                        ranger['z'] = item[1]
                    except TypeError:
                        ranger['z'] = item[1]
                else:
                    ranger['z'] = agg_fn(ranger['z'], item[1])

    return [[x.get("y"), x.get("z")] for x in range_sets if x.get("z") is not None]


def merge_series(series):
    """ Merge multiple time series together
    
    Args:
        series: 

    Returns:

    """
    result = []

    pre_dict = dict()

    for sii in series:
        for data_date, value in sii:
            if value is None:
                value = 0
            try:
                pre_dict[data_date] = pre_dict[data_date] + value
            except KeyError:
                pre_dict[data_date] = value

    return [[key, value] for key, value in pre_dict.items()]


def merge_statics(series):
    """ Merge multiple static value series together.
    
    Args:
        series: 

    Returns:

    """
    result = 0

    for slice in series:
        result = result + slice.get("data", 0)

    return result
