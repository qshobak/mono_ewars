from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import date_utils


def get_missing_reports_period_based(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval", None)
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    active_locations = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
                AND status = 'ACTIVE'
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        active_locations = cur.fetchone()['ct']

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], form['time_interval'])

    reports = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date, (SELECT 1) AS ct
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            end_date,
        ))
        reports = cur.fetchall()

    if len(reports) <= 0:
        return []

    pre_data = dict((key.strftime("%Y-%m-%d"), dict(expected=0, have=0)) for key in form_time_series)

    for report in reports:
        try:
            pre_data[report.get("data_date").strftime("%Y-%m-%d")]['have'] += 1
        except KeyError:
            pre_data[report.get("data_date").strftime("%Y-%m-%d")]['have'] = 1

    for key, value in pre_data.items():
        value['expected'] = active_locations

    for key, value in pre_data.items():
        expected = value.get("expected")
        have = value.get("have")

        if expected <= 0:
            value = 0
        else:
            try:
                value = expected - have
            except Exception:
                value = 0

        pre_data[key] = value


    results = [[key, value] for key, value in pre_data.items()]

    return results
