import datetime

from ewars.db import get_db_cursor
from ewars.utils import date_utils
from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs


def get_submission_counts(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []
    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    reports = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.submitted_date, (SELECT 1) AS ct
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.form_id = %s
                AND r.submitted_date >= %s
                AND r.submitted_date <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            lookup_start,
            end_date,
            [target_location['uuid']],
        ))
        reports = cur.fetchall()

    df = pd.DataFrame(reports)
    df['submitted_date'] = pd.to_datetime(df['submitted_date'])
    df.set_index(pd.DatetimeIndex(df['submitted_date']), inplace=True)
    df = df.resample(rule="1W", how="sum")

    subseto = [df.columns.tolist()] + df.reset_index().values.tolist()
    data = subseto[1:]

    results = [[x[0].strftime("%Y-%m-%d"), x[1]] for x in data]

    return results


def get_submitted_for_periods(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    form_time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], target_interval)

    reports = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            end_date,
        ))
        reports = cur.fetchall()

    if len(reports) <= 0:
        return []

    pre_data = dict((key.strftime("%Y-%m-%d"), 0) for key in form_time_series)

    for report in reports:
        try:
            pre_data[report.get("data_date").strftime("%Y-%m-%d")] += 1
        except KeyError:
            pre_data[report.get("data_date").strftime("%Y-%m-%d")] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results


def get_expected_reports_for_period(*args, **kwargs):
    """
    Returns the number of reports expected for each period
    :param target_location:
    :param target_interval:
    :param start_date:
    :param end_date:
    :param reduction:
    :param form_id:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []
    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    location_count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE status = 'ACTIVE'
                AND lineage::TEXT[] @> %s::TEXT[]
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        location_count = cur.fetchone()['ct']

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    pre_data = dict((item.strftime("%Y-%m-%d"), location_count) for item in form_time_series)

    # Clean up in case this is a current year
    for key, value in pre_data.items():
        today = datetime.date.today()
        edate = date_utils.parse_date(key)

        if edate > today:
            del pre_data[key]


    if target_interval == form['time_interval']:
        results = [(key, value) for key, value in pre_data.items()]
    elif target_interval == CONSTANTS.YEAR:
        coarse = dict((item.strftime("%Y-%m-%d"), 0) for item in time_series)
        for key, data in pre_data.items():
            source_date = date_utils.parse_date(key)

            # Find which target this belongs to
            target = None
            for target_date in time_series:
                if date_utils.is_in(source_date, form['time_interval'], target_date, target_interval):
                    target = target_date

            if target is not None:
                string_target = target.strftime("%Y-%m-%d")
                coarse[string_target] += data

        for key, data in coarse.items():
            value = 0
            try:
                value = data
            except ZeroDivisionError:
                pass

            results.append((key, value))

    return results
