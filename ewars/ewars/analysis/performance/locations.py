from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import date_utils


def get_reporting_locations(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    results = []
    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    location_count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
                WHERE lineage::TEXT[] @> %s::TEXT[]
                AND status = 'ACTIVE'
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        location_count = cur.fetchone()['ct']

    results = [[dt, location_count] for dt in time_series]

    return results
