import datetime

from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs
from ewars.constants import CONSTANTS

from ewars.utils import date_utils


def get_timeliness(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    # TODO: This is temporary until location reporting
    # intervals are in place, this just assumes that
    # every period has the same number of locations available
    # and that all locations have been reporting since the beginning
    # of time
    location_count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE status = 'ACTIVE'
                AND lineage::TEXT[] @> %s::TEXT[]
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        location_count = cur.fetchone()['ct']

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    pre_data = dict((item.strftime("%Y-%m-%d"), dict(on_time=0, late=0, submitted=0)) for item in form_time_series)
    late_delta = date_utils.get_delta(form['overdue_threshold'], form['overdue_interval'])

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], form['time_interval'])

    reports = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date, r.submitted_date
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            form_time_series[-1],
        ))
        reports = cur.fetchall()

    # For each report, map its data out to the correct pre_data category
    for report in reports:
        ddate = date_utils.parse_date(report['data_date'])
        sdate = date_utils.parse_date(report['submitted_date'])

        string_date = ddate.strftime("%Y-%m-%d")

        is_late = False

        if string_date in pre_data.keys():
            try:
                pre_data[string_date]['submitted'] += 1
            except KeyError:
                pre_data[string_date] = dict(
                        late=0,
                        expected=0,
                        submitted=0
                )

            if sdate > ddate + late_delta:
                try:
                    pre_data[string_date]['late'] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            submitted=0,
                            on_time=0
                    )

            if sdate <= ddate + late_delta:
                try:
                    pre_data[string_date]["on_time"] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            submitted=0,
                            on_time=1
                    )

    # Iterate through and adjust based on whether
    # the reporting date is over
    for key, data in pre_data.items():
        ddate = date_utils.parse_date(key)

        is_over = False
        if ddate + late_delta > datetime.date.today():
            is_over = True

        if is_over:
            data['late'] = location_count - data.get("on_time")

        # if we're IN the ddate interval now, leave it in, but if it's past the current period take it out
        # so that it doesn't skew the timeliness
        if ddate > datetime.date.today():
            del pre_data[key]

        if target_interval == form['time_interval']:
            if ddate > end_date and key in pre_data.keys():
                del pre_data[key]

    results = []

    if target_interval == form['time_interval']:
        for key, data in pre_data.items():
            value = 0
            try:
                value = float(data.get("on_time")) / data.get("submitted")
            except ZeroDivisionError:
                pass
            results.append((key, value,))
    elif target_interval == CONSTANTS.YEAR:
        coarse = dict((item.strftime("%Y-%m-%d"), dict(on_time=0, late=0, expected=0, submitted=0)) for item in time_series)

        for key, data in pre_data.items():
            source_date = date_utils.parse_date(key)
            # Need to check if this is a partial year (i.e. it's the current year and it's not over yet)
            is_partial_year = False

            if date_utils.is_in(datetime.date.today(), "DAY", source_date, CONSTANTS.YEAR):
                is_partial_year = True

            # Find which target this belongs to
            target = None
            for target_date in time_series:
                if date_utils.is_in(source_date, form['time_interval'], target_date, target_interval):
                    target = target_date

            if target is not None:
                string_target = target.strftime("%Y-%m-%d")
                coarse[string_target]['expected'] += location_count
                coarse[string_target]['on_time'] += data.get("on_time")
                coarse[string_target]['late'] += data.get("late")
                coarse[string_target]['submitted'] += data.get("submitted")

        for key, data in coarse.items():
            value = 0
            try:
                value = float(data.get("on_time")) / float(data.get('submitted'))
            except ZeroDivisionError:
                pass

            results.append((key, value))
    else:
        for key, data in pre_data.items():
            value = 0
            try:
                value = float(data.get("on_time")) / data.get("submitted")
            except ZeroDivisionError:
                pass
            results.append((key, value,))

    return results


def get_late_reports(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    # TODO: This is temporary until location reporting
    # intervals are in place, this just assumes that
    # every period has the same number of locations available
    # and that all locations have been reporting since the beginning
    # of time
    location_count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE status = 'ACTIVE'
                AND lineage::TEXT[] @> %s::TEXT[]
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        location_count = cur.fetchone()['ct']

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    pre_data = dict((item.strftime("%Y-%m-%d"), dict(on_time=0, late=0)) for item in form_time_series)
    late_delta = date_utils.get_delta(form['overdue_threshold'], form['overdue_interval'])

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], form['time_interval'])

    reports = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date, r.submitted_date
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            form_time_series[-1],
        ))
        reports = cur.fetchall()

    # For each report, map its data out to the correct pre_data category
    for report in reports:
        ddate = date_utils.parse_date(report['data_date'])
        sdate = date_utils.parse_date(report['submitted_date'])

        string_date = ddate.strftime("%Y-%m-%d")

        is_late = False

        if string_date in pre_data.keys():
            if sdate > ddate + late_delta:
                try:
                    pre_data[string_date]['late'] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            on_time=0
                    )

            if sdate <= ddate + late_delta:
                try:
                    pre_data[string_date]["on_time"] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            on_time=1
                    )

    # Iterate through and adjust based on whether
    # the reporting date is over
    for key, data in pre_data.items():
        ddate = date_utils.parse_date(key)

        is_over = False
        if ddate + late_delta > datetime.date.today():
            is_over = True

        if is_over:
            data['late'] = location_count - data.get("on_time")

        # if we're IN the ddate interval now, leave it in, but if it's past the current period take it out
        # so that it doesn't skew the timeliness
        if ddate > datetime.date.today():
            del pre_data[key]

    results = []

    if target_interval == form['time_interval']:
        for key, data in pre_data.items():
            value = 0
            try:
                value = data.get("late")
            except ZeroDivisionError:
                pass
            results.append((key, value,))
    elif target_interval == CONSTANTS.YEAR:
        coarse = dict((item.strftime("%Y-%m-%d"), 0) for item in time_series)

        for key, data in pre_data.items():
            source_date = date_utils.parse_date(key)
            # Need to check if this is a partial year (i.e. it's the current year and it's not over yet)
            is_partial_year = False

            if date_utils.is_in(datetime.date.today(), "DAY", source_date, CONSTANTS.YEAR):
                is_partial_year = True

            # Find which target this belongs to
            target = None
            for target_date in time_series:
                if date_utils.is_in(source_date, form['time_interval'], target_date, target_interval):
                    target = target_date

            if target is not None:
                string_target = target.strftime("%Y-%m-%d")
                coarse[string_target] += data.get("late")

        results = [(key, data) for key, data in coarse.items()]

    return results


def get_on_time_reports(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    # TODO: This is temporary until location reporting
    # intervals are in place, this just assumes that
    # every period has the same number of locations available
    # and that all locations have been reporting since the beginning
    # of time
    location_count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE status = 'ACTIVE'
                AND lineage::TEXT[] @> %s::TEXT[]
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        location_count = cur.fetchone()['ct']

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    pre_data = dict((item.strftime("%Y-%m-%d"), dict(on_time=0, late=0)) for item in form_time_series)
    late_delta = date_utils.get_delta(form['overdue_threshold'], form['overdue_interval'])

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], form['time_interval'])

    reports = []
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date, r.submitted_date
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            form_time_series[-1],
        ))
        reports = cur.fetchall()

    # For each report, map its data out to the correct pre_data category
    for report in reports:
        ddate = date_utils.parse_date(report['data_date'])
        sdate = date_utils.parse_date(report['submitted_date'])

        string_date = ddate.strftime("%Y-%m-%d")

        is_late = False

        if string_date in pre_data.keys():
            if sdate > ddate + late_delta:
                try:
                    pre_data[string_date]['late'] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            on_time=0
                    )

            if sdate <= ddate + late_delta:
                try:
                    pre_data[string_date]["on_time"] += 1
                except KeyError:
                    pre_data[string_date] = dict(
                            late=0,
                            on_time=1
                    )

    # Iterate through and adjust based on whether
    # the reporting date is over
    for key, data in pre_data.items():
        ddate = date_utils.parse_date(key)

        is_over = False
        if ddate + late_delta > datetime.date.today():
            is_over = True

        if is_over:
            data['late'] = location_count - data.get("on_time")

        # if we're IN the ddate interval now, leave it in, but if it's past the current period take it out
        # so that it doesn't skew the timeliness
        if ddate > datetime.date.today():
            del pre_data[key]

    results = []

    if target_interval == form['time_interval']:
        for key, data in pre_data.items():
            results.append((key, data.get("on_time"),))
    elif target_interval == CONSTANTS.YEAR:
        coarse = dict((item.strftime("%Y-%m-%d"), 0) for item in time_series)

        for key, data in pre_data.items():
            source_date = date_utils.parse_date(key)
            # Need to check if this is a partial year (i.e. it's the current year and it's not over yet)
            is_partial_year = False

            if date_utils.is_in(datetime.date.today(), "DAY", source_date, CONSTANTS.YEAR):
                is_partial_year = True

            # Find which target this belongs to
            target = None
            for target_date in time_series:
                if date_utils.is_in(source_date, form['time_interval'], target_date, target_interval):
                    target = target_date

            if target is not None:
                string_target = target.strftime("%Y-%m-%d")
                coarse[string_target] += data.get("on_time")

        results = [(key, data) for key, data in coarse.items()]

    return results
