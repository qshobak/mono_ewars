from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import date_utils


def get_assignment_series(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    assignments = []


    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.created, (SELECT 1) AS ct
                FROM %s.assignments AS a
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE l.lineage::TEXT[] @> %s::TEXT[]
                    AND a.status = 'ACTIVE'
                    AND a.location_id = %s
                    AND a.form_id = %s
                    AND created >= %s
                    AND created <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form_id,
            lookup_start,
            end_date,
        ))
        assignments = cur.fetchall()

    return []

