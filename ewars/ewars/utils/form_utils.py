def get_field_by_path(definition, path):
    current = definition

    for item in path.split("."):
        if item in current:
            current = current[item]

    return current


def get_field_details(definition, field_path):
    """
    Get a field from a form definition by it's FQP (fully qualified path)
    :param definition:
    :param field_path:
    :return:
    """
    result = None

    if "." in field_path:
        fixed = ".fields.".join(field_path.split("."))

        result = get_field_by_path(definition, fixed)
    else:
        result = definition[field_path]

    return result


def get_field_full_name(definition, field_path):
    """
    Get the fully qualified name of a field, traversing the definition for labels
    if the field is nested in the definition
    :param definition: {dict} Form definition to look up the path in
    :param field_path: {str} Path to the end field
    :return:
    """
    names = []

    parts = field_path.split(".")

    obj = definition
    for part in parts:
        if part in obj:
            if isinstance(obj[part].get("label"), (dict,)):
                names.append(obj[part].get("label", {}).get("en", "Unknown"))
            else:
                names.append(obj[part].get("label", "Unknown"))
            if 'fields' in obj[part]:
                obj = obj[part]['fields']

    return " > ".join(names)


def get_field_definition(definition, path):
    """
    get a dot delineated path from within a form definition, this is a special case as the
    children of a field exist under a `fields` property
    :param definition:
    :param path:
    :return:
    """
    trail = path.split('.')

    current = definition
    for item in trail:
        if current.get(item, None) is not None:
            current = current.get(item)

        # Check if this field has a fields setting
        if current.get("fields", None) is not None:
            current = current.get("fields")

    return current


def get_key_path(item, path):
    """Retrieve a value at a specified locale in a dict

    Args:
        item (dict): The dict to look in
        path (str): A (.) delineated path to look up a value in the dict

    Returns:
        The value if found, or None

    """
    nodes = path.split(".")
    nodes = nodes[1:]

    res = item

    for node in nodes:
        if res is not None \
                and isinstance(res, object):
            if node in res:
                res = res.get(node, None)
            else:
                res = None

    # Need to handle values set to NULL
    if res == "NULL":
        res = None

    return res


IGNORED_FIELDS = ["header", "display"]
NESTED_FIELDS = ["matrix", "row"]


def flatten_definition(definition, parentKey=None):
    result = dict()

    for key, value in definition.items():
        if value.get('type') not in IGNORED_FIELDS:
            lineage = key
            if parentKey is not None:
                lineage = "%s.%s" % (parentKey, key)

            if value.get("type") in NESTED_FIELDS:
                childs = flatten_definition(value.get("fields"), parentKey=lineage)
                for k, v in childs.items():
                    result[k] = v
            else:

                result[lineage] = value

    return result
