import os
from jinja2 import Environment, PackageLoader, FileSystemLoader
from tornado import template

from ewars.conf import settings
from ewars.utils._os import get_install_root

env = Environment(loader=PackageLoader('ewars', 'static'))
file_env = Environment(loader=FileSystemLoader(os.path.join(get_install_root(), "templates/emails"),
                                               followlinks=True))


def render_template(template_name, **kwargs):
    """Render a template stored on the file system

    Args:
        template_name: The filename/path of the template to render
        **kwargs: (dict) A dict of data to pass to the template

    Returns:
        A formatted string containing the processed template

    """
    template = file_env.get_template(template_name)
    return template.render(**kwargs)


def render_string(content, **kwargs):
    """ REnder a plain string with variables

    Args:
        content:  The string template to render
        **kwargs: The variables to pass in

    Returns:
        A compiled string of content based on the template and variables
    """

    template = file_env.from_string(content)
    return template.render(**kwargs)


def render_html(content, **kwargs):
    """Renders a template from a source string

    Args:
        content: (str) A string containing the template to render
        **kwargs: (dict) A dict of data to pass to the template

    Returns:
        A parsed and complete rendered template

    """

    content = "{% extends 'email_template.html' %}{% block content %}" + content + "{% endblock %}"
    template = file_env.from_string(content)
    return template.render(**kwargs)


def render_plain(content, **kwargs):
    """ Renders a template from source in plaintext with no html

    Args:
        content: The plaintext template to use
        **kwargs: Data associated with the template

    Returns:
        The rendered template with the content injected
    """
    content = "{%extends 'plain_email_template.txt' %}{% block content %}" + content + "{% endblock %}"
    template = file_env.from_string(content)
    return template.render(**kwargs)
