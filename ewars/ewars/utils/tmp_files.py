import uuid
import csv
import os
import timeit
import time

from ewars.conf import settings


csv.register_dialect(
        'ewars_dialect',
        delimiter=',',
        quotechar='"',
        doublequote=True,
        skipinitialspace=True,
        lineterminator='\r\n',
        quoting=csv.QUOTE_MINIMAL
)


def write_tmp_file(data, filename, file_type):
    """Write a file to the OS as a tmp file

    The file_type is used for routing to the correct exporting
    mechanism, whether it's PDF or CSV usually.

    TODO: Port the report data download to use this API as well.

    Args:
        data: THe data to write, or definition of the data
        filename: The string to use as the filename
        file_type: The type of file name

    Returns:

    """
    result = None

    if file_type == "csv":
        result = _write_tmp_csv(data, filename)
    elif file_type == "pdf":
        result = None
    elif file_type == "xls":
        result = None
    elif file_type == "jpg":
        # Probably will use for exporting images of graphs
        result = None

    return result


def _write_tmp_csv(data, filename):
    file_name = "%s_sep_%s_%s.csv" % (str(uuid.uuid4()), filename, str(time.time()).replace('.', ''))
    write_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)

    start_time = time.time()
    with open(write_path, "w", encoding='utf-8') as f:
        d_writer = csv.writer(f, dialect='ewars_dialect')
        d_writer.writerows(data)

    print('WRITE', time.time() - start_time)


    return file_name
