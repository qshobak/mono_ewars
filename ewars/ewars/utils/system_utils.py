import os


def which(program):
    """Finds an executable on the system path

    This is primarily used to find WKHTMLTOPDF on the
    host system as it can potentially be in any number of
    locations

    Args:
        program (str): The executable name of the binary we're looking for

    Returns:
        The path to the binary/executable on the host system

    """

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    last_resorts = [
        "/usr/local/bin/"
    ]

    for item in last_resorts:
        exe_file = os.path.join(item, program)
        if is_exe(exe_file):
            return exe_file

    if program == "wkhtmltopdf":
        if is_exe("/opt/wkhtmltox/bin/wkhtmltopdf"):
            return "/opt/wkhtmltox/bin/wkhtmltopdf"

    return None
