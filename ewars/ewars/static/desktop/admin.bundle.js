/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 273);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(79), __esModule: true };

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(102);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(106);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(68);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Block = exports.Panel = exports.Vbox = exports.Hbox = undefined;

var _lt = __webpack_require__(109);

var _lt2 = _interopRequireDefault(_lt);

var _lt3 = __webpack_require__(113);

var _lt4 = _interopRequireDefault(_lt3);

var _lt5 = __webpack_require__(114);

var _lt6 = _interopRequireDefault(_lt5);

var _lt7 = __webpack_require__(115);

var _lt8 = _interopRequireDefault(_lt7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Hbox = _lt2.default;
exports.Vbox = _lt4.default;
exports.Panel = _lt6.default;
exports.Block = _lt8.default;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ActionItem = function (_React$Component) {
    (0, _inherits3.default)(ActionItem, _React$Component);

    function ActionItem(props) {
        (0, _classCallCheck3.default)(this, ActionItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ActionItem.__proto__ || (0, _getPrototypeOf2.default)(ActionItem)).call(this, props));

        _this._onClick = function () {
            _this._hideHover();
            _this.props.onClick(_this.props.data[1]);
        };

        _this._showHover = function () {
            _this._hover = document.createElement('div');
            _this._hover.setAttribute('class', 'action-hover');
            _this._hover.appendChild(document.createTextNode(_this.props.data[2]));

            document.body.appendChild(_this._hover);
        };

        _this._hideHover = function () {
            if (_this._hover) {
                if (_this._hover) {
                    document.body.removeChild(_this._hover);
                    _this._hover = null;
                }
            }
        };

        _this._hover = function (e) {
            e.stopPropagation();
            e.preventDefault();
        };

        _this._hover = null;
        return _this;
    }

    (0, _createClass3.default)(ActionItem, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (this.props.data[2]) {

                this.el.addEventListener('mouseover', this._showHover);
                this.el.addEventListener('mouseout', this._hideHover);
            }
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            if (this.props.data[2]) {
                this.el.removeEventListener('mouseover', this._showHover);
                this.el.removeEventListener('mouseout', this._hideHover);
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return React.createElement(
                'div',
                {
                    onHover: this._hover,
                    ref: function ref(el) {
                        _this2.el = el;
                    },
                    className: 'action',
                    onClick: this._onClick },
                React.createElement('i', { className: "fal " + this.props.data[0] })
            );
        }
    }]);
    return ActionItem;
}(React.Component);

var ActionGroup = function (_React$Component2) {
    (0, _inherits3.default)(ActionGroup, _React$Component2);

    function ActionGroup(props) {
        (0, _classCallCheck3.default)(this, ActionGroup);
        return (0, _possibleConstructorReturn3.default)(this, (ActionGroup.__proto__ || (0, _getPrototypeOf2.default)(ActionGroup)).call(this, props));
    }

    (0, _createClass3.default)(ActionGroup, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate() {
            return true;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this4 = this;

            var items = this.props.actions || [];

            var className = "action-group";
            className += this.props.right ? ' right' : '';
            className += this.props.vertical ? ' vert' : '';
            return React.createElement(
                'div',
                { className: className },
                items.map(function (item) {
                    return React.createElement(ActionItem, { data: item, onClick: _this4.props.onAction });
                })
            );
        }
    }]);
    return ActionGroup;
}(React.Component);

ActionGroup.defaultProps = {
    actions: [],
    onAction: function onAction(action) {
        console.log("ACTION: ", action);
    },
    style: {},
    height: "20px",
    vertical: false,
    right: false
};
exports.default = ActionGroup;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.3' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 9 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ROLES = exports.COLORS = exports.FIELDS = undefined;

var _const = __webpack_require__(76);

var _const2 = _interopRequireDefault(_const);

var _const3 = __webpack_require__(33);

var _const4 = _interopRequireDefault(_const3);

var _const5 = __webpack_require__(117);

var _const6 = _interopRequireDefault(_const5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FIELDS = _const2.default;
exports.COLORS = _const4.default;
exports.ROLES = _const6.default;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(40)('wks');
var uid = __webpack_require__(26);
var Symbol = __webpack_require__(9).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var ctx = __webpack_require__(23);
var hide = __webpack_require__(18);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(13);
var IE8_DOM_DEFINE = __webpack_require__(53);
var toPrimitive = __webpack_require__(41);
var dP = Object.defineProperty;

exports.f = __webpack_require__(15) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(19)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var createDesc = __webpack_require__(27);
module.exports = __webpack_require__(15) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(58);
var defined = __webpack_require__(38);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(57);
var enumBugKeys = __webpack_require__(44);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(36);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(110), __esModule: true };

/***/ }),
/* 26 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 28 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(14).f;
var has = __webpack_require__(17);
var TAG = __webpack_require__(11)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ComponentState = exports.state = undefined;

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var state = void 0;

var ComponentState = function ComponentState() {
    var _this = this;

    (0, _classCallCheck3.default)(this, ComponentState);

    this.subscribe = function (event, callback) {
        if (_this._listeners[event]) {
            _this._listeners[event].push(callback);
        } else {
            _this._listeners[event] = [callback];
        }
    };

    this._emit = function (event) {
        if (_this._listeners[event]) {
            _this._listeners[event].forEach(function (cb) {
                cb();
            });
        }
    };

    this.addTab = function (tab) {
        var newId = utils.uuid();
        _this.tabs.push({
            _: newId,
            cmp: tab._,
            n: tab.n,
            i: tab.i || null,
            _id: null
        });
        _this.activeTab = newId;

        _this._emit('VIEW_UPDATED');
    };

    this.removeTab = function (tid) {
        _this.activeTab = 'DEFAULT';
        var rIndex = void 0;
        _this.tabs.forEach(function (item, index) {
            if (item._ == tid) rIndex = index;
        });
        _this.tabs.splice(rIndex, 1);
        _this._emit('VIEW_UPDATED');
    };

    this.getActiveTab = function () {
        var tab = _this.tabs.filter(function (item) {
            return item._ == _this.activeTab;
        })[0] || null;

        return tab;
    };

    this.setActiveTab = function (tid) {
        _this.activeTab = tid;
        _this._emit('VIEW_UPDATED');
    };

    this.setView = function (view) {
        _this.view = view;
        _this._emit('VIEW_UPDATED');
    };

    this.registerSubState = function (id, state) {
        _this.subStates[id] = state;
    };

    this.getSubState = function (id) {
        if (_this.subStates[id]) return _this.subStates[id];
        return null;
    };

    exports.state = state = this;
    this.tabs = [];
    this.view = 'DEFAULT';
    this.dirty = false;
    this.activeTab = 'DEFAULT';

    this._listeners = {};

    this.subStates = {};
};

exports.default = ComponentState;
exports.state = state;
exports.ComponentState = ComponentState;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(38);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var COLORS = {
    polar0: 'rgb(47, 52, 64)',
    polar1: 'rgb(59, 66, 81)',
    polar2: 'rgb(67, 76, 93)',
    polar3: 'rgb(76, 86, 105)',

    snow0: 'rgb(216, 222, 233)',
    snow1: 'rgb(229, 233, 240)',
    snow2: 'rgb(236, 239, 244)',

    frost0: 'rgb(144, 188, 187)',
    frost1: 'rgb(138, 192, 207)',
    frost2: 'rgb(130, 161, 192)',
    frost3: 'rgb(95, 130, 170)',

    aurora0: 'rgb(190, 97, 107)',
    aurora1: 'rgb(207, 135, 114)',
    aurora2: 'rgb(234, 202, 143)',
    aurora3: 'rgb(164, 189, 142)',
    aurora4: 'rgb(179, 143, 172)'
};

exports.default = COLORS;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(49);

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkDrop = function (_React$Component) {
    (0, _inherits3.default)(LinkDrop, _React$Component);

    function LinkDrop(props) {
        (0, _classCallCheck3.default)(this, LinkDrop);

        var _this = (0, _possibleConstructorReturn3.default)(this, (LinkDrop.__proto__ || (0, _getPrototypeOf2.default)(LinkDrop)).call(this, props));

        _this._handleOuterClick = function (e) {
            if (_this._elW && !_this._elW.contains(e.target)) {
                _this._el.style.display = 'none';
            }
        };

        _this._toggle = function (e) {
            e.preventDefault();

            if (_this._el.style.display == 'none') {

                _this._el.style.display = 'block';
            } else {
                _this._el.style.display = 'none';
            }
        };

        _this._onSelect = function (value) {
            _this._el.style.display = 'none';
            _this.props.onClick(value);
        };

        return _this;
    }

    (0, _createClass3.default)(LinkDrop, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            document.addEventListener('mousedown', this._handleOuterClick);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            document.removeEventListener('mousedown', this._handleOuterClick);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var label = "No selection";

            if (this.props.value) {
                this.props.options.forEach(function (item) {
                    if (item[0] == _this2.props.value) label = item[1];
                });
            }

            var style = {};
            if (this.props.style) style = (0, _extends3.default)({}, style, this.props.style);

            return React.createElement(
                'div',
                { className: 'link-drop', ref: function ref(el) {
                        _this2._elW = el;
                    }, style: style },
                React.createElement(
                    'a',
                    { href: '#', onClick: this._toggle },
                    label,
                    '\xA0',
                    React.createElement('i', { className: 'fal fa-caret-down' })
                ),
                React.createElement(
                    'div',
                    {
                        ref: function ref(el) {
                            _this2._el = el;
                        },
                        className: 'children',
                        style: { display: 'none' } },
                    React.createElement(
                        'div',
                        { className: 'children-inner' },
                        this.props.options.map(function (item) {
                            return React.createElement(
                                'div',
                                {
                                    onClick: function onClick() {
                                        _this2._onSelect(item[0]);
                                    },
                                    className: 'link-item' },
                                item[1]
                            );
                        })
                    )
                )
            );
        }
    }]);
    return LinkDrop;
}(React.Component);

LinkDrop.defaultProps = {
    value: null,
    options: []
};
exports.default = LinkDrop;

/***/ }),
/* 35 */,
/* 36 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(120), __esModule: true };

/***/ }),
/* 38 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(40)('keys');
var uid = __webpack_require__(26);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(16);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 42 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(13);
var dPs = __webpack_require__(87);
var enumBugKeys = __webpack_require__(44);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(50)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(64).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 44 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(11);


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var LIBRARY = __webpack_require__(29);
var wksExt = __webpack_require__(45);
var defineProperty = __webpack_require__(14).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 47 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.remote_api = exports.api = undefined;

var _stringify = __webpack_require__(37);

var _stringify2 = _interopRequireDefault(_stringify);

var _promise = __webpack_require__(69);

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var api = function api(method, args) {
    return new _promise2.default(function (resolve, reject) {
        var oReq = new XMLHttpRequest();
        // oReq.open("POST", `/api/${method}`, true);
        oReq.open("POST", "/api", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        oReq.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    if (oReq.responseText == "NO_AUTH") {
                        document.location = "/login";
                    }
                    var resp = JSON.parse(oReq.responseText);
                    resolve(resp);
                } else {
                    if (oReq.status == 500) {}

                    if (oReq.status == 101) {}
                    reject(oReq.status);
                }
            }
        }.bind(undefined);

        var _args = args ? args : {};
        oReq.send((0, _stringify2.default)([method, _args]));
    });
};

var remote_api = function remote_api(method, args) {
    return new _promise2.default(function (resolve, reject) {
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "http://127.0.0.1:9000/api/_d", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        console.log(window.app.settings.activeAccount);
        oReq.setRequestHeader("EC_TOKEN", window.app.win.tki);

        oReq.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    if (oReq.responseText == "NO_AUTH") {
                        reject("NO_AUTH");
                    } else {
                        var resp = JSON.parse(oReq.responseText);
                        resolve(resp);
                    }
                } else {
                    reject(oReq.status);
                }
            }
        }.bind(undefined);

        var _args = args ? args : {};
        oReq.send((0, _stringify2.default)([method, args]));
    });
};

exports.default = api;
exports.api = api;
exports.remote_api = remote_api;

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
var document = __webpack_require__(9).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.emit = exports.forget = exports.listen = exports.copy = exports.debounce = exports.uuid = exports.tx = undefined;

var _stringify = __webpack_require__(37);

var _stringify2 = _interopRequireDefault(_stringify);

var _api = __webpack_require__(48);

var _api2 = _interopRequireDefault(_api);

var _debounce = __webpack_require__(135);

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uuid = function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
    });
};

var copy = function copy(data) {
    return JSON.parse((0, _stringify2.default)(data));
};

var _LISTENERS = {};

var listen = function listen(event, callback) {
    if (_LISTENERS[event]) {
        _LISTENERS[event].push(callback);
    } else {
        _LISTENERS[event] = [callback];
    }
};

var emit = function emit(event, data) {
    if (_LISTENERS[event]) {
        _LISTENERS[event].forEach(function (cb) {
            cb(data);
        });
    }
};

var forget = function forget(event, callback) {
    if (_LISTENERS[event]) {
        var rIndex = void 0;
        _LISTENERS[event].forEach(function (cb, index) {
            if (cb == callback) rIndex = index;
        });
        _LISTENERS[event].splice(rIndex, 1);
    }
};

exports.tx = _api2.default;
exports.uuid = uuid;
exports.debounce = _debounce2.default;
exports.copy = copy;
exports.listen = listen;
exports.forget = forget;
exports.emit = emit;

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(17);
var toObject = __webpack_require__(32);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(15) && !__webpack_require__(19)(function () {
  return Object.defineProperty(__webpack_require__(50)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(83);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(93);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(29);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var hide = __webpack_require__(18);
var has = __webpack_require__(17);
var Iterators = __webpack_require__(21);
var $iterCreate = __webpack_require__(86);
var setToStringTag = __webpack_require__(30);
var getPrototypeOf = __webpack_require__(52);
var ITERATOR = __webpack_require__(11)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = (!BUGGY && $native) || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(18);


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(17);
var toIObject = __webpack_require__(20);
var arrayIndexOf = __webpack_require__(88)(false);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(24);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(57);
var hiddenKeys = __webpack_require__(44).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(28);
var createDesc = __webpack_require__(27);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var has = __webpack_require__(17);
var IE8_DOM_DEFINE = __webpack_require__(53);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(15) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 25.4.1.5 NewPromiseCapability(C)
var aFunction = __webpack_require__(36);

function PromiseCapability(C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction(resolve);
  this.reject = aFunction(reject);
}

module.exports.f = function (C) {
  return new PromiseCapability(C);
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(85)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(55)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(42);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(9).document;
module.exports = document && document.documentElement;


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(90);
var global = __webpack_require__(9);
var hide = __webpack_require__(18);
var Iterators = __webpack_require__(21);
var TO_STRING_TAG = __webpack_require__(11)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 66 */
/***/ (function(module, exports) {



/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(12);
var core = __webpack_require__(8);
var fails = __webpack_require__(19);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(81), __esModule: true };

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(121), __esModule: true };

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(24);
var TAG = __webpack_require__(11)('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__(13);
var aFunction = __webpack_require__(36);
var SPECIES = __webpack_require__(11)('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(23);
var invoke = __webpack_require__(128);
var html = __webpack_require__(64);
var cel = __webpack_require__(50);
var global = __webpack_require__(9);
var process = global.process;
var setTask = global.setImmediate;
var clearTask = global.clearImmediate;
var MessageChannel = global.MessageChannel;
var Dispatch = global.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;
var run = function () {
  var id = +this;
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};
var listener = function (event) {
  run.call(event.data);
};
// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!setTask || !clearTask) {
  setTask = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      invoke(typeof fn == 'function' ? fn : Function(fn), args);
    };
    defer(counter);
    return counter;
  };
  clearTask = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (__webpack_require__(24)(process) == 'process') {
    defer = function (id) {
      process.nextTick(ctx(run, id, 1));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(ctx(run, id, 1));
    };
  // Browsers with MessageChannel, includes WebWorkers
  } else if (MessageChannel) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = ctx(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
    defer = function (id) {
      global.postMessage(id + '', '*');
    };
    global.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in cel('script')) {
    defer = function (id) {
      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run.call(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(ctx(run, id, 1), 0);
    };
  }
}
module.exports = {
  set: setTask,
  clear: clearTask
};


/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(13);
var isObject = __webpack_require__(16);
var newPromiseCapability = __webpack_require__(61);

module.exports = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _utils = __webpack_require__(51);

var _fields = __webpack_require__(279);

var FIELD_CMPS = _interopRequireWildcard(_fields);

var _const = __webpack_require__(76);

var _const2 = _interopRequireDefault(_const);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormField = function (_React$Component) {
    (0, _inherits3.default)(FormField, _React$Component);

    function FormField(props) {
        (0, _classCallCheck3.default)(this, FormField);

        var _this = (0, _possibleConstructorReturn3.default)(this, (FormField.__proto__ || (0, _getPrototypeOf2.default)(FormField)).call(this, props));

        _this._onChange = function (e) {
            _this.props.onChange(_this.props.n, e);
        };

        _this.label = _this.props.data.l || 'Unnamed';

        _this.Cmp = null;
        _this._isLayout = false;
        _this._isRaw = false;

        switch (_this.props.data.t) {
            case _const2.default.TEXT:
                _this.Cmp = FIELD_CMPS.FieldText;
                break;
            case _const2.default.NUMERIC:
                _this.Cmp = FIELD_CMPS.FieldNumeric;
                break;
            case _const2.default.SELECT:
                _this.Cmp = FIELD_CMPS.FieldSelect;
                break;
            case _const2.default.WORKFLOW:
                _this.Cmp = FIELD_CMPS.FieldWorkflow;
                break;
            case _const2.default.HBOX:
                _this._isLayout = true;
                _this.Cmp = FIELD_CMPS.FieldHBox;
                break;
            case _const2.default.HBOX:
                _this._isLayout = true;
                _this.Cmp = FIELD_CMPS.FieldVBox;
                break;
            case _const2.default.LABEL:
                _this._isRaw = true;
                _this.Cmp = FIELD_CMPS.FieldLabel;
                break;
            case _const2.default.LOCATION_TYPE:
                _this.Cmp = FIELD_CMPS.FieldLocationType;
                break;
            default:
                _this.Cmp = FIELD_CMPS.FieldText;
                break;
        }

        return _this;
    }

    (0, _createClass3.default)(FormField, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps) {
            if (nextProps.value != this.props.value) return true;
            return false;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            if (this._isRaw) {
                return React.createElement(this.Cmp, { data: this.props.data });
            }

            var children = void 0;
            if (this._isLayout) {
                children = this.props.data.f.map(function (item) {
                    return React.createElement(FormField, {
                        formData: _this2.props.formData,
                        n: item.n,
                        onChange: _this2.props.onChange,
                        data: item });
                });

                return React.createElement(
                    this.Cmp,
                    null,
                    children
                );
            }

            var value = this.props.formData[this.props.data.n] || null;

            var isVisible = true;
            console.log(this.props.data);

            return React.createElement(
                'div',
                { className: 'form-field' },
                !this._isLayout ? React.createElement(
                    'label',
                    { htmlFor: '' },
                    this.label
                ) : null,
                React.createElement(
                    'div',
                    { className: 'form-control' },
                    React.createElement(
                        this.Cmp,
                        {
                            options: this.props.data.o || [],
                            value: value,
                            onChange: this._onChange },
                        children
                    )
                )
            );
        }
    }]);
    return FormField;
}(React.Component);

var Form = function (_React$Component2) {
    (0, _inherits3.default)(Form, _React$Component2);

    function Form(props) {
        (0, _classCallCheck3.default)(this, Form);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (Form.__proto__ || (0, _getPrototypeOf2.default)(Form)).call(this, props));

        _this3._onFieldChange = function (prop, value) {
            console.log(prop, value);
            _this3.props.onChange(prop, value);
        };

        _this3.state = {};
        return _this3;
    }

    (0, _createClass3.default)(Form, [{
        key: 'render',
        value: function render() {
            var _this4 = this;

            var rootFields = this.props.definition.sort(function (a, b) {
                if (a._ > b._) return 1;
                if (a._ < b._) return -1;
                return 0;
            });

            var style = {};
            if (this.props.style) (0, _assign2.default)(style, this.props.style);

            return React.createElement(
                'div',
                { className: 'ide-form', style: style },
                rootFields.map(function (field) {
                    return React.createElement(FormField, {
                        value: _this4.props.data[field.n] || null,
                        formData: _this4.props.data,
                        onChange: _this4._onFieldChange,
                        n: field.n,
                        data: field });
                })
            );
        }
    }]);
    return Form;
}(React.Component);

Form.defaultProps = {
    definition: {},
    onAction: null,
    errorFn: function errorFn() {
        return null;
    },
    data: {}
};
exports.default = Form;

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    TEXT: 0,
    SELECT: 1,
    NUMERIC: 2,
    DATE: 3,
    LOCATION: 4,
    INTERVAL: 5,
    COORDINATE: 6,
    AGE: 7,
    TIME: 8,
    WORKFLOW: 99,
    SWITCH: 100,
    OVERDUE: 101,
    VBOX: 102,
    HBOX: 103,
    LABEL: 104,
    LOCATION_TYPE: 105,
    COLOUR: 106,
    BORDER_STYLE: 107
};

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BaseState = function BaseState() {
    var _this = this;

    (0, _classCallCheck3.default)(this, BaseState);

    this.on = function (event, callback) {
        if (_this._listeners[event]) {
            _this._listeners[event].push(callback);
        } else {
            _this._listeners[event] = [callback];
        }
    };

    this.off = function (event, callback) {
        if (_this._listeners[event]) {
            var rIndex = void 0;
            _this._listeners[event].forEach(function (cb, index) {
                if (cb == callback) rIndex = index;
            });
            _this._listeners[event].splice(rIndex, 1);
        }
    };

    this._emit = function (event) {
        if (_this._listeners[event]) {
            _this._listeners[event].forEach(function (cb) {
                cb();
            });
        }
    };

    this.getStoreableState = function () {};

    this.registerSubState = function (id, state) {
        _this.subStates[id] = state;
    };

    this.getSubState = function (id) {
        if (_this.subStates[id]) return _this.subStates[id];
        return null;
    };

    this.removeSubState = function (id) {
        if (_this.subStates[id]) delete _this.subStates[id];
        return null;
    };

    this._listeners = {};
    this._subStates = {};
}

/**
 * Retrieve state in s storable format, JSON serializable
 */
;

exports.default = BaseState;

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _utils = __webpack_require__(51);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FOOT_ACTIONS = [['fa-step-backward', 'B'], ['fa-fast-backward', 'FB'], ['fa-fast-forward', 'FF'], ['fa-step-forward', 'F']].reverse();

var TableRow = function (_React$Component) {
    (0, _inherits3.default)(TableRow, _React$Component);

    function TableRow(props) {
        (0, _classCallCheck3.default)(this, TableRow);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TableRow.__proto__ || (0, _getPrototypeOf2.default)(TableRow)).call(this, props));

        _this._onDoubleClick = function () {
            _this.props.onAction('DBL_CLICK', _this.props.data);
        };

        return _this;
    }

    (0, _createClass3.default)(TableRow, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            return React.createElement(
                'tr',
                { onDoubleClick: this._onDoubleClick },
                this.props.rowActions ? React.createElement(
                    'td',
                    null,
                    React.createElement(_c2.default, {
                        onAction: function onAction(action) {
                            _this2.props.onAction(action, _this2.props.data);
                        },
                        actions: this.props.rowActions })
                ) : null,
                this.props.columns.map(function (col) {
                    return React.createElement(
                        'td',
                        null,
                        _this2.props.data[col[0]] || ""
                    );
                })
            );
        }
    }]);
    return TableRow;
}(React.Component);

var DataTable = function (_React$Component2) {
    (0, _inherits3.default)(DataTable, _React$Component2);

    function DataTable(props) {
        (0, _classCallCheck3.default)(this, DataTable);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (DataTable.__proto__ || (0, _getPrototypeOf2.default)(DataTable)).call(this, props));

        _this3.query = function (resource, filters, ordering, limit, offset) {
            (0, _utils.tx)(resource, [filters, ordering, limit, offset]).then(function (resp) {
                console.log(resp);
                _this3.setState({
                    data: resp.d,
                    filters: filters,
                    ordering: ordering,
                    limit: limit,
                    offset: offset,
                    resource: resource
                });
            });
        };

        _this3._onAction = function (action, data) {
            switch (action) {
                case 'DBL_CLICK':
                    if (_this3.props.onRowDblClick) {
                        _this3.props.onRowDblClick(data);
                    }
                    break;
                default:
                    _this3.props.onAction(action, data);
                    break;
            }
        };

        _this3.state = {
            resource: props.resource,
            data: [],
            filters: {},
            ordering: {},
            limit: 60,
            offset: 0
        };

        _this3.query(props.resource, props.initialFilters || {}, props.initialOrdering || {}, 60, 0);
        return _this3;
    }

    (0, _createClass3.default)(DataTable, [{
        key: 'render',
        value: function render() {
            var _this4 = this;

            var icon = void 0;

            if (this.props.icon) icon = React.createElement('i', { className: "fal " + this.props.icon });

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        icon,
                        this.props.title
                    ),
                    this.props.actions ? React.createElement(_c2.default, {
                        onAction: this._onAction,
                        actions: this.props.actions,
                        right: true }) : null
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'table',
                        { className: 'data-table' },
                        React.createElement(
                            'thead',
                            null,
                            React.createElement(
                                'tr',
                                null,
                                this.props.rowActions ? React.createElement(
                                    'th',
                                    null,
                                    '\xA0'
                                ) : null,
                                this.props.columns.map(function (item) {
                                    return React.createElement(
                                        'th',
                                        null,
                                        item[1]
                                    );
                                })
                            )
                        ),
                        React.createElement(
                            'tbody',
                            null,
                            this.state.data.map(function (row) {
                                return React.createElement(TableRow, {
                                    data: row,
                                    rowActions: _this4.props.rowActions,
                                    onAction: _this4._onAction,
                                    columns: _this4.props.columns });
                            })
                        )
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderTop: true, addClass: 'tbar' },
                    React.createElement(_c2.default, {
                        right: true,
                        actions: FOOT_ACTIONS,
                        onAction: this._onAction })
                )
            );
        }
    }]);
    return DataTable;
}(React.Component);

DataTable.defaultProps = {
    resource: null,
    columns: [],
    initialOrdering: {},
    initialFilters: {},
    title: 'New Grid',
    icon: null,
    actions: null,
    rowActions: null,
    onRowDblClick: null
};
exports.default = DataTable;

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(80);
module.exports = __webpack_require__(8).Object.getPrototypeOf;


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(32);
var $getPrototypeOf = __webpack_require__(52);

__webpack_require__(67)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(82);
var $Object = __webpack_require__(8).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(15), 'Object', { defineProperty: __webpack_require__(14).f });


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(84), __esModule: true };

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(62);
__webpack_require__(65);
module.exports = __webpack_require__(45).f('iterator');


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var defined = __webpack_require__(38);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(43);
var descriptor = __webpack_require__(27);
var setToStringTag = __webpack_require__(30);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(18)(IteratorPrototype, __webpack_require__(11)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var anObject = __webpack_require__(13);
var getKeys = __webpack_require__(22);

module.exports = __webpack_require__(15) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(20);
var toLength = __webpack_require__(63);
var toAbsoluteIndex = __webpack_require__(89);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(91);
var step = __webpack_require__(92);
var Iterators = __webpack_require__(21);
var toIObject = __webpack_require__(20);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(55)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 91 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 92 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(94), __esModule: true };

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(95);
__webpack_require__(66);
__webpack_require__(100);
__webpack_require__(101);
module.exports = __webpack_require__(8).Symbol;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(9);
var has = __webpack_require__(17);
var DESCRIPTORS = __webpack_require__(15);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var META = __webpack_require__(96).KEY;
var $fails = __webpack_require__(19);
var shared = __webpack_require__(40);
var setToStringTag = __webpack_require__(30);
var uid = __webpack_require__(26);
var wks = __webpack_require__(11);
var wksExt = __webpack_require__(45);
var wksDefine = __webpack_require__(46);
var enumKeys = __webpack_require__(97);
var isArray = __webpack_require__(98);
var anObject = __webpack_require__(13);
var isObject = __webpack_require__(16);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var createDesc = __webpack_require__(27);
var _create = __webpack_require__(43);
var gOPNExt = __webpack_require__(99);
var $GOPD = __webpack_require__(60);
var $DP = __webpack_require__(14);
var $keys = __webpack_require__(22);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(59).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(28).f = $propertyIsEnumerable;
  __webpack_require__(47).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(29)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(18)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(26)('meta');
var isObject = __webpack_require__(16);
var has = __webpack_require__(17);
var setDesc = __webpack_require__(14).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(19)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(24);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(20);
var gOPN = __webpack_require__(59).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('asyncIterator');


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('observable');


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(103), __esModule: true };

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(104);
module.exports = __webpack_require__(8).Object.setPrototypeOf;


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(12);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(105).set });


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(16);
var anObject = __webpack_require__(13);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(23)(Function.call, __webpack_require__(60).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(107), __esModule: true };

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(108);
var $Object = __webpack_require__(8).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(43) });


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Hbox = function (_React$Component) {
    (0, _inherits3.default)(Hbox, _React$Component);

    function Hbox(props) {
        (0, _classCallCheck3.default)(this, Hbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Hbox.__proto__ || (0, _getPrototypeOf2.default)(Hbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Hbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxHeight = this.props.height || null;
            style.padding = this.props.padding || null;

            var className = "hbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Hbox;
}(React.Component);

Hbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Hbox;

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(111);
module.exports = __webpack_require__(8).Object.assign;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(12);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(112) });


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
var toObject = __webpack_require__(32);
var IObject = __webpack_require__(58);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(19)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Vbox = function (_React$Component) {
    (0, _inherits3.default)(Vbox, _React$Component);

    function Vbox(props) {
        (0, _classCallCheck3.default)(this, Vbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Vbox.__proto__ || (0, _getPrototypeOf2.default)(Vbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Vbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxWidth = this.props.width || null;
            style.padding = this.props.padding || null;

            var className = "vbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Vbox;
}(React.Component);

Vbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Vbox;

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Panel = function (_React$Component) {
    (0, _inherits3.default)(Panel, _React$Component);

    function Panel(props) {
        (0, _classCallCheck3.default)(this, Panel);
        return (0, _possibleConstructorReturn3.default)(this, (Panel.__proto__ || (0, _getPrototypeOf2.default)(Panel)).call(this, props));
    }

    (0, _createClass3.default)(Panel, [{
        key: "render",
        value: function render() {
            var style = {};
            if (this.props.style) {
                for (var i in this.props.style) {
                    style[i] = this.props.style[i];
                }
            }
            return React.createElement(
                "div",
                { className: "ide-panel", style: style },
                this.props.children
            );
        }
    }]);
    return Panel;
}(React.Component);

exports.default = Panel;

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Block = function (_React$Component) {
    (0, _inherits3.default)(Block, _React$Component);

    function Block(props) {
        (0, _classCallCheck3.default)(this, Block);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Block.__proto__ || (0, _getPrototypeOf2.default)(Block)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Block, [{
        key: "render",
        value: function render() {
            var style = {};
            style.maxHeight = this.props.height || null;
            style.maxWidth = this.props.width || null;

            style.padding = this.props.padding || null;

            var className = "block";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";
            className += this.props.yO ? " overY " : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            var click = void 0;
            if (this.props.onClick) click = this._onClick;

            return React.createElement(
                "div",
                { className: className, style: style, onClick: click, id: this.props.id },
                this.props.children
            );
        }
    }]);
    return Block;
}(React.Component);

Block.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {},
    yO: false,
    id: null
};
exports.default = Block;

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tab = function (_React$Component) {
    (0, _inherits3.default)(Tab, _React$Component);

    function Tab(props) {
        (0, _classCallCheck3.default)(this, Tab);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Tab.__proto__ || (0, _getPrototypeOf2.default)(Tab)).call(this, props));

        _this._setTab = function () {
            _this.props.onClick(_this.props.data);
        };

        return _this;
    }

    (0, _createClass3.default)(Tab, [{
        key: 'render',
        value: function render() {
            var className = "ind-tab";
            if (this.props.active) className += ' active';

            return React.createElement(
                'div',
                { className: className, onClick: this._setTab },
                React.createElement(
                    _layout.Hbox,
                    { onClick: this._setTab },
                    React.createElement(
                        _layout.Block,
                        { width: '20px', addClass: 'icon' },
                        React.createElement('i', { className: "fal " + this.props.icon })
                    ),
                    this.props.label ? React.createElement(
                        _layout.Block,
                        null,
                        this.props.label
                    ) : null
                )
            );
        }
    }]);
    return Tab;
}(React.Component);

exports.default = Tab;

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var ROLES = {
    USER: "Reporting user",
    ACCOUNT_ADMIN: "Account admin",
    REGIONAL_ADMIN: "Geo. admin"
};

exports.default = ROLES;

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var DASH_ACTIONS = [['fa-pencil', 'EDIT'], ['fa-copy', 'COPY'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']].reverse();

var DashboardItem = function (_React$Component) {
    (0, _inherits3.default)(DashboardItem, _React$Component);

    function DashboardItem(props) {
        (0, _classCallCheck3.default)(this, DashboardItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (DashboardItem.__proto__ || (0, _getPrototypeOf2.default)(DashboardItem)).call(this, props));

        _this._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'Dashboard',
                        i: data.uuid,
                        _: 'DASHBOARD'
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(DashboardItem, [{
        key: 'render',
        value: function render() {

            var iconStyle = { color: _const2.default.aurora3 };

            return React.createElement(
                'div',
                { className: 'dash-item' },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { width: '25px', addClass: 'text-block', style: { textIndent: '5px' } },
                        React.createElement('i', { className: 'fal fa-dot-circle', style: iconStyle })
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'text-block' },
                        'Dashboard Name [User, Account Admin]'
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(_c2.default, {
                            right: true,
                            actions: DASH_ACTIONS,
                            onAction: this._action })
                    )
                )
            );
        }
    }]);
    return DashboardItem;
}(React.Component);

var ViewDashboards = function (_React$Component2) {
    (0, _inherits3.default)(ViewDashboards, _React$Component2);

    function ViewDashboards(props) {
        (0, _classCallCheck3.default)(this, ViewDashboards);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (ViewDashboards.__proto__ || (0, _getPrototypeOf2.default)(ViewDashboards)).call(this, props));

        _this2._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'Dashboard',
                        i: null,
                        _: 'DASHBOARD'
                    });
                    break;
                default:
                    break;
            }
        };

        return _this2;
    }

    (0, _createClass3.default)(ViewDashboards, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { addClass: 'tbar', height: '30px', borderBottom: true },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-tachometer' }),
                        '\xA0Dashboards'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        onAction: this._action,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { style: { padding: '16px' } },
                    React.createElement(DashboardItem, null)
                )
            );
        }
    }]);
    return ViewDashboards;
}(React.Component);

exports.default = ViewDashboards;

/***/ }),
/* 119 */,
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(8);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(66);
__webpack_require__(62);
__webpack_require__(65);
__webpack_require__(122);
__webpack_require__(133);
__webpack_require__(134);
module.exports = __webpack_require__(8).Promise;


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(29);
var global = __webpack_require__(9);
var ctx = __webpack_require__(23);
var classof = __webpack_require__(70);
var $export = __webpack_require__(12);
var isObject = __webpack_require__(16);
var aFunction = __webpack_require__(36);
var anInstance = __webpack_require__(123);
var forOf = __webpack_require__(124);
var speciesConstructor = __webpack_require__(71);
var task = __webpack_require__(72).set;
var microtask = __webpack_require__(129)();
var newPromiseCapabilityModule = __webpack_require__(61);
var perform = __webpack_require__(73);
var promiseResolve = __webpack_require__(74);
var PROMISE = 'Promise';
var TypeError = global.TypeError;
var process = global.process;
var $Promise = global[PROMISE];
var isNode = classof(process) == 'process';
var empty = function () { /* empty */ };
var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

var USE_NATIVE = !!function () {
  try {
    // correct subclassing with @@species support
    var promise = $Promise.resolve(1);
    var FakePromise = (promise.constructor = {})[__webpack_require__(11)('species')] = function (exec) {
      exec(empty, empty);
    };
    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    return (isNode || typeof PromiseRejectionEvent == 'function') && promise.then(empty) instanceof FakePromise;
  } catch (e) { /* empty */ }
}();

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};
var notify = function (promise, isReject) {
  if (promise._n) return;
  promise._n = true;
  var chain = promise._c;
  microtask(function () {
    var value = promise._v;
    var ok = promise._s == 1;
    var i = 0;
    var run = function (reaction) {
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then;
      try {
        if (handler) {
          if (!ok) {
            if (promise._h == 2) onHandleUnhandled(promise);
            promise._h = 1;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value);
            if (domain) domain.exit();
          }
          if (result === reaction.promise) {
            reject(TypeError('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (e) {
        reject(e);
      }
    };
    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
    promise._c = [];
    promise._n = false;
    if (isReject && !promise._h) onUnhandled(promise);
  });
};
var onUnhandled = function (promise) {
  task.call(global, function () {
    var value = promise._v;
    var unhandled = isUnhandled(promise);
    var result, handler, console;
    if (unhandled) {
      result = perform(function () {
        if (isNode) {
          process.emit('unhandledRejection', value, promise);
        } else if (handler = global.onunhandledrejection) {
          handler({ promise: promise, reason: value });
        } else if ((console = global.console) && console.error) {
          console.error('Unhandled promise rejection', value);
        }
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
    } promise._a = undefined;
    if (unhandled && result.e) throw result.v;
  });
};
var isUnhandled = function (promise) {
  return promise._h !== 1 && (promise._a || promise._c).length === 0;
};
var onHandleUnhandled = function (promise) {
  task.call(global, function () {
    var handler;
    if (isNode) {
      process.emit('rejectionHandled', promise);
    } else if (handler = global.onrejectionhandled) {
      handler({ promise: promise, reason: promise._v });
    }
  });
};
var $reject = function (value) {
  var promise = this;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  promise._v = value;
  promise._s = 2;
  if (!promise._a) promise._a = promise._c.slice();
  notify(promise, true);
};
var $resolve = function (value) {
  var promise = this;
  var then;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  try {
    if (promise === value) throw TypeError("Promise can't be resolved itself");
    if (then = isThenable(value)) {
      microtask(function () {
        var wrapper = { _w: promise, _d: false }; // wrap
        try {
          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
        } catch (e) {
          $reject.call(wrapper, e);
        }
      });
    } else {
      promise._v = value;
      promise._s = 1;
      notify(promise, false);
    }
  } catch (e) {
    $reject.call({ _w: promise, _d: false }, e); // wrap
  }
};

// constructor polyfill
if (!USE_NATIVE) {
  // 25.4.3.1 Promise(executor)
  $Promise = function Promise(executor) {
    anInstance(this, $Promise, PROMISE, '_h');
    aFunction(executor);
    Internal.call(this);
    try {
      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
    } catch (err) {
      $reject.call(this, err);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    this._c = [];             // <- awaiting reactions
    this._a = undefined;      // <- checked in isUnhandled reactions
    this._s = 0;              // <- state
    this._d = false;          // <- done
    this._v = undefined;      // <- value
    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
    this._n = false;          // <- notify
  };
  Internal.prototype = __webpack_require__(130)($Promise.prototype, {
    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
    then: function then(onFulfilled, onRejected) {
      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = isNode ? process.domain : undefined;
      this._c.push(reaction);
      if (this._a) this._a.push(reaction);
      if (this._s) notify(this, false);
      return reaction.promise;
    },
    // 25.4.5.1 Promise.prototype.catch(onRejected)
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    this.promise = promise;
    this.resolve = ctx($resolve, promise, 1);
    this.reject = ctx($reject, promise, 1);
  };
  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
    return C === $Promise || C === Wrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
__webpack_require__(30)($Promise, PROMISE);
__webpack_require__(131)(PROMISE);
Wrapper = __webpack_require__(8)[PROMISE];

// statics
$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
  // 25.4.4.5 Promise.reject(r)
  reject: function reject(r) {
    var capability = newPromiseCapability(this);
    var $$reject = capability.reject;
    $$reject(r);
    return capability.promise;
  }
});
$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
  // 25.4.4.6 Promise.resolve(x)
  resolve: function resolve(x) {
    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
  }
});
$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(132)(function (iter) {
  $Promise.all(iter)['catch'](empty);
})), PROMISE, {
  // 25.4.4.1 Promise.all(iterable)
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var values = [];
      var index = 0;
      var remaining = 1;
      forOf(iterable, false, function (promise) {
        var $index = index++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        C.resolve(promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[$index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.e) reject(result.v);
    return capability.promise;
  },
  // 25.4.4.4 Promise.race(iterable)
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var reject = capability.reject;
    var result = perform(function () {
      forOf(iterable, false, function (promise) {
        C.resolve(promise).then(capability.resolve, reject);
      });
    });
    if (result.e) reject(result.v);
    return capability.promise;
  }
});


/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(23);
var call = __webpack_require__(125);
var isArrayIter = __webpack_require__(126);
var anObject = __webpack_require__(13);
var toLength = __webpack_require__(63);
var getIterFn = __webpack_require__(127);
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(13);
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__(21);
var ITERATOR = __webpack_require__(11)('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(70);
var ITERATOR = __webpack_require__(11)('iterator');
var Iterators = __webpack_require__(21);
module.exports = __webpack_require__(8).getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),
/* 128 */
/***/ (function(module, exports) {

// fast apply, http://jsperf.lnkit.com/fast-apply/5
module.exports = function (fn, args, that) {
  var un = that === undefined;
  switch (args.length) {
    case 0: return un ? fn()
                      : fn.call(that);
    case 1: return un ? fn(args[0])
                      : fn.call(that, args[0]);
    case 2: return un ? fn(args[0], args[1])
                      : fn.call(that, args[0], args[1]);
    case 3: return un ? fn(args[0], args[1], args[2])
                      : fn.call(that, args[0], args[1], args[2]);
    case 4: return un ? fn(args[0], args[1], args[2], args[3])
                      : fn.call(that, args[0], args[1], args[2], args[3]);
  } return fn.apply(that, args);
};


/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var macrotask = __webpack_require__(72).set;
var Observer = global.MutationObserver || global.WebKitMutationObserver;
var process = global.process;
var Promise = global.Promise;
var isNode = __webpack_require__(24)(process) == 'process';

module.exports = function () {
  var head, last, notify;

  var flush = function () {
    var parent, fn;
    if (isNode && (parent = process.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (e) {
        if (head) notify();
        else last = undefined;
        throw e;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // Node.js
  if (isNode) {
    notify = function () {
      process.nextTick(flush);
    };
  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
    var toggle = true;
    var node = document.createTextNode('');
    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise && Promise.resolve) {
    var promise = Promise.resolve();
    notify = function () {
      promise.then(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global, flush);
    };
  }

  return function (fn) {
    var task = { fn: fn, next: undefined };
    if (last) last.next = task;
    if (!head) {
      head = task;
      notify();
    } last = task;
  };
};


/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

var hide = __webpack_require__(18);
module.exports = function (target, src, safe) {
  for (var key in src) {
    if (safe && target[key]) target[key] = src[key];
    else hide(target, key, src[key]);
  } return target;
};


/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var dP = __webpack_require__(14);
var DESCRIPTORS = __webpack_require__(15);
var SPECIES = __webpack_require__(11)('species');

module.exports = function (KEY) {
  var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__(11)('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// https://github.com/tc39/proposal-promise-finally

var $export = __webpack_require__(12);
var core = __webpack_require__(8);
var global = __webpack_require__(9);
var speciesConstructor = __webpack_require__(71);
var promiseResolve = __webpack_require__(74);

$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
  var C = speciesConstructor(this, core.Promise || global.Promise);
  var isFunction = typeof onFinally == 'function';
  return this.then(
    isFunction ? function (x) {
      return promiseResolve(C, onFinally()).then(function () { return x; });
    } : onFinally,
    isFunction ? function (e) {
      return promiseResolve(C, onFinally()).then(function () { throw e; });
    } : onFinally
  );
} });


/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// https://github.com/tc39/proposal-promise-try
var $export = __webpack_require__(12);
var newPromiseCapability = __webpack_require__(61);
var perform = __webpack_require__(73);

$export($export.S, 'Promise', { 'try': function (callbackfn) {
  var promiseCapability = newPromiseCapability.f(this);
  var result = perform(callbackfn);
  (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
  return promiseCapability.promise;
} });


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

exports.default = debounce;

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.LocationsState = exports.component_state = undefined;

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _state = __webpack_require__(77);

var _state2 = _interopRequireDefault(_state);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

var _state3 = __webpack_require__(31);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var component_state = void 0;

var LocationsState = function (_BaseState) {
    (0, _inherits3.default)(LocationsState, _BaseState);

    function LocationsState() {
        (0, _classCallCheck3.default)(this, LocationsState);

        var _this = (0, _possibleConstructorReturn3.default)(this, (LocationsState.__proto__ || (0, _getPrototypeOf2.default)(LocationsState)).call(this));

        _this.getLocations = function (pid) {};

        _this.destroy = function () {
            _state3.state.removeSubState('LOCATIONS');
            exports.component_state = component_state = null;
        };

        _this.editLocation = function (data) {
            _this._location = utils.copy(data);
            _this.location = utils.copy(data);
            _this._emit("VIEW_CHANGE");
        };

        _this.setExpanded = function (uuid) {
            if (_this.expanded.indexOf(uuid) >= 0) {
                _this.expanded.splice(_this.expanded.indexOf(uuid), 1);
            } else {
                _this.expanded.push(uuid);
            }
        };

        _this.locations = [];
        _this.viewTree = 'HIERARCH';
        _this._location = null;
        _this.location = null;
        _this.locationErrors = {};
        _this.expanded = [];
        _this.checked = [];

        exports.component_state = component_state = _this;

        // Register the state with the global state
        _state3.state.registerSubState('LOCATIONS', _this);
        return _this;
    }

    return LocationsState;
}(_state2.default);

exports.component_state = component_state;
exports.LocationsState = LocationsState;
exports.default = LocationsState;

/***/ }),
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PermitSwitch = function (_React$Component) {
    (0, _inherits3.default)(PermitSwitch, _React$Component);

    function PermitSwitch(props) {
        (0, _classCallCheck3.default)(this, PermitSwitch);
        return (0, _possibleConstructorReturn3.default)(this, (PermitSwitch.__proto__ || (0, _getPrototypeOf2.default)(PermitSwitch)).call(this, props));
    }

    (0, _createClass3.default)(PermitSwitch, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "permit-switch" },
                React.createElement("i", { className: "fal fa-check-square" })
            );
        }
    }]);
    return PermitSwitch;
}(React.Component);

var PermissionsEditor = function (_React$Component2) {
    (0, _inherits3.default)(PermissionsEditor, _React$Component2);

    function PermissionsEditor(props) {
        (0, _classCallCheck3.default)(this, PermissionsEditor);
        return (0, _possibleConstructorReturn3.default)(this, (PermissionsEditor.__proto__ || (0, _getPrototypeOf2.default)(PermissionsEditor)).call(this, props));
    }

    (0, _createClass3.default)(PermissionsEditor, [{
        key: "render",
        value: function render() {
            return React.createElement(
                _layout.Block,
                { style: { padding: '8px' } },
                React.createElement(
                    "table",
                    { className: "permissions-editor" },
                    React.createElement(
                        "thead",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "th",
                                null,
                                "Role"
                            ),
                            this.props.definition.map(function (item) {
                                return React.createElement(
                                    "th",
                                    null,
                                    item.l
                                );
                            })
                        )
                    ),
                    React.createElement(
                        "tbody",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "th",
                                null,
                                "User"
                            ),
                            this.props.definition.map(function (item) {
                                return React.createElement(
                                    "td",
                                    null,
                                    React.createElement(PermitSwitch, null)
                                );
                            })
                        )
                    )
                )
            );
        }
    }]);
    return PermissionsEditor;
}(React.Component);

PermissionsEditor.defaultProps = {
    definition: []
};
exports.default = PermissionsEditor;

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _state = __webpack_require__(77);

var _state2 = _interopRequireDefault(_state);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldState = function (_BaseState) {
    (0, _inherits3.default)(FieldState, _BaseState);

    function FieldState(props) {
        (0, _classCallCheck3.default)(this, FieldState);
        return (0, _possibleConstructorReturn3.default)(this, (FieldState.__proto__ || (0, _getPrototypeOf2.default)(FieldState)).call(this, props));
    }

    return FieldState;
}(_state2.default);

var FormDefinitionState = function (_BaseState2) {
    (0, _inherits3.default)(FormDefinitionState, _BaseState2);

    function FormDefinitionState(props) {
        (0, _classCallCheck3.default)(this, FormDefinitionState);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (FormDefinitionState.__proto__ || (0, _getPrototypeOf2.default)(FormDefinitionState)).call(this, props));

        _this2.setView = function (view) {
            _this2.view = view;
            _this2._emit('on_view_changed');
        };

        _this2.addRootField = function (field) {
            _this2.definition.push({
                _: utils.uuid(),
                t: field,
                l: 'New Field'
            });
            _this2._emit('on_structure_change');
        };

        _this2.addRelative = function (neighbour, data) {
            if (!neighbour) {
                _this2.definition.push({
                    _: utils.uuid(),
                    t: data,
                    l: 'New Field'
                });
            } else {
                var insertion = void 0;
                _this2.definition.forEach(function (field, index) {
                    if (field._ == neighbour) {
                        insertion = index;
                    }
                });
                _this2.definition.splice(insertion, 0, {
                    _: utils.uuid(),
                    t: data,
                    l: 'New Field'
                });
            }
            _this2._emit('on_structure_change');
        };

        _this2.removeRootField = function (field_id) {};

        _this2.repositionField = function (field_id, index, pos) {};

        _this2.definition = [];
        _this2.view = 'EDITOR';
        return _this2;
    }

    /**
     *
     * @param field_id The field id to reposition
     * @param index The index to reposition relative to
     * @param pos The direction BEFORE or AFTER
     */


    return FormDefinitionState;
}(_state2.default);

exports.default = FormDefinitionState;

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var LOCATION_FORM = [{
    _: -1,
    l: 'General',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-cog',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 0,
    l: 'Name',
    n: 'name',
    t: _constants.FIELDS.TEXT
}, {
    _: 0.1,
    l: 'Alternate names',
    n: 'alt_names',
    t: _constants.FIELDS.OPTIONS
}, {
    _: 1,
    l: 'Status',
    n: 'status',
    t: _constants.FIELDS.LOCATION_STATUS
}, {
    _: 1.1,
    l: 'Parent location',
    n: 'pid',
    t: _constants.FIELDS.LOCATION
}, {
    _: 2,
    l: 'Location type',
    n: 'lti',
    t: _constants.FIELDS.LOCATION_TYPE
}, {
    _: 2.1,
    l: 'Identification',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-tag',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 3,
    l: 'Codes',
    n: 'codes',
    t: _constants.FIELDS.LOCATION_CODES
}, {
    _: 4,
    l: 'Group(s)',
    n: 'groups',
    t: _constants.FIELDS.LOCATION_GROUPS
}, {
    _: 5,
    l: 'Mapping',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-map',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 6,
    l: 'Geometry',
    t: _constants.FIELDS.LOCATION_GEOM,
    n: 'geom'
}, {
    _: 7,
    l: 'Point',
    t: _constants.FIELDS.LOCATION_POINT,
    n: 'point'
}];

exports.default = LOCATION_FORM;

/***/ }),
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _view = __webpack_require__(274);

var _view2 = _interopRequireDefault(_view);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

ReactDOM.render(React.createElement(_view2.default, null), document.getElementById("app"));

/***/ }),
/* 274 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(275);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

var _tab = __webpack_require__(276);

var _tab2 = _interopRequireDefault(_tab);

var _tab3 = __webpack_require__(297);

var _tab4 = _interopRequireDefault(_tab3);

var _tab5 = __webpack_require__(298);

var _tab6 = _interopRequireDefault(_tab5);

var _tab7 = __webpack_require__(305);

var _tab8 = _interopRequireDefault(_tab7);

var _tab9 = __webpack_require__(306);

var _tab10 = _interopRequireDefault(_tab9);

var _tab11 = __webpack_require__(307);

var _tab12 = _interopRequireDefault(_tab11);

var _tab13 = __webpack_require__(308);

var _tab14 = _interopRequireDefault(_tab13);

var _view = __webpack_require__(309);

var _view2 = _interopRequireDefault(_view);

var _tab15 = __webpack_require__(334);

var _tab16 = _interopRequireDefault(_tab15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TABS = {
    FORM: _tab2.default,
    FEED: _tab4.default,
    LOCATIONS: _tab16.default,
    DASHBOARD: _tab6.default,
    WORKFLOW: _tab8.default,
    ALARM: _tab10.default,
    COMPONENT: _tab12.default,
    WIKI: _tab14.default
};

var ViewMain = function (_React$Component) {
    (0, _inherits3.default)(ViewMain, _React$Component);

    function ViewMain(props) {
        (0, _classCallCheck3.default)(this, ViewMain);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewMain.__proto__ || (0, _getPrototypeOf2.default)(ViewMain)).call(this, props));

        _this._onChange = function () {
            _this.setState(_this.state);
        };

        _this.state = new _state.ComponentState();

        _state.state.subscribe('VIEW_UPDATED', _this._onChange);
        return _this;
    }

    (0, _createClass3.default)(ViewMain, [{
        key: 'render',
        value: function render() {
            var view = void 0;

            if (_state.state.activeTab != 'DEFAULT') {
                var tab = _state.state.getActiveTab();
                if (TABS[tab.cmp]) {
                    var Cmp = TABS[tab.cmp];
                    view = React.createElement(Cmp, { sid: tab._ });
                }
                console.log(tab);
            } else {
                view = React.createElement(_view2.default, null);
            }

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Hbox,
                    { height: '30px', borderBottom: true },
                    React.createElement(_c2.default, null)
                ),
                view
            );
        }
    }]);
    return ViewMain;
}(React.Component);

exports.default = ViewMain;

/***/ }),
/* 275 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TAB_NAMES = {
    FORM: 'Form: ',
    DEFAULT: ''
};

var TabItem = function (_React$Component) {
    (0, _inherits3.default)(TabItem, _React$Component);

    function TabItem(props) {
        (0, _classCallCheck3.default)(this, TabItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabItem.__proto__ || (0, _getPrototypeOf2.default)(TabItem)).call(this, props));

        _this._setTab = function () {
            _state.state.setActiveTab(_this.props.data._);
        };

        _this._removeTab = function (e) {
            if (e) e.preventDefault();
            _state.state.removeTab(_this.props.data._);
        };

        return _this;
    }

    (0, _createClass3.default)(TabItem, [{
        key: 'render',
        value: function render() {
            var icon = void 0;

            if (this.props.data.i) icon = React.createElement('i', { className: 'fal ' + this.props.data.i });

            var label = this.props.data.n || 'Unnamed';
            if (TAB_NAMES[label]) label = TAB_NAMES[label];

            var className = "tab";

            if (_state.state.activeTab == this.props.data._) className += ' active';

            var closeable = true;
            if (this.props.data.n == 'DEFAULT') closeable = false;

            return React.createElement(
                'div',
                { className: className, onClick: this._setTab },
                React.createElement(
                    _layout.Hbox,
                    null,
                    icon ? React.createElement(
                        _layout.Block,
                        {
                            onClick: this._setTab,
                            width: '20px',
                            style: { paddingRight: '8px', paddingTop: '5px' } },
                        icon
                    ) : null,
                    React.createElement(
                        _layout.Block,
                        {
                            onClick: this._setTab,
                            style: { paddingRight: '8px', paddingTop: '5px' } },
                        label
                    ),
                    closeable ? React.createElement(
                        _layout.Block,
                        { width: '20px', style: { textAlign: 'center', paddingTop: '5px' }, onClick: this._removeTab },
                        React.createElement('i', { className: 'fal fa-times hover-red' })
                    ) : null
                )
            );
        }
    }]);
    return TabItem;
}(React.Component);

var Tabs = function (_React$Component2) {
    (0, _inherits3.default)(Tabs, _React$Component2);

    function Tabs(props) {
        (0, _classCallCheck3.default)(this, Tabs);
        return (0, _possibleConstructorReturn3.default)(this, (Tabs.__proto__ || (0, _getPrototypeOf2.default)(Tabs)).call(this, props));
    }

    (0, _createClass3.default)(Tabs, [{
        key: 'render',
        value: function render() {

            var tabs = [{
                n: 'DEFAULT',
                i: 'fa-tachometer',
                _: 'DEFAULT'
            }];

            _state.state.tabs.forEach(function (tab) {
                tabs.push(tab);
            });

            return React.createElement(
                'div',
                { className: 'tab-bar' },
                tabs.map(function (item) {
                    return React.createElement(TabItem, { data: item });
                })
            );
        }
    }]);
    return Tabs;
}(React.Component);

exports.default = Tabs;

/***/ }),
/* 276 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

var _viewForm = __webpack_require__(277);

var _viewForm2 = _interopRequireDefault(_viewForm);

var _c5 = __webpack_require__(141);

var _c6 = _interopRequireDefault(_c5);

var _state = __webpack_require__(290);

var _state2 = _interopRequireDefault(_state);

var _form = __webpack_require__(291);

var _state3 = __webpack_require__(31);

var _constForm = __webpack_require__(296);

var _constForm2 = _interopRequireDefault(_constForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE'], ['fa-times', 'CLOSE'], ['fa-ellipsis-v', 'NOOP1'], ['fa-trash', 'DELETE']].reverse();

var TabForm = function (_React$Component) {
    (0, _inherits3.default)(TabForm, _React$Component);

    function TabForm(props) {
        (0, _classCallCheck3.default)(this, TabForm);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabForm.__proto__ || (0, _getPrototypeOf2.default)(TabForm)).call(this, props));

        _this._onChange = function () {
            _this.setState(_state3.state.getSubState(_this.props.sid));
        };

        if (_state3.state.getSubState(props.sid)) {
            _this.state = _state3.state.getSubState(props.sid);
        } else {
            var newState = new _state2.default();
            _this.state = newState;
            _state3.state.registerSubState(props.sid, newState);
        }

        return _this;
    }

    (0, _createClass3.default)(TabForm, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.state.on("on_view_changed", this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.state.off("on_view_changed", this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var view = void 0;

            if (this.state.view == 'DEFAULT') view = React.createElement(_viewForm2.default, { sid: this.props.sid });
            if (this.state.view == 'DEFINITION') view = React.createElement(_form.EditorFormDefinition, { sid: this.props.sid });
            if (this.state.view == 'PERMISSIONS') view = React.createElement(_c6.default, { definition: _constForm2.default });

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', addClass: 'tbar', borderBottom: true },
                    React.createElement(_c4.default, {
                        active: this.state.view == 'DEFAULT',
                        label: 'Settings',
                        onClick: function onClick(data) {
                            _this2.state.setView(data);
                        },
                        data: 'DEFAULT',
                        icon: 'fa-cog' }),
                    React.createElement(_c4.default, {
                        active: this.state.view == 'DEFINITION',
                        onClick: function onClick(data) {
                            _this2.state.setView(data);
                        },
                        label: 'Definition',
                        data: 'DEFINITION',
                        icon: 'fa-clipboard' }),
                    React.createElement(_c4.default, {
                        active: this.state.view == 'PERMISSIONS',
                        label: 'Permissions',
                        onClick: function onClick(data) {
                            _this2.state.setView(data);
                        },
                        data: 'PERMISSIONS',
                        icon: 'fa-lock' }),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    view
                )
            );
        }
    }]);
    return TabForm;
}(React.Component);

exports.default = TabForm;

/***/ }),
/* 277 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _viewForm = __webpack_require__(278);

var _viewForm2 = _interopRequireDefault(_viewForm);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewFormSettings = function (_React$Component) {
    (0, _inherits3.default)(ViewFormSettings, _React$Component);

    function ViewFormSettings(props) {
        (0, _classCallCheck3.default)(this, ViewFormSettings);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewFormSettings.__proto__ || (0, _getPrototypeOf2.default)(ViewFormSettings)).call(this, props));

        _this._onChange = function () {
            _this.setState(_this.state);
        };

        _this.state = _state.state.getSubState(props.sid);
        return _this;
    }

    (0, _createClass3.default)(ViewFormSettings, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.state.on('on_settings_view_change', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.state.off('on_settings_view_change', this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(_viewForm2.default, null),
                React.createElement(
                    _layout.Block,
                    { width: '33%', borderLeft: true, addClass: 'noise' },
                    React.createElement(
                        'div',
                        { className: 'guide' },
                        React.createElement(
                            'h1',
                            null,
                            'Guidance'
                        ),
                        React.createElement(
                            'a',
                            { href: '/guidance/admin/forms' },
                            'Guidance: Forms'
                        ),
                        React.createElement(
                            'h1',
                            null,
                            'Form Statistics'
                        ),
                        React.createElement(
                            'dl',
                            null,
                            React.createElement(
                                'dt',
                                null,
                                'Records'
                            ),
                            React.createElement(
                                'dd',
                                null,
                                '10,000,00'
                            )
                        ),
                        React.createElement(
                            'h1',
                            null,
                            'Administration'
                        ),
                        React.createElement(
                            'p',
                            null,
                            'Perform administrative actions on this form and it\'s data.'
                        ),
                        React.createElement(
                            'button',
                            null,
                            'Purge records'
                        ),
                        React.createElement(
                            'button',
                            null,
                            'Remap data'
                        )
                    )
                )
            );
        }
    }]);
    return ViewFormSettings;
}(React.Component);

exports.default = ViewFormSettings;

/***/ }),
/* 278 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(75);

var _c2 = _interopRequireDefault(_c);

var _form = __webpack_require__(289);

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewFormGeneral = function (_React$Component) {
    (0, _inherits3.default)(ViewFormGeneral, _React$Component);

    function ViewFormGeneral(props) {
        (0, _classCallCheck3.default)(this, ViewFormGeneral);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewFormGeneral.__proto__ || (0, _getPrototypeOf2.default)(ViewFormGeneral)).call(this, props));

        _this._onChange = function (prop, value) {
            console.log(prop, value);
        };

        return _this;
    }

    (0, _createClass3.default)(ViewFormGeneral, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Block,
                { yO: true },
                React.createElement(_c2.default, {
                    style: { padding: '8px' },
                    onChange: this._onChange,
                    definition: _form2.default })
            );
        }
    }]);
    return ViewFormGeneral;
}(React.Component);

exports.default = ViewFormGeneral;

/***/ }),
/* 279 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.FieldLabel = exports.FieldVBox = exports.FieldHBox = exports.FieldLocationType = exports.FieldWorkflow = exports.FieldDate = exports.FieldNumeric = exports.FieldSelect = exports.FieldText = undefined;

var _f = __webpack_require__(280);

var _f2 = _interopRequireDefault(_f);

var _f3 = __webpack_require__(281);

var _f4 = _interopRequireDefault(_f3);

var _f5 = __webpack_require__(282);

var _f6 = _interopRequireDefault(_f5);

var _f7 = __webpack_require__(283);

var _f8 = _interopRequireDefault(_f7);

var _sf = __webpack_require__(284);

var _sf2 = _interopRequireDefault(_sf);

var _sfLocation = __webpack_require__(285);

var _sfLocation2 = _interopRequireDefault(_sfLocation);

var _fl = __webpack_require__(286);

var _fl2 = _interopRequireDefault(_fl);

var _fl3 = __webpack_require__(287);

var _fl4 = _interopRequireDefault(_fl3);

var _f9 = __webpack_require__(288);

var _f10 = _interopRequireDefault(_f9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FieldText = _f2.default;
exports.FieldSelect = _f4.default;
exports.FieldNumeric = _f8.default;
exports.FieldDate = _f6.default;
exports.FieldWorkflow = _sf2.default;
exports.FieldLocationType = _sfLocation2.default;
exports.FieldHBox = _fl4.default;
exports.FieldVBox = _fl2.default;
exports.FieldLabel = _f10.default;

// Layout fields


// System fields

/***/ }),
/* 280 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldText = function (_React$Component) {
    (0, _inherits3.default)(FieldText, _React$Component);

    function FieldText(props) {
        (0, _classCallCheck3.default)(this, FieldText);
        return (0, _possibleConstructorReturn3.default)(this, (FieldText.__proto__ || (0, _getPrototypeOf2.default)(FieldText)).call(this, props));
    }

    (0, _createClass3.default)(FieldText, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement("input", {
                value: this.props.value || '',
                onChange: function onChange(e) {
                    _this2.props.onChange(e.target.value);
                },
                type: "text" });
        }
    }]);
    return FieldText;
}(React.Component);

exports.default = FieldText;

/***/ }),
/* 281 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldSelect = function (_React$Component) {
    (0, _inherits3.default)(FieldSelect, _React$Component);

    function FieldSelect(props) {
        (0, _classCallCheck3.default)(this, FieldSelect);
        return (0, _possibleConstructorReturn3.default)(this, (FieldSelect.__proto__ || (0, _getPrototypeOf2.default)(FieldSelect)).call(this, props));
    }

    (0, _createClass3.default)(FieldSelect, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement(
                "div",
                { className: "select-wrapper" },
                React.createElement(
                    "select",
                    {
                        onChange: function onChange(e) {
                            _this2.props.onChange(e.target.value);
                        },
                        value: this.props.value || '' },
                    this.props.options.map(function (option) {
                        return React.createElement(
                            "option",
                            { value: option[0] },
                            option[1]
                        );
                    })
                ),
                React.createElement("i", { className: "fal fa-caret-down" })
            );
        }
    }]);
    return FieldSelect;
}(React.Component);

exports.default = FieldSelect;

/***/ }),
/* 282 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldDate = function (_React$Component) {
    (0, _inherits3.default)(FieldDate, _React$Component);

    function FieldDate(props) {
        (0, _classCallCheck3.default)(this, FieldDate);
        return (0, _possibleConstructorReturn3.default)(this, (FieldDate.__proto__ || (0, _getPrototypeOf2.default)(FieldDate)).call(this, props));
    }

    (0, _createClass3.default)(FieldDate, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement("input", {
                value: this.props.value || new Date(),
                onChange: function onChange(e) {
                    _this2.props.onChange(e.target.value);
                },
                type: "date" });
        }
    }]);
    return FieldDate;
}(React.Component);

exports.default = FieldDate;

/***/ }),
/* 283 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldNumeric = function (_React$Component) {
    (0, _inherits3.default)(FieldNumeric, _React$Component);

    function FieldNumeric(props) {
        (0, _classCallCheck3.default)(this, FieldNumeric);
        return (0, _possibleConstructorReturn3.default)(this, (FieldNumeric.__proto__ || (0, _getPrototypeOf2.default)(FieldNumeric)).call(this, props));
    }

    (0, _createClass3.default)(FieldNumeric, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement("input", {
                value: this.props.value || null,
                onChange: function onChange(e) {
                    _this2.props.onChange(e.target.value);
                },
                type: "number" });
        }
    }]);
    return FieldNumeric;
}(React.Component);

exports.default = FieldNumeric;

/***/ }),
/* 284 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldWorkflow = function (_React$Component) {
    (0, _inherits3.default)(FieldWorkflow, _React$Component);

    function FieldWorkflow(props) {
        (0, _classCallCheck3.default)(this, FieldWorkflow);
        return (0, _possibleConstructorReturn3.default)(this, (FieldWorkflow.__proto__ || (0, _getPrototypeOf2.default)(FieldWorkflow)).call(this, props));
    }

    (0, _createClass3.default)(FieldWorkflow, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "select-wrapper" },
                React.createElement(
                    "select",
                    { name: "", id: "" },
                    React.createElement(
                        "option",
                        { value: "" },
                        "Default workflow"
                    )
                ),
                React.createElement("i", { className: "fal fa-caret-down" })
            );
        }
    }]);
    return FieldWorkflow;
}(React.Component);

exports.default = FieldWorkflow;

/***/ }),
/* 285 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _api = __webpack_require__(48);

var _api2 = _interopRequireDefault(_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldLocationType = function (_React$Component) {
    (0, _inherits3.default)(FieldLocationType, _React$Component);

    function FieldLocationType(props) {
        (0, _classCallCheck3.default)(this, FieldLocationType);

        var _this = (0, _possibleConstructorReturn3.default)(this, (FieldLocationType.__proto__ || (0, _getPrototypeOf2.default)(FieldLocationType)).call(this, props));

        _this.state = {
            data: []
        };

        (0, _api2.default)("com.sonoma.location.types", []).then(function (resp) {
            _this.setState({
                data: resp.d
            });
        });
        return _this;
    }

    (0, _createClass3.default)(FieldLocationType, [{
        key: "render",
        value: function render() {

            var options = this.state.data.map(function (item) {
                return [item.uuid, item.name];
            });

            return React.createElement(
                "div",
                { className: "select-wrapper" },
                React.createElement(
                    "select",
                    { name: "", id: "", value: null },
                    React.createElement("option", { value: null }),
                    options.map(function (item) {
                        return React.createElement(
                            "option",
                            { value: item[0] },
                            item[1]
                        );
                    }),
                    React.createElement(
                        "option",
                        { value: "NEW" },
                        "Create new location type"
                    )
                ),
                React.createElement("i", { className: "fal fa-caret-down" })
            );
        }
    }]);
    return FieldLocationType;
}(React.Component);

FieldLocationType.defaultProps = {
    n: null,
    value: "",
    d: null,
    o: []
};
exports.default = FieldLocationType;

/***/ }),
/* 286 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldVBox = function (_React$Component) {
    (0, _inherits3.default)(FieldVBox, _React$Component);

    function FieldVBox(props) {
        (0, _classCallCheck3.default)(this, FieldVBox);
        return (0, _possibleConstructorReturn3.default)(this, (FieldVBox.__proto__ || (0, _getPrototypeOf2.default)(FieldVBox)).call(this, props));
    }

    (0, _createClass3.default)(FieldVBox, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "form-vbox" },
                this.props.children
            );
        }
    }]);
    return FieldVBox;
}(React.Component);

exports.default = FieldVBox;

/***/ }),
/* 287 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldHBox = function (_React$Component) {
    (0, _inherits3.default)(FieldHBox, _React$Component);

    function FieldHBox(props) {
        (0, _classCallCheck3.default)(this, FieldHBox);
        return (0, _possibleConstructorReturn3.default)(this, (FieldHBox.__proto__ || (0, _getPrototypeOf2.default)(FieldHBox)).call(this, props));
    }

    (0, _createClass3.default)(FieldHBox, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "form-hbox" },
                this.props.children
            );
        }
    }]);
    return FieldHBox;
}(React.Component);

exports.default = FieldHBox;

/***/ }),
/* 288 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FieldLabel = function (_React$Component) {
    (0, _inherits3.default)(FieldLabel, _React$Component);

    function FieldLabel(props) {
        (0, _classCallCheck3.default)(this, FieldLabel);
        return (0, _possibleConstructorReturn3.default)(this, (FieldLabel.__proto__ || (0, _getPrototypeOf2.default)(FieldLabel)).call(this, props));
    }

    (0, _createClass3.default)(FieldLabel, [{
        key: "render",
        value: function render() {

            var style = {};
            if (this.props.data.style) (0, _assign2.default)(style, this.props.data.style);

            var icon = void 0;
            if (this.props.data.icon) {
                icon = React.createElement("i", { className: "fal " + this.props.data.icon, style: { paddingRight: '8px' } });
            }

            return React.createElement(
                "div",
                { className: "field-label", style: style },
                icon,
                this.props.data.l
            );
        }
    }]);
    return FieldLabel;
}(React.Component);

exports.default = FieldLabel;

/***/ }),
/* 289 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

exports.default = [{
    _: '0',
    l: 'General Settings',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-cog',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        background: _constants.COLORS.frost3
    }
}, {
    _: '1',
    l: 'Form name',
    t: _constants.FIELDS.TEXT,
    n: 'name',
    r: 1
}, {
    _: '2',
    l: 'Status',
    n: 'status',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Draft'], [1, 'Active'], [2, 'Inactive'], [3, 'Archived']],
    r: 1
}, {
    _: '2.01',
    l: 'Custom record id',
    n: 'eid_prefix',
    t: _constants.FIELDS.TEXT
}, {
    _: '2.1',
    l: 'Description',
    n: 'description',
    t: _constants.FIELDS.TEXT,
    multi_line: 1
}, {
    _: '2.2',
    l: 'Workflow',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-sitemap',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: '3',
    l: 'Amendment workflow',
    n: 'wid',
    t: _constants.FIELDS.WORKFLOW,
    r: 1
}, {
    _: '4',
    l: 'Retraction workflow',
    n: 'rid',
    t: _constants.FIELDS.WORKFLOW,
    r: 1
}, {
    _: '5',
    l: 'Edit mode',
    n: 'mode',
    t: _constants.FIELDS.SELECT,
    r: 1,
    o: [['SINGLE', 'Individual entry'], ['LINE', 'Line entry']]
}, {
    _: '5.1',
    l: 'Overdue',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-clock',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: '6',
    t: _constants.FIELDS.HBOX,
    f: [{
        _: '6.1',
        l: 'Overdue',
        n: 'overdue',
        t: _constants.FIELDS.SWITCH,
        r: true,
        bL: true
    }, {
        _: '6.2',
        l: 'OD TIme',
        n: 'od_val',
        t: _constants.FIELDS.NUMERIC,
        bL: false
    }, {
        _: '6.3',
        l: 'OD Units',
        n: 'od_units',
        t: _constants.FIELDS.SELECT,
        o: [['D', 'Day(s)'], ['W', 'Week(s)'], ['M', 'Month(s)'], ['Y', 'Year(s)']],
        bL: false
    }]
}];

/***/ }),
/* 290 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _state = __webpack_require__(77);

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormState = function (_BaseState) {
    (0, _inherits3.default)(FormState, _BaseState);

    function FormState(props) {
        (0, _classCallCheck3.default)(this, FormState);

        var _this = (0, _possibleConstructorReturn3.default)(this, (FormState.__proto__ || (0, _getPrototypeOf2.default)(FormState)).call(this, props));

        _this.setView = function (view) {
            _this.view = view;
            _this._emit('on_view_changed');
        };

        _this.updateFormProp = function (prop, value) {
            _this.form[prop] = value;
            _this._emit('on_setting_change');
        };

        _this.form = {};
        _this.change_sets = [];
        _this.view = 'DEFAULT';
        return _this;
    }

    return FormState;
}(_state2.default);

exports.default = FormState;

/***/ }),
/* 291 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.FormDefinitionState = exports.EditorFormDefinition = undefined;

var _editor = __webpack_require__(292);

var _editor2 = _interopRequireDefault(_editor);

var _state = __webpack_require__(142);

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.EditorFormDefinition = _editor2.default;
exports.FormDefinitionState = _state2.default;

/***/ }),
/* 292 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

var _view = __webpack_require__(293);

var _view2 = _interopRequireDefault(_view);

var _view3 = __webpack_require__(295);

var _view4 = _interopRequireDefault(_view3);

var _state = __webpack_require__(142);

var _state2 = _interopRequireDefault(_state);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FORM_TYPES = [[0, 'Text field', 'fa-font'], [1, 'Select field', 'fa-list'], [2, 'Numeric field', 'fa-hash'], [3, 'Date field', 'fa-calendar'], [4, 'Location field', 'fa-map-marker'], [5, 'Reporting interval field', 'fa-calendar'], [6, 'Coordinate field', 'fa-map-marker'], [7, 'Age field', 'fa-child'], [8, 'Time field', 'fa-clock'], [102, 'Hbox', 'fa-bars fa-rotate-90'], [103, 'Vbox', 'fa-bars'], [104, 'Fieldset', 'fa-square']];

var NodeDrop = function (_React$Component) {
    (0, _inherits3.default)(NodeDrop, _React$Component);

    function NodeDrop(props) {
        (0, _classCallCheck3.default)(this, NodeDrop);

        var _this = (0, _possibleConstructorReturn3.default)(this, (NodeDrop.__proto__ || (0, _getPrototypeOf2.default)(NodeDrop)).call(this, props));

        _this._onDragStart = function (e) {
            e.dataTransfer.setData('m', _this.props.data[0]);
            utils.emit('on_drag_start');
        };

        _this._onDragEnd = function () {
            utils.emit('on_drag_cancel');
        };

        return _this;
    }

    (0, _createClass3.default)(NodeDrop, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                {
                    onDragStart: this._onDragStart,
                    onDragEnd: this._onDragEnd,
                    className: 'node-drop',
                    draggable: true },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { width: '20px' },
                        React.createElement('i', { className: "fal " + this.props.data[2] })
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        this.props.data[1]
                    )
                )
            );
        }
    }]);
    return NodeDrop;
}(React.Component);

var cmp_state = void 0;

var EditorFormDefinition = function (_React$Component2) {
    (0, _inherits3.default)(EditorFormDefinition, _React$Component2);

    function EditorFormDefinition(props) {
        (0, _classCallCheck3.default)(this, EditorFormDefinition);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (EditorFormDefinition.__proto__ || (0, _getPrototypeOf2.default)(EditorFormDefinition)).call(this, props));

        _this2._onChange = function () {
            _this2.setState(cmp_state);
        };

        cmp_state = new _state2.default();

        _this2.state = cmp_state;

        return _this2;
    }

    (0, _createClass3.default)(EditorFormDefinition, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.state.on('on_view_changed', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.state.off('on_view_changed', this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            var view = void 0;

            if (this.state.view == 'EDITOR') view = React.createElement(_view2.default, { data: this.state });
            if (this.state.view == 'PREVIEW') view = React.createElement(_view4.default, { data: this.state });

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { width: '250px', borderRight: true, style: { paddingTop: '8px' } },
                    FORM_TYPES.map(function (item) {
                        return React.createElement(NodeDrop, { data: item });
                    })
                ),
                view
            );
        }
    }]);
    return EditorFormDefinition;
}(React.Component);

EditorFormDefinition.defaultProps = {
    definition: {}
};
exports.default = EditorFormDefinition;

/***/ }),
/* 293 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(37);

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(34);

var _c4 = _interopRequireDefault(_c3);

var _c5 = __webpack_require__(75);

var _c6 = _interopRequireDefault(_c5);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

var _form = __webpack_require__(294);

var _form2 = _interopRequireDefault(_form);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FIELD_ACTIONS = [['fa-pencil', 'EDIT', 'Edit field'], ['fa-copy', 'DUPE', 'Duplicate field'], ['fa-ellipsis-v', 'NOOP3'], ['fa-trash', 'DELETE', 'Delete field']].reverse();

var FIELD_TYPES = [[0, 'Text field'], [1, 'Select field'], [2, 'Numeric field']];

var Field = function (_React$Component) {
    (0, _inherits3.default)(Field, _React$Component);

    function Field() {
        var _ref;

        var _temp, _this, _ret;

        (0, _classCallCheck3.default)(this, Field);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Field.__proto__ || (0, _getPrototypeOf2.default)(Field)).call.apply(_ref, [this].concat(args))), _this), _this._onTypeChange = function (newType) {}, _this._action = function (action) {
            switch (action) {
                case 'DUPE':
                    break;
                default:
                    break;
            }
        }, _this._onDragStart = function (e) {
            e.dataTransfer.setData('m', (0, _stringify2.default)({
                _: _this.props.data._,
                t: 'MOVE'
            }));
            utils.emit('on_drag_start');
        }, _this._onDragEnd = function () {
            utils.emit('on_drag_cancel');
        }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
    }

    (0, _createClass3.default)(Field, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                {
                    onDragStart: this._onDragStart,
                    onDragEnd: this._onDragEnd,
                    draggable: true,
                    className: 'definition-field' },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            _layout.Block,
                            null,
                            this.props.data.n,
                            ' (',
                            this.props.data._,
                            ')'
                        ),
                        React.createElement(
                            _layout.Block,
                            null,
                            React.createElement(_c4.default, {
                                value: null,
                                onChange: this._onTypeChange,
                                options: FIELD_TYPES })
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(_c2.default, {
                            right: true,
                            onAction: this._action,
                            actions: FIELD_ACTIONS })
                    )
                )
            );
        }
    }]);
    return Field;
}(React.Component);

var MainDropArea = function (_React$Component2) {
    (0, _inherits3.default)(MainDropArea, _React$Component2);

    function MainDropArea(props) {
        (0, _classCallCheck3.default)(this, MainDropArea);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (MainDropArea.__proto__ || (0, _getPrototypeOf2.default)(MainDropArea)).call(this, props));

        _this2._onDragOver = function (e) {
            e.preventDefault();

            if (_this2._el) {
                _this2._el.opacity = 1;
            }
        };

        _this2._onDragLeave = function (e) {
            e.preventDefault();
        };

        _this2._onDrop = function (e) {
            e.preventDefault();
            var data = e.dataTransfer.getData('m');
            _this2.props.data.addRootField(data);
        };

        return _this2;
    }

    (0, _createClass3.default)(MainDropArea, [{
        key: 'render',
        value: function render() {
            var _this3 = this;

            return React.createElement('div', {
                style: {
                    opacity: '0',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    background: 'rgba(0,0,0,0.6)'
                },
                ref: function ref(el) {
                    _this3._el = el;
                },
                onDragOver: this._onDragOver,
                onDragLeave: this._onDragLeave,
                onDrop: this._onDrop,
                className: 'definition-drop' });
        }
    }]);
    return MainDropArea;
}(React.Component);

MainDropArea.defaultProps = {
    data: null
};

var RelativeFieldDrop = function (_React$Component3) {
    (0, _inherits3.default)(RelativeFieldDrop, _React$Component3);

    function RelativeFieldDrop(props) {
        (0, _classCallCheck3.default)(this, RelativeFieldDrop);

        var _this4 = (0, _possibleConstructorReturn3.default)(this, (RelativeFieldDrop.__proto__ || (0, _getPrototypeOf2.default)(RelativeFieldDrop)).call(this, props));

        _this4._showDrop = function () {
            if (_this4._el) _this4._el.style.display = 'block';
        };

        _this4._hideDrop = function () {
            if (_this4._el) _this4._el.style.display = 'none';
        };

        _this4._onDragOver = function (e) {
            e.preventDefault();
        };

        _this4._onDragEnter = function (e) {
            if (_this4._el) _this4._el.style.borderStyle = 'solid';
        };

        _this4._onDragLeave = function (e) {
            if (_this4._el) _this4._el.style.borderStyle = 'dotted';
        };

        _this4._onDrop = function (e) {
            var data = e.dataTransfer.getData('m');

            if (_this4._el) _this4._el.style.borderStyle = 'dotted';
            _this4.props.state.addRelative(_this4.props.fid, data);
        };

        return _this4;
    }

    (0, _createClass3.default)(RelativeFieldDrop, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            utils.listen('on_drag_start', this._showDrop);
            utils.listen('on_drag_cancel', this._hideDrop);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            utils.forget('on_drag_start', this._showDrop);
            utils.forget('on_drag_cancel', this._hideDrop);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this5 = this;

            return React.createElement('div', {
                style: { display: 'none' },
                ref: function ref(el) {
                    _this5._el = el;
                },
                onDragOver: this._onDragOver,
                onDragEnter: this._onDragEnter,
                onDragLeave: this._onDragLeave,
                onDrop: this._onDrop,
                className: 'field-pre-drop' });
        }
    }]);
    return RelativeFieldDrop;
}(React.Component);

var ViewEditor = function (_React$Component4) {
    (0, _inherits3.default)(ViewEditor, _React$Component4);

    function ViewEditor(props) {
        (0, _classCallCheck3.default)(this, ViewEditor);

        var _this6 = (0, _possibleConstructorReturn3.default)(this, (ViewEditor.__proto__ || (0, _getPrototypeOf2.default)(ViewEditor)).call(this, props));

        _this6._onChange = function () {
            _this6.setState(_this6.state);
        };

        _this6.state = {
            field: null
        };
        return _this6;
    }

    (0, _createClass3.default)(ViewEditor, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.data.on('on_structure_change', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.data.off('on_structure_change', this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this7 = this;

            var view = void 0;

            if (this.props.data.definition.length <= 0) {
                view = React.createElement(MainDropArea, { data: this.props.data });
            } else {
                view = [];
                this.props.data.definition.forEach(function (item) {
                    view.push(React.createElement(RelativeFieldDrop, { fid: item._, state: _this7.props.data }));
                    view.push(React.createElement(Field, { data: item, state: _this7.props.data }));
                });
                view.push(React.createElement(RelativeFieldDrop, { state: this.props.data }));
            }

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Block,
                    { style: { paddingTop: '8px' }, yO: true },
                    view
                ),
                this.state.field ? React.createElement(
                    _layout.Block,
                    { width: '300px', borderLeft: true },
                    React.createElement(_c6.default, {
                        definition: _form2.default })
                ) : null
            );
        }
    }]);
    return ViewEditor;
}(React.Component);

ViewEditor.defaultProps = {
    fields: []
};
exports.default = ViewEditor;

/***/ }),
/* 294 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var FORM = [{
    _: 0,
    l: 'Label',
    t: _constants.FIELDS.TEXT,
    n: 'l',
    r: 1
}, {
    _: 1,
    l: 'Type',
    t: _constants.FIELDS.SELECT,
    n: 't',
    o: [],
    r: 1
}, {
    _: 1.01,
    l: 'Default value',
    t: _constants.FIELDS.TEXT,
    n: 'd'
}, {
    _: 1.1,
    l: 'Status',
    t: _constants.FIELDS.SELECT,
    n: 's',
    o: [[0, '-'], [1, '-']]
}, {
    _: 2,
    l: 'Output name',
    t: _constants.FIELDS.TEXT,
    n: 'on'
}, {
    _: '2.1',
    l: 'ID',
    n: '_',
    t: _constants.FIELDS.LABEL
}, {
    _: 3,
    l: 'Unique',
    t: _constants.FIELDS.SWITCH,
    n: 'uniq'
}, {
    _: 4,
    l: 'Required',
    t: _constants.FIELDS.SWITCH,
    n: 'r'
}, {
    _: 5,
    l: 'Multi-Select',
    t: _constants.FIELDS.SWITCH,
    n: 'multi_select',
    o: []
}, {
    _: 6,
    l: 'Options',
    n: 'o',
    t: _constants.FIELDS.TEXT
}, {
    _: 7,
    l: 'Minimum',
    t: _constants.FIELDS.NUMERIC,
    n: 'min'
}, {
    _: 8,
    l: 'Maximum',
    t: _constants.FIELDS.NUMERIC,
    n: 'max'
}, {
    _: 9,
    l: 'Interval type',
    t: _constants.FIELDS.TEXT,
    n: 'interval'
}];

exports.default = FORM;

/***/ }),
/* 295 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewPreview = function (_React$Component) {
    (0, _inherits3.default)(ViewPreview, _React$Component);

    function ViewPreview(props) {
        (0, _classCallCheck3.default)(this, ViewPreview);
        return (0, _possibleConstructorReturn3.default)(this, (ViewPreview.__proto__ || (0, _getPrototypeOf2.default)(ViewPreview)).call(this, props));
    }

    (0, _createClass3.default)(ViewPreview, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                "Preview"
            );
        }
    }]);
    return ViewPreview;
}(React.Component);

exports.default = ViewPreview;

/***/ }),
/* 296 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var FORM_PERMISSIONS = [{
    n: 'READ',
    l: 'Can read'
}, {
    n: 'SUBMIT',
    l: 'Can submit'
}, {
    n: 'RETRACT',
    l: 'Can retract'
}, {
    n: 'AMEND',
    l: 'Can amend'
}, {
    n: 'EXPORT',
    l: 'Can export'
}, {
    n: 'IMPORT',
    l: 'Can import'
}];

exports.default = FORM_PERMISSIONS;

/***/ }),
/* 297 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'ADD_ITEM', 'New Feed Item']];

var ITEM_ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Delete feed item']].reverse();

var TabFeed = function (_React$Component) {
    (0, _inherits3.default)(TabFeed, _React$Component);

    function TabFeed(props) {
        (0, _classCallCheck3.default)(this, TabFeed);
        return (0, _possibleConstructorReturn3.default)(this, (TabFeed.__proto__ || (0, _getPrototypeOf2.default)(TabFeed)).call(this, props));
    }

    (0, _createClass3.default)(TabFeed, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { width: '30%', borderRight: true },
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c2.default, {
                            actions: ACTIONS,
                            onAction: this._action,
                            right: true })
                    ),
                    React.createElement(_layout.Block, null)
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c2.default, {
                            actions: ITEM_ACTIONS,
                            onAction: this._action,
                            right: true })
                    ),
                    React.createElement(_layout.Block, null)
                )
            );
        }
    }]);
    return TabFeed;
}(React.Component);

exports.default = TabFeed;

/***/ }),
/* 298 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

var _layout2 = __webpack_require__(299);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TabDashboard = function (_React$Component) {
    (0, _inherits3.default)(TabDashboard, _React$Component);

    function TabDashboard(props) {
        (0, _classCallCheck3.default)(this, TabDashboard);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabDashboard.__proto__ || (0, _getPrototypeOf2.default)(TabDashboard)).call(this, props));

        var l_state = new _layout2.LayoutState();
        l_state.mode = 'FITTED';
        l_state.hasPermissions = true;

        _this.state = {
            l_state: l_state
        };
        return _this;
    }

    (0, _createClass3.default)(TabDashboard, [{
        key: 'render',
        value: function render() {
            return React.createElement(_layout2.EditorLayout, {
                onAction: this._action,
                state: this.state.l_state });
        }
    }]);
    return TabDashboard;
}(React.Component);

exports.default = TabDashboard;

/***/ }),
/* 299 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.LayoutState = exports.EditorLayout = undefined;

var _e = __webpack_require__(300);

var _e2 = _interopRequireDefault(_e);

var _state = __webpack_require__(304);

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.EditorLayout = _e2.default;
exports.LayoutState = _state2.default;

/***/ }),
/* 300 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(116);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(7);

var _c4 = _interopRequireDefault(_c3);

var _c5 = __webpack_require__(301);

var _c6 = _interopRequireDefault(_c5);

var _c7 = __webpack_require__(303);

var _c8 = _interopRequireDefault(_c7);

var _c9 = __webpack_require__(141);

var _c10 = _interopRequireDefault(_c9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Delete dashboard']].reverse();

var EditorLayout = function (_React$Component) {
    (0, _inherits3.default)(EditorLayout, _React$Component);

    function EditorLayout(props) {
        (0, _classCallCheck3.default)(this, EditorLayout);

        var _this = (0, _possibleConstructorReturn3.default)(this, (EditorLayout.__proto__ || (0, _getPrototypeOf2.default)(EditorLayout)).call(this, props));

        _this._onChange = function () {
            _this.setState(_this.props.state);
        };

        _this.state = _this.props.state;

        return _this;
    }

    (0, _createClass3.default)(EditorLayout, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.state.on('on_view_changed', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.state.off('on_view_changed', this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var sideView = void 0,
                view = void 0;

            if (this.state.sideView == 'WIDGETS') sideView = React.createElement(_c6.default, null);
            if (this.state.sideView == 'TREE') sideView = React.createElement(_c8.default, null);

            if (this.state.view == 'PERMISSIONS') view = React.createElement(_c10.default, null);

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Vbox,
                        { borderRight: true, width: '25%' },
                        React.createElement(
                            _layout.Block,
                            { borderBottom: true, height: '30px', addClass: 'tbar' },
                            React.createElement(_c2.default, {
                                active: this.state.sideView == 'DEFAULT',
                                onClick: function onClick() {
                                    _this2.state.setSideView('DEFAULT');
                                },
                                icon: 'fa-cog' }),
                            React.createElement(_c2.default, {
                                active: this.state.sideView == 'WIDGETS',
                                onClick: function onClick() {
                                    _this2.state.setSideView('WIDGETS');
                                },
                                icon: 'fa-cube' }),
                            React.createElement(_c2.default, {
                                active: this.state.sideView == 'STYLE',
                                onClick: function onClick() {
                                    _this2.state.setSideView('STYLE');
                                },
                                icon: 'fa-paint-brush' }),
                            React.createElement(_c2.default, {
                                active: this.state.sideView == 'TREE',
                                onClick: function onClick() {
                                    _this2.state.setSideView('TREE');
                                },
                                icon: 'fa-code-branch' })
                        ),
                        sideView
                    ),
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            _layout.Block,
                            { borderBottom: true, height: '30px', addClass: 'tbar' },
                            React.createElement(_c2.default, {
                                active: this.state.view == 'EDITOR',
                                onClick: function onClick() {
                                    _this2.state.setView('EDITOR');
                                },
                                icon: 'fa-cog',
                                label: 'Editor' }),
                            React.createElement(_c2.default, {
                                active: this.state.view == 'PREVIEW',
                                onClick: function onClick() {
                                    _this2.state.setView('PREVIEW');
                                },
                                icon: 'fa-eye',
                                label: 'Preview' }),
                            this.state.hasPermissions ? React.createElement(_c2.default, {
                                active: this.state.view == 'PERMISSIONS',
                                onClick: function onClick() {
                                    _this2.state.setView('PERMISSIONS');
                                },
                                icon: 'fa-lock',
                                label: 'Permissions' }) : null,
                            React.createElement(_c4.default, {
                                actions: ACTIONS,
                                right: true })
                        ),
                        view
                    )
                )
            );
        }
    }]);
    return EditorLayout;
}(React.Component);

EditorLayout.defaultProps = {
    state: null
};
exports.default = EditorLayout;

/***/ }),
/* 301 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _const = __webpack_require__(302);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WidgetItem = function (_React$Component) {
    (0, _inherits3.default)(WidgetItem, _React$Component);

    function WidgetItem(props) {
        (0, _classCallCheck3.default)(this, WidgetItem);
        return (0, _possibleConstructorReturn3.default)(this, (WidgetItem.__proto__ || (0, _getPrototypeOf2.default)(WidgetItem)).call(this, props));
    }

    (0, _createClass3.default)(WidgetItem, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'item', draggable: true },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { width: '30px', style: { padding: "8px" } },
                        React.createElement(
                            'div',
                            { className: 'icon-circle' },
                            React.createElement('i', { className: 'fal ' + this.props.data.i })
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { style: { padding: '8px' } },
                        this.props.data.n
                    )
                )
            );
        }
    }]);
    return WidgetItem;
}(React.Component);

var Widgets = function (_React$Component2) {
    (0, _inherits3.default)(Widgets, _React$Component2);

    function Widgets(props) {
        (0, _classCallCheck3.default)(this, Widgets);
        return (0, _possibleConstructorReturn3.default)(this, (Widgets.__proto__ || (0, _getPrototypeOf2.default)(Widgets)).call(this, props));
    }

    (0, _createClass3.default)(Widgets, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Block,
                { yO: true },
                React.createElement(
                    'div',
                    { className: 'task-list' },
                    _const2.default.map(function (item) {
                        return React.createElement(WidgetItem, { data: item });
                    })
                )
            );
        }
    }]);
    return Widgets;
}(React.Component);

exports.default = Widgets;

/***/ }),
/* 302 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var WIDGETS = [{
    _: 'VBOX',
    n: 'Vertical stack',
    t: 'LAYOUT',
    i: 'fa-list'
}, {
    _: 'HBOX',
    n: 'Horizontal stack',
    t: 'LAYOUT',
    i: 'fa-list fa-rotate-90'
}, {
    _: 'LIST',
    n: 'List view',
    t: 'LAYOUT',
    i: 'fa-list'
}, {
    _: 'GRID',
    n: 'Grid view',
    t: 'LAYOUT',
    i: 'fa-th'
}, {
    _: 'DEFAULT',
    n: "Default chart",
    t: 'VIS',
    i: 'fa-chart-line'
}, {
    _: 'PIE',
    n: "Pie chart",
    t: 'VIS',
    i: "fa-chart-pie"
}, {
    _: 'HEATMAP',
    n: "Heatmap",
    t: 'VIS',
    i: 'fa-chart-line'
}, {
    _: 'MAP',
    n: 'Map',
    t: 'VIS',
    i: "fa-map"
}, {
    _: 'DATAPOINT',
    n: 'Datapoint',
    t: 'VIS',
    i: 'fa-information-circle'
}, {
    _: 'ALERTS',
    n: 'Alerts',
    t: 'CON',
    i: "fa-exclamation-triangle"
}, {
    _: 'ACTIVITY',
    n: 'Activity',
    t: 'CON',
    i: "fa-cube"
}, {
    _: 'FUNNEL',
    n: "Funnel",
    t: 'VIS'
}, {
    _: 'PYRAMID',
    n: 'Pyramid',
    t: 'VIS'
}, {
    _: 'TABLE',
    n: 'Table',
    t: 'VIS'
}, {
    _: 'PIVOT',
    n: 'Pivot table',
    t: 'VIS'
}, {
    _: "RECENT_SUBMISSIONS",
    n: 'Recent submission',
    t: 'CON'
}, {
    _: 'DOCUMENTS',
    n: 'Documents',
    t: 'CON'
}, {
    _: 'TEAMS',
    n: 'Teams',
    t: 'CON'
}, {
    _: 'HTML',
    n: 'HTML',
    t: 'CON'
}, {
    _: 'TASKS',
    n: 'Tasks',
    t: 'CON'
}, {
    _: "ASSIGNMENTS",
    n: 'Assignments',
    t: 'CON'
}, {
    _: 'IMAGE',
    n: "Image",
    t: 'CON'
}, {
    _: 'LINKS',
    n: 'Links',
    t: 'CON'
}, {
    _: 'FEED',
    n: 'Feed',
    t: 'CON'
}, {
    _: 'WIKIS',
    n: 'Wikis',
    t: 'CON'
}, {
    _: 'WIKI',
    n: "Wiki",
    t: 'CON'
}];

exports.default = WIDGETS;

/***/ }),
/* 303 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LayoutTree = function (_React$Component) {
    (0, _inherits3.default)(LayoutTree, _React$Component);

    function LayoutTree(props) {
        (0, _classCallCheck3.default)(this, LayoutTree);
        return (0, _possibleConstructorReturn3.default)(this, (LayoutTree.__proto__ || (0, _getPrototypeOf2.default)(LayoutTree)).call(this, props));
    }

    (0, _createClass3.default)(LayoutTree, [{
        key: "render",
        value: function render() {
            return React.createElement("div", null);
        }
    }]);
    return LayoutTree;
}(React.Component);

exports.default = LayoutTree;

/***/ }),
/* 304 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _state = __webpack_require__(77);

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LayoutState = function (_BaseState) {
    (0, _inherits3.default)(LayoutState, _BaseState);

    function LayoutState(props) {
        (0, _classCallCheck3.default)(this, LayoutState);

        var _this = (0, _possibleConstructorReturn3.default)(this, (LayoutState.__proto__ || (0, _getPrototypeOf2.default)(LayoutState)).call(this, props));

        _this.setView = function (view) {
            _this.view = view;
            _this._emit('on_view_changed');
        };

        _this.setSideView = function (view) {
            _this.sideView = view;
            _this._emit('on_view_changed');
        };

        _this.definition = [];
        _this.data = {};
        _this.style = {};

        _this.mode = 'FITTED'; // FITTED  | SCROLLING | PAGED
        _this.hasPermissions = false;

        _this.view = 'EDITOR';
        _this.sideView = 'DEFAULT';
        return _this;
    }

    return LayoutState;
}(_state2.default);

exports.default = LayoutState;

/***/ }),
/* 305 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Delete workflow']].reverse();

var TabWorkflow = function (_React$Component) {
    (0, _inherits3.default)(TabWorkflow, _React$Component);

    function TabWorkflow(props) {
        (0, _classCallCheck3.default)(this, TabWorkflow);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabWorkflow.__proto__ || (0, _getPrototypeOf2.default)(TabWorkflow)).call(this, props));

        _this.state = {
            view: 'SETTINGS'
        };
        return _this;
    }

    (0, _createClass3.default)(TabWorkflow, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            _layout.Block,
                            { height: '30px', borderBottom: true, addClass: 'tbar' },
                            React.createElement(_c4.default, {
                                active: this.state.view == 'SETTINGS',
                                icon: 'fa-cog',
                                label: 'Settings' }),
                            React.createElement(_c4.default, {
                                active: this.state.view == 'DEFINITION',
                                icon: 'fa-cog',
                                label: 'Definition' }),
                            React.createElement(_c4.default, {
                                active: this.state.view == 'PREVIEW',
                                icon: 'fa-eye',
                                label: 'Preview' }),
                            React.createElement(_c4.default, {
                                active: this.state.view == 'PERMISSIONS',
                                icon: 'fa-lock',
                                label: 'permissions' }),
                            React.createElement(_c2.default, {
                                actions: ACTIONS,
                                right: true })
                        ),
                        React.createElement(_layout.Block, null)
                    )
                )
            );
        }
    }]);
    return TabWorkflow;
}(React.Component);

exports.default = TabWorkflow;

/***/ }),
/* 306 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Delete alarm']].reverse();

var TabAlarm = function (_React$Component) {
    (0, _inherits3.default)(TabAlarm, _React$Component);

    function TabAlarm(props) {
        (0, _classCallCheck3.default)(this, TabAlarm);
        return (0, _possibleConstructorReturn3.default)(this, (TabAlarm.__proto__ || (0, _getPrototypeOf2.default)(TabAlarm)).call(this, props));
    }

    (0, _createClass3.default)(TabAlarm, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Alarms'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true,
                        onAction: this._action })
                ),
                React.createElement(_layout.Block, null)
            );
        }
    }]);
    return TabAlarm;
}(React.Component);

exports.default = TabAlarm;

/***/ }),
/* 307 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(116);

var _c4 = _interopRequireDefault(_c3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Add page']];

var ITEM_ACTIONS = [['fa-save', 'SAVE', 'Save change(s)']];

var TabComponent = function (_React$Component) {
    (0, _inherits3.default)(TabComponent, _React$Component);

    function TabComponent(props) {
        (0, _classCallCheck3.default)(this, TabComponent);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabComponent.__proto__ || (0, _getPrototypeOf2.default)(TabComponent)).call(this, props));

        _this.state = {
            view: 'SETTINGS'
        };
        return _this;
    }

    (0, _createClass3.default)(TabComponent, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { borderRight: true, width: '25%' },
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c2.default, {
                            actions: ACTIONS,
                            right: true,
                            onAction: this._action })
                    ),
                    React.createElement(_layout.Block, { yO: true })
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c4.default, {
                            active: this.state.view == 'SETTINGS',
                            icon: 'fa-cog',
                            label: 'Settings' }),
                        React.createElement(_c4.default, {
                            active: this.state.view == 'PERMISSIONS',
                            icon: 'fa-lock',
                            label: 'Permissions' }),
                        React.createElement(_c2.default, {
                            actions: ITEM_ACTIONS,
                            right: true,
                            onAction: this._action })
                    ),
                    React.createElement(_layout.Block, null)
                )
            );
        }
    }]);
    return TabComponent;
}(React.Component);

exports.default = TabComponent;

/***/ }),
/* 308 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'New page']];

var ITEM_ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Delete item']].reverse();

var TabWiki = function (_React$Component) {
    (0, _inherits3.default)(TabWiki, _React$Component);

    function TabWiki(props) {
        (0, _classCallCheck3.default)(this, TabWiki);
        return (0, _possibleConstructorReturn3.default)(this, (TabWiki.__proto__ || (0, _getPrototypeOf2.default)(TabWiki)).call(this, props));
    }

    (0, _createClass3.default)(TabWiki, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { width: '25%', borderRight: true },
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c2.default, {
                            actions: ACTIONS,
                            right: true,
                            onAction: this._action })
                    ),
                    React.createElement(_layout.Block, null)
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(_c2.default, {
                            actions: ITEM_ACTIONS,
                            right: true,
                            onAction: this._action })
                    )
                )
            );
        }
    }]);
    return TabWiki;
}(React.Component);

exports.default = TabWiki;

/***/ }),
/* 309 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _menu = __webpack_require__(310);

var _menu2 = _interopRequireDefault(_menu);

var _state = __webpack_require__(31);

var _view = __webpack_require__(312);

var _view2 = _interopRequireDefault(_view);

var _view3 = __webpack_require__(313);

var _view4 = _interopRequireDefault(_view3);

var _view5 = __webpack_require__(314);

var _view6 = _interopRequireDefault(_view5);

var _view7 = __webpack_require__(315);

var _view8 = _interopRequireDefault(_view7);

var _view9 = __webpack_require__(316);

var _view10 = _interopRequireDefault(_view9);

var _view11 = __webpack_require__(317);

var _view12 = _interopRequireDefault(_view11);

var _view13 = __webpack_require__(318);

var _view14 = _interopRequireDefault(_view13);

var _view15 = __webpack_require__(118);

var _view16 = _interopRequireDefault(_view15);

var _view17 = __webpack_require__(319);

var _view18 = _interopRequireDefault(_view17);

var _view19 = __webpack_require__(320);

var _view20 = _interopRequireDefault(_view19);

var _view21 = __webpack_require__(321);

var _view22 = _interopRequireDefault(_view21);

var _view23 = __webpack_require__(323);

var _view24 = _interopRequireDefault(_view23);

var _view25 = __webpack_require__(325);

var _view26 = _interopRequireDefault(_view25);

var _view27 = __webpack_require__(327);

var _view28 = _interopRequireDefault(_view27);

var _view29 = __webpack_require__(329);

var _view30 = _interopRequireDefault(_view29);

var _view31 = __webpack_require__(330);

var _view32 = _interopRequireDefault(_view31);

var _view33 = __webpack_require__(331);

var _view34 = _interopRequireDefault(_view33);

var _view35 = __webpack_require__(332);

var _view36 = _interopRequireDefault(_view35);

var _view37 = __webpack_require__(333);

var _view38 = _interopRequireDefault(_view37);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VIEWS = {
    ALARMS: _view36.default,
    COMPONENTS: _view38.default,
    DEFAULT: _view20.default,
    FORMS: _view2.default,
    FEEDS: _view4.default,
    WORKFLOWS: _view6.default,
    WIKIS: _view8.default,
    SITES: _view10.default,
    DEVICES: _view12.default,
    GATEWAYS: _view14.default,
    DASHBOARDS: _view16.default,
    DOCUMENTS: _view18.default,
    ACCOUNT: _view22.default,
    THEME: _view24.default,
    NOTIFICATIONS: _view26.default,
    SECURITY: _view28.default,
    ROLES: _view30.default,
    MENU: _view32.default,
    USERS: _view34.default
};

var ViewDefault = function (_React$Component) {
    (0, _inherits3.default)(ViewDefault, _React$Component);

    function ViewDefault(props) {
        (0, _classCallCheck3.default)(this, ViewDefault);
        return (0, _possibleConstructorReturn3.default)(this, (ViewDefault.__proto__ || (0, _getPrototypeOf2.default)(ViewDefault)).call(this, props));
    }

    (0, _createClass3.default)(ViewDefault, [{
        key: 'render',
        value: function render() {

            var view = void 0;

            if (_state.state.view == 'DEFAULT') {
                view = React.createElement(_view20.default, null);
            } else {

                if (VIEWS[_state.state.view._]) {
                    var Cmp = VIEWS[_state.state.view._];
                    view = React.createElement(Cmp, null);
                }
            }

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Block,
                    { width: '300px', borderRight: true },
                    React.createElement(_menu2.default, null)
                ),
                view
            );
        }
    }]);
    return ViewDefault;
}(React.Component);

exports.default = ViewDefault;

/***/ }),
/* 310 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _const = __webpack_require__(311);

var _const2 = _interopRequireDefault(_const);

var _layout = __webpack_require__(6);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};

var TreeNode = function (_React$Component) {
    (0, _inherits3.default)(TreeNode, _React$Component);

    function TreeNode(props) {
        (0, _classCallCheck3.default)(this, TreeNode);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TreeNode.__proto__ || (0, _getPrototypeOf2.default)(TreeNode)).call(this, props));

        _this._toggleShow = function () {
            console.log(_this.state.show);
            if (!_this.state.show) {
                console.log('here');
                console.log(_this.iconEl);
                _this.iconEl.className = 'fal fa-caret-down';
            } else {
                console.log(_this.iconEl);
                console.log('here2');
                _this.iconEl.className = 'fal fa-caret-right';
            }
            _this.setState({ show: !_this.state.show }, _this.render);
        };

        _this._onClick = function () {
            if (_this.props.data.t == TYPES.TAB) {
                _state.state.addTab(_this.props.data);
            } else {
                _state.state.setView(_this.props.data);
            }
        };

        _this.getIconClass = function () {
            return _this.state.show == true ? 'fal fa-caret-down' : 'fal fa-caret-right';
        };

        _this.state = { show: false };

        _this.hasChilds = _this.props.data.c ? true : false;
        return _this;
    }

    (0, _createClass3.default)(TreeNode, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            if (nextState.show != this.state.show) return true;
            return false;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var icon = void 0;

            if (this.props.data.i) icon = React.createElement('i', { className: 'fal ' + this.props.data.i });

            var caretIcon = React.createElement('i', { className: 'fal fa-caret-right' });
            if (this.state.show) caretIcon = React.createElement('i', { className: 'fal fa-caret-down' });

            return React.createElement(
                'li',
                null,
                React.createElement(
                    _layout.Hbox,
                    null,
                    this.hasChilds ? React.createElement(
                        _layout.Block,
                        {
                            onClick: this._toggleShow,
                            width: '20px' },
                        React.createElement('i', { ref: function ref(el) {
                                _this2.iconEl = el;
                            }, className: 'fal fa-caret-right' })
                    ) : null,
                    !this.hasChilds ? React.createElement(
                        _layout.Block,
                        { width: '20px' },
                        '\xA0'
                    ) : null,
                    icon ? React.createElement(
                        _layout.Block,
                        {
                            onClick: this._onClick,
                            width: '25px' },
                        icon
                    ) : null,
                    React.createElement(
                        _layout.Block,
                        {
                            onClick: this._onClick,
                            style: { cursor: 'pointer' } },
                        this.props.data.n
                    )
                ),
                this.props.data.c && this.state.show ? React.createElement(
                    'ul',
                    null,
                    this.props.data.c.map(function (item) {
                        return React.createElement(TreeNode, { key: item._, data: item });
                    })
                ) : null
            );
        }
    }]);
    return TreeNode;
}(React.Component);

var AdminTreeMenu = function (_React$Component2) {
    (0, _inherits3.default)(AdminTreeMenu, _React$Component2);

    function AdminTreeMenu(props) {
        (0, _classCallCheck3.default)(this, AdminTreeMenu);
        return (0, _possibleConstructorReturn3.default)(this, (AdminTreeMenu.__proto__ || (0, _getPrototypeOf2.default)(AdminTreeMenu)).call(this, props));
    }

    (0, _createClass3.default)(AdminTreeMenu, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Block,
                { yO: true },
                React.createElement(
                    'ul',
                    { className: 'config-menu', style: { marginTop: '8px', marginLeft: '8px' } },
                    _const2.default.map(function (item) {
                        return React.createElement(TreeNode, { key: item._, data: item });
                    })
                )
            );
        }
    }]);
    return AdminTreeMenu;
}(React.Component);

exports.default = AdminTreeMenu;

/***/ }),
/* 311 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};

exports.default = [{
    n: 'Overview',
    i: 'fa-tachometer',
    _: 'DEFAULT',
    t: TYPES.VIEW
}, {
    n: 'Appearance & Behaviour',
    i: 'fa-cog',
    _: 'APPEARANCE',
    t: TYPES.VIEW,
    c: [{ n: 'Account', i: 'fa-life-ring', _: 'ACCOUNT', t: TYPES.VIEW }, { n: 'Theme', i: 'fa-paint-brush', _: 'THEME', t: TYPES.VIEW }, { n: 'Email & Notifications', i: 'fa-envelope', _: 'NOTIFICATIONS', t: TYPES.VIEW }, { n: 'Security & Authentication', i: 'fa-key', _: 'SECURITY', t: TYPES.VIEW }, { n: 'Menu', i: 'fa-globe', _: 'MENU', t: TYPES.VIEW }, {
        n: 'Legal',
        i: 'fa-balance-scale',
        _: 'LEGAL',
        t: null,
        c: [{ n: 'Terms of use', i: 'fa-file', _: 'TERMS', t: TYPES.VIEW }, { n: 'Privacy policy', i: 'fa-file', _: 'PRIVACY', t: TYPES.VIEW }]
    }]
}, {
    n: 'Alarms',
    i: 'fa-bell',
    _: 'ALARMS',
    t: TYPES.VIEW
}, {
    n: 'Locations',
    i: 'fa-map-marker',
    t: TYPES.TAB,
    _: 'LOCATIONS',
    c: [{ n: 'Location types', i: 'fa-tags', _: 'LOCATION_TYPES', t: TYPES.TAB }, { n: 'Location groups', i: 'fa-tags', _: 'LOCATION_GROUPS', t: TYPES.TAB }, { n: 'Identification codes', i: 'fa-barcode', _: 'LOCATION_ID_CODES', t: TYPES.VIEW }, { n: 'Location statuses', i: 'fa-status', _: 'LOCATION_STATUSES', t: TYPES.VIEW }]
}, {
    n: 'Users',
    i: 'fa-user',
    _: 'USERS',
    t: TYPES.VIEW,
    c: [{ n: 'Roles', i: 'fa-users', _: 'ROLES', t: TYPES.TABLE }, { n: 'Assignments', i: 'fa-clipboard', _: 'ASSIGNMENTS', t: TYPES.TABLE }, { n: 'Organizations', i: 'fa-building', _: 'ORGANIZATIONS', t: TYPES.TABLE }, { n: 'Teams', i: 'fa-users', _: 'TEAMS', t: TYPES.TABLE }, { n: 'Profile extensions', i: 'fa-id-card', _: 'USER_PROFILES', t: TYPES.TABLE }, { n: 'Distribution lists', i: 'fa-envelope', _: 'DISTRIBUTIONS', t: TYPES.TABLE }]
}, {
    n: 'Forms',
    i: 'fa-list-alt',
    _: 'FORMS',
    t: TYPES.VIEW
}, {
    n: 'Workflows',
    i: 'fa-code-branch',
    _: 'WORKFLOWS',
    t: TYPES.VIEW
}, {
    n: 'Events',
    i: 'fa-calendar',
    _: 'EVENTS',
    t: TYPES.VIEW,
    c: [{ n: 'Event types', i: 'fa-tag', _: 'EVENT_TYPES' }]
}, {
    n: 'Processes',
    i: 'fa-wrench',
    _: 'PROCESSES',
    t: TYPES.VIEW
}, { n: 'Datasets', i: 'fa-database', _: 'RELATIONAL', t: TYPES.TABLE }, { n: 'External data', i: 'fa-server', _: 'CONNECTORS', t: TYPES.TABLE }, {
    n: 'Analysis',
    i: 'fa-chart-line',
    _: 'ANALYSIS',
    c: [{ n: 'Measures', i: 'fa-tag', _: 'MEASURES', t: TYPES.TAB }, { n: 'Dimensions', i: 'fa-cog', _: 'DIMENSIONS', t: TYPES.TAB }, { n: 'Indicators', i: 'fa-tag', _: 'INDICATORS', t: TYPES.TAB }]

}, { n: 'Feeds', i: 'fa-file-alt', _: 'FEEDS', t: TYPES.TABLE }, { n: 'Wikis', i: 'fa-graduation-cap', _: 'WIKIS', t: TYPES.TABLE }, { n: 'Digest emails', i: 'fa-envelope', _: 'DIGESTS', t: TYPES.TABLE }, { n: 'HUDs', i: 'fa-browser', _: 'HUDS', t: TYPES.TABLE }, { n: 'Dashboards', i: 'fa-tachometer', _: 'DASHBOARDS', t: TYPES.TABLE }, { n: 'Documents', i: 'fa-file', _: 'DOCUMENTS', t: TYPES.TABLE }, {
    n: 'Devices',
    i: 'fa-mobile',
    _: 'DEVICES',
    t: TYPES.VIEW,
    c: [{ n: 'SMS gateways', i: 'fa-server', _: 'GATEWAYS', t: TYPES.TABLE }, { n: 'Relay nodes', i: 'fa-server', _: 'RELAY_NODES', t: TYPES.TABLE }, { n: 'GSM Modems', i: 'fa-server', _: 'GSM', t: TYPES.VIEW }]
}, {
    n: 'Hub',
    i: 'fa-dot-circle',
    _: 'HUB',
    t: TYPES.VIEW,
    c: [{ n: 'Subscriptions', i: 'fa-arrow-to-right', _: 'SUBSCRIPTIONS', t: TYPES.TABLE }, { n: 'Shared data', i: 'fa-exchange', _: 'SHARED_DATA', t: TYPES.TABLE }, { n: 'Subscribers', i: 'fa-arrow-from-right', _: 'SUBSCRIBERS', t: TYPES.TABLE }]
}, {
    n: 'Integrations',
    i: 'fa-cubes',
    _: 'INTEGRATIONS',
    t: TYPES.VIEW
}, {
    n: 'Custom components',
    i: 'fa-cubes',
    _: 'COMPONENTS',
    t: TYPES.TABLE
}, {
    n: 'Sites',
    i: 'fa-sitemap',
    _: 'SITES',
    t: TYPES.TABLE
}, {
    n: 'API',
    i: 'fa-leaf',
    _: 'API',
    t: TYPES.VIEW
}, {
    n: 'Export',
    i: 'fa-download',
    _: 'EXPORT',
    t: TYPES.VIEW
}, {
    n: 'System administration',
    i: 'fa-cogs',
    _: 'ADMIN',
    t: TYPES.VIEW,
    c: [{ n: 'Usage', i: 'fa-fire', _: 'USAGE', t: TYPES.VIEW }, { n: 'Invitations', i: 'fa-envelope', _: 'INVITATIONS', t: TYPES.TABLE }, { n: 'System notices', i: 'fa-exclamation-triangle', _: 'NOTICES', t: TYPES.TABLE }, { n: 'Backup(s)', i: 'fa-database', _: 'BACKUPS', t: TYPES.VIEW }, { n: 'EWARS releases', i: 'fa-thumbs-up', _: 'RELEASES', t: TYPES.TABLE }, { n: 'Translation(s)', i: 'fa-language', _: 'TRANSLATIONS', t: TYPES.TABLE }, { n: 'PDF cache', i: 'fa-file-archive', _: 'PDF_CACHE', t: TYPES.TABLE }, { n: 'Indexing', i: 'fa-list-alt', _: 'INDEXING', t: TYPES.VIEW }, { n: 'Reporting intervals', i: 'fa-calendar', _: 'INTERVALS', t: TYPES.VIEW }, { n: 'Jobs', i: 'fa-clock', _: 'JOBS', t: TYPES.VIEW }, {
        n: 'Auditing',
        i: 'fa-briefcase',
        _: 'AUDITING',
        t: TYPES.VIEW,
        c: [{ n: 'Task auditing', i: 'fa-tasks', _: 'TASK_AUDITOR', t: TYPES.TABLE }, { n: 'Audit log(s)', i: 'fa-clock', _: 'AUDIT_LOGS', t: TYPES.TABLE }, { n: 'Authentication log', i: 'fa-shield', _: 'AUTH_LOG', t: TYPES.TABLE }, { n: 'Event log', i: 'fa-sync', _: 'EVENT_LOG', t: TYPES.TABLE }]
    }]
}];

/***/ }),
/* 312 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [["name", "Name"], ["status", "Status"], ["created", "Created"], ["modified", "Modified"], ["uuid", "ID"]];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ROW_ACTIONS = [['fa-pencil', 'ROW_EDIT'], ['fa-trash', 'ROW_DELETE'], ['fa-download', 'ROW_EXPORT']].reverse();

var ViewForms = function (_React$Component) {
    (0, _inherits3.default)(ViewForms, _React$Component);

    function ViewForms(props) {
        (0, _classCallCheck3.default)(this, ViewForms);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewForms.__proto__ || (0, _getPrototypeOf2.default)(ViewForms)).call(this, props));

        _this._onSelectForm = function (data) {
            _state.state.addTab({
                n: 'Form',
                i: data.uuid,
                _: 'FORM'
            });
        };

        _this._onAction = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'Form',
                        i: null,
                        _: 'FORM'
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewForms, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.forms',
                title: 'Forms',
                onAction: this._onAction,
                icon: 'fal fa-clipboard',
                onRowDblClick: this._onSelectForm,
                actions: ACTIONS,
                rowActions: ROW_ACTIONS,
                columns: COLUMNS });
        }
    }]);
    return ViewForms;
}(React.Component);

exports.default = ViewForms;

/***/ }),
/* 313 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'New Feed']];

var ViewFeed = function (_React$Component) {
    (0, _inherits3.default)(ViewFeed, _React$Component);

    function ViewFeed(props) {
        (0, _classCallCheck3.default)(this, ViewFeed);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewFeed.__proto__ || (0, _getPrototypeOf2.default)(ViewFeed)).call(this, props));

        _this._action = function (action) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'FEED',
                        _: 'FEED',
                        i: null
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewFeed, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Feeds'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true,
                        onAction: this._action })
                ),
                React.createElement(_layout.Block, null)
            );
        }
    }]);
    return ViewFeed;
}(React.Component);

exports.default = ViewFeed;

/***/ }),
/* 314 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewWorkflows = function (_React$Component) {
    (0, _inherits3.default)(ViewWorkflows, _React$Component);

    function ViewWorkflows(props) {
        (0, _classCallCheck3.default)(this, ViewWorkflows);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewWorkflows.__proto__ || (0, _getPrototypeOf2.default)(ViewWorkflows)).call(this, props));

        _this._action = function (action) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        _: 'WORKFLOW',
                        n: 'Workflow',
                        i: null
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewWorkflows, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.workflows',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'Workflows',
                icon: 'fa-sitemap',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewWorkflows;
}(React.Component);

exports.default = ViewWorkflows;

/***/ }),
/* 315 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewWikis = function (_React$Component) {
    (0, _inherits3.default)(ViewWikis, _React$Component);

    function ViewWikis(props) {
        (0, _classCallCheck3.default)(this, ViewWikis);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewWikis.__proto__ || (0, _getPrototypeOf2.default)(ViewWikis)).call(this, props));

        _this._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'WIKI',
                        _: 'WIKI',
                        i: null
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewWikis, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.wikis',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'Wikis',
                icon: 'fa-graduation-cap',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewWikis;
}(React.Component);

exports.default = ViewWikis;

/***/ }),
/* 316 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewSites = function (_React$Component) {
    (0, _inherits3.default)(ViewSites, _React$Component);

    function ViewSites(props) {
        (0, _classCallCheck3.default)(this, ViewSites);
        return (0, _possibleConstructorReturn3.default)(this, (ViewSites.__proto__ || (0, _getPrototypeOf2.default)(ViewSites)).call(this, props));
    }

    (0, _createClass3.default)(ViewSites, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.sites',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'Sites',
                icon: 'fa-sitemap',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewSites;
}(React.Component);

exports.default = ViewSites;

/***/ }),
/* 317 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewDevices = function (_React$Component) {
    (0, _inherits3.default)(ViewDevices, _React$Component);

    function ViewDevices(props) {
        (0, _classCallCheck3.default)(this, ViewDevices);
        return (0, _possibleConstructorReturn3.default)(this, (ViewDevices.__proto__ || (0, _getPrototypeOf2.default)(ViewDevices)).call(this, props));
    }

    (0, _createClass3.default)(ViewDevices, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.devices',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'Devices',
                icon: 'fa-mobile',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewDevices;
}(React.Component);

exports.default = ViewDevices;

/***/ }),
/* 318 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewGateways = function (_React$Component) {
    (0, _inherits3.default)(ViewGateways, _React$Component);

    function ViewGateways(props) {
        (0, _classCallCheck3.default)(this, ViewGateways);
        return (0, _possibleConstructorReturn3.default)(this, (ViewGateways.__proto__ || (0, _getPrototypeOf2.default)(ViewGateways)).call(this, props));
    }

    (0, _createClass3.default)(ViewGateways, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.gateways',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'SMS Gateways',
                icon: 'fa-server',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewGateways;
}(React.Component);

exports.default = ViewGateways;

/***/ }),
/* 319 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(78);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name']];

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var ViewDocuments = function (_React$Component) {
    (0, _inherits3.default)(ViewDocuments, _React$Component);

    function ViewDocuments(props) {
        (0, _classCallCheck3.default)(this, ViewDocuments);
        return (0, _possibleConstructorReturn3.default)(this, (ViewDocuments.__proto__ || (0, _getPrototypeOf2.default)(ViewDocuments)).call(this, props));
    }

    (0, _createClass3.default)(ViewDocuments, [{
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                resource: 'com.sonoma.documents',
                columns: COLUMNS,
                actions: ACTIONS,
                title: 'Documents',
                icon: 'fa-book',
                onAction: this._action,
                rowActions: [] });
        }
    }]);
    return ViewDocuments;
}(React.Component);

exports.default = ViewDocuments;

/***/ }),
/* 320 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _constants = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Widget = function (_React$Component) {
    (0, _inherits3.default)(Widget, _React$Component);

    function Widget(props) {
        (0, _classCallCheck3.default)(this, Widget);
        return (0, _possibleConstructorReturn3.default)(this, (Widget.__proto__ || (0, _getPrototypeOf2.default)(Widget)).call(this, props));
    }

    (0, _createClass3.default)(Widget, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'widget' },
                this.props.children
            );
        }
    }]);
    return Widget;
}(React.Component);

var ViewOverview = function (_React$Component2) {
    (0, _inherits3.default)(ViewOverview, _React$Component2);

    function ViewOverview(props) {
        (0, _classCallCheck3.default)(this, ViewOverview);
        return (0, _possibleConstructorReturn3.default)(this, (ViewOverview.__proto__ || (0, _getPrototypeOf2.default)(ViewOverview)).call(this, props));
    }

    (0, _createClass3.default)(ViewOverview, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-tachometer' }),
                        '\xA0Account Overview'
                    )
                ),
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Vbox,
                        { width: '33.33%' },
                        React.createElement(
                            Widget,
                            null,
                            React.createElement(
                                'h3',
                                null,
                                'Account Overview'
                            ),
                            React.createElement(
                                'dl',
                                { className: 'meta' },
                                React.createElement(
                                    'dt',
                                    null,
                                    'Records'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '1,000,000'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Users'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '1234'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Storage size (MB)'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '1,000MB'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Locations'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '123'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Forms'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '12'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    '# of reporting locations'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '12'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Emails sent'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '1.5k'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Devices'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '1,000'
                                ),
                                React.createElement(
                                    'dt',
                                    null,
                                    'Organizations'
                                ),
                                React.createElement(
                                    'dd',
                                    null,
                                    '16'
                                )
                            )
                        )
                    ),
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            Widget,
                            null,
                            React.createElement(
                                'h4',
                                null,
                                'Form Submissions'
                            )
                        ),
                        React.createElement(
                            Widget,
                            null,
                            React.createElement(
                                'h4',
                                null,
                                'User Onboarding'
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewOverview;
}(React.Component);

exports.default = ViewOverview;

/***/ }),
/* 321 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(75);

var _c4 = _interopRequireDefault(_c3);

var _form = __webpack_require__(322);

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewAccount = function (_React$Component) {
    (0, _inherits3.default)(ViewAccount, _React$Component);

    function ViewAccount(props) {
        (0, _classCallCheck3.default)(this, ViewAccount);
        return (0, _possibleConstructorReturn3.default)(this, (ViewAccount.__proto__ || (0, _getPrototypeOf2.default)(ViewAccount)).call(this, props));
    }

    (0, _createClass3.default)(ViewAccount, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Account Settings'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS,
                        onAction: this._action })
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(_c4.default, {
                        definition: _form2.default })
                )
            );
        }
    }]);
    return ViewAccount;
}(React.Component);

exports.default = ViewAccount;

/***/ }),
/* 322 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var FORM_ACCOUNT = [{
    _: -1,
    l: 'General Settings',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-cog',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 0,
    l: 'Account name',
    t: _constants.FIELDS.TEXT,
    n: 'acc_name'
}, {
    _: 1,
    l: 'Account status',
    t: _constants.FIELDS.SELECT,
    n: 'acc_status',
    o: [[0, 'Inactive'], [1, 'Active']]
}, {
    _: 2,
    l: 'Domain',
    t: _constants.FIELDS.DOMAIN,
    n: 'acc_domain'
}, {
    _: 3,
    l: 'Internal domain',
    t: _constants.FIELDS.DOMAIN,
    n: 'acc_temp_domain'
}];

exports.default = FORM_ACCOUNT;

/***/ }),
/* 323 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(75);

var _c4 = _interopRequireDefault(_c3);

var _form = __webpack_require__(324);

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewTheme = function (_React$Component) {
    (0, _inherits3.default)(ViewTheme, _React$Component);

    function ViewTheme(props) {
        (0, _classCallCheck3.default)(this, ViewTheme);
        return (0, _possibleConstructorReturn3.default)(this, (ViewTheme.__proto__ || (0, _getPrototypeOf2.default)(ViewTheme)).call(this, props));
    }

    (0, _createClass3.default)(ViewTheme, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Theme'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS,
                        onAction: this._action })
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(_c4.default, {
                        definition: _form2.default })
                )
            );
        }
    }]);
    return ViewTheme;
}(React.Component);

exports.default = ViewTheme;

/***/ }),
/* 324 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var FORM_THEME = [{
    _: -1,
    l: 'General Theme Settings',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-cog',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 0,
    l: 'Account name',
    t: _constants.FIELDS.TEXT,
    n: 'acc_name'
}, {
    _: 1,
    t: _constants.FIELDS.LABEL,
    l: 'Colours',
    icon: 'fa-paint-brush',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 2,
    t: _constants.FIELDS.COLOR,
    l: 'Header background colour',
    n: 'col_head_bgd'
}, {
    _: 3,
    t: _constants.FIELDS.COLOR,
    l: 'Default text colour',
    n: 'col_text'
}, {
    _: 4,
    t: _constants.FIELDS.COLOR,
    l: 'Default button colour',
    n: 'col_button'
}];

exports.default = FORM_THEME;

/***/ }),
/* 325 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(75);

var _c4 = _interopRequireDefault(_c3);

var _form = __webpack_require__(326);

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewNotifications = function (_React$Component) {
    (0, _inherits3.default)(ViewNotifications, _React$Component);

    function ViewNotifications(props) {
        (0, _classCallCheck3.default)(this, ViewNotifications);
        return (0, _possibleConstructorReturn3.default)(this, (ViewNotifications.__proto__ || (0, _getPrototypeOf2.default)(ViewNotifications)).call(this, props));
    }

    (0, _createClass3.default)(ViewNotifications, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Email & Notifications'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS,
                        onAction: this._action })
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(_c4.default, {
                        definition: _form2.default })
                )
            );
        }
    }]);
    return ViewNotifications;
}(React.Component);

exports.default = ViewNotifications;

/***/ }),
/* 326 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var FORM_THEME = [{
    _: -1,
    l: 'Mail settings',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-cog',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 0,
    l: 'Mail driver',
    t: _constants.FIELDS.SELECT,
    n: 'acc_name',
    o: [['SONOMA', 'Sonoma (default)'], ['SES', 'Amazon SES'], ['SMTP', 'SMTP'], ['MAILGUN', 'Mailgun'], ['SENDMAIL', 'Sendmail'], ['DISABLED', 'Disabled']]
}, {
    _: 1.1,
    l: 'Amazon AWS Key',
    t: _constants.FIELDS.TEXT,
    n: 'AWS_KEY'
}, {
    _: 1.2,
    l: 'Amazon AWS Secret',
    t: _constants.FIELDS.TEXT,
    n: 'AWS_SECRET'
}, {
    _: 1.3,
    l: 'Amazon send address',
    t: _constants.FIELDS.EMAIL,
    n: 'AWS_SEND_ADDRESS'
}, {
    _: 3,
    l: 'SMTP host',
    t: _constants.FIELDS.TEXT,
    n: 'SMTP_HOST'
}, {
    _: 4,
    l: 'SMTP user',
    t: _constants.FIELDS.TEXT,
    n: 'SMTP_USER'
}, {
    _: 5,
    l: 'SMTP password',
    t: _constants.FIELDS.PASSWORD,
    n: 'SMTP_PASS'
}, {
    _: 6,
    l: 'SMTP send name',
    t: _constants.FIELDS.TEXT,
    n: 'SMTP_SEND_NAME'
}, {
    _: 7,
    l: 'SMTP reply-to',
    t: _constants.FIELDS.TEXT,
    n: 'SMTP_REPLY_TO'
}];

exports.default = FORM_THEME;

/***/ }),
/* 327 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(75);

var _c4 = _interopRequireDefault(_c3);

var _form = __webpack_require__(328);

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewSecurity = function (_React$Component) {
    (0, _inherits3.default)(ViewSecurity, _React$Component);

    function ViewSecurity(props) {
        (0, _classCallCheck3.default)(this, ViewSecurity);
        return (0, _possibleConstructorReturn3.default)(this, (ViewSecurity.__proto__ || (0, _getPrototypeOf2.default)(ViewSecurity)).call(this, props));
    }

    (0, _createClass3.default)(ViewSecurity, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Email & Notifications'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS,
                        onAction: this._action })
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(_c4.default, {
                        definition: _form2.default })
                )
            );
        }
    }]);
    return ViewSecurity;
}(React.Component);

exports.default = ViewSecurity;

/***/ }),
/* 328 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(10);

var FORM_SECURITY = [{
    _: -1,
    l: 'General Security Settings',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-lock',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: -0.1,
    l: 'Authentication system',
    t: _constants.FIELDS.SELECT,
    n: 'REG_MODE',
    o: [['DEFAULT', 'Sonoma (default)'], ['LDAP', 'LDAP Authentication']]
}, {
    _: 0,
    l: 'Registration enabled',
    n: 'REG_ENABLED',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 1,
    l: 'Email verification required',
    n: 'REG_VERIFICATION',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 2,
    l: 'Restrict registration domains',
    n: 'REG_DOMAINS',
    t: _constants.FIELDS.TEXT
}, {
    _: 3,
    l: 'Two-factor authentication',
    n: '2FACTOR',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 3.1,
    l: 'Mobile access',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 3.2,
    l: 'Desktop access',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 3.3,
    l: 'Password recovery',
    t: _constants.FIELDS.SELECT,
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 3.4,
    l: 'Password recovery token expiry (days)',
    t: _constants.FIELDS.NUMERIC,
    n: 'SEC_TOKEN_EXPIRY'
}, {
    _: 3.5,
    l: 'Persistent sessions',
    t: _constants.FIELDS.SELECT,
    n: 'SEC_PERSIST_SESSIONS',
    o: [[0, 'Disabled'], [1, 'Enabled']]
}, {
    _: 3.6,
    l: 'Session expiry (days)',
    t: _constants.FIELDS.NUMERIC,
    n: 'SEC_SESSION_EXPIRY'
}, {
    _: 4,
    l: 'Backups',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-database',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}, {
    _: 5,
    l: 'LDAP Integration',
    t: _constants.FIELDS.LABEL,
    icon: 'fa-lock',
    style: {
        padding: '8px',
        marginBottom: '8px',
        color: _constants.COLORS.snow0,
        fontSize: '18px',
        marginTop: '16px',
        background: _constants.COLORS.frost3,
        borderTop: '1px solid ' + _constants.COLORS.polar0
    }
}];

exports.default = FORM_SECURITY;

/***/ }),
/* 329 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'ADD', 'New role']];

var ROLE_ACTIONS = [['fa-pencil', 'EDIT', 'Edit role'], ['fa-trash', 'DELETE', 'Delete role'], ['fa-copy', 'COPY', 'Copy role']].reverse();

var ROLE_LOCKED_ACTIONS = [['fa-copy', 'COPY', 'Copy role']];

var DEFAULT_ROLES = [{ name: 'Super Administrator', locked: true }, { name: 'Account Administrator', locked: true }, { name: 'Geographic Administrator', locked: true }, { name: 'Organization Administrator', locked: true }, { name: 'Reporting User', locked: true }, { name: 'Reader', locked: false }];

var RoleItem = function (_React$Component) {
    (0, _inherits3.default)(RoleItem, _React$Component);

    function RoleItem(props) {
        (0, _classCallCheck3.default)(this, RoleItem);
        return (0, _possibleConstructorReturn3.default)(this, (RoleItem.__proto__ || (0, _getPrototypeOf2.default)(RoleItem)).call(this, props));
    }

    (0, _createClass3.default)(RoleItem, [{
        key: 'render',
        value: function render() {
            var locked = false;
            if (this.props.data.locked) locked = true;

            var name = this.props.data.name;
            if (locked) name += ' (locked)';

            var actions = ROLE_ACTIONS;
            if (locked) actions = ROLE_LOCKED_ACTIONS;

            return React.createElement(
                'div',
                { className: 'dash-item' },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { addClass: 'text-block' },
                        name
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(_c2.default, {
                            actions: actions,
                            onAction: this._action,
                            right: true })
                    )
                )
            );
        }
    }]);
    return RoleItem;
}(React.Component);

var ViewRoles = function (_React$Component2) {
    (0, _inherits3.default)(ViewRoles, _React$Component2);

    function ViewRoles(props) {
        (0, _classCallCheck3.default)(this, ViewRoles);
        return (0, _possibleConstructorReturn3.default)(this, (ViewRoles.__proto__ || (0, _getPrototypeOf2.default)(ViewRoles)).call(this, props));
    }

    (0, _createClass3.default)(ViewRoles, [{
        key: 'render',
        value: function render() {

            var items = DEFAULT_ROLES.map(function (item) {
                return React.createElement(RoleItem, { data: item });
            });

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { addClass: 'tbar', borderBottom: true, height: '30px' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-users' }),
                        '\xA0Roles'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        onAction: this._action,
                        actions: ACTIONS })
                ),
                React.createElement(
                    _layout.Block,
                    { style: { padding: '16px' } },
                    items
                )
            );
        }
    }]);
    return ViewRoles;
}(React.Component);

exports.default = ViewRoles;

/***/ }),
/* 330 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'ADD', 'Add menu item']];

var ViewMenu = function (_React$Component) {
    (0, _inherits3.default)(ViewMenu, _React$Component);

    function ViewMenu(props) {
        (0, _classCallCheck3.default)(this, ViewMenu);
        return (0, _possibleConstructorReturn3.default)(this, (ViewMenu.__proto__ || (0, _getPrototypeOf2.default)(ViewMenu)).call(this, props));
    }

    (0, _createClass3.default)(ViewMenu, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Menu Manager'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        onAction: this._action,
                        right: true })
                ),
                React.createElement(_layout.Block, null)
            );
        }
    }]);
    return ViewMenu;
}(React.Component);

exports.default = ViewMenu;

/***/ }),
/* 331 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(78);

var _c4 = _interopRequireDefault(_c3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COLUMNS = [['name', 'Name'], ['email', 'Email'], ['role', 'Role'], ['location', 'Location'], ['organization', 'Organization']];

var ACTIONS = [['fa-plus', 'CREATE', 'Create a new user']];

var ROW_ACTIONS = [['fa-pencil', 'EDIT', 'Edit user'], ['fa-trash', 'REVOKE', 'Revoke user access'], ['fa-clipboard', 'ASSIGNMENTS', 'Modify assignments']];

var ViewUsers = function (_React$Component) {
    (0, _inherits3.default)(ViewUsers, _React$Component);

    function ViewUsers(props) {
        (0, _classCallCheck3.default)(this, ViewUsers);
        return (0, _possibleConstructorReturn3.default)(this, (ViewUsers.__proto__ || (0, _getPrototypeOf2.default)(ViewUsers)).call(this, props));
    }

    (0, _createClass3.default)(ViewUsers, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(_c4.default, {
                    title: 'Users',
                    icon: 'fa-user',
                    resource: 'com.sonoma.users',
                    actions: ACTIONS,
                    rowActions: ROW_ACTIONS,
                    columns: COLUMNS })
            );
        }
    }]);
    return ViewUsers;
}(React.Component);

exports.default = ViewUsers;

/***/ }),
/* 332 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'New alarm']];

var ViewAlarms = function (_React$Component) {
    (0, _inherits3.default)(ViewAlarms, _React$Component);

    function ViewAlarms(props) {
        (0, _classCallCheck3.default)(this, ViewAlarms);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewAlarms.__proto__ || (0, _getPrototypeOf2.default)(ViewAlarms)).call(this, props));

        _this._action = function (action) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'ALARM',
                        _: 'ALARM',
                        i: null
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewAlarms, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Alarms'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        onAction: this._action,
                        right: true })
                ),
                React.createElement(_layout.Block, null)
            );
        }
    }]);
    return ViewAlarms;
}(React.Component);

exports.default = ViewAlarms;

/***/ }),
/* 333 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new']];

var ViewComponents = function (_React$Component) {
    (0, _inherits3.default)(ViewComponents, _React$Component);

    function ViewComponents(props) {
        (0, _classCallCheck3.default)(this, ViewComponents);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewComponents.__proto__ || (0, _getPrototypeOf2.default)(ViewComponents)).call(this, props));

        _this._action = function (action) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'COMPONENT',
                        _: 'COMPONENT',
                        i: null
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(ViewComponents, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Components'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        onAction: this._action,
                        right: true })
                ),
                React.createElement('block', null)
            );
        }
    }]);
    return ViewComponents;
}(React.Component);

exports.default = ViewComponents;

/***/ }),
/* 334 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _cLocation = __webpack_require__(335);

var _cLocation2 = _interopRequireDefault(_cLocation);

var _view = __webpack_require__(336);

var _view2 = _interopRequireDefault(_view);

var _state = __webpack_require__(136);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-ellipsis-v', 'NOOP'], ['fa-upload', 'IMPORT'], ['fa-download', 'EXPORT']].reverse();

var ViewLocationDashboard = function (_React$Component) {
    (0, _inherits3.default)(ViewLocationDashboard, _React$Component);

    function ViewLocationDashboard(props) {
        (0, _classCallCheck3.default)(this, ViewLocationDashboard);
        return (0, _possibleConstructorReturn3.default)(this, (ViewLocationDashboard.__proto__ || (0, _getPrototypeOf2.default)(ViewLocationDashboard)).call(this, props));
    }

    (0, _createClass3.default)(ViewLocationDashboard, [{
        key: 'render',
        value: function render() {
            return React.createElement(_layout.Block, null);
        }
    }]);
    return ViewLocationDashboard;
}(React.Component);

var TabLocations = function (_React$Component2) {
    (0, _inherits3.default)(TabLocations, _React$Component2);

    function TabLocations(props) {
        (0, _classCallCheck3.default)(this, TabLocations);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (TabLocations.__proto__ || (0, _getPrototypeOf2.default)(TabLocations)).call(this, props));

        _this2._onChange = function () {
            _this2.setState(_state.component_state);
        };

        _this2._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.component_state.editLocation({});
                    break;
                default:
                    break;
            }
        };

        _this2.state = new _state.LocationsState();
        return _this2;
    }

    (0, _createClass3.default)(TabLocations, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            _state.component_state.on('VIEW_CHANGE', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            _state.component_state.off("VIEW_CHANGE", this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {

            var view = React.createElement(ViewLocationDashboard, null);
            if (_state.component_state.location) view = React.createElement(_view2.default, null);

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', addClass: 'tbar', borderBottom: true },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-map marker' }),
                        '\xA0Locations'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        onAction: this._action,
                        actions: ACTIONS })
                ),
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Vbox,
                        { width: '33.33%', borderRight: true },
                        React.createElement(_cLocation2.default, null)
                    ),
                    view
                )
            );
        }
    }]);
    return TabLocations;
}(React.Component);

exports.default = TabLocations;

/***/ }),
/* 335 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(34);

var _c4 = _interopRequireDefault(_c3);

var _state = __webpack_require__(136);

var _api = __webpack_require__(48);

var _api2 = _interopRequireDefault(_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-map-marker', 'HIERARCHICAL'], ['fa-tag', 'GROUPS'], ['fa-cube', 'TYPE']];

var VIEW_OPTIONS = [['HIERARCH', 'Hierarchical'], ['GROUPS', 'Group'], ['TYPE', 'Location type'], ['ARCHIVED', 'Archived locations']];

var LocationTreeNode = function (_React$Component) {
    (0, _inherits3.default)(LocationTreeNode, _React$Component);

    function LocationTreeNode(props) {
        (0, _classCallCheck3.default)(this, LocationTreeNode);

        var _this = (0, _possibleConstructorReturn3.default)(this, (LocationTreeNode.__proto__ || (0, _getPrototypeOf2.default)(LocationTreeNode)).call(this, props));

        _this._onClick = function () {
            _state.component_state.editLocation(_this.props.data);
        };

        _this.state = {
            showChilds: false,
            hasChilds: false
        };

        return _this;
    }

    (0, _createClass3.default)(LocationTreeNode, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'tree-node' },
                React.createElement(
                    _layout.Hbox,
                    { onClick: this._onClick },
                    React.createElement(
                        _layout.Block,
                        { width: '20px' },
                        React.createElement('i', { className: 'fal fa-caret-right' })
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        this.props.data.name,
                        ' (',
                        this.props.data.lti_name,
                        ')'
                    )
                )
            );
        }
    }]);
    return LocationTreeNode;
}(React.Component);

var LocationTree = function (_React$Component2) {
    (0, _inherits3.default)(LocationTree, _React$Component2);

    function LocationTree(props) {
        (0, _classCallCheck3.default)(this, LocationTree);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (LocationTree.__proto__ || (0, _getPrototypeOf2.default)(LocationTree)).call(this, props));

        _this2._onChangeView = function (view) {
            _this2.setState({ view: view });
        };

        _this2.state = {
            view: 'HIERARCH',
            search: '',
            data: []
        };

        (0, _api2.default)("com.sonoma.locations", []).then(function (resp) {
            _this2.setState({
                data: resp.d
            });
        });
        return _this2;
    }

    (0, _createClass3.default)(LocationTree, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'right', style: { marginTop: '8px', marginRight: '8px' } },
                        React.createElement(_c4.default, {
                            value: this.state.view,
                            onClick: this._onChangeView,
                            options: VIEW_OPTIONS })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { height: '47px' },
                    React.createElement(
                        'div',
                        { className: 'search-wrapper' },
                        React.createElement('input', {
                            value: this.state.search,
                            placeholder: 'Search locations...',
                            type: 'text',
                            className: 'search-input' })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    this.state.data.map(function (item) {
                        return React.createElement(LocationTreeNode, { data: item });
                    })
                )
            );
        }
    }]);
    return LocationTree;
}(React.Component);

;

exports.default = LocationTree;

/***/ }),
/* 336 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(337);

var _c4 = _interopRequireDefault(_c3);

var _c5 = __webpack_require__(75);

var _c6 = _interopRequireDefault(_c5);

var _form = __webpack_require__(143);

var _form2 = _interopRequireDefault(_form);

var _viewLocation = __webpack_require__(338);

var _viewLocation2 = _interopRequireDefault(_viewLocation);

var _viewLocation3 = __webpack_require__(339);

var _viewLocation4 = _interopRequireDefault(_viewLocation3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE', 'Save change(s)'], ['fa-ellipsis-v', 'NOOP'], ['fa-code-merge', 'MERGE', 'Merge location'], ['fa-cube', 'ARCHIVE', 'Archive location'], ['fa-ellipsis-v', 'NOOP2'], ['fa-trash', 'DELETE', 'Delete location']].reverse();

var ViewLocation = function (_React$Component) {
    (0, _inherits3.default)(ViewLocation, _React$Component);

    function ViewLocation(props) {
        (0, _classCallCheck3.default)(this, ViewLocation);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewLocation.__proto__ || (0, _getPrototypeOf2.default)(ViewLocation)).call(this, props));

        _this._action = function (action, data) {
            console.log(action);
        };

        _this.state = {
            view: 'SETTINGS'
        };
        return _this;
    }

    (0, _createClass3.default)(ViewLocation, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            var view = void 0;
            if (this.state.view == 'SETTINGS') view = React.createElement(_viewLocation2.default, null);
            if (this.state.view == 'REPORTING') view = React.createElement(_viewLocation4.default, null);

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(_c4.default, {
                        active: this.state.view == 'SETTINGS',
                        onClick: function onClick() {
                            _this2.setState({ view: 'SETTINGS' });
                        },
                        label: 'Settings',
                        icon: 'fa-cog' }),
                    React.createElement(_c4.default, {
                        active: this.state.view == 'POPULATION',
                        onClick: function onClick() {
                            _this2.setState({ view: 'POPULATION' });
                        },
                        label: 'Population',
                        icon: 'fa-users' }),
                    React.createElement(_c4.default, {
                        active: this.state.view == 'REPORTING',
                        onClick: function onClick() {
                            _this2.setState({ view: 'REPORTING' });
                        },
                        label: 'Reporting',
                        icon: 'fa-clipboard' }),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true,
                        onAction: this._action })
                ),
                view
            );
        }
    }]);
    return ViewLocation;
}(React.Component);

exports.default = ViewLocation;

/***/ }),
/* 337 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tab = function (_React$Component) {
    (0, _inherits3.default)(Tab, _React$Component);

    function Tab(props) {
        (0, _classCallCheck3.default)(this, Tab);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Tab.__proto__ || (0, _getPrototypeOf2.default)(Tab)).call(this, props));

        _this._setTab = function () {
            _this.props.onClick(_this.props.data);
        };

        return _this;
    }

    (0, _createClass3.default)(Tab, [{
        key: 'render',
        value: function render() {
            var className = "ind-tab";
            if (this.props.active) className += ' active';

            return React.createElement(
                'div',
                { className: className, onClick: this._setTab },
                React.createElement(
                    _layout.Hbox,
                    { onClick: this._setTab },
                    React.createElement(
                        _layout.Block,
                        { width: '20px', addClass: 'icon' },
                        React.createElement('i', { className: "fal " + this.props.icon })
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        this.props.label
                    )
                )
            );
        }
    }]);
    return Tab;
}(React.Component);

exports.default = Tab;

/***/ }),
/* 338 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _c = __webpack_require__(75);

var _c2 = _interopRequireDefault(_c);

var _form = __webpack_require__(143);

var _form2 = _interopRequireDefault(_form);

var _state = __webpack_require__(136);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewLocationSettings = function (_React$Component) {
    (0, _inherits3.default)(ViewLocationSettings, _React$Component);

    function ViewLocationSettings(props) {
        (0, _classCallCheck3.default)(this, ViewLocationSettings);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewLocationSettings.__proto__ || (0, _getPrototypeOf2.default)(ViewLocationSettings)).call(this, props));

        _this._onChange = function () {
            _this.setState({ data: _state.component_state.location });
        };

        _this.state = { data: _state.component_state.location };
        return _this;
    }

    (0, _createClass3.default)(ViewLocationSettings, [{
        key: 'componenWillMount',
        value: function componenWillMount() {
            _state.component_state.on("LOC_PROP_CHANGE", this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            _state.component_state.off("LOC_PROP_CHANGE", this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(_c2.default, {
                data: this.state.data,
                definition: _form2.default });
        }
    }]);
    return ViewLocationSettings;
}(React.Component);

exports.default = ViewLocationSettings;

/***/ }),
/* 339 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewLocationReporting = function (_React$Component) {
    (0, _inherits3.default)(ViewLocationReporting, _React$Component);

    function ViewLocationReporting(props) {
        (0, _classCallCheck3.default)(this, ViewLocationReporting);
        return (0, _possibleConstructorReturn3.default)(this, (ViewLocationReporting.__proto__ || (0, _getPrototypeOf2.default)(ViewLocationReporting)).call(this, props));
    }

    (0, _createClass3.default)(ViewLocationReporting, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { style: { padding: '8px' } },
                    React.createElement(
                        'table',
                        { className: 'reporting-table' },
                        React.createElement(
                            'thead',
                            null,
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    '2016-01-01'
                                )
                            )
                        ),
                        React.createElement(
                            'tbody',
                            null,
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'td',
                                    null,
                                    '\xA0'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    '\xA0'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'x'
                                )
                            )
                        )
                    )
                ),
                React.createElement(_layout.Block, { height: '30px', addClass: 'tbar', borderTop: true, borderBottom: true }),
                React.createElement(_layout.Block, { height: '75%' })
            );
        }
    }]);
    return ViewLocationReporting;
}(React.Component);

exports.default = ViewLocationReporting;

/***/ })
/******/ ]);
//# sourceMappingURL=admin.map