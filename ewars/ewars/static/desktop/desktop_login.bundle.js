/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 424);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(79), __esModule: true };

/***/ }),

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ROLES = exports.COLORS = exports.FIELDS = undefined;

var _const = __webpack_require__(76);

var _const2 = _interopRequireDefault(_const);

var _const3 = __webpack_require__(33);

var _const4 = _interopRequireDefault(_const3);

var _const5 = __webpack_require__(117);

var _const6 = _interopRequireDefault(_const5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FIELDS = _const2.default;
exports.COLORS = _const4.default;
exports.ROLES = _const6.default;

/***/ }),

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('asyncIterator');


/***/ }),

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('observable');


/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(103), __esModule: true };

/***/ }),

/***/ 103:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(104);
module.exports = __webpack_require__(8).Object.setPrototypeOf;


/***/ }),

/***/ 104:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(12);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(105).set });


/***/ }),

/***/ 105:
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(16);
var anObject = __webpack_require__(13);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(23)(Function.call, __webpack_require__(60).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ 106:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(107), __esModule: true };

/***/ }),

/***/ 107:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(108);
var $Object = __webpack_require__(8).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),

/***/ 108:
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(43) });


/***/ }),

/***/ 109:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Hbox = function (_React$Component) {
    (0, _inherits3.default)(Hbox, _React$Component);

    function Hbox(props) {
        (0, _classCallCheck3.default)(this, Hbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Hbox.__proto__ || (0, _getPrototypeOf2.default)(Hbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Hbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxHeight = this.props.height || null;
            style.padding = this.props.padding || null;

            var className = "hbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Hbox;
}(React.Component);

Hbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Hbox;

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(40)('wks');
var uid = __webpack_require__(26);
var Symbol = __webpack_require__(9).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ 110:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(111);
module.exports = __webpack_require__(8).Object.assign;


/***/ }),

/***/ 111:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(12);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(112) });


/***/ }),

/***/ 112:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
var toObject = __webpack_require__(32);
var IObject = __webpack_require__(58);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(19)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),

/***/ 113:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Vbox = function (_React$Component) {
    (0, _inherits3.default)(Vbox, _React$Component);

    function Vbox(props) {
        (0, _classCallCheck3.default)(this, Vbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Vbox.__proto__ || (0, _getPrototypeOf2.default)(Vbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Vbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxWidth = this.props.width || null;
            style.padding = this.props.padding || null;

            var className = "vbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Vbox;
}(React.Component);

Vbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Vbox;

/***/ }),

/***/ 114:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Panel = function (_React$Component) {
    (0, _inherits3.default)(Panel, _React$Component);

    function Panel(props) {
        (0, _classCallCheck3.default)(this, Panel);
        return (0, _possibleConstructorReturn3.default)(this, (Panel.__proto__ || (0, _getPrototypeOf2.default)(Panel)).call(this, props));
    }

    (0, _createClass3.default)(Panel, [{
        key: "render",
        value: function render() {
            var style = {};
            if (this.props.style) {
                for (var i in this.props.style) {
                    style[i] = this.props.style[i];
                }
            }
            return React.createElement(
                "div",
                { className: "ide-panel", style: style },
                this.props.children
            );
        }
    }]);
    return Panel;
}(React.Component);

exports.default = Panel;

/***/ }),

/***/ 115:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Block = function (_React$Component) {
    (0, _inherits3.default)(Block, _React$Component);

    function Block(props) {
        (0, _classCallCheck3.default)(this, Block);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Block.__proto__ || (0, _getPrototypeOf2.default)(Block)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Block, [{
        key: "render",
        value: function render() {
            var style = {};
            style.maxHeight = this.props.height || null;
            style.maxWidth = this.props.width || null;

            style.padding = this.props.padding || null;

            var className = "block";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";
            className += this.props.yO ? " overY " : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            var click = void 0;
            if (this.props.onClick) click = this._onClick;

            return React.createElement(
                "div",
                { className: className, style: style, onClick: click, id: this.props.id },
                this.props.children
            );
        }
    }]);
    return Block;
}(React.Component);

Block.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {},
    yO: false,
    id: null
};
exports.default = Block;

/***/ }),

/***/ 117:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var ROLES = {
    USER: "Reporting user",
    ACCOUNT_ADMIN: "Account admin",
    REGIONAL_ADMIN: "Geo. admin"
};

exports.default = ROLES;

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var ctx = __webpack_require__(23);
var hide = __webpack_require__(18);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ 138:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(139), __esModule: true };

/***/ }),

/***/ 139:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(140);
module.exports = __webpack_require__(8).Object.keys;


/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(13);
var IE8_DOM_DEFINE = __webpack_require__(53);
var toPrimitive = __webpack_require__(41);
var dP = Object.defineProperty;

exports.f = __webpack_require__(15) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ 140:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(32);
var $keys = __webpack_require__(22);

__webpack_require__(67)('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(19)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ 17:
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var createDesc = __webpack_require__(27);
module.exports = __webpack_require__(15) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ 19:
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(58);
var defined = __webpack_require__(38);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ 21:
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ 22:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(57);
var enumBugKeys = __webpack_require__(44);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(36);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ 24:
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(110), __esModule: true };

/***/ }),

/***/ 26:
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ 27:
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ 28:
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ 29:
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(102);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(106);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(14).f;
var has = __webpack_require__(17);
var TAG = __webpack_require__(11)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(38);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var COLORS = {
    polar0: 'rgb(47, 52, 64)',
    polar1: 'rgb(59, 66, 81)',
    polar2: 'rgb(67, 76, 93)',
    polar3: 'rgb(76, 86, 105)',

    snow0: 'rgb(216, 222, 233)',
    snow1: 'rgb(229, 233, 240)',
    snow2: 'rgb(236, 239, 244)',

    frost0: 'rgb(144, 188, 187)',
    frost1: 'rgb(138, 192, 207)',
    frost2: 'rgb(130, 161, 192)',
    frost3: 'rgb(95, 130, 170)',

    aurora0: 'rgb(190, 97, 107)',
    aurora1: 'rgb(207, 135, 114)',
    aurora2: 'rgb(234, 202, 143)',
    aurora3: 'rgb(164, 189, 142)',
    aurora4: 'rgb(179, 143, 172)'
};

exports.default = COLORS;

/***/ }),

/***/ 36:
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ 38:
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(40)('keys');
var uid = __webpack_require__(26);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(68);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(16);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ 42:
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _view = __webpack_require__(425);

var _view2 = _interopRequireDefault(_view);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.app = {};
document.addEventListener("DOMContentLoaded", function () {
    new QWebChannel(qt.webChannelTransport, function (channel) {
        window.channel = channel;
        window.app.settings = channel.objects.settings || null;
        window.app.auth = channel.objects.auth || null;
        window.app.socket = channel.objects.socket || null;

        ReactDOM.render(React.createElement(_view2.default, null), document.getElementById("app"));
    });
});

/***/ }),

/***/ 425:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _keys = __webpack_require__(138);

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _constants = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var STYLES = {
    TITLE: {
        display: 'block',
        marginBottom: '16px',
        fontSize: '25px',
        color: '#F2F2F2',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: '16px'
    },
    LINKER: {
        display: 'block',
        marginBottom: '5px',
        textAlign: 'center'
    },
    LIST_ITEM: {
        display: 'block',
        margin: '8px',
        background: 'rgba(0,0,0,0.1)',
        padding: '8px',
        userSelect: 'none'
    },
    LABEL: {
        display: 'block',
        padding: '8px 8px 8px 0'
    },
    TEXT_INPUT: {
        clear: 'both',
        width: '100%',
        fontSize: '14px',
        padding: '3px',
        marginBottom: '8px'
    },
    CONTAINER: {},
    HEADER_ITEM: {
        display: 'block',
        margin: '16px 8px 8px 16px',
        textTransform: 'uppercase'
    }
};

var AccountListItem = function (_React$Component) {
    (0, _inherits3.default)(AccountListItem, _React$Component);

    function AccountListItem(props) {
        (0, _classCallCheck3.default)(this, AccountListItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (AccountListItem.__proto__ || (0, _getPrototypeOf2.default)(AccountListItem)).call(this, props));

        _this._setAccount = function () {
            window.app.settings.setAccount(_this.props.data);
        };

        _this._removeAccount = function () {
            window.app.settings.removeAccount(_this.props.data, function () {
                _this.props.reload();
            });
        };

        return _this;
    }

    (0, _createClass3.default)(AccountListItem, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'login-item', onClick: this._setAccount },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'span',
                            { style: { marginBottom: '2px' } },
                            this.props.data.name
                        ),
                        React.createElement(
                            'span',
                            { style: { fontSize: '10px' } },
                            ROLES[this.props.data.role]
                        ),
                        React.createElement(
                            'span',
                            null,
                            this.props.data.email
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { width: '25px', onClick: this._removeAccount, addClass: 'clicky', borderLeft: true },
                        React.createElement('i', { className: 'fal fa-trash' })
                    )
                )
            );
        }
    }]);
    return AccountListItem;
}(React.Component);

var AccountListHeader = function (_React$Component2) {
    (0, _inherits3.default)(AccountListHeader, _React$Component2);

    function AccountListHeader(props) {
        (0, _classCallCheck3.default)(this, AccountListHeader);
        return (0, _possibleConstructorReturn3.default)(this, (AccountListHeader.__proto__ || (0, _getPrototypeOf2.default)(AccountListHeader)).call(this, props));
    }

    (0, _createClass3.default)(AccountListHeader, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { style: STYLES.HEADER_ITEM },
                this.props.data
            );
        }
    }]);
    return AccountListHeader;
}(React.Component);

var ViewTab = function (_React$Component3) {
    (0, _inherits3.default)(ViewTab, _React$Component3);

    function ViewTab(props) {
        (0, _classCallCheck3.default)(this, ViewTab);
        return (0, _possibleConstructorReturn3.default)(this, (ViewTab.__proto__ || (0, _getPrototypeOf2.default)(ViewTab)).call(this, props));
    }

    (0, _createClass3.default)(ViewTab, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                {
                    onClick: this.props.onClick,
                    className: 'vert-tab' + (this.props.active ? ' active' : '') },
                React.createElement('i', { className: 'fal ' + this.props.icon }),
                '\xA0',
                this.props.label
            );
        }
    }]);
    return ViewTab;
}(React.Component);

var ViewRegistration = function (_React$Component4) {
    (0, _inherits3.default)(ViewRegistration, _React$Component4);

    function ViewRegistration(props) {
        (0, _classCallCheck3.default)(this, ViewRegistration);
        return (0, _possibleConstructorReturn3.default)(this, (ViewRegistration.__proto__ || (0, _getPrototypeOf2.default)(ViewRegistration)).call(this, props));
    }

    (0, _createClass3.default)(ViewRegistration, [{
        key: 'render',
        value: function render() {
            return React.createElement(_layout.Hbox, null);
        }
    }]);
    return ViewRegistration;
}(React.Component);

var ViewRegistrationComplete = function (_React$Component5) {
    (0, _inherits3.default)(ViewRegistrationComplete, _React$Component5);

    function ViewRegistrationComplete(props) {
        (0, _classCallCheck3.default)(this, ViewRegistrationComplete);
        return (0, _possibleConstructorReturn3.default)(this, (ViewRegistrationComplete.__proto__ || (0, _getPrototypeOf2.default)(ViewRegistrationComplete)).call(this, props));
    }

    (0, _createClass3.default)(ViewRegistrationComplete, [{
        key: 'render',
        value: function render() {
            return React.createElement(_layout.Hbox, null);
        }
    }]);
    return ViewRegistrationComplete;
}(React.Component);

var ROLES = {
    USER: "Reporting user",
    REGIONAL_ADMIN: "Geographic admin",
    ACCOUNT_ADMIN: "Account admin"
};

var ViewDefault = function (_React$Component6) {
    (0, _inherits3.default)(ViewDefault, _React$Component6);

    function ViewDefault(props) {
        (0, _classCallCheck3.default)(this, ViewDefault);

        var _this6 = (0, _possibleConstructorReturn3.default)(this, (ViewDefault.__proto__ || (0, _getPrototypeOf2.default)(ViewDefault)).call(this, props));

        _this6._reload = function () {
            window.app.settings.getAccounts(function (resp) {
                _this6.setState({
                    accounts: resp
                });
            });
        };

        _this6._showProjects = function () {
            _this6.setState({
                view: 'PROJECTS'
            });
        };

        _this6._showAccounts = function () {
            _this6.setState({
                view: 'ACCOUNTS'
            });
        };

        _this6.state = {
            accounts: [],
            view: 'ACCOUNTS'
        };

        window.app.settings.getAccounts(function (resp) {
            _this6.setState({
                accounts: resp
            });
        });
        return _this6;
    }

    (0, _createClass3.default)(ViewDefault, [{
        key: 'render',
        value: function render() {
            var _this7 = this;

            var items = [];

            this.state.accounts.forEach(function (item) {
                if (items.indexOf(item.account_name) < 0) {
                    items.push(item.account_name);
                }
            });

            items = items.sort(function (a, b) {
                return a.localeCompare(b);
            });

            var renders = [];

            items.forEach(function (account) {
                renders.push(React.createElement(AccountListHeader, {
                    data: account }));

                var users = _this7.state.accounts.filter(function (user) {
                    return user.account_name == account;
                });
                users = users.sort(function (a, b) {
                    return a.name.localeCompare(b.name);
                });

                users.forEach(function (user) {
                    renders.push(React.createElement(AccountListItem, {
                        reload: _this7._reload,
                        data: user }));
                });
            });

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Block,
                    { yO: true, width: '50%', borderRight: true, style: { background: 'rgba(0,0,0,0.1)' } },
                    renders
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(
                        'div',
                        { className: 'login-option top-border', onClick: this.props.onAddAccount },
                        React.createElement(
                            _layout.Hbox,
                            null,
                            React.createElement(
                                _layout.Block,
                                { width: '30px', addClass: 'icon' },
                                React.createElement('i', { className: 'fal fa-plus' })
                            ),
                            React.createElement(
                                _layout.Block,
                                null,
                                React.createElement(
                                    'div',
                                    { className: 'label' },
                                    'Sign in to Account'
                                ),
                                React.createElement(
                                    'p',
                                    null,
                                    'Sign in to an existing EWARS user account'
                                )
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'login-option', onClick: this.props.onRegister },
                        React.createElement(
                            _layout.Hbox,
                            null,
                            React.createElement(
                                _layout.Block,
                                { width: '30px', addClass: 'icon' },
                                React.createElement('i', { className: 'fal fa-list' })
                            ),
                            React.createElement(
                                _layout.Block,
                                null,
                                React.createElement(
                                    'div',
                                    { className: 'label' },
                                    'Register an Account'
                                ),
                                React.createElement(
                                    'p',
                                    null,
                                    'Register for a user account with on EWARS instance.'
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewDefault;
}(React.Component);

var ViewAddAccount = function (_React$Component7) {
    (0, _inherits3.default)(ViewAddAccount, _React$Component7);

    function ViewAddAccount(props) {
        (0, _classCallCheck3.default)(this, ViewAddAccount);

        var _this8 = (0, _possibleConstructorReturn3.default)(this, (ViewAddAccount.__proto__ || (0, _getPrototypeOf2.default)(ViewAddAccount)).call(this, props));

        _this8._authResult = function (data, tt) {
            if (data.success) {
                _this8.props.onComplete(data.tenancies);
            } else {
                _this8.setState({
                    err: data.res
                });
            }
        };

        _this8._cancel = function () {
            _this8.props.onCancel();
        };

        _this8._attemptLogin = function () {
            window.app.socket.loginUser(_this8.state.email, _this8.state.password, function () {});
        };

        _this8.state = {
            email: 'alpha@ewars.ws',
            password: 'omnideadrobot',
            domain: '127.0.0.1:9000',
            res: null
        };

        window.app.socket.nodeRxAuthResult.connect(_this8._authResult);
        return _this8;
    }

    (0, _createClass3.default)(ViewAddAccount, [{
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            window.app.socket.nodeRxAuthResult.disconnect(this._authResult);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this9 = this;

            if (!window.app.socket.isConnected) {
                return React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'p',
                            null,
                            'You must be connected to the internet in order to add a new account.'
                        ),
                        React.createElement(
                            'button',
                            { onClick: this._cancel },
                            React.createElement('i', { className: 'fal fa-times' }),
                            '\xA0Cancel'
                        )
                    )
                );
            }
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(_layout.Block, { width: '25%' }),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(
                        'h1',
                        { style: STYLES.TITLE },
                        'Sign into Account'
                    ),
                    this.state.res ? React.createElement(
                        'p',
                        null,
                        this.state.res
                    ) : null,
                    React.createElement(
                        'label',
                        { htmlFor: '', style: STYLES.LABEL },
                        'Email/Username'
                    ),
                    React.createElement('input', {
                        style: STYLES.TEXT_INPUT,
                        type: 'email',
                        onChange: function onChange(e) {
                            _this9.setState({ email: e.target.value });
                        },
                        value: this.state.email }),
                    React.createElement(
                        'label',
                        { htmlFor: '', style: STYLES.LABEL },
                        'Password'
                    ),
                    React.createElement('input', {
                        onChange: function onChange(e) {
                            _this9.setState({ password: e.target.value });
                        },
                        style: STYLES.TEXT_INPUT,
                        value: this.state.password,
                        type: 'password' }),
                    React.createElement(
                        'label',
                        {
                            style: STYLES.LABEL,
                            htmlFor: '' },
                        'EWARS Instance'
                    ),
                    React.createElement('input', {
                        onChange: function onChange(e) {
                            _this9.setState({ domain: e.target.value });
                        },
                        value: this.state.domain,
                        style: STYLES.TEXT_INPUT,
                        type: 'text' }),
                    React.createElement('div', {
                        className: 'clear',
                        style: { marginBottom: '8px' } }),
                    React.createElement(
                        'button',
                        { onClick: this._attemptLogin },
                        'Add account'
                    ),
                    '\xA0',
                    React.createElement(
                        'button',
                        { onClick: this._cancel },
                        'Cancel'
                    ),
                    '\xA0',
                    React.createElement(
                        'a',
                        { href: '#' },
                        'Forgot Password'
                    )
                ),
                React.createElement(_layout.Block, { width: '25%' })
            );
        }
    }]);
    return ViewAddAccount;
}(React.Component);

var MultiAccountLoginView = function (_React$Component8) {
    (0, _inherits3.default)(MultiAccountLoginView, _React$Component8);

    function MultiAccountLoginView(props) {
        (0, _classCallCheck3.default)(this, MultiAccountLoginView);

        var _this10 = (0, _possibleConstructorReturn3.default)(this, (MultiAccountLoginView.__proto__ || (0, _getPrototypeOf2.default)(MultiAccountLoginView)).call(this, props));

        _this10._toggle = function (token) {
            if (_this10.state.checked.indexOf(token) >= 0) {
                var items = _this10.state.checked;
                items.splice(items.indexOf(token), 1);
                _this10.setState({ checked: items });
            } else {
                var _items = _this10.state.checked;
                _items.push(token);
                _this10.setState({ checked: _items });
            }
        };

        _this10._onAddSelected = function () {
            var items = _this10.props.data.filter(function (item) {
                return _this10.state.checked.indexOf(item.token) >= 0;
            });

            window.app.settings.addAccounts(items, function () {
                _this10.props.onComplete();
            });
        };

        _this10._onAddAll = function () {
            window.app.settings.addAccounts(_this10.props.data, function () {
                _this10.props.onComplete();
            });
        };

        _this10.state = {
            checked: _this10.props.data.map(function (item) {
                return item.token;
            })
        };
        return _this10;
    }

    (0, _createClass3.default)(MultiAccountLoginView, [{
        key: 'render',
        value: function render() {
            var _this11 = this;

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(_layout.Block, { width: '25%' }),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(
                        'p',
                        { className: 'text-block' },
                        'We\'ve found multiple accounts associated with your user, select the accounts below that you would like to add.'
                    ),
                    this.props.data.map(function (item) {
                        return React.createElement(
                            'div',
                            {
                                onClick: function onClick() {
                                    _this11._toggle(item.token);
                                },
                                className: 'acc-list-item',
                                style: { cursor: 'pointer' } },
                            React.createElement(
                                _layout.Hbox,
                                null,
                                React.createElement(
                                    _layout.Block,
                                    { width: '20px' },
                                    _this11.state.checked.indexOf(item.token) >= 0 ? React.createElement('i', { className: 'fal fa-check' }) : null
                                ),
                                React.createElement(
                                    _layout.Block,
                                    null,
                                    item.account_name,
                                    ' - ',
                                    item.domain
                                )
                            )
                        );
                    }),
                    this.state.checked.length == this.props.data.length ? React.createElement(
                        'button',
                        { onClick: this._onAddAll },
                        'Add all accounts'
                    ) : React.createElement(
                        'button',
                        { onClick: this._onAddSelected },
                        'Add selected account(s)'
                    ),
                    React.createElement(
                        'button',
                        { onClick: this.props.onRejectAddMulti },
                        'Cancel'
                    )
                ),
                React.createElement(_layout.Block, { width: '25%' })
            );
        }
    }]);
    return MultiAccountLoginView;
}(React.Component);

var ViewRegister = function (_React$Component9) {
    (0, _inherits3.default)(ViewRegister, _React$Component9);

    function ViewRegister(props) {
        (0, _classCallCheck3.default)(this, ViewRegister);

        var _this12 = (0, _possibleConstructorReturn3.default)(this, (ViewRegister.__proto__ || (0, _getPrototypeOf2.default)(ViewRegister)).call(this, props));

        _this12._request = function () {
            var errors = {};
            if (_this12.state.email == "") errors.email = "REQUIRED";
            if (_this12.state.email.indexOf("@") < 0) errors.email = "INVALID";
            if (_this12.state.email_confirm == "") errors.email_confirm = "REQUIRED";
            if (!errors.email_confirm) {
                if (_this12.state.email != _this12.state.email_confirm) errors.email_conform = "MISMATCH";
            }

            if (_this12.state.password == "") errors.password = "REQUIRED";
            if (_this12.state.pass_confirm == "") errors.pass_confirm = "REQUIRED";
            if (!errors.password) {
                if (_this12.state.password.length <= 8) errors.password = "";
            }

            if (!errors.password) {
                if (_this12.state.password != _this12.state.pass_confirm) errors.password = "MISMATCH";
            }

            if (_this12.state.domain == "") errors.domain = "REQUIRED";
            if (_this12.state.name == "") errors.name = "REQUIRED";
            if (_this12.state.org_id == "") errors.org_id = "REQUIRED";

            if ((0, _keys2.default)(errors).length > 0) {
                _this12.setState({
                    errors: errors
                });
                return;
            }

            // We're valid, fire off the registration to the remote server
            // Show the verification message
        };

        _this12._requestAccess = function () {
            var errors = {};
            if (_this12.state.email == "") errors.email = "REQUIRED";
            if (_this12.state.password == "") errors.password = "REQUIRED";
            if (_this12.state.domain == "") errors.domain = "REQUIRED";

            if ((0, _keys2.default)(errors).length > 0) {
                _this12.setState({
                    errors: errors
                });
                return;
            }

            // attempt the access request with the provided credentials
        };

        _this12.state = {
            email: "",
            email_confirm: "",
            password: "",
            pass_confirm: "",
            domain: "",
            name: "",
            phone: "",
            org_id: "",
            view: "REGISTER",
            org_name: "",
            org_site: "",
            org_email: "",
            errors: []
        };
        return _this12;
    }

    (0, _createClass3.default)(ViewRegister, [{
        key: 'render',
        value: function render() {
            var _this13 = this;

            if (!window.app.socket.isConnected) {
                return React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'p',
                            null,
                            'You must be connected to the internet to register an account.'
                        ),
                        React.createElement(
                            'button',
                            null,
                            React.createElement('i', { className: 'fal fa-times' }),
                            '\xA0Cancel'
                        )
                    )
                );
            }

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Block,
                    { width: '30px', addClass: 'tbar', borderRight: true,
                        style: { position: "relative", overflow: "hidden" } },
                    React.createElement(
                        'div',
                        { className: 'vert-tabs' },
                        React.createElement(
                            'div',
                            {
                                onClick: function onClick() {
                                    _this13.setState({ view: "REGISTER" });
                                },
                                className: "vert-tab " + (this.state.view == "REGISTER" ? " active" : "") },
                            React.createElement('i', { className: 'fal fa-plus' }),
                            '\xA0New account'
                        ),
                        React.createElement(
                            'div',
                            {
                                onClick: function onClick() {
                                    _this13.setState({ view: "ACCESS" });
                                },
                                className: "vert-tab " + (this.state.view == "ACCESS" ? " active" : "") },
                            React.createElement('i', { className: 'fal fa-lock' }),
                            '\xA0Access request'
                        )
                    )
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    React.createElement(
                        'div',
                        { className: 'hform' },
                        React.createElement(
                            'div',
                            { className: 'title' },
                            this.state.view == 'REGISTER' ? React.createElement(
                                'h1',
                                null,
                                'Register'
                            ) : React.createElement(
                                'h1',
                                null,
                                'Request Access'
                            ),
                            React.createElement(
                                'p',
                                null,
                                'Please provide us with the details below to start your registration.'
                            )
                        ),
                        this.state.view == 'REGISTER' ? React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Your name*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({ name: e.target.value });
                                    },
                                    type: 'text',
                                    value: this.state.name })
                            )
                        ) : null,
                        React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Email*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            email: e.target.value
                                        });
                                    },
                                    type: 'email', value: this.state.email })
                            )
                        ),
                        this.state.view == 'REGISTER' ? React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Email confirmation*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            email_confirm: e.target.value
                                        });
                                    },
                                    type: 'email', value: this.state.email_confirm })
                            )
                        ) : null,
                        React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Password*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            password: e.target.value
                                        });
                                    },
                                    type: 'password', value: this.state.password })
                            )
                        ),
                        this.state.view == 'REGISTER' ? React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Password confirmation*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            pass_confirm: e.target.value
                                        });
                                    },
                                    type: 'password', value: this.state.pass_confirm })
                            )
                        ) : null,
                        this.state.view == "REGISTER" ? React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Phone:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            phone: e.target.value
                                        });
                                    },
                                    type: 'phone', value: this.state.phone })
                            )
                        ) : null,
                        React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Account*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement('input', {
                                    onChange: function onChange(e) {
                                        _this13.setState({
                                            domain: e.target.value
                                        });
                                    },
                                    type: 'text', value: this.state.domain })
                            )
                        ),
                        this.state.view == "REGISTER" ? React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement(
                                'div',
                                { className: 'label' },
                                'Organization*:'
                            ),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement(
                                    'select',
                                    {
                                        onChange: function onChange(e) {
                                            _this13.setState({
                                                org_id: e.target.value
                                            });
                                        },
                                        value: this.state.org_id },
                                    React.createElement('option', { value: '' }),
                                    React.createElement(
                                        'option',
                                        { value: 'NEW' },
                                        'My organization is not listed'
                                    )
                                )
                            )
                        ) : null,
                        this.state.org_id == 'NEW' ? React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'div',
                                { className: 'field' },
                                React.createElement(
                                    'div',
                                    { className: 'label' },
                                    'Organization name*:'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'input' },
                                    React.createElement('input', {
                                        onChange: function onChange(e) {
                                            _this13.setState({
                                                org_name: e.target.value
                                            });
                                        },
                                        value: this.state.org_name,
                                        type: 'text' })
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'field' },
                                React.createElement(
                                    'div',
                                    { className: 'label' },
                                    'Organization acronym:'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'input' },
                                    React.createElement('input', {
                                        onChange: function onChange(e) {
                                            _this13.setState({
                                                org_acronym: e.target.value
                                            });
                                        },
                                        value: this.state.org_acronym,
                                        type: 'text' })
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'field' },
                                React.createElement(
                                    'div',
                                    { className: 'label' },
                                    'Organization website:'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'input' },
                                    React.createElement('input', {
                                        onChange: function onChange(e) {
                                            _this13.setState({
                                                org_site: e.target.value
                                            });
                                        },
                                        value: this.state.org_site,
                                        type: 'text' })
                                )
                            )
                        ) : null,
                        React.createElement(
                            'div',
                            { className: 'field' },
                            React.createElement('div', { className: 'label' }),
                            React.createElement(
                                'div',
                                { className: 'input' },
                                React.createElement(
                                    'button',
                                    { style: { background: _constants.COLORS.aurora3 } },
                                    React.createElement('i', {
                                        className: 'fal fa-check' }),
                                    '\xA0Register'
                                ),
                                '\xA0',
                                React.createElement(
                                    'button',
                                    {
                                        onClick: this.props.onCancel,
                                        style: { background: _constants.COLORS.aurora0 } },
                                    React.createElement('i', { className: 'fal fa-ban' }),
                                    '\xA0Cancel'
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewRegister;
}(React.Component);

var ViewLogin = function (_React$Component10) {
    (0, _inherits3.default)(ViewLogin, _React$Component10);

    function ViewLogin(props) {
        (0, _classCallCheck3.default)(this, ViewLogin);

        var _this14 = (0, _possibleConstructorReturn3.default)(this, (ViewLogin.__proto__ || (0, _getPrototypeOf2.default)(ViewLogin)).call(this, props));

        _this14._connectionLost = function () {
            _this14.setState({
                isConnected: false
            });
        };

        _this14._connected = function () {
            _this14.setState({
                isConnected: true
            });
        };

        _this14._onAddSuccess = function (accs) {
            if (accs.length > 1) {
                // this user has access to multiple
                _this14.setState({
                    view: 'MULTIPLE',
                    accs: accs
                });
            } else {
                window.app.settings.addAccounts(accs, function () {
                    window.app.socket.reloadNodes(function () {
                        _this14.setState({
                            view: 'DEFAULT'
                        });
                    });
                });
            }
        };

        _this14._rejectMulti = function () {
            _this14.setState({
                view: 'DEFAULT'
            });
        };

        _this14._addAllMulti = function () {
            _this14._totalAdds = _this14.state.acc.alternates.length;
            _this14._added = 0;

            _this14.state.acc.alternates.forEach(function (item) {
                window.app.settings.addAccount(item, function (res) {
                    _this14._added++;

                    if (_this14._added >= _this14._totalAdds) {
                        _this14._added = 0;
                        _this14._totalAdds = 0;
                        _this14.setState({
                            view: 'DEFAULT',
                            acc: null
                        });
                    }
                });
            });
        };

        _this14._onLoginComplete = function () {
            _this14.setState({
                accs: [],
                view: "DEFAULT"
            });
        };

        _this14.state = {
            view: 'DEFAULT',
            accs: [],
            isConnected: window.app.socket.isConnected
        };

        window.app.socket.connectionLost.connect(_this14._connectionLost);
        window.app.socket.connectionEstablished.connect(_this14._connected);
        return _this14;
    }

    (0, _createClass3.default)(ViewLogin, [{
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            window.app.socket.connectionLost.disconnect(this._connectionLost);
            window.app.socket.connectionEstablished.disconnect(this._connected);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this15 = this;

            var view = void 0;
            if (this.state.view == 'DEFAULT') {
                view = React.createElement(ViewDefault, {
                    onRegister: function onRegister() {
                        _this15.setState({ view: "REGISTER" });
                    },
                    onAddAccount: function onAddAccount() {
                        _this15.setState({ view: 'ADD' });
                    } });
            }

            if (this.state.view == 'REGISTER') {
                view = React.createElement(ViewRegister, {
                    onCancel: function onCancel() {
                        _this15.setState({
                            view: "DEFAULT"
                        });
                    } });
            }

            if (this.state.view == 'MULTIPLE') {
                view = React.createElement(MultiAccountLoginView, {
                    onComplete: this._onLoginComplete,
                    data: this.state.accs });
            }

            if (this.state.view == 'ADD') {
                view = React.createElement(ViewAddAccount, {
                    onCancel: function onCancel() {
                        _this15.setState({ view: "DEFAULT" });
                    },
                    onComplete: this._onAddSuccess,
                    onCancelAdd: function onCancelAdd() {
                        _this15.setState({ view: 'DEFAULT' });
                    } });
            }

            var connectIcon = React.createElement('i', { className: 'fal fa-plug', style: { color: _constants.COLORS.aurora3 } });
            if (!this.state.isConnected) connectIcon = React.createElement('i', { className: 'fal fa-plug', style: { color: _constants.COLORS.aurora0 } });
            var connectLabel = "Connected";
            if (!this.state.isConnected) connectLabel = "Not connected";

            return React.createElement(
                _layout.Vbox,
                null,
                view,
                React.createElement(
                    _layout.Block,
                    { height: '30px', addClass: 'tbar', borderTop: true },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        connectIcon,
                        '\xA0',
                        connectLabel
                    )
                )
            );

            return view;
        }
    }]);
    return ViewLogin;
}(React.Component);

exports.default = ViewLogin;

/***/ }),

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(13);
var dPs = __webpack_require__(87);
var enumBugKeys = __webpack_require__(44);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(50)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(64).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ 44:
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(11);


/***/ }),

/***/ 46:
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var LIBRARY = __webpack_require__(29);
var wksExt = __webpack_require__(45);
var defineProperty = __webpack_require__(14).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),

/***/ 47:
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
var document = __webpack_require__(9).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(17);
var toObject = __webpack_require__(32);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(15) && !__webpack_require__(19)(function () {
  return Object.defineProperty(__webpack_require__(50)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(83);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(93);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),

/***/ 55:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(29);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var hide = __webpack_require__(18);
var has = __webpack_require__(17);
var Iterators = __webpack_require__(21);
var $iterCreate = __webpack_require__(86);
var setToStringTag = __webpack_require__(30);
var getPrototypeOf = __webpack_require__(52);
var ITERATOR = __webpack_require__(11)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = (!BUGGY && $native) || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(18);


/***/ }),

/***/ 57:
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(17);
var toIObject = __webpack_require__(20);
var arrayIndexOf = __webpack_require__(88)(false);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(24);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(57);
var hiddenKeys = __webpack_require__(44).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Block = exports.Panel = exports.Vbox = exports.Hbox = undefined;

var _lt = __webpack_require__(109);

var _lt2 = _interopRequireDefault(_lt);

var _lt3 = __webpack_require__(113);

var _lt4 = _interopRequireDefault(_lt3);

var _lt5 = __webpack_require__(114);

var _lt6 = _interopRequireDefault(_lt5);

var _lt7 = __webpack_require__(115);

var _lt8 = _interopRequireDefault(_lt7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Hbox = _lt2.default;
exports.Vbox = _lt4.default;
exports.Panel = _lt6.default;
exports.Block = _lt8.default;

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(28);
var createDesc = __webpack_require__(27);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var has = __webpack_require__(17);
var IE8_DOM_DEFINE = __webpack_require__(53);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(15) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(85)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(55)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(42);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(9).document;
module.exports = document && document.documentElement;


/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(90);
var global = __webpack_require__(9);
var hide = __webpack_require__(18);
var Iterators = __webpack_require__(21);
var TO_STRING_TAG = __webpack_require__(11)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ 66:
/***/ (function(module, exports) {



/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(12);
var core = __webpack_require__(8);
var fails = __webpack_require__(19);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(81), __esModule: true };

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    TEXT: 0,
    SELECT: 1,
    NUMERIC: 2,
    DATE: 3,
    LOCATION: 4,
    INTERVAL: 5,
    COORDINATE: 6,
    AGE: 7,
    TIME: 8,
    WORKFLOW: 99,
    SWITCH: 100,
    OVERDUE: 101,
    VBOX: 102,
    HBOX: 103,
    LABEL: 104,
    LOCATION_TYPE: 105,
    COLOUR: 106,
    BORDER_STYLE: 107
};

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(80);
module.exports = __webpack_require__(8).Object.getPrototypeOf;


/***/ }),

/***/ 8:
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.3' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(32);
var $getPrototypeOf = __webpack_require__(52);

__webpack_require__(67)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(82);
var $Object = __webpack_require__(8).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(15), 'Object', { defineProperty: __webpack_require__(14).f });


/***/ }),

/***/ 83:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(84), __esModule: true };

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(62);
__webpack_require__(65);
module.exports = __webpack_require__(45).f('iterator');


/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var defined = __webpack_require__(38);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(43);
var descriptor = __webpack_require__(27);
var setToStringTag = __webpack_require__(30);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(18)(IteratorPrototype, __webpack_require__(11)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var anObject = __webpack_require__(13);
var getKeys = __webpack_require__(22);

module.exports = __webpack_require__(15) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(20);
var toLength = __webpack_require__(63);
var toAbsoluteIndex = __webpack_require__(89);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ 9:
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ 90:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(91);
var step = __webpack_require__(92);
var Iterators = __webpack_require__(21);
var toIObject = __webpack_require__(20);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(55)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ 91:
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ 92:
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(94), __esModule: true };

/***/ }),

/***/ 94:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(95);
__webpack_require__(66);
__webpack_require__(100);
__webpack_require__(101);
module.exports = __webpack_require__(8).Symbol;


/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(9);
var has = __webpack_require__(17);
var DESCRIPTORS = __webpack_require__(15);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var META = __webpack_require__(96).KEY;
var $fails = __webpack_require__(19);
var shared = __webpack_require__(40);
var setToStringTag = __webpack_require__(30);
var uid = __webpack_require__(26);
var wks = __webpack_require__(11);
var wksExt = __webpack_require__(45);
var wksDefine = __webpack_require__(46);
var enumKeys = __webpack_require__(97);
var isArray = __webpack_require__(98);
var anObject = __webpack_require__(13);
var isObject = __webpack_require__(16);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var createDesc = __webpack_require__(27);
var _create = __webpack_require__(43);
var gOPNExt = __webpack_require__(99);
var $GOPD = __webpack_require__(60);
var $DP = __webpack_require__(14);
var $keys = __webpack_require__(22);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(59).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(28).f = $propertyIsEnumerable;
  __webpack_require__(47).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(29)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(18)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),

/***/ 96:
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(26)('meta');
var isObject = __webpack_require__(16);
var has = __webpack_require__(17);
var setDesc = __webpack_require__(14).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(19)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ 97:
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(24);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ 99:
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(20);
var gOPN = __webpack_require__(59).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ })

/******/ });
//# sourceMappingURL=desktop_login.map