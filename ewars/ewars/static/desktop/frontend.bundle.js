/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 340);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(79), __esModule: true };

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(102);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(106);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(54);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(68);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Block = exports.Panel = exports.Vbox = exports.Hbox = undefined;

var _lt = __webpack_require__(109);

var _lt2 = _interopRequireDefault(_lt);

var _lt3 = __webpack_require__(113);

var _lt4 = _interopRequireDefault(_lt3);

var _lt5 = __webpack_require__(114);

var _lt6 = _interopRequireDefault(_lt5);

var _lt7 = __webpack_require__(115);

var _lt8 = _interopRequireDefault(_lt7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Hbox = _lt2.default;
exports.Vbox = _lt4.default;
exports.Panel = _lt6.default;
exports.Block = _lt8.default;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ActionItem = function (_React$Component) {
    (0, _inherits3.default)(ActionItem, _React$Component);

    function ActionItem(props) {
        (0, _classCallCheck3.default)(this, ActionItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ActionItem.__proto__ || (0, _getPrototypeOf2.default)(ActionItem)).call(this, props));

        _this._onClick = function () {
            _this._hideHover();
            _this.props.onClick(_this.props.data[1]);
        };

        _this._showHover = function () {
            _this._hover = document.createElement('div');
            _this._hover.setAttribute('class', 'action-hover');
            _this._hover.appendChild(document.createTextNode(_this.props.data[2]));

            document.body.appendChild(_this._hover);
        };

        _this._hideHover = function () {
            if (_this._hover) {
                if (_this._hover) {
                    document.body.removeChild(_this._hover);
                    _this._hover = null;
                }
            }
        };

        _this._hover = function (e) {
            e.stopPropagation();
            e.preventDefault();
        };

        _this._hover = null;
        return _this;
    }

    (0, _createClass3.default)(ActionItem, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (this.props.data[2]) {

                this.el.addEventListener('mouseover', this._showHover);
                this.el.addEventListener('mouseout', this._hideHover);
            }
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            if (this.props.data[2]) {
                this.el.removeEventListener('mouseover', this._showHover);
                this.el.removeEventListener('mouseout', this._hideHover);
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return React.createElement(
                'div',
                {
                    onHover: this._hover,
                    ref: function ref(el) {
                        _this2.el = el;
                    },
                    className: 'action',
                    onClick: this._onClick },
                React.createElement('i', { className: "fal " + this.props.data[0] })
            );
        }
    }]);
    return ActionItem;
}(React.Component);

var ActionGroup = function (_React$Component2) {
    (0, _inherits3.default)(ActionGroup, _React$Component2);

    function ActionGroup(props) {
        (0, _classCallCheck3.default)(this, ActionGroup);
        return (0, _possibleConstructorReturn3.default)(this, (ActionGroup.__proto__ || (0, _getPrototypeOf2.default)(ActionGroup)).call(this, props));
    }

    (0, _createClass3.default)(ActionGroup, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate() {
            return true;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this4 = this;

            var items = this.props.actions || [];

            var className = "action-group";
            className += this.props.right ? ' right' : '';
            className += this.props.vertical ? ' vert' : '';
            return React.createElement(
                'div',
                { className: className },
                items.map(function (item) {
                    return React.createElement(ActionItem, { data: item, onClick: _this4.props.onAction });
                })
            );
        }
    }]);
    return ActionGroup;
}(React.Component);

ActionGroup.defaultProps = {
    actions: [],
    onAction: function onAction(action) {
        console.log("ACTION: ", action);
    },
    style: {},
    height: "20px",
    vertical: false,
    right: false
};
exports.default = ActionGroup;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.3' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 9 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ROLES = exports.COLORS = exports.FIELDS = undefined;

var _const = __webpack_require__(76);

var _const2 = _interopRequireDefault(_const);

var _const3 = __webpack_require__(33);

var _const4 = _interopRequireDefault(_const3);

var _const5 = __webpack_require__(117);

var _const6 = _interopRequireDefault(_const5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FIELDS = _const2.default;
exports.COLORS = _const4.default;
exports.ROLES = _const6.default;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(40)('wks');
var uid = __webpack_require__(26);
var Symbol = __webpack_require__(9).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var ctx = __webpack_require__(23);
var hide = __webpack_require__(18);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(13);
var IE8_DOM_DEFINE = __webpack_require__(53);
var toPrimitive = __webpack_require__(41);
var dP = Object.defineProperty;

exports.f = __webpack_require__(15) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(19)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var createDesc = __webpack_require__(27);
module.exports = __webpack_require__(15) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(58);
var defined = __webpack_require__(38);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(57);
var enumBugKeys = __webpack_require__(44);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(36);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(110), __esModule: true };

/***/ }),
/* 26 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 28 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(14).f;
var has = __webpack_require__(17);
var TAG = __webpack_require__(11)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ComponentState = exports.state = undefined;

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var state = void 0;

var ComponentState = function ComponentState() {
    var _this = this;

    (0, _classCallCheck3.default)(this, ComponentState);

    this.subscribe = function (event, callback) {
        if (_this._listeners[event]) {
            _this._listeners[event].push(callback);
        } else {
            _this._listeners[event] = [callback];
        }
    };

    this._emit = function (event) {
        if (_this._listeners[event]) {
            _this._listeners[event].forEach(function (cb) {
                cb();
            });
        }
    };

    this.addTab = function (tab) {
        var newId = utils.uuid();
        _this.tabs.push({
            _: newId,
            cmp: tab._,
            n: tab.n,
            i: tab.i || null,
            _id: null
        });
        _this.activeTab = newId;

        _this._emit('VIEW_UPDATED');
    };

    this.removeTab = function (tid) {
        _this.activeTab = 'DEFAULT';
        var rIndex = void 0;
        _this.tabs.forEach(function (item, index) {
            if (item._ == tid) rIndex = index;
        });
        _this.tabs.splice(rIndex, 1);
        _this._emit('VIEW_UPDATED');
    };

    this.getActiveTab = function () {
        var tab = _this.tabs.filter(function (item) {
            return item._ == _this.activeTab;
        })[0] || null;

        return tab;
    };

    this.setActiveTab = function (tid) {
        _this.activeTab = tid;
        _this._emit('VIEW_UPDATED');
    };

    this.setView = function (view) {
        _this.view = view;
        _this._emit('VIEW_UPDATED');
    };

    this.registerSubState = function (id, state) {
        _this.subStates[id] = state;
    };

    this.getSubState = function (id) {
        if (_this.subStates[id]) return _this.subStates[id];
        return null;
    };

    exports.state = state = this;
    this.tabs = [];
    this.view = 'DEFAULT';
    this.dirty = false;
    this.activeTab = 'DEFAULT';

    this._listeners = {};

    this.subStates = {};
};

exports.default = ComponentState;
exports.state = state;
exports.ComponentState = ComponentState;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(38);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var COLORS = {
    polar0: 'rgb(47, 52, 64)',
    polar1: 'rgb(59, 66, 81)',
    polar2: 'rgb(67, 76, 93)',
    polar3: 'rgb(76, 86, 105)',

    snow0: 'rgb(216, 222, 233)',
    snow1: 'rgb(229, 233, 240)',
    snow2: 'rgb(236, 239, 244)',

    frost0: 'rgb(144, 188, 187)',
    frost1: 'rgb(138, 192, 207)',
    frost2: 'rgb(130, 161, 192)',
    frost3: 'rgb(95, 130, 170)',

    aurora0: 'rgb(190, 97, 107)',
    aurora1: 'rgb(207, 135, 114)',
    aurora2: 'rgb(234, 202, 143)',
    aurora3: 'rgb(164, 189, 142)',
    aurora4: 'rgb(179, 143, 172)'
};

exports.default = COLORS;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(49);

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkDrop = function (_React$Component) {
    (0, _inherits3.default)(LinkDrop, _React$Component);

    function LinkDrop(props) {
        (0, _classCallCheck3.default)(this, LinkDrop);

        var _this = (0, _possibleConstructorReturn3.default)(this, (LinkDrop.__proto__ || (0, _getPrototypeOf2.default)(LinkDrop)).call(this, props));

        _this._handleOuterClick = function (e) {
            if (_this._elW && !_this._elW.contains(e.target)) {
                _this._el.style.display = 'none';
            }
        };

        _this._toggle = function (e) {
            e.preventDefault();

            if (_this._el.style.display == 'none') {

                _this._el.style.display = 'block';
            } else {
                _this._el.style.display = 'none';
            }
        };

        _this._onSelect = function (value) {
            _this._el.style.display = 'none';
            _this.props.onClick(value);
        };

        return _this;
    }

    (0, _createClass3.default)(LinkDrop, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            document.addEventListener('mousedown', this._handleOuterClick);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            document.removeEventListener('mousedown', this._handleOuterClick);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var label = "No selection";

            if (this.props.value) {
                this.props.options.forEach(function (item) {
                    if (item[0] == _this2.props.value) label = item[1];
                });
            }

            var style = {};
            if (this.props.style) style = (0, _extends3.default)({}, style, this.props.style);

            return React.createElement(
                'div',
                { className: 'link-drop', ref: function ref(el) {
                        _this2._elW = el;
                    }, style: style },
                React.createElement(
                    'a',
                    { href: '#', onClick: this._toggle },
                    label,
                    '\xA0',
                    React.createElement('i', { className: 'fal fa-caret-down' })
                ),
                React.createElement(
                    'div',
                    {
                        ref: function ref(el) {
                            _this2._el = el;
                        },
                        className: 'children',
                        style: { display: 'none' } },
                    React.createElement(
                        'div',
                        { className: 'children-inner' },
                        this.props.options.map(function (item) {
                            return React.createElement(
                                'div',
                                {
                                    onClick: function onClick() {
                                        _this2._onSelect(item[0]);
                                    },
                                    className: 'link-item' },
                                item[1]
                            );
                        })
                    )
                )
            );
        }
    }]);
    return LinkDrop;
}(React.Component);

LinkDrop.defaultProps = {
    value: null,
    options: []
};
exports.default = LinkDrop;

/***/ }),
/* 35 */,
/* 36 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(120), __esModule: true };

/***/ }),
/* 38 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(40)('keys');
var uid = __webpack_require__(26);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(16);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 42 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(13);
var dPs = __webpack_require__(87);
var enumBugKeys = __webpack_require__(44);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(50)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(64).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 44 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(11);


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var LIBRARY = __webpack_require__(29);
var wksExt = __webpack_require__(45);
var defineProperty = __webpack_require__(14).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 47 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.remote_api = exports.api = undefined;

var _stringify = __webpack_require__(37);

var _stringify2 = _interopRequireDefault(_stringify);

var _promise = __webpack_require__(69);

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var api = function api(method, args) {
    return new _promise2.default(function (resolve, reject) {
        var oReq = new XMLHttpRequest();
        // oReq.open("POST", `/api/${method}`, true);
        oReq.open("POST", "/api", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        oReq.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    if (oReq.responseText == "NO_AUTH") {
                        document.location = "/login";
                    }
                    var resp = JSON.parse(oReq.responseText);
                    resolve(resp);
                } else {
                    if (oReq.status == 500) {}

                    if (oReq.status == 101) {}
                    reject(oReq.status);
                }
            }
        }.bind(undefined);

        var _args = args ? args : {};
        oReq.send((0, _stringify2.default)([method, _args]));
    });
};

var remote_api = function remote_api(method, args) {
    return new _promise2.default(function (resolve, reject) {
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "http://127.0.0.1:9000/api/_d", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        console.log(window.app.settings.activeAccount);
        oReq.setRequestHeader("EC_TOKEN", window.app.win.tki);

        oReq.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    if (oReq.responseText == "NO_AUTH") {
                        reject("NO_AUTH");
                    } else {
                        var resp = JSON.parse(oReq.responseText);
                        resolve(resp);
                    }
                } else {
                    reject(oReq.status);
                }
            }
        }.bind(undefined);

        var _args = args ? args : {};
        oReq.send((0, _stringify2.default)([method, args]));
    });
};

exports.default = api;
exports.api = api;
exports.remote_api = remote_api;

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
var document = __webpack_require__(9).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.emit = exports.forget = exports.listen = exports.copy = exports.debounce = exports.uuid = exports.tx = undefined;

var _stringify = __webpack_require__(37);

var _stringify2 = _interopRequireDefault(_stringify);

var _api = __webpack_require__(48);

var _api2 = _interopRequireDefault(_api);

var _debounce = __webpack_require__(135);

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uuid = function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
    });
};

var copy = function copy(data) {
    return JSON.parse((0, _stringify2.default)(data));
};

var _LISTENERS = {};

var listen = function listen(event, callback) {
    if (_LISTENERS[event]) {
        _LISTENERS[event].push(callback);
    } else {
        _LISTENERS[event] = [callback];
    }
};

var emit = function emit(event, data) {
    if (_LISTENERS[event]) {
        _LISTENERS[event].forEach(function (cb) {
            cb(data);
        });
    }
};

var forget = function forget(event, callback) {
    if (_LISTENERS[event]) {
        var rIndex = void 0;
        _LISTENERS[event].forEach(function (cb, index) {
            if (cb == callback) rIndex = index;
        });
        _LISTENERS[event].splice(rIndex, 1);
    }
};

exports.tx = _api2.default;
exports.uuid = uuid;
exports.debounce = _debounce2.default;
exports.copy = copy;
exports.listen = listen;
exports.forget = forget;
exports.emit = emit;

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(17);
var toObject = __webpack_require__(32);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(15) && !__webpack_require__(19)(function () {
  return Object.defineProperty(__webpack_require__(50)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(83);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(93);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(29);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var hide = __webpack_require__(18);
var has = __webpack_require__(17);
var Iterators = __webpack_require__(21);
var $iterCreate = __webpack_require__(86);
var setToStringTag = __webpack_require__(30);
var getPrototypeOf = __webpack_require__(52);
var ITERATOR = __webpack_require__(11)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = (!BUGGY && $native) || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(18);


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(17);
var toIObject = __webpack_require__(20);
var arrayIndexOf = __webpack_require__(88)(false);
var IE_PROTO = __webpack_require__(39)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(24);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(57);
var hiddenKeys = __webpack_require__(44).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(28);
var createDesc = __webpack_require__(27);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var has = __webpack_require__(17);
var IE8_DOM_DEFINE = __webpack_require__(53);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(15) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 25.4.1.5 NewPromiseCapability(C)
var aFunction = __webpack_require__(36);

function PromiseCapability(C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction(resolve);
  this.reject = aFunction(reject);
}

module.exports.f = function (C) {
  return new PromiseCapability(C);
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(85)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(55)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(42);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(9).document;
module.exports = document && document.documentElement;


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(90);
var global = __webpack_require__(9);
var hide = __webpack_require__(18);
var Iterators = __webpack_require__(21);
var TO_STRING_TAG = __webpack_require__(11)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 66 */
/***/ (function(module, exports) {



/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(12);
var core = __webpack_require__(8);
var fails = __webpack_require__(19);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(81), __esModule: true };

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(121), __esModule: true };

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(24);
var TAG = __webpack_require__(11)('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__(13);
var aFunction = __webpack_require__(36);
var SPECIES = __webpack_require__(11)('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(23);
var invoke = __webpack_require__(128);
var html = __webpack_require__(64);
var cel = __webpack_require__(50);
var global = __webpack_require__(9);
var process = global.process;
var setTask = global.setImmediate;
var clearTask = global.clearImmediate;
var MessageChannel = global.MessageChannel;
var Dispatch = global.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;
var run = function () {
  var id = +this;
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};
var listener = function (event) {
  run.call(event.data);
};
// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!setTask || !clearTask) {
  setTask = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      invoke(typeof fn == 'function' ? fn : Function(fn), args);
    };
    defer(counter);
    return counter;
  };
  clearTask = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (__webpack_require__(24)(process) == 'process') {
    defer = function (id) {
      process.nextTick(ctx(run, id, 1));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(ctx(run, id, 1));
    };
  // Browsers with MessageChannel, includes WebWorkers
  } else if (MessageChannel) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = ctx(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
    defer = function (id) {
      global.postMessage(id + '', '*');
    };
    global.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in cel('script')) {
    defer = function (id) {
      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run.call(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(ctx(run, id, 1), 0);
    };
  }
}
module.exports = {
  set: setTask,
  clear: clearTask
};


/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(13);
var isObject = __webpack_require__(16);
var newPromiseCapability = __webpack_require__(61);

module.exports = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};


/***/ }),
/* 75 */,
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    TEXT: 0,
    SELECT: 1,
    NUMERIC: 2,
    DATE: 3,
    LOCATION: 4,
    INTERVAL: 5,
    COORDINATE: 6,
    AGE: 7,
    TIME: 8,
    WORKFLOW: 99,
    SWITCH: 100,
    OVERDUE: 101,
    VBOX: 102,
    HBOX: 103,
    LABEL: 104,
    LOCATION_TYPE: 105,
    COLOUR: 106,
    BORDER_STYLE: 107
};

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BaseState = function BaseState() {
    var _this = this;

    (0, _classCallCheck3.default)(this, BaseState);

    this.on = function (event, callback) {
        if (_this._listeners[event]) {
            _this._listeners[event].push(callback);
        } else {
            _this._listeners[event] = [callback];
        }
    };

    this.off = function (event, callback) {
        if (_this._listeners[event]) {
            var rIndex = void 0;
            _this._listeners[event].forEach(function (cb, index) {
                if (cb == callback) rIndex = index;
            });
            _this._listeners[event].splice(rIndex, 1);
        }
    };

    this._emit = function (event) {
        if (_this._listeners[event]) {
            _this._listeners[event].forEach(function (cb) {
                cb();
            });
        }
    };

    this.getStoreableState = function () {};

    this.registerSubState = function (id, state) {
        _this.subStates[id] = state;
    };

    this.getSubState = function (id) {
        if (_this.subStates[id]) return _this.subStates[id];
        return null;
    };

    this.removeSubState = function (id) {
        if (_this.subStates[id]) delete _this.subStates[id];
        return null;
    };

    this._listeners = {};
    this._subStates = {};
}

/**
 * Retrieve state in s storable format, JSON serializable
 */
;

exports.default = BaseState;

/***/ }),
/* 78 */,
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(80);
module.exports = __webpack_require__(8).Object.getPrototypeOf;


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(32);
var $getPrototypeOf = __webpack_require__(52);

__webpack_require__(67)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(82);
var $Object = __webpack_require__(8).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(15), 'Object', { defineProperty: __webpack_require__(14).f });


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(84), __esModule: true };

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(62);
__webpack_require__(65);
module.exports = __webpack_require__(45).f('iterator');


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var defined = __webpack_require__(38);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(43);
var descriptor = __webpack_require__(27);
var setToStringTag = __webpack_require__(30);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(18)(IteratorPrototype, __webpack_require__(11)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(14);
var anObject = __webpack_require__(13);
var getKeys = __webpack_require__(22);

module.exports = __webpack_require__(15) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(20);
var toLength = __webpack_require__(63);
var toAbsoluteIndex = __webpack_require__(89);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(42);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(91);
var step = __webpack_require__(92);
var Iterators = __webpack_require__(21);
var toIObject = __webpack_require__(20);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(55)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 91 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 92 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(94), __esModule: true };

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(95);
__webpack_require__(66);
__webpack_require__(100);
__webpack_require__(101);
module.exports = __webpack_require__(8).Symbol;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(9);
var has = __webpack_require__(17);
var DESCRIPTORS = __webpack_require__(15);
var $export = __webpack_require__(12);
var redefine = __webpack_require__(56);
var META = __webpack_require__(96).KEY;
var $fails = __webpack_require__(19);
var shared = __webpack_require__(40);
var setToStringTag = __webpack_require__(30);
var uid = __webpack_require__(26);
var wks = __webpack_require__(11);
var wksExt = __webpack_require__(45);
var wksDefine = __webpack_require__(46);
var enumKeys = __webpack_require__(97);
var isArray = __webpack_require__(98);
var anObject = __webpack_require__(13);
var isObject = __webpack_require__(16);
var toIObject = __webpack_require__(20);
var toPrimitive = __webpack_require__(41);
var createDesc = __webpack_require__(27);
var _create = __webpack_require__(43);
var gOPNExt = __webpack_require__(99);
var $GOPD = __webpack_require__(60);
var $DP = __webpack_require__(14);
var $keys = __webpack_require__(22);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(59).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(28).f = $propertyIsEnumerable;
  __webpack_require__(47).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(29)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(18)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(26)('meta');
var isObject = __webpack_require__(16);
var has = __webpack_require__(17);
var setDesc = __webpack_require__(14).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(19)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(24);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(20);
var gOPN = __webpack_require__(59).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('asyncIterator');


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46)('observable');


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(103), __esModule: true };

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(104);
module.exports = __webpack_require__(8).Object.setPrototypeOf;


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(12);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(105).set });


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(16);
var anObject = __webpack_require__(13);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(23)(Function.call, __webpack_require__(60).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(107), __esModule: true };

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(108);
var $Object = __webpack_require__(8).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(12);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(43) });


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Hbox = function (_React$Component) {
    (0, _inherits3.default)(Hbox, _React$Component);

    function Hbox(props) {
        (0, _classCallCheck3.default)(this, Hbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Hbox.__proto__ || (0, _getPrototypeOf2.default)(Hbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Hbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxHeight = this.props.height || null;
            style.padding = this.props.padding || null;

            var className = "hbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Hbox;
}(React.Component);

Hbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Hbox;

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(111);
module.exports = __webpack_require__(8).Object.assign;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(12);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(112) });


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(22);
var gOPS = __webpack_require__(47);
var pIE = __webpack_require__(28);
var toObject = __webpack_require__(32);
var IObject = __webpack_require__(58);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(19)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Vbox = function (_React$Component) {
    (0, _inherits3.default)(Vbox, _React$Component);

    function Vbox(props) {
        (0, _classCallCheck3.default)(this, Vbox);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Vbox.__proto__ || (0, _getPrototypeOf2.default)(Vbox)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Vbox, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxWidth = this.props.width || null;
            style.padding = this.props.padding || null;

            var className = "vbox";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);
    return Vbox;
}(React.Component);

Vbox.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};
exports.default = Vbox;

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Panel = function (_React$Component) {
    (0, _inherits3.default)(Panel, _React$Component);

    function Panel(props) {
        (0, _classCallCheck3.default)(this, Panel);
        return (0, _possibleConstructorReturn3.default)(this, (Panel.__proto__ || (0, _getPrototypeOf2.default)(Panel)).call(this, props));
    }

    (0, _createClass3.default)(Panel, [{
        key: "render",
        value: function render() {
            var style = {};
            if (this.props.style) {
                for (var i in this.props.style) {
                    style[i] = this.props.style[i];
                }
            }
            return React.createElement(
                "div",
                { className: "ide-panel", style: style },
                this.props.children
            );
        }
    }]);
    return Panel;
}(React.Component);

exports.default = Panel;

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = __webpack_require__(25);

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Block = function (_React$Component) {
    (0, _inherits3.default)(Block, _React$Component);

    function Block(props) {
        (0, _classCallCheck3.default)(this, Block);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Block.__proto__ || (0, _getPrototypeOf2.default)(Block)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    (0, _createClass3.default)(Block, [{
        key: "render",
        value: function render() {
            var style = {};
            style.maxHeight = this.props.height || null;
            style.maxWidth = this.props.width || null;

            style.padding = this.props.padding || null;

            var className = "block";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";
            className += this.props.yO ? " overY " : "";

            if (this.props.onClick) style.cursor = "pointer";

            (0, _assign2.default)(style, this.props.style || {});

            var click = void 0;
            if (this.props.onClick) click = this._onClick;

            return React.createElement(
                "div",
                { className: className, style: style, onClick: click, id: this.props.id },
                this.props.children
            );
        }
    }]);
    return Block;
}(React.Component);

Block.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {},
    yO: false,
    id: null
};
exports.default = Block;

/***/ }),
/* 116 */,
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var ROLES = {
    USER: "Reporting user",
    ACCOUNT_ADMIN: "Account admin",
    REGIONAL_ADMIN: "Geo. admin"
};

exports.default = ROLES;

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

var _state = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE'], ['fa-upload', 'IMPORT']].reverse();

var DASH_ACTIONS = [['fa-pencil', 'EDIT'], ['fa-copy', 'COPY'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']].reverse();

var DashboardItem = function (_React$Component) {
    (0, _inherits3.default)(DashboardItem, _React$Component);

    function DashboardItem(props) {
        (0, _classCallCheck3.default)(this, DashboardItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (DashboardItem.__proto__ || (0, _getPrototypeOf2.default)(DashboardItem)).call(this, props));

        _this._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'Dashboard',
                        i: data.uuid,
                        _: 'DASHBOARD'
                    });
                    break;
                default:
                    break;
            }
        };

        return _this;
    }

    (0, _createClass3.default)(DashboardItem, [{
        key: 'render',
        value: function render() {

            var iconStyle = { color: _const2.default.aurora3 };

            return React.createElement(
                'div',
                { className: 'dash-item' },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { width: '25px', addClass: 'text-block', style: { textIndent: '5px' } },
                        React.createElement('i', { className: 'fal fa-dot-circle', style: iconStyle })
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'text-block' },
                        'Dashboard Name [User, Account Admin]'
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(_c2.default, {
                            right: true,
                            actions: DASH_ACTIONS,
                            onAction: this._action })
                    )
                )
            );
        }
    }]);
    return DashboardItem;
}(React.Component);

var ViewDashboards = function (_React$Component2) {
    (0, _inherits3.default)(ViewDashboards, _React$Component2);

    function ViewDashboards(props) {
        (0, _classCallCheck3.default)(this, ViewDashboards);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (ViewDashboards.__proto__ || (0, _getPrototypeOf2.default)(ViewDashboards)).call(this, props));

        _this2._action = function (action, data) {
            switch (action) {
                case 'CREATE':
                    _state.state.addTab({
                        n: 'Dashboard',
                        i: null,
                        _: 'DASHBOARD'
                    });
                    break;
                default:
                    break;
            }
        };

        return _this2;
    }

    (0, _createClass3.default)(ViewDashboards, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { addClass: 'tbar', height: '30px', borderBottom: true },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-tachometer' }),
                        '\xA0Dashboards'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        onAction: this._action,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { style: { padding: '16px' } },
                    React.createElement(DashboardItem, null)
                )
            );
        }
    }]);
    return ViewDashboards;
}(React.Component);

exports.default = ViewDashboards;

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StateMain = exports.state = undefined;

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _utils = __webpack_require__(51);

var utils = _interopRequireWildcard(_utils);

var _view = __webpack_require__(341);

var _view2 = _interopRequireDefault(_view);

var _state = __webpack_require__(77);

var _state2 = _interopRequireDefault(_state);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var state = void 0;

var TABS = {
    LOCATIONS: _view2.default,
    WIKI: _view2.default,
    AUDIT: _view2.default,
    RECORD: _view2.default,
    RECORDS: _view2.default
};

var VIEWS = {
    OVERVIEW: _view2.default,
    REPORTING: _view2.default,
    DASHBOARD: _view2.default,
    NOTEBOOKS: _view2.default,
    PLOTS: _view2.default,
    ALERTS: _view2.default,
    MAPS: _view2.default
};

var StateMain = function (_BaseState) {
    (0, _inherits3.default)(StateMain, _BaseState);

    function StateMain() {
        (0, _classCallCheck3.default)(this, StateMain);

        var _this = (0, _possibleConstructorReturn3.default)(this, (StateMain.__proto__ || (0, _getPrototypeOf2.default)(StateMain)).call(this));

        _this.addComponent = function (cmpType, config) {
            console.log(cmpType, config);

            if (TABS[cmpType]) {
                _this.addTab(cmpType, config);
            } else {
                _this.activeView = cmpType;
                _this.activeViewConfig = config || null;
            }

            _this._emit('VIEW_CHANGE');
        };

        _this.addTab = function (tab) {
            var newId = utils.uuid();
            var tabConfig = {
                _: newId,
                cmp: tab,
                n: null,
                i: null,
                _id: null
            };
            _this.tabs.push(tabConfig);

            _this.activeTab = newId;
            _this.activeTabConfig = tabConfig;

            _this._emit('VIEW_CHANGE');
        };

        _this.removeTab = function (tid) {
            _this.activeTab = null;
            _this.activeTabConfig = null;

            var rIndex = void 0;
            _this.tabs.forEach(function (item, index) {
                if (item._ == tid) rIndex = index;
            });
            _this.tabs.splice(rIndex, 1);
            _this._emit('VIEW_CHANGE');
        };

        _this.getActiveTab = function () {
            var tab = _this.tabs.filter(function (item) {
                return item._ == _this.activeTab;
            })[0] || null;

            return tab;
        };

        _this.setActiveTab = function (tid) {
            if (tid == 'DEFAULT') {
                _this.activeTab = null;
                _this.activeTabConfig = null;
            } else {
                _this.activeTab = tid;
            }
            _this._emit('VIEW_CHANGE');
        };

        _this.setView = function (view) {
            _this.view = view;
            _this._emit('VIEW_UPDATED');
        };

        _this.state = {};
        _this.tabs = [];
        _this.activeTab = null;
        _this.activeTabConfig = null;
        _this.activeView = 'OVERVIEW';
        _this.activeViewConfig = null;

        exports.state = state = _this;
        return _this;
    }

    return StateMain;
}(_state2.default);

exports.default = StateMain;
exports.state = state;
exports.StateMain = StateMain;

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(8);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(66);
__webpack_require__(62);
__webpack_require__(65);
__webpack_require__(122);
__webpack_require__(133);
__webpack_require__(134);
module.exports = __webpack_require__(8).Promise;


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(29);
var global = __webpack_require__(9);
var ctx = __webpack_require__(23);
var classof = __webpack_require__(70);
var $export = __webpack_require__(12);
var isObject = __webpack_require__(16);
var aFunction = __webpack_require__(36);
var anInstance = __webpack_require__(123);
var forOf = __webpack_require__(124);
var speciesConstructor = __webpack_require__(71);
var task = __webpack_require__(72).set;
var microtask = __webpack_require__(129)();
var newPromiseCapabilityModule = __webpack_require__(61);
var perform = __webpack_require__(73);
var promiseResolve = __webpack_require__(74);
var PROMISE = 'Promise';
var TypeError = global.TypeError;
var process = global.process;
var $Promise = global[PROMISE];
var isNode = classof(process) == 'process';
var empty = function () { /* empty */ };
var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

var USE_NATIVE = !!function () {
  try {
    // correct subclassing with @@species support
    var promise = $Promise.resolve(1);
    var FakePromise = (promise.constructor = {})[__webpack_require__(11)('species')] = function (exec) {
      exec(empty, empty);
    };
    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    return (isNode || typeof PromiseRejectionEvent == 'function') && promise.then(empty) instanceof FakePromise;
  } catch (e) { /* empty */ }
}();

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};
var notify = function (promise, isReject) {
  if (promise._n) return;
  promise._n = true;
  var chain = promise._c;
  microtask(function () {
    var value = promise._v;
    var ok = promise._s == 1;
    var i = 0;
    var run = function (reaction) {
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then;
      try {
        if (handler) {
          if (!ok) {
            if (promise._h == 2) onHandleUnhandled(promise);
            promise._h = 1;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value);
            if (domain) domain.exit();
          }
          if (result === reaction.promise) {
            reject(TypeError('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (e) {
        reject(e);
      }
    };
    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
    promise._c = [];
    promise._n = false;
    if (isReject && !promise._h) onUnhandled(promise);
  });
};
var onUnhandled = function (promise) {
  task.call(global, function () {
    var value = promise._v;
    var unhandled = isUnhandled(promise);
    var result, handler, console;
    if (unhandled) {
      result = perform(function () {
        if (isNode) {
          process.emit('unhandledRejection', value, promise);
        } else if (handler = global.onunhandledrejection) {
          handler({ promise: promise, reason: value });
        } else if ((console = global.console) && console.error) {
          console.error('Unhandled promise rejection', value);
        }
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
    } promise._a = undefined;
    if (unhandled && result.e) throw result.v;
  });
};
var isUnhandled = function (promise) {
  return promise._h !== 1 && (promise._a || promise._c).length === 0;
};
var onHandleUnhandled = function (promise) {
  task.call(global, function () {
    var handler;
    if (isNode) {
      process.emit('rejectionHandled', promise);
    } else if (handler = global.onrejectionhandled) {
      handler({ promise: promise, reason: promise._v });
    }
  });
};
var $reject = function (value) {
  var promise = this;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  promise._v = value;
  promise._s = 2;
  if (!promise._a) promise._a = promise._c.slice();
  notify(promise, true);
};
var $resolve = function (value) {
  var promise = this;
  var then;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  try {
    if (promise === value) throw TypeError("Promise can't be resolved itself");
    if (then = isThenable(value)) {
      microtask(function () {
        var wrapper = { _w: promise, _d: false }; // wrap
        try {
          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
        } catch (e) {
          $reject.call(wrapper, e);
        }
      });
    } else {
      promise._v = value;
      promise._s = 1;
      notify(promise, false);
    }
  } catch (e) {
    $reject.call({ _w: promise, _d: false }, e); // wrap
  }
};

// constructor polyfill
if (!USE_NATIVE) {
  // 25.4.3.1 Promise(executor)
  $Promise = function Promise(executor) {
    anInstance(this, $Promise, PROMISE, '_h');
    aFunction(executor);
    Internal.call(this);
    try {
      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
    } catch (err) {
      $reject.call(this, err);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    this._c = [];             // <- awaiting reactions
    this._a = undefined;      // <- checked in isUnhandled reactions
    this._s = 0;              // <- state
    this._d = false;          // <- done
    this._v = undefined;      // <- value
    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
    this._n = false;          // <- notify
  };
  Internal.prototype = __webpack_require__(130)($Promise.prototype, {
    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
    then: function then(onFulfilled, onRejected) {
      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = isNode ? process.domain : undefined;
      this._c.push(reaction);
      if (this._a) this._a.push(reaction);
      if (this._s) notify(this, false);
      return reaction.promise;
    },
    // 25.4.5.1 Promise.prototype.catch(onRejected)
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    this.promise = promise;
    this.resolve = ctx($resolve, promise, 1);
    this.reject = ctx($reject, promise, 1);
  };
  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
    return C === $Promise || C === Wrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
__webpack_require__(30)($Promise, PROMISE);
__webpack_require__(131)(PROMISE);
Wrapper = __webpack_require__(8)[PROMISE];

// statics
$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
  // 25.4.4.5 Promise.reject(r)
  reject: function reject(r) {
    var capability = newPromiseCapability(this);
    var $$reject = capability.reject;
    $$reject(r);
    return capability.promise;
  }
});
$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
  // 25.4.4.6 Promise.resolve(x)
  resolve: function resolve(x) {
    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
  }
});
$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(132)(function (iter) {
  $Promise.all(iter)['catch'](empty);
})), PROMISE, {
  // 25.4.4.1 Promise.all(iterable)
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var values = [];
      var index = 0;
      var remaining = 1;
      forOf(iterable, false, function (promise) {
        var $index = index++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        C.resolve(promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[$index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.e) reject(result.v);
    return capability.promise;
  },
  // 25.4.4.4 Promise.race(iterable)
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var reject = capability.reject;
    var result = perform(function () {
      forOf(iterable, false, function (promise) {
        C.resolve(promise).then(capability.resolve, reject);
      });
    });
    if (result.e) reject(result.v);
    return capability.promise;
  }
});


/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(23);
var call = __webpack_require__(125);
var isArrayIter = __webpack_require__(126);
var anObject = __webpack_require__(13);
var toLength = __webpack_require__(63);
var getIterFn = __webpack_require__(127);
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(13);
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__(21);
var ITERATOR = __webpack_require__(11)('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(70);
var ITERATOR = __webpack_require__(11)('iterator');
var Iterators = __webpack_require__(21);
module.exports = __webpack_require__(8).getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),
/* 128 */
/***/ (function(module, exports) {

// fast apply, http://jsperf.lnkit.com/fast-apply/5
module.exports = function (fn, args, that) {
  var un = that === undefined;
  switch (args.length) {
    case 0: return un ? fn()
                      : fn.call(that);
    case 1: return un ? fn(args[0])
                      : fn.call(that, args[0]);
    case 2: return un ? fn(args[0], args[1])
                      : fn.call(that, args[0], args[1]);
    case 3: return un ? fn(args[0], args[1], args[2])
                      : fn.call(that, args[0], args[1], args[2]);
    case 4: return un ? fn(args[0], args[1], args[2], args[3])
                      : fn.call(that, args[0], args[1], args[2], args[3]);
  } return fn.apply(that, args);
};


/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(9);
var macrotask = __webpack_require__(72).set;
var Observer = global.MutationObserver || global.WebKitMutationObserver;
var process = global.process;
var Promise = global.Promise;
var isNode = __webpack_require__(24)(process) == 'process';

module.exports = function () {
  var head, last, notify;

  var flush = function () {
    var parent, fn;
    if (isNode && (parent = process.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (e) {
        if (head) notify();
        else last = undefined;
        throw e;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // Node.js
  if (isNode) {
    notify = function () {
      process.nextTick(flush);
    };
  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
    var toggle = true;
    var node = document.createTextNode('');
    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise && Promise.resolve) {
    var promise = Promise.resolve();
    notify = function () {
      promise.then(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global, flush);
    };
  }

  return function (fn) {
    var task = { fn: fn, next: undefined };
    if (last) last.next = task;
    if (!head) {
      head = task;
      notify();
    } last = task;
  };
};


/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

var hide = __webpack_require__(18);
module.exports = function (target, src, safe) {
  for (var key in src) {
    if (safe && target[key]) target[key] = src[key];
    else hide(target, key, src[key]);
  } return target;
};


/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(9);
var core = __webpack_require__(8);
var dP = __webpack_require__(14);
var DESCRIPTORS = __webpack_require__(15);
var SPECIES = __webpack_require__(11)('species');

module.exports = function (KEY) {
  var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__(11)('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// https://github.com/tc39/proposal-promise-finally

var $export = __webpack_require__(12);
var core = __webpack_require__(8);
var global = __webpack_require__(9);
var speciesConstructor = __webpack_require__(71);
var promiseResolve = __webpack_require__(74);

$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
  var C = speciesConstructor(this, core.Promise || global.Promise);
  var isFunction = typeof onFinally == 'function';
  return this.then(
    isFunction ? function (x) {
      return promiseResolve(C, onFinally()).then(function () { return x; });
    } : onFinally,
    isFunction ? function (e) {
      return promiseResolve(C, onFinally()).then(function () { throw e; });
    } : onFinally
  );
} });


/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// https://github.com/tc39/proposal-promise-try
var $export = __webpack_require__(12);
var newPromiseCapability = __webpack_require__(61);
var perform = __webpack_require__(73);

$export($export.S, 'Promise', { 'try': function (callbackfn) {
  var promiseCapability = newPromiseCapability.f(this);
  var result = perform(callbackfn);
  (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
  return promiseCapability.promise;
} });


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

exports.default = debounce;

/***/ }),
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _ct = __webpack_require__(145);

var _ct2 = _interopRequireDefault(_ct);

var _view = __webpack_require__(342);

var _view2 = _interopRequireDefault(_view);

var _view3 = __webpack_require__(343);

var _view4 = _interopRequireDefault(_view3);

var _view5 = __webpack_require__(344);

var _view6 = _interopRequireDefault(_view5);

var _view7 = __webpack_require__(345);

var _view8 = _interopRequireDefault(_view7);

var _view9 = __webpack_require__(346);

var _view10 = _interopRequireDefault(_view9);

var _view11 = __webpack_require__(347);

var _view12 = _interopRequireDefault(_view11);

var _viewUser = __webpack_require__(348);

var _viewUser2 = _interopRequireDefault(_viewUser);

var _view13 = __webpack_require__(349);

var _view14 = _interopRequireDefault(_view13);

var _state = __webpack_require__(119);

var _view15 = __webpack_require__(118);

var _view16 = _interopRequireDefault(_view15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-user', 'PROFILE', 'My Profile'], ['fa-cog', 'ADMIN', 'Admin'], ['fa-cube', 'SWAP', 'Swap accounts'], ['fa-sign-out', 'OUT', 'Sign out']].reverse();

var FOOTER_ACTIONS = [['fa-flag-checkered', 'NOTIFICATIONS'], ['fa-building', 'SWAP', 'Swap Accounts'], ['fa-sign-out', 'OUT', 'Sign out']].reverse();

var ViewMain = function (_React$Component) {
    (0, _inherits3.default)(ViewMain, _React$Component);

    function ViewMain(props) {
        (0, _classCallCheck3.default)(this, ViewMain);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewMain.__proto__ || (0, _getPrototypeOf2.default)(ViewMain)).call(this, props));

        _this._onChange = function () {
            _this.setState(_state.state);
        };

        _this._action = function (action, data) {
            switch (action) {
                case 'ADMIN':
                    document.location = '/admin';
                    break;
                case 'OUT':
                    document.location = '/logout';
                    break;
                case 'PROFILE':
                    break;
                case 'SWAP':
                    break;
                default:
                    break;
            }
        };

        _this.state = _this.props.state;
        return _this;
    }

    (0, _createClass3.default)(ViewMain, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.state.on('VIEW_CHANGE', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.state.off("VIEW_CHANGE", this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {

            var view = void 0;

            if (_state.state.activeView == 'OVERVIEW') view = React.createElement(_view2.default, null);
            if (_state.state.activeView == 'DASHBOARD') view = React.createElement(_view6.default, null);
            if (_state.state.activeView == 'REPORTING') view = React.createElement(_view4.default, null);
            if (_state.state.activeView == 'PLOTS') view = React.createElement(_view8.default, null);
            if (_state.state.activeView == 'NOTEBOOKS') view = React.createElement(_view10.default, null);
            // if (state.activeView == 'ALERTS') view = <ViewAlerts/>;
            if (_state.state.activeView == 'ALERTS') view = React.createElement(_viewUser2.default, null);
            if (_state.state.activeView == 'MAPS') view = React.createElement(_view14.default, null);
            console.log(view);

            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Block,
                    { width: '25%', borderRight: true, yO: true },
                    React.createElement(_ct2.default, null)
                ),
                view
            );
        }
    }]);
    return ViewMain;
}(React.Component);

exports.default = ViewMain;

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _state = __webpack_require__(119);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ITEMS = [['fa-user', 'Jeff Uren', false, 'OVERVIEW'], ['fa-exclamation-triangle', 'Alerts', false, 'ALERTS'], ['fa-tachometer', 'Measles', false, 'DASHBOARD'], ['fa-tachometer', 'Malaria', false, 'DASHBOARD'], ['fa-tachometer', 'All causes', false, 'DASHBOARD'], ['fa-users', 'Malaria team', false, 'TEAM'], ['fa-users', 'Measles team', false, 'TEAM'], ['fa-map-marker', 'Tracing', false, 'TRACING'], ['fa-clipboard', 'Reporting', true], ['fa-map-marker', 'Alpha Facility', false, 'REPORTING'], ['fa-map-marker', 'Beta Facility', false, 'REPORTING'], ['fa-map-marker', 'Delta Facility', false, 'REPORTING'], ['sep'], ['fa-clipboard', 'Reporting', true], ['fa-th-list', 'IDSR Weekly', false, 'REPORTING_ADMIN'], ['fa-th-list', 'Diphtheria line list', false, 'REPORTING_ADMIN'], ['fa-th-list', 'Cholera line list', false, 'REPORTING_ADMIN'], ['fa-th-list', 'Nutrition Survey', false, 'REPORTING_ADMIN'], ['sep'], ['fa-map-marker', 'Location Explorer', false, 'LOCATIONS'], ['fa-book', 'Documents', false, 'DOCUMENTS'], ['fa-chart-line', 'Plotting', false, 'PLOTS'], ['fa-book', 'Notebooks', false, 'NOTEBOOKS'], ['fa-map', 'Maps', false, 'MAPS'], ['fa-graduation-cap', 'IDSR Guidance', false, 'WIKI'], ['fa-graduation-cap', 'Wikis', false, 'WIKIS'], ['fa-search', 'M&E Auditor', false, 'AUDIT']];

var MenuItem = function (_React$Component) {
    (0, _inherits3.default)(MenuItem, _React$Component);

    function MenuItem() {
        var _ref;

        var _temp, _this, _ret;

        (0, _classCallCheck3.default)(this, MenuItem);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = MenuItem.__proto__ || (0, _getPrototypeOf2.default)(MenuItem)).call.apply(_ref, [this].concat(args))), _this), _this._select = function () {
            _state.state.addComponent(_this.props.data[3]);
        }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
    }

    (0, _createClass3.default)(MenuItem, [{
        key: 'render',
        value: function render() {
            if (this.props.data[2] == true) {
                return React.createElement(
                    'div',
                    { className: 'header' },
                    React.createElement('i', { className: "fal " + this.props.data[0] }),
                    '\xA0',
                    this.props.data[1]
                );
            }

            if (this.props.data[0] == 'sep') {
                return React.createElement('div', { className: 'separator' });
            }

            return React.createElement(
                'div',
                { className: 'item', onClick: this._select },
                React.createElement('i', { className: "fal " + this.props.data[0] }),
                '\xA0',
                this.props.data[1]
            );
        }
    }]);
    return MenuItem;
}(React.Component);

var ControlMenu = function (_React$Component2) {
    (0, _inherits3.default)(ControlMenu, _React$Component2);

    function ControlMenu(props) {
        (0, _classCallCheck3.default)(this, ControlMenu);
        return (0, _possibleConstructorReturn3.default)(this, (ControlMenu.__proto__ || (0, _getPrototypeOf2.default)(ControlMenu)).call(this, props));
    }

    (0, _createClass3.default)(ControlMenu, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'front-menu' },
                ITEMS.map(function (item) {
                    return React.createElement(MenuItem, { data: item });
                })
            );
        }
    }]);
    return ControlMenu;
}(React.Component);

exports.default = ControlMenu;

/***/ }),
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */,
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */,
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */,
/* 294 */,
/* 295 */,
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */,
/* 302 */,
/* 303 */,
/* 304 */,
/* 305 */,
/* 306 */,
/* 307 */,
/* 308 */,
/* 309 */,
/* 310 */,
/* 311 */,
/* 312 */,
/* 313 */,
/* 314 */,
/* 315 */,
/* 316 */,
/* 317 */,
/* 318 */,
/* 319 */,
/* 320 */,
/* 321 */,
/* 322 */,
/* 323 */,
/* 324 */,
/* 325 */,
/* 326 */,
/* 327 */,
/* 328 */,
/* 329 */,
/* 330 */,
/* 331 */,
/* 332 */,
/* 333 */,
/* 334 */,
/* 335 */,
/* 336 */,
/* 337 */,
/* 338 */,
/* 339 */,
/* 340 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _view = __webpack_require__(144);

var _view2 = _interopRequireDefault(_view);

var _view3 = __webpack_require__(350);

var _view4 = _interopRequireDefault(_view3);

var _state = __webpack_require__(119);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ = new _state.StateMain();

ReactDOM.render(React.createElement(_view4.default, { state: _ }), document.getElementById("app"));

/***/ }),
/* 341 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NullComponent = function (_React$Component) {
    (0, _inherits3.default)(NullComponent, _React$Component);

    function NullComponent() {
        (0, _classCallCheck3.default)(this, NullComponent);
        return (0, _possibleConstructorReturn3.default)(this, (NullComponent.__proto__ || (0, _getPrototypeOf2.default)(NullComponent)).apply(this, arguments));
    }

    (0, _createClass3.default)(NullComponent, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    null,
                    'Component undefined'
                )
            );
        }
    }]);
    return NullComponent;
}(React.Component);

/***/ }),
/* 342 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(34);

var _c4 = _interopRequireDefault(_c3);

var _constants = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TASK_OPTIONS = [['ALL', 'All tasks'], ['RECORD_DUE', 'Records due'], ['ALERTS', 'Alerts'], ['REGISTER', 'Registration requests'], ['LOCATION_ACCESS', 'Location access'], ['LOCATION_CREATE', 'Location (new)']];

var ACTIVITY_TYPES = [['ALL', 'All activity'], ['RECORD', 'Record submissions'], ['ALERTS', 'Alerts & Actions'], ['TASKS', 'Task actions']];

var ViewOverview = function (_React$Component) {
    (0, _inherits3.default)(ViewOverview, _React$Component);

    function ViewOverview(props) {
        (0, _classCallCheck3.default)(this, ViewOverview);
        return (0, _possibleConstructorReturn3.default)(this, (ViewOverview.__proto__ || (0, _getPrototypeOf2.default)(ViewOverview)).call(this, props));
    }

    (0, _createClass3.default)(ViewOverview, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { width: '50%', borderRight: true },
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(
                            'div',
                            { className: 'tbar-title' },
                            React.createElement('i', { className: 'fal fa-check-square' }),
                            '\xA0Tasks'
                        ),
                        React.createElement(
                            'div',
                            { className: 'right', style: { marginTop: '7px', marginRight: '8px' } },
                            React.createElement(_c4.default, {
                                value: 'ALL',
                                options: TASK_OPTIONS })
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { yO: true, style: { background: _constants.COLORS.snow0 } },
                        React.createElement(
                            'div',
                            { className: 'task-list' },
                            React.createElement(
                                'div',
                                { className: 'header', style: { color: _constants.COLORS.aurora0 } },
                                'High Priority'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'User account request (jduren@gmail.com)'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    'Jeff Uren (jduren@gmail.com) has requested an account.'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Approve'
                                    ),
                                    React.createElement(
                                        'button',
                                        null,
                                        'Reject'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'Location access request'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    'Jeff Uren (jduren@gmail.com) has requested access to Alpha Facility'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Approve'
                                    ),
                                    React.createElement(
                                        'button',
                                        null,
                                        'Reject'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'Alert verification required'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    React.createElement(
                                        'p',
                                        null,
                                        'An alert in one of your locations requires verification'
                                    ),
                                    React.createElement(
                                        'a',
                                        { href: '#' },
                                        'view alert details'
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Verify'
                                    ),
                                    React.createElement(
                                        'button',
                                        null,
                                        'Discard'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header', style: { color: _constants.COLORS.aurora1 } },
                                'Low Priority'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'New location request'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    'User Jeff Uren (jduren@gmail.com) has requested creation of a new location ',
                                    React.createElement(
                                        'strong',
                                        null,
                                        'Echo Facility'
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Approve'
                                    ),
                                    React.createElement(
                                        'button',
                                        null,
                                        'Reject'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'Form submission approval'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    'A ',
                                    React.createElement(
                                        'strong',
                                        null,
                                        'Quarterly report'
                                    ),
                                    ' for Alpha Facility is awaiting approval.'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Approve'
                                    ),
                                    React.createElement(
                                        'button',
                                        null,
                                        'Reject'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header', style: { color: _constants.COLORS.aurora4 } },
                                'Reporting'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                React.createElement(
                                    'div',
                                    { className: 'title' },
                                    'Weekly IDSR due for Alpha Facility Week 1'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'body' },
                                    'A Weekly IDSR submission is do for ',
                                    React.createElement(
                                        'strong',
                                        null,
                                        'Alpha Facility'
                                    ),
                                    ' for week 1 of 2019'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'actions' },
                                    React.createElement(
                                        'button',
                                        null,
                                        'Create report'
                                    )
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { height: '30px', borderBottom: true, addClass: 'tbar' },
                        React.createElement(
                            'div',
                            { className: 'tbar-title' },
                            React.createElement('i', { className: 'fal fa-bars' }),
                            '\xA0Activity'
                        ),
                        React.createElement(
                            'div',
                            { className: 'right', style: { marginTop: '7px', marginRight: '8px' } },
                            React.createElement(_c4.default, {
                                value: 'ALL',
                                options: ACTIVITY_TYPES })
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { yO: true, style: { background: _constants.COLORS.snow0 } },
                        React.createElement(
                            'div',
                            { className: 'activity-list' },
                            React.createElement(
                                'div',
                                { className: 'pinned' },
                                React.createElement(
                                    'div',
                                    { className: 'content' },
                                    React.createElement(
                                        'p',
                                        null,
                                        React.createElement('i', { className: 'fal fa-exclamation-triangle' }),
                                        '\xA0Maintenance'
                                    ),
                                    React.createElement(
                                        'p',
                                        null,
                                        'Sonoma will be down for approximately one (1) hour on January 1st 2018 for maintenance.'
                                    )
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'award' },
                                React.createElement(
                                    'p',
                                    null,
                                    React.createElement('i', { className: 'fal fa-trophy-alt' }),
                                    '\xA0Congratulations! You\'ve submitted your first report!! Well done, you\'re well on your way to getting a handle on using Sonoma.'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header' },
                                'Today'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header' },
                                'Wed, Jan 1st 2017'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item clickable' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header' },
                                'Wed, Dec 31st 2017'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item clickable' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'header' },
                                'Wed, Dec 30th 2017'
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item clickable' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            ),
                            React.createElement(
                                'div',
                                { className: 'item' },
                                'Jeff Uren (jduren@gmail.com) submitted ',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Weekly IDSR (W1 2019)'
                                ),
                                ' in',
                                React.createElement(
                                    'strong',
                                    null,
                                    'Alpha Facility'
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewOverview;
}(React.Component);

exports.default = ViewOverview;

/***/ }),
/* 343 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _c3 = __webpack_require__(34);

var _c4 = _interopRequireDefault(_c3);

var _constants = __webpack_require__(10);

var _state = __webpack_require__(119);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TMP_LINK_DROP = [['ALL', 'All forms'], ['', 'Weekly IDSR']];

var ACTIONS = [['fa-plus', 'CREATE', 'Create new record'], ['fa-external-link', 'EXTERNAL', 'Open in new tab']].reverse();

var ViewReporting = function (_React$Component) {
    (0, _inherits3.default)(ViewReporting, _React$Component);

    function ViewReporting(props) {
        (0, _classCallCheck3.default)(this, ViewReporting);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewReporting.__proto__ || (0, _getPrototypeOf2.default)(ViewReporting)).call(this, props));

        _this._createRecord = function () {
            _state.state.addComponent('RECORD', {});
        };

        _this._browseRecords = function () {
            _state.state.addComponent('RECORDS', {});
        };

        return _this;
    }

    (0, _createClass3.default)(ViewReporting, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Reporting: Alpha Facility'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { style: { padding: '8px', flex: 0, flexBasis: 0, flexShrink: 0 }, borderBottom: true },
                    React.createElement(
                        'table',
                        { className: 'completeness' },
                        React.createElement(
                            'thead',
                            null,
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    { className: 'keystone' },
                                    '\xA0'
                                ),
                                React.createElement(
                                    'th',
                                    { width: '20px', className: 'nav' },
                                    React.createElement('i', { className: 'fal fa-caret-left' })
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    null,
                                    'date'
                                ),
                                React.createElement(
                                    'th',
                                    { className: 'nav' },
                                    React.createElement('i', { className: 'fal fa-caret-right' })
                                )
                            )
                        ),
                        React.createElement(
                            'tbody',
                            null,
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    'Weekly IDSR'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                )
                            ),
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    'Cholera line list'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                )
                            ),
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    'Nutrition survey'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                )
                            ),
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    'Weekly Surveillance'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                )
                            ),
                            React.createElement(
                                'tr',
                                null,
                                React.createElement(
                                    'th',
                                    null,
                                    'Clinic Evaluation'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    null,
                                    'X'
                                ),
                                React.createElement(
                                    'td',
                                    { colSpan: 2 },
                                    'X'
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    _layout.Hbox,
                    { style: { flex: 2 } },
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            _layout.Block,
                            { height: '30px', addClass: 'tbar', borderBottom: true, borderRight: true },
                            React.createElement(
                                'div',
                                { className: 'tbar-title' },
                                React.createElement('i', { className: 'fal fa-database' }),
                                '\xA0Records'
                            ),
                            React.createElement(
                                'div',
                                { className: 'right', style: { marginTop: '8px', marginRight: '8px' } },
                                React.createElement(_c4.default, {
                                    value: 'ALL',
                                    options: TMP_LINK_DROP })
                            )
                        ),
                        React.createElement(
                            _layout.Block,
                            { borderRight: true, style: { background: _constants.COLORS.snow0 }, yO: true },
                            React.createElement(
                                'div',
                                { className: 'task-list' },
                                React.createElement(
                                    'div',
                                    { className: 'item', style: { borderLeft: '6px solid red' } },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR - Alpha Facility - W2 2018'
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item', style: { borderLeft: '6px solid green' } },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR - Alpha Facility - W1 2018'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted:'
                                            ),
                                            ' 2018-01-01'
                                        ),
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted by:'
                                            ),
                                            ' Jeff Uren (jduren@gmail.com)'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item', style: { borderLeft: '6px solid green' } },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR - Alpha Facility - W53 2017'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted:'
                                            ),
                                            ' 2018-01-01'
                                        ),
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted by:'
                                            ),
                                            ' Jeff Uren (jduren@gmail.com)'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item', style: { borderLeft: '6px solid green' } },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR - Alpha Facility - W53 2017'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted:'
                                            ),
                                            ' 2018-01-01'
                                        ),
                                        React.createElement(
                                            'p',
                                            null,
                                            React.createElement(
                                                'strong',
                                                null,
                                                'Submitted by:'
                                            ),
                                            ' Jeff Uren (jduren@gmail.com)'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item', style: { borderLeft: '6px solid red' } },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR - Alpha Facility - W53 2017'
                                    )
                                )
                            )
                        )
                    ),
                    React.createElement(
                        _layout.Vbox,
                        null,
                        React.createElement(
                            _layout.Block,
                            { height: '30px', addClass: 'tbar', borderBottom: true },
                            React.createElement(
                                'div',
                                { className: 'tbar-title' },
                                React.createElement('i', { className: 'fal fa-th-list' }),
                                '\xA0Forms'
                            )
                        ),
                        React.createElement(
                            _layout.Block,
                            { yO: true, style: { background: _constants.COLORS.snow0 } },
                            React.createElement(
                                'div',
                                { className: 'task-list' },
                                React.createElement(
                                    'div',
                                    { className: 'header' },
                                    'Active Forms'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item' },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Weekly IDSR'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            'Weekly submitted totals for top diseases at a given location.'
                                        )
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'actions' },
                                        React.createElement(
                                            'button',
                                            { onClick: this._createRecord },
                                            'Create record'
                                        ),
                                        React.createElement(
                                            'button',
                                            { onClick: this._browseRecords },
                                            'Browse'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'header' },
                                    'Inactive Forms'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item' },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Diphtheria line list'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            'Used for tracking cases of diptheria for a location.'
                                        )
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'actions' },
                                        React.createElement(
                                            'button',
                                            null,
                                            'Start reporting'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item' },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Cholera line list V2'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            'Used for recording cases of cholera at a given location.'
                                        )
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'actions' },
                                        React.createElement(
                                            'button',
                                            null,
                                            'Start reporting'
                                        ),
                                        React.createElement(
                                            'button',
                                            null,
                                            'Browse'
                                        )
                                    )
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'header' },
                                    'Archived Forms'
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'item' },
                                    React.createElement(
                                        'div',
                                        { className: 'title' },
                                        'Cholera line list V1'
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'body' },
                                        React.createElement(
                                            'p',
                                            null,
                                            'Used for recording cases of cholera at a given location.'
                                        )
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'actions' },
                                        React.createElement(
                                            'button',
                                            null,
                                            'Browse'
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewReporting;
}(React.Component);

exports.default = ViewReporting;

/***/ }),
/* 344 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-download', 'DOWNLOAD', 'Download data'], ['fa-print', 'PRINT', 'Print dashboard'], ['fa-external-link', 'EXTERNAL', 'Open in a new tab']].reverse();

var ViewDashboard = function (_React$Component) {
    (0, _inherits3.default)(ViewDashboard, _React$Component);

    function ViewDashboard(props) {
        (0, _classCallCheck3.default)(this, ViewDashboard);
        return (0, _possibleConstructorReturn3.default)(this, (ViewDashboard.__proto__ || (0, _getPrototypeOf2.default)(ViewDashboard)).call(this, props));
    }

    (0, _createClass3.default)(ViewDashboard, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-tachometer' }),
                        '\xA0Dashboard name'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    _layout.Block,
                    null,
                    '[dashboard]'
                )
            );
        }
    }]);
    return ViewDashboard;
}(React.Component);

exports.default = ViewDashboard;

/***/ }),
/* 345 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new plot']];

var NOTEBOOK_ACTIONS = [['fa-pencil', 'EDIT', 'Edit notebook'], ['fa-thumbtack', 'PIN', 'Pin this notebook'], ['fa-copy', 'DUPE', 'Duplicate this notebook'], ['fa-trash', 'DELETE', 'Delete this notebook']].reverse();

var Notebook = function (_React$Component) {
    (0, _inherits3.default)(Notebook, _React$Component);

    function Notebook() {
        (0, _classCallCheck3.default)(this, Notebook);
        return (0, _possibleConstructorReturn3.default)(this, (Notebook.__proto__ || (0, _getPrototypeOf2.default)(Notebook)).apply(this, arguments));
    }

    (0, _createClass3.default)(Notebook, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'item' },
                React.createElement(
                    _layout.Vbox,
                    { style: { height: '100%' } },
                    React.createElement(
                        _layout.Block,
                        { height: '30px' },
                        React.createElement(
                            'div',
                            { className: 'title' },
                            'Plot 1'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'div',
                            { className: 'description' },
                            'This is a notebook containing some stuff and things.'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'tbar', height: '30px' },
                        React.createElement(_c2.default, {
                            actions: NOTEBOOK_ACTIONS,
                            right: true })
                    )
                )
            );
        }
    }]);
    return Notebook;
}(React.Component);

var ViewPlots = function (_React$Component2) {
    (0, _inherits3.default)(ViewPlots, _React$Component2);

    function ViewPlots(props) {
        (0, _classCallCheck3.default)(this, ViewPlots);
        return (0, _possibleConstructorReturn3.default)(this, (ViewPlots.__proto__ || (0, _getPrototypeOf2.default)(ViewPlots)).call(this, props));
    }

    (0, _createClass3.default)(ViewPlots, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-chart-line' }),
                        '\xA0Plots'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { height: '30px', style: { background: _const2.default.snow0 } },
                    React.createElement(
                        'div',
                        { className: 'search-wrapper', style: { width: '33.33%', paddingLeft: '16px', paddingTop: '8px' } },
                        React.createElement('input', {
                            placeholder: 'Search notebooks...',
                            type: 'text', className: 'search-input' })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'grid' },
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'My Plots'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'Shared Plots'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null)
                    )
                )
            );
        }
    }]);
    return ViewPlots;
}(React.Component);

exports.default = ViewPlots;

/***/ }),
/* 346 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new plot']];

var NOTEBOOK_ACTIONS = [['fa-pencil', 'EDIT', 'Edit notebook'], ['fa-thumbtack', 'PIN', 'Pin this notebook'], ['fa-copy', 'DUPE', 'Duplicate this notebook'], ['fa-trash', 'DELETE', 'Delete this notebook']].reverse();

var Notebook = function (_React$Component) {
    (0, _inherits3.default)(Notebook, _React$Component);

    function Notebook() {
        (0, _classCallCheck3.default)(this, Notebook);
        return (0, _possibleConstructorReturn3.default)(this, (Notebook.__proto__ || (0, _getPrototypeOf2.default)(Notebook)).apply(this, arguments));
    }

    (0, _createClass3.default)(Notebook, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'item' },
                React.createElement(
                    _layout.Vbox,
                    { style: { height: '100%' } },
                    React.createElement(
                        _layout.Block,
                        { height: '30px' },
                        React.createElement(
                            'div',
                            { className: 'title' },
                            'Notebook 1'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'div',
                            { className: 'description' },
                            'This is a notebook containing some stuff and things.'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'tbar', height: '30px' },
                        React.createElement(_c2.default, {
                            actions: NOTEBOOK_ACTIONS,
                            right: true })
                    )
                )
            );
        }
    }]);
    return Notebook;
}(React.Component);

var ViewNotebooks = function (_React$Component2) {
    (0, _inherits3.default)(ViewNotebooks, _React$Component2);

    function ViewNotebooks(props) {
        (0, _classCallCheck3.default)(this, ViewNotebooks);
        return (0, _possibleConstructorReturn3.default)(this, (ViewNotebooks.__proto__ || (0, _getPrototypeOf2.default)(ViewNotebooks)).call(this, props));
    }

    (0, _createClass3.default)(ViewNotebooks, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-book' }),
                        '\xA0Notebooks'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { height: '30px', style: { background: _const2.default.snow0 } },
                    React.createElement(
                        'div',
                        { className: 'search-wrapper', style: { width: '33.33%', paddingLeft: '16px', paddingTop: '8px' } },
                        React.createElement('input', {
                            placeholder: 'Search notebooks...',
                            type: 'text', className: 'search-input' })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'grid' },
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'My Notebooks'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'Shared Notebooks'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null)
                    )
                )
            );
        }
    }]);
    return ViewNotebooks;
}(React.Component);

exports.default = ViewNotebooks;

/***/ }),
/* 347 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new plot']];

var ViewAlerts = function (_React$Component) {
    (0, _inherits3.default)(ViewAlerts, _React$Component);

    function ViewAlerts(props) {
        (0, _classCallCheck3.default)(this, ViewAlerts);
        return (0, _possibleConstructorReturn3.default)(this, (ViewAlerts.__proto__ || (0, _getPrototypeOf2.default)(ViewAlerts)).call(this, props));
    }

    (0, _createClass3.default)(ViewAlerts, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-exclamation-triangle' }),
                        '\xA0Alerts'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                )
            );
        }
    }]);
    return ViewAlerts;
}(React.Component);

exports.default = ViewAlerts;

/***/ }),
/* 348 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-map', 'MAP', 'View alert map'], ['fa-th-list', 'LIST', 'View alert table'], ['fa-chart-line', 'ANALYSE', 'See alerts metrics'], ['fa-download', 'EXPORT', 'Export alerts']].reverse();

var NOTEBOOK_ACTIONS = [['fa-pencil', 'EDIT', 'Edit notebook'], ['fa-thumbtack', 'PIN', 'Pin this notebook'], ['fa-copy', 'DUPE', 'Duplicate this notebook'], ['fa-trash', 'DELETE', 'Delete this notebook']].reverse();

var Notebook = function (_React$Component) {
    (0, _inherits3.default)(Notebook, _React$Component);

    function Notebook() {
        (0, _classCallCheck3.default)(this, Notebook);
        return (0, _possibleConstructorReturn3.default)(this, (Notebook.__proto__ || (0, _getPrototypeOf2.default)(Notebook)).apply(this, arguments));
    }

    (0, _createClass3.default)(Notebook, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'item' },
                React.createElement(
                    _layout.Vbox,
                    { style: { height: '100%' } },
                    React.createElement(
                        _layout.Block,
                        { height: '30px' },
                        React.createElement(
                            'div',
                            { className: 'title' },
                            'Notebook 1'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'div',
                            { className: 'description' },
                            'This is a notebook containing some stuff and things.'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'tbar', height: '30px' },
                        React.createElement(_c2.default, {
                            actions: NOTEBOOK_ACTIONS,
                            right: true })
                    )
                )
            );
        }
    }]);
    return Notebook;
}(React.Component);

var ViewUserAlerts = function (_React$Component2) {
    (0, _inherits3.default)(ViewUserAlerts, _React$Component2);

    function ViewUserAlerts(props) {
        (0, _classCallCheck3.default)(this, ViewUserAlerts);
        return (0, _possibleConstructorReturn3.default)(this, (ViewUserAlerts.__proto__ || (0, _getPrototypeOf2.default)(ViewUserAlerts)).call(this, props));
    }

    (0, _createClass3.default)(ViewUserAlerts, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-exclamation-triangle' }),
                        '\xA0Alerts'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'grid' },
                        React.createElement(
                            'div',
                            { className: 'header', style: { color: _const2.default.aurora0 } },
                            'Open Alerts'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'Closed Alerts'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null)
                    )
                )
            );
        }
    }]);
    return ViewUserAlerts;
}(React.Component);

exports.default = ViewUserAlerts;

/***/ }),
/* 349 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _const = __webpack_require__(33);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new map']];

var NOTEBOOK_ACTIONS = [['fa-pencil', 'EDIT', 'Edit map'], ['fa-thumbtack', 'PIN', 'Pin this map'], ['fa-copy', 'DUPE', 'Duplicate this map'], ['fa-trash', 'DELETE', 'Delete this map']].reverse();

var Notebook = function (_React$Component) {
    (0, _inherits3.default)(Notebook, _React$Component);

    function Notebook() {
        (0, _classCallCheck3.default)(this, Notebook);
        return (0, _possibleConstructorReturn3.default)(this, (Notebook.__proto__ || (0, _getPrototypeOf2.default)(Notebook)).apply(this, arguments));
    }

    (0, _createClass3.default)(Notebook, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'item' },
                React.createElement(
                    _layout.Vbox,
                    { style: { height: '100%' } },
                    React.createElement(
                        _layout.Block,
                        { height: '30px' },
                        React.createElement(
                            'div',
                            { className: 'title' },
                            'Map 1'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        React.createElement(
                            'div',
                            { className: 'description' },
                            'This is a notebook containing some stuff and things.'
                        )
                    ),
                    React.createElement(
                        _layout.Block,
                        { addClass: 'tbar', height: '30px' },
                        React.createElement(_c2.default, {
                            actions: NOTEBOOK_ACTIONS,
                            right: true })
                    )
                )
            );
        }
    }]);
    return Notebook;
}(React.Component);

var ViewMaps = function (_React$Component2) {
    (0, _inherits3.default)(ViewMaps, _React$Component2);

    function ViewMaps(props) {
        (0, _classCallCheck3.default)(this, ViewMaps);
        return (0, _possibleConstructorReturn3.default)(this, (ViewMaps.__proto__ || (0, _getPrototypeOf2.default)(ViewMaps)).call(this, props));
    }

    (0, _createClass3.default)(ViewMaps, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        React.createElement('i', { className: 'fal fa-map' }),
                        '\xA0Maps'
                    ),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(
                    _layout.Block,
                    { height: '30px', style: { background: _const2.default.snow0 } },
                    React.createElement(
                        'div',
                        { className: 'search-wrapper', style: { width: '33.33%', paddingLeft: '16px', paddingTop: '8px' } },
                        React.createElement('input', {
                            placeholder: 'Search notebooks...',
                            type: 'text', className: 'search-input' })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'grid' },
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'My Maps'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(
                            'div',
                            { className: 'header' },
                            'Shared Maps'
                        ),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null),
                        React.createElement(Notebook, null)
                    )
                )
            );
        }
    }]);
    return ViewMaps;
}(React.Component);

exports.default = ViewMaps;

/***/ }),
/* 350 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _constants = __webpack_require__(10);

var _ct = __webpack_require__(145);

var _ct2 = _interopRequireDefault(_ct);

var _c3 = __webpack_require__(351);

var _c4 = _interopRequireDefault(_c3);

var _tab = __webpack_require__(352);

var _tab2 = _interopRequireDefault(_tab);

var _tab3 = __webpack_require__(353);

var _tab4 = _interopRequireDefault(_tab3);

var _tab5 = __webpack_require__(354);

var _tab6 = _interopRequireDefault(_tab5);

var _tab7 = __webpack_require__(355);

var _tab8 = _interopRequireDefault(_tab7);

var _tab9 = __webpack_require__(356);

var _tab10 = _interopRequireDefault(_tab9);

var _view = __webpack_require__(144);

var _view2 = _interopRequireDefault(_view);

var _state = __webpack_require__(119);

var _view3 = __webpack_require__(118);

var _view4 = _interopRequireDefault(_view3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-user', 'PROFILE', 'My Profile'], ['fa-cog', 'ADMIN', 'Admin']].reverse();

var FOOTER_ACTIONS = [['fa-flag-checkered', 'NOTIFICATIONS'], ['fa-building', 'SWAP', 'Swap Accounts'], ['fa-sign-out', 'OUT', 'Sign out']].reverse();

var ViewRoot = function (_React$Component) {
    (0, _inherits3.default)(ViewRoot, _React$Component);

    function ViewRoot(props) {
        (0, _classCallCheck3.default)(this, ViewRoot);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewRoot.__proto__ || (0, _getPrototypeOf2.default)(ViewRoot)).call(this, props));

        _this._onChange = function () {
            _this.setState(_state.state);
        };

        _this._action = function (action, data) {
            switch (action) {
                case 'ADMIN':
                    document.location = '/admin';
                    break;
                case 'OUT':
                    document.location = '/logout';
                    break;
                case 'PROFILE':
                    break;
                case 'SWAP':
                    break;
                default:
                    break;
            }
        };

        _this.state = _this.props.state;
        return _this;
    }

    (0, _createClass3.default)(ViewRoot, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.state.on('VIEW_CHANGE', this._onChange);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.state.off("VIEW_CHANGE", this._onChange);
        }
    }, {
        key: 'render',
        value: function render() {

            var view = void 0;

            if (_state.state.activeTab) {
                var tabConfig = _state.state.activeTabConfig;

                if (tabConfig.cmp == 'WIKI') view = React.createElement(_tab2.default, null);
                if (tabConfig.cmp == 'LOCATIONS') view = React.createElement(_tab4.default, null);
                if (tabConfig.cmp == 'AUDIT') view = React.createElement(_tab6.default, null);
                if (tabConfig.cmp == 'RECORD') view = React.createElement(_tab8.default, null);
                if (tabConfig.cmp == 'RECORDS') view = React.createElement(_tab10.default, null);
            } else {
                view = React.createElement(_view2.default, { state: _state.state });
            }

            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar', style: { background: _constants.COLORS.frost3 } },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        '\xA0Sonoma'
                    ),
                    React.createElement(_c4.default, null),
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true,
                        onAction: this._action })
                ),
                view,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderTop: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Sonoma V0.0.1 - Logged in since 2017-01-01'
                    ),
                    React.createElement(_c2.default, {
                        actions: FOOTER_ACTIONS,
                        right: true }),
                    React.createElement(
                        'div',
                        { className: 'task-box' },
                        React.createElement(
                            'div',
                            { className: 'handle' },
                            React.createElement('i', { className: 'fal fa-cog' }),
                            '\xA0Jobs (0)'
                        )
                    )
                )
            );
        }
    }]);
    return ViewRoot;
}(React.Component);

exports.default = ViewRoot;

/***/ }),
/* 351 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _state = __webpack_require__(119);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TAB_NAMES = {
    FORM: 'Form: ',
    DEFAULT: ''

};

var TabItem = function (_React$Component) {
    (0, _inherits3.default)(TabItem, _React$Component);

    function TabItem(props) {
        (0, _classCallCheck3.default)(this, TabItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TabItem.__proto__ || (0, _getPrototypeOf2.default)(TabItem)).call(this, props));

        _this._setTab = function () {
            _state.state.setActiveTab(_this.props.data._);
        };

        _this._removeTab = function (e) {
            if (e) e.preventDefault();
            _state.state.removeTab(_this.props.data._);
        };

        return _this;
    }

    (0, _createClass3.default)(TabItem, [{
        key: 'render',
        value: function render() {
            var icon = void 0;

            if (this.props.data.i) icon = React.createElement('i', { className: 'fal ' + this.props.data.i });

            var label = void 0;
            label = this.props.data.cmp;
            // if (TAB_NAMES[this.props.data.n]) label = TAB_NAMES[this.props.data.n];


            var className = "tab";

            if (_state.state.activeTab == this.props.data._) className += ' active';

            var closeable = true;
            if (this.props.data.n == 'DEFAULT') closeable = false;

            var paddingRight = '8px';
            if (!label) paddingRight = undefined;

            return React.createElement(
                'div',
                { className: className, onClick: this._setTab },
                React.createElement(
                    _layout.Hbox,
                    null,
                    icon ? React.createElement(
                        _layout.Block,
                        {
                            onClick: this._setTab,
                            width: '20px',
                            style: { paddingRight: paddingRight, paddingTop: '5px' } },
                        icon
                    ) : null,
                    label ? React.createElement(
                        _layout.Block,
                        {
                            onClick: this._setTab,
                            style: { paddingRight: '8px', paddingTop: '5px' } },
                        label
                    ) : null,
                    closeable ? React.createElement(
                        _layout.Block,
                        { width: '20px', style: { textAlign: 'center', paddingTop: '5px' }, onClick: this._removeTab },
                        React.createElement('i', { className: 'fal fa-times hover-red' })
                    ) : null
                )
            );
        }
    }]);
    return TabItem;
}(React.Component);

var Tabs = function (_React$Component2) {
    (0, _inherits3.default)(Tabs, _React$Component2);

    function Tabs(props) {
        (0, _classCallCheck3.default)(this, Tabs);
        return (0, _possibleConstructorReturn3.default)(this, (Tabs.__proto__ || (0, _getPrototypeOf2.default)(Tabs)).call(this, props));
    }

    (0, _createClass3.default)(Tabs, [{
        key: 'render',
        value: function render() {

            var tabs = [{
                n: 'DEFAULT',
                i: 'fa-tachometer',
                _: 'DEFAULT'
            }];

            _state.state.tabs.forEach(function (tab) {
                tabs.push(tab);
            });

            return React.createElement(
                'div',
                { className: 'tab-bar' },
                tabs.map(function (item) {
                    return React.createElement(TabItem, { data: item });
                })
            );
        }
    }]);
    return Tabs;
}(React.Component);

exports.default = Tabs;

/***/ }),
/* 352 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _constants = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'ADD', 'Add a new page'], ['fa-file-pdf', 'PDF_WIKI', 'Download pdf of Wiki']];

var PAGE_ACTIONS = [['fa-pencil', 'EDIT', 'Edit page'], ['fa-print', 'PRINT', 'Print page'], ['fa-file-pdf', 'PDF', 'Download wiki as PDF']];

var WikiTree = function (_React$Component) {
    (0, _inherits3.default)(WikiTree, _React$Component);

    function WikiTree() {
        (0, _classCallCheck3.default)(this, WikiTree);
        return (0, _possibleConstructorReturn3.default)(this, (WikiTree.__proto__ || (0, _getPrototypeOf2.default)(WikiTree)).apply(this, arguments));
    }

    (0, _createClass3.default)(WikiTree, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                { borderRight: true, width: '25%' },
                React.createElement(_layout.Block, { height: '30px' }),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'page-tree-list' },
                        React.createElement(
                            'div',
                            { className: 'item' },
                            'Home'
                        )
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { addClass: 'tbar', borderTop: true, height: '30px' },
                    React.createElement(_c2.default, {
                        actions: ACTIONS })
                )
            );
        }
    }]);
    return WikiTree;
}(React.Component);

var WikiPage = function (_React$Component2) {
    (0, _inherits3.default)(WikiPage, _React$Component2);

    function WikiPage() {
        (0, _classCallCheck3.default)(this, WikiPage);
        return (0, _possibleConstructorReturn3.default)(this, (WikiPage.__proto__ || (0, _getPrototypeOf2.default)(WikiPage)).apply(this, arguments));
    }

    (0, _createClass3.default)(WikiPage, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Page name'
                    ),
                    React.createElement(_c2.default, {
                        right: true,
                        actions: PAGE_ACTIONS })
                )
            );
        }
    }]);
    return WikiPage;
}(React.Component);

var TabWiki = function (_React$Component3) {
    (0, _inherits3.default)(TabWiki, _React$Component3);

    function TabWiki(props) {
        (0, _classCallCheck3.default)(this, TabWiki);
        return (0, _possibleConstructorReturn3.default)(this, (TabWiki.__proto__ || (0, _getPrototypeOf2.default)(TabWiki)).call(this, props));
    }

    (0, _createClass3.default)(TabWiki, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { height: '30px', borderBottom: true, addClass: 'tbar' },
                    React.createElement(
                        'div',
                        { className: 'tbar-title' },
                        'Wiki: Some wiki name'
                    )
                ),
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(WikiTree, null),
                    React.createElement(_layout.Block, { yO: true, style: { background: _constants.COLORS.snow0 } })
                )
            );
        }
    }]);
    return TabWiki;
}(React.Component);

exports.default = TabWiki;

/***/ }),
/* 353 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _api = __webpack_require__(48);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocationTreeNode = function (_React$Component) {
    (0, _inherits3.default)(LocationTreeNode, _React$Component);

    function LocationTreeNode(props) {
        (0, _classCallCheck3.default)(this, LocationTreeNode);
        return (0, _possibleConstructorReturn3.default)(this, (LocationTreeNode.__proto__ || (0, _getPrototypeOf2.default)(LocationTreeNode)).call(this, props));
    }

    (0, _createClass3.default)(LocationTreeNode, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'tree-node' },
                React.createElement(
                    _layout.Hbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { width: '20px' },
                        React.createElement('i', { className: 'fal fa-caret-right' })
                    ),
                    React.createElement(
                        _layout.Block,
                        null,
                        this.props.data.name
                    )
                )
            );
        }
    }]);
    return LocationTreeNode;
}(React.Component);

var LocationTree = function (_React$Component2) {
    (0, _inherits3.default)(LocationTree, _React$Component2);

    function LocationTree(props) {
        (0, _classCallCheck3.default)(this, LocationTree);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (LocationTree.__proto__ || (0, _getPrototypeOf2.default)(LocationTree)).call(this, props));

        _this2.state = {
            data: []
        };

        (0, _api.api)("com.sonoma.locations", {
            limit: 100,
            offset: 0
        }).then(function (resp) {
            _this2.setState({
                data: resp
            });
        });

        return _this2;
    }

    (0, _createClass3.default)(LocationTree, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                { borderRight: true, width: '25%' },
                React.createElement(_layout.Block, { height: '30px', borderBottom: true, addClass: 'tbar' }),
                React.createElement(
                    _layout.Block,
                    { height: '30px' },
                    React.createElement(
                        'div',
                        { className: 'search-wrapper' },
                        React.createElement('input', {
                            placeholder: 'Search locations...',
                            type: 'text',
                            className: 'search-input' })
                    )
                ),
                React.createElement(
                    _layout.Block,
                    { yO: true },
                    React.createElement(
                        'div',
                        { className: 'loc-tree' },
                        this.state.data.map(function (item) {
                            return React.createElement(LocationTreeNode, { data: item });
                        })
                    )
                )
            );
        }
    }]);
    return LocationTree;
}(React.Component);

var Location = function (_React$Component3) {
    (0, _inherits3.default)(Location, _React$Component3);

    function Location(props) {
        (0, _classCallCheck3.default)(this, Location);
        return (0, _possibleConstructorReturn3.default)(this, (Location.__proto__ || (0, _getPrototypeOf2.default)(Location)).call(this, props));
    }

    (0, _createClass3.default)(Location, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(_layout.Block, { height: '30px', addClass: 'tbar', borderBottom: true }),
                React.createElement(_layout.Block, { yO: true })
            );
        }
    }]);
    return Location;
}(React.Component);

var TabLocations = function (_React$Component4) {
    (0, _inherits3.default)(TabLocations, _React$Component4);

    function TabLocations(props) {
        (0, _classCallCheck3.default)(this, TabLocations);
        return (0, _possibleConstructorReturn3.default)(this, (TabLocations.__proto__ || (0, _getPrototypeOf2.default)(TabLocations)).call(this, props));
    }

    (0, _createClass3.default)(TabLocations, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(LocationTree, null),
                React.createElement(Location, null)
            );
        }
    }]);
    return TabLocations;
}(React.Component);

exports.default = TabLocations;

/***/ }),
/* 354 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AUDIT_ACTIONS = [['fa-save', 'SAVE', 'Save this audit'], ['fa-email', 'SHARE', 'Share this audit'], ['fa-file-pdf', 'PDF', 'Download audit as PDF'], ['fa-print', 'PRINT', 'Print this audit'], ['fa-ellipsis-v', 'NOOP'], ['fa-expand-alt', 'EXPAND', 'Expand all nodes'], ['fa-compress-alt', 'COLLAPSE', 'Collapse all nodes']].reverse();

var TabAuditor = function (_React$Component) {
    (0, _inherits3.default)(TabAuditor, _React$Component);

    function TabAuditor(props) {
        (0, _classCallCheck3.default)(this, TabAuditor);
        return (0, _possibleConstructorReturn3.default)(this, (TabAuditor.__proto__ || (0, _getPrototypeOf2.default)(TabAuditor)).call(this, props));
    }

    (0, _createClass3.default)(TabAuditor, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(
                    _layout.Vbox,
                    { borderRight: true, width: '20%' },
                    React.createElement(_layout.Block, { yO: true })
                ),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { height: '30px', addClass: 'tbar', borderBottom: true },
                        React.createElement(_c2.default, {
                            right: true,
                            actions: AUDIT_ACTIONS })
                    ),
                    React.createElement(_layout.Block, { yO: true })
                )
            );
        }
    }]);
    return TabAuditor;
}(React.Component);

;

exports.default = TabAuditor;

/***/ }),
/* 355 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

var _constants = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RECORD_ACTIONS = [['fa-pencil', 'EDIT', 'Amend report'], ['fa-share-square', 'SUBMIT', 'Submit form'], ['fa-save', 'SAVE', 'Save form'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE', 'Retract submission'], ['fa-ellipsis-v', 'NOOP2'], ['fa-print', 'PRINT', 'Print record'], ['fa-file-pdf', 'PDF', 'Download PDF of record']].reverse();

var TabRecord = function (_React$Component) {
    (0, _inherits3.default)(TabRecord, _React$Component);

    function TabRecord(props) {
        (0, _classCallCheck3.default)(this, TabRecord);
        return (0, _possibleConstructorReturn3.default)(this, (TabRecord.__proto__ || (0, _getPrototypeOf2.default)(TabRecord)).call(this, props));
    }

    (0, _createClass3.default)(TabRecord, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Hbox,
                null,
                React.createElement(_layout.Block, { width: '25%', borderRight: true }),
                React.createElement(
                    _layout.Vbox,
                    null,
                    React.createElement(
                        _layout.Block,
                        { addClass: 'tbar', height: '30px', borderBottom: true },
                        React.createElement(
                            'div',
                            { className: 'tbar-title' },
                            'Weekly IDSR'
                        ),
                        React.createElement(_c2.default, {
                            right: true,
                            actions: RECORD_ACTIONS })
                    ),
                    React.createElement(_layout.Block, { yO: true, style: { background: _constants.COLORS.snow2 } })
                )
            );
        }
    }]);
    return TabRecord;
}(React.Component);

exports.default = TabRecord;

/***/ }),
/* 356 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(4);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(2);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(3);

var _inherits3 = _interopRequireDefault(_inherits2);

var _layout = __webpack_require__(6);

var _c = __webpack_require__(7);

var _c2 = _interopRequireDefault(_c);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'CREATE', 'Create new record'], ['fa-ellipsis-v', 'NOOP'], ['fa-download', 'EXPORT', 'Export records']].reverse();

var TabRecords = function (_React$Component) {
    (0, _inherits3.default)(TabRecords, _React$Component);

    function TabRecords(props) {
        (0, _classCallCheck3.default)(this, TabRecords);
        return (0, _possibleConstructorReturn3.default)(this, (TabRecords.__proto__ || (0, _getPrototypeOf2.default)(TabRecords)).call(this, props));
    }

    (0, _createClass3.default)(TabRecords, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                _layout.Vbox,
                null,
                React.createElement(
                    _layout.Block,
                    { borderBottom: true, height: '30px', addClass: 'tbar' },
                    React.createElement(_c2.default, {
                        actions: ACTIONS,
                        right: true })
                ),
                React.createElement(_layout.Block, null)
            );
        }
    }]);
    return TabRecords;
}(React.Component);

exports.default = TabRecords;

/***/ })
/******/ ]);
//# sourceMappingURL=frontend.map