# 5.0.17

- Added new Admin / Outbreaks component
- Added new Outbreaks dashbaord widget
- Added new outbreaks system indicator
- Updated date field to allow clearing the field
- Internal: Added Outbreak selector field
- Internal: Added outbreak API methods
- Internal: Added method to more easily retrieve accessible form list
- Added `unique` property to form fields for version 2 to allow specifying unique columns

