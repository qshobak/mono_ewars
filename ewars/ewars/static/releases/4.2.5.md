# 4.2.5

- (4.2.5.1) Documents now support generation for a specific location
  type
- (4.2.5.2) Fixes a regression in bar/column charts where groupPadding,
  pointPadding and borderWidth were regressed to original values
