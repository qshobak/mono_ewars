# 4.2.1

1. Ordering by column in table widgets
2. Row number limiting in table widgets
3. Fixed an issue where legacy table widgets did not have uuids assigned
   to columns for internal referencing
4. Added LRU caching to analysis logic to speed up calculations
5. Refactored system indicator submissions calculations to use array_aggs
6. Fixes an issue where submission counts would fail for forms submitted
   at more than one location type
7. Implemented new materialized view to store location types associated
   with a form based on DISTINCT against collection
8. Fixes an issue with Moment not parsing single digit months and day
   values correctly in Firefox
9. Removed WYSIWYG field in prep for removal of JQuery
10. Added daily specific interval generation for documents
