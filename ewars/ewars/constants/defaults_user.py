DEFAULTS = [
    "USER_OVERVIEW",
    "USER_SURVEILLANCE",
    "USER_ALERTS"
]

DEV = dict(
    name="Dev",
    icon="fa-code",
    definition=[
        [
            dict(type="METRICS_OVERALL"),
            dict(type="DOCUMENTS")
        ],
        [
            dict(type="ASSIGNMENTS"),
            dict(type="METRICS", title="Metrics Table", metric_layout="TABLE", metrics_items=[]),
        ],
        [
            dict(type="METRICS", title="Metrics Bar", metric_layout="BAR", metrics_items=[])
        ],
        [
            dict(type="METRICS", title="Metrics Minibar", metric_layout="MINI_BAR", metrics_items=[])
        ],
        [
            dict(type="ALERTS", title="Alerts Map")
            # dict(type="ALERTS_LIST", title="Alerts List")
        ],
        [
            dict(type="OVERDUE", title="Overdue Reports"),
            dict(type="UPCOMING", title="Upcoming Reports")
        ],
        [
            dict(type="IMAGE", title="Test Image"),
            dict(type="TEXT", title="Test Text", content="This is the content of the item")
        ],
        [
            dict(type="NOTIFICATIONS", title="Notifications"),
            dict(type="DELETIONS", title="Deletions"),
            dict(type="AMENDMENTS", title="Amendments")
        ],
        [
            dict(type="TASKS", title="Tasks")
        ]
    ]
)

USER_OVERVIEW = dict(
        name="Overview",
        icon="fa-dashboard",
        definition=[
            [
                dict(type="METRICS_OVERALL", title="Overview"),
                dict(type="DOCUMENTS", title="Documents")
            ],
            [
                dict(type="ACTIVITY", title="Activity"),
                dict(type="ASSIGNMENTS", title="My Assignments")
            ],
            [
                dict(type="METRICS", title="Metrics", metrics_items=["FORM_SUBMISSIONS", "ASSIGNMENTS", "REPORTING_LOCATIONS", "PARTNERS"], metric_layout="BAR")
            ]
        ]
)

USER_SURVEILLANCE = dict(
        name="Surveillance",
        icon="fa-line-chart",
        color="#febf32",
        definition=[
            [
                dict(type="OVERDUE", title="Overdue Reports"),
                dict(type="UPCOMING", title="Upcoming Reports")
            ],
            [
                dict(type="AMENDMENTS", title="Pending Amendments"),
                dict(type="DELETIONS", title="Pending Deletions")
            ]
        ]
)

USER_ALERTS = dict(
        name="Alerts",
        icon="fa-bell-o",
        metric="OPEN_ALERTS",
        color="#f18b23",
        definition=[
            [
                dict(type="ALERTS", title="Alerts")
            ]
        ]
)
