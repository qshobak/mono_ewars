EMAILS_PLAIN = dict(
    ASSIGNMENT_REQUEST="""
{{data.requestor}} [{{data.requestor_email}}] has requested a new assignment with the following details:

Report Type: {{data.form_name}}
Location: {{data.location_name_long}}

Please review this request from your tasks and take an appropriate action. 
    """,
    ASSIGNMENT_APPROVED="""
{{data.approver}} [{{data.approver_email}}] has APPROVED the following assignmnet request.

Report Type: {{data.form_name}}
Location: {{data.location_name}}

You can now submit reports for the assignment from your account. 
    """,
    ASSIGNMENT_REJECTED="""
{{data.approver}} [{{data.approver_email}}] has REJECTED the following assignment request:

Report Type: {{data.form_name}}
Location: {{data.location_name}}

They gave the following reason for the rejection:

{{data.reason}} 
    """,
    RETRACTION_REQUEST="""
{{data.deleted}} [{{data.deleter_email}}] has request a report deletion:\n
\n
Report Type: {{data.form_name}}\n
Location: {{data.location_name}}\n
Report Date: {{data.report_date}}\n
Reason: \n
\t{{data.reason}}\n
\n
Please review this request from "Tasks" in your EWARS account and take an appropriate action. 
    """,
    RETRACTION_APPROVED="""
{{data.approver}} [{{data.approver_email}}] has APPROVED your request to delete the following report:\n
\n
\tReport Type: {{data.form_name}}\n
\tLocation: {{data.location_name}}\n
\tReport Date: {{data.report_date}}\n

You do not need to take any other action ast the report is now removed from the system. 
    """,
    RETRACTION_REJECTED="""
{{data.approver}} [{{data.approver_email}}] has REJECTED yoru request to delete the following report:\n
\n
\tReport Type: {{data.form_name}}\n
\tLocation: {{data.location_name}}\n
\tReport Date: {{data.report_date}}\n
\n
They gave the following reason:\n
\t{{data.reason}} 
    """,
    EMAIL_VERIFICATION="""
Thank you for submitting a registration for EWARS. Before we can process with your registration. please follow the link below to verify ownership of this email address.\n
\n
http://ewars.ws/register/verify/{{data.verification_code}}_{{data.tki}}
    """,
    SHARE="""
{{data.sender_name}} ({{data.sender_email}}) has shared a {{data.report_title}} document with you.\n
\n
You can view the document by clicking (or pasting the URL below into your browser) the link below:\n
\n
{{data.uri}}
    """,
    AMENDMENT_REQUEST="""
{{data.requestor}} [{{data.requestor_email}}] has requested and amendment to the following report:

Report Type: {{data.form_name}}
Location: {{data.location_name}}
Report Date: {{data.report_date}}

Reason: {{data.reason}}

""",
    AMENDMENT_APPROVED="""
{{data.approver}} [{{data.approver_email}}] has approved your request to amend the following report:

Report Type: {{data.form_name}}
Location: {{data.location_name}}
Report Date: {{data.report_date}}

You do not need to take any other action as the report is now updated in the system.

""",
    AMENDMENT_REJECTED="""
{{data.approver}} [{{data.approver_email}}] has rejected your request to amend the following report:

Report Type: {{data.form_name}}
Location: {{data.location_name}}
Report Date: {{data.report_date}}

They gave the following reason for the rejection:

{{data.reason}}
""",
    ALERT="""
        A {{data.form_name}} has triggered the following alert:

        Name: {{data.alarm_name}}
        Location: {{data.alert_location}}
        Date: {{data.alert_date}}
        EID: {{data.eid}}

        You can view more information and take actions on the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
    """,
    ALERT_USER="""
        A {{data.form_name}} that you recently submitted has triggered the following alert:

        Name: {{data.alarm_name}}
        Location: {{data.alert_location}}
        Date: {{data.alert_date}}
        EID: {{data.eid}}

        You can view more information and take actions on the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}} 
    """,
    RECOVERY="""
        You recently requested recovery of your password for you {{data.get("email") }} account in EWARS\n
        \n
        Please follow the link below to recover your account and set a new password\n
        \n
        http://ewars.ws/authentication/reset/{{data.get("token")}}\n
    """
)
