EMAILS = {
    'DISCARD': """
        <p><strong>{{data.user_name}}</strong> has <strong>discarded</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.content}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'VERIFICATION': """
        <p><strong>{{data.user_name}}</strong> has completed <Strong>Verification</strong> for the following alert:.</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Triage:</strong><br/>
            {{data.content}}
            <br/>
            <strong>Verification Outcome:</strong> {{data.outcome}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'RISK_CHAR': """
        <p><strong>{{data.user_name}}</strong> has completed <strong>Risk Characterisation</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Risk:</strong> {{data.risk}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'RISK_ASSESS': """
        <p><strong>{{data.user_name}}</strong> has completed <strong>Risk Assessment</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Hazard Assessment:</strong><br/>
            {{data.hazard_assessment}}<br/>
            <Strong>Exposure Assessment:</strong><br/>
            {{data.exposure_assessment}}<br/>
            <strong>Context Assessment</strong><br/>
            {{data.context_assessment}}

        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'REOPEN': """
        <p><strong>{{data.user_name}}</strong> has <strong>re-opened</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.reason}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'OUTCOME': """
       <p><strong>{{data.user_name}}</strong> has completed <strong>Outcome</strong> for the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Outcome: </strong> {{data.outcome}}<br />
            <strong>Comments:</strong><br/>
            {{data.comments}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'AUTOCLOSED': """
        <p>The following alert has been auto-discarded:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
        </p>
        <p>You can re-open this alert from the alert dashboard.</p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    'REPORT_SUBMISSION': """
        <p><strong>{{data.user_name}}</strong> has submitted a <strong>{{data.form_name}}</strong> report for the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
        </p>
        <p>You can re-open this alert from the alert dashboard.</p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """
}

SUBJECTS = {
    "TRIGGERED": "New alert",
    "TRIGGERED_USER": "New alert",
    "DISCARD": "Alert discarded",
    "VERIFICATION": "Alert verified",
    "RISK_CHAR": "Alert risk characterisation completed",
    "RISK_ASSESS": "Alert risk assessment completed",
    "REOPEN": "Alert re-opened",
    'OUTCOME': "Alert outcome completed",
    'AUTOCLOSED': "Alert discarded due to inaction",
    'REPORT_SUBMISSION': "Investigation report submitted to alert"
}

ASSIGNMENT_REQUEST_HTML = """
<p><strong>{{data.requestor}} [{{data.requestor_email}}]</strong> has requested a new assignment with the following details:</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name_long}}<br />
    <strong>Start Date: </strong> {{data.start_date}}<br />
    <strong>End Date: </strong> {{data.end_date}}
</p>

<p>Please review this request from "Tasks" in your dashboard and take an appropriate action.</p>
"""

ASSIGNMENT_REQUEST_PLAIN = """
{{data.requestor}} [{{data.requestor_email}}] has requested a new assignment with the following details:

Report Type: {{data.form_name}}
Location: {{data.location_name_long}}
Start Date: {{data.start_date}}
End Date: {{data.end_date}}

Please review this request from your tasks and take an appropriate action.
"""

ASSIGNMENT_APPROVED_HTML = """
<p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>approved</strong> the following assignment request.</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name}}<br />
    <strong>Start Date: </strong> {{data.start_date}}<br />
    <strong>End Date: </strong> {{data.end_date}}<br />
</p>

<p>You can now submit reports for the assignment from your account.</p>
"""

ASSIGNMENT_APPROVED_PLAIN = """
{{data.approver}} [{{data.approver_email}}] has APPROVED the following assignmnet request.

Report Type: {{data.form_name}}
Location: {{data.location_name}}
Start Date: {{data.start_date}}
End Date: {{data.end_date}}

You can now submit reports for the assignment from your account.
"""

ASSIGNMENT_REJECTED_HTML = """
<p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>rejected</strong> the following assignment request:</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name}}<br />
    <strong>Start Date: </strong> {{data.end_date}}<br />
    <strong>End Date: </strong> {{data.end_date}}
</p>

<p>They gave the following reason for the rejection:</p>

<p>{{data.reason}}</p>
"""

ASSIGNMENT_REJECTED_PLAIN = """
{{data.approver}} [{{data.approver_email}}] has REJECTED the following assignment request:

Report Type: {{data.form_name}}
Location: {{data.location_name}}
Start Date: {{data.start_date}}
End Date: {{data.end_date}}

They gave the following reason for the rejection:

{{data.reason}}
"""
