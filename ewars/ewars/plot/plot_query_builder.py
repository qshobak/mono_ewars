import uuid
import datetime

from ewars.utils.six import string_types

# from ewars.db import get_db_cur
# from ewars.db import get_db_cursor

from ewars.plot.date_parser import _process_date
from .completeness import get_completeness

from psycopg2.extensions import AsIs

TEST_TKI = ''
TEST_X = [
    ['b5737eae-c7f9-4ee2-a688-02b93d4d42b4', 'D', 'RECORD_DATE', {"type": 'DATE', 'interval': 'DAY'}]
]

TEST_Y = [
    ['d9a8712f-9126-41e7-a128-1dd3b6b049f1', 'M', 'RECORDS', dict()],
    ['24775336-a931-49e0-836f-43ff960a2452', 'M', 'FIELD.38.data_entry.cons.cons_tot', dict()]
]

TEST_FILTERS = {
    'b5737eae-c7f9-4ee2-a688-02b93d4d42b4': [
        ["ab3a908f-2a20-4c8d-9521-e0f855eac960", 'D', 'RECORD_DATE', dict(period=('{NOW-30D}', '{NOW}'))]
    ],
    'd9a8712f-9126-41e7-a128-1dd3b6b049f1': [
        ['40af3781-b082-4054-b914-c531c8c7dd0d', 'D', 'FORM', ['EQ:38']]
    ]
}

COMPARATORS = {
    "EQ": "=",
    "NEQ": "!=",
    "GT": ">",
    "LT": "<",
    "GTE": ">=",
    "LTE": "<="
}

STATICS = dict(
    ALARM_STATUS=['ACTIVE', 'INACTIVE'],
    USER_STATUS=['ACTIVE', 'DELETED', 'PENDING'],
    FORM_STATUS=['ACTIVE', 'INACTIVE'],
    ALERT_STAGE=['VERIFICATION', 'RISK_CHAR', 'RISK_ASSESS', 'OUTCOME'],
    ALERT_STAGE_STATE=['PENDING', 'COMPLETED'],
    ALARM_RISK=['SEVERE', 'HIGH', 'MODERATE', 'LOW'],
    ALERT_OUTCOME=['DISCARDED', 'MONITOR', 'RESPONSE'],
    USER_TYPE=['ACCOUNT_ADMIN', 'REGIONAL_ADMIN', 'USER'],
    SOURCES=['SYSTEM', 'ANDROID', 'MOBILE', 'IOS', 'DESKTOP']
)

STATIC_DYNS = dict(
    LOCATION_TYPE=None
)

TABLE_DEFS = {
    'ALARM': 'alarms_v2',
    'ALERT': 'alerts',
    'LOCATION': 'locations',
    'LOCATION_TYPE': 'location_types',
    'RECORD': 'collections',
    'FORM': 'forms',
    'RECORD_DS': 'data_set_data',
    'ORGANIZATION': 'organizations'
}

JOIN_DEFS = {
    ('ALARM', 'USER',): None,
    ('ALARM', 'ALERT',): ('uuid', 'alarm_id'),
    ('ALARM', 'LOCATION',): None,
    ('ALERT', 'LOCATION',): ('location_id', 'uuid'),
    ('ALERT', 'RECORD',): ('trigger_period_end', 'data_date'),
}

PROP_MAP = {
    'FORM': 'form_id',
    'ALARM_STATE': 'state',
    'LOCATION_TYPE': 'site_type_id',
    'LOCATION_GROUP': 'groups',
    'LOCATION_GEOM': 'geometry_type',
    'USER_TYPE': 'role',
    'USER': 'id',
    'ALERT_STATE': 'state',
    'ALERT_STAGE_STATE': 'stage_state',
    'ALERT_OUTCOME': 'outcome',
    'ALERT_RISK': 'risk',
    'LOCATION_STATUS': 'status',
    'RECORD_SUBMISSION_DATE': 'submitted',
    'ORGANIZATION': 'org_id',
    'ALTER_DATE': 'trigger_end'
}


def _parseFilter(filt):
    """ Parse a filter

    Args:
        filt:

    Returns:

    """
    if 'FIELD.' in filt[2]:
        comparator, value = filt[3].split(':')
        local_field = '.'.join(filt[2].split('.')[2:])
        return "data->>'%s' %s '%s'" % (
            local_field,
            COMPARATORS.get(comparator, 'EQ'),
            value,
        )

    if filt[2] == 'FORM':
        comparator, value = filt[3].split(':')
        return "form_id %s %s" % (
            COMPARATORS.get(comparator),
            value,
        )


DEFINITIONS = dict(
    RECORDS=dict(
        table='collection_counts',
        base_selects=('data_date', 'location_id', 'count'),
        joins=dict()
    ),
    FIELD=dict(
        table='collections',
        base_selects=('data_date', 'location_id'),
        joins=dict()
    ),
    RECORD_DATE=dict(
        table=None,
        base_selects=None,
        joins=None
    ),
    LOCATION=dict(
        table='locations',
        base_selects=('uuid', 'site_type_id'),
        joins=dict()
    ),
    FORM=dict(
        table='forms',
        base_selects=('id'),
        joins=dict()
    ),
    LOCATION_TYPE=dict(
        table='location_types',
        base_selects=('id'),
        joins=dict()
    )
)


class Component(object):
    def __init__(self, data, filters):
        self.id = data[0]
        self.type = data[1]
        self.name = data[2]
        self.fitlers = filters


class Dimension(object):
    def __init__(self, data, filters):
        self.id = data.get('_')
        self.type = data.get('t')
        self.name = data.get('n')
        self.conf = data
        self.filters = filters

        # Some basic info
        self.is_field = 'FIELD.' in self.name

        self.is_date = 'DATE' in self.name
        if not self.is_date:
            self._is_date = data.get('_dt', None) == 'DATE'

        self.is_location = self.name == 'LOCATION'  # Is this a location
        self.is_alarm = self.name == 'ALARM'

        # Table information
        self.with_stmt = None
        self.group = None
        self.select = None

        # Date specific fields
        self.date_interval = data.get('interval', 'DAY')
        self.date_period = None
        self.date_start = None
        self.date_end = None

        # Location specific fields
        self.location_type = None
        self.location_parent = None
        self.location_status = None

        # Base aliases
        self.table_alias = get_id()
        self.inner_alias = get_id()
        self.join_alias = get_id()
        self.result_alias = get_id()

        # Unlike measures, we do not parse filters of different types,
        # we only parse a filter if it matches up exactly to the
        # dimension that we're using. For instance, it doesn't make sense
        # to filter a RECORD_DATE dimension by location as there is no location
        # data associated with a RECORD_DATE, if you want to filter by location
        # the the filter needs to exist on the measure OR add a location dimension to
        # dis-aggregate the data producing multiple values, each representing a location

        self._wheres = []

        if len(self.filters) > 0:
            for item in self.filters:
                if not self._is_filt_date(item):
                    appends = []
                    prop = PROP_MAP.get(item.get('n'), item.get('n'))
                    for f_def in item.get('f', []):
                        cmp, value = f_def.split(':')
                        parsed = "{PROP} {CMP} '{VALUE}'".format(**dict(
                            PROP=prop,
                            CMP=COMPARATORS.get(cmp),
                            VALUE=value
                        ))
                        appends.append(parsed)

                    self._wheres.append('(' + ' AND '.join(appends) + ')')

            if len(self._wheres) > 0:
                self._wheres = ' WHERE ' + ' AND '.join(self._wheres)
            else:
                self._wheres = ""

        if self.is_date:
            self._handle_date_type()
        elif self.is_location:
            self._handle_location_type()
        elif self.is_field:
            self._handle_field_type()
        elif self.name in STATICS.keys():
            self._handle_static_type()
        elif self.is_alarm:
            self._handle_alarm_type()
        elif self.name == 'LOCATION_TYPE':
            self._handle_location_type()
        else:
            self._handle_other_type()

    def _is_filt_date(self, item):
        """ Check if a filter is a date type, we handle these explicitly in
        the _handle_date_type method instead of as a global filter

        Args:
            item:

        Returns:

        """

        if 'DATE' in item.get('n'):
            return True

        if item.get('_dt', None) == 'DATE':
            return True

        return False

    def _handle_date_type(self):

        # We're dealing with a date dimension, so we're going
        # to be using generate_series
        for flt in self.filters:
            if flt.get('n') == self.name:
                self.date_period = flt.get('f', [])[0].split(',')

        # Parse out the filtered period for the query
        if self.date_period is not None:
            self.date_end = _process_date(self.date_period[1], datetime.datetime.now(), None)
            self.date_start = _process_date(self.date_period[0], datetime.datetime.now(), self.date_end)

        if self.date_interval == 'WEEK':
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT generate_series(
                        date_trunc('week', '{START_DATE}'::TIMESTAMP) - '1 DAY'::INTERVAL,
                        date_trunc('week', '{END_DATE}'::TIMESTAMP) - '1 DAY'::INTERVAL,
                        '1 week'
                        ) AS {INNER_ALIAS}
                    {WHERES}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                START_DATE=self.date_start,
                END_DATE=self.date_end,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
            ))
        else:
            # We don't build a statement up, we just create it
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT generate_series('{START_DATE}', '{END_DATE}'::TIMESTAMP, '1 {INTERVAL}') AS {INNER_ALIAS}
                    {WHERES}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                START_DATE=self.date_start,
                END_DATE=self.date_end,
                INNER_ALIAS=self.inner_alias,
                INTERVAL=self.date_interval,
                WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
            ))

        self.select = "date_part('epoch', {JOIN_ALIAS}.{INNER_ALIAS})*1000 AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))
        self.group = "date_part('epoch', {JOIN_ALIAS}.{INNER_ALIAS})".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias
        ))
        self.stype = 'DATE'

    def _handle_location_type(self):
        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT 
                    id AS {INNER_ALIAS},
                    name
                FROM __SCHEMA__.location_types
                {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))

        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

        self.group = "{JOIN_ALIAS}.{INNER_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias
        ))
        self.stype = 'LOCATION_TYPE'

    def _handle_field_type(self):

        local_field = '.'.join(self.name[2].split('.')[2:])

        local_filters = []
        for flt in self.filters:
            local_filters.append(_parseFilter(flt))

        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT
                  DISTINCT(data->>'{LOCAL_FIELD}') AS {INNER_ALIAS},
                  data_date,
                  location_id,
                  form_id
              FROM  __SCHEMA__.collections
                {WHERES}
            )
        """.format(**dict(
            LOCAL_FIELD=local_field,
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))

        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

        self.group = "{JOIN_ALIAS}.{INNER_ALIAS}"
        self.stype = 'RECORD_FIELD'

    def _handle_alarm_type(self):
        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT uuid::TEXT AS {INNER_ALIAS}
                FROM __SCHEMA__.alarms_v2
                {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))

        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))
        self.group = "{JOIN_ALIAS}.{INNER_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias
        ))
        self.stype = 'ALARM'

    def _handle_location_type(self):
        """ Location types are complex, in that they may require multiple levels of
        data to be loaded. Ultimately we want the final end level that we're going to
        load

        Returns:

        """

        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT
                    uuid::TEXT AS {INNER_ALIAS},
                    lineage,
                    site_type_id
                FROM __SCHEMA__.locations 
                {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))
        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))
        self.group = "{JOIN_ALIAS}.{INNER_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias
        ))
        self.stype = 'LOCATION'

    def _handle_static_type(self):
        """ Handle a static type dimension, the values are simply
        a static set of pre-defined values, so we just inject them as a table

        Returns:

        """

        self.with_stmt = """
            {TABLE_ALIAS} AS (
              SELECT ARRAY['{VALUES}'] AS {INNER_ALIAS}
              {WHERES}
            )
        """.format(**dict(
            VALUES="','".join(STATICS.get(self.name)),
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))
        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))
        self.group = "{RESULT_ALIAS}".format(**dict(
            RESULT_ALIAS=self.result_alias
        ))

        self.stype = 'STATIC'

    def _handle_other_type(self):
        """ Handle other types of dimensions, usually
        a DISTINCT call

        Returns:

        """
        self.with_stmt = ""
        self.select = ""
        self.group = ""
        self.stype = None

    def _handle_static_dynamic(self):
        """ These are a DISTINCT set of values which are
        loaded from a tertiary table. An example of this would be
        location types loaded from the __SCHEMA__.location_types
        table as a set of ids.

        Returns:

        """

        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT {PROP} AS {INNER_ALIAS}
                FROM __SCHEMA__.{TABLE}
                {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            PROP=None,
            INNER_ALIAS=self.inner_alias,
            TABLE=None,
            WHERES=self._wheres if isinstance(self._wheres, (string_types,)) else ""
        ))
        self.select = "{JOIN_ALIAS}.{INNER_ALIAS} AS {RESULT_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))
        self.group = "{JOIN_ALIAS}.{INNER_ALIAS}".format(**dict(
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias
        ))

    def get_join(self, measure):
        """ Figure out how the specified measure can be joined to the dimension

        If a measure can't be joined to the dimension, do a CROSS JOIN?

        Args:
            measure:

        Returns:

        """

        print(measure.name, self.name)

        join = None

        if self.stype == 'DATE':
            # We're joining on a date setup, this would be a generate_series
            # and is somewhat of a special case as we use the BETWEEN syntax
            trunc_interval = self.date_interval
            if self.date_interval == 'DAY':
                join = """{MEAS_TABLE}.{MEAS_PROP}::DATE = {DIM_TABLE}.{DIM_PROP}::DATE""".format(**dict(
                    MEAS_TABLE=measure.join_alias,
                    MEAS_PROP=measure.get_date_prop(),
                    DIM_TABLE=self.join_alias,
                    DIM_PROP=self.inner_alias
                ))
            elif self.date_interval == 'WEEK':
                join = "({MEAS_TABLE}.{MEAS_PROP} BETWEEN date_trunc('week', {DIM_TABLE}.{DIM_PROP}) AND {DIM_TABLE}.{DIM_PROP})".format(
                    **dict(
                        MEAS_TABLE=measure.join_alias,
                        MEAS_PROP=measure.get_date_prop(),
                        DIM_TABLE=self.join_alias,
                        DIM_PROP=self.inner_alias
                    ))
        elif self.stype == 'LOCATION':
            join = "{DIM_TABLE}.{DIM_PROP} = ANY({MEAS_TABLE}.{MEAS_PROP}::TEXT[])".format(**dict(
                MEAS_TABLE=measure.join_alias,
                MEAS_PROP='lineage',
                DIM_TABLE=self.join_alias,
                DIM_PROP=self.inner_alias
            ))
        elif self.stype == 'ALARM':
            join = "{DIM_TABLE}.{DIM_PROP} = {MEAS_TABLE}.alarm_id".format(**dict(
                DIM_TABLE=self.join_alias,
                DIM_PROP=self.inner_alias,
                MEAS_TABLE=measure.join_alias
            ))
        elif self.stype == 'LOCATION_TYPE':
            join = "{DIM_TABLE}.{DIM_PROP} = {MEAS_TABLE}.site_type_id".format(**dict(
                DIM_TABLE=self.join_alias,
                DIM_PROP=self.inner_alias,
                MEAS_TABLE=measure.join_alias
            ))
        else:
            join = "{MEAS_TABLE}.{MEAS_PROP} = {DIM_TABLE}.{DIM_PROP}".format(**dict(
                MEAS_TABLE=measure.join_alias,
                MEAS_PROP=measure.get_dim_prop(self.id),
                DIM_TABLE=self.join_alias,
                DIM_PROP=self.result_alias
            ))

        return join


# Measures where the select for data is a SELECT 1 AS {}
MEASURE_SELECT_ONE = [
    'ORGANIZATIONS',
    'FORMS',
    'DEVICES',
    'USERS',
    'LOCATIONS',
    'LOCATION_TYPES',
    'ALARMS'
]


def get_id():
    id = str(uuid.uuid4()).split('-')
    id = id[:1][0]

    return '_%s' % (id)


class Measure(object):
    def __init__(self, data, filters, dimensions):
        self.id = data.get('_')
        self.type = data.get('t')
        self.name = data.get('n')
        self.conf = data
        self.filters = filters or []
        self.dimensions = dimensions

        self.is_field = 'FIELD.' in self.name
        self.is_complex = False

        self.agg = data.get('agg', 'SUM')

        # Extraction details
        self.with_stmt = None
        self.main_select = None
        self.group = None

        # Source details
        self.inner_alias = get_id()
        self.table_alias = get_id()
        self.join_alias = get_id()
        self.result_alias = get_id()

        self.joins = []

        self._selects = []
        self._wheres = []

        for dim in self.dimensions:
            if dim.stype == 'STATIC':
                pass
            elif dim.stype == 'FORM_FIELD':
                pass
            elif dim.stype == 'DATE':
                pass
            elif dim.stype == 'LOCATION':
                pass
            elif dim.stype == 'FORM_CORE':
                pass
            else:
                pass

        self.value_type = 'NUM'

        self._wheres = []
        self._joins = []

        self._can_use_aggs = True

        if len(self.dimensions) <= 0:
            self.join_alias = self.table_alias

        # TODO: May need to join
        for flt in self.filters:
            flt_group = []
            if 'FIELD.' in flt.get('n'):
                self._can_use_aggs = False
                # We're filtering on a field value
                for item in flt.get('f'):
                    cmp, value = item.split(':')
                    parse = "c.data->>'{FIELD}' {CMP} '{VALUE}'".format(**dict(
                        FIELD='.'.join(flt.get('n').split('.')[2:]),
                        CMP=COMPARATORS.get(cmp),
                        VALUE=value
                    ))
                    flt_group.append(parse)
            elif flt.get('n') == 'RECORD_DATE':
                period = flt.get('f', [])[0].split(',')
                if period is None:
                    period = ['{NOW}-30D', '{NOW}']
                date_end = _process_date(period[1], datetime.datetime.now(), None)
                date_start = _process_date(period[0], datetime.datetime.now(), date_end)
                flt_group.append("c.data_date >= '{VALUE}'".format(**dict(
                    VALUE=date_start.format('%Y-%m-%d')
                )))
                flt_group.append("c.data_date <= '{VALUE}'".format(**dict(
                    VALUE=date_end.format('%Y-%m-%d')
                )))
            elif flt.get('n') == 'ALERT_DATE':
                period = flt.get('f', [])[0].split(',')
                if period is None:
                    period = ['{NOW}-30D', '{NOW}']
                date_end = _process_date(period[1], datetime.datetime.now(), None)
                date_start = _process_date(period[0], datetime.datetime.now(), date_end)
                flt_group.append("c.trigger_end >= '{VALUE}'".format(**dict(
                    VALUE=date_start.format('%Y-%m-%d')
                )))
                flt_group.append("c.trigger_end <= '{VALUE}'".format(**dict(
                    VALUE=date_end.format('%Y-%m-%d')
                )))
            elif flt.get('n') == 'DATE_SUB':
                self._can_use_aggs = False
                period = flt.get('f', [])[0].split(',')
                if period is None:
                    period = ['{NOW}-30D', '{NOW}']
                date_end = _process_date(period[1], datetime.datetime.now(), None)
                date_start = _process_date(period[0], datetime.datetime.now(), date_end)
                flt_group.append("c.submitted >= '{VALUE}'".format(**dict(
                    VALUE=date_start.format('%Y-%m-%d')
                )))
                flt_group.append("c.submitted <= '{VALUE}'".format(**dict(
                    VALUE=date_end.format('%Y-%m-%d')
                )))
            else:
                prop = flt.get('n')
                if len(flt.get('f', [])) > 0:
                    for item in flt.get('f', []):
                        cmp, value = item.split(':')
                        parsed = "c.{PROP} {CMP} '{VALUE}'".format(**dict(
                            PROP=PROP_MAP.get(prop),
                            CMP=COMPARATORS.get(cmp),
                            VALUE=value
                        ))
                        flt_group.append(parsed)

            if len(flt_group) > 0:
                end_sql = '( %s )' % (' AND '.join(flt_group))
                self._wheres.append(end_sql)

        # Create the filter segment for the sql
        if len(self._wheres) > 0:
            self._wheres = 'WHERE ' + ' AND '.join(self._wheres)
        else:
            self._wheres = ''

        if self.is_field:
            self._handle_field_type()
        elif self.name == 'RECORDS':
            self._handle_record_count()
        elif self.name in MEASURE_SELECT_ONE:
            self._handle_select_type()
        elif self.name == 'ALERTS':
            self._handle_alert_type()
        elif self.name == 'COMPLETENESS':
            self._handle_completeness()
        elif self.name == 'RECORDS_EXPECTED':
            self._handle_expected()
        elif self.name == 'RECORDS_TIMELY':
            self._handle_records_timely()
        elif self.name == 'TIMELINESS':
            self._handle_timeliness()

    def get_calc_select(self):
        """ When used as part of a calculation
        returns a select statement that works with the
        formula in question

        Returns:

        """
        pass

    def _handle_records_timely(self):
        form_id = self.conf.get('fid')
        interval = self.conf.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        location_type = self.conf.get('features', {}).get('LOCATION_REPORTING', {}).get('site_type_id', None)

        overdue = self.conf.get('features', {}).get('OVERDUE', None)

        if overdue is not None:
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT 
                        c.data_date::DATE,
                        l.uuid::TEXT,
                        l.site_type_id,
                        (CASE WHEN c.data_date <= (c.data_date + '{UNITS} {INTERVAL}'::INTERVAL) THEN 1 ELSE 0 END) AS {INNER_ALIAS}
                    FROM __SCHEMA__.collections AS c
                        LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                    WHERE c.form_id = {FORM_ID}
                        AND l.status = 'ACTIVE'
                        AND l.site_type_id = {LOCATION_TYPE}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                UNITS=overdue.get('threshold'),
                INTERVAL=overdue.get('interval'),
                FORM_ID=form_id,
                LOCATION_TYPE=location_type
            ))

            self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
                AGG=self.agg,
                JOIN_ALIAS=self.join_alias,
                INNER_ALIAS=self.inner_alias,
                RESULT_ALIAS=self.result_alias
            ))

    def _handle_timeliness(self):

        form_id = self.conf.get('fid')
        interval = self.conf.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        location_type = self.conf.get('features', {}).get('LOCATION_REPORTING', {}).get('site_type_id', None)

        overdue = self.conf.get('features', {}).get('OVERDUE', None)

        if overdue is not None:
            self.with_stmt = """
                 {TABLE_ALIAS} AS (
                    SELECT
                        gen_date::DATE,
                        rp.location_id::TEXT,
                        l.lineage::TEXT[] AS lineage,
                        l.site_type_id,
                        (CASE WHEN c.data_date <= (c.data_date + '{UNITS} {INTERVAL}'::INTERVAL) THEN 1 ELSE 0 END) AS on_time,
                        (CASE WHEN rp.uuid IS NOT NULL THEN 1 ELSE 0 END) AS expected
                    FROM generate_series(date_trunc('week', '2001-01-01'::TIMESTAMP) - '1 day'::INTERVAL, date_trunc('week', NOW()) - '1 day'::INTERVAL, '1 week') AS gen_date
                        LEFT JOIN __SCHEMA__.location_reporting AS rp ON gen_date BETWEEN date_trunc('week', rp.start_date) AND date_trunc('week', COALESCE(rp.end_date, NOW()::DATE)) + '7 days'::INTERVAL AND rp.form_id = {FORM_ID}
                        LEFT OUTER JOIN __SCHEMA__.collections AS c ON c.data_date = gen_date AND c.form_id = rp.form_id AND c.status = 'SUBMITTED' AND c.location_id = rp.location_id
                        LEFT OUTER JOIN __SCHEMA__.locations AS l ON l.uuid = rp.location_id AND l.status = 'ACTIVE'
                    WHERE rp.location_id IS NOT NULL
                        AND l.site_type_id = {LOCATION_TYPE}
                 )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                FORM_ID=form_id,
                LOCATION_TYPE=location_type,
                UNITS=overdue.get('threshold'),
                INTERVAL=overdue.get('interval')
            ))

            self.select = "({AGG}({JOIN_ALIAS}.on_time) / NULLIF({AGG}({JOIN_ALIAS}.expected), 0)) AS {RESULT_ALIAS}".format(
                **dict(
                    AGG=self.agg,
                    JOIN_ALIAS=self.join_alias,
                    RESULT_ALIAS=self.result_alias
                ))

    def _handle_records_late(self):
        form_id = self.conf.get('fid')
        interval = self.conf.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        location_type = self.conf.get('features', {}).get('LOCATION_REPORTING', {}).get('site_type_id', None)

        overdue = self.conf.get('features', {}).get('OVERDUE', None)

        if overdue is not None:
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT 
                        c.data_date,
                        l.uuid::TEXT,
                        l.site_type_id,
                        l.lineage::TEXT[] AS lineage,
                        l.site_type_id,
                        (CASE WHEN c.data_date > (c.data_date + '{UNITS} {INTERVAL}'::INTERVAL) THEN 1 ELSE 0 END) AS {INNER_ALIAS}
                    FROM __SCHEMA__.collections AS c
                        LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                    WHERE c.form_id = {FORM_ID}
                        AND l.status = 'ACTIVE'
                        AND l.site_type_id = {LOCATION_TYPE}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                UNITS=overdue.get('threshold'),
                INTERVAL=overdue.get('interval'),
                FORM_ID=form_id,
                LOCATION_TYPE=location_type
            ))

            self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
                AGG=self.agg,
                JOIN_ALIAS=self.join_alias,
                INNER_ALIAS=self.inner_alias,
                RESULT_ALIAS=self.result_alias
            ))

    def _handle_completeness(self):
        self.main_select = ""

        form_id = self.conf.get('fid')
        interval = self.conf.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        location_type = self.conf.get('features', {}).get('LOCATION_REPORTING', {}).get('site_type_id', None)

        if interval == 'WEEK':
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT
                        gen_date::DATE,
                        rp.location_id,
                        l.site_type_id,
                        l.lineage::TEXT[] AS lineage,
                        (CASE WHEN rp.uuid IS NOT NULL THEN 1 ELSE 0 END) AS expected,
                        (CASE WHEN c.uuid IS NOT NULL THEN 1 ELSE 0 END) AS received
                    FROM generate_series(date_trunc('week', '2000-01-01'::TIMESTAMP) - '1 day'::INTERVAL, date_trunc('week', NOW()::TIMESTAMP) - '1 day'::INTERVAL, '1 week') AS gen_date
                        LEFT JOIN __SCHEMA__.location_reporting AS rp ON gen_date BETWEEN date_trunc('week', rp.start_date) AND date_trunc('week', COALESCE(rp.end_date, NOW()::DATE)) + '7 days'::INTERVAL AND rp.form_id = {FORM_ID}
                        LEFT OUTER JOIN __SCHEMA__.collections AS c ON c.data_date = gen_date AND c.form_id = rp.form_id AND c.status = 'SUBMITTED' AND c.location_id = rp.location_id
                        LEFT OUTER JOIN __SCHEMA__.locations AS l ON l.uuid = rp.location_id AND l.status = 'ACTIVE'
                    WHERE rp.location_id IS NOT NULL
                        AND l.site_type_id = {LOCATION_TYPE}
                )
            """.format(**dict(
                FORM_ID=form_id,
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres,
                INTERVAL=interval.lower(),
                LOCATION_TYPE=location_type
            ))
        elif interval == 'DAY':
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT
                        gen_date::DATE,
                        rp.location_id,
                        l.site_type_id,
                        (CASE WHEN rp.uuid IS NOT NULL THEN 1 ELSE 0 END) AS expected,
                        (CASE WHEN c.uuid IS NOT NULL THEN 1 ELSE 0 END) AS received
                    FROM generate_series('2000-01-01'::TIMESTAMP, NOW()::TIMESTAMP, '1 day') AS gen_date
                        LEFT JOIN __SCHEMA__.location_reporting AS rp ON gen_date BETWEEN rp.start_date AND COALESCE(rp.end_date, NOW()::DATE) AND rp.form_id = {FORM_ID}
                        LEFT OUTER JOIN __SCHEMA__.collections AS c ON c.data_date = gen_date AND c.form_id = rp.form_id AND c.status = 'SUBMITTED' AND c.location_id = rp.location_id
                        LEFT OUTER JOIN __SCHEMA__.locations AS l ON l.uuid = rp.location_id AND l.status = 'ACTIVE'
                    WHERE rp.location_id IS NOT NULL
                        AND l.site_type_id = {LOCATION_TYPE}
                )
            """.format(**dict(
                FORM_ID=form_id,
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres,
                INTERVAL=interval.lower(),
                LOCATION_TYPE=location_type
            ))

        self.select = "(CAST({AGG}({JOIN_ALIAS}.received) AS DECIMAL) / NULLIF({AGG}({JOIN_ALIAS}.expected), 0)) AS {RESULT_ALIAS}".format(
            **dict(
                AGG=self.agg,
                JOIN_ALIAS=self.join_alias,
                RESULT_ALIAS=self.result_alias
            ))

    def _handle_expected(self):

        form_id = self.conf.get('fid')
        interval = self.conf.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        location_type = self.conf.get('features', {}).get('LOCATION_REPORTING', {}).get('site_type_id', None)

        loc_get = ""
        loc_join = ""
        loc_spec = ""
        if location_type is not None:
            loc_get = "l.site_type_id,"
            loc_join = "LEFT OUTER JOIN __SCHEMA__.locations AS l ON l.uuid = rp.location_id AND l.status = 'ACTIVE'"
            loc_spec = "AND l.site_type_id = {LOCATION_TYPE}".format(**dict(
                LOCATION_TYPE=location_type
            ))

        if interval == 'WEEK':
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT
                        gen_date::DATE,
                        rp.location_id,
                        {LOC_GET}
                        (CASE WHEN rp.uuid IS NOT NULL THEN 1 ELSE 0 END) as {INNER_ALIAS}
                    FROM generate_series(date_trunc('week', '2001-01-01'::TIMESTAMP) - '1 day'::INTERVAL, date_trunc('week', NOW()::DATE) - '1 day'::INTERVAL, '1 week') AS gen_date
                        LEFT JOIN __SCHEMA__.location_reporting AS rp ON gen_date BETWEEN date_trunc('week', rp.start_date) AND date_trunc('week', COALESCE(rp.end_date, NOW()::DATE)) + '7 days'::INTERVAL AND rp.form_id = {FORM_ID}
                        {LOC_JOIN}
                    WHERE rp.location_id IS NOT NULL
                        {LOC_SPEC}
                )
            """.format(**dict(
                FORM_ID=form_id,
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                INTERVAL=interval.lower(),
                LOCATION_TYPE=location_type,
                LOC_GET=loc_get,
                LOC_JOIN=loc_join,
                LOC_SPEC=loc_spec
            ))
        elif interval == 'DAY':
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT
                        gen_date::DATE,
                        rp.location_id,
                        {LOC_GET}
                        (CASE WHEN rp.uuid IS NOT NULL THEN 1 ELSE 0 END) AS expected,
                        (CASE WHEN c.uuid IS NOT NULL THEN 1 ELSE 0 END) AS received
                    FROM generate_series('2000-01-01'::TIMESTAMP, NOW()::TIMESTAMP, '1 day') AS gen_date
                        LEFT JOIN __SCHEMA__.location_reporting AS rp ON gen_date BETWEEN rp.start_date AND COALESCE(rp.end_date, NOW()::DATE) AND rp.form_id = {FORM_ID}
                        LEFT OUTER JOIN __SCHEMA__.collections AS c ON c.data_date = gen_date AND c.form_id = rp.form_id AND c.status = 'SUBMITTED' AND c.location_id = rp.location_id
                        {LOC_JOIN}
                    WHERE rp.location_id IS NOT NULL
                        {LOC_SPEC}
                )
            """.format(**dict(
                FORM_ID=form_id,
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres,
                INTERVAL=interval.lower(),
                LOCATION_TYPE=location_type,
                LOC_GET=loc_get,
                LOC_JOIN=loc_join,
                LOC_SPEC=loc_spec
            ))

        self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

    def _handle_field_type(self):
        """ Handle a measure where the result is an aggregation of
        a value from a record

        Returns:

        """

        # Handle field source types
        self.main_select = "{AGG}({INNER_ALIAS}) AS {RESULT_ALIAS}"
        self.field = '.'.join(self.name.split('.')[2:])
        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT
                  c.data_date,
                  c.location_id::TEXT,
                  c.form_id,
                  COALESCE(CAST(c.data->>'{FIELD}' AS NUMERIC), 0) AS {INNER_ALIAS},
                  l.lineage::TEXT AS lineage
                FROM __SCHEMA__.collections AS c
                    LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            FIELD=self.field,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres
        ))

        self.select = "{AGG}({JOIN_ALIAS}.{FIELD_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            JOIN_ALIAS=self.join_alias,
            FIELD_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

        self.non_join_select = "{AGG}({TABLE_ALIAS}.{FIELD_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            TABLE_ALIAS=self.table_alias,
            FIELD_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

    def _handle_record_count(self):
        """ Perform a count of records which meet some criteria

        Returns:

        """

        if self._can_use_aggs is True:
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT 
                        c.count AS {INNER_ALIAS},
                        c.data_date,
                        l.uuid::TEXT as location_id,
                        c.form_id,
                        l.lineage::TEXT[] AS lineage,
                        l.site_type_id AS site_type_id
                    FROM __SCHEMA__.collection_counts AS c 
                        LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                    {WHERES}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres
            ))
        else:
            self.with_stmt = """
                {TABLE_ALIAS} AS (
                    SELECT
                      1 AS {INNER_ALIAS},
                      c.uuid::TEXT,
                      c.data_date,
                      c.form_id,
                      c.location_id::TEXT,
                      l.lineage::TEXT[] AS lineage,
                      l.site_type_id AS site_type_id
                    FROM __SCHEMA__.collections AS c
                        LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                    {WHERES}
                )
            """.format(**dict(
                TABLE_ALIAS=self.table_alias,
                INNER_ALIAS=self.inner_alias,
                WHERES=self._wheres
            ))

        self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))


    def _handle_select_type(self):
        """ Handle types where the resultant values is simply a select
        and filter, e.g. number of users who are ACCOUNT_ADMINS

        Returns:

        """

        self.with_stmt = """
            {TABLE_ALIAS} AS (
              SELECT 1 AS {INNER_ALIAS}
              FROM __SCHEMA__.{TABLE}
              {WHERES}
            )
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres
        ))


        self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias,
            WHERES=' AND '.join(self._wheres)
        ))

        self.non_join_select = "{AGG}({TABLE_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

    def _handle_alert_type(self):
        self.with_stmt = """
            {TABLE_ALIAS} AS (
                SELECT 1 AS {INNER_ALIAS},
                    c.trigger_end::DATE as trigger_end,
                    c.location_id::TEXT as location_id,
                    c.created::DATE as created,
                    c.alarm_id::TEXT AS alarm_id,
                    l.lineage::TEXT[] AS lineage,
                    l.site_type_id AS site_type_id
                FROM __SCHEMA__.alerts AS c
                    LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                {WHERES}
            ) 
        """.format(**dict(
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            WHERES=self._wheres
        ))

        self.select = "{AGG}({JOIN_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            JOIN_ALIAS=self.join_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

        self.non_join_select = "{AGG}({TABLE_ALIAS}.{INNER_ALIAS}) AS {RESULT_ALIAS}".format(**dict(
            AGG=self.agg,
            TABLE_ALIAS=self.table_alias,
            INNER_ALIAS=self.inner_alias,
            RESULT_ALIAS=self.result_alias
        ))

    def _handle_complex_type(self):
        pass

    def get_date_prop(self):
        """ Get the date prop for the WITH statement,
        this is used to join a measure against this dimension
        if it's a query

        Returns:

        """

        if 'FIELD.' in self.name:
            return 'data_date'

        if 'RECORDS' == self.name:
            return 'data_date'

        if 'ALERTS' == self.name:
            return 'trigger_end'

        if self.name in ('COMPLETENESS', 'TIMELINESS', 'RECORDS_EXPECTED'):
            return 'gen_date'

        if self.name in ('RECORDS_TIMELY', 'RECORDS_LATE'):
            return 'data_date'

    def get_location_prop(self):
        """ Get the property that should be joined on from
        a WITH statement for a location join

        Returns:

        """
        if 'FIELD.' in self.name:
            return 'location_id'
        elif 'RECORDS' == self.name:
            return 'location_id'
        elif self.name in ('COMPLETENESS', 'TIMELINESS'):
            return 'location_id'


class Complex(object):
    def __init__(self, data):
        self.id = data[0]
        self.type = data[1]
        self.conf = data[2]

    def get_select(self):
        pass


class Property(object):
    """ Represents a loadable property, for instance a name, location type
    that shouldn't be used in aggregation or grouping.

    """

    def __init__(self, with_stmt, type='COLLECTION'):
        self.with_stmt = with_stmt
        self.astype = type


class CalcSubGroup(object):
    """ Represents a sub-group query for populating
    a calculation

    """

    def __init__(self, with_stmt, type='COLLECTION', table_alias=None, join_alias=None):
        self.name = None

        self.with_stmt = with_stmt
        self.astype = type

        self.table_alias = table_alias
        self.join_alias = join_alias

    def get_date_prop(self):
        if self.astype == 'COLLECTION':
            return 'data_date'

        if self.astype == 'EXPECTED':
            return 'gen_date'


class Calculation(object):
    """ Represents a calculation in the query

    """

    def __init__(self, calc, variables, filters):
        self.id = calc.get('_')
        self.calc = calc
        self.variables = variables
        self.filters = filters
        self.name = calc.get('fx', 'calulation')

        self.formula = calc.get('formula', '')

        self.table_alias = str(uuid.uuid4()).split('-')[:1]
        self.result_alias = get_id()

        self.withs = []
        self.formula = calc.get('fx', '')
        self.wheres = []
        self.groups = []
        self.join_alias = None
        self.result_aliases = {}

        self.vars = {}
        self.params = {}

        self.sub_groups = []

        self.vars = {}

        for var in self.variables:
            if var.get('t') == 'C':
                self._handle_sub_calc(var)
            else:
                self._handle_default(var)

        self._handle_calc_def()

    def _handle_default(self, var):
        """ Calculation uses measure class to get a value for
        a specific item out of it.

        Args:
            var:

        Returns:

        """
        meas = Measure(var, self.filters.get(var.get('_', [])), [])

        self.params[var.get('var')] = meas

    def _handle_sub_calc(self, var):

        pass

    def _handle_fields(self):
        """ Create a with and select statements grouped together from the
        collections table,

        Returns:

        """
        _queries = []
        fids = []
        table_alias = get_id()
        join_alias = get_id()
        # TODO: Optimize so that the same data isn't requested twice to fill a variable
        for key, val in self.vars.items():
            if 'FIELD.' in key:
                fids.append(key.split('.')[1])
                for item in val:
                    var_id = get_id()
                    self.params[item.get('var')] = (table_alias, join_alias, var_id,)
                    _queries.append("COALESCE(CAST(c.data->>'{FIELD}' AS NUMERIC), 0) AS {INNER_ALIAS}".format(**dict(
                        FIELD='.'.join(item.get('n').split('.')[2:]),
                        INNER_ALIAS=var_id
                    )))

        if len(_queries) > 0:
            wheres = []

            with_stmt = """
                {TABLE_ALIAS} AS (
                  SELECT
                    c.data_date,
                    c.location_id,
                    l.lineage AS lineage,
                    l.site_type_id AS site_type_id,
                    {SELECTS}
                  FROM __SCHEMA__.collections AS c
                      LEFT JOIN __SCHEMA__.locations AS l ON l.uuid = c.location_id
                  WHERE c.form_id = ANY(ARRAY[{FORM_IDS}])
                      AND c.status = 'SUBMITTED'
                      AND l.status = 'ACTIVE'
                )
            """.format(**dict(
                TABLE_ALIAS=table_alias,
                FORM_IDS=','.join(fids),
                SELECTS=', '.join(_queries),
            ))

            self.sub_groups.append(CalcSubGroup(
                with_stmt,
                type='COLLECTION',
                table_alias=table_alias,
                join_alias=join_alias
            ))

            self.withs.append(with_stmt)

    def _handle_other(self):
        pass

    def _handle_calc_def(self):
        """ Build up the formula for the calculation

        Iterate over each of variables and replace into the formula
        a reference to the data and an agg function for that given value

        Returns:

        """
        outer_agg = self.calc.get('agg', 'SUM')
        fx = self.formula
        for var in self.variables:
            var_name = var.get('var')
            var_meas = self.params.get(var_name)
            inner_agg = var.get('agg', 'SUM')
            fx = fx.replace(var_name, """SUM(COALESCE({TABLE_ALIAS}.{VAR_ALIAS}, 0))""".format(**dict(
                AGG=inner_agg,
                TABLE_ALIAS=var_meas.join_alias,
                VAR_ALIAS=var_meas.inner_alias,
            )))

        self.select = """
            ({FORMULA}) AS {RESULT_ALIAS}
        """.format(**dict(
            AGG=outer_agg,
            FORMULA=fx,
            RESULT_ALIAS=self.result_alias,
        ))

    def _get_calc_def(self):
        pass

    def get_prim_table_alias(self):
        return self.sub_groups[0].table_alias

    def get_prim_join_alias(self):
        return self.sub_groups[0].join_alias


SYSTEM_MEASURES = [
    'COMPLETENESS',
    'TIMELINESS',
    'EXPECTED_RECORDS',
    'ON_TIME_RECORDS',
    'LATE_RECORDS'
]


class QueryBuilder(object):
    def __init__(self, filters, tki=None):
        self.filters = filters
        self.tki = tki

        # Query components
        self.withs = []
        self.selects = []
        self.orders = []
        self.primary = None
        self.joins = []
        self.groups = []

        # Store as added to the query
        self.dimensions = []
        self.measures = []
        self.calculations = []
        self.properties = []

    def add_dimension(self, dimension):
        """ Adds a dimension to the query

        Args:
            dimension:

        Returns:

        """
        filters = self.filters.get(dimension.get('_'), [])
        self.dimensions.append(Dimension(dimension, filters))

    def add_measure(self, measure):
        """ Adds a measure to the query

        Args:
            measure:

        Returns:

        """
        filters = self.filters.get(measure.get('_'), [])
        self.measures.append(Measure(measure, filters, self.dimensions))

    def add_property(self, prop_def):
        self.properties.append(Property(prop_def))

    def add_calculation(self, calc, variables, filterables):
        self.calculations.append(Calculation(calc, variables, self.filters))

    # TODO: Add support for calculated measures
    # def add_calculated(self, calculated):
    #     filters = self.filters.get(calculated[0], [])
    #     self.calculated.append(Calculated(calculated, filters))

    def has_locations(self):
        """ Check if the query involves locations

        Returns:

        """
        return False

    def get_joins(self):
        """ Process the joins for the query

        Returns:

        """
        # Process dimensions first

        dim_joins = []
        meas_joins = []

        # Skip the primary dimension as its already been handled
        # TODO: If there are no dimensions, don't join
        for dim in self.dimensions[1:]:
            join = " CROSS JOIN {TABLE_ALIAS} AS {JOIN_ALIAS} ".format(**dict(
                TABLE_ALIAS=dim.table_alias,
                JOIN_ALIAS=dim.join_alias
            ))
            dim_joins.append(join)

        # For each measure, it needs to be joined to all dimensions

        measures = self.measures
        join_type = " LEFT JOIN "
        if len(self.dimensions) <= 0:
            join_type = " FULL OUTER JOIN "
            measures = self.measures[1:]

        for measure in measures:
            primary = " {JOIN_TYPE} {TABLE_ALIAS} AS {JOIN_ALIAS} ON ".format(**dict(
                JOIN_TYPE=join_type,
                TABLE_ALIAS=measure.table_alias,
                JOIN_ALIAS=measure.join_alias
            ))
            if len(self.dimensions) <= 0 and len(self.measures) <= 1:
                primary = primary.replace('ON', '')

            prim_joins = []
            for dim in self.dimensions:
                join_def = dim.get_join(measure)
                prim_joins.append(join_def)

            if len(self.dimensions) <= 0 and len(self.measures) > 1:
                # no dimensions so we need to join the reports on uuid as a convenience
                # for avoiding cross join
                for meas in self.measures:
                    if meas.id != measure.id:
                        prim_joins.append("""{TABLE_ALIAS}.uuid = {FOREIGN_ALIAS}.uuid""".format(**dict(
                            TABLE_ALIAS=measure.join_alias,
                            FOREIGN_ALIAS=meas.join_alias
                        )))

            primary = primary + " " + " AND ".join(prim_joins)
            meas_joins.append(primary)

        isFirst = True
        sub_calcs = []
        for calc in self.calculations:
            sub_calcs = sub_calcs + list(calc.params.values())

        # If there are no dimensions and measures, lop off the first
        # sub-group as it will be forming the primary FROM clause in the query
        if len(self.dimensions) <= 0:
            join_type = "FULL OUTER JOIN "

        if len(self.dimensions) <= 0 and len(self.measures) <= 0:
            sub_calcs = sub_calcs[1:]

        for calc in sub_calcs:
            primary = " {JOIN_TYPE} {TABLE_ALIAS} AS {JOIN_ALIAS} ON ".format(**dict(
                JOIN_TYPE=join_type,
                TABLE_ALIAS=calc.table_alias,
                JOIN_ALIAS=calc.join_alias,
            ))

            if len(self.dimensions) <= 0:
                primary = primary.replace('ON', '')

            prim_joins = []
            for dim in self.dimensions:
                join_def = dim.get_join(calc)
                prim_joins.append(join_def)

            primary = primary + " " + " AND ".join(prim_joins)
            meas_joins.append(primary)

        return dim_joins, meas_joins

    def get_primary(self):
        """ Get the primary FROM clause for the query

        Returns:

        """
        # If we have dimensions use the first dimension
        if len(self.dimensions) > 0:
            primary = "{TABLE} AS {ALIAS} ".format(**dict(
                TABLE=self.dimensions[0].table_alias,
                ALIAS=self.dimensions[0].join_alias,
            ))

        # if we don't have dimensions, use the first measure
        else:
            if len(self.measures) > 0:
                primary = "{TABLE} AS {ALIAS} ".format(**dict(
                    TABLE=self.measures[0].table_alias,
                    ALIAS=self.measures[0].join_alias,
                ))

            else:
                if len(self.calculations) > 0:
                    primary = "{TABLE} AS {ALIAS} ".format(**dict(
                        TABLE=self.calculations[0].get_prim_table_alias(),
                        ALIAS=self.calculations[0].get_prim_join_alias()
                    ))

        return primary

    def collect(self):
        """ Collect together the different parts of the query
        and return a formed SQL query to get a resul

        """

        sql = ""

        # Collect with statements
        withs_d = [d.with_stmt.strip() for d in self.dimensions]
        withs_m = [m.with_stmt.strip() for m in self.measures]

        # Calculations can have multiple withs
        withs_c = []
        for calc in self.calculations:
            for _, sub_calc in calc.params.items():
                withs_c.append(sub_calc.with_stmt.strip())

        withs = ', '.join(withs_d + withs_m + withs_c)
        sql += 'WITH ' + withs

        # Collect selects
        if len(self.dimensions) > 0:
            selects_d = [d.select.strip() for d in self.dimensions]
            selects_m = [m.select.strip() for m in self.measures]
            selects_c = [c.select.strip() for c in self.calculations]


            selects = ', \n'.join(selects_d + selects_m + selects_c)
        else:
            selects_m = []
            selects_c = []
            for meas in self.measures:
                sel = meas.select.strip()
                sel = sel.replace(meas.result_alias, '')
                sel = sel.replace('AS', '')
                sel = '(SELECT %s FROM %s) AS %s' % (
                    sel,
                    meas.table_alias,
                    meas.result_alias,
                )
                selects_m.append(sel)

            for calc in self.calculations:
                sel = calc.select.strip()
                sel = sel.replace(calc.result_alias, '')
                sel = sel.replace('AS', '')
                sel = '(SELECT %s FROM %s) AS %s' % (
                    sel,
                    calc.table_alias,
                    calc.result_alias,
                )
                selects_c.append(sel)

            selects = ', \n'.join(selects_m + selects_c)



        sql += 'SELECT \n' + selects

        if len(self.dimensions) > 0:
            # Collect primary FROM and joins
            primary = self.get_primary()
            joins = self.get_joins()

            sql += '\nFROM %s' % (primary.strip())

            for join in joins[0]:
                sql += ' \n%s' % (join.strip())

            for join in joins[1]:
                sql += ' \n%s' % (join.strip())

        groups = [d.group.strip() for d in self.dimensions]
        if len(groups) > 0:
            sql += ' \nGROUP BY \n' + '\n, '.join(groups)

        return sql
