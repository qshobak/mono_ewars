import datetime
import calendar

from ewars.utils.isoweek import Week
from .constants import CODES, MARKERS_INTERVAL, MARKERS_OPERATOR


def _parseCode(code, base_date, end_date):
    """ Parse the EWARS standard for specifying a date range

    Args:
        code:
        base_date:
        end_date:

    Returns:

    """
    today = datetime.datetime.utcnow()
    if code == "{NOW}":
        return datetime.datetime.utcnow()
    elif code == "{D_DATE}":
        return base_date
    elif code == "{END}":
        return end_date
    elif code == "{D_W_S}":
        w = Week.fromstring(base_date)
        return w.day(0)
    elif code == "{D_W_E}":
        w = Week.fromstring(base_date)
        return w.day(6)
    elif code == "{D_M_S}":
        return datetime.date(base_date.year, base_date.month, 1)
    elif code == "{D_M_E}":
        m_range = calendar.monthrange(base_date.year, base_date.month)
        return datetime.date(base_date.year, base_date.month, m_range[1])
    elif code == "{D_Y_S}":
        return datetime.date(base_date.year, 1, 1)
    elif code == "{D_Y_E}":
        return datetime.date(base_date.year, 12, 31)
    elif code == "{N_W_S}":
        w = Week.fromordinal(today)
        return w.day(0)
    elif code == "{N_W_E}":
        w = Week.fromordinal(today)
        return w.day(6)
    elif code == "{N_M_S}":
        return datetime.date(today.year, 1, 1)
    elif code == "{N_M_E}":
        m_range = calendar.month(today.year, today.month)
        return datetime.date(today.year, today.month, m_range[1])
    elif code == "{N_Y_S}":
        return datetime.date(today.year, 1, 1)
    elif code == "{N_Y_E}":
        return datetime.date(today.year, 12, 31)
    else:
        return today.date()


def _getRawOffset(offset):
    """ Get the raw offset value in an offset

    Args:
        offset:

    Returns:

    """
    result = offset
    for mark in MARKERS_OPERATOR + MARKERS_INTERVAL:
        result = result.replace(mark, "")

    if result == "":
        return None

    return result


def _getOffsetAsDays(count, interval_type):
    """ Get an offset as a base of days

    Args:
        count:
        interval_type:

    Returns:

    """
    if interval_type == "D":
        return count

    if interval_type == "W":
        return count * 7

    if interval_type == "M":
        return count * 31

    if interval_type == "Y":
        return count * 365

    return count


def _process_date(date_spec, base_date, end_date):
    iso_date = False

    try:
        datetime.datetime.strptime(date_spec, "%Y-%m-%d")
        return date_spec
    except Exception:
        pass

    tmp = date_spec
    code = None
    dir = "-"
    offset = None
    interval = None

    for date_code in CODES:
        if date_code in tmp:
            code = date_code
            tmp = tmp.replace(date_code, "")

    if "-" in date_spec:
        dir = "-"
        tmp = tmp.replace("-", "")

    if "+" in date_spec:
        dir = "+"
        tmp = tmp.replace("+", "")

    for mark in MARKERS_INTERVAL:
        if mark in tmp:
            interval = mark
            tmp = tmp.replace(mark, "")

    # Whatevers left should be the offset
    if tmp == "":
        offset = None
    else:
        offset = int(tmp)

    result = _parseCode(code, base_date, end_date)

    if offset:
        daysOffset = _getOffsetAsDays(offset, interval)
        if dir == "+":
            result = result + datetime.timedelta(days=daysOffset)
        else:
            result = result - datetime.timedelta(days=daysOffset)

    return result.strftime("%Y-%m-%d")
