def _parse_filter(comparator, token, value):
    sql = ""

    if comparator == "EQ":
        sql = "%s = '%s'" % (token, value,)
    elif comparator == "NEQ":
        sql = "%s != '%s'" % (token, value,)
    elif comparator == "GT":
        sql = "%s > '%s'" % (token, value,)
    elif comparator == "GTE":
        sql = "%s >= '%s'" % (token, value,)
    elif comparator == "LT":
        sql = "%s < '%s'" % (token, value,)
    elif comparator == "LTE":
        sql = "%s <= '%s'" % (token, value,)

    return sql
