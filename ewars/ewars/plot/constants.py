import re

OFFSET_REG = r'/((-|+][0-9]*[DWMY]))/'
DATE_REG = r'([0-9]{4}-[0-9]{2}-[0-9]{2})/'
CODE_REG = re.compile('([{][A-Z_]*[}])([-|+]{1})?([0-9]*)?([DWMY]{1})?')

NUMERALS = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

INT_MARKERS = ("D", "W", "M", "Y")

CODES = (
    "{NOW}",
    "{D_DATE}",
    "{END}",
    "{D_W_S}",
    "{D_W_E}",
    "{D_M_S}",
    "{D_M_E}",
    "{D_Y_S}",
    "{D_Y_E}",
    "{N_W_S}",
    "{N_W_E}",
    "{N_M_S}",
    "{N_M_E}",
    "{N_Y_S}",
    "{N_Y_E}"
)

DATE_TYPES_PRESET = (
    "RECORD_DATE",
    "ALERT_DATE",
    "USER_REGISTER_DATE"
)

MARKERS_NUM = ("1", "2", "3", "4", "5", "6", "7", "8", "9", "0")
MARKERS_OPERATOR = ("+", "-")
MARKERS_INTERVAL = ("D", "W", "M", "Y")

ALIASES = ("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
           "w", "x", "y", "z")
