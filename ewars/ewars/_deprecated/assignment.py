import json
import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.core.tasks import create_task

from ewars.core.locations import utils

from psycopg2.extensions import AsIs

from ewars.core.serializers.json_encoder import JSONEncoder


async def get_user_assigns(user_id, user=None):
    """ Get a specific users assignments

    Args:
        user_id: The id of the user
        user:  The calling user

    Returns:

    """
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*, f.name as form_name, l.name as location_name
            FROM %s.assignments AS a
              LEFT JOIN %s.forms AS f on f.id = a.form_id
              LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.user_id = %s
              AND a.status != 'DELETED';
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user_id,
        ))
        results = await cur.fetchall()

    return results


def get_user_assignments(user=None):
    """REtrieve a users assignments

    Args:
        user: The calling user

    Returns:
        A list of assignments
    """
    results = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name AS location_name, f.name AS form_name
                FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
                LEFT JOIN %s.forms AS f ON f.id = a.form_id
            WHERE a.user_id = %s
                AND a.status IN ('ACTIVE','INACTIVE');
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user.get("id"),
        ))
        results = cur.fetchall()

    # Organize the assignments grouping them together
    # by for id

    assigns = dict()

    for assign in results:
        try:
            assigns[assign.get("form_id")]['locations'].append(dict(
                created=assign.get("created"),
                created_by=assign.get("created_by"),
                end_date=assign.get("end_date"),
                start_date=assign.get("start_date"),
                location_id=assign.get("location_id", None),
                name=(
                    assign.get("location_name", None),
                    utils.get_location_full_name(assign.get("location_id"), user=user)
                ),
                status=assign.get("status", None),
                uuid=assign.get("uuid", None)
            ))
        except KeyError:
            assigns[assign.get("form_id")] = dict(
                form_name=assign.get("form_name"),
                form_id=assign.get("form_id"),
                locations=[dict(
                    created=assign.get("created"),
                    created_by=assign.get("created_by"),
                    end_date=assign.get("end_date"),
                    start_date=assign.get("start_date"),
                    location_id=assign.get("location_id", None),
                    name=(
                        assign.get("location_name", None),
                        utils.get_location_full_name(assign.get("location_id"), user=user)
                    ),
                    status=assign.get("status", None),
                    uuid=assign.get("uuid", None)
                )],
                definition=assign.get("definition")
            )

    return [x for x in assigns.values()]

