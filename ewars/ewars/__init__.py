from __future__ import unicode_literals

import threading

import os
import json
import gettext

gettext.install("ewars")

from ewars import interfaces

from tornado.queues import Queue
import tornado.platform.asyncio
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen

from tornado.options import define, options
import asyncio

from ewars.core.scheduler import scheduler
from concurrent.futures import ThreadPoolExecutor
from ewars.db import create_pool, create_async_pool, get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.interfaces import api_desktop
from ewars.interfaces import api_mobile, api_web, sync_package_download, api_desktop
from ewars.interfaces.tcp_interface import SonomaServer
from ewars.conf import settings

VERSION = (3, 4, 6, 'alpha', 0)
__version__ = "5.0.17"

JOBS = dict()


def timed_event():
    print("HERE")


def configure(set_prefix=True):
    """
    Configure the settings (this happens as a side effect of accessing the first setting),
    configure logging and populate the app registry.

    :param set_prefix:
    :return:
    """
    from ewars.conf import settings
    # from ewars.utils.log import configure_logging

    # configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)


define("port", default=8088, help="Select port to run application on", type=int)


class DownHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("maintenance.html")


class StaticFileHandler(tornado.web.StaticFileHandler):
    def get_content_type(self):
        if self.absolute_path.endswith(".js.gz"):
            return "application/javascript"

        content_type = super(StaticFileHandler, self).get_content_type()

        if content_type == "application/octet-stream":
            return "text/html"

        return content_type

    def set_headers(self):
        if self.absolute_path.endswith(".js.gz"):
            self.set_header("Content-Encoding", "gzip")

        super(StaticFileHandler, self).set_headers()


class Application(tornado.web.Application):
    def __init__(self):
        configure()
        abs_path = os.path.dirname(os.path.realpath(__file__))
        tmpl_path = os.path.join(abs_path, "templates")
        static_path = os.path.join(abs_path, "static")

        handlers = [
            (r"/sync_pack/(.*)", sync_package_download.SyncPackageService),
            # (r"/arc/fb", interfaces.SocketFallback),
            (r"/arc/analysis", interfaces.AnalysisHandler),
            (r"/plotter", interfaces.PlotterHandler),
            # (r"/api/_d", api_desktop.Service),
            (r"/api/_w", api_web.Service),
            (r"/api/_m", api_mobile.Service),
            (r"/api/_d", api_desktop.Service),
        ]
        handlers = handlers + interfaces.socket_routes
        handlers = handlers + interfaces.socket_web_routes
        handlers = handlers + interfaces.docs_uris + interfaces.print_routes
        handlers = handlers + interfaces.register_applications
        handlers = handlers + interfaces.document_uris + interfaces.report_uris
        handlers = handlers + interfaces.site_uris + interfaces.applications

        # self.connection_pool = tornado.ioloop.IOLoop.current().run_sync(create_async_pool)

        tornado.web.Application.__init__(self,
                                         handlers=handlers,
                                         template_path=tmpl_path,
                                         static_path=static_path,
                                         static_handler_class=StaticFileHandler,
                                         debug=True,
                                         compiled_template_cache=settings.DEBUG,
                                         gzip=True,
                                         compress_response=True,
                                         cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE"
                                         )


def run(port=8088):
    print("EWARS Starting\n")
    print("  Binding socket...")
    sockets = tornado.netutil.bind_sockets(port)
    tcp_sockets = tornado.netutil.bind_sockets(6142)

    num_processes = None
    if settings.DEBUG:
        print("  DEBUG mode enabled")
        num_processes = 1
    print("  Starting ewars...")
    tornado.process.fork_processes(num_processes)
    tornado.platform.asyncio.AsyncIOMainLoop().install()
    print("  Starting %s process(es)..." % (num_processes,))

    # Create connection here
    # create_pool()
    print("  Bootstrapping application...")
    application = Application()

    application.pool = create_pool()

    server = tornado.httpserver.HTTPServer(
        application,
        max_body_size=500 * 1024 * 1024,
        xheaders=True,
    )
    print("  Starting web interface...")
    server.add_sockets(sockets)

    print("  Starting job scheduler...")
    if tornado.process.task_id() == 0:
        tornado.ioloop.PeriodicCallback(lambda: tornado.ioloop.IOLoop.current().spawn_callback(scheduler, JOBS),
                                        1000).start()

    tcp_server = SonomaServer()
    tcp_server.add_sockets(tcp_sockets)
    print("\n## EWARS started\n")
    print("   Version ---- %s" % (settings.VERSION,))
    print("   Port ------- %s" % (port,))
    print("   Processes -- %s" % (num_processes,))
    print("   Debug ------ %s" % (settings.DEBUG,))
    print("   Mode ------- MULTITENANT")
    print("   TCP Port --- 6142")
    print("\n")
    print("CTRL+C to stop server")
    asyncio.get_event_loop().run_forever()
