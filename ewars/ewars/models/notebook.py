import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Notebook:
    @staticmethod
    async def get(user=None):
        """ Get a comprehensive list of notebooks that this user has access to

        Args:
            user: The calling user

        Returns:
            A list of notebooks including the users own and any shared
            within the account
        """
        results = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT n.*, u.name AS user_name
                FROM %s.notebooks AS n
                  LEFT JOIN _iw.users AS u ON u.id = n.created_by
                WHERE n.created_by = %s OR n.shared IS TRUE;
            """, (
                AsIs(user.get('tki')),
                user.get('id'),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def update(not_id, data, user=None):
        """  Update a notebook within the system
        Args:
            not_id:
            data:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.notebooks
                SET name = %s,
                definition = %s,
                description = %s,
                last_modified = %s,
                shared = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get("definition")),
                data.get("description"),
                datetime.datetime.utcnow(),
                data.get('shared'),
                not_id,
            ))

        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.notebooks WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                not_id
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(not_id, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.notebooks WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                not_id,
            ))

        return True

    @staticmethod
    async def shared(user=None):
        """ Get shared notebooks

        Args:
            user: The calling user

        Returns:

        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT n.*, u.name AS user_name
                FROM %s.notebooks AS n
                  LEFT JOIN %s.users AS u ON u.id = n.created_by
                WHERE n.shared is TRUE
                AND n.created_by != %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def create(data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.notebooks
                (name, definition, created_by, created, last_modified, description, shared)
                VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get("definition")),
                user.get('id'),
                datetime.datetime.now(),
                datetime.datetime.now(),
                data.get("description"),
                data.get("shared", False),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def query(q_type, user=None):
        """

        Args:
            q_type:
            user:

        Returns:

        """
        results = []

        if q_type == "USER":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.notebooks WHERE created_by = %s
                """, (
                    AsIs(user.get("tki")),
                    user.get('id'),
                ))
                results = await cur.fetchall()

        elif q_type == "SHARED":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT n.*, u.name AS user_name, u.email AS user_email
                    FROM %s.notebooks AS n
                        LEFT JOIN _iw.users AS u ON u.id = n.created_by
                    WHERE n.created_by != %s
                """, (
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                results = await cur.fetchall()

        return results
