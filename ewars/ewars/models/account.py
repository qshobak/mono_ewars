import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars import administration


class AccountModel(object):
    __slots__ = [
        'tki',
        'name',
        'aid',
        'domain'
    ]

    def __init__(self, aid, tki=None):
        self.aid = aid
        self.token = tki

    def update_domain(self, new_domain):
        self.domain = new_domain

    def get_domain(self):
        return self.domain

    def disable(self):
        pass

    def enable(self):
        pass

    def is_active(self):
        pass

    def get_roles(self):
        pass

    def update_setting(self, prop, value):
        pass

    def update_settings(self, conf):
        pass

    def clear_setting(seld, prop):
        pass


class Account:
    @staticmethod
    async def get_regional_admins(lineage, user=None):
        """ Get all REGIONAL_ADMINS within a lineage
        :param lineage: {list} A list of location ids which make up a lineage
        :param user: {dict} The calling user
        :return: List of user records
        """
        admins = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, email FROM %s.users
                    WHERE role = 'REGIONAL_ADMIN'
                    AND status = 'ACTIVE'
                    AND location_id::TEXT IN %s;
            """, (
                AsIs(user.get("tki")),
                tuple(lineage),
            ))
            admins = await cur.fetchall()

        return admins

    @staticmethod
    async def get_account_admins_by_tki(tki):
        users = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, email
                FROM %s.users
                WHERE role = 'ACCOUNT_ADMIN'
                  AND status = 'ACTIVE';
            """, (
                AsIs(tki),
            ))
            users = await cur.fetchall()

        return users

    @staticmethod
    async def get_account_admins(account_id=None, user=None):
        """
        Get all ACCOUNT_ADMINS for a give account
        :param user:
        :return:
        """
        users = []

        async with get_db_cur() as cur:
            await cur.execute("""
                    SELECT id, name, email FROM %s.users
                    WHERE role = 'ACCOUNT_ADMIN'
                        AND status = 'ACTIVE'
                """, (
                AsIs(user.get("tki")),
            ))
            users = await cur.fetchall()

        return users

    @staticmethod
    async def update_dashboards(data, user=None):
        """ Update the dashboards configured for an account

       Args:
           acc_id: The account to update
           data: The data to update with
           user: The calling user

       Returns:

       """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE _iw.accounts
                SET dashboards = %s
                WHERE id = %s
            """, (
                json.dumps(data),
                user.get("aid"),
            ))

        return True

    @staticmethod
    async def create(data, user=None):
        """ Create a new account within the system
        
        Args:
            data: The details for the account
            user:  The calling user

        Returns:

        """
        if user.get('user_type', None) != 'SUPER_ADMIN':
            return False

        result = await administration.create_account(data, user=user)

        return result

    @staticmethod
    async def delete():
        pass

    @staticmethod
    async def reset_admin_password():
        pass

    @staticmethod
    async def get_admin():
        pass

    @staticmethod
    async def disabled_account():
        pass

    @staticmethod
    async def update():
        pass

    @staticmethod
    async def patch():
        pass

    @staticmethod
    async def usage(user=None):
        result = dict()

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.locations WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get('tki')),
            ))
            locations = await cur.fetchone()
            result['locations'] = locations.get('total', 0)

            await cur.execute("""
                SELECT COUNT(*) As total FROM %s.users;
            """, (
                AsIs(user.get('tki')),
            ))
            users = await cur.fetchone()
            result['users'] = users.get('total', 0)

            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.forms WHERE status != 'DELETED';
            """, (
                AsIs(user.get('tki')),
            ))
            forms = await cur.fetchone()
            result['forms'] = forms.get('total', 0)

            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.devices;
            """, (
                AsIs(user.get('tki')),
            ))
            devices = await cur.fetchone()
            result['devices'] = devices.get('total', 0)

            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.collections WHERE status = 'SUBMITTED';
            """, (
                AsIs(user.get('tki')),
            ))
            records = await cur.fetchone()
            result['records'] = records.get('total', 0)

            await cur.execute("""
                SELECT COUNT(*) AS total from %s.data_sets_data;
            """, (
                AsIs(user.get('tki')),
            ))
            records_ds = await cur.fetchone()
            result['records_ds'] = records_ds.get('total', 0)

            await cur.execute("""
                SELECT schema_name,
                         pg_size_pretty(sum(table_size)::bigint) AS total,
                         (sum(table_size) / pg_database_size(current_database())) * 100
                FROM
                  (SELECT pg_catalog.pg_namespace.nspname AS schema_name,
                         pg_relation_size(pg_catalog.pg_class.oid) AS table_size
                  FROM pg_catalog.pg_class
                  JOIN pg_catalog.pg_namespace
                      ON relnamespace = pg_catalog.pg_namespace.oid ) t
                  WHERE schema_name = %s
                GROUP BY  schema_name
            """, (
                user.get('tki'),
            ))
            datasize = await cur.fetchone()
            result['db_size'] = datasize.get('total')

            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.alerts;
            """, (
                AsIs(user.get('tki')),
            ))
            alerts = await cur.fetchone()
            result['alerts'] = alerts.get('total', 0)

        return result

