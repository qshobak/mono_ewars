import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Map:
    @staticmethod
    async def get_form_points(data, user=None):
        tki = data.get('tki', None)

        lid = data.get("lid", None)
        start_date = data.get("start_date", None)
        end_date = data.get("end_date", None)

        where_lid = ""
        where_dates = ""
        if lid is not None:
            where_lid = "AND l.lineage::TEXT[] @> ARRAY['%s']::TEXT[] " % (
                lid,
            )

        if start_date is not None and end_date is not None:
            where_dates = " AND c.data_date >= '%s' AND c.data_date <= '%s' " % (
                start_date,
                end_date,
            )

        if user is not None:
            tki = user.get('tki', None)

        result = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT name 
                FROM %s.forms
                WHERE id = %s;
            """, (
                AsIs(tki),
                data.get("field")[0],
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT c.uuid::TEXT AS uuid, 
                    c.location_id, 
                    c.data->'%s' AS point,
                    c.data_date,
                    u.name AS user_name,
                    u.email AS user_email,
                    f.name AS form_name
                FROM %s.collections AS c
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                  LEFT JOIN %s.users AS u ON u.id = c.created_by
                  LEFT JOIN %s.forms AS f ON f.id = c.form_id
                WHERE c.form_id = %s 
                  AND c.status = 'SUBMITTED'
                  AND c.data->>'%s'::TEXT IS NOT NULL
                  %s 
                  %s
            """, (
                AsIs(data.get("field")[1]),
                AsIs(tki),
                AsIs(tki),
                AsIs(tki),
                AsIs(tki),
                data.get("field")[0],
                AsIs(data.get("field")[1]),
                AsIs(where_lid),
                AsIs(where_dates),
            ))
            result = await cur.fetchall()

            await cur.execute("""
                SELECT COUNT(c.*) as count
                FROM %s.collections AS c
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE c.form_id = %s 
                  AND c.status = 'SUBMITTED'
                  AND c.data->'%s' IS NOT NULL
                  %s 
                  %s
            """, (
                AsIs(tki),
                AsIs(tki),
                data.get("field")[0],
                AsIs(data.get("field")[1]),
                AsIs(where_lid),
                AsIs(where_dates),
            ))
            valid = await cur.fetchone()

            await cur.execute("""
                SELECT COUNT(c.*) as count
                FROM %s.collections AS c
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE c.form_id = %s 
                  AND c.status = 'SUBMITTED'
                  AND c.data->'%s' IS NULL
                  %s 
                  %s
            """, (
                AsIs(tki),
                AsIs(tki),
                data.get("field")[0],
                AsIs(data.get("field")[1]),
                AsIs(where_lid),
                AsIs(where_dates),
            ))
            missing = await cur.fetchone()

        return dict(
            d=result,
            missing=missing.get("count", 0),
            valid=valid.get("count", 0),
            form_name=form.get("name", {})
        )

    @staticmethod
    async def get_geometries(data, user=None):
        tki = data.get('tki', None)

        if user is not None:
            tki = user.get('tki', None)

        result = {}

        if data.get("type", None) == 'TYPE':
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name, geojson, geometry_type
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s::TEXT[]
                    AND site_type_id = %s 
                    AND status = 'ACTIVE';
                """, (
                    AsIs(tki),
                    [data.get("lid")],
                    data.get("loc_type"),
                ))
                result = await cur.fetchall()

        if data.get("type", None) == "SINGLE":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name, geojson, geometry_type
                    FROM %s.locations
                    WHERE uuid = %s;
                """, (
                    AsIs(tki),
                    data.get("lid"),
                ))
                result = await cur.fetchone()
                result = [result]

        if data.get("type", None) == "GROUP":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name, geojson, geometry_type
                    FROM %s.locations
                    WHERE groups::TEXT[] @> %s::TEXT[]
                    AND status = 'ACTIVE';
                """, (
                    AsIs(tki),
                    [data.get("group")],
                ))
                result = await cur.fetchall()

        return result

    async def get_map_base_options(user=None):
        result = dict()

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.id, f.name, fv.definition
                FROM %s.forms AS f 
                LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

            await cur.execute("""
                SELECT id, name
                FROM %s.location_types;
            """, (
                AsIs(user.get("tki")),
            ))
            location_types = await cur.fetchall()

            await cur.execute("""
                SELECT groups 
                FROM %s.locations
                WHERE groups IS NOT NULL 
                  AND array_length(groups, 1) > 0;
            """, (
                AsIs(user.get("tki")),
            ))
            _groups = await cur.fetchall()

            groups = []
            for location in _groups:
                groups = groups + location.get("groups", [])

            unique = []
            for item in groups:
                if item not in unique:
                    unique.append(item)

            result['forms'] = forms
            result['location_types'] = location_types
            result['groups'] = unique

        return result

    @staticmethod
    async def get(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.maps
                WHERE created_by = %s
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def create(data, user=None):
        """ Save a new map

        Args:
            data:  The data for the map
            user: THe calling user

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.maps
                (name, definition, created_by, created, last_modified, description, locked, shared)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get("definition", {})),
                user.get("id"),
                datetime.datetime.now(),
                datetime.datetime.now(),
                data.get("description", ""),
                False,
                data.get("shared", False),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(map_id, data, user=None):
        """ UPdate a maps settings

        Args:
            data: The data to update into the map
            user:  The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.maps
                SET name = %s,
                    definition = %s,
                    last_modified = %s,
                    description = %s,
                    shared = %s
                WHERE uuid =%s
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get("definition")),
                datetime.datetime.now(),
                data.get('description'),
                data.get("shared", False),
                map_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.maps WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                map_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(map_id, user=None):
        """ DElete a map from the system

        Args:
            map_id: THe id of the map to delete
            user: The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.maps WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                map_id,
            ))

        return True

    @staticmethod
    async def get_shared(user=None):
        results = []
        async with get_db_cur() as cur:
            await cur.execute("""
                    SELECT m.*, u.name AS user_name, u.email AS user_email
                    FROM %s.maps AS m
                    LEFT JOIN %s.users AS u on u.id = m.created_by
                    WHERE u.aid = %s
                    AND u.id != %s
                    AND m.shared = TRUE
                """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("aid"),
                user.get("id"),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def query(map_type, user=None):
        """ Retrieve maps

        Args:
            user: The calling user

        Returns:

        """
        results = []

        if map_type == "SHARED":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT m.*, u.name AS user_name, u.email AS user_email
                    FROM %s.maps AS m
                    LEFT JOIN %s.users AS u on u.id = m.created_by
                    WHERE u.aid = %s
                    AND u.id != %s
                    AND m.shared = TRUE
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get("aid"),
                    user.get("id"),
                ))
                results = await cur.fetchall()
        elif map_type == "USER":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT *
                    FROM %s.maps
                    WHERE created_by = %s
                """, (
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                results = await cur.fetchall()

        return results

    @staticmethod
    async def create_map(data, user=None):
        """ Create a new map in the system

        Args:
            data:
            user:

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.mapping
                (title, shared, description, created_by, modified_by, annotations, layers, config)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("title", "New map"),
                data.get("shared", False),
                data.get("description", ""),
                user.get('id'),
                user.get("id"),
                json.dumps(data.get("annotations", [])),
                json.dumps(data.get("layers", [])),
                json.dumps(data.get("config", {})),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update_map(map_id, data, user=None):
        """ Update a map in the system

        Args:
            map_id:
            data:
            user:

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.mapping
                SET title = %s,
                    shared = %s,
                    description = %s,
                    modified_by = %s,
                    modified = %s,
                    annotations = %s,
                    layers = %s,
                    config = %s
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                data.get("title", "New map"),
                data.get("shared", False),
                data.get('description', ""),
                user.get('id'),
                datetime.datetime.utcnow(),
                json.dumps(data.get("annotations", [])),
                json.dumps(data.get("layers", [])),
                json.dumps(data.get("config", {})),
                map_id,
            ))

        return data

    @staticmethod
    async def delete_map(map_id, user=None):
        """ Delete a map from the fsystem

        Args:
            map_id:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.mapping
                WHERE uuid =%s;
            """, (
                AsIs(user.get("tki")),
                map_id,
            ))

        return True

    @staticmethod
    async def get_maps(shared, user=None):
        results = []

        async with get_db_cur() as cur:
            if shared:
                await cur.execute("""
                    SELECT * FROM %s.mapping
                    WHERE created_by != %s 
                    AND shared = TRUE;
                """, (
                    AsIs(user.get('tki')),
                    user.get("id"),
                ))
                results = await cur.fetchall()

            else:
                await cur.execute("""
                    SELECT * FROM %s.mapping
                    WHERE created_by = %s;
                """, (
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                results = await cur.fetchall()

        return results
