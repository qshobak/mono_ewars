import json
import uuid
import datetime

from ewars.db import get_db_cur
from ewars.utils.six import string_types
from ewars.core.serializers import JSONEncoder

from psycopg2.extensions import AsIs

from ewars.constants import defaults_account_admin, defaults_regional_admin, defaults_user

USER_DASHES = dict(
    ACCOUNT_ADMIN=defaults_account_admin,
    REGIONAL_ADMIN=defaults_regional_admin,
    USER=defaults_user,
)


class Dashboard:
    @staticmethod
    async def get(dash_id, user=None):
        result = None

        if isinstance(dash_id, (string_types,)):
            # This is a system default dashboard
            if "-" in dash_id:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT * FROM %s.layouts
                        WHERE uuid = %s
                    """, (
                        AsIs(user.get("tki")),
                        dash_id,
                    ))
                    result = await cur.fetchone()
            else:
                result = Dashboard.get_default_dashboard(dash_id, user=user)
        else:
            item_id = dash_id.get("uuid")
            if "-" in item_id:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT * FROM %s.layouts
                        WHERE uuid = %s
                    """, (
                        AsIs(user.get("tki")),
                        item_id,
                    ))
                    result = await cur.fetchone()
            else:
                result = Dashboard.get_default_dashboard(item_id, user=user)

        return result

    @staticmethod
    async def update(preset_id, data, user=None):
        """ Update a preset

        Args:
            preset_id: The UUID of the preset
            data: The data to update
            user: THe calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.layouts
                    SET name = %s,
                    definition = %s,
                    last_modified = %s,
                    modified_by = %s,
                    description = %s,
                    access = %s,
                    status = %s,
                    locked = %s,
                    color = %s
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get('name'),
                json.dumps(data.get("definition"), cls=JSONEncoder),
                datetime.datetime.now(),
                user.get("id"),
                json.dumps(data.get('description', {})),
                data.get('access', []),
                data.get('status'),
                data.get("locked", False),
                data.get("color", None),
                preset_id,
            ))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.layouts
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                preset_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        """ Create a new preset in the system

        Args:
            data: The data for the preset
            user: The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.layouts
                (name, status, layout_type, created, created_by, definition, last_modified, modified_by, description, access, locked, color)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                data.get("status", "INACTIVE"),
                "DASHBOARD",
                datetime.datetime.now(),
                user.get("id"),
                json.dumps(data.get("definition", '[]')),
                datetime.datetime.now(),
                user.get("id"),
                json.dumps(data.get('description')),
                data.get("access"),
                False,
                data.get("color", None),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(preset_id, user=None):
        """ Delete a preset from the system

        Args:
            preset_id: The UUID of the preset
            user: The calling user

        Returns:

        """

        dashboards = None
        new_dashes = dict()

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT dashboards
                FROM _iw.accounts
                WHERE id = %s
            """, (
                user.get("aid"),
            ))
            dashboards = await cur.fetchone()
            dashboards = dashboards.get('dashboards', [])

            for user_type, dashes in dashboards.items():
                new_dashes[user_type] = []

                for dash in dashes:
                    if isinstance(dash, (dict,)):
                        if dash.get("uuid") != preset_id:
                            new_dashes[user_type].append(dash)
                    elif isinstance(dash, (list,)):
                        replacements = []
                        for sub_dash in dash[2]:
                            if sub_dash[0] != preset_id:
                                replacements.append(sub_dash)
                        dash[2] = replacements

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE _iw.accounts
                SET dashboards = %s
                WHERE id = %s
            """, (
                json.dumps(new_dashes),
                user.get("aid"),
            ))

            await cur.execute("""
                DELETE FROM %s.layouts
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                preset_id,
            ))

        return True

    @staticmethod
    def get_default_dashboard(dash_id, user=None):
        """ Retrieve a default dashboard for a user

        Args:
            dash_id: The id of the dashboard
            user: THe calling user

        Returns:
            A dashboard definition with any associated metrics loaded
        """

        # Check the id, if it has a slash in it, it's a UUID for a stored layout
        # if it doesn't it's a static default dashboard
        result = None
        if "-" in dash_id:
            pass
        else:
            item_set = USER_DASHES.get(user.get("role"))

            result = getattr(item_set, dash_id)

            result['uuid'] = dash_id

        return result

    @staticmethod
    def get_default_dashboards(user_type):
        """ Return system default dashboards, used
        when there are no configured dashboards for the user type
        in the account

        Args:
            user_type: The type of user to retrieve dashboards for

        Returns:
            The list of dashboards for the user, their metric loads
            and the layout for the initial dashboard to load to offset
            re-calling to retrieve the dashboard
        """
        dashboards = []
        initial = None
        metrics = dict()

        item_set = USER_DASHES.get(user_type)

        dashes = getattr(item_set, "DEFAULTS")
        for item in dashes:
            dashboards.append(dict(
                uuid=item,
                name=getattr(item_set, item).get("name"),
                icon=getattr(item_set, item).get("icon"),
                color=getattr(item_set, item).get("color")
            ))

        initial = getattr(item_set, dashes[0])

        return dict(
            dashboards=dashboards,
            initial=initial
        )
