import os
import sys

from ewars.utils._os import get_install_root
from ewars.utils import markdown


class Release:
    @staticmethod
    def get_release_list():
        """ Get a list of release files for rendering

        Returns:

        """
        install_root = get_install_root()

        releases_path = os.path.join(install_root, "static/releases")

        files = [f for f in os.listdir(releases_path) if os.path.isfile(os.path.join(releases_path, f))]

        results = []

        for file in files:
            if "development" not in file:
                name = file.replace(releases_path, "")
                name = name.replace(".md", "")
                results.append((
                    name,
                    file,
                ))

        results = sorted(results, key=lambda s: [int(u) for u in s[0].split(".")])
        results = list(reversed(results))

        return results

    @staticmethod
    def get_release_file(file_name):
        """ Retrieve a release file

        Args:
            file_name: The file name to retrieve

        Returns:

        """
        install_root = get_install_root()

        releases_path = os.path.join(install_root, "static/releases", file_name)

        content = None
        with open(releases_path, "r") as f:
            content = f.read()

        return markdown.markdown(content, extras=["task_list"])
