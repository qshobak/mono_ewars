import json
import uuid
import datetime
import asyncio

from ewars.constants import CONSTANTS
from ewars.db import get_db_cur
from ewars.utils import date_utils, form_utils
from .location import Location
from ewars.core import user_lookup, notifications
from ewars.core.serializers import JSONEncoder
from ewars.core import tasks

from psycopg2.extensions import AsIs


class Assignment:
    @staticmethod
    async def get_assignables(user=None):
        """Retrieve forms that a user can request assignment for

        Args:
            user: The calling user

        Returns:
            List of forms
        """
        forms = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, location_aware, time_interval, location_type, description
                    FROM %s.forms
                    WHERE ftype = 'PRIMARY'
                        AND status = 'ACTIVE';
            """ % (
                AsIs(user.get("tki"))
            ))
            forms = await cur.fetchall()

        return forms

    @staticmethod
    async def request(definition, user=None):
        """Create a new user assignment

        Args:
            user_id: The id of the user to add the assignment to
            definition: The definition for the assignment
            user: The calling user

        Returns:
            The result of the addition
        """
        results = None

        async with get_db_cur() as cur:
            await cur.execute("""
             SELECT * FROM %s.assignments
             WHERE user_id = %s 
              AND form_id = %s
              AND location_id = %s
              AND status != 'INACTIVE';
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
                definition.get("form_id"),
                definition.get("location_id"),
            ))
            results = await cur.fetchone()

        hasPending = False
        hasActive = False
        if results is not None:
            if results.get("status") == "PENDING":
                hasPending = True

            if results.get("status") == "ACTIVE":
                hasActive = True

        # There's an assignment already pending
        if hasPending:
            return dict(
                err=True,
                code="PENDING",
                message="You already have an assignment awaiting approval from an administrator."
            )

        # There's already an active assignment
        if hasActive:
            return dict(
                err=True,
                code="ACTIVE",
                message="You already have an active assignment of this type."
            )

        result = await tasks.create_task(
            'ASSIGNMENT',
            user.get('id'),
            definition,
            user
        )

        return True


    @staticmethod
    async def delete(assignment_uuid, user=None):
        """
        Delete an assignment, same as marking it as inactive
        :param assignment_uuid:
        :param user:
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                 DELETE FROM %s.assignments
                 WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                assignment_uuid,
            ))

    @staticmethod
    async def update(assign_id, data, user=None):
        """
        Update an assignment, the only property currently updateable is the end date of the
        assignment
        :param assignUUID: {str} THe UUID of the assignment to update
        :param data: {dict} Key-Value of properties to update
        :param user: {dict} The calling user
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.assignments
                SET start_date = %s,
                    end_date = %s,
                    status = %s,
                    last_modified = %s,
                    type = %s,
                    definition = %s,
                    location_id = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.utcnow(),
                datetime.datetime.utcnow(),
                data.get("status"),
                datetime.datetime.utcnow(),
                data.get("type", "DEFAULT"),
                json.dumps(data.get("definition", {})),
                data.get("location_id", None),
                assign_id,
            ))

    @staticmethod
    async def create(user_id, data, user=None):
        """
        Create a new assignment for the user
        :param data: {dict} The definition of the assignment
        :param user: {dict} The calling user
        :return:
        """
        location_id = data.get("location_uuid", None)

        if location_id is None:
            location_id = data.get("location_id", None)

        # Check to make sure that a pre-0existing assignment doesn't already exists
        existing = None


        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.assignments
                WHERE user_id = %s
                    AND location_id = %s
                    AND form_id = %s
                    AND type = %s
                    AND status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                data.get("user_id"),
                location_id,
                data.get("form_id"),
                data.get("type", "DEFAULT"),
            ))

        if existing is not None:
            raise Exception("Duplicate assignment exists")

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.assignments (user_id, created, created_by, last_modified, status, form_id, location_id, start_date, end_date, definition, type)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                data.get("user_id"),
                datetime.datetime.now(),
                user['id'],
                datetime.datetime.now(),
                'ACTIVE',
                data.get("form_id"),
                location_id,
                datetime.datetime.utcnow(),
                datetime.datetime.utcnow(),
                json.dumps(data.get("definition", {}), cls=JSONEncoder),
                data.get("type", "DEFAULT"),
            ))
