import datetime
import json
import uuid

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class ReportingPeriod:
    @staticmethod
    async def delete_reporting_period(period_id, user=None):
        """ Delete a reporting period form a location
        :param period_id: {int} The id of the reporting period
        :param rules: {dict} The rules to apply to the deletion
        :param user: {dict}
        :return: {bool} True|False the operation was successful
        """
        period = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.location_reporting
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                period_id,
            ))
            period = await cur.fetchone()

            # Delete root-level reporting period
            await cur.execute("""
                DELETE FROM %s.location_reporting WHERE pid = %s;
                DELETE FROM %s.location_reporting WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                period_id,
                AsIs(user.get("tki")),
                period_id,
            ))

            # Get parent location
            parent = None
            await cur.execute("""
                SELECT parent_id::TEXT
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                period.get('location_id'),
            ))
            parent = await cur.fetchone()

            # Get any parent reporting periods for the form in the parent
            if parent.get('parent_id', None) is not None:
                parent_rule = None
                await cur.execute("""
                    SELECT *
                    FROM %s.location_reporting
                    WHERE form_id = %s
                      AND location_id::TEXT = %s;
                """, (
                    AsIs(user.get('tki')),
                    period.get('form_id'),
                    parent.get('parent_id'),
                ))
                parent_rule = await cur.fetchone()

                if parent_rule is not None:
                    # Get locations which have no rule already set for
                    # them for this form type, this after we've
                    # deleted all rules based on the original period we
                    # just deleted
                    updatable_locs = []
                    await cur.execute("""
                        SELECT l.uuid, lr.form_id
                        FROM %s.locations AS l
                          LEFT OUTER JOIN %s.location_reporting AS lr ON lr.location_id = l.uuid AND lr.form_id = %s
                        WHERE l.lineage::TEXT[] @> %s::TEXT[]
                        AND l.uuid != %s;
                    """, (
                        AsIs(user.get('tki')),
                        AsIs(user.get('tki')),
                        parent_rule.get('form_id'),
                        [period.get('location_id')],
                        period.get('location_id'),
                    ))
                    updatable_locs = await cur.fetchall()
                    updatable_locs = [x for x in updatable_locs if x.get('form_id', None) is None]

                    insertable_pid = parent_rule.get('uuid')
                    if parent_rule.get('pid', None) is not None:
                        insertable_pid = parent_rule.get('pid')

                    for loc in updatable_locs:
                        await cur.execute("""
                            INSERT INTO %s.location_reporting
                            (location_id, pid, start_date, end_date, form_id, created, created_by)
                            VALUES (%s, %s, %s, %s, %s, %s, %s);
                        """, (
                            AsIs(user.get('tki')),
                            loc.get('uuid'),
                            insertable_pid,
                            parent_rule.get('start_date'),
                            parent_rule.get('end_date', None),
                            parent_rule.get('form_id', None),
                            parent_rule.get('created', datetime.datetime.utcnow()),
                            parent_rule.get('created_by'),
                        ))

                    await cur.execute("""
                        INSERT INTO %s.location_reporting
                        (location_id, pid, start_date, end_date, form_id, created, created_by)
                        VALUES (%s, %s, %s, %s, %s, %s, %s);
                    """, (
                        AsIs(user.get('tki')),
                        period.get('location_id'),
                        insertable_pid,
                        parent_rule.get('start_date'),
                        parent_rule.get('end_date', None),
                        parent_rule.get('form_id', None),
                        parent_rule.get('created', datetime.datetime.utcnow()),
                        parent_rule.get('created_by'),
                    ))

        return True

    @staticmethod
    async def rebuild_reporting_periods(user=None):
        return False

    @staticmethod
    async def update_reporting_period(period_id, data, user=None):
        """ Update an existing reporting period
        :param period_id: {int} The id of the reporting period to update
        :param data: {dict} The data to update into the reporting period definition
        :param user: {dict} The calling user
        :return: {dict} The updated reporting period definition
        """
        type = data.get('type')
        if data.get("override_disable", False):
            type = "DISABLED"

        children = []

        reporting_period = None
        pid = None

        has_changed = False
        prop_compares = ['start_date', 'end_date', 'status', 'form_id']

        async with get_db_cur() as cur:
            # get the reporting period
            await cur.execute("""
                SELECT *
                FROM %s.location_reporting
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                period_id,
            ))
            reporting_period = await cur.fetchone()

            for prop in prop_compares:
                if reporting_period.get(prop) != data.get(prop):
                    has_changed = True

        if has_changed == False:
            return dict(
                success=True
            )

        if reporting_period.get('pid', None) is not None:
            result = await ReportingPeriod._refactor_location_to_custom(period_id, data, user=user)
        else:
            result = await ReportingPeriod._refactor_location_custom_period(period_id, data, user=user)

        return dict(
            success=True
        )

    @staticmethod
    async def _refactor_location_custom_period(period_id, data, user=None):
        """ We're updating a custom period, only need to update the period itself
            and it's inherited periods below it

        Args:
            period_id:
            data:
            user:

        Returns:

        """
        async with get_db_cur(commit=True) as cur:
            await cur.execute("""
                UPDATE %s.location_reporting
                SET start_date = %s,
                  end_date = %s,
                  status = %s
                WHERE uuid =%s;
            """, (
                AsIs(user.get('tki')),
                data.get('start_date', None),
                data.get('end_date', None),
                data.get('status', 'ACTIVE'),
                period_id,
            ))

            await cur.execute("""
                UPDATE %s.location_reporting
                SET start_date = %s,
                  end_date = %s,
                  status = %s
                WHERE pid = %s;
            """, (
                AsIs(user.get('tki')),
                data.get('start_date', None),
                data.get('end_date', None),
                data.get('status', 'ACTIVE'),
                period_id,
            ))

        return True

    @staticmethod
    async def _refactor_location_to_custom(period_id, data, user=None):
        """ Used to change a reporting period from an inherited period
        to a custom set period, updating all the child locations
        which are inherited themselves.

        Args:
            period_id:
            data:
            user:

        Returns:

        """
        original_period = None

        async with get_db_cur(commit=True) as cur:
            # Get the original reporting period entry
            await cur.execute("""
                SELECT * FROM %s.location_reporting
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                period_id,
            ))
            original_period = await cur.fetchone()

            deletable_pid = original_period.get('uuid')

            if original_period.get('pid', None) is not None:
                deletable_pid = original_period.get('pid')


            # Delete all reporting periods under this location (including itself)
            # that inherit the pid
            await cur.execute("""
                WITH locs AS (
                    SELECT uuid
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s::TEXT[]
                    AND status != 'DELETED'
                )
                DELETE FROM %s.location_reporting
                WHERE location_id = ANY(SELECT uuid from locs)
                AND pid = %s;
            """, (
                AsIs(user.get('tki')),
                [original_period.get('location_id')],
                AsIs(user.get('tki')),
                deletable_pid,
            ))

            # Create the initial reporting period
            await cur.execute("""
                INSERT INTO %s.location_reporting (location_id, form_id, start_date, end_date, status, created_by, created)
                VALUES (%s, %s, %s, %s, %s, %s, %s) returning uuid;
            """, (
                AsIs(user.get('tki')),
                original_period.get("location_id"),
                original_period.get("form_id"),
                data.get("start_date"),
                data.get("end_date"),
                data.get("status"),
                user.get("id"),
                datetime.datetime.utcnow(),
            ))
            root_period = await cur.fetchone()
            rp_uuid = root_period.get("uuid")

            # Get the locations under this one that need to have reporting periods added
            updatable_locs = []
            await cur.execute("""
                SELECT l.uuid, lr.form_id, lr.pid
                FROM %s.locations AS l
                  LEFT OUTER JOIN %s.location_reporting AS lr on lr.location_id = l.uuid AND lr.form_id = %s
                WHERE l.lineage::TEXT[] @> %s::TEXT[]
                AND l.uuid != %s;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                data.get("form_id"),
                [data.get("location_id")],
                data.get("location_id"),
            ))
            updatable_locs = await cur.fetchall()
            updatable_locs = [x for x in updatable_locs if x.get('form_id', None) is None]

            # Add the periods
            for loc in updatable_locs:
                await cur.execute("""
                    INSERT INTO %s.location_reporting (location_id, form_id, start_date, end_date, status, created_by, created, pid)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(user.get('tki')),
                    loc.get("uuid"),
                    data.get("form_id"),
                    data.get("start_date"),
                    data.get("end_date"),
                    data.get("status"),
                    user.get("id"),
                    datetime.datetime.utcnow(),
                    rp_uuid,
                ))

    @staticmethod
    async def add_reporting_period(data, user=None):
        result = None

        exists = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.location_reporting
                WHERE location_id = %s
                    AND form_id = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("location_id", None),
                data.get("form_id"),
            ))
            exists = await cur.fetchone()

        if exists is not None:
            result = dict(
                success=False,
                error="Duplicate reporting period already exists"
            )
        else:
            type = data.get('type')
            if data.get("override_disable", False) == True:
                type = "DISABLED"

            affected_locations = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT l.uuid, l.status, lr.uuid AS lr_id
                    FROM %s.locations AS l
                        LEFT JOIN %s.location_reporting AS lr ON lr.location_id = l.uuid AND lr.form_id = %s
                    WHERE l.lineage::TEXT[] @> %s::TEXT[]
                        AND l.status != 'DELETED'
                        AND l.uuid::TEXT != %s;
                """, (
                    AsIs(user.get('tki')),
                    AsIs(user.get('tki')),
                    data.get("form_id"),
                    [data.get('location_id')],
                    data.get('location_id'),
                ))
                affected_locations = await cur.fetchall()
                affected_locations = [dict(x) for x in affected_locations]

            root_id = str(uuid.uuid4())
            async with get_db_cur() as cur:
                await cur.execute("""
                            INSERT INTO %s.location_reporting (uuid, location_id, form_id, pid, start_date, end_date, status, created_by, created)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);
                        """, (
                    AsIs(user.get("tki")),
                    root_id,
                    data.get('location_id'),
                    data.get("form_id"),
                    None,
                    data.get('start_date'),
                    data.get("end_date", None),
                    data.get('status', 'ACTIVE'),
                    user.get("id"),
                    datetime.datetime.now(),
                ))

            affected_locs = list(filter(lambda k: k.get("lr_id", None) is None, affected_locations))
            async with get_db_cur() as cur:
                for loc in affected_locs:
                    if loc.get('lr_id', None) is None:
                        await cur.execute("""
                            INSERT INTO %s.location_reporting (location_id, form_id, pid, start_date, end_date, status, created_by, created)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                        """, (
                            AsIs(user.get("tki")),
                            loc.get('uuid'),
                            data.get("form_id"),
                            root_id,
                            data.get('start_date'),
                            data.get("end_date", None),
                            data.get('status', 'ACTIVE'),
                            user.get("id"),
                            datetime.datetime.now(),
                        ))

            result = dict(
                success=True
            )

        return result
