import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Task:


    @staticmethod
    async def create(task_type, assigned_to, assigned_role, data, lid, state, priority, created_by, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.tasks_v2 (task_type, assigned_user_id, assigned_user_types, data, location_id, state, priority, created_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                task_type,
                None,
                assigned_role,
                json.dumps(data),
                lid,
                state,
                priority,
                created_by,
            ))

        return True

    @staticmethod
    async def get(task_id, user=None):
        ''' Retrieve a task from the system
        
        Args:
            task_id: 
            user: 

        Returns:

        '''
        result = None

        async with get_db_cur() as cur:
            await cur.execute(''''
                SELECT * 
                FROM %s.tasks_v2
                WHERE uuid = %s;
            ''', (
                AsIs(user.get('tki')),
                task_id,
            ))
            result = await cur.fetchone()

        return result
