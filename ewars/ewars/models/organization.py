import uuid
import json
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Organization:
    @staticmethod
    async def remove(id, user=None):
        """ REmove an organization from an accounts list of organizations

        Args:
            id:
            user:

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.orgs
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                id,
            ))

        return True

    @staticmethod
    async def update_organization(id, data, user=None):
        """ Update an organizations information

        Args:
            id:
            data:
            user:

        Returns:

        """
        result = None

        current = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM _iw.organizations
                WHERE uuid = %s;
            """, (
                id,
            ))
            current = await cur.fetchone()

        # Check for name collision
        orig_name = current.get("name", {}).get("en", None)
        new_name = data.get("name", {}).get("en", None)

        if orig_name is not None:
            if orig_name != new_name:
                # Need to check if there's an org with the new name already
                existing = None
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT * FROM _iw.organizations 
                        WHERE name->>'en' = %s;
                    """, (
                        new_name,
                    ))
                    existing = await cur.fetchone()

                if existing is not None:
                    return dict(
                        err=True,
                        code="EXISTING",
                        uuid=existing.get("uuid")
                    )

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE _iw.organizations
                SET name = %s,
                    status = %s,
                    acronym = %s,
                    site = %s,
                    last_modified = %s
                WHERE uuid = %s
            """, (
                json.dumps(data.get("name")),
                data.get('status'),
                data.get('acronym', None),
                data.get("site", None),
                datetime.datetime.now(),
                id,
            ))

            await cur.execute("""
                SELECT * FROM _iw.organizations WHERE uuid = %s;
            """, (
                id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create_organization(data, user=None):
        """ Create a new organization

        Args:
            data: THe data for the organization

        Returns:

        """
        result = None

        existing = None
        async with get_db_cur() as cur:
            where = " WHERE name->>'en' ILIKE '%" + data.get("name", {}).get("en", "") + "%'"
            await cur.execute("""
                SELECT * FROM _iw.organizations
                %s;
            """, (
                AsIs(where),
            ))
            existing = await cur.fetchone()

            result = existing

        if existing is None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO _iw.organizations
                    (name, status, acronym, site, created_by, created, last_modified)
                    VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *;
                """, (
                    json.dumps(data.get('name')),
                    data.get('status'),
                    data.get("acronym", None),
                    data.get("site", None),
                    user.get('id'),
                    datetime.datetime.now(),
                    datetime.datetime.now(),
                ))
                result = await cur.fetchone()

            local = await Organization.add_organization(result.get('uuid'), user=user)

        else:
            return dict(
                err=True,
                code="EXISTING",
                uuid=existing.get("uuid")
            )

        return result

    @staticmethod
    async def delete_organization(org_id, user=None):
        """ Remove an organization from an accounts org list
        
        Args:
            org_id: 
            user: 

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.orgs
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                org_id,
            ))

        return True

    @staticmethod
    async def add_organization(org_id, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid FROM %s.orgs WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                org_id,
            ))
            exists = await cur.fetchone()

            if exists is None:
                await cur.execute("""
                    INSERT INTO %s.orgs (uuid, created, added_by)
                    VALUES (%s, %s, %s);
                """, (
                    AsIs(user.get("tki")),
                    org_id,
                    datetime.datetime.utcnow(),
                    user.get('id'),
                ))

        return True
