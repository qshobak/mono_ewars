import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Device:
    @staticmethod
    async def delete(device_id, user=None):
        """ DElete a device from the system

        Args:
            device_id: The id of the device in the system

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE from %s.devices WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                device_id,
            ))

        return True

    @staticmethod
    async def update(device_id, data, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.devices
                SET status = %s,
                    gate_id = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                data.get('status'),
                data.get('gate_id', None),
                device_id,
            ))

        return True
