import json

from ewars.utils.six import string_types

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

SOURCES = [
    "*",
    "ewars"
]

DIMENSIONS = ["select", "location", "date", "text", "textarea", 'barcode', 'lat_long']
METRICS = ["number"]

class Apec:

    @staticmethod
    async def get_metrics(user=None):
        forms = []
        indicators = []
        ind_groups = []
        location_types = []
        location_groups = []
        end_specs = []
        system = [
            "records",
            "completeness",

        ]

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.uuid, f.name, f.features, fv.definition
                FROM %s.forms AS f 
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.status != 'INACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

            await cur.execute("""
                SELECT uuid, name, itype, icode, group_id FROM %s.indicators
                WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
            ))
            indicators = await cur.fetchall()

            await cur.execute("""
                SELECT id, name, parent_id
                FROM %s.indicator_groups;
            """, (
                AsIs(user.get("tki")),
            ))
            indicator_groups = await cur.fetchall()

            await cur.execute("""
                SELECT id, name
                FROM %s.location_types;
            """, (
                AsIs(user.get("tki")),
            ))
            location_types = await cur.fetchall()

        for form in forms:
            has_interval = form.get("features", {}).get("INTERVAL_REPORTING", None) is not None
            has_location = form.get("features", {}).get("LOCATION_REPORTING", None) is not None
            _form = dict(
                loc_rep=has_location,
                int_rep=has_interval,
                name=form.get("name", {}),
                uuid=form.get("uuid"),
                metrics={},
                dimensions={},
                completeness=has_interval
            )
            metrics = {}
            dimensions = {}

            definition = form.get("definition", {})
            if isinstance(definition, (string_types,)):
                definition = json.loads(definition)
            for key, value in definition.items():
                field_type = value.get("type", None)
                if field_type == "number":
                    metrics[key] = value
                if field_type in DIMENSIONS:
                    dimensions[key] = value
                if field_type == "matrix":
                    for row_key, row_value in value.get("fields", {}).items():
                        for cell_key, cell_value in row_value.get("fields", {}).items():
                            cell_type = cell_value.get("type", None)
                            if cell_type == "number":
                                metrics["%s~%s~%s" % (key, row_key, cell_key,)] = cell_value
                            elif cell_type in DIMENSIONS:
                                dimensions["%s~%s~%s" % (key, row_key, cell_key,)] = cell_value
                            else:
                                pass

            _form['metrics'] = metrics
            _form['dimensions'] = dimensions
            end_specs.append(_form)


        return dict(
            forms=end_specs,
            location_types=location_types,
            location_groups=location_groups,
            indicators=indicators,
            indicator_groups=indicator_groups
        )





