import json
import uuid
import datetime

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.core.notifications import send


class Notification:
    @staticmethod
    async def send(template, content, recipient, metadata, user=None):
        """ Send a notification
        
        Args:
            template: The template to use for the notification
            content: The content of the notification
            recipient: The recipient of the notification
            metadata: Additional information to attach to the notification
            user: The calling user

        Returns:

        """
        pass



    @staticmethod
    async def set_read(message_id, user=None):
        """ Set a message as read

        Args:
            message_id: The id of the mesage to mark as read
            user: The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.messages
                SET read = TRUE
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                message_id,
            ))

        return True

    @staticmethod
    async def delete(message_id, user=None):
        """ Delete a message

        Args:
            message_id: The id of the message to delete from the system
            user: The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.messages
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                message_id,
            ))

        return True

    @staticmethod
    async def get(message_id, user=None):
        """ Retrieve a notification from the system

        Args:
            message_id: The uuid of the message to retrieve
            user: The calling user, must be recipient of message

        Returns:
            The notification in question
        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT m.*
                (SELECT name from %s.users WHERE id = m.sender) AS sender_name,
                (SELECT email from %s.users WHERE id = m.sender) AS sender_email
                FROM %s.messages AS m
                WHERE m.uuid = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                message_id,
            ))
            result = await cur.fetchone()

        if result.get("recipient") != user.get("id"):
            return dict(
                err=True,
                msg="MESSAGE_NOT_FOUND"
            )

        return result
