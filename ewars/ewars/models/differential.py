from difflib import HtmlDiff

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Differential:
    @staticmethod
    def get_diff(original, amended, user=None):
        """ Get a track of what was changed

        Args:
            original: The original report in the system
            amended: The newly amended report

        Returns:
            A dict of keys containing tuples of (original value, amended value)
        """
        changes = {
            "data": {}
        }

        # Check root items
        if original.get("data_date") != amended.get("data_date"):
            changes["data_date"] = (original.get("data_date"), amended.get("data_date"))

        if original.get("location_id") != amended.get("location_id"):
            changes['location_id'] = (original.get("location_id"), amended.get("location_id"))

        for key, value in original['data'].items():
            if key in amended:
                if amended['data'][key] != value:
                    changes['data'][key] = (value, amended['data'][key])
            else:
                changes['data'][key] = (original['data'][key], None)

        for key, value in amended['data'].items():
            if key not in original['data'].keys():
                changes['data'][key] = (None, amended['data'][key])

        return changes

    @staticmethod
    def get_fields(object_set, parent_key, order_path):
        new_items = []

        count = 0
        for key, value in object_set.items():

            sub_key = "%s.%s" % (parent_key, key)
            sub_order = order_path + (count,)

            new_items.append((
                sub_order,
                sub_key,
                value.get("label", {}).get("en", "Un-labelled Field"),
                value,
            ))

            # Does this field have children of its own?
            if value.get("fields", None) is not None:
                child_items = Differential.get_fields(value.get("fields"), sub_key, sub_order)
                new_items = new_items + child_items

            count = count + 1

        return new_items

    @staticmethod
    async def get_differential(diff_id, user=None):
        diff_set = None
        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.v_amendments
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                diff_id,
            ))
            diff_set = await cur.fetchone()

            await cur.execute("""
                SELECT f.id, 
                    f.features, 
                    f.name, 
                    v.definition
                FROM %s.forms AS f 
                  LEFT JOIN %s.form_versions AS v ON v.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                diff_set.get("form_id"),
            ))
            form = await cur.fetchone()

        definition = form.get('definition', {})

        has_location = False
        location_features = form.get("features", {}).get("LOCATION_REPORTING", None)
        if location_features is not None:
            has_location = True

        # Add report date
        fields_ordered = [
            ((-2,), "data_date", "Report Date",)
        ]

        # Add location if applicable
        if has_location:
            fields_ordered.append(((-1,), "location_id", "Location",))

        ordered_fields = []
        for key, field in definition.items():
            label = "No label"

            if isinstance(field.get("label", None), (dict)):
                label = field.get("label", {}).get("en", "Un-labelled Field")
            else:
                label = field.get("label", "Un-labelled field")

            ordered_fields.append(
                (
                    ((int(field.get("order")),)),
                    key,
                    label,
                    field,
                )
            )

        ordered_fields = sorted(ordered_fields, key=lambda k: k[0][0])

        for item in ordered_fields:
            if item[3].get("fields", None) is not None:
                sub_fields = Differential.get_fields(item[3].get("fields"), item[1], item[0])
                ordered_fields = ordered_fields + sub_fields

        ordered_fields = sorted(ordered_fields, key=lambda k: k[0])

        original_diff = []
        amended_diff = []

        original_data = diff_set.get("original")
        amended_data = diff_set.get("amended")

        original_diff.append("Report Date: %s\n" % (original_data.get('data_date')))
        amended_diff.append("Report Date: %s\n" % (amended_data.get("data_date")))

        orig_form_data = original_data.get('data')
        amend_form_data = amended_data.get('data')
        for item in ordered_fields:
            label = item[2]

            len_key = len(item[1].split("."))
            spacer = ""

            if len_key > 1:
                for x in range(0, len_key):
                    spacer = spacer + "    "

            if item[3].get("type", "display") not in ("display", "header", "matrix", "row"):

                original_diff.append("%s%s: %s\n" % (spacer, label, orig_form_data.get(item[1], "Not Reported")))
                amended_diff.append("%s%s: %s\n" % (spacer, label, amend_form_data.get(item[1], "Not Reported")))
            else:
                original_diff.append("%s%s" % (spacer, label))
                amended_diff.append("%s%s" % (spacer, label))

        differ = HtmlDiff()
        html = differ.make_table(original_diff, amended_diff, "Original", "Revised")

        return html
