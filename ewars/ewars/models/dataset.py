import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Dataset:
    @staticmethod
    async def get_datasets(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.data_sets
                WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def create(data, user=None):
        """ Create a new data aset
        
        Args:
            data: 
            user: 

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.data_sets 
                (name, status, description, permissions, columns, date_field, ds_type, etl, created, created_by, last_modified, last_modified_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name", "Unnamed data set"),
                data.get("status", 'INACTIVE'),
                data.get("description", "No description provided"),
                json.dumps(data.get("permissions", {})),
                json.dumps(data.get("columns", {})),
                data.get('date_field', None),
                data.get('ds_type', "DEFAULT"),
                json.dumps(data.get("etl", {})),
                datetime.datetime.utcnow(),
                user.get("id"),
                datetime.datetime.utcnow(),
                user.get("id"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(ds_id, data, user=None):
        """ UPdate a data set in the system
        
        Args:
            ds_id: 
            data: 
            user: 

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.data_sets
                SET name = %s,
                  status = %s,
                  description = %s,
                  permissions = %s,
                  columns = %s,
                  date_field = %s,
                  ds_type = %s,
                  last_modified = %s,
                  last_modified_by = %s 
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("name", 'No Name'),
                data.get("status", "INACTIVE"),
                data.get("description", 'No description'),
                json.dumps(data.get("permissions", {})),
                json.dumps(data.get("columns", [])),
                data.get("date_field", None),
                data.get("ds_type", None),
                datetime.datetime.utcnow(),
                user.get("id"),
                ds_id,
            ))

        return True

    @staticmethod
    async def delete(ds_id, user=None):
        """ DElete a data set form the system
        
        Args:
            ds_id: 
            user: 

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.data_sets_data WHERE dsid = %s;
            """, (
                AsIs(user.get("tki")),
                ds_id,
            ))

            await cur.execute("""
                 DELETE FROM %s.data_sets WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                ds_id,
            ))

        return True

    @staticmethod
    async def get_keyed(ds_id, user=None):
        """ Return the data item for a keyed data set
        
        Args:
            ds_id: 
            user: 

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.data_sets_data
                WHERE dsid = %s;
            """, (
                AsIs(user.get("tki")),
                ds_id,
            ))
            result = await cur.fetchone()

        if result is None:
            result = dict(
                dsid=ds_id,
                data=dict()
            )

        return result

    @staticmethod
    async def remove_key(ds_id, key, user=None):
        """ Remove a key from a key/value store
        
        Args:
            ds_id: 
            key: 
            user: 

        Returns:

        """

        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.data_sets_data
                WHERE dsid = %s;
            """, (
                AsIs(user.get("tki")),
                ds_id,
            ))
            result = await cur.fetchone()

        if result is not None:
            data = result.get("data", {})
            if key in data.keys():
                del data[key]

            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.data_sets_data
                    SET data = %s,
                    last_modified = %s,
                    last_modified_by = %s
                  WHERE uuid = %s;
                """, (
                    AsIs(user.get("tki")),
                    json.dumps(data),
                    datetime.datetime.utcnow(),
                    user.get("id"),
                    result.get("uuid"),
                ))

        return True

    @staticmethod
    async def update_key(ds_id, key, value, user=None):
        """ Update a key in the key/value store
        
        Args:
            ds_id: 
            key: 
            value: 
            user: 

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.data_sets_data
                WHERE dsid = %s;
            """, (
                AsIs(user.get("tki")),
                ds_id,
            ))
            result = await cur.fetchone()

        if result is not None:
            data = result.get("data", {})
            if ":" not in key:
                data[key] = value
            else:
                del data[key.split(":")[0]]
                data[key.split(":")[1]] = value

            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.data_sets_data
                    SET data = %s,
                    last_modified = %s,
                    last_modified_by = %s
                  WHERE uuid = %s;
                """, (
                    AsIs(user.get("tki")),
                    json.dumps(data),
                    datetime.datetime.utcnow(),
                    user.get("id"),
                    result.get("uuid"),
                ))

        return True

    @staticmethod
    async def get_measures():
        return []

    @staticmethod
    async def get_dimensions():
        return []

    @staticmethod
    async def create_row(dsid, row, user=None):
        """ Create a new row in the data sets

        Args:
            dsid:
            row:
            user:

        Returns:

        """
        r_uuid = row.get('uuid')
        del row['uuid']
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.data_sets_data
                (uuid, dsid, data, created_by, last_modified_by) 
                VALUES (%s, %s, %s, %s, %s);
            """, (
                AsIs(user.get('tki')),
                r_uuid,
                dsid,
                json.dumps(row),
                user.get('id'),
                user.get('id'),
            ))

        return True

    @staticmethod
    async def update_row(row_id, data, user=None):
        """ Bulk update a row

        Args:
            row_id:
            data:
            user:

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.data_sets_data
                SET data = %s
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                json.dumps(data),
                row_id,
            ))

        return True

    @staticmethod
    async def delete_row(row_id, user=None):
        """ Delete a single row from a data set

        Args:
            row_id:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.data_sets_data
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                row_id,
            ))

        return True

    @staticmethod
    async def delete_rows(row_ids, user=None):
        """ Delete multiple rows from a data set

        Args:
            row_ids:
            user:

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.data_sets_data
                WHERE uuid = ANY(%s);
            """, (
                AsIs(user.get('tki')),
                tuple(row_ids),
            ))

        return True

    @staticmethod
    async def update_row_prop(row_id, prop, value, user=None):
        """ Update a single prop of a relational row

        Args:
            row_id:
            prop:
            value:
            user:

        Returns:

        """
        data = dict()

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT data from %s.data_sets_data WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                row_id,
            ))
            data = await cur.fetchone()
            data = data.get('data')

            data[prop] = value

            await cur.execute("""
                UPDATE %s.data_sets_data
                SET data = %s 
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                json.dumps(data),
                row_id,
            ))

        return True

    @staticmethod
    async def query(ds_id, limit, offset, sorting, filtering, user=None):
        """ Query a relational data set

        Args:
            ds_id:
            limit:
            offset:
            sorting:
            filtering:
            user:

        Returns:

        """
        if limit is None:
            limit = 60

        if offset is None:
            offset = 0

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.data_sets_data
                WHERE dsid = %s 
                LIMIT %s 
                OFFSET %s
            """, (
                AsIs(user.get('tki')),
                ds_id,
                limit,
                offset,
            ))
            results = await cur.fetchall()

        return results



