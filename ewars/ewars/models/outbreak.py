import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

GET_ACTIVE = """
    SELECT * FROM %s.outbreaks WHERE status = 'ACTIVE';
"""

OUTBREAK_CREATE = """
    INSERT INTO %s.outbreaks
    (title, description, status, start_date, end_date, locations, forms, created_by, modified_by)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
"""

OUTBREAK_UPDATE = """
    UPDATE %s.outbreaks
        SET title = %s,
            description = %s,
            status = %s,
            start_date = %s,
            end_date = %s,
            locations = %s,
            forms = %s,
            modified = NOW(),
            modified_by = %s
    WHERE uuid = %s RETURNING *;
"""

GET_BY_ID = """
    SELECT r.*,
        uu.name || ' (' || uu.email || ')' AS creator,
        uc.name || ' (' || uc.email || ')' AS modifier
    FROM %s.outbreaks AS r
        LEFT JOIN %s.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN %s.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = %s;
"""

DELETE_BY_ID = """
    DELETE FROM %s.outbreaks WHERE uuid = %s;
"""

class Outbreak(object):

    @staticmethod
    async def get_by_id(oid, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute(GET_BY_ID, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                oid,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        forms = [uuid.UUID(x) for x in data.get("forms", [])]
        locations = [uuid.UUID(x) for x in data.get("locations", [])]
        result = None
        async with get_db_cur(commit=True) as cur:
            await cur.execute(OUTBREAK_CREATE, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("description", None),
                data.get("status", 'INACTIVE'),
                data.get("start_date", None),
                data.get("end_date", None),
                locations,
                forms,
                user.get("uuid"),
                user.get("uuid"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(data, user=None):
        forms = [uuid.UUID(x) for x in data.get("forms", [])]
        locations = [uuid.UUID(x) for x in data.get("locations", [])]
        result = None
        async with get_db_cur(commit=True) as cur:
            await cur.execute(OUTBREAK_UPDATE, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("description"),
                data.get("status"),
                data.get("start_date"),
                data.get("end_date", None),
                locations,
                forms,
                user.get("uuid"),
                data.get("uuid"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(outbreak_id, user=None):
        async with get_db_cur(commit=True) as cur:
            await cur.execute(DELETE_BY_ID, (
                AsIs(user.get("tki")),
                oid,
            ))

        return True

    @staticmethod
    async def get_active_outbreaks(user=None):
        results = []
        async with get_db_cur() as cur:
            await cur.execute(GET_ACTIVE, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results




