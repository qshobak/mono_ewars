import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from .indicator import Indicator


class IndicatorGroup:
    @staticmethod
    async def update(group_id, data, user=None):
        """ Update and indicator group

        Args:
            group_id:
            data:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.indicator_groups
                SET name = %s,
                    parent_id = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name")),
                data.get("parent_id"),
                group_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.indicator_groups WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                group_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        """ Create a new indicator group

        Args:
            data:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.indicator_groups
                (name, parent_id)
                VALUES (%s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get('name')),
                data.get("parent_id", None)
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(group_id, user=None):
        """ Delete an indicator group, there are a few things that need to be done
        when an indicator is removed from the system, for instance removing any alarms and alerts
        configured based on that indicator

        Args:
            group_id:
            user:

        Returns:

        """

        child_groups = None
        indicators = None

        async with get_db_cur() as cur:
            await cur.execute("""
                WITH RECURSIVE sources AS (
                    SELECT id, ARRAY[]::INTEGER[] AS ancestors
                    FROM %s.indicator_groups WHERE parent_id IS NULL

                    UNION ALL

                    SELECT %s.indicator_groups.id, sources.ancestors || %s.indicator_groups.parent_id
                    FROM %s.indicator_groups, sources
                    WHERE %s.indicator_groups.parent_id = sources.id
                ) SELECT * FROM sources WHERE %s = ANY(sources.ancestors);
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                group_id,
            ))
            child_groups = await cur.fetchall()

            groups = [x.get('id') for x in child_groups]

            await cur.execute("""
                SELECT uuid
                FROM %s.indicators
                WHERE group_id = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                groups + [group_id],
            ))
            indicators = await cur.fetchall()

        for indicator in indicators:
            await Indicator._delete_indicator(indicator.get("uuid"), user=user)

        async with get_db_cur() as cur:
            # Delete child folders of this folder
            sorted_ids = sorted(groups, reverse=True)
            await cur.execute("""
                DELETE FROM %s.indicator_groups
                WHERE id = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                sorted_ids,
            ))

            # Delete the group
            await cur.execute("""
                DELETE FROM %s.indicator_groups WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                group_id,
            ))

        return True
