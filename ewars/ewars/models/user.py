import json
import uuid
import datetime

from ewars.db import get_db_cur
from ewars.constants import CONSTANTS, DEFAULT_NOTIFICATIONS
from ewars.utils.six import string_types
from ewars.utils import date_utils
from ewars.core import authentication, notifications

from psycopg2.extensions import AsIs
import tornado
import tornado.ioloop

from .location import Location
from .dashboard import Dashboard


class UserModel(object):

    __slots__ = [
            "id",
            'name',
            'email',
            'org_id',
            'org_name',
            'location_id',
            'role',
            'accounts',
            'password',
            'api_token',
            'tki'
    ]

    def __init__(self, user, tki=None):
        self.id = user
        self.tki = tki

    @classmethod
    def fromid(cls, uid, tki=None):
        pass

    def get_assignments(self):
        pass

    def get_profile(self):
        pass

    def get_location(self):
        """ Get information about the users assignment location

        """
        pass

    def get_teams(self):
        pass

    def update_password(self, new_password, password_confirm):
        pass

    def add_notification(self, data):
        pass

    def get_overdue_records(self):
        pass

    def get_upcoming_records(self):
        pass

    def get_activity(self):
        pass

    def get_notification_count(self):
        pass

    def get_task_count(self):
        pass

    def revoke_access(self):
        pass

    def update_status(self, new_status):
        pass





class User(object):
    def __init__(self):
        pass

    @staticmethod
    async def reinstate_account_access(email, user=None):
        """ Reinstante a users access to an account

        Args:
            uid:
            user:

        Returns:

        """
        acc_user = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name
                FROM %s.users
                WHERE email = %s;
            """, (
                AsIs(user.get("tki")),
                email,
            ))
            acc_user = await cur.fetchone()

            if acc_user is not None:
                await cur.execute("""
                    UPDATE %s.accounts
                    SET status = 'ACTIVE'
                    WHERE user_id = %s;
                """, (
                    AsIs(user.get("tki")),
                    acc_user.get("id"),
                ))

        return dict(
            err=False
        )

    @staticmethod
    async def get_profile(user_id, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.users
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            result = await cur.fetchone()

        # Remove password from output
        if result is not None \
                and "password" in result.keys():
            del result['password']

        return result

    @staticmethod
    async def stats(user_id, user=None):
        result = dict(
            submissions=0,
            assignments=0,
            completeness=0,
            timeliness=0
        )

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.collections
                WHERE status = 'SUBMITTED'
                    AND created_by = %s
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            result['submissions'] = await cur.fetchone()['total']

            await cur.execute("""
                SELECT COUNT(*) AS total FROM %s.assignments
                WHERE status = 'ACTIVE' AND user_id = %s
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            result['assignments'] = await cur.fetchone()['total']

        return result

    @staticmethod
    async def get_notebooks(user=None):
        """ Get a users notebooks

        Args:
            user: The calling user

        Returns:

        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.notebooks
                WHERE created_by = %s;
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def get_self(user=None):
        ''' Convenience function for a user to get their own data

        Args:
            user:

        Returns:

        '''
        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT * FROM %s.users
                WHERE id = %s;
            ''', (
                AsIs(user.get('tki')),
                user.get('id'),
            ))
            result = await cur.fetchone()

    @staticmethod
    async def get_activity(offset, user=None):
        ''' Legacy proxy to get-user activitty

        Args:
            offset:
            user:

        Returns:

        '''
        results = await User.get_user_activity(0, offset, user=user)
        return results

    @staticmethod
    async def get_activity_all(offset, user=None):
        results = None
        if user.get('role') == 'ACCOUNT_ADMIN':
            pass
        else:
            results = await User.get_user_activity(0, offset, user=user)

        return results


    @staticmethod
    async def get_profile_activity(user_id, limit, offset, user=None):
        return []

    @staticmethod
    async def get_user_activity(limit, offset, user=None):
        """ Get activity feed items dealing with a specific user

        Args:
            user_id: The id of the user to retrieve data for
            skip: The number of items to skip

        Returns:

        """
        results = []
        total = 0

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, u.name
                FROM %s.activity_feed AS a
                  LEFT JOIN _iw.users AS u ON u.id = a.triggered_by
                  WHERE a.triggered_by = %s
                ORDER BY a.created DESC
                LIMIT 10
                OFFSET %s;
            """, (
                AsIs(user.get("tki")),
                user.get('id'),
                offset,
            ))
            results = await cur.fetchall()

        for item in results:
            item_data = None
            if len(item.get("attachments")) > 0:
                item_type = item.get("attachments")[0][0]
                item_id = item.get("attachments")[0][1]

                if item_type == "REPORT":
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT f.name->>'en' AS form_name,
                                l.name->>'en' AS location_name,
                                c.data_date,
                                c.uuid,
                                f.features->'INTERVAL_REPORTING'->'interval' AS reporting_interval
                            FROM %s.collections AS c
                              LEFT JOIN %s.forms AS f ON f.id = c.form_id
                              LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                            WHERE c.uuid = %s;
                        """, (
                            AsIs(user.get("tki")),
                            AsIs(user.get("tki")),
                            AsIs(user.get("tki")),
                            item_id,
                        ))
                        item_data = await cur.fetchone()
                        if item_data is not None:
                            item_data['type'] = 'REPORT'

                if item_type == "USER":
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT u.name, u.role, o.name AS org_name
                            FROM %s.users AS u
                              LEFT JOIN %s.organizations AS o ON o.uuid = u.org_id
                            WHERE u.id = %s;
                        """, (
                            AsIs(user.get("tki")),
                            AsIs(user.get("tki")),
                            item_id,
                        ))
                        item_data = await cur.fetchone()
                        if item_data is not None:
                            item_data['type'] = 'USER'

            item['meta'] = item_data

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.activity_feed
                WHERE triggered_by = %s;
            """, (
                AsIs(user.get("tki")),
                user.get('id'),
            ))
            total = await cur.fetchone()
            total = total.get('total')

        return dict(
            activity=results,
            count=total
        )

    @staticmethod
    async def assignments(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*,
                l.name AS location_name,
                f.name AS form_name,
                f.uuid AS form_uuid,
                f.features AS form_features
                    FROM %s.assignments AS a
                    LEFT JOIN %s.locations AS l ON a.location_id = l.uuid
                    LEFT JOIN %s.forms AS f ON f.id = a.form_id
                WHERE a.user_id = %s
                    AND (l.uuid IS NULL OR (l.uuid IS NOT NULL AND a.location_id IS NOT NULL))
                    AND a.status IN ('ACTIVE', 'INACTIVE');
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            results = await cur.fetchall()

        # Organize the assignments grouping them together
        # by for id

        assigns = dict()
        assign_locs = dict()

        for assign in results:
            assign_locs[assign.get("form_id")] = []

            assigns[assign.get("form_id")] = dict(
                form_name=assign.get("form_name"),
                form_id=assign.get('form_id'),
                form_uuid=assign.get("form_uuid"),
                locations=[],
                features=assign.get("form_features", {})
            )

        for assign in results:
            if assign.get("type", "DEFAULT") == "DEFAULT":
                location_name = await Location.get_full_name(assign.get('location_id'), user=user)

                if assign.get("location_id") not in assign_locs[assign.get("form_id")]:
                    assign_locs[assign.get('form_id')].append(assign.get("location_id"))
                    assigns[assign.get("form_id")]['locations'].append(dict(
                        created=assign.get("created"),
                        created_by=assign.get("created_by"),
                        end_date=assign.get("end_date"),
                        form_uuid=assign.get("form_uuid"),
                        start_date=assign.get("start_date"),
                        location_id=assign.get("location_id", None),
                        name=(
                            assign.get("location_name", None),
                            location_name
                        ),
                        status=assign.get("status", None),
                        uuid=assign.get("uuid", None)
                    ))

            elif assign.get("type", "GROUP") == "GROUP":
                locations = []
                site_type_id = assign.get("form_features", {}).get("LOCATION_REPORTING", {}).get("site_type_id", None)
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT uuid, name
                        FROM %s.locations
                        WHERE groups::TEXT[] @> %s::TEXT[]
                        AND site_type_id = %s
                        AND status = 'ACTIVE';
                    """, (
                        AsIs(user.get("tki")),
                        [assign.get("definition", {}).get("group", None)],
                        site_type_id,
                    ))
                    locations = await cur.fetchall()

                for location in locations:
                    if location.get("uuid") not in assign_locs[assign.get("form_id")]:
                        assign_locs[assign.get('form_id')].append(location.get("uuid"))
                        location_name = await Location.get_full_name(location.get('uuid'), user=user)
                        assigns[assign.get("form_id")]['locations'].append(dict(
                            created=assign.get("created"),
                            created_by=assign.get("created_by"),
                            end_date=assign.get("end_date"),
                            start_date=assign.get("start_date"),
                            form_uuid=assign.get("form_uuid"),
                            location_id=location.get("uuid", None),
                            name=(
                                location.get("name", None),
                                location_name
                            ),
                            status=assign.get("status", None),
                            uuid=assign.get("uuid", None)
                        ))

            elif assign.get("type", None) == "UNDER":
                locations = []
                site_type_id = assign.get("form_features", {}).get("LOCATION_REPORTING", {}).get("site_type_id", None)
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT uuid, name
                        FROM %s.locations
                        WHERE site_type_id = %s
                        AND lineage::TEXT[] @> %s::TEXT[]
                        AND status = 'ACTIVE';
                    """, (
                        AsIs(user.get("tki")),
                        site_type_id,
                        [assign.get("location_id")],
                    ))
                    locations = await cur.fetchall()

                for location in locations:
                    if location.get("uuid") not in assign_locs[assign.get("form_id")]:
                        assign_locs[assign.get('form_id')].append(location.get("uuid"))
                        location_name = await Location.get_full_name(location.get('uuid'), user=user)
                        assigns[assign.get("form_id")]['locations'].append(dict(
                            created=assign.get("created"),
                            created_by=assign.get("created_by"),
                            end_date=assign.get("end_date"),
                            start_date=assign.get("start_date"),
                            form_uuid=assign.get("form_uuid"),
                            location_id=location.get("uuid", None),
                            name=(
                                location.get("name", None),
                                location_name
                            ),
                            status=assign.get("status", None),
                            uuid=assign.get("uuid", None)
                        ))

        return [x for x in assigns.values()]

    @staticmethod
    async def get_assignments(user_id, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                    a.uuid,
                    a.form_id,
                    a.location_id,
                    a.status,
                    a.type,
                    a.definition,
                    f.name as form_name,
                    f.features AS form_features,
                    l.name AS location_name,
                    rp.start_date AS start_date,
                    rp.end_date as end_date,
                    rp.status as rp_status
                FROM %s.assignments AS a
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.forms AS f ON f.id = a.form_id
                  LEFT JOIN %s.location_reporting AS rp ON rp.location_id = a.location_id AND rp.form_id = a.form_id
                 WHERE a.user_id = %s
                  AND a.status IN ('ACTIVE', 'INACTIVE')
                  AND f.id IS NOT NULL;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user_id,
            ))
            results = await cur.fetchall()

        for result in results:
            if result.get("location_id", None) is not None:
                result['l_full'] = await Location.get_full_name(result.get("location_id"), user=user)

        return results

    @staticmethod
    async def grant_access(user_id, data, user=None):
        """ Grant a user access to an account

        Args:
            user_id (int): The id of the user to add
            data (obj): The data specifying the users access
            user (obj): The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.accounts
                (user_id, aid, status, role, location_id, created_by)
                VALUES (%s, %s, %s, %s, %s, %s);
            """, (
                user_id,
                user.get("aid"),
                data.get("status"),
                data.get("role"),
                data.get("location_id"),
                user.get("id"),
            ))

        # Send a notification to the user that they've been granted access to an
        # account
        cur_loop = tornado.ioloop.IOLoop.current()
        cur_loop.spawn_callback(
            notifications.send,
            'ACCESS_GRANTED',
            user_id,
            dict(),
            account=user.get("account_name", None),
            user=user
        )

    @staticmethod
    async def revoke_access(user_id, user=None):
        """ Revoke a users access to an account

        Args:
            user_id: The id of the user
            user: THe user requesting the revoke

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.accounts
                SET status = 'REVOKED'
                WHERE user_id = %s;
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))

        return True

    @staticmethod
    async def reinstate_access(uid, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.accounts
                SET status = 'ACTIVE'
                WHERE user_id = %s;
            """, (
                AsIs(user.get("tki")),
                uid,
            ))

        return True


    @staticmethod
    async def clear_notifications(user=None):
        """ Clear all messages from a users inbox

        Args:
            user_id: The id of the user
            user: The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.messages
                WHERE recipient = %s
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
            ))

        return True

    @staticmethod
    async def get_unread_count(user=None):
        """ Get the total # of messages in the users inbox that are unread

        Args:
            user_id:

        Returns:

        """
        result = 0

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.messages
                WHERE recipient = %s
                AND read IS FALSE
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            result = await cur.fetchone()
            result = result.get('total')

        return result

    @staticmethod
    async def get_messages(offset=0, limit=10, user=None):
        """ Get a users messages

        Args:
            offset: The offset to query
            limit: The number of items to return
            user: The calling user

        Returns:
            A list of messages
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT m.*,
                (SELECT name FROM _iw.users WHERE id = m.sender) AS sender_name,
                (SELECT email FROM _iw.users WHERE id = m.sender) AS sender_email
                    FROM %s.messages AS m
                    WHERE recipient = %s
                        AND state = 'ON'
                    ORDER BY sent DESC
                    OFFSET %s
                    LIMIT %s
            """, (
                AsIs(user.get("tki")),
                user.get('id'),
                offset,
                limit,
            ))
            results = await cur.fetchall()

            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.messages
                WHERE recipient = %s
                  AND state = 'ON';
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
            ))
            total = await cur.fetchone()
            total = total.get('total')

        return dict(
            items=results,
            total=total
        )

    @staticmethod
    async def get_user_task_count(user=None):
        """ Get a total count of unactioned tasks alotted to a user

        Args:
            user: The calling user

        Returns:

        """
        task_count = 0

        if user.get("role") == CONSTANTS.SUPER_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE (assigned_user_id = %s OR assigned_user_types @> '{SUPER_ADMIN}')
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user.get('id'),
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        elif user.get("role") == CONSTANTS.GLOBAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE (assigned_user_id = %s OR assigned_user_types @> '{GLOBAL_ADMIN}')
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user['id'],
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        elif user.get("role") == CONSTANTS.INSTANCE_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE (assigned_user_id = %s OR assigned_user_types @> '{INSTANCE_ADMIN}')
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user['id'],
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        elif user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE (assigned_user_id = %s OR assigned_user_types @> '{ACCOUNT_ADMIN}')
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user['id'],
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        elif user.get("role") == CONSTANTS.REGIONAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(t.*) AS total
                    FROM %s.tasks_v2 AS t
                        LEFT JOIN %s.locations AS l ON l.uuid = t.location_id
                    WHERE (t.assigned_user_id = %s OR (t.assigned_user_types @> '{REGIONAL_ADMIN}' AND l.lineage::TEXT[] @> %s))
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user['id'],
                    [user['location_id']],
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        elif user.get("role") == CONSTANTS.USER:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE assigned_user_id = %s
                        AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user['id'],
                ))
                res = await cur.fetchone()
                task_count = res.get('total')

        return task_count

    @staticmethod
    async def get_unactioned_tasks(user_id, user=None):
        """ Retrieve a count of unactinoed tasks for a given user
        within a specific account

        Args:
            user_id: The id of the user to query

        Returns:

        """
        result = 0

        async with get_db_cur() as cur:
            if user.get("role") == "ACCOUNT_ADMIN":
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                    AND (assigned_user_id = %s OR assigned_user_id IS NULL)
                    AND assigned_user_types::TEXT[] @> %s::TEXT[]
                """, (
                    AsIs(user.get("tki")),
                    user_id,
                    ["ACCOUNT_ADMIN"],
                ))
                result = await cur.fetchone()
                result = result.get('total')

            elif user.get("role") == "REGIONAL_ADMIN":
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                    AND (assigned_user_id = %s OR assigned_user_id IS NULL)
                    AND assigned_user_types::TEXT[] @> %s::TEXT[]
                    AND location_id = %s
                """, (
                    AsIs(user.get("tki")),
                    user_id,
                    ["REGIONAL_ADMIN"],
                    user.get("location_id"),
                ))
                result = await cur.fetchone()
                result = result.get('total')

            elif user.get("role") == "USER":
                await cur.execute("""
                    SELECT COUNT(*) AS total
                    FROM %s.tasks_v2
                    WHERE assigned_user_id = %s
                    AND state = 'OPEN'
                """, (
                    AsIs(user.get("tki")),
                    user_id,
                ))
                result = await cur.fetchone()
                result = result.get('total')
            else:
                result = 0

        return result

    @staticmethod
    async def get_user_tasks(user=None):
        """
        Return a list of a users tasks
        :param user:
        :return:
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                    FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                        AND assigned_user_id = %s
            """, (
                AsIs(user.get("tki")),
                user['id'],
            ))
            results = await cur.fetchall()

        if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                        AND assigned_user_id IS NULL
                        AND assigned_user_types @> '{ACCOUNT_ADMIN}'
                """, (
                    AsIs(user.get("tki")),
                ))
                tasks = await cur.fetchall()
                results = results + tasks

        if user.get("role") == CONSTANTS.REGIONAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT t.*
                    FROM %s.tasks_v2 AS t
                        LEFT JOIN %s.locations AS l ON l.uuid = t.location_id
                    WHERE t.state = 'OPEN'
                        AND t.assigned_user_id IS NULL
                        AND t.assigned_user_types @> %s
                        AND l.lineage::TEXT[] @> %s::TEXT[]
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    ['REGIONAL_ADMIN'],
                    [user['location_id']],
                ))
                tasks = await cur.fetchall()
                results = results + tasks

        return results

    @staticmethod
    async def get_full_profile(user_id, user=None):
        """ Retrieve a users full profile data

        Args:
            user_id:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.users WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def patch(prop, value, user=None):
        """ Update and individual property of a users profile

        Args:
            user_id: The id of the user
            prop: The property to update
            value: The value to update it with
            user: The calling user

        Returns:

        """
        can_edit = False
        user_id = user.get("id")

        if user.get("role") in ("ACCOUNT_ADMIN", "GLOBAL_ADMIN", "SUPER_ADMIN"):
            can_edit = True

        if user.get("id") == user_id:
            can_edit = True

        if prop == "password":
            await User.update_password(user_id, value, user=user)
            return None

        if "." in prop:
            column = prop.split(".")[0]
            key = prop.split(".")[1]

            data = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT %s FROM _iw.users WHERE id = %s
                """, (
                    AsIs(column),
                    user_id,
                ))
                data = await cur.fetchone()
                data = data.get(column)

            data[key] = value

            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE _iw.users
                    SET %s = %s
                    WHERE id = %s
                """, (
                    AsIs(column),
                    json.dumps(data),
                    user_id,
                ))
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE _iw.users
                    SET %s = %s
                    WHERE id = %s
                """, (
                    AsIs(prop),
                    value,
                    user_id,
                ))

        return None

    @staticmethod
    async def update_password(user_id, new_password, user=None):
        """
        com.ewars.user.password.update
        :param user_id:
        :param new_password:
        :param user:
        :return:
        """

        edit_ok = False
        if user['role'] in (CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN, CONSTANTS.ACCOUNT_ADMIN):
            edit_ok = True

        if user['id'] == user_id:
            edit_ok = True

        if edit_ok == False:
            return dict(
                result=False,
                message=dict(
                    en="You do not have permissions to edit this user profile"
                )
            )

        editing_user = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM _iw.users WHERE id = %s
            """, (
                user_id,
            ))
            editing_user = await cur.fetchone()

        salt = authentication.generate_random_salt()
        password = authentication.generate_password(new_password, password_salt=salt)

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE _iw.users
                    SET password = %s
                    WHERE id = %s
            """, (
                password,
                user_id,
            ))

        return dict(result=True)

    @staticmethod
    async def update_profile():
        return None

    @staticmethod
    async def get_tasks(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                    FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                        AND assigned_user_id = %s
            """, (
                AsIs(user.get("tki")),
                user['id'],
            ))
            results = await cur.fetchall()

        if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.tasks_v2
                    WHERE state = 'OPEN'
                        AND assigned_user_id IS NULL
                        AND assigned_user_types @> '{ACCOUNT_ADMIN}'
                """, (
                    AsIs(user.get("tki")),
                ))
                tasks = await cur.fetchall()
                results = results + tasks

        if user.get("role") == CONSTANTS.REGIONAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT t.*
                    FROM %s.tasks_v2 AS t
                        LEFT JOIN %s.locations AS l ON l.uuid = t.location_id
                    WHERE t.state = 'OPEN'
                        AND t.assigned_user_id IS NULL
                        AND t.assigned_user_types @> %s
                        AND l.lineage::TEXT[] @> %s::TEXT[]
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    ['REGIONAL_ADMIN'],
                    [user['location_id']],
                ))
                tasks = await cur.fetchall()
                results = results + tasks

        return results

    @staticmethod
    async def get_upcoming_submissions(user=None):
        result = []
        forms = dict()
        assignments = await User.assignments(user=user)

        for assign in assignments:
            if "INTERVAL_REPORTING" in assign.get("features").keys():
                interval = assign['features']['INTERVAL_REPORTING'].get("interval")

                # Based on the interval, what's the next report thats due?
                next_interval = date_utils.get_end_of_current_interval(datetime.date.today(), interval)

                if assign.get("features", {}).get("LOCATION_REPORTING", None) is not None:
                    for loc in assign.get("locations", []):
                        result.append(dict(
                            form_id=assign.get('form_id'),
                            form_name=assign.get("form_name"),
                            location_id=loc.get("location_id"),
                            data_date=next_interval,
                            location_name=loc.get("name")[0],
                            lname=loc.get("name")[1],
                            interval=interval
                        ))
                else:
                    result.append(dict(
                        form_id=assign.get("form_id"),
                        form_name=assign.get("name"),
                        data_date=next_interval,
                        interval=interval
                    ))

        return result

    @staticmethod
    async def get_overdue_submissions(user=None):
        result = []
        forms = dict()
        assignments = await User.assignments(user=user)

        for assign in assignments:
            if "INTERVAL_REPORTING" in assign.get("features").keys() and "LOCATION_REPORTING" in assign.get("features").keys():
                interval = assign['features']['INTERVAL_REPORTING'].get("interval")

            if assign.get("features", {}).get("LOCATION_REPORTING", None) is not None:
                for loc in assign.get("locations", []):
                    loc_rp = None
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT * FROM %s.location_reporting
                            WHERE location_id = %s
                            AND form_id = %s
                            AND status = 'ACTIVE';
                        """, (
                            AsIs(user.get('tki')),
                            loc.get("location_id"),
                            assign.get('form_id'),
                        ))
                        loc_rp = await cur.fetchone()


                    if loc_rp is not None:
                        start_date = loc_rp.get("start_date")
                        end_date = loc_rp.get("end_date", datetime.date.today())
                    else:
                        start_date = "2017-01-01"
                        end_date = datetime.date.today()

                    if end_date is None:
                        end_date = datetime.date.today()

                    spec = date_utils.get_date_range(start_date, end_date, interval)

                    existing = []
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT data_date
                            FROM %s.collections
                            WHERE location_id = %s
                            AND form_id = %s
                            AND data_date >= %s
                            AND data_date <= %s
                            AND status = 'SUBMITTED';
                        """, (
                            AsIs(user.get("tki")),
                            loc.get("location_id"),
                            assign.get("form_id"),
                            start_date,
                            end_date,
                        ))
                        existing = await cur.fetchall()

                    existing = [x.get("data_date") for x in existing]
                    missing = [x for x in spec if x not in existing]
                    missing = [x for x in missing if x < end_date]

                    result.append(dict(
                        form_id=assign.get('form_id'),
                        lname=loc.get("name")[1],
                        name=assign.get("form_name"),
                        dates=missing,
                        interval=interval,
                        location_name=loc.get("name")[0],
                        location_id=loc.get("location_id")
                    ))

        return result

    @staticmethod
    async def update(user_id, data, user=None):
        """
        Update a users user record, you can not update a users password through this API, you need to use the seconday
        user password update API
        :param user_id: {int} Thd id of the user
        :param data: {dict} The data to update
        :param user: {dict} The calling uyser
        :return:
        """
        cap_user = None
        end_user = None

        async with get_db_cur() as cur:
            await cur.execute("""
             SELECT * FROM _iw.users
             WHERE id = %s;
            """, (
                user_id,
            ))
            cap_user = await cur.fetchone()

        profile = cap_user.get("profile", dict())
        if "bio" in data.keys():
            profile['bio'] = data.get("bio", None)

        if "occupation" in data.keys():
            profile['occupation'] = data.get("occupation", None)

        if "profile" in data.keys():
            profile = data.get("profile", dict())

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE _iw.users
                SET name = %s,
                  email = %s,
                  phone = %s,
                  org_id = %s,
                  profile = %s
                WHERE id = %s;
            """, (
                data.get("name"),
                data.get("email"),
                data.get("phone"),
                data.get("org_id"),
                json.dumps(profile),
                user_id,
            ))

            await cur.execute("""
                UPDATE %s.accounts
                SET role = %s,
                  location_id = %s,
                  status = %s
                WHERE user_id = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("role"),
                data.get("location_id", None),
                data.get("status"),
                user_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.users WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            end_user = await cur.fetchone()

        return end_user

    @staticmethod
    async def create_system(data, user=None):
        """ Create a new user in the system

        Args:
            data (obj:`str`): The data for the user
                email (str): Email address of the user to add
                name (str): The name of the user to add
                password (str): The desired password for the user
                phone (str): The users phoen number
                occupation (str): The user occupation
                role (str): The target role for the user in the account
                status (str): The users access status for the account
                location (str optional): If a REGIONAL_ADMIN, the location they're sandboxed to
                system (bool): Whether the user is account controlled
            user (obj:`str`):  The calling user

        Returns:
            Success message

        Raises:
            USER_SENT_INVITE: The email address belongs to a user that was sent an invite
            USER_EXISTS_IS_ACTIVE: The email address belongs to a user that exists and is active
            USER_EXISTS_IS_INACTIVE: The email address belongs to a user that exists but is set to inactive
            USER_EXISTS_SSO: A user within EWARS already exists with this user; prompt to invite
            USER_EXISTS_IS_REVOKED: The email address belongs to a user that exists but whose access is currently revoked from the account

        ToDo:
            * Send a notification to the user with their password informing they've been added

        """

        result = True
        invite = None

        if '@' in data.get('email', ''):
            return dict(
                err=True,
                code='INVALID_EMAIL'
            )

        data['email'] = '%s@ewars.ws' % (data.get('email'))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.invites
                WHERE email = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("email").lower().strip(),
            ))
            invite = await cur.fetchone()

        # There's a user with this email whose been invited already
        if invite is not None:
            return dict(
                err=True,
                code="USER_SENT_INVITE"
            )

        # Check if there's an existing user already
        acc_user = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, email, name, status
                FROM %s.users
                WHERE email = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("email").lower().strip(),
            ))
            acc_user = await cur.fetchone()

        # Check the status of the user
        if acc_user is not None:
            # User exists and is active
            if acc_user.get("status") == "ACTIVE":
                return dict(
                    err=True,
                    code="USER_EXISTS_IS_ACTIVE"
                )

            # User exists but is active; prompt to reactive
            if acc_user.get("status") == "INACTIVE":
                return dict(
                    err=True,
                    code="USER_EXISTS_IS_INACTIVE"
                )

            if acc_user.get("status") == "DELETED":
                return dict(
                    err=True,
                    code="USER_EXISTS_IS_REVOKED"
                )
        else:
            # No user, check if there's an root user
            root_user = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT id, name, email
                    FROM _iw.users
                    WHERE email = %s;
                """, (
                    data.get("email").lower().strip(),
                ))
                root_user = await cur.fetchone()

            # A root user with this email already exists; send them an invite
            if root_user is not None:
                return dict(
                    err=True,
                    code="USER_EXISTS_AT_ROOT"
                )
            else:

                # No user in the system, lets create one
                password_salt = authentication.generate_random_salt()
                password = authentication.generate_password(data.get("password"), password_salt=password_salt)

                result = None

                # Insert user record
                async with get_db_cur() as cur:
                    await cur.execute("""
                        INSERT INTO _iw.users
                        (uuid, status, group_id, account_id, email, password, name, phone, created, created_by, registered, profile, org_id, language, api_token, user_type,
                        location_id, notifications, timezone, date_format, time_format, pronoun, accessibility, email_format, desk_not_status, email_status, accounts, system)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;

                    """, (
                        str(uuid.uuid4()),
                        data.get("status", "INACTIVE"),
                        4,
                        data.get("aid"),
                        data.get("email").lower().strip(),
                        password,
                        data.get("name"),
                        data.get("phone"),
                        datetime.datetime.now(),
                        user.get("id"),
                        datetime.datetime.now(),
                        json.dumps(dict(
                            bio=data.get("bio", None),
                            occupation=data.get("occupation", None)
                        )),
                        data.get("org_id", None),
                        'en',
                        str(uuid.uuid4()),
                        data.get("role", "USER"),
                        data.get("location", None),
                        json.dumps(DEFAULT_NOTIFICATIONS),
                        'DEFAULT',
                        'DEFAULT',
                        'DEFAULT',
                        'DEFAULT',
                        'DEFAULT',
                        'DEFAULT',
                        'DISABLED',
                        'DEFAULT',
                        [user.get("aid")],
                        True,
                    ))
                    new_user = await cur.fetchone()

                    # INsert account record
                    await cur.execute("""
                        INSERT INTO %s.accounts
                        (user_id, aid, status, role, location_id, created_by)
                        VALUES (%s, %s, %s, %s, %s, %s);
                    """, (
                        AsIs(user.get("tki")),
                        new_user.get("id"),
                        user.get("aid"),
                        data.get('status'),
                        data.get("role"),
                        data.get("location"),
                        user.get("id"),
                    ))

                    await cur.execute("""
                        SELECT * FROM %s.users WHERE id = %s;
                    """, (
                        AsIs(user.get("tki")),
                        new_user.get("id"),
                    ))
                    result = await cur.fetchone()

                # Send notification to new users
                # cur_loop = tornado.ioloop.IOLoop.current()
                # cur_loop.spawn_callback(
                #     notifications.send,
                #     'WELCOME',
                #     new_user.get("id"),
                #     dict(
                #         email=result.get("email"),
                #         name=result.get("name"),
                #         account_name=result.get("account_name"),
                #         account_domain=user.get("account_domain", "ewars.ws"),
                #         password=data.get("password"),
                #         adder_name=user.get("name"),
                #         adder_email=user.get("email")
                #     ),
                #     user=user
                # )

                # Create activity feed item
                async with get_db_cur() as cur:
                    await cur.execute("""
                        INSERT INTO %s.activity_feed
                        (event_type, triggered_by, created, icon, attachments)
                        VALUES (%s, %s, %s, %s, %s);
                    """, (
                        AsIs(user.get("tki")),
                        "NEW_USER",
                        new_user.get("id"),
                        datetime.datetime.now(),
                        "fa-clipboard",
                        json.dumps([["USER", new_user.get("id")]])
                    ))

                    # TODO: Add an activity feed item for the user joingin

        return result

    @staticmethod
    async def get_locations(generation, user=None):
        locations = []

        if generation.get("location_spec") == "SPECIFIC":
            loc_id = generation.get("location_uuid", None)
            if loc_id is None:
                loc_id = generation.get("location", None)

            if loc_id is not None:
                async with get_db_cur() as cur:
                    await cur.execute("""
                            SELECT uuid, name FROM %s.locations
                            WHERE uuid = %s
                        """, (
                        AsIs(user.get("tki")),
                        generation.get("location_uuid"),
                    ))
                    locations = await cur.fetchall()

        elif generation.get("location_spec") == "TYPE":
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid, name
                            FROM %s.locations
                            WHERE lineage::TEXT[] @> %s
                                AND status = 'ACTIVE'
                    """, (
                    AsIs(user.get("tki")),
                    [user.get("clid")],
                ))
                locations = await cur.fetchall()

        return locations

    @staticmethod
    async def get_documents(user=None):
        """Retrieve latest documents that a user has access to

       Args:
           user:

       Returns:

       """
        results = []
        templates = []

        if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, template_name, instance_name, generation, status, template_type
                    FROM %s.templates
                    WHERE (status = 'ACTIVE' OR status = 'DRAFT');
                """, (
                    AsIs(user.get("tki")),
                ))
                templates = await cur.fetchall()
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, template_name, instance_name, generation, template_type
                        FROM %s.templates
                        WHERE status = 'ACTIVE';
                """, (
                    AsIs(user.get("tki")),
                ))
                templates = await cur.fetchall()

        for template in templates:
            locations = await User.get_locations(template.get("generation"), user=user)

            results.append(dict(
                template=template,
                locations=locations
            ))

        return results

    @staticmethod
    async def add_assignment(user_id, data, user=None):
        result = None

        # Check if an assignment already exists
        location_id = data.get('location_id', None)
        if location_id is None:
            location_id = data.get("location_uuid", None)

        count = 0
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.assignments
                WHERE form_id = %s
                    AND type = %s
                    AND location_id = %s
                    AND user_id = %s
            """, (
                AsIs(user.get("tki")),
                data.get("form_id"),
                data.get("type"),
                location_id,
                user_id,
            ))
            sub_count = await cur.fetchone()
            count = sub_count.get('total')

        if count > 0:
            return dict(
                err=True,
                error="DUPLICATE_ASSIGNMENT"
            )

        async with get_db_cur() as cur:
            await cur.execute("""
             INSERT INTO %s.assignments
             (user_id, created, created_by, last_modified, status, location_id, form_id, start_date, end_date, type, definition)
             VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                user_id,
                datetime.datetime.now(),
                user.get("id"),
                datetime.datetime.now(),
                data.get('status'),
                location_id,
                data.get("form_id", None),
                data.get("start_date", None),
                data.get("end_date", None),
                data.get("type", "DEFAULT"),
                json.dumps(data.get("definition", {})),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get_dashboard(dash_id, user=None):
        """ Retrieve a specific dashboard

        Args:
            dash_id: The id of the dashboard
            user: The calling user

        Returns:

        """

        result = None

        if isinstance(dash_id, (string_types,)):
            # This is a system default dashboard
            if "-" in dash_id:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT * FROM %s.layouts
                        WHERE uuid = %s
                    """, (
                        AsIs(user.get("tki")),
                        dash_id,
                    ))
                    result = await cur.fetchone()
            else:
                result = Dashboard.get_default_dashboard(dash_id, user=user)
        else:
            item_id = dash_id.get("uuid")
            if "-" in item_id:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT * FROM %s.layouts
                        WHERE uuid = %s
                    """, (
                        AsIs(user.get("tki")),
                        item_id,
                    ))
                    result = await cur.fetchone()
            else:
                result = Dashboard.get_default_dashboard(item_id, user=user)

        return result

    @staticmethod
    async def get_dashboards(user_id, user=None):
        """ Retrieve a list of dashboards for a user

        Args:
            user_id: The users id
            user: The calling user

        Returns:

        """

        result = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT dashboards->>'%s' AS dashboards
                FROM _iw.accounts
                WHERE id = %s
            """, (
                AsIs(user.get("role")),
                user.get("aid"),
            ))
            result = await cur.fetchone()
            result = result.get("dashboards", None)

        if result is None:
            return Dashboard.get_default_dashboards(user.get("role"))
        else:
            result = json.loads(result)

            for item in result:
                if isinstance(item, (list,)):
                    for index, sub in enumerate(item[2]):
                        async with get_db_cur() as cur:
                            await cur.execute("""
                                SELECT name
                                FROM %s.layouts
                                WHERE uuid = %s
                            """, (
                                AsIs(user.get("tki")),
                                sub[0],
                            ))
                            item[2][index] = dict(
                                uuid=sub[0],
                                name=await cur.fetchone()['name'],
                                order=sub[1]
                            )

                else:
                    if "-" in item.get("uuid"):
                        async with get_db_cur() as cur:
                            await cur.execute("""
                                SELECT name
                                FROM %s.layouts
                                WHERE uuid = %s
                            """, (
                                AsIs(user.get("tki")),
                                item.get("uuid"),
                            ))
                            dash = await cur.fetchone()
                            item['name'] = dash.get('name', None)

            return dict(
                dashboards=result,
                initial=await User.get_dashboard(result[0], user=user)
            )
