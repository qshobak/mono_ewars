import json
import datetime

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

ROLE_CREATE = """
    INSERT INTO %s.roles
    (uuid, name, status, description, inh, defaults, permissions, properties, created, created_by, modified, modified_by)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
"""

ROLE_UPDATE = """
    UPDATE %s.roles
        SET name = %s,
            status = %s,
            description = %s,
            inh = %s,
            defaults = %s,
            permissions = %s,
            properties = %s,
            modified = NOW(),
            modified_by = %s,
    WHERE uuid = %s;
"""

ROLE_GET_BY_ID = """
    SELECT r.*,
            uu.name || ' (' || uu.email || ')' AS creator,
            uc.name || ' (' || uc.email || ')' AS modifier
    FROM %s.roles AS r
        LEFT JOIN %s.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN %s.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = %s;
"""

ROLE_DELETE = """
    DELETE FROM %s.roles WHERE uuid = %s;
"""

DEGRADE_USERS = """
    UPDATE %s.accounts
    SET role = %s
    WHERE role = %s;
"""

class Role(object):

    @staticmethod
    async def get_by_id(rid, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute(ROLE_GET_BY_ID, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                rid,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        result = None
        async with get_db_cur(commit=True) as cur:
            await cur.execute(ROLE_CREATE, (
                AsIs(user.get("tki")),
                data.get("name"),
                data.get("status"),
                data.get("description", None),
                data.get("inh", "USER"),
                json.dumps(data.get("defaults", {})),
                json.dumps(data.get("properties", {})),
                user.get("uuid"),
                user.get("uuid"),
            ))
            result = await cur.fetchone()

        return result


    @staticmethod
    async def update(data, user=None):
        async with get_db_cur() as cur:
            await cur.execute(ROLE_UPDATE, (
                AsIs(user.get("tki")),
                data.get("name"),
                data.get("status"),
                data.get("description", None),
                data.get("inh", "USER"),
                json.dumps(data.get("defaults", {})),
                json.dumps(data.get("properties", {})),
                user.get("uuid"),
                data.get("uuid"),
            ))

        return True

    @staticmethod
    async def delete(rid, user=None):
        # get the role
        role = await Role.get_by_id(rid, user=user)

        async with get_db_cur(commit=True) as cur:
            # Downgrade users of this role to the inherited role
            await cur.execute(DOWNGRADE_USERS, (
                AsIs(user.get("tki")),
                role.get("inh", "USER"),
                str(role.get("uuid")),
            ))

            # Delete the role from the system
            await cur.execute(ROLE_DELETE, (
                AsIs(user.get("uuid")),
                rid,
            ))

        return True
