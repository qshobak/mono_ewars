import json
import time

from ewars.db import get_db_cur
from ewars.db import get_db_cursor
from ewars.core.serializers import JSONEncoder

import tornado.ioloop
from psycopg2.extensions import AsIs


class EventLog:
    def __init__(self):
        pass

    @staticmethod
    async def add_event(event_type, origin_uid, origin_did, metadata, data, ts=None, user=None):
        """

            ret = await EventLog.add_event(
            'RES_NEW',
            user.get('id'),
            user.get('did', None),
            dict(
                lid=report.get('location_id', None)
            ),
            dict(report),
            user=user
        )

        Args:
            event_type:
            ts:
            origin_uid:
            origin_did:
            metadata:
            data:
            user:

        Returns:

        """
        event_ts = ts
        if ts is None:
            event_ts = int(time.time())

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.event_log (event_type, data, ts, origin_uid, origin_did, metadata)
                VALUES (%s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get('tki')),
                event_type,
                json.dumps(data, cls=JSONEncoder),
                event_ts,
                origin_uid,
                origin_did,
                json.dumps(metadata or dict(), cls=JSONEncoder),
            ))

        return None

    @staticmethod
    def add_event_sync(event_type, origin_uid, origin_did, metadata, data, ts=None, user=None):
        event_ts = ts
        if ts is None:
            event_ts = int(time.time())

        with get_db_cursor() as cur:
            cur.execute("""
                INSERT INTO %s.event_log (event_type, data, ts, origin_uid, origin_did, metadata)
                VALUES (%s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get('tki')),
                event_type,
                json.dumps(data, cls=JSONEncoder),
                event_ts,
                origin_uid,
                origin_did,
                json.dumps(metadata or dict(), cls=JSONEncoder),
            ))

        return None

    @staticmethod
    async def get_highest_ts(user=None):
        """ Get the highest ts available for a given user

        Args:
            user:

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT MAX(ts)  AS maximum
                FROM %s.event_log
                WHERE origin_uid = %s
                  AND origin_did = %s;
            """, (
                AsIs(user.get('tki')),
                user.get('id'),
                user.get('did'),
            ))
            result = await cur.fetchone()
            if result is not None:
                result = result.get('maximum')
            else:
                result = 0

        return result
