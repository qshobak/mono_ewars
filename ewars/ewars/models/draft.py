import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Draft:
    @staticmethod
    async def delete(report_uuid, user=None):
        """ Delete a draft report form the system

        Args:
            report_uuid:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                report_uuid,
            ))

        return True

    @staticmethod
    async def create(report_uuid, data, user=None):
        """ Update a drafts data within the system

        Args:
            report_uuid: The UUID of the report
            data: The data to insert
            user: The calling user

        Returns:
            The updated report
        """

        result = None

        async with get_db_cur() as cur:
            if report_uuid is not None:
                await cur.execute("""
                    UPDATE %s.collections
                    SET data_date = %s,
                    location_id = %s,
                    data = %s
                    WHERE uuid = %s;
                """, (
                    AsIs(user.get("tki")),
                    data.get('data_date'),
                    data.get("location_id", None),
                    json.dumps(data.get('data')),
                    report_uuid,
                ))

                await cur.execute("""
                    SELECT *
                    FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    report_uuid,
                ))
                result = await cur.fetchone()

            else:
                form = None
                await cur.execute("""
                    SELECT id, name, version_id
                    FROM %s.forms
                    WHERE id = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get('form_id'),
                ))
                form = await cur.fetchone()

                await cur.execute("""
                    INSERT INTO %s.collections
                    (form_id, created_by, created, last_modified, status, data, form_version_id, data_date, location_id, source)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
                """, (
                    AsIs(user.get("tki")),
                    form.get('id'),
                    user.get("id"),
                    datetime.datetime.now(),
                    datetime.datetime.now(),
                    'DRAFT',
                    json.dumps(data.get('data')),
                    form.get("version_id"),
                    data.get('data_date'),
                    data.get("location_id"),
                    "SYSTEM",
                ))
                result = await cur.fetchone()

        return result
