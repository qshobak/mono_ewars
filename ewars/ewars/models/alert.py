import datetime
import json
import uuid
import asyncio

from ewars.constants import CONSTANTS
from ewars.utils.six import string_types
from ewars.db import get_db_cur
from ewars.utils import date_utils
from .location import Location
from ewars.core import notifications, user_lookup
from ewars.analysis.analysis_rest_parser import analysis_parser
from ewars.analysis import immediate, comparators
from ewars.models.event_log import EventLog
from ewars.models.indicator import Indicator
from ewars.models.location_group import LocationGroup
from ewars.core.serializers import JSONEncoder
from ewars.models import User


from psycopg2.extensions import AsIs

import tornado
import tornado.ioloop

COMPARATORS = {
    "eq": "=",
    "neq": "!=",
    "gt": ">",
    "gte": ">=",
    "lt": "<",
    "lte": "<=",
    "in": None
}

CAST_TYPES = dict(
    number="NUMERIC",
    date="DATE",
    select="TEXT"
)

PROGRESSION = {
    "VERIFICATION": "RISK_ASSESS",
    "RISK_ASSESS": "RISK_CHAR",
    "RISK_CHAR": "OUTCOME"
}

RISK_LEVELS = dict(
    LOW='Low Risk',
    MODERATE='Moderate Risk',
    HIGH='High Risk',
    SEVERE='Very High Risk'
)

OUTCOMES = dict(
    MONITOR="Monitor",
    RESPOND="Respond",
    DISCARD="Discard",
    ADVANCE="Start risk assessment"
)


class Alert:

    @staticmethod
    async def query(filter, limit, offset, lt=None, user=None):
        """Retrieve alerts that a user has access to

        Args:
           filter: Any filtering applied to the alerts
           limit: The number of alerts to return
           offset: The offset for pagination
           user: The calling user

        Returns:

        """
        results = []
        alerts = []
        location_ids = []

        indicator = None
        if filter == "OPEN":
            indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_OPEN")
        else:
            indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_CLOSED")

        if user['role'] == CONSTANTS.ACCOUNT_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.ds_interval AS interval_type
                        FROM %s.alerts AS a
                            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                            LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                        WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                            AND a.state = '%s'
                """ % (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get("clid"),
                    filter,
                ))
                alerts = await cur.fetchall()

        elif user.get("role") == CONSTANTS.REGIONAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.ds_interval AS interval_type
                        FROM %s.alerts AS a
                            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                            LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                        WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                            AND a.state = '%s'
                """ % (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get("clid"),
                    filter,
                ))
                alerts = await cur.fetchall()

        elif user['role'] == CONSTANTS.USER:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.ds_interval AS interval_type
                        FROM %s.alerts AS a
                            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                            LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                            LEFT JOIN %s.alert_users AS au ON au.alert_id = a.uuid
                        WHERE au.user_id = %s
                            AND a.state = %s
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get('id'),
                    filter,
                ))
                alerts = await cur.fetchall()

        location_ids = [x['location_id'] for x in alerts]
        confinement = None
        if user['role'] not in (CONSTANTS.SUPER_ADMIN, CONSTANTS.INSTANCE_ADMIN, CONSTANTS.GLOBAL_ADMIN):
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT ST_AsGeoJSON(default_center) AS center, default_zoom
                    FROM %s.locations
                    WHERE uuid::TEXT = %s
                """, (
                    AsIs(user.get("tki")),
                    user.get("clid"),
                ))
                confinement = await cur.fetchone()
                if confinement.get("center", None) is not None:
                    confinement['center'] = json.loads(confinement['center'])

        if len(alerts) <= 0:
            return dict(
                confinement=confinement,
                results=[]
            )

        locations = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                  uuid::TEXT,
                  name,
                  geometry_type,
                  ST_AsGeoJSON(ST_Centroid(point)) as centroid,
                  ST_AsGeoJSON(ST_Extent(point)) as extent
                  FROM %s.locations
                  WHERE uuid IN %s
                  GROUP BY uuid
            """, (
                AsIs(user.get("tki")),
                tuple(location_ids),
            ))
            locations = await cur.fetchall()

        locs = dict((loc['uuid'], loc) for loc in locations)

        for alert in alerts:
            results.append(dict(
                alert=alert,
                location=locs[str(alert.get("location_id"))]
            ))

        return dict(
            confinement=confinement,
            results=results
        )

    @staticmethod
    async def retrieve_alerts(filter, limit, offset, lt=None, user=None):
        """Retrieve alerts that a user has access to

        Args:
            filter: Any filtering applied to the alerts
            limit: The number of alerts to return
            offset: The offset for pagination
            user: The calling user

        Returns:

        """
        results = []
        alerts = []
        location_ids = []

        indicator = None
        if filter == "OPEN":
            indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_OPEN")
        else:
            indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_CLOSED")

        if user['role'] == CONSTANTS.ACCOUNT_ADMIN:
            results = await analysis_parser(dict(
                indicator=indicator,
                location=dict(
                    parent_id=user.get("clid"),
                    site_type_id=lt or 10,
                    status='ACTIVE'
                ),
                interval="DAY",
                start_date="2011-01-01",
                end_date=datetime.date.today(),
                reduction="SUM",
                type="SLICE",
                geometry=True
            ), user=user)

            results['d'] = [x for x in results.get('d') if x.get("data", 0) > 0]
        elif user['role'] == CONSTANTS.REGIONAL_ADMIN:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT 
                      a.*, 
                      l.name AS location_name, 
                      al.name AS alarm_name, 
                      al.date_spec_interval_type AS interval_type,
                      l.geojson AS geojson
                        FROM %s.alerts AS a
                            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                            LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                        WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                            AND a.state = '%s'
                """ % (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get("location_id"),
                    filter,
                ))
                alerts = await cur.fetchall()
                results = dict(d=alerts)

            location_ids = [x['location_id'] for x in alerts]
        elif user['role'] == CONSTANTS.USER:
            assignments = await User.assignments(user=user)
            loc_ids = []
            for assign in assignments:
                for loc in assign.get("locations", []):
                    loc_ids.append(loc.get("uuid"))

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT 
                      a.*, 
                      l.name AS location_name, 
                      al.name AS alarm_name, 
                      al.ds_interval AS interval_type,
                      l.geojson AS geojson
                        FROM %s.alerts AS a
                            LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                            LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                        WHERE a.location_id = ANY(%s)
                            AND a.state = %s
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    loc_ids,
                    filter,
                ))
                alerts = await cur.fetchall()
                results = dict(d=alerts)

            location_ids = [x['location_id'] for x in alerts]

        confinement = None
        if user['role'] not in (CONSTANTS.SUPER_ADMIN, CONSTANTS.INSTANCE_ADMIN, CONSTANTS.GLOBAL_ADMIN):
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT ST_AsGeoJSON(default_center) AS center, default_zoom
                    FROM %s.locations
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    user.get("clid"),
                ))
                confinement = await cur.fetchone()
                if confinement.get("center", None) is not None:
                    confinement['center'] = json.loads(confinement['center'])

        return results

    @staticmethod
    async def get_activity(alert_id, user=None):
        """ Retrieve alert activity for a given alert
        :param alert_id: {str} The UUID of the alert
        :param user: {dict} The calling userj
        :return:
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, u.name AS user_name, u.email AS user_email
                FROM %s.alert_events AS a
                LEFT JOIN _iw.users AS u ON u.id = a.user_id
                WHERE a.alert_id = %s
                ORDER BY a.action_date DESC
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def add_activity(alert_id, action_type, icon, user_id, content, user=None):
        """ Add an alert activity item
        :param alert_id: {uuid} The UUID of the alert
        :param action_type: {str} The action type
        :param icon: {str} The icon to use for the alert
        :param user_id: {int} The user that triggered the activity
        :param content: {str} The content to display alongside the feed item (if applicable)
        :param user: {dict} The calling user
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alert_events
                (action_date, action_type, icon, node_type, user_id, content, alert_id)
                VALUES
                (%s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.now(),
                action_type,
                icon,
                CONSTANTS.ACTION,
                user_id,
                content,
                alert_id,
            ))

    @staticmethod
    async def add_event(alert_id, action_type, icon, user=None):
        """ Add a non-user related event to the alerts events
        :param alert_id: {str} The uuid of the alert
        :param action_type: {str} The action type
        :param icon: {str} The icon tp use
        :param user: {dict} The calling user
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alert_events
                (action_date, action_type, icon, node_type, alert_id)
                VALUES
                (%s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.now(),
                action_type,
                icon,
                CONSTANTS.EVENT,
                alert_id,
            ))

            # TODO: Get alert and all it's events to add to alert topic

    @staticmethod
    async def comment(alert_uuid, data, user=None):
        """ Add a comment to an alert activity feed.
        :param alert_uuid: {str} THe UUID of the alert
        :param data: {dict} Data associated with the commetn
        :param user: {dict} The calling user
        :return:
        """

        alert = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alert_events (action_date, action_type, icon, node_type, user_id, content, alert_id)
                VALUES (%s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.now(),
                CONSTANTS.COMMENT,
                'fa-comment',
                CONSTANTS.COMMENT,
                data.get('user_id'),
                data.get("content"),
                alert_uuid,
            ))

            await cur.execute("""
                SELECT * FROM %s.alerts_full WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
            ))
            alert = await cur.fetchone()

        cur_loop = asyncio.get_event_loop()
        cur_loop.create_task(
            Alert.notify_comment(
                alert,
                data,
                user=user
            )
        )

        return True

    @staticmethod
    async def notify_comment(alert, data, user=None):
        stakeholders = await Alert.get_stakeholders(alert.get("uuid"), user=user)

        notification_data = dict(
            alert_uuid=alert.get("uuid"),
            commenter=user.get("name"),
            alarm_name=alert.get("alarm_name", "Unnamed alert"),
            location_name=alert['location_name'].get("en", None),
            alert_date=date_utils.display(alert.get("trigger_end"), alert.get("interval")),
            content=data.get("content", None),
        )

        cur_loop = asyncio.get_event_loop()
        for stakeholder in stakeholders:
            cur_loop.create_task(
                notifications.send(
                    "ALERT_COMMENT",
                    stakeholder,
                    notification_data,
                    account=user.get("account_name", None),
                    user=user
                )
            )
        return True

    @staticmethod
    async def resend_alert_notification(alert_id, user=None):
        """ Resend the alert notification email for an already raised alert
        :param alert_id: {str} The UUID of the alert
        :param user: {dict} The calling user
        :return:
        """
        alert = None
        submitter = None
        alarm = None
        users = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, al.name AS alarm_name, l.uuid AS location_uuid, l.name AS location_name, al.date_spec_interval_type
                FROM %s.alerts AS a
                    LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE a.uuid = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert_id,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT u.name AS user_name,
                    c.data_date,
                    c.created_by,
                    u.email,
                    l.name AS location_name,
                    f.name AS form_name,
                    f.time_interval AS form_interval
                FROM %s.collections AS c
                    LEFT JOIN _iw.users AS u ON c.created_by = u.id
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                    LEFT JOIN %s.forms AS f ON f.id = c.form_id
                WHERE c.uuid = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert.get("triggering_report_id"),
            ))
            submitter = await cur.fetchone()

            await cur.execute("""
                SELECT u.id, u.user_type, u.name, u.email, u.bounced
                FROM %s.alert_users AS au
                    LEFT JOIN _iw.users AS u ON u.id = au.user_id
                WHERE au.alert_id = %s
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))
            users = await cur.fetchall()

        cur_loop = tornado.ioloop.IOLoop.current()
        if alert.get("state") == CONSTANTS.ACTIVE:
            for admin in users:
                if admin.get("id") != submitter.get("created_by"):
                    if admin.get("user_type") in (
                            CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN, CONSTANTS.REGIONAL_ADMIN,
                            CONSTANTS.ACCOUNT_ADMIN,
                            CONSTANTS.INSTANCE_ADMIN):
                        cur_loop.create_task(
                            notifications.send(
                                'ALERT_RAISED_ADMIN',
                                admin,
                                dict(
                                    form_name=submitter['form_name'].get("en", None),
                                    submitter=submitter.get("user_name"),
                                    alarm_name=alert['alarm_name'].get("en", None),
                                    alert_uuid=alert.get("uuid"),
                                    alert_location=alert['location_name'].get("en"),
                                    trigger_period=date_utils.display(alert.get("trigger_end"),
                                                                      alert.get("date_spec_interval_type"))
                                ),
                                account=user.get('account_name', None),
                                user=user
                            )
                        )
                elif admin.get("id") == submitter.get("created_by"):
                    cur_loop.create_task(
                        notifications.send(
                            'ALERT_RAISED_SUBMITTED',
                            submitter,
                            dict(
                                form_name=submitter['form_name'].get("en", None),
                                submitter=submitter.get("user_name"),
                                alarm_name=alert['alarm_name'].get("en", None),
                                alert_uuid=alert.get("uuid"),
                                alert_location=alert['location_name'].get("en"),
                                trigger_period=date_utils.display(alert.get("trigger_end"),
                                                                  alert.get("date_spec_interval_type")),
                                report_date=date_utils.display(submitter.get("data_date"),
                                                               submitter.get("form_interval"))
                            ),
                            account_name=user.get('account_name', None),
                            user=user
                        )
                    )

        else:
            return dict(
                success=True
            )

    @staticmethod
    async def get_all_assigned_users(form_id, location_id, user=None):
        """Get all users who are assigned to a specific location
        for a specific form
        :param: form_id: {int} THe id of the form
        :param: location_id: {str} The UUID of the location
        :param: user: {dict} The calling user
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT u.id, u.name, u.email
                FROM %s.users AS u
                    LEFT JOIN %s.assignments AS a ON a.user_id = u.id
                WHERE a.location_id = %s
                    AND a.form_id = %s
                    AND a.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
                form_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def get_notifiable_users(alert_id, user=None):
        """Get the ids of all users that should be notified of a change in an alert"""
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT au.user_id AS id
                FROM %s.alert_users AS au
                    LEFT JOIN %s.users AS u ON u.id = au.user_id
                WHERE au.alert_id = %s
                    AND u.status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert_id,
            ))
            results = await cur.fetchall()

        return [x.get("id") for x in results]

    @staticmethod
    async def get_stakeholders(alert_id, user=None):
        """ Get stakeholders for an alert
        
        Args:
            alert_id: 
            user: 

        Returns:

        """
        users = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT lineage, location_id, triggering_report_id FROM %s.alerts_full WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))
            alert = await cur.fetchone()

            report = None
            if alert.get("triggering_report_id", None) is not None:
                await cur.execute("""
                    SELECT form_id
                    FROM %s.collections
                    WHERE uuid = %s;
                """, (
                    AsIs(user.get("tki")),
                    alert.get("triggering_report_id"),
                ))
                report = await cur.fetchone()

            if alert is not None:
                if report is not None:
                    await cur.execute("""
                        SELECT u.id,
                            u.name,
                            u.email,
                            u.role,
                            u.profile,
                            o.name AS organization_name,
                            l.name AS location_name,
                            lt.name AS location_type
                        FROM %s.users AS u
                            LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                            LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                            LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                            LEFT JOIN _iw.laboratories AS lab ON lab.uuid = u.lab_id
                            LEFT JOIN %s.assignments AS ass ON ass.user_id = u.id
                        WHERE u.status = 'ACTIVE'
                            AND 
                              (
                                (u.role = 'REGIONAL_ADMIN' AND u.location_id IN %s) 
                                OR (u.role = 'ACCOUNT_ADMIN') 
                                OR (u.role = 'USER' AND ass.location_id = %s AND ass.status = 'ACTIVE' AND ass.form_id = %s)
                                );
                    """, (
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        tuple(alert.get("lineage")),
                        alert.get("location_id"),
                        report.get("form_id"),
                    ))
                    users = await cur.fetchall()
                else:
                    await cur.execute("""
                        SELECT u.id,
                            u.name,
                            u.email,
                            u.role,
                            u.profile,
                            o.name AS organization_name,
                            l.name AS location_name,
                            lt.name AS location_type
                        FROM %s.users AS u
                            LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                            LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                            LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                            LEFT JOIN _iw.laboratories AS lab ON lab.uuid = u.lab_id
                            LEFT JOIN %s.assignments AS ass ON ass.user_id = u.id
                        WHERE u.status = 'ACTIVE'
                            AND 
                              (
                                (u.role = 'REGIONAL_ADMIN' AND u.location_id IN %s) 
                                OR (u.role = 'ACCOUNT_ADMIN') 
                                OR (u.role = 'USER' AND ass.location_id = %s AND ass.status = 'ACTIVE')
                                );
                    """, (
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        tuple(alert.get("lineage")),
                        alert.get("location_id"),
                    ))
                    users = await cur.fetchall()

        concrete = dict((x.get("id"), x) for x in users)
        users = list(concrete.values())

        return users

    @staticmethod
    async def get_alert_users(alert_id, user=None):
        """ Retrieve information about the users assigned to an alert
        :param alert_id: {str} The UUID of the report
        :param user: {dict} The calling user
        :return:
        """
        results = []

        alert = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alerts_full
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT u.id,
                    u.name,
                    u.email,
                    u.role,
                    u.profile,
                    o.name AS organization_name,
                    l.name AS location_name,
                    lt.name AS location_type
                FROM %s.users AS u
                    LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                    LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                    LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                    LEFT JOIN _iw.laboratories AS lab ON lab.uuid = u.lab_id
                    LEFT JOIN %s.assignments AS ass ON ass.user_id = u.id
                WHERE u.status = 'ACTIVE'
                    AND 
                      (
                        (u.role = 'REGIONAL_ADMIN' AND u.location_id IN %s) 
                        OR (u.role = 'ACCOUNT_ADMIN') 
                        OR (u.role = 'USER' AND ass.location_id = %s AND ass.status = 'ACTIVE')
                        );
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                tuple(alert.get("lineage")),
                alert.get("location_id"),
            ))
            results = await cur.fetchall()

        concrete = dict((x.get("id"), x) for x in results)
        results = list(concrete.values())

        return results

    @staticmethod
    async def _set_stage_active(alert_id, stage, user=None):
        """ Sets whatever the current alert stage is at to state ACTIVE
        :param alert_id: {str} The UUID of the alert
        :param stage: {str} The stage of the alert
        :param user: {dict} The calling user
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alerts
                SET stage_stage = 'ACTIVE'
                WHERE uuid =%s
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))

        return True

    @staticmethod
    async def create_action(data, user=None):
        """ Create the action item
        :param action_id: {int} The id of the action
        :param data: {dict} The action data
        :param user: {dict} The calling user
        :return:
        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alert_actions (alert_id, user_id, type, status, data, last_modified, created)
                VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("alert_id"),
                user.get("id"),
                data.get("type"),
                data.get("status", CONSTANTS.DRAFT),
                json.dumps(data.get("data")),
                datetime.datetime.now(),
                datetime.datetime.now(),
            ))
            result = await cur.fetchone()

        #TODO: Generate event

        return result

    @staticmethod
    async def update_action(action_id, data, user=None):
        """ Update an alert action item
        :param action_id: {int} The action id
        :param data: {dict} The action data
        :param user: {dict} The calling user
        :return:
        """
        result = None

        if data.get("status") == CONSTANTS.DRAFT_OPEN:
            data['status'] = CONSTANTS.DRAFT

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alert_actions
                SET status = %s,
                    data = %s,
                    last_modified = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                data.get("status", CONSTANTS.DRAFT),
                json.dumps(data.get("data")),
                datetime.datetime.now(),
                action_id,
            ))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alert_actions WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                action_id,
            ))
            result = await cur.fetchone()

        #TODO: Generate event

        return result

    @staticmethod
    async def submit_action(alert_id, action_id, data, user=None):
        """ Submit an alert action, alters the state of the alert

        Args:
            alert_id: The UUID of the alert
            action_id: The action_id if it exists
            data: Any data associated with the update
            user: The calling user

        Returns:

        """
        alert = None


        account = None

        if action_id is None:
            data = await Alert.create_action(data, user=user)
            action_id = data.get("id")

        data['status'] = 'SUBMITTED'
        await Alert.update_action(action_id, data, user=user)

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM _iw.accounts
                WHERE tki = %s;
            """, (
                user.get("tki"),
            ))
            account = await cur.fetchone()

            await cur.execute("""
                SELECT a.*,
                    al.name AS alarm_name,
                    al.ds_interval AS interval,
                    l.name AS location_name
                FROM %s.alerts AS a
                    LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE a.uuid = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert_id,
            ))
            alert = await cur.fetchone()

        # Workflow logic
        current_stage = alert.get("stage", None)
        next_stage = None

        # State variables
        risk = alert.get("risk", None)
        risk_cell = None
        alert_outcome = alert.get("outcome", None)
        state = alert.get("state", CONSTANTS.OPEN)
        stage = alert.get("stage", CONSTANTS.VERIFICATION)
        stage_state = alert.get("stage_stage", CONSTANTS.ACTIVE)
        outcome = None

        alarm_name = alert.get('alarm_name')
        if isinstance(alarm_name, (dict)):
            alarm_name = alarm_name.get('en')

        notifiers = await Alert.get_stakeholders(alert_id, user=user)
        notifiers = list(filter(lambda x: x.get("id") != user.get("id"), notifiers))
        action_data = data.get("data", dict())
        notification_data = dict(
            user_name=user['name'],
            alarm_name=alarm_name,
            location_name=alert['location_name'].get("en", None),
            alert_date=date_utils.display(alert.get("trigger_end"), alert.get("interval")),
            risk=RISK_LEVELS.get(action_data.get("risk", None), None),
            reason=action_data.get("reason", None),
            content=action_data.get("content", None),
            hazard_assessment=action_data.get("hazard_assessment", None),
            exposure_assessment=action_data.get("exposure_assessment", None),
            context_assessment=action_data.get("context_assessment", None),
            outcome=OUTCOMES.get(action_data.get("outcome", None), None),
            alert_uuid=alert_id
        )

        action_type = data.get("type")

        cur_loop = asyncio.get_event_loop()
        # Progressing from VERIFICATION stage
        if action_type == CONSTANTS.VERIFICATION:
            outcome = data['data'].get("outcome")
            if outcome in (CONSTANTS.DISCARDED, "DISCARD"):
                state = CONSTANTS.CLOSED
                stage = CONSTANTS.VERIFICATION
                stage_state = CONSTANTS.COMPLETED
                alert_outcome = CONSTANTS.DISCARDED
                await Alert.add_activity(alert_id, "DISCARDED", "fa-trash", user.get("id"),
                                         data['data'].get("content", None),
                                         user=user)

                cur_loop.create_task(
                    notifications.send_bulk(
                        'ALERT_DISCARDED',
                        notifiers,
                        notification_data,
                        account=account.get('name', ""),
                        user=user
                    )
                )

                await Alert.add_event(alert_id, "CLOSED", "fa-ban", user=user)
            elif outcome == CONSTANTS.MONITOR:
                state = CONSTANTS.OPEN
                stage = CONSTANTS.VERIFICATION
                stage_state = CONSTANTS.COMPLETED
                alert_outcome = CONSTANTS.MONITOR

                cur_loop.create_task(
                    notifications.send_bulk(
                        'ALERT_OUTCOME',
                        notifiers,
                        notification_data,
                        account=account.get('name', ''),
                        user=user
                    )
                )


                await Alert.add_activity(alert_id, "MONITOR", "fa-eye", user.get("id"), data['data'].get("content"),
                                         user=user)
            elif outcome == CONSTANTS.ADVANCE:
                state = CONSTANTS.OPEN
                stage = CONSTANTS.RISK_ASSESS
                stage_state = CONSTANTS.PENDING
                alert_outcome = None

                cur_loop.create_task(
                    notifications.send_bulk(
                        'ALERT_VERIFICATION',
                        notifiers,
                        notification_data,
                        account=account.get('name', ''),
                        user=user
                    )
                )

                await Alert.add_activity(
                    alert_id,
                    "VERIFIED",
                    "fa-check",
                    user.get("id"),
                    data['data'].get("content"),
                    user=user
                )
        # Progressing from Risk Assessment stage
        elif action_type == CONSTANTS.RISK_ASSESS:
            stage = CONSTANTS.RISK_CHAR
            stage_state = CONSTANTS.PENDING

            cur_loop.create_task(
                notifications.send_bulk(
                    'ALERT_RISK_ASSESS',
                    notifiers,
                    notification_data,
                    account=account.get('name', ''),
                    user=user
                )
            )

            await Alert.add_activity(
                alert_id,
                "ASSESSED", "fa-check", user.get("id"), "", user=user)
        # Progressing from Risk characterisation
        elif action_type == CONSTANTS.RISK_CHAR:
            risk = data['data'].get("risk")
            risk_cell = data['data'].get("risk_cell")
            stage = CONSTANTS.OUTCOME
            stage_state = CONSTANTS.PENDING

            cur_loop.create_task(
                notifications.send_bulk(
                    'ALERT_RISK_CHAR',
                    notifiers,
                    notification_data,
                    account=account.get('name', 'en'),
                    user=user
                )
            )


            await Alert.add_activity(alert_id, "CHARACTERIZED", "fa-check", user.get("id"),
                                     RISK_LEVELS[data['data'].get("risk")],
                                     user=user)
        # Progressing from outcome
        elif action_type == CONSTANTS.OUTCOME:
            outcome = data['data'].get("outcome")
            if outcome in (CONSTANTS.DISCARD, "DISCARDED"):
                alert_outcome = CONSTANTS.DISCARDED
                stage = CONSTANTS.OUTCOME
                stage_state = CONSTANTS.COMPLETED
                state = CONSTANTS.CLOSED

                cur_loop.create_task(
                    notifications.send_bulk(
                        'ALERT_OUTCOME',
                        notifiers,
                        notification_data,
                        account=account.get('name', ''),
                        user=user
                    )
                )

                await Alert.add_activity(alert_id, "DISCARDED", "fa-trash", user.get("id"),
                                         data['data'].get("comments", None),
                                         user=user)
                await Alert.add_event(alert_id, "CLOSED", "fa-ban", user=user)
            elif outcome == CONSTANTS.MONITOR:
                alert_outcome = CONSTANTS.MONITOR
                stage = CONSTANTS.OUTCOME
                stage_state = CONSTANTS.MONITOR
                state = CONSTANTS.OPEN

                cur_loop.create_task(
                    notifications.send_bulk(
                        'ALERT_OUTCOME',
                        notifiers,
                        notification_data,
                        account=account.get('name', ''),
                        user=user
                    )
                )

                await Alert.add_activity(alert_id, "MONITOR", "fa-eye", user.get("id"),
                                         data['data'].get("comments", None),
                                         user=user)
            elif outcome in (CONSTANTS.RESPONSE, "RESPOND"):
                alert_outcome = CONSTANTS.RESPONSE
                stage = CONSTANTS.OUTCOME
                stage_state = CONSTANTS.COMPLETED
                state = CONSTANTS.CLOSED

                cur_loop.create_task(
                    notifications.send(
                        'ALERT_OUTCOME',
                        notifiers,
                        notification_data,
                        account=user.get('account_name', None),
                        user=user
                    )
                )


                await Alert.add_activity(alert_id, "RESPONSE", "fa-hospital-o", user.get("id"),
                                         data['data'].get("comments", None), user=user)
                await Alert.add_event(alert_id, "CLOSED", "fa-ban", user=user)

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alert_actions
                SET status = 'SUBMITTED',
                    submitted_date = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.now(),
                action_id,
            ))

            await cur.execute("""
                UPDATE %s.alerts
                SET outcome = %s,
                    risk = %s,
                    state = %s,
                    stage = %s,
                    stage_state = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert_outcome,
                risk,
                state,
                stage,
                stage_state,
                data.get("alert_id"),
            ))

        #TODO: Generate event

        return True

    @staticmethod
    async def get_alert_action(alert_uuid, type, user=None):
        """ Retrieve all alert actions of a specific type for a given alert
        :param alert_uuid: {str} The UUId of the alert that the action is attached to
        :param type: {str} The type of action it is
        :param user: {dict} The calling user
        :return:
        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT aa.*, u.name AS user_name, u.email AS user_email
                FROM %s.alert_actions AS aa
                    LEFT JOIN _iw.users AS u ON u.id = aa.user_id
                WHERE aa.alert_id = %s
                    AND aa.type = %s
                ORDER BY aa.created DESC
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
                type,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def set_pending_state(alert_id, user=None):
        """ Set the state of a given stage to active
        :param alert_id:
        :param user:
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alerts
                SET stage_state = 'ACTIVE'
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))

        #TODO: Generate event

    @staticmethod
    async def reopen(alert_uuid, reason=None, user=None):
        """ Re-open a closed/discarded alert
        :param alert_uuid:
        :param user:
        :return:
        """
        alert = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, al.name As alarm_name, al.ds_interval AS interval, l.name AS location_name
                FROM %s.alerts AS a
                    LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE a.uuid = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert_uuid,
            ))
            alert = await cur.fetchone()

        notifiers = await Alert.get_stakeholders(alert_uuid, user=user)
        notifiers = list(filter(lambda x: x.get('id') != user.get("id"), notifiers))
        notification_data = dict(
            user_name=user['name'],
            alarm_name=alert.get('alarm_name', 'No alarm name'),
            location_name=alert['location_name'].get("en", None),
            alert_date=date_utils.display(alert.get("trigger_end"), alert.get("interval")),
            reason=reason
        )

        stage = None
        stage_state = None

        if alert.get("stage") == CONSTANTS.VERIFICATION:
            stage = CONSTANTS.VERIFICATION

        elif alert.get("stage") == CONSTANTS.OUTCOME:
            stage = CONSTANTS.RISK_ASSESS
            stage_state = CONSTANTS.PENDING

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alert_actions
                SET status = 'DRAFT_OPEN'
                WHERE alert_id = %s
                AND type != 'VERIFICATION'
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
            ))

            if stage == CONSTANTS.VERIFICATION:
                await cur.execute("""
                    UPDATE %s.alert_actions
                    SET status = 'DRAFT_OPEN'
                    WHERE alert_id = %s
                        AND type = 'VERIFICATION'
                """, (
                    AsIs(user.get("tki")),
                    alert_uuid,
                ))

            await cur.execute("""
                UPDATE %s.alerts
                SET state = 'OPEN',
                    stage = %s,
                    stage_state = %s,
                    risk = NULL,
                    outcome = NULL
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                stage,
                stage_state,
                alert_uuid,
            ))

        # ret = await EventLog.add_event(
        #     'RES_CHG',
        #     user.get('id'),
        #     user.get('did', None),
        #     dict(),
        #     dict(
        #         res='ALERT',
        #         id=alert_uuid,
        #         d=dict(
        #             state=CONSTANTS.OPEN,
        #             stage=CONSTANTS.VERIFICATION,
        #             stage_state=CONSTANTS.PENDING
        #         )
        #     ),
        #     user=user
        # )

        cur_loop = asyncio.get_event_loop()
        asyncio.ensure_future(
            notifications.send_bulk(
                'ALERT_REOPENED',
                notifiers,
                notification_data,
                user=user
            )
        )

        asyncio.ensure_future(
            Alert.add_activity(
                alert_uuid,
                'REOPEN',
                'fa-exclamation-triangle',
                user.get('id'),
                reason,
                user=user
            )
        )

        #TODO: Generate event

        return True

    @staticmethod
    async def suppress_alert(alert_uuid, data, user=None):
        """ Suppress an alert
        
        Args:
            alert_uuid: 
            data: 
            user: 

        Returns:

        """
        """Suppress and alert

        Args:
            alert_uuid: The UUID of the alert
            data: The reason for the suppression
            user: The calling user

        Returns:

        """
        alert = None
        location = None
        alarm = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alerts
                SET state = 'INACTIVE',
                    action_taken = 'SUPPRESSED',
                    actioned_date = %s,
                    actioned_by = %s,
                    action_reason = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                datetime.datetime.now(),
                user['id'],
                data.get('reason'),
                alert_uuid,
            ))

            await cur.execute("""
                SELECT *
                FROM %s.alerts
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT * FROM %s.alarms_v2 WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert['alarm_id'],
            ))
            alarm = await cur.fetchone()

            await cur.execute(""" SELECT uuid, name, lineage FROM %s.locations WHERE uuid = %s """, (
                AsIs(user.get("tki")),
                alert['location_id'],
            ))
            location = await cur.fetchone()

        admins = await user_lookup.get_account_admins(user=user)
        admins = admins + await user_lookup.get_regional_admins(location.get("lineage"), user=user)

        # Add support to all suppression emails
        admins.append(dict(
            id=3,
            email="support@ewars.ws",
            name="EWARS Support",
            bounced=False
        ))

        cur_loop = asyncio.get_event_loop()

        cur_loop.create_task(
            notifications.send_bulk(
                'ALERT_SUPPRESSED',
                admins,
                data=dict(
                    suppressor=user.get('name'),
                    alarm_name=alarm.get('name', {}).get('en'),
                    location_name=location.get('name', {}).get('en', 'Unknown'),
                    alert_date=date_utils.display(alert.get('trigger_end'), alarm.get('date_spec_interval')),
                    alert_uuid=alert_uuid,
                    reason=data.get('reason', 'No reason provided')
                )
            )
        )

        #TODO: Generateg event

        return True

    @staticmethod
    async def get_related(alert_id, user=None):
        """ Get other alerts related to this alert
        
        Args:
            alert_id: 
            user: 

        Returns:

        """
        alert = None
        alerts = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alerts WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert_id,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT a.uuid,
                    a.trigger_end,
                    a.location_id,
                    a.state,
                    a.stage,
                    a.stage_state,
                    a.created,
                    al.name AS alarm_name,
                    i.name AS indicator_name,
                    l.name AS location_name
                FROM %s.alerts AS a
                    LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                    LEFT JOIN %s.indicators AS i ON i.uuid::TEXT = al.ds_indicator
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE a.alarm_id = %s
                    AND a.location_id = %s
                    AND a.uuid != %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alert.get("alarm_id"),
                alert.get("location_id"),
                alert_id,
            ))
            alerts = await cur.fetchall()

        for alert in alerts:
            alert['location_name_full'] = await Location.get_full_name(alert.get("location_id"), user=user)

        return alerts

    @staticmethod
    async def get_action(alert_uuid, type, user=None):
        """ Get the canonical version of an action for a specific alert
        
        Args:
            alert_uuid: 
            type: 
            user: 

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT aa.*, u.name AS user_name, u.email AS user_email
                FROM %s.alert_actions AS aa
                    LEFT JOIN _iw.users AS u ON u.id = aa.user_id
                WHERE aa.alert_id = %s
                    AND aa.type = %s
                ORDER BY aa.created DESC
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
                type,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get_actions(alert_uuid, user=None):
        """ Get actions associated with an alert
        
        Args:
            alert_uuid: 
            user: 

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT aa.*, u.name AS user_name, u.email AS user_email
                FROM %s.alert_actions AS aa
                    LEFT JOIN _iw.users AS u ON u.id = aa.user_id
                WHERE aa.alert_id = %s
                    AND aa.type = %s
                ORDER BY aa.created DESC
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
                type,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get(alert_uuid, user=None):
        """ Get an alert within the system
        
        Args:
            alert_uuid: 
            user: 

        Returns:

        """

        result = None
        alert = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*
                FROM %s.alerts_full AS a
                WHERE a.uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
            ))
            alert = await cur.fetchone()

            if alert is not None:
                await cur.execute("""
                    SELECT *
                    FROM %s.alarms_V2
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    alert['alarm_id'],
                ))
                alarm = await cur.fetchone()

                await cur.execute("""
                    SELECT type, submitted_date
                    FROM %s.alert_actions
                    WHERE alert_id = %s
                """, (
                    AsIs(user.get("tki")),
                    alert_uuid,
                ))
                actions = await cur.fetchall()
                map_actions = {}

                for item in actions:
                    map_actions[item.get("type")] = item

                result = alert

                report_id = alert.get("trigger_report_id", None)
                if report_id is not None:
                    await cur.execute("""
                        SELECT uuid, form_id, form_version_id, submitted_date, data_date
                        FROM %s.collections
                        WHERE uuid = %s
                    """, (
                        AsIs(user.get("tki")),
                        report_id,
                    ))
                    result['report'] = await cur.fetchone()
                else:
                    result['report'] = {}

                result['alarm'] = alarm
                result['actions'] = map_actions

        result['location_name_full'] = await Location.get_full_name(alert.get("location_id"), user=user)

        return result

    @staticmethod
    async def get_inbound_forms(alert_uuid, user=None):
        """ Get forms that feed into this alarm
        
        Args:
            alert_uuid: The uuid of the alert
            user: 

        Returns:

        """
        result = []

        return result

    @staticmethod
    async def get_records(aid, user=None):
        """ Get records from during a trigger period

        Args:
            aid: The id of the alert to get records for
            user: The calling user

        Returns:

        """
        alert = None
        alarm = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alerts 
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                aid,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT * FROM %s.alarms_v2
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                alert.get('alarm_id'),
            ))
            alarm = await cur.fetchone()

        if alarm.get('monitor_type', 'AGGREGATE') == 'RECORD':
            async with get_db_cur() as cur:
                await cur.execute("""
                     SELECT
                        r.uuid,
                        r.data_date,
                        r.location_id,
                        r.data,
                        u.name AS user_name,
                        u.email AS user_email,
                        l.uuid AS location_uuid,
                        l.name->>'en' AS location_name,
                        f.name->>'en' AS form_name
                    FROM %s.collections AS r 
                      LEFT JOIN %s.users AS u ON u.id = r.created_by
                      LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                      LEFT JOIN %s.forms AS f ON f.id = r.form_id
                    WHERE r.uuid = '%s';
                """, (
                        AsIs(user.get('tki')),
                        AsIs(user.get('tki')),
                        AsIs(user.get('tki')),
                        AsIs(user.get('tki')),
                        AsIs(alert.get('triggering_report_id')),  # Pre-built above
                    ))
                records = await cur.fetchall()

            return records

        indicator_ids = []
        form_ids = []
        indicator = alarm.get('ds_indicator', None)
        if '{' in indicator:
            indicator = json.loads(indicator)
            form_ids = [indicator.get('form_id', None)]
        else:
            indicator_ids = [indicator]

        if len(form_ids) <= 0:
            for ind_id in indicator_ids:
                res = await Indicator.inbound(ind_id, user=user)
                form_ids = form_ids + [x.get('id') for x in res]

        records = []

        start_date = None
        end_date = alert.get('trigger_end')

        # Figure out the date range for querying for source data
        if alarm.get("ds_interval_type") == "TRIGGER_PERIOD" or alarm.get("date_spec") == "TRIGGER":
            end_date = date_utils.get_end_of_current_interval(alert.get("trigger_end"), alarm.get("ds_interval"))
            start_date = date_utils.get_start_of_interval(end_date, alarm.get("ds_interval"))
        elif alarm.get("ds_interval_type") == "RANGE":
            end_date = date_utils.get_end_of_current_interval(alert.get("trigger_end"), alarm.get('ds_interval'))
            # Get the delta to apply to the end_date
            interval_delta = date_utils.get_delta(alarm.get("ds_sd_intervals"), alarm.get("ds_interval"))
            start_date = end_date - interval_delta
            # Get the start date (start of the week) for the start date interval
            start_date = date_utils.get_start_of_interval(start_date, alarm.get("ds_interval"))

        ds_agg_lower = alarm.get("ds_agg_lower", False)
        loc_id = alert.get("location_id")

        location_spec = "AND r.location_id = '%s'" % (loc_id,)
        if ds_agg_lower:
            location_spec = " AND l.lineage::TEXT[] @> ARRAY['%s']" % (
                loc_id,
            )

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT
                    r.uuid,
                    r.data_date,
                    r.location_id,
                    r.data,
                    u.name AS user_name,
                    u.email AS user_email,
                    l.uuid AS location_uuid,
                    l.name->>'en' AS location_name,
                    f.name->>'en' AS form_name
                FROM %s.collections AS r 
                  LEFT JOIN %s.users AS u ON u.id = r.created_by
                  LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                  LEFT JOIN %s.forms AS f ON f.id = r.form_id
                WHERE r.status = 'SUBMITTED'
                  %s
                  AND r.form_id = ANY(%s)
                  AND r.data_date >= %s 
                  AND r.data_date <= %s;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(location_spec),  # Pre-built above
                form_ids,
                start_date,
                end_date,
            ))
            records = await cur.fetchall()

        return records

    @staticmethod
    async def reevaluate(alert_id, user=None):
        report = None
        alert = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alerts
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                alert_id,
            ))
            alert = await cur.fetchone()

            await cur.execute("""
                SELECT * FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                alert.get("triggering_report_id"),
            ))
            report = await cur.fetchone()

        result = None
        if report is not None:
            result = await _hard_evaluate(
                alert.get('alarm_id'),
                alert_id,
                report.get('uuid'),
                token=user.get('tki'),
            )
        else:
            result = False
            async with get_db_cur() as cur:
                await cur.execute("""
                    DELETE FROM %s.alert_actions WHERE alert_id = %s;
                    DELETE FROM %s.alert_events WHERE alert_id = %s;
                    DELETE FROM %s.alerts WHERE uuid = %s;
                """, (
                    AsIs(user.get('tki')),
                    alert_id,
                    AsIs(user.get('tki')),
                    alert_id,
                    AsIs(user.get('tki')),
                    alert_id,
                ))

        return dict(
            keep=result
        )

async def _hard_evaluate(alarm_uuid, alert_uuid, report_uuid, token=None):
    report = None
    alarm = None
    form = None
    form_logic = None

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT * FROM %s.alarms_V2 WHERE uuid = %s
            """, (
            AsIs(token),
            alarm_uuid,
        ))
        alarm = await cur.fetchone()

        await cur.execute("""
                SELECT c.*, l.lineage, l.site_type_id
                  FROM %s.collections AS c
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                  WHERE c.uuid = %s;
            """, (
            AsIs(token),
            AsIs(token),
            report_uuid,
        ))
        report = await cur.fetchone()

        if report is not None:
            await cur.execute("""
                    SELECT id, name, time_interval, version_id
                    FROM %s.forms
                    WHERE id = %s
                """, (
                AsIs(token),
                report.get("form_id"),
            ))
            form = await cur.fetchone()

            await cur.execute("""
                    SELECT etl FROM %s.form_versions
                    WHERE uuid = %s
                """, (
                AsIs(token),
                form.get("version_id"),
            ))
            form_logic = await cur.fetchone()

    if report is None:
        return None

    # Does this report have an effect on the alarm?
    has_effect = False

    # Need to filter to figure out if this report should actually be run for the given
    # Need to know if this report affects either the source indicator for the alarm,
    # or the comparison/comparator indication for the alarm

    # Indicators which the report outputs
    output_indicators = []

    etl = form_logic.get("etl")
    if etl is None:
        etl = dict()
    output_indicators = list(set(etl.keys()))

    # Need to add in system indicators to the output_indicators array
    output_indicators.append(dict(
        uuid='e51f4ca7-1678-44e3-b5cb-64f84555696f',
        form_id=form.get("id")
    ))

    alarm_indicators = []
    is_complex = False
    ds_formula = None
    ds_sources = None

    # Primary alarm indicator
    ds_indicator = alarm.get("ds_indicator", None)
    ds_ind_uuid = alarm.get("ds_indicator", None)
    if "{" in ds_indicator:
        ds_indicator = json.loads(ds_indicator)
        if 'ds_formula' in ds_indicator.keys():
            ds_sources = ds_indicator.get('ds_sources')
            ds_formula = ds_indicator.get('ds_formula')
            is_complex = True
        else:
            alarm_indicators.append(ds_indicator)
    else:
        alarm_indicators.append(ds_ind_uuid)

    crit_indicator = None
    crit_indicator_uuid = None
    if alarm.get("crit_source", None) == "DATA":
        crit_indicator = alarm.get("crit_indicator", None)
        if "uuid" in crit_indicator:
            crit_indicator = json.loads(crit_indicator)
            crit_indicator_uuid = crit_indicator.get("uuid")

    for a_ind in alarm_indicators:
        for o_ind in output_indicators:
            if isinstance(a_ind, (string_types,)) and isinstance(o_ind, (string_types,)):
                if o_ind == a_ind:
                    has_effect = True
            elif isinstance(a_ind, (dict,)) and isinstance(o_ind, (dict,)):
                if a_ind.get("uuid", None) == o_ind.get("uuid", None):
                    if a_ind.get("uuid", None) is not None and o_ind.get("uuid", None) is not None:
                        # Check that they have matching form id so we don't allow other forms to trigger alerts
                        if a_ind.get("form_id", "NO_A") == o_ind.get("form_id", "NO_B"):
                            has_effect = True

    if has_effect == False:
        return None

    start_date = None
    end_date = None
    locations = []

    # Figure out the date range for querying for source data
    if alarm.get("ds_interval_type") == "TRIGGER_PERIOD" or alarm.get("date_spec") == "TRIGGER":
        end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get("ds_interval"))
        start_date = date_utils.get_start_of_interval(end_date, alarm.get("ds_interval"))
    elif alarm.get("ds_interval_type") == "RANGE":
        end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get('ds_interval'))
        # Get the delta to apply to the end_date
        interval_delta = date_utils.get_delta(alarm.get("ds_sd_intervals"), alarm.get("ds_interval"))
        start_date = end_date - interval_delta
        # Get the start date (start of the week) for the start date interval
        start_date = date_utils.get_start_of_interval(start_date, alarm.get("ds_interval"))

    ds_agg_lower = alarm.get("ds_agg_lower", False)
    sti = None
    loc_id = report.get("location_id")
    loc_groups = None

    # Check if the location is restricted for raising this alert
    if alarm.get("restrict_loc", False) is True:
        if alarm.get("loc_spec") == "SPECIFIC":
            loc_id = alarm.get("loc_id", None)
            if loc_id not in report.get("lineage", []):
                return None
        elif alarm.get("loc_spec") == "GROUPS":
            loc_groups = alarm.get("loc_groups")
            is_ok = False

            loc_groups_full = await LocationGroup.get_locations_in_group(loc_groups, user=dict(
                tki=token
            ))

            for loc in loc_groups_full:
                if loc.get("uuid") in report.get("lineage"):
                    is_ok = True

            if is_ok != True:
                return None

    # Check if this is restricted to a specific location type
    if alarm.get('sti_restrict', False) is True:
        sti = alarm.get("sti")
        if report.get("site_type_id") != int(sti):
            return None

    # get the data
    ds_location = report.get("location_id", None)

    constrain_record = alarm.get('monitor_type', 'AGGREGATE')
    if constrain_record == 'RECORD':
        constrain_record = report_uuid
        loc_id = report.get('location_id')
    else:
        constrain_record = False

    ds_result = None
    if alarm.get("ds_type") == "INDICATOR":
        results = await immediate.run(
            target_interval=alarm.get("ds_interval", "DAY"),
            target_location=str(loc_id),
            indicator=ds_indicator,
            options=dict(
                start_date=start_date,
                end_date=end_date,
                # constrain=agg_lower,
                constrain_report=constrain_record,
                roll_up=ds_agg_lower,
                reduction=alarm.get("ds_aggregate", "SUM")
            ),
            user=dict(
                tki=token,
                group_id=1
            )
        )
        ds_result = results.get("data", None)

    if ds_result is not None:
        ds_result = float(ds_result)

    # Apply the modifier if one has been specified
    if alarm.get('ds_modifier') is not None:
        ds_result = ds_result * float(alarm.get("ds_modifier", 1))

    # Set up the value to compare to
    comparator = alarm.get("crit_comparator")
    modifier = alarm.get("crit_modifier", 1)
    floor = alarm.get('crit_floor', None)

    result = None
    if alarm.get("crit_source") == "VALUE":
        # Create a lambda function for the comparator
        crit_value = alarm.get("crit_value", 0)
        a = None
        if comparator == "GT":
            a = lambda x: True if x > crit_value else False
        elif comparator == "GTE":
            a = lambda x: True if x >= crit_value else False
        elif comparator == "EQ":
            a = lambda x: True if x == crit_value else False
        elif alarm.get("comparator") == "LT":
            a = lambda x: True if x < crit_value else False
        elif alarm.get("comparator") == "LTE":
            a = lambda x: True if x <= crit_value else False
        elif alarm.get("comparator") == "NEQ":
            a = lambda x: True if x != crit_value else False

        # Iterate through the locations and apply the lambda to the values
        # to figure out if the value should trigger the alarm
        result = a(ds_result)

    elif alarm.get("crit_source") == "DATA":
        comparator_data = await _get_alarm_comparator(alarm,
                                                            report,
                                                            str(report.get("location_id")),
                                                            user=dict(tki=token))

        if comparator_data is not None:
            # Iterate through the location results and do the comparison
            crit_result = comparator_data

            if alarm.get("crit_modifier", None) is not None:
                crit_result = crit_result * float(alarm.get("crit_modifier"))

                if alarm.get('crit_floor', None) is not None:
                    if crit_result < float(alarm.get('crit_floor')):
                        crit_result = float(alarm.get("crit_floor"))

                if ds_result is None:
                    result = False
                else:
                    result = comparators.compare(comparator, ds_result, crit_result)
        else:
            result = False

    # Set up a dummy user
    account = None
    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT id FROM _iw.accounts WHERE tki = %s;
            """, (
            token,
        ))
        account = await cur.fetchone()

    dummy_user = dict(
        tki=token,
        aid=account.get("id")
    )

    if result is True:
        return True
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                    DELETE FROM %s.alert_actions WHERE alert_id = %s;
                    DELETE FROM %s.alert_events WHERE alert_id = %s;
                    DELETE FROM %s.alerts WHERE uuid = %s;
                """, (
                AsIs(token),
                alert_uuid,
                AsIs(token),
                alert_uuid,
                AsIs(token),
                alert_uuid,
            ))
        return False

    return True

async def _get_alarm_comparator(alarm, report, location, user=None):
    """ Get the comparator for an alarm evaluate

    Args:
        alarm:
        report:
        location:
        user:

    Returns:

    """
    reduction = alarm.get("crit_reduction", "SUM")

    start_date = report.get("data_date")
    end_date = report.get("data_date")

    if alarm.get("crit_sd_spec") == "SPECIFIC":
        start_date = date_utils.parse_date(alarm.get("start_date"))
    elif alarm.get("crit_sd_spec") == "INTERVALS_BACK":
        start_date = start_date - date_utils.get_delta(
            alarm.get("crit_sd_intervals"),
            alarm.get("ds_interval")
        )

    if alarm.get("crit_ed_spec") == "SPECIFIC":
        end_date = date_utils.parse_date(alarm.get("end_date"))
    elif alarm.get("crit_ed_spec") == "INTERVALS_BACK":
        end_date = end_date - date_utils.get_delta(
            alarm.get("crit_ed_intervals"),
            alarm.get("ds_interval")
        )

    indicator = alarm.get("crit_indicator", None)
    if indicator is not None:
        if "uuid" in indicator:
            indicator = json.loads(indicator)

    if indicator is None:
        return None

    data = await immediate.run(
        target_interval=alarm.get("ds_interval"),
        target_location=location,
        indicator=indicator,
        options=dict(
            start_date=start_date,
            end_date=end_date,
            roll_up=alarm.get("ds_agg_lower", False)
        ),
        user=user
    )

    expected_intervals = date_utils.get_date_range(start_date, end_date, alarm.get("ds_interval"))
    if len(data.get("data")) < len(expected_intervals):
        # There's not enough data to do the calculation, return None
        return None

    agg_result = 0

    data = [[x[0], x[1]] for x in data.get("data") if x[1] is not None]

    if len(data) > 0:
        if reduction == "SUM":
            agg_result = sum([x[1] for x in data])
        elif reduction == "AVG":
            agg_result = sum([x[1] for x in data]) / len(data)
        elif reduction == "MIN":
            min = data[0][1]
            for x in data:
                if x[1] > min:
                    min = x[1]
            agg_result = min
        elif reduction == "MAX":
            max = data[0][1]
            for x in data:
                if x[1] > max:
                    max = x[1]
            agg_result = max
        elif reduction == "COUNT":
            agg_result = len(data)
        elif reduction == "DISTINCT":
            items = [x[1] for x in data]
            agg_result = len(set(items))

    return agg_result