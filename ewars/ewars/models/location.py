import json
import datetime
import uuid

from ewars.db import get_db_cur
from ewars.core.location_utils import check_name
from ewars.constants import CONSTANTS
from ewars.core.async_lru import async_lru
from ewars.models.event_log import EventLog
from ewars.utils.six import string_types

from psycopg2.extensions import AsIs

GET_BY_ID = """
    SELECT l.uuid, l.name, l.site_type_id, l.status, lt.name AS lti_name
    FROM %s.locations AS l
        LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
    WHERE l.uuid = %s;
"""


class Location:
    @staticmethod
    async def get_by_id(lid, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute(GET_BY_ID, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                lid,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get(lid, user=None):
        """ Get a specific location

        Args:
            cls:
            lid:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT * FROM %s.locations
                WHERE uuid = %s;
            ''', (
                AsIs(user.get('tki')),
                lid,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(location_id, data, user=None):
        """ Update a locations settings and details

        Args:
            cls:
            location_id:
            data:
            user:

        Returns:

        """
        result = None

        errors = dict()
        has_moved = False

        if data.get("pcode", None) in (None, ""):
            errors['pcode'] = "Please provide a valid pcode"

        # Check for pcode duplication
        if data.get("pcode", None) not in ("None", ""):
            dupe = None
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid
                        FROM %s.locations
                        WHERE pcode = %s
                        AND uuid != %s
                    """, (
                    AsIs(user.get("tki")),
                    data.get("pcode"),
                    location_id,
                ))
                dupe = await cur.fetchone()

            if dupe is not None:
                errors['pcode'] = "The pcode specified is already in use"

        if data.get("parent_id", None) in (None, ""):
            if location_id != user.get("clid"):
                errors['parent_id'] = "Please specify a parent"

        if data.get("site_type_id", None) in (None, ""):
            errors['site_type_id'] = "Please specify a location type"

        if data.get("status", None) in (None, ""):
            errors['status'] = "Please specify a status"

        if check_name(data.get("name", None)) == False:
            errors['name'] = "Please provide a valid name"

        if len(errors.keys()) > 0:
            return dict(
                err=True,
                errs=errors
            )

        # Get the currently stored version of the location
        original = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, lineage, parent_id
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                location_id,
            ))
            original = await cur.fetchone()

        lineage = original.get('lineage')
        # Sort out lineage
        if data.get("parent_id", None) is not None:
            parent = None
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT lineage FROM %s.locations WHERE uuid = %s
                    """, (
                    AsIs(user.get("tki")),
                    data.get("parent_id"),
                ))
                parent = await cur.fetchone()

            if parent is not None:
                lineage = parent.get("lineage") + [location_id]
        else:
            lineage = [location_id]

        # Sort out geojson
        geojson = None
        if data.get('geojson', None) not in ('', None, 'null'):
            geojson = data.get('geojson')

        if isinstance(geojson, (string_types,)):
            geojson = json.loads(geojson)

        # Update the record
        async with get_db_cur() as cur:
            await cur.execute("""
                    UPDATE %s.locations
                        SET
                            name = %s,
                            location_type = %s,
                            pcode = %s,
                            description = %s,
                            parent_id = %s,
                            lineage = %s,
                            status = %s,
                            geometry_type = %s,
                            site_type_id = %s,
                            default_zoom = %s,
                            groups = %s,
                            geojson = %s
                        WHERE uuid = %s
                """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name")),
                data.get("geometry_type"),
                data.get("pcode"),
                data.get("description"),
                data.get("parent_id"),
                lineage,
                data.get("status"),
                data.get("geometry_type", "POINT"),
                data.get("site_type_id", 11),
                data.get("default_zoom", 10),
                data.get("groups", []),
                json.dumps(geojson),
                location_id,
            ))

            # If location is being disabled, disable all
            # assignments for this location
            if data.get('status', None) == "DISABLED":
                await cur.execute("""
                    UPDATE %s.assignments
                    SET status = 'INACTIVE'
                    WHERE location_id = %s;
                """, (
                    AsIs(user.get("tki")),
                    location_id,
                ))

            await cur.execute("""
                SELECT * FROM %s.locations WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            result = await cur.fetchone()

        # If location is DISABLED, disable all children of the location
        if data.get('status') == CONSTANTS.DISABLED:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.locations
                    SET status = 'DISABLED'
                    WHERE lineage::TEXT[] @> %s
                """, (
                    AsIs(user.get("tki")),
                    [location_id],
                ))

        # if the location is set to ACTIVE, we need to ACTIVE all
        # its parent locations
        if data.get("status") == CONSTANTS.ACTIVE:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.locations
                    SET status = 'ACTIVE'
                    WHERE uuid IN %s
                """, (
                    AsIs(user.get("tki")),
                    tuple(lineage),
                ))

        # Deal with reporting periods
        if result.get('parent_id') != original.get('parent_id'):
            # We may need to rejig child reporting periods as well
            periods = []
            parent_periods = []
            affected_locations = []
            async with get_db_cur() as cur:
                # Get the parent locations reporting periods
                if result.get('parent_id', None) is not None:
                    await cur.execute("""
                        SELECT uuid::TEXT, pid::TEXT, form_id FROM %s.location_reporting
                        WHERE location_id = %s;
                    """, (
                        AsIs(user.get('tki')),
                        original.get('parent_id'),
                    ))
                    parent_periods = await cur.fetchall()

                # Get affected locations
                await cur.execute("""
                    SELECT uuid::TEXT
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s::TEXT[]
                      AND status != 'DELETED'
                      AND uuid != %s;
                """, (
                    AsIs(user.get('tki')),
                    [result.get('uuid')],
                    result.get('uuid'),
                ))
                affected_locations = await cur.fetchall()
                affected_locations = [x.get('uuid') for x in affected_locations]

                ## Delete inherited reporting periods for immediate location
                await cur.execute("""
                    DELETE FROM %s.location_reporting
                    WHERE location_id = %s
                    AND pid != NULL;
                """, (
                    AsIs(user.get('tki')),
                    result.get('uuid'),
                ))

                parent_p_uuids = [x.get('uuid') for x in parent_periods]
                parent_p_uuids = parent_p_uuids + [x.get('pid') for x in parent_periods if
                                                   x.get('pid', None) is not None]

                if len(parent_p_uuids) > 0:
                    # Delete inherited periods from childs
                    await cur.execute("""
                        DELETE FROM %s.location_reporting
                        WHERE location_id::TEXT = ANY(%s)
                         AND pid::TEXT = ANY(%s);
                    """, (
                        AsIs(user.get('tki')),
                        affected_locations,
                        parent_p_uuids,
                    ))

                    # Delete inherited reporting periods from current location
                    await cur.execute("""
                        DELETE FROM %s.location_reporting
                        WHERE location_id = %s
                          AND pid::TEXT = ANY(%s);
                    """, (
                        AsIs(user.get('tki')),
                        result.get('uuid'),
                        parent_p_uuids,
                    ))

                # Get new parent periods
                n_parent_periods = []
                await cur.execute("""
                    SELECT *
                    FROM %s.location_reporting
                    WHERE location_id = %s;
                """, (
                    AsIs(user.get('tki')),
                    result.get('parent_id'),
                ))
                n_parent_periods = await cur.fetchall()

                # Add the current location to the affected locations
                affected_locations.append(str(result.get('uuid')))
                if len(n_parent_periods) > 0:
                    # trickier as we have to avoid overlapping
                    # reporting periods for the same form if there are still
                    # some in-tact
                    for loc in affected_locations:
                        loc_periods = []
                        await cur.execute("""
                            SELECT form_id FROM
                            %s.location_reporting
                            WHERE location_id = %s;
                        """, (
                            AsIs(user.get('tki')),
                            loc,
                        ))
                        loc_periods = await cur.fetchall()
                        loc_periods = [x.get('form_id') for x in loc_periods]

                        for n_period in n_parent_periods:
                            if n_period.get('form_id') not in loc_periods:
                                # Create the reporting period
                                pid = n_period.get('pid', None)
                                if pid is None:
                                    pid = n_period.get('uuid')

                                await cur.execute("""
                                    INSERT INTO %s.location_reporting
                                    (location_id, form_id, pid, start_date, end_date, status, created, created_by)
                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                                """, (
                                    AsIs(user.get('tki')),
                                    loc,
                                    n_period.get('form_id'),
                                    pid,
                                    n_period.get('start_date'),
                                    n_period.get('end_date', None),
                                    n_period.get('status', 'ACTIVE'),
                                    n_period.get('created'),
                                    n_period.get('created_by'),
                                ))

        # Need to update lineage of any children if the parent
        # id of the location has changed
        if original.get('parent_id') != result.get('parent_id'):
            children = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid::TEXT, lineage::TEXT[], parent_id::TEXT
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s::TEXT[]
                    AND uuid != %s;
                """, (
                    AsIs(user.get('tki')),
                    [result.get('uuid')],
                    result.get('uuid'),
                ))
                children = await cur.fetchall()

                loc_dict = dict((x.get('uuid'), x) for x in children)
                loc_dict[str(result.get('uuid'))] = result
                locations = sorted(children, key=lambda k: len(k.get("lineage")))

                for location in locations:
                    if location.get("parent_id", None) is not None:
                        parent = loc_dict.get(location.get("parent_id"))
                        if parent is not None:
                            location['lineage'] = parent.get("lineage") + [location.get("uuid")]

                for location in locations:
                    await cur.execute("""
                        UPDATE %s.locations
                        SET lineage = %s
                        WHERE uuid = %s;
                    """, (
                        AsIs(user.get('tki')),
                        location.get('lineage'),
                        location.get('uuid'),
                    ))

        return result

    @staticmethod
    async def delete(location_id, user=None):
        locations = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s::TEXT[]
            """, (
                AsIs(user.get("tki")),
                [str(location_id)],
            ))
            locations = await cur.fetchall()

        loc_uuids = [str(c['uuid']) for c in locations]

        async with get_db_cur() as cur:
            # Delete reporting periods
            await cur.execute("""
                DELETE FROM %s.location_reporting
                WHERE location_id::TEXT IN %s;
            """, (
                AsIs(user.get("tki")),
                tuple(loc_uuids),
            ))

            # Delete Alerts
            alerts = None
            await cur.execute("""
                SELECT uuid FROM %s.alerts WHERE location_id::TEXT IN %s
            """, (
                AsIs(user.get("tki")),
                tuple(loc_uuids),
            ))
            alerts = await cur.fetchall()

            alert_uuids = [x.get("uuid") for x in alerts]
            if len(alert_uuids) > 0:
                await cur.execute("""
                    DELETE FROM %s.alert_events WHERE alert_id IN %s;
                    DELETE FROM %s.alert_actions WHERE alert_id IN %s;
                    DELETE FROM %s.alert_users WHERE alert_id IN %s;
                    DELETE FROM %s.alerts WHERE uuid IN %s;
                """, (
                    AsIs(user.get("tki")), tuple(alert_uuids),
                    AsIs(user.get("tki")), tuple(alert_uuids),
                    AsIs(user.get("tki")), tuple(alert_uuids),
                    AsIs(user.get("tki")), tuple(alert_uuids),
                ))

            # Delete Reports
            collection_uuids = None
            await cur.execute("""
                SELECT uuid::TEXT FROM %s.collections
                WHERE location_id IN %s;
            """, (
                AsIs(user.get("tki")),
                tuple(loc_uuids),
            ))
            collection_uuids = await cur.fetchall()
            collection_uuids = [x.get("uuid") for x in collection_uuids]

            if len(collection_uuids) > 0:
                await cur.execute("""
                    DELETE FROM %s.amendments WHERE report_id::TEXT = ANY(%s);
                    DELETE FROM %s.retractions WHERE report_id::TEXT = ANY(%s);
                    DELETE FROM %s.collection_comments WHERE collection_id::TEXT = ANY(%s);
                    DELETE FROM %s.collections WHERE uuid::TEXT = ANY(%s);
                """, (
                    AsIs(user.get("tki")), collection_uuids,
                    AsIs(user.get("tki")), collection_uuids,
                    AsIs(user.get("tki")), collection_uuids,
                    AsIs(user.get("tki")), collection_uuids,
                ))

            # Delete assignments
            await cur.execute("""
                DELETE FROM %s.assignments
                WHERE location_id::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                loc_uuids,
            ))

            await cur.execute("""
                DELETE FROM %s.tasks_v2
                WHERE location_id::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                loc_uuids,
            ))

            await cur.execute("""
                UPDATE %s.accounts
                SET location_id = NULL
                WHERE location_id::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                loc_uuids,
            ))

            await cur.execute("""
                UPDATE _iw.accounts
                SET location_id = NULL
                WHERE location_id::TEXT = ANY(%s)
            """, (
                loc_uuids,
            ))

            # Delete
            await cur.execute("""
                DELETE FROM %s.locations
                    WHERE uuid::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                loc_uuids,
            ))

        # ret = EventLog.add_event(
        #     'RES_DEL',
        #     user.get('id'),
        #     None,
        #     dict(),
        #     dict(
        #         res='LOCATION',
        #         id=location_id,
        #     )
        # )

        return True

    @staticmethod
    async def create(data, user=None):
        result = None

        new_uuid = str(uuid.uuid4())

        errors = dict()

        if data.get("pcode", None) in (None, ""):
            errors['pcode'] = "Please provide a valid pcode"

        # Check for pcode duplication
        if data.get("pcode", None) not in ("None", ""):
            dupe = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid FROM %s.locations WHERE pcode = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get("pcode"),
                ))
                dupe = await cur.fetchone()

            if dupe is not None:
                errors['pcode'] = "The pcode specified is already in use"

        if data.get("parent_id", None) in (None, ""):
            errors['parent_id'] = "Please specify a parent"

        if data.get("site_type_id", None) in (None, ""):
            errors['site_type_id'] = "Please specify a location type"

        if data.get("status", None) in (None, ""):
            errors['status'] = "Please specify a status"

        if check_name(data.get("name", None)) == False:
            errors['name'] = "Please provide a valid name"

        if len(errors.keys()) > 0:
            return dict(
                err=True,
                errs=errors
            )

        # Sort out lineage
        lineage = []
        if data.get("parent_id", None) is not None:
            parent = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT lineage FROM %s.locations WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get("parent_id"),
                ))
                parent = await cur.fetchone()

            if parent is not None:
                lineage = parent.get("lineage") + [new_uuid]
        else:
            lineage = [new_uuid]

        # Insert the new record
        new_location = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.locations
                    (uuid, name, location_type, pcode, description, parent_id, lineage, status, geometry_type, geojson, site_type_id, default_zoom, groups)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                new_uuid,
                json.dumps(data.get("name")),
                data.get("geometry_type", 'POINT'),
                data.get("pcode"),
                data.get("description"),
                data.get("parent_id"),
                lineage,
                data.get("status"),
                data.get("geometry_type", "POINT"),
                json.dumps(data.get('geojson', {})),
                data.get("site_type_id", 11),
                data.get("default_zoom", 10),
                data.get("groups", []),
            ))
            new_location = await cur.fetchone()


        # Deal with reporting periods that it should inherit
        if new_location.get('parent_id', None) is not None:
            reporting_periods = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.location_reporting
                    WHERE location_id = %s;
                """, (
                    AsIs(user.get('tki')),
                    new_location.get('parent_id'),
                ))
                reporting_periods = await cur.fetchall()

                for period in reporting_periods:
                    await cur.execute("""
                        INSERT INTO %s.location_reporting
                        (location_id, form_id, pid, start_date, end_date, created, created_by)
                        VALUES (%s, %s, %s, %s, %s, %s, %s);
                    """, (
                        AsIs(user.get('tki')),
                        new_location.get('uuid'),
                        period.get('form_id'),
                        period.get('pid', None),
                        period.get('start_date'),
                        period.get('end_date', None),
                        period.get('created'),
                        period.get('created_by'),
                    ))

        # ret = await EventLog.add_event(
        #     'RES_NEW',
        #     user.get('id'),
        #     None,
        #     dict(
        #         res='LOCATION',
        #         d=new_location
        #     )
        # )

        return new_location

    @staticmethod
    async def get_reporting_periods(location_id, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT lr.*, f.name AS form_name
                FROM %s.location_reporting AS lr
                  LEFT JOIN %s.forms AS f ON f.id = lr.form_id
                WHERE lr.location_id = %s;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                location_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def enable_children(location_id, user=None):
        locations = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT
                FROM %s.locations
                WHERE lineage::TEXT[] @> %s
                AND status != 'DELETED'
            """, (
                AsIs(user.get("tki")),
                [location_id],
            ))
            locations = await cur.fetchall()

            locations = [x.get("uuid") for x in locations]

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.locations
                SET status = 'ACTIVE'
                WHERE uuid::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                locations,
            ))

        return True

    @staticmethod
    async def disable_children(location_id, user=None):
        locations = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT
                FROM %s.locations
                WHERE lineage::TEXT[] @> %s
                AND status != 'DELETED'
            """, (
                AsIs(user.get("tki")),
                [location_id],
            ))
            locations = await cur.fetchall()

            locations = [x.get("uuid") for x in locations]

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.locations
                SET status = 'DISABLED'
                WHERE uuid::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                locations,
            ))

            # Disable all assignments
            await cur.execute("""
                UPDATE %s.assignments
                SET status = 'INACTIVE'
                WHERE location_id::TEXT = ANY(%s)
            """, (
                AsIs(user.get("tki")),
                locations,
            ))

        return True

    @staticmethod
    async def get_profile(location_id, user=None):
        location = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.v_location_profile
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            location = await cur.fetchone()

        return location

    @staticmethod
    async def get_position(location_id, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT l.name, l.parent_id, l.site_type_id, l.lineage, lt.name as sti_name
                FROM %s.locations AS l
                  LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                WHERE l.uuid = %s;
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            location = await cur.fetchone()

            await cur.execute("""
                SELECT l.name, l.parent_id, l.site_type_id, l.lineage, lt.name AS sti_name
                FROM %s.locations AS l
                  LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                WHERE l.lineage[] @> %s::TEXT[]
                  AND l.status = 'ACTIVE'
                  AND l.uuid !- %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [location_id],
                location_id,
            ))
            children = await cur.fetchall()

            await cur.execute("""
                SELECT l.name, l.parent_id, l.site_type_id, l.lineage, lt.name AS sti_name
                FROM %s.locations AS l
                  LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                WHERE l.uuid IN %s
                  AND l.uuid != %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location.get("lineage"),
                location_id,
            ))
            ancestors = await cur.fetchall()

            results = [location] + children + ancestors

    @staticmethod
    async def get_assignments(location_id, user=None):
        results = []
        location = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, lineage
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            location = await cur.fetchone()

            await cur.execute("""
                SELECT a.*,
                    l.name AS location_name,
                    f.name AS form_name,
                    u.name AS user_name,
                    u.email AS user_email
                FROM %s.assignments AS a
                    LEFT JOIN %s.locations AS l on l.uuid = a.location_id
                    LEFT JOIN %s.forms AS f ON f.id = a.form_id
                    LEFT JOIN _iw.users AS u ON u.id = a.user_id
                WHERE a.status = ANY('ACTIVE', 'INACTIVE')
                    AND a.location_id = %s
                    AND a.type = 'DEFAULT';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
            ))
            results = await cur.fetchall()


        return results

    @staticmethod
    async def get_users(location_id, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT u.name,
                u.email,
                u.role,
                u.location_id,
                u.org_id,
                l.name AS location_name,
                o.name AS org_name
                FROM %s.users AS u
                  LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                  LEFT JOIN %s.organizations AS o ON o.uuid = u.org_id
                WHERE %s::TEXT = ANY(l.lineage::TEXT[])
                  AND u.status = 'ACTIVE'
                  AND u.role = 'REGIONAL_ADMIN';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
            ))
            results = await cur.fetchall()

            await cur.execute("""
                SELECT u.name,
                  u.id,
                    u.email,
                    u.role,
                    u.org_id,
                    l.name AS location_name,
                    o.name AS org_name
                FROM %s.assignments AS a
                  LEFT JOIN %s.users AS u ON u.id = a.user_id
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                WHERE u.role = 'USER'
                  AND u.status = 'ACTIVE'
                  AND a.status = 'ACTIVE'
                  AND %s::TEXT = ANY(l.lineage::TEXT[]);
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
            ))
            users = await cur.fetchall()

            pre_dict = dict((x.get("id"), x) for x in users)
            users = list(pre_dict.values())

            results = results + users

        return results

    @staticmethod
    async def get_activity(location_id, limit, offset, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.activity
                WHERE location_id = %s
                    ORDER BY created DESC
                    LIMIT %s
                    OFFSET %s
            """, (
                AsIs(user.get("tki")),
                location_id,
                limit,
                offset,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def get_geometry(location_id, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT geojson
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                location_id,
            ))
            result = await cur.fetchone()

        if result.get('geojson', None) is None:
            return None
        else:
            return result.get('geojson')

    @staticmethod
    async def geometry(location_id, tki=None):
        """ Retrieve geometry for a location

        Args:
            location_id:
            tki:

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT geojson
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(tki),
                location_id
            ))
            result = await cur.fetchone()

        return result.get('geojson', {})

    @staticmethod
    async def centroid(location_id, tki=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT public.ST_AsGeoJSON(public.ST_SetSRID(default_center, 3857)) as default_center, default_zoom
                        FROM %s.locations
                        WHERE uuid = %s
                    """, (
                AsIs(tki),
                location_id,
            ))
            result = await cur.fetchone()

        if result is None:
            return None
        else:
            return dict(result)

    @staticmethod
    async def point(location_id, tki=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT geojson
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(tki),
                location_id,
            ))
            result = await cur.fetchone()

        return result.get('geojson', {})

    @staticmethod
    async def get_organizations(location_id, user=None):
        results = []
        location = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, parent_id
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            location = await cur.fetchone()

            # Retrieve direct descendant locations
            await cur.execute("""
                SELECT l.uuid, l.name, l.parent_id, lt.name AS site_type
                FROM %s.locations AS l
                    LEFT JOIN %s.location_types AS lt ON l.site_type_id = lt.id
                WHERE l.parent_id = %s
                    AND l.status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                location_id,
            ))
            results = await cur.fetchall()

            # Retrieve reporting users
            await cur.execute("""
                SELECT u.id, u.name, u.email, u.user_type, f.name
                FROM _iw.users AS u
                    LEFT JOIN %s.assignments AS a ON a.user_id = u.id
                    LEFT JOIN %s.forms AS f ON f.id = a.form_id
                WHERE a.status = 'ACTIVE'
                    AND a.location_id = %s
                    AND u.status = 'ACTIVE'
                    AND f.status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
            ))
            results = results + await cur.fetchall()

    @staticmethod
    async def get_rps_locale(location_id, site_type_id, form_id, user=None):
        """ Get open reporting periods for a given form under a given location

        Args:
            location_id:
            site_type_id:
            form_id:
            user:

        Returns:

        """
        reporting_periods = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                    l.uuid::TEXT AS location_id,
                    l.name,
                    rp.form_id,
                    rp.start_date,
                    COALESCE(rp.end_date, NOW()::DATE) AS end_date,
                    l.status
                FROM %s.locations AS l
                  LEFT JOIN %s.location_reporting AS rp ON rp.location_id = l.uuid
                WHERE rp.form_id = %s
                  AND l.lineage::TEXT[] @> %s::TEXT[]
                  AND l.site_type_id = %s
                  AND l.status = 'ACTIVE'
                  AND rp.status = 'ACTIVE';
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                form_id,
                [location_id],
                site_type_id,
            ))
            reporting_periods = await cur.fetchall()

        return reporting_periods

    @staticmethod
    async def get_reporting_periods_for_form(location_id, form_id, user=None):
        """ Get reporting periods for a given form under and at a location

        Args:
            location_id:
            form_id:
            user:

        Returns:

        """
        reporting_periods = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                    l.uuid::TEXT as location_id,
                    l.name,
                    rp.form_id,
                    rp.start_date,
                    COALESCE(rp.end_date, NOW()::DATE) as end_date,
                    l.status
                FROM %s.locations AS l
                  LEFT JOIN %s.location_reporting AS rp ON rp.location_id = l.uuid
                WHERE rp.form_id = %s
                  AND lineage::TEXT[] @> %s::TEXT[];
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                form_id,
                [location_id],
            ))
            reporting_periods = await cur.fetchall()

        return reporting_periods

    @staticmethod
    async def get_type_name(location_id, user=None):
        """ Get a locations type name

        Args:
            location_id:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT t.name
                FROM %s.locations AS l
                  LEFT JOIN %s.location_types AS t ON l.site_type_id = t.id
                WHERE l.uuid = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                location_id,
            ))
            result = await cur.fetchone()

        if result is not None:
            return result.get("name", None)
        else:
            return None

    @staticmethod
    async def get_full_name(location_uuid, user=None):
        """Get the full path name of a location

        Args:
            location_uuid: The UUID of the location to get the path for
            user: The calling user

        Returns:
            A (,) delineated string of location names leading up to the
            location in question (e.g. Valhalla, Alpha District, Alpha Facility)
        """

        if location_uuid is None:
            return None

        location = None
        locations = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT name, lineage FROM %s.locations WHERE uuid::TEXT = %s
            """, (
                AsIs(user.get("tki")),
                str(location_uuid),
            ))
            location = await cur.fetchone()

            if location is None:
                return ""

            await cur.execute("""
                SELECT uuid::TEXT, name
                FROM %s.locations
                WHERE uuid::TEXT IN %s
            """, (
                AsIs(user.get("tki")),
                tuple(location.get("lineage")),
            ))
            locations = await cur.fetchall()

        loc_dict = dict((x.get("uuid"), x.get("name")) for x in locations)

        location_name = ""

        location_names = []

        for uuid in location.get("lineage"):
            if uuid in loc_dict.keys():
                location_names.append(loc_dict[uuid]['en'])

        return ", ".join(location_names)

    @staticmethod
    async def merge():
        return None

    @staticmethod
    async def get_merge_preview():
        return None

    @staticmethod
    async def get_location_tree():
        return None

    @staticmethod
    async def get_groups(user=None):
        result = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT groups
                FROM %s.locations
                WHERE groups IS NOT NULL
                  AND array_length(groups, 1) > 0;
            """, (
                AsIs(user.get("tki")),
            ))
            result = await cur.fetchall()

        groups = []
        for location in result:
            groups = groups + location.get("groups", [])

        unique = []
        for item in groups:
            if item not in unique:
                unique.append(item)

        return unique

    @staticmethod
    async def get_table_locations(data, user=None):
        tki = data.get('tki', None)

        if user is not None:
            tki = user.get('tki', None)

        status_sql = "AND l.status = 'ACTIVE'"
        if data.get('status', 'ACTIVE') == 'ANY':
            status_sql = "AND l.status = ANY(ARRAY['ACTIVE', 'INACTIVE', 'DISABLED']) "
        if data.get('status', 'ACTIVE') == 'INACTIVE':
            status_sql = " AND l.status = 'DISABLED'"
        if data.get('status', 'ACTIVE') == 'DISABLED':
            status_sql = " AND l.status = 'DISABLED'"

        results = []
        if data.get('type') == 'GENERATOR':
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT
                      l.uuid::TEXT,
                      l.name->>'en' AS name,
                      p.name->>'en' AS parent_name,
                      l.pcode
                    FROM %s.locations AS l
                      LEFT JOIN %s.locations AS p ON p.uuid = l.parent_id
                    WHERE l.site_type_id = %s
                      %s
                      AND l.lineage::TEXT[] @> %s::TEXT[];
                """, (
                    AsIs(tki),
                    AsIs(tki),
                    data.get('sti'),
                    AsIs(status_sql),
                    [data.get('parent')],
                ))
                results = await cur.fetchall()
        elif data.get('type') == 'SPECIFIC':
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT
                      l.uuid::TEXT,
                      l.name->>'en' AS name,
                      p.name->>'en' AS parent_name,
                      l.pcode
                    FROM %s.locations  AS l
                      LEFT JOIN %s.locations AS p ON p.uuid = l.parent_id
                    WHERE l.uuid::TEXT = %s;
                """, (
                    AsIs(tki),
                    AsIs(tki),
                    data.get('lid'),
                ))
                results = await cur.fetchall()
        elif data.get('type') == 'GROUP':
            results = await Location.get_locations_in_groups(data.get('groups'), user=dict(
                tki=tki
            ))

        return results

    @staticmethod
    async def get_locations_in_groups(group_ids, token=None, collapse=True, user=None):
        """ Retrieve all locations within a group(s)
        If collapsed, only the highest level locations will be used. For instance if
        a two locations have the same group, but one is under the other , the child
        location will be discarded

        Args:
            group_ids: A list of group ids to retrieve
            collapse: Whether to discard children of a grouped location sharing the same group
            user: The calling user

        Returns:

        """
        locations = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                  l.uuid::TEXT,
                  l.name->>'en' AS name,
                  l.lineage,
                  p.name->>'en' AS parent_name,
                  l.pcode
                FROM %s.locations  AS l
                  LEFT JOIN %s.locations AS p ON p.uuid = l.parent_id
                WHERE l.status = 'ACTIVE'
                  AND l.groups::TEXT[] @> %s::TEXT[];
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                group_ids,
            ))
            locations = await cur.fetchall()

        pre_dict = dict((x.get("uuid"), x) for x in locations)

        if not collapse:
            return locations

        removals = []

        for key, value in pre_dict.items():
            for root_key in pre_dict.keys():
                if root_key in value.get("lineage")[0:-1]:
                    removals.append(key)

        result = []

        for key, value in pre_dict.items():
            if key not in removals:
                result.append(value)

        return result

    @staticmethod
    async def get_geoms_for_spec(spec, user=None):
        result = None
        if spec.get('spec', 'DEFAULT') == 'DEFAULT':
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid, geojson
                        FROM %s.locations
                        WHERE uuid = %s;
                    """, (
                    AsIs(user.get('tki')),
                    spec.get('parent'),
                ))
                result = await cur.fetchone()

        return result

    @staticmethod
    async def search_locations(term, user=None):
        results = []

        print(AsIs("'%" + term + "%'"))
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT l.uuid, l.name, lti.name AS location_type,
                    l.site_type_id, l.geometry_type, l.status,
                    l.parent_id, l.geometry, l.point, l.groups,
                    l.status, l.lineage,
                    l.name::TEXT AS txt_leme,
                    (SELECT COUNT(ll.uuid) FROM %s.locations AS ll WHERE ll.lineage::UUID[] @> ARRAY[l.uuid]::UUID[]) AS children
                FROM %s.locations AS l
                    LEFT JOIN %s.location_types AS lti ON lti.id = l.site_type_id
                WHERE
                    l.name->>'en' ILIKE %s
                    OR l.pcode::TEXT ILIKE %s
                    OR l.groups::TEXT ILIKE %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs("'%" + term.strip() + "%'"),
                AsIs("'%" + term.strip() + "%'"),
                AsIs("'%" + term.strip() + "%'"),
            ))
            results = await cur.fetchall()

        for item in results:
            item['location_type'] = {
                "name": item['location_type']
            }

        return results
