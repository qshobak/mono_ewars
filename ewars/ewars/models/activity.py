import json
import datetime
import uuid

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

class ActivityModel(object):

    def __init__(self, tki=None):
        self.tki = tki

    @staticmethod
    def create(self, data):
        pass

    def delete(self, actid):
        pass

    def as_dict(self):
        return dict()




class Activity:
    @staticmethod
    async def create():
        pass

    @staticmethod
    async def remove():
        pass

    @staticmethod
    async def get_activity_stream(offset, user=None):
        """ get feed items that a given user should be able to see

        Args:
            offset:
            user:

        Returns:

        """
        results = []
        total = 0

        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT a.*, u.name
                FROM %s.activity_feed AS a
                  LEFT JOIN _iw.users AS u ON u.id = a.triggered_by
                ORDER BY a.created DESC
                LIMIT 10
                OFFSET %s;
            ''', (
                AsIs(user.get('tki')),
                offset,
            ))
            results = await cur.fetchall()

        for item in results:
            item_data = None
            if len(item.get('attachments')) > 0:
                item_type = item.get('attachments')[0][0]
                item_id = item.get('attachments')[0][1]

                if item_type == 'REPORT':
                    async with get_db_cur() as cur:
                        await cur.execute('''
                            SELECT f.name->>'en' AS form_name,
                                l.name->>'en' AS location_name,
                                c.data_date,
                                c.uuid,
                                f.features->'INTERVAL_REPORTING'->'interval' AS reporting_interval
                            FROM %s.collections AS c
                              LEFT JOIN %s.forms AS f ON f.id = c.form_id
                              LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                            WHERE c.uuid = %s;
                        ''', (
                            AsIs(user.get('tki')),
                            AsIs(user.get('tki')),
                            AsIs(user.get('tki')),
                            item_id,
                        ))
                        item_data = await cur.fetchone()
                        if item_data is not None:
                            item_data['type'] = 'REPORT'

                if item_type == 'USER':
                    async with get_db_cur() as cur:
                        await cur.execute('''
                            SELECT u.name, u.role, o.name AS org_name
                            FROM %s.users AS u
                              LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                            WHERE u.id = %s;
                        ''', (
                            AsIs(user.get('tki')),
                            item_id,
                        ))
                        item_data = await cur.fetchone()
                        if item_data is not None:
                            item_data['type'] = 'USER'

            item['meta'] = item_data

        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT COUNT(*) AS total FROM %s.activity_feed;
            ''', (
                AsIs(user.get('tki')),
            ))
            total = await cur.fetchone()
            total = total.get('total', 0)

        return dict(
            activity=results,
            count=total
        )
