import sys
import os
from enum import Enum
import base64
from openpyxl.writer.excel import save_virtual_workbook

import boto3

from ewars.core import notifications
from openpyxl import Workbook
from ewars.conf import settings

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

import tornado
import tornado.ioloop


class TemplateStatus(Enum):
    INACTIVE = 1
    ACTIVE = 2
    DRAFT = 3


DEFAULT_PERMISSIONS = {
    '*': ('VIEW', 'PDF', 'DOWNLOAD', 'SHARE', 'PRINT')
}


class Document:
    @staticmethod
    async def query(limit, offset, sorting, filters, user=None):
        return []

    @staticmethod
    async def save_template(data, user=None):
        pass

    @staticmethod
    async def update_template(tid, data, user=None):
        pass

    @staticmethod
    async def delete_template(tid, user=None):
        pass

    @staticmethod
    async def export_template(tid, user=None):
        pass

    @staticmethod
    async def import_template(data, user=None):
        pass

    @staticmethod
    async def export_data(instance_name, raws, sheets, user=None):
        wb = Workbook()

        ws = wb.active
        ws.title = "Single Values"

        for row in raws:
            ws.append(row)

        for sheet in sheets:
            new_sheet = wb.create_sheet(title=sheet[0])

            for row in sheet[1]:
                new_sheet.append(row)

        instance_name = "%s.xlsx" % instance_name
        save_name = base64.urlsafe_b64encode(instance_name.encode("utf-8"))

        wb.save(
            filename=os.path.join(settings.FILE_UPLOAD_TEMP_DIR, save_name.decode("utf-8") + ".xlsx")
        )

        return dict(
            n=save_name.decode("utf-8")
        )

    @staticmethod
    async def share(reportURI, recipients, instance_name, user=None):
        """  Share a document within the system

        Args:
            reportURI (str): The URI of the report
            recipients (str): The recipients to send the report out to
            user (obj): The sharing user

        Returns:
            True if the emails were sent
       """

        recipients = recipients.replace(" ", "").split(",")

        account = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT domain, name
                FROM _iw.accounts
                WHERE id = %s;
            """, (
                user.get('aid'),
            ))
            account = await cur.fetchone()

        # Iterate through recipients and send email
        cur_loop = tornado.ioloop.IOLoop.current()
        cur_loop.spawn_callback(
            notifications.send_bulk,
            'DOCUMENT_SHARE',
            recipients,
            dict(
                report_title=instance_name,
                uri="http://%s%s" % (
                    account.get('domain'),
                    reportURI
                ),
                sender_name=user['name'],
                sender_email=user['email']
            ),
            account=account.get('name'),
            user=user
        )

        return True
