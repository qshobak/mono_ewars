from ewars.core.api._base import BaseAPI

from ewars.server import seeding

async def test_method(user=None):
    return dict(
        description="HELLO",
        result=user.get("name")
    )

class COMMANDS(BaseAPI):
    _methods = {
        "com.sonoma.desktop.digest": None,
        "com.sonoma.test": test_method,

        # Get data from base system types
        "com.sonoma.seed.locations": seeding.get_locations_seed,
        "com.sonoma.seed.location_types": seeding.get_location_types_seed,
        "com.sonoma.seed.forms": seeding.get_forms_seed,
        "com.sonoma.seed.alarms": seeding.get_alarms_seed,
        "com.sonoma.seed.users": seeding.get_users_seed,
        "com.sonoma.seed.assignments": seeding.get_assignments_seed,
        "com.sonoma.seed.indicators": seeding.get_indicators_seed,
        "com.sonoma.seed.organizations": seeding.get_organizations,
        "com.sonoma.seed.dashboards": seeding.get_dashboards,
        "com.sonoma.seed.indicator_groups": seeding.get_indicator_groups,
        "com.sonoma.seed.huds": seeding.get_huds,
        "com.sonoma.seed.notebooks": seeding.get_notebooks,
        "com.sonoma.seed.maps": seeding.get_maps,
        "com.sonoma.seed.roles": seeding.get_roles,
        "com.sonoma.seed.tasks": seeding.get_tasks,
        "com.sonoma.seed.documents": seeding.get_documents,
        "com.sonoma.seed.plots": seeding.get_plots,
        "com.sonoma.seed.retractions": seeding.get_retractions,
        "com.sonoma.seed.reporting": seeding.get_location_reporting_seed,
        "com.sonoma.seed.config": seeding.get_config,
        "com.sonoma.seed.teams": seeding.get_teams,

        "com.sonoma.updates": None, # get updates from remote based on a ts_ms

        "com.sonoma.mbtiles": None, # Get list of available MBTILES
        "com.sonoma.mbtile": None, # Get an mbtile set

        # Get resources of a specific type
        "com.sonoma.seed.alerts": seeding.get_alerts,
        "com.sonoma.seed.records": seeding.seed_records,
        "com.sonoma.seed.records.oldest": seeding.get_oldest_date,
        "com.sonoma.seed.alerts.oldest": seeding.get_oldest_alert_date,

        "com.sonoma.desktop.push": None,
        "com.sonoma.desktop.pull": None

    }