from ewars.db import get_db_cur
from psycopg2.extensions import AsIs


async def get_forms_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.forms;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_users_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users;
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def get_locations_seed(user=None):
    locations = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.locations;
        """, (
            AsIs(user.get("tki")),
        ))
        locations = await cur.fetchall()

    return locations


async def get_location_reporting_seed(user=None):
    reporting = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.reporting;
        """, (
            AsIs(user.get("tki")),
        ))
        reporting = await cur.fetchall()

    return reporting


async def get_location_types_seed(user=None):
    location_types = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.location_types;
        """, (
            AsIs(user.get("tki")),
        ))
        location_types = await cur.fetchall()

    return location_types


async def get_indicators_seed(user=None):
    indicators = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.indicators;
        """, (
            AsIs(user.get('tki')),
        ))
        indicators = await cur.fetchall()

    return indicators


async def get_indicator_groups(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.indicator_groups;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_assignments_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.assignments;
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def get_alarms_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alarms;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_organizations(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.organizations;
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def seed_records(form_id, period, user=None):
    results = []

    start_date = period[0]
    end_date = period[1]

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * 
            FROM %s.records
            WHERE fid = %s 
            AND status = 'SUBMITTED'
            AND data_date >= %s 
            AND data_date <= %s
            ORDER BY submitted DESC;
        """, (
            AsIs(user.get('tki')),
            form_id,
            start_date,
            end_date,
        ))

        results = await cur.fetchall()

    return results


async def get_oldest_date(form_id, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT data_date 
            FROM %s.records
            WHERE fid = %s 
            AND status = 'SUBMITTED'
            LIMIT 1;
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        result = await cur.fetchone()

    if result is None:
        return None
    else:
        return result.get("data_date", None)


async def get_alerts(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.alerts;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_notebooks(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * 
            FROM %s.notebooks;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_documents(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.documents;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_plots(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.plots
        """, (
            AsIs(user.get("tki")),
        ))

        results = await cur.fetchall()

    return results


async def get_dashboards(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.dashboards;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_maps(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.maps;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_devices(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.devices;
        """, (
            AsIs(user.get("tki"))
        ))
        result = await cur.fetchall()

    return result


async def get_sites(user=None):
    return []


async def get_tasks(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.tasks;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_roles(user=None):
    return []


async def get_huds(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.huds;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_oldest_alert_date(alarm_id, user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT raised
            FROM %s.alerts
            WHERE alid = %s 
            ORDER BY raised DESC 
            LIMIT 1;
        """, (
            AsIs(user.get("tki")),
            alarm_id,
        ))
        result = await cur.fetchone()

    if result is not None:
        result = result.get("raised")
    else:
        result = None

    return result


async def get_retractions(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.retractions;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_config(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.conf;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results

async def get_teams(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.teams;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


