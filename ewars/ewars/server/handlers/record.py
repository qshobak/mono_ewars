import json

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

class Record:

    @staticmethod
    async def update(data, tki=None, device_id=None):
        record_data = data.get("data", {})

        cur_record = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, data, revisions, comments
                FROM %s.records 
                WHERE uuid = %s;
            """, (
                AsIs(tki),
                record_data.get("uuid"),
            ))
            cur_record = await cur.fetchone()

            cur_revisions = cur_record.get('revisions', [])
            if isinstance(cur_revisions, (dict,)):
                cur_revisions = []

            new_revisions = record_data.get("revisions", []) + cur_revisions
            new_revisions = dict({v['uuid']:v for v in new_revisions}.values())

            old_data = cur_record.get("data", {})
            old_data.update(record_data.get("data", {}))

            cur_comments = cur_record.get("commands", [])
            if isinstance(cur_comments, (dict,)):
                cur_comments = []

            new_comments = record_data.get("comments", []) + cur_comments
            new_comments = dict({v['uuid']:v for v in new_comments}.values())

            await cur.execute("""
                UPDATE %s.records
                SET data_date = %s,
                    location_id = %s,
                    modified = %s,
                    modified_by = %s,
                    data = %s,
                    revisions = %s,
                    comments = %s 
                WHERE uuid = %s;
            """, (
                AsIs(tki),
                record_data.get("data_date", None),
                record_data.get("location_id", None),
                record_data.get("modified"),
                record_data.get("modified_by"),
                json.dumps(old_data),
                json.dumps(new_revisions),
                json.dumps(new_comments),
                record_data.get("uuid"),
            ))

    @staticmethod
    async def delete(data, tki=None, device_id=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.records
                WHERE uuid = %s;
            """, (
                AsIs(tki),
                data.get("ref_id"),
            ))

    @staticmethod
    async def create(data, tki=None, device_id=None):
        record_data = data.get("data", {})
        exits = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid FROM %s.records WHERE uuid = %s;
            """, (
                AsIs(tki),
                record_data.get("uuid"),
            ))
            exists = await cur.fetchone()

            if exists is None:
                await cur.execute("""
                    INSERT INTO %s.records
                    (uuid, status, fid, fv_id, location_id, data_date, data, submitted, submitted_by, created, created_by, modified, modified_by, revisions, comments, eid, import_id, source)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(tki),
                    record_data.get("uuid"),
                    "SUBMITTED",
                    record_data.get("fid"),
                    record_data.get("fv_id"),
                    record_data.get("location_id", None),
                    record_data.get("data_date", None),
                    json.dumps(record_data.get("data", {})),
                    record_data.get("submitted"),
                    record_data.get("submitted_by"),
                    record_data.get("created"),
                    record_data.get("created_by"),
                    record_data.get("modified"),
                    record_data.get("modified_by"),
                    json.dumps(record_data.get("revisions", [])),
                    json.dumps(record_data.get("comments", [])),
                    record_data.get("eid", None),
                    record_data.get("import_id", None),
                    "DESKTOP",
                ))
            else:
                # TODO: Conflict task
                pass