import json
import uuid
import time
import datetime

from ewars.db import get_db_cur
from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs


async def get_subscription_seed(sub_type, id, user=None):
    """ Get an initial seeding package for a given resource
    
    For instance, the user may request to get the seed package for a given data collection.
    The system will use COPY TO to create a downloadable file containing the seed data and return a 
    URL to the file as well as an event log id and timestamp which the downloadable file is consistent up to.
    
    This allows the users desktop application to parse and create a local log and data store for the data.
    
    Args:
        sub_type: The type of subscription (FORM, ALARM, USER, LOCATIONS)
        id: An id identifying the resource being subscribed to (form_id, alarm id), this dictates where the event log is
        user: The calling user

    Returns:

    """
    pass


async def _get_account_set(aid, uid, tki=None):
    """ Get an initial manifest for a user to allow them to download seed data for a given account that they have access to
    
    Args:
        aid: The account id to get the seed package for
        uid: The user id of the user within that account
        tki: The account schema token to use 

    Returns:

    """
    acc_user = None

    forms = None
    form_versions = None
    locations = []
    location_types = None
    location_reporting = None
    alarms = None
    account = None

    ds_subs = []
    alert_subs = []
    locations = []
    assignments = []

    subscriptions = []

    # Get the users acc local user account
    async with get_db_cur() as cur:
        await cur.execute('''
            SELECT * FROM %s.users
            WHERE id = %s;
        ''', (
            AsIs(tki),
            uid,
        ))
        acc_user = await cur.fetchone()

        await cur.execute('''
            SELECT uuid, name, parent_id, lineage, status  FROM %s.locations;
        ''', (
            AsIs(tki),
        ))
        locations = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM _iw.accounts
            WHERE id = %s;
        ''', (
            aid,
        ))
        account = await cur.fetchone()

        await cur.execute('''
            SELECT id, name 
            FROM %s.location_types
        ''', (
            AsIs(tki),
        ))
        location_types = await cur.fetchall()

    if acc_user.get('role') == 'USER':
        # Get their assignments
        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT *
                FROM %s.assignments
                WHERE user_id = %s;
            ''', (
                AsIs(tki),
                uid,
            ))
            assignments = await cur.fetchall()

    async with get_db_cur() as cur:
        await cur.execute('''
            SELECT * 
            FROM %s.forms;
        ''', (
            AsIs(tki),
        ))
        forms = await cur.fetchall()

        await cur.execute('''
            SELECT uuid, definition, form_id
            FROM %s.form_versions;
        ''', (
            AsIs(tki),
        ))
        form_versions = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.alarms_v2;
        ''', (
            AsIs(tki),
        ))
        alarms = await cur.fetchall()

    locations = '*'
    if acc_user.get('role') == 'USER':
        locations = [x.get('location_id') for x in assignments]

    if acc_user.get('role') == 'REGIONAL_ADMIN':
        locations = [acc_user.get('location_id')]

    for alarm in alarms:
        subscriptions.append(dict(
            type='ALARM',
            id=alarm.get('uuid'),
            name=alarm.get('name'),
            locations=locations
        ))

    for form in forms:
        subscriptions.append(dict(
            type='FORM',
            id=form.get('id'),
            versions=[[x.get('uuid'), x.get('definition')] for x in form_versions if x.get('form_id') == form.get('id')],
            name=form.get('name'),
            version=form.get('version_id'),
            features=form.get('features'),
            status=form.get('status'),
            locations=[[x.get('location_id'), 'RW', x.get('start_date'), x.get('end_date'), x.get('status')] for x in
                       assignments if x.get('form_id') == form.get('id')]
        ))

    subscriptions.append(dict(
        type='LOCATIONS'
    ))

    subscriptions.append(dict(
        type='ALARMS'
    ))

    return dict(
        assignments=assignments,
        user=acc_user,
        account=account,
        subscriptions=subscriptions,
        locations=locations,
        location_types=location_types
    )


async def get_seed_manifest(user=None):
    """ Given a user, find out what data they should have seeded to them and return a description of their account
    
    Args:
        user: 

    Returns:

    """

    results = await _get_account_set(
        user.get('aid'),
        user.get('id'),
        tki=user.get('tki')
    )

    return results
