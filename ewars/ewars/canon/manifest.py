from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def get_manifest(user=None):
    """ get a seed manifest

    Args:
        user:

    Returns:

    """
    manifest = dict(
        resources=[
            'location',
            'location_type',
            'alarm',
            'assignment',
            'user',
            'indicator',
            'indicator_group',
            'notebook',
            'task',
            'workflow',
            'organization',
            'conf',
            'location_reporting',
            'workflow'
        ]
    )

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name 
            FROM %s.forms 
            WHERE status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
        ))
        manifest['forms'] = await cur.fetchall()

        await cur.execute("""
            SELECT uuid, name 
            FROM %s.data_sets;
        """, (
            AsIs(user.get('tki')),
        ))
        manifest['datasets'] = await cur.fetchall()

    manifest['account'] = dict(
        id=user.get('aid'),
        tki=user.get('tki'),
        name=user.get('account_name')
    )

    manifest['user'] = user

    return manifest
