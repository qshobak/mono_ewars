import json

from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder

from psycopg2.extensions import AsIs


async def _get_user_alerts(user=None):
    results = dict(
        open=[],
        closed=[]
    )

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * 
            FROM %s.assignments
            WHERE user_id = %s;
        """, (
            AsIs(user.get('tki')),
            user.get('id'),
        ))
        assignments = await cur.fetchall()

    locations = set([x.get('location_id') for x in assignments])

    alerts = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*, al.*
            FROM %s.alerts AS a 
              LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
            WHERE a.location_id = ANY(%s)
            AND al.status = 'ACTIVE';
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
            locations,
        ))
        alerts = await cur.fetchall()

    results['open'] = list(filter(lambda k: k['state'] == 'OPEN', alerts))
    results['closed'] = list(filter(lambda k: k['state'] == 'CLOSED', alerts))

    return results


async def _get_all_alerts(user=None):

    results = dict(
        open=[],
        closed=[]
    )

    alerts = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
                a.*, 
                al.*, 
                l.name->>'en' AS location_name
            FROM %s.alerts AS a 
              LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id 
              LEFT JOIN %s.locations AS l ON l.uuid = a.location_id;
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        alerts = await cur.fetchall()

    results['open'] = list(filter(lambda k: k['state'] == 'OPEN', alerts))
    results['closed'] = list(filter(lambda k: k['state'] == 'CLOSED', alerts))

    return results


async def get_alerts(user=None):
    results = dict(
        open=[],
        closed=[]
    )
    if user.get('role', None) == 'USER':
        results = await _get_user_alerts(user=user)
    elif user.get('role', None) == 'ACCOUNT_ADMIN':
        results = await _get_all_alerts(user=user)

    return results
