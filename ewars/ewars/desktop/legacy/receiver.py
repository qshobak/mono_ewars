"""

EWARS Global receiver

These APIs are responsible for receiving events from remote clients

The types of events that can be sent vary



"""


from ewars.db import get_db_cur

EVENT_TYPES = dict()

class EventHandler:
    """ Event Handling class for incoming remotes

    """

    @staticmethod
    async def receive(did, events, user=None):
        """ Receive a stream of events from a remote node

        Args:
            did: The device id that is calling
            events: The list of events that need to be applied to the system
            user: The remote user calling

        Returns:

        """

        return True

    @staticmethod
    def _handle_submission(user=None):
        """ Handle the submission of a report

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_retraction(user=None):
        """ Handle an record retraction event

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_task_action(user=None):
        """ Handle an action on a task

        Args:
            user:

        Returns:

        """

        pass

    @staticmethod
    def _handle_location_update(user=None):
        """ Handle an update to a location

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_location_creation(user=None):
        """ Handle the creation of a location

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_import(user=None):
        """ Handle the import of some data

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_user_update(user=None):
        """ Handle the update of a user

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    def _handle_amendment(user=None):
        """ Handle an amendment to a record

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    async def _handle_assignment_request(user=None):
        """ Handle a request for a new assignment

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    async def _handle_assignment_addition(user=None):
        """ Handle adding an assignment to a user

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    async def _handle_kill_assignment(user=None):
        """ A user can kill their own assignment if they are no longer
        responsible for a given location and form

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    async def _handle_alert_action(user=None):
        """ Handle an action take on an alert

        If the alert is proxy to pre-existing alert, then we apply changes to the root alert,
        if the change is blocked on the root alert, then we attach the additional info and
        generate a task for admins to resolve the issue.

        Args:
            user:

        Returns:

        """
        pass

    @staticmethod
    async def _handle_alert(user=None):
        """ Handle the creation of an alert

        If the same alert exists on global, we don't recreate it, we tag the alert with the
        duplicate alerts uuid and apply any changes coming in later to the root alert

        Args:
            user:

        Returns:

        """
