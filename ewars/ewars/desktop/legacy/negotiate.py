from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def negotiate(remote_event_id, remote_root_id, user_id, client_id, user=None):
    pass


async def seed(user=None):
    async with get_db_cur() as cur:
        await cur.execute('''
            SELECT * FROM %s.assignments
            WHERE user_id = %s;
        ''', (
            AsIs(user.get('tki')),
            user.get('id'),
        ))
        assignments = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.locations;
        ''', (
            AsIs(user.get('tki')),
        ))
        locations = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.location_types;
        ''', (
            AsIs(user.get('tki')),
        ))
        location_types = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.forms
            WHERE status = 'ACTIVE';
        ''', (
            AsIs(user.get('tki')),
        ))
        forms = await cur.fetchall()

        await cur.execute('''
             SELECT * FROM %s.form_versions
        ''', (
            AsIs(user.get('tki')),
        ))
        form_versions = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.collections
            WHERE status = 'SUBMITTED'
            ORDER BY submitted_date DESC
            LIMIT 1000;
        ''', (
            AsIs(user.get('tki')),
        ))
        submissions = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM %s.alarms_v2;
        ''', (
            AsIs(user.get('tki')),
        ))
        alarms = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM _iw.users
            WHERE id = %s;
        ''', (
            user.get('id'),
        ))
        end_user = await cur.fetchone()

        await cur.execute('''
            SELECT id, name, email, org_id
            FROM %s.users;
        ''', (
            AsIs(user.get('tki')),
        ))
        peers = await cur.fetchall()

        await cur.execute('''
            SELECT * FROM _iw.accounts
            WHERE id = ANY(%s)
              AND status = 'ACTIVE';
        ''', (
            AsIs(user.get('tki')),
            user.get('accounts'),
        ))
        accounts = await cur.fetchall()

    datapack = dict(
        assignments=assignments,
        forms=forms,
        locations=locations,
        submissions=submissions,
        form_versions=form_versions,
        location_types=location_types,
        alarms=alarms,
        user=end_user,
        accounts=accounts,
        peers=peers
    )

    return datapack


async def push(client_id, data, user=None):
    pass
