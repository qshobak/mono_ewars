--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ewars; Type: SCHEMA; Schema: -; Owner: ewars
--

CREATE SCHEMA IF NOT EXISTS __SCHEMA__;


ALTER SCHEMA __SCHEMA__ OWNER TO ewars;

SET search_path = __SCHEMA__, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activity; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE activity (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    situation_uuid uuid,
    content json DEFAULT '{}'::json NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    account_id integer,
    icon text DEFAULT 'fa-bullhorn'::text,
    event_type text DEFAULT 'GENERAL'::text NOT NULL,
    group_id integer,
    user_id integer,
    lab_id uuid,
    location_id uuid
);


ALTER TABLE activity OWNER TO ewars;

--
-- Name: alarms; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE alarms (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json NOT NULL,
    status boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    indicator_id uuid,
    comparator text DEFAULT 'EQ'::text NOT NULL,
    description json DEFAULT '{"en": ""}'::json,
    actions json DEFAULT '{}'::json,
    date_spec text DEFAULT 'TRIGGER'::text NOT NULL,
    date_spec_interval_type text DEFAULT 'WEEK'::text NOT NULL,
    date_spec_intervals numeric,
    date_spec_aggregation text,
    location_spec text DEFAULT 'LOCATION_TYPE'::text NOT NULL,
    location_spec_type text,
    location_spec_parent text,
    location_spec_uuid text,
    data_source_type text DEFAULT 'INDICATOR'::text NOT NULL,
    data_aggregation text DEFAULT 'SUM'::text NOT NULL,
    data_modifier numeric,
    data_formula text,
    data_sources json,
    comparator_value numeric,
    account_id integer,
    comparator_source text DEFAULT 'VALUE'::text,
    comparator_modifier numeric,
    comparator_definition json DEFAULT '{}'::json,
    indicator_definition json,
    recovery_enabled boolean DEFAULT false NOT NULL,
    recovery_interval_type text,
    recovery_interval_period integer,
    inv_enabled boolean DEFAULT false NOT NULL,
    inv_form_ids integer[],
    inv_outcomes json
);


ALTER TABLE alarms OWNER TO ewars;

--
-- Name: alert_actions; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE alert_actions (
    id integer NOT NULL,
    alert_id uuid NOT NULL,
    type text NOT NULL,
    user_id integer NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    data json DEFAULT '{}'::json,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    submitted_date timestamp without time zone
);


ALTER TABLE alert_actions OWNER TO ewars;

--
-- Name: alert_actions_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE alert_actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_actions_id_seq OWNER TO ewars;

--
-- Name: alert_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE alert_actions_id_seq OWNED BY alert_actions.id;


--
-- Name: alert_events; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE alert_events (
    id integer NOT NULL,
    action_date timestamp without time zone DEFAULT now() NOT NULL,
    action_type text,
    icon text DEFAULT 'fa-exclamation-circle'::text NOT NULL,
    node_type text DEFAULT 'ACTION'::text NOT NULL,
    user_id integer,
    content text,
    alert_id uuid NOT NULL
);


ALTER TABLE alert_events OWNER TO ewars;

--
-- Name: alert_events_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE alert_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_events_id_seq OWNER TO ewars;

--
-- Name: alert_events_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE alert_events_id_seq OWNED BY alert_events.id;


--
-- Name: alert_users; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE alert_users (
    id integer NOT NULL,
    alert_id uuid NOT NULL,
    user_id integer NOT NULL,
    date_added timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE alert_users OWNER TO ewars;

--
-- Name: alert_users_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE alert_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_users_id_seq OWNER TO ewars;

--
-- Name: alert_users_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE alert_users_id_seq OWNED BY alert_users.id;


--
-- Name: alerts; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE alerts (
    uuid uuid NOT NULL,
    alarm_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    state text DEFAULT 'OK'::text NOT NULL,
    indicator_id uuid,
    location_id uuid,
    no_trigger_interval text,
    no_trigger_period numeric,
    trigger_start date DEFAULT now() NOT NULL,
    trigger_end date DEFAULT now() NOT NULL,
    triggering_user integer,
    action_taken text,
    actioned_date date,
    actioned_by integer,
    action_reason text,
    source text DEFAULT 'SYSTEM'::text NOT NULL,
    triggering_report_id uuid,
    indicator_definition json,
    investigation_id uuid,
    stage text DEFAULT 'VERIFICATION'::text NOT NULL,
    stage_state text DEFAULT 'COMPLETE'::text,
    outcome text,
    risk text
);


ALTER TABLE alerts OWNER TO ewars;

--
-- Name: COLUMN alerts.created; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN alerts.created IS 'This is assumed to be the first date the alert was in NOT OK state';


--
-- Name: indicators; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE indicators (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    group_id integer NOT NULL,
    name json DEFAULT '{"en": "New Indicator"}'::json NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    protected boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    ranked boolean DEFAULT false,
    colour text,
    itype text DEFAULT 'DEFAULT'::text,
    definition json,
    description json DEFAULT '{}'::json,
    guidance json DEFAULT '{}'::json,
    accounts integer[],
    icode text,
    account_id integer
);


ALTER TABLE indicators OWNER TO ewars;

--
-- Name: locations; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE locations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json,
    location_type text DEFAULT 'ADMIN'::text NOT NULL,
    pcode text,
    description text DEFAULT ''::text,
    parent_id uuid,
    lineage text[] NOT NULL,
    data json,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    geometry public.geometry(MultiPolygon,3857),
    point public.geometry(Point,3857),
    geometry_type text DEFAULT 'ADMIN'::text NOT NULL,
    site_type_id integer DEFAULT 1,
    default_center public.geometry(Point,3857),
    default_zoom integer DEFAULT 10,
    groups text[],
    created_date date DEFAULT now() NOT NULL
);


ALTER TABLE locations OWNER TO ewars;

--
-- Name: alerts_full; Type: VIEW; Schema: ewars; Owner: jduren
--

CREATE VIEW alerts_full AS
 SELECT a.uuid,
    a.alarm_id,
    a.created,
    a.state,
    a.indicator_id,
    a.location_id,
    a.no_trigger_interval,
    a.no_trigger_period,
    a.trigger_start,
    a.trigger_end,
    a.triggering_user,
    a.action_taken,
    a.actioned_date,
    a.actioned_by,
    a.action_reason,
    a.source,
    a.triggering_report_id,
    a.indicator_definition,
    a.investigation_id,
    a.stage,
    a.stage_state,
    a.outcome,
    a.risk,
    m.name AS alarm_name,
    m.account_id,
    ac.name AS account_name,
    l.name AS location_name,
    l.lineage
   FROM alerts a
     LEFT JOIN alarms m ON m.uuid = a.alarm_id
     LEFT JOIN indicators i ON i.uuid = m.indicator_id
     LEFT JOIN _iw.accounts ac ON ac.id = m.account_id
     LEFT JOIN locations l ON l.uuid = a.location_id;


ALTER TABLE alerts_full OWNER TO jduren;

--
-- Name: amendments; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE amendments (
    id integer NOT NULL,
    report_id uuid,
    original json,
    amended json,
    amended_date timestamp without time zone DEFAULT now() NOT NULL,
    amendment_reason text,
    status text DEFAULT 'PENDING'::text NOT NULL,
    form_id integer NOT NULL,
    amended_by integer NOT NULL
);


ALTER TABLE amendments OWNER TO ewars;

--
-- Name: amendments_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE amendments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE amendments_id_seq OWNER TO ewars;

--
-- Name: amendments_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE amendments_id_seq OWNED BY amendments.id;


--
-- Name: analysis_notebooks; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE analysis_notebooks (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json NOT NULL,
    description text NOT NULL,
    public boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL
);


ALTER TABLE analysis_notebooks OWNER TO ewars;

--
-- Name: assignments; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE assignments (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'UNAPPROVED'::text NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    location_id uuid,
    form_id integer,
    start_date date,
    end_date date
);


ALTER TABLE assignments OWNER TO ewars;

--
-- Name: collections; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE collections (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    submitted_date date,
    form_id integer NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    data jsonb DEFAULT '{}'::json NOT NULL,
    form_version_id uuid NOT NULL,
    data_date date DEFAULT now(),
    location_id uuid,
    import_set uuid,
    import_data json DEFAULT '{}'::json,
    source text DEFAULT 'SYSTEM'::text,
    history json DEFAULT '[]'::json NOT NULL,
    attachments json,
    revisions json,
    account_id integer,
    alert_id uuid,
    submitted timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE collections OWNER TO ewars;

--
-- Name: collections_taxonomies; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE collections_taxonomies (
    tax_id uuid NOT NULL,
    collection_id uuid NOT NULL
);


ALTER TABLE collections_taxonomies OWNER TO ewars;

--
-- Name: completeness; Type: VIEW; Schema: ewars; Owner: jduren
--

CREATE VIEW completeness AS
 SELECT c.data_date,
    count(*) AS total,
    c.location_id,
    c.form_id
   FROM collections c
  WHERE (c.status = 'SUBMITTED'::text)
  GROUP BY c.data_date, c.location_id, c.form_id;


ALTER TABLE completeness OWNER TO jduren;

--
-- Name: conf; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE conf (
    id integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    modified_by integer NOT NULL,
    aid integer NOT NULL,
    grp text DEFAULT 'NONE'::text NOT NULL
);


ALTER TABLE conf OWNER TO jdu;

--
-- Name: conf_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE conf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conf_id_seq OWNER TO jdu;

--
-- Name: conf_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE conf_id_seq OWNED BY conf.id;


--
-- Name: datasets; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE datasets (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    description text NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL
);


ALTER TABLE datasets OWNER TO jduren;

--
-- Name: denorm; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE denorm (
    fid integer NOT NULL,
    cid uuid NOT NULL,
    dd date,
    lid uuid,
    key text NOT NULL,
    val text
);


ALTER TABLE denorm OWNER TO jdu;

--
-- Name: devices; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE devices (
    id integer NOT NULL,
    device_type text DEFAULT 'UNKNOWN'::text,
    device_id text NOT NULL,
    device_os_version text,
    device_lat text,
    device_lng text,
    device_number text,
    gate_id integer,
    user_id integer,
    last_seen timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    app_version text,
    app_version_name text,
    android_version text,
    device_name text,
    account_id integer
);


ALTER TABLE devices OWNER TO ewars;

--
-- Name: COLUMN devices.device_type; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.device_type IS 'The OS type of the device ANDROID|IOS|WINDOWS';


--
-- Name: COLUMN devices.device_id; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.device_id IS 'A unique device ID (i.e. the IMEI)';


--
-- Name: COLUMN devices.device_os_version; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.device_os_version IS 'The version of the phone operating system that is being run';


--
-- Name: COLUMN devices.gate_id; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.gate_id IS 'The id of the SMS gateway that this phone should use';


--
-- Name: COLUMN devices.user_id; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.user_id IS 'The is of the user that this phone is assigned to';


--
-- Name: COLUMN devices.last_seen; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.last_seen IS 'The last time that this phone connected with EWARS';


--
-- Name: COLUMN devices.status; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN devices.status IS 'The status of the phone ACTIVE|INACTIVE|BLOCKED';


--
-- Name: devices_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devices_id_seq OWNER TO ewars;

--
-- Name: devices_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE devices_id_seq OWNED BY devices.id;


--
-- Name: discussion_thread_messages; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE discussion_thread_messages (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id integer NOT NULL,
    discussion_id integer NOT NULL,
    content text NOT NULL,
    created timestamp without time zone DEFAULT now()
);


ALTER TABLE discussion_thread_messages OWNER TO ewars;

--
-- Name: discussion_threads; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE discussion_threads (
    id integer NOT NULL,
    node_type text NOT NULL,
    node_id text NOT NULL,
    email boolean DEFAULT false,
    sms boolean DEFAULT false
);


ALTER TABLE discussion_threads OWNER TO ewars;

--
-- Name: discussions_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE discussions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discussions_id_seq OWNER TO ewars;

--
-- Name: discussions_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE discussions_id_seq OWNED BY discussion_threads.id;


--
-- Name: distributions; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE distributions (
    id integer NOT NULL,
    name text NOT NULL,
    data jsonb DEFAULT '[]'::jsonb NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL
);


ALTER TABLE distributions OWNER TO jdu;

--
-- Name: distributions_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE distributions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE distributions_id_seq OWNER TO jdu;

--
-- Name: distributions_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE distributions_id_seq OWNED BY distributions.id;


--
-- Name: feed_items; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE feed_items (
    id integer NOT NULL,
    title text NOT NULL,
    sef text NOT NULL,
    feeds integer[] NOT NULL,
    content text NOT NULL,
    content_type text DEFAULT 'MARKDOWN'::text NOT NULL,
    keys text[],
    status text DEFAULT 'DRAFT'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    publish_date timestamp without time zone DEFAULT now() NOT NULL,
    access text[]
);


ALTER TABLE feed_items OWNER TO jdu;

--
-- Name: feed_items_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE feed_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_items_id_seq OWNER TO jdu;

--
-- Name: feed_items_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE feed_items_id_seq OWNED BY feed_items.id;


--
-- Name: feeds; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE feeds (
    id integer NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE feeds OWNER TO jdu;

--
-- Name: feeds_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE feeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feeds_id_seq OWNER TO jdu;

--
-- Name: feeds_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE feeds_id_seq OWNED BY feeds.id;


--
-- Name: form_versions; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE form_versions (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    form_id integer,
    version integer DEFAULT 1 NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    logic json DEFAULT '[]'::json NOT NULL,
    dia json DEFAULT '{}'::json,
    etl jsonb DEFAULT '[]'::jsonb NOT NULL
);


ALTER TABLE form_versions OWNER TO ewars;

--
-- Name: forms; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE forms (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    version_id uuid,
    location_aware boolean DEFAULT false NOT NULL,
    time_interval text DEFAULT 'NONE'::text NOT NULL,
    single_report_context boolean DEFAULT false,
    allowed_accounts text[],
    location_type text[],
    is_lab boolean DEFAULT false,
    block_future_dates boolean DEFAULT false,
    overdue_threshold numeric,
    overdue_interval text DEFAULT 'DAY'::text,
    guidance json DEFAULT '{}'::json,
    description json,
    ftype text DEFAULT 'PRIMARY'::text NOT NULL,
    account_id integer,
    site_type_id integer,
    features jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE forms OWNER TO ewars;

--
-- Name: forms_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forms_id_seq OWNER TO ewars;

--
-- Name: forms_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE forms_id_seq OWNED BY forms.id;


--
-- Name: groups; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE groups (
    id integer NOT NULL,
    name text NOT NULL,
    public boolean DEFAULT true
);


ALTER TABLE groups OWNER TO ewars;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_id_seq OWNER TO ewars;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: guidance; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE guidance (
    id integer NOT NULL,
    token text NOT NULL,
    title json DEFAULT '{}'::json NOT NULL,
    content json DEFAULT '{}'::json,
    active boolean DEFAULT false,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL
);


ALTER TABLE guidance OWNER TO ewars;

--
-- Name: guidance_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE guidance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE guidance_id_seq OWNER TO ewars;

--
-- Name: guidance_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE guidance_id_seq OWNED BY guidance.id;


--
-- Name: indicator_groups; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE indicator_groups (
    id integer NOT NULL,
    name json NOT NULL,
    parent_id integer
);


ALTER TABLE indicator_groups OWNER TO ewars;

--
-- Name: indicator_groups_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE indicator_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE indicator_groups_id_seq OWNER TO ewars;

--
-- Name: indicator_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE indicator_groups_id_seq OWNED BY indicator_groups.id;


--
-- Name: investigations; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE investigations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    opened timestamp without time zone DEFAULT now() NOT NULL,
    closed timestamp without time zone
);


ALTER TABLE investigations OWNER TO ewars;

--
-- Name: jobs; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE jobs (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    last_run timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE jobs OWNER TO jduren;

--
-- Name: kb_article_attachments; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE kb_article_attachments (
    id integer NOT NULL,
    article_id integer NOT NULL,
    name text NOT NULL,
    file_token text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL
);


ALTER TABLE kb_article_attachments OWNER TO ewars;

--
-- Name: kb_article_attachments_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE kb_article_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kb_article_attachments_id_seq OWNER TO ewars;

--
-- Name: kb_article_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE kb_article_attachments_id_seq OWNED BY kb_article_attachments.id;


--
-- Name: kb_articles; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE kb_articles (
    id integer NOT NULL,
    slug text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    section_id integer NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    parent_id integer,
    views integer DEFAULT 0 NOT NULL,
    title json DEFAULT '{"en": "Article Title"}'::json,
    keywords text,
    featured boolean DEFAULT false,
    content json DEFAULT '{"en": ""}'::json
);


ALTER TABLE kb_articles OWNER TO ewars;

--
-- Name: kb_articles_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE kb_articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kb_articles_id_seq OWNER TO ewars;

--
-- Name: kb_articles_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE kb_articles_id_seq OWNED BY kb_articles.id;


--
-- Name: kb_sections; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE kb_sections (
    id integer NOT NULL,
    slug text NOT NULL,
    icon text DEFAULT 'fa-bookmark-o'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    colour text DEFAULT '#86898f'::text NOT NULL,
    name json DEFAULT '{"en": ""}'::json,
    description json DEFAULT '{"en": ""}'::json
);


ALTER TABLE kb_sections OWNER TO ewars;

--
-- Name: kb_sections_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE kb_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kb_sections_id_seq OWNER TO ewars;

--
-- Name: kb_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE kb_sections_id_seq OWNED BY kb_sections.id;


--
-- Name: layouts; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE layouts (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    layout_type text DEFAULT 'DASHBOARD'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    modified_by integer NOT NULL,
    description json DEFAULT '{}'::json NOT NULL,
    access text[],
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    account_id integer NOT NULL,
    locked boolean DEFAULT false,
    color text
);


ALTER TABLE layouts OWNER TO jduren;

--
-- Name: location_gis; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE location_gis (
    id integer NOT NULL,
    location_id uuid NOT NULL,
    geom public.geometry(Geometry,3857)
);


ALTER TABLE location_gis OWNER TO ewars;

--
-- Name: location_gis_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE location_gis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE location_gis_id_seq OWNER TO ewars;

--
-- Name: location_gis_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE location_gis_id_seq OWNED BY location_gis.id;


--
-- Name: location_reporting; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE location_reporting (
    id integer NOT NULL,
    location_id uuid NOT NULL,
    form_id integer NOT NULL,
    start_date date DEFAULT now() NOT NULL,
    end_date date,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    type text,
    submissions integer DEFAULT 0 NOT NULL
);


ALTER TABLE location_reporting OWNER TO ewars;

--
-- Name: location_reporting_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE location_reporting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE location_reporting_id_seq OWNER TO ewars;

--
-- Name: location_reporting_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE location_reporting_id_seq OWNED BY location_reporting.id;


--
-- Name: location_types; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE location_types (
    id integer NOT NULL,
    name json DEFAULT '{}'::json,
    description text,
    status text DEFAULT 'ACTIVE'::text,
    created timestamp without time zone DEFAULT now(),
    created_by integer DEFAULT 3 NOT NULL,
    hid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE location_types OWNER TO ewars;

--
-- Name: location_types_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE location_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE location_types_id_seq OWNER TO ewars;

--
-- Name: location_types_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE location_types_id_seq OWNED BY location_types.id;


--
-- Name: logs; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE logs (
    log_type text NOT NULL,
    log_date timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer
);


ALTER TABLE logs OWNER TO jduren;

--
-- Name: maps; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE maps (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    locked boolean DEFAULT false,
    account_id integer NOT NULL,
    shared boolean DEFAULT false
);


ALTER TABLE maps OWNER TO jduren;

--
-- Name: messages; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE messages (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    recipient integer NOT NULL,
    lineage uuid[] NOT NULL,
    sender integer NOT NULL,
    subject text,
    thread json DEFAULT '[]'::json NOT NULL,
    sent timestamp without time zone DEFAULT now() NOT NULL,
    read boolean DEFAULT false NOT NULL,
    state text DEFAULT 'ON'::text NOT NULL,
    message_type text DEFAULT 'NOTIFICATION'::text NOT NULL
);


ALTER TABLE messages OWNER TO jduren;

--
-- Name: news_categories; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE news_categories (
    id integer NOT NULL,
    name json DEFAULT '{}'::json NOT NULL
);


ALTER TABLE news_categories OWNER TO ewars;

--
-- Name: news_categories_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE news_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE news_categories_id_seq OWNER TO ewars;

--
-- Name: news_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE news_categories_id_seq OWNED BY news_categories.id;


--
-- Name: news_posts; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE news_posts (
    id integer NOT NULL,
    title text NOT NULL,
    slug text NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    excerpt text NOT NULL,
    content text NOT NULL,
    published_date timestamp without time zone DEFAULT now() NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    history json DEFAULT '{}'::json NOT NULL,
    category_id integer DEFAULT 1
);


ALTER TABLE news_posts OWNER TO ewars;

--
-- Name: news_posts_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE news_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE news_posts_id_seq OWNER TO ewars;

--
-- Name: news_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE news_posts_id_seq OWNED BY news_posts.id;


--
-- Name: notebooks; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE notebooks (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    shared boolean DEFAULT false,
    account_id integer NOT NULL
);


ALTER TABLE notebooks OWNER TO jduren;

--
-- Name: ops; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE ops (
    key_name text NOT NULL,
    value text NOT NULL
);


ALTER TABLE ops OWNER TO ewars;

--
-- Name: plots; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE plots (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    shared boolean DEFAULT false NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE plots OWNER TO jduren;

--
-- Name: reporting; Type: VIEW; Schema: ewars; Owner: jduren
--

CREATE VIEW reporting AS
 SELECT l.uuid,
    l.name,
    l.lineage,
    l.status AS location_status,
    l.site_type_id,
    l.parent_id,
    r.id,
    r.form_id,
    r.start_date,
    COALESCE(r.end_date, ('now'::text)::date) AS end_date,
    r.type,
    r.status
   FROM (locations l
     LEFT JOIN location_reporting r ON ((l.uuid = r.location_id)));


ALTER TABLE reporting OWNER TO jduren;

--
-- Name: retractions; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE retractions (
    id integer NOT NULL,
    report_id uuid,
    retracted_by integer NOT NULL,
    retracted_date timestamp without time zone DEFAULT now() NOT NULL,
    retraction_reason text,
    status text DEFAULT 'PENDING'::text NOT NULL,
    form_id integer NOT NULL
);


ALTER TABLE retractions OWNER TO ewars;

--
-- Name: retractions_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE retractions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE retractions_id_seq OWNER TO ewars;

--
-- Name: retractions_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE retractions_id_seq OWNED BY retractions.id;


--
-- Name: sessions; Type: TABLE; Schema: ewars; Owner: jduren
--

CREATE TABLE sessions (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id integer NOT NULL,
    socket_ids text[],
    token text NOT NULL,
    data json DEFAULT '{}'::json NOT NULL
);


ALTER TABLE sessions OWNER TO jduren;

--
-- Name: settings; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE settings (
    id integer NOT NULL,
    token text NOT NULL,
    value text NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE settings OWNER TO ewars;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE settings_id_seq OWNER TO ewars;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: site_pages; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE site_pages (
    id integer NOT NULL,
    site_id integer NOT NULL,
    title text NOT NULL,
    sef text NOT NULL,
    parent_id integer,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    access text DEFAULT 'AUTHENTICATED'::text NOT NULL,
    created_by integer NOT NULL,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL,
    page_type text DEFAULT 'MARKDOWN'::text NOT NULL,
    description text
);


ALTER TABLE site_pages OWNER TO jdu;

--
-- Name: site_pages_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE site_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE site_pages_id_seq OWNER TO jdu;

--
-- Name: site_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE site_pages_id_seq OWNED BY site_pages.id;


--
-- Name: sites; Type: TABLE; Schema: ewars; Owner: jdu
--

CREATE TABLE sites (
    id integer NOT NULL,
    title text NOT NULL,
    account_id integer NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    access text DEFAULT 'AUTHENTICATED'::text NOT NULL,
    created_by integer NOT NULL,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL,
    theme text DEFAULT 'DEFAULT'::text NOT NULL,
    hub_indexed text DEFAULT 'NO'::text NOT NULL,
    goog text,
    description text
);


ALTER TABLE sites OWNER TO jdu;

--
-- Name: sites_id_seq; Type: SEQUENCE; Schema: ewars; Owner: jdu
--

CREATE SEQUENCE sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sites_id_seq OWNER TO jdu;

--
-- Name: sites_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: jdu
--

ALTER SEQUENCE sites_id_seq OWNED BY sites.id;


--
-- Name: situation_messages; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE situation_messages (
    id integer NOT NULL,
    content json DEFAULT '{}'::json NOT NULL,
    situation_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE situation_messages OWNER TO ewars;

--
-- Name: situation_messages_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE situation_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE situation_messages_id_seq OWNER TO ewars;

--
-- Name: situation_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE situation_messages_id_seq OWNED BY situation_messages.id;


--
-- Name: situations; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE situations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    kit_id integer,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    description text NOT NULL,
    opened timestamp without time zone DEFAULT now() NOT NULL,
    closed timestamp without time zone,
    config json DEFAULT '{}'::json NOT NULL,
    location_id uuid,
    name json DEFAULT '{}'::json NOT NULL,
    account_id integer
);


ALTER TABLE situations OWNER TO ewars;

--
-- Name: situations_users; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE situations_users (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    situation_id uuid NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE situations_users OWNER TO ewars;

--
-- Name: sms_gateways; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE sms_gateways (
    id integer NOT NULL,
    account_id integer NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    can_receive boolean DEFAULT false,
    can_send boolean DEFAULT true,
    receive_number text,
    send_number text,
    gate_type text DEFAULT 'ENVAYA'::text NOT NULL,
    gate_version text,
    created_date date DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    out_rate_limit integer,
    out_rate_limit_interval text,
    in_rate_limit text,
    in_rate_limit_interval integer,
    effective_area public.geometry(Polygon,3857)
);


ALTER TABLE sms_gateways OWNER TO ewars;

--
-- Name: COLUMN sms_gateways.status; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.status IS 'ACTIVE|INACTIVE';


--
-- Name: COLUMN sms_gateways.can_receive; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.can_receive IS 'Can this gate forward messages to EWARS';


--
-- Name: COLUMN sms_gateways.can_send; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.can_send IS 'Can this gate forward message to clients';


--
-- Name: COLUMN sms_gateways.receive_number; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.receive_number IS 'The number of that EWARS will receive messages from the relay on';


--
-- Name: COLUMN sms_gateways.send_number; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.send_number IS 'The number that will be used to send TEXTS on behalf of EWARS';


--
-- Name: COLUMN sms_gateways.gate_type; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.gate_type IS 'The type of gateway this is, translates to a driver on the server-side';


--
-- Name: COLUMN sms_gateways.out_rate_limit; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.out_rate_limit IS 'The limit that SMS can be sent through this gateway';


--
-- Name: COLUMN sms_gateways.out_rate_limit_interval; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.out_rate_limit_interval IS 'The period that out_rate_limit applies to';


--
-- Name: COLUMN sms_gateways.in_rate_limit; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.in_rate_limit IS 'The limit per hour that SMS can be received through this gateway';


--
-- Name: COLUMN sms_gateways.in_rate_limit_interval; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.in_rate_limit_interval IS 'The period that in_rate_limit applies to';


--
-- Name: COLUMN sms_gateways.effective_area; Type: COMMENT; Schema: ewars; Owner: ewars
--

COMMENT ON COLUMN sms_gateways.effective_area IS 'A geographic boundary within which, phones will be auto-configured to use this gateway';


--
-- Name: sms_gateways_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE sms_gateways_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sms_gateways_id_seq OWNER TO ewars;

--
-- Name: sms_gateways_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE sms_gateways_id_seq OWNED BY sms_gateways.id;


--
-- Name: system_notices; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE system_notices (
    id integer NOT NULL,
    content text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    active boolean DEFAULT false
);


ALTER TABLE system_notices OWNER TO ewars;

--
-- Name: system_notices_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE system_notices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_notices_id_seq OWNER TO ewars;

--
-- Name: system_notices_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE system_notices_id_seq OWNED BY system_notices.id;


--
-- Name: tasks; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE tasks (
    id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    completed timestamp without time zone DEFAULT now(),
    assigned_to integer,
    definition json DEFAULT '{}'::json NOT NULL,
    data json DEFAULT '{}'::json NOT NULL,
    state text DEFAULT 'OPEN'::text NOT NULL,
    priority text DEFAULT 'LOW'::text NOT NULL,
    assigned_group_id integer,
    assigned_account_id integer,
    form json,
    label json DEFAULT '{}'::json,
    details json DEFAULT '{}'::json,
    related json[],
    history json[]
);


ALTER TABLE tasks OWNER TO ewars;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tasks_id_seq OWNER TO ewars;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- Name: tasks_v2; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE tasks_v2 (
    id integer NOT NULL,
    task_type text NOT NULL,
    assigned_user_id integer,
    assigned_user_types text[],
    account_id integer NOT NULL,
    location_id uuid,
    state text DEFAULT 'OPEN'::text NOT NULL,
    priority text DEFAULT 'LOW'::text NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    claimed_date timestamp without time zone,
    action_taken text,
    actioned_date timestamp without time zone,
    actioned_by integer,
    data json DEFAULT '{}'::json
);


ALTER TABLE tasks_v2 OWNER TO ewars;

--
-- Name: tasks_v2_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE tasks_v2_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tasks_v2_id_seq OWNER TO ewars;

--
-- Name: tasks_v2_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE tasks_v2_id_seq OWNED BY tasks_v2.id;


--
-- Name: taxonomies; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE taxonomies (
    id integer NOT NULL,
    name json NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    hierarchical boolean DEFAULT false,
    description text DEFAULT 'Default Description'::text NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    created timestamp without time zone NOT NULL,
    created_by integer NOT NULL,
    end_select_only boolean DEFAULT false,
    issynced boolean DEFAULT false
);


ALTER TABLE taxonomies OWNER TO ewars;

--
-- Name: taxonomies_id_seq; Type: SEQUENCE; Schema: ewars; Owner: ewars
--

CREATE SEQUENCE taxonomies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE taxonomies_id_seq OWNER TO ewars;

--
-- Name: taxonomies_id_seq; Type: SEQUENCE OWNED BY; Schema: ewars; Owner: ewars
--

ALTER SEQUENCE taxonomies_id_seq OWNED BY taxonomies.id;


--
-- Name: taxonomies_items; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE taxonomies_items (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    parent_id uuid,
    lineage text,
    created_by integer NOT NULL,
    tax_id integer NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    data json DEFAULT '{}'::json NOT NULL
);


ALTER TABLE taxonomies_items OWNER TO ewars;

--
-- Name: templates; Type: TABLE; Schema: ewars; Owner: ewars
--

CREATE TABLE templates (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    template_name json DEFAULT '{}'::json NOT NULL,
    instance_name json DEFAULT '{}'::json NOT NULL,
    description json DEFAULT '{}'::json,
    created date DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    content text DEFAULT ''::text NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    generation json DEFAULT '{}'::json NOT NULL,
    shareable boolean DEFAULT false,
    access text DEFAULT 'PRIVATE'::text NOT NULL,
    account_id integer,
    data json,
    template_type text DEFAULT 'GENERATED'::text NOT NULL,
    orientation text DEFAULT 'PORTRAIT'::text NOT NULL
);


ALTER TABLE templates OWNER TO ewars;

--
-- Name: timeliness; Type: VIEW; Schema: ewars; Owner: jduren
--

CREATE VIEW timeliness AS
 SELECT c.data_date,
    c.form_id,
    c.location_id,
    c.submitted_date,
    (c.submitted_date - c.data_date) AS delay
   FROM collections c
  WHERE (c.status = 'SUBMITTED'::text);


ALTER TABLE timeliness OWNER TO jduren;

--
-- Name: alert_actions id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_actions ALTER COLUMN id SET DEFAULT nextval('alert_actions_id_seq'::regclass);


--
-- Name: alert_events id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_events ALTER COLUMN id SET DEFAULT nextval('alert_events_id_seq'::regclass);


--
-- Name: alert_users id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_users ALTER COLUMN id SET DEFAULT nextval('alert_users_id_seq'::regclass);


--
-- Name: amendments id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY amendments ALTER COLUMN id SET DEFAULT nextval('amendments_id_seq'::regclass);


--
-- Name: conf id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY conf ALTER COLUMN id SET DEFAULT nextval('conf_id_seq'::regclass);


--
-- Name: devices id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY devices ALTER COLUMN id SET DEFAULT nextval('devices_id_seq'::regclass);


--
-- Name: discussion_threads id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY discussion_threads ALTER COLUMN id SET DEFAULT nextval('discussions_id_seq'::regclass);


--
-- Name: distributions id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY distributions ALTER COLUMN id SET DEFAULT nextval('distributions_id_seq'::regclass);


--
-- Name: feed_items id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feed_items ALTER COLUMN id SET DEFAULT nextval('feed_items_id_seq'::regclass);


--
-- Name: feeds id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feeds ALTER COLUMN id SET DEFAULT nextval('feeds_id_seq'::regclass);


--
-- Name: forms id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms ALTER COLUMN id SET DEFAULT nextval('forms_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: guidance id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY guidance ALTER COLUMN id SET DEFAULT nextval('guidance_id_seq'::regclass);


--
-- Name: indicator_groups id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicator_groups ALTER COLUMN id SET DEFAULT nextval('indicator_groups_id_seq'::regclass);


--
-- Name: kb_article_attachments id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_article_attachments ALTER COLUMN id SET DEFAULT nextval('kb_article_attachments_id_seq'::regclass);


--
-- Name: kb_articles id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_articles ALTER COLUMN id SET DEFAULT nextval('kb_articles_id_seq'::regclass);


--
-- Name: kb_sections id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_sections ALTER COLUMN id SET DEFAULT nextval('kb_sections_id_seq'::regclass);


--
-- Name: location_gis id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_gis ALTER COLUMN id SET DEFAULT nextval('location_gis_id_seq'::regclass);


--
-- Name: location_reporting id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_reporting ALTER COLUMN id SET DEFAULT nextval('location_reporting_id_seq'::regclass);


--
-- Name: location_types id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_types ALTER COLUMN id SET DEFAULT nextval('location_types_id_seq'::regclass);


--
-- Name: news_categories id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_categories ALTER COLUMN id SET DEFAULT nextval('news_categories_id_seq'::regclass);


--
-- Name: news_posts id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_posts ALTER COLUMN id SET DEFAULT nextval('news_posts_id_seq'::regclass);


--
-- Name: retractions id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY retractions ALTER COLUMN id SET DEFAULT nextval('retractions_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: site_pages id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY site_pages ALTER COLUMN id SET DEFAULT nextval('site_pages_id_seq'::regclass);


--
-- Name: sites id; Type: DEFAULT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY sites ALTER COLUMN id SET DEFAULT nextval('sites_id_seq'::regclass);


--
-- Name: situation_messages id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situation_messages ALTER COLUMN id SET DEFAULT nextval('situation_messages_id_seq'::regclass);


--
-- Name: sms_gateways id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY sms_gateways ALTER COLUMN id SET DEFAULT nextval('sms_gateways_id_seq'::regclass);


--
-- Name: system_notices id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY system_notices ALTER COLUMN id SET DEFAULT nextval('system_notices_id_seq'::regclass);


--
-- Name: tasks id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- Name: tasks_v2 id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks_v2 ALTER COLUMN id SET DEFAULT nextval('tasks_v2_id_seq'::regclass);


--
-- Name: taxonomies id; Type: DEFAULT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies ALTER COLUMN id SET DEFAULT nextval('taxonomies_id_seq'::regclass);


--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (uuid);


--
-- Name: alarms alarms_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alarms
    ADD CONSTRAINT alarms_pkey PRIMARY KEY (uuid);


--
-- Name: alert_actions alert_actions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_actions
    ADD CONSTRAINT alert_actions_pkey PRIMARY KEY (id);


--
-- Name: alert_events alert_events_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_events
    ADD CONSTRAINT alert_events_pkey PRIMARY KEY (id);


--
-- Name: alert_users alert_users_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_users
    ADD CONSTRAINT alert_users_pkey PRIMARY KEY (id);


--
-- Name: alerts alerts_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_pkey PRIMARY KEY (uuid);


--
-- Name: amendments amendments_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY amendments
    ADD CONSTRAINT amendments_pkey PRIMARY KEY (id);


--
-- Name: analysis_notebooks analysis_notebooks_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY analysis_notebooks
    ADD CONSTRAINT analysis_notebooks_pkey PRIMARY KEY (id);


--
-- Name: assignments assignments_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignments_pkey PRIMARY KEY (uuid);


--
-- Name: collections collections_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_pkey PRIMARY KEY (uuid);


--
-- Name: conf conf_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY conf
    ADD CONSTRAINT conf_pkey PRIMARY KEY (id);


--
-- Name: datasets datasets_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_pkey PRIMARY KEY (uuid);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: discussion_threads discussions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY discussion_threads
    ADD CONSTRAINT discussions_pkey PRIMARY KEY (id);


--
-- Name: distributions distributions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY distributions
    ADD CONSTRAINT distributions_pkey PRIMARY KEY (id);


--
-- Name: feed_items feed_items_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feed_items
    ADD CONSTRAINT feed_items_pkey PRIMARY KEY (id);


--
-- Name: feeds feeds_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feeds
    ADD CONSTRAINT feeds_pkey PRIMARY KEY (id);


--
-- Name: form_versions form_versions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY form_versions
    ADD CONSTRAINT form_versions_pkey PRIMARY KEY (uuid);


--
-- Name: forms forms_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: guidance guidance_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY guidance
    ADD CONSTRAINT guidance_pkey PRIMARY KEY (id);


--
-- Name: indicator_groups indicator_groups_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicator_groups
    ADD CONSTRAINT indicator_groups_pkey PRIMARY KEY (id);


--
-- Name: indicators indicators_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicators
    ADD CONSTRAINT indicators_pkey PRIMARY KEY (uuid);


--
-- Name: investigations investigations_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY investigations
    ADD CONSTRAINT investigations_pkey PRIMARY KEY (uuid);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (uuid);


--
-- Name: kb_article_attachments kb_article_attachments_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_article_attachments
    ADD CONSTRAINT kb_article_attachments_pkey PRIMARY KEY (id);


--
-- Name: kb_articles kb_articles_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_articles
    ADD CONSTRAINT kb_articles_pkey PRIMARY KEY (id);


--
-- Name: kb_sections kb_sections_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_sections
    ADD CONSTRAINT kb_sections_pkey PRIMARY KEY (id);


--
-- Name: layouts layouts_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT layouts_pkey PRIMARY KEY (uuid);


--
-- Name: location_gis location_gis_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_gis
    ADD CONSTRAINT location_gis_pkey PRIMARY KEY (id);


--
-- Name: location_reporting location_reporting_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_reporting
    ADD CONSTRAINT location_reporting_pkey PRIMARY KEY (id);


--
-- Name: location_types location_types_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_types
    ADD CONSTRAINT location_types_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (uuid);


--
-- Name: maps maps_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY maps
    ADD CONSTRAINT maps_pkey PRIMARY KEY (uuid);


--
-- Name: discussion_thread_messages messages_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY discussion_thread_messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (uuid);


--
-- Name: messages messages_pkey1; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey1 PRIMARY KEY (uuid);


--
-- Name: news_categories news_categories_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_categories
    ADD CONSTRAINT news_categories_pkey PRIMARY KEY (id);


--
-- Name: news_posts news_posts_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_posts
    ADD CONSTRAINT news_posts_pkey PRIMARY KEY (id);


--
-- Name: notebooks notebooks_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY notebooks
    ADD CONSTRAINT notebooks_pkey PRIMARY KEY (uuid);


--
-- Name: ops ops_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY ops
    ADD CONSTRAINT ops_pkey PRIMARY KEY (key_name);


--
-- Name: plots plots_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY plots
    ADD CONSTRAINT plots_pkey PRIMARY KEY (uuid);


--
-- Name: retractions retractions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY retractions
    ADD CONSTRAINT retractions_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (uuid);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: site_pages site_pages_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY site_pages
    ADD CONSTRAINT site_pages_pkey PRIMARY KEY (id);


--
-- Name: sites sites_pkey; Type: CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- Name: situation_messages situation_messages_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situation_messages
    ADD CONSTRAINT situation_messages_pkey PRIMARY KEY (id);


--
-- Name: situations situations_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations
    ADD CONSTRAINT situations_pkey PRIMARY KEY (uuid);


--
-- Name: situations_users situations_users_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations_users
    ADD CONSTRAINT situations_users_pkey PRIMARY KEY (uuid);


--
-- Name: sms_gateways sms_gateways_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY sms_gateways
    ADD CONSTRAINT sms_gateways_pkey PRIMARY KEY (id);


--
-- Name: system_notices system_notices_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY system_notices
    ADD CONSTRAINT system_notices_pkey PRIMARY KEY (id);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: tasks_v2 tasks_v2_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks_v2
    ADD CONSTRAINT tasks_v2_pkey PRIMARY KEY (id);


--
-- Name: taxonomies_items taxonomies_items_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies_items
    ADD CONSTRAINT taxonomies_items_pkey PRIMARY KEY (uuid);


--
-- Name: taxonomies taxonomies_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies
    ADD CONSTRAINT taxonomies_pkey PRIMARY KEY (id);


--
-- Name: templates templates_pkey; Type: CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (uuid);


--
-- Name: activity_account_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX activity_account_id_idx ON activity USING btree (account_id);


--
-- Name: activity_group_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX activity_group_id_idx ON activity USING btree (group_id);


--
-- Name: activity_lab_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX activity_lab_id_idx ON activity USING btree (lab_id);


--
-- Name: activity_user_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX activity_user_id_idx ON activity USING btree (user_id);


--
-- Name: alarms_indicator_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX alarms_indicator_id_idx ON alarms USING btree (indicator_id);


--
-- Name: alerts_alarm_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX alerts_alarm_id_idx ON alerts USING btree (alarm_id);


--
-- Name: alerts_indicator_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX alerts_indicator_id_idx ON alerts USING btree (indicator_id);


--
-- Name: alerts_location_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX alerts_location_id_idx ON alerts USING btree (location_id);


--
-- Name: analysis_notebooks_created_by_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX analysis_notebooks_created_by_idx ON analysis_notebooks USING btree (created_by);


--
-- Name: assignments_user_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX assignments_user_id_idx ON assignments USING btree (user_id);


--
-- Name: collection_date_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX collection_date_idx ON collections USING btree (data_date);


--
-- Name: collections_location_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX collections_location_id_idx ON collections USING btree (location_id);


--
-- Name: form_versions_form_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX form_versions_form_id_idx ON form_versions USING btree (form_id);


--
-- Name: idx_form_id_collection; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX idx_form_id_collection ON collections USING btree (form_id);


--
-- Name: idx_user_id_collection; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX idx_user_id_collection ON collections USING btree (created_by);


--
-- Name: indicators_group_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX indicators_group_id_idx ON indicators USING btree (group_id);


--
-- Name: locations_parent_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX locations_parent_id_idx ON locations USING btree (parent_id);


--
-- Name: locations_site_type_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX locations_site_type_id_idx ON locations USING btree (site_type_id);


--
-- Name: situations_location_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX situations_location_id_idx ON situations USING btree (location_id);


--
-- Name: tasks_assigned_account_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX tasks_assigned_account_id_idx ON tasks USING btree (assigned_account_id);


--
-- Name: tasks_assigned_group_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX tasks_assigned_group_id_idx ON tasks USING btree (assigned_group_id);


--
-- Name: tasks_assigned_to_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX tasks_assigned_to_idx ON tasks USING btree (assigned_to);


--
-- Name: tasks_state_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX tasks_state_idx ON tasks USING btree (state);


--
-- Name: taxonomies_items_parent_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX taxonomies_items_parent_id_idx ON taxonomies_items USING btree (parent_id);


--
-- Name: taxonomies_items_tax_id_idx; Type: INDEX; Schema: ewars; Owner: ewars
--

CREATE INDEX taxonomies_items_tax_id_idx ON taxonomies_items USING btree (tax_id);


--
-- Name: collections test_trigger; Type: TRIGGER; Schema: ewars; Owner: ewars
--

CREATE TRIGGER test_trigger AFTER INSERT OR DELETE OR UPDATE ON collections FOR EACH ROW EXECUTE PROCEDURE public.plv8_test();


--
-- Name: activity activity_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: activity activity_group_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: activity activity_lab_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_lab_id_fkey FOREIGN KEY (lab_id) REFERENCES _iw.laboratories(uuid);


--
-- Name: activity activity_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: activity activity_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY activity
    ADD CONSTRAINT activity_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: alarms alarms_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alarms
    ADD CONSTRAINT alarms_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: alarms alarms_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alarms
    ADD CONSTRAINT alarms_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: alarms alarms_indicator_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alarms
    ADD CONSTRAINT alarms_indicator_id_fkey FOREIGN KEY (indicator_id) REFERENCES indicators(uuid);


--
-- Name: alert_actions alert_actions_alert_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_actions
    ADD CONSTRAINT alert_actions_alert_id_fkey FOREIGN KEY (alert_id) REFERENCES alerts(uuid);


--
-- Name: alert_actions alert_actions_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_actions
    ADD CONSTRAINT alert_actions_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: alert_events alert_events_alert_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_events
    ADD CONSTRAINT alert_events_alert_id_fkey FOREIGN KEY (alert_id) REFERENCES alerts(uuid);


--
-- Name: alert_events alert_events_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alert_events
    ADD CONSTRAINT alert_events_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: alerts alerts_actioned_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_actioned_by_fkey FOREIGN KEY (actioned_by) REFERENCES _iw.users(id);


--
-- Name: alerts alerts_alarm_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_alarm_id_fkey FOREIGN KEY (alarm_id) REFERENCES alarms(uuid);


--
-- Name: alerts alerts_indicator_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_indicator_id_fkey FOREIGN KEY (indicator_id) REFERENCES indicators(uuid);


--
-- Name: alerts alerts_investigation_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_investigation_id_fkey FOREIGN KEY (investigation_id) REFERENCES investigations(uuid);


--
-- Name: alerts alerts_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: alerts alerts_triggering_report_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_triggering_report_id_fkey FOREIGN KEY (triggering_report_id) REFERENCES collections(uuid);


--
-- Name: alerts alerts_triggering_user_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_triggering_user_fkey FOREIGN KEY (triggering_user) REFERENCES _iw.users(id);


--
-- Name: amendments amendments_amended_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY amendments
    ADD CONSTRAINT amendments_amended_by_fkey FOREIGN KEY (amended_by) REFERENCES _iw.users(id);


--
-- Name: amendments amendments_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY amendments
    ADD CONSTRAINT amendments_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: amendments amendments_report_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY amendments
    ADD CONSTRAINT amendments_report_id_fkey FOREIGN KEY (report_id) REFERENCES collections(uuid);


--
-- Name: analysis_notebooks analysis_notebooks_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY analysis_notebooks
    ADD CONSTRAINT analysis_notebooks_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: assignments assignments_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignments_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: assignments assignments_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignments_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: assignments assignments_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignments_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: assignments assignments_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT assignments_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: collections collections_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: collections collections_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: collections collections_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: collections collections_form_version_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_form_version_id_fkey FOREIGN KEY (form_version_id) REFERENCES form_versions(uuid);


--
-- Name: collections collections_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: collections_taxonomies collections_taxonomies_collection_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections_taxonomies
    ADD CONSTRAINT collections_taxonomies_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collections(uuid);


--
-- Name: collections_taxonomies collections_taxonomies_tax_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY collections_taxonomies
    ADD CONSTRAINT collections_taxonomies_tax_id_fkey FOREIGN KEY (tax_id) REFERENCES taxonomies_items(uuid);


--
-- Name: conf conf_aid_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY conf
    ADD CONSTRAINT conf_aid_fkey FOREIGN KEY (aid) REFERENCES _iw.accounts(id);


--
-- Name: conf conf_modified_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY conf
    ADD CONSTRAINT conf_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES _iw.users(id);


--
-- Name: datasets datasets_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: devices devices_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: devices devices_gate_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_gate_id_fkey FOREIGN KEY (gate_id) REFERENCES sms_gateways(id);


--
-- Name: devices devices_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: distributions distributions_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY distributions
    ADD CONSTRAINT distributions_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: feed_items feed_items_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feed_items
    ADD CONSTRAINT feed_items_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: feeds feeds_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY feeds
    ADD CONSTRAINT feeds_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: form_versions form_versions_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY form_versions
    ADD CONSTRAINT form_versions_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id) DEFERRABLE;


--
-- Name: forms forms_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: forms forms_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: forms forms_site_type_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_site_type_id_fkey FOREIGN KEY (site_type_id) REFERENCES location_types(id);


--
-- Name: forms forms_version_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_version_id_fkey FOREIGN KEY (version_id) REFERENCES form_versions(uuid) DEFERRABLE;


--
-- Name: indicator_groups indicator_groups_parent_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicator_groups
    ADD CONSTRAINT indicator_groups_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES indicator_groups(id);


--
-- Name: indicators indicators_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicators
    ADD CONSTRAINT indicators_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: indicators indicators_group_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY indicators
    ADD CONSTRAINT indicators_group_id_fkey FOREIGN KEY (group_id) REFERENCES indicator_groups(id);


--
-- Name: kb_article_attachments kb_article_attachments_article_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_article_attachments
    ADD CONSTRAINT kb_article_attachments_article_id_fkey FOREIGN KEY (article_id) REFERENCES kb_articles(id);


--
-- Name: kb_article_attachments kb_article_attachments_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_article_attachments
    ADD CONSTRAINT kb_article_attachments_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: kb_articles kb_articles_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_articles
    ADD CONSTRAINT kb_articles_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: kb_articles kb_articles_parent_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_articles
    ADD CONSTRAINT kb_articles_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES kb_articles(id);


--
-- Name: kb_articles kb_articles_section_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_articles
    ADD CONSTRAINT kb_articles_section_id_fkey FOREIGN KEY (section_id) REFERENCES kb_sections(id);


--
-- Name: kb_sections kb_sections_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY kb_sections
    ADD CONSTRAINT kb_sections_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: layouts layouts_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT layouts_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: layouts layouts_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT layouts_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: layouts layouts_modified_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT layouts_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES _iw.users(id);


--
-- Name: location_gis location_gis_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_gis
    ADD CONSTRAINT location_gis_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: location_reporting location_reporting_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_reporting
    ADD CONSTRAINT location_reporting_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: location_reporting location_reporting_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_reporting
    ADD CONSTRAINT location_reporting_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: location_reporting location_reporting_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_reporting
    ADD CONSTRAINT location_reporting_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: location_types location_types_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY location_types
    ADD CONSTRAINT location_types_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: locations locations_parent_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES locations(uuid) DEFERRABLE;


--
-- Name: locations locations_site_type_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_site_type_id_fkey FOREIGN KEY (site_type_id) REFERENCES location_types(id) DEFERRABLE;


--
-- Name: logs logs_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY logs
    ADD CONSTRAINT logs_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: maps maps_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY maps
    ADD CONSTRAINT maps_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: maps maps_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY maps
    ADD CONSTRAINT maps_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: discussion_thread_messages messages_discussion_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY discussion_thread_messages
    ADD CONSTRAINT messages_discussion_id_fkey FOREIGN KEY (discussion_id) REFERENCES discussion_threads(id);


--
-- Name: messages messages_recipient_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_recipient_fkey FOREIGN KEY (recipient) REFERENCES _iw.users(id);


--
-- Name: messages messages_sender_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_sender_fkey FOREIGN KEY (sender) REFERENCES _iw.users(id);


--
-- Name: discussion_thread_messages messages_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY discussion_thread_messages
    ADD CONSTRAINT messages_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: news_posts news_posts_category_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_posts
    ADD CONSTRAINT news_posts_category_id_fkey FOREIGN KEY (category_id) REFERENCES news_categories(id);


--
-- Name: news_posts news_posts_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY news_posts
    ADD CONSTRAINT news_posts_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: notebooks notebooks_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY notebooks
    ADD CONSTRAINT notebooks_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: notebooks notebooks_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY notebooks
    ADD CONSTRAINT notebooks_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: plots plots_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY plots
    ADD CONSTRAINT plots_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: plots plots_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY plots
    ADD CONSTRAINT plots_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: retractions retractions_form_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY retractions
    ADD CONSTRAINT retractions_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: retractions retractions_report_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY retractions
    ADD CONSTRAINT retractions_report_id_fkey FOREIGN KEY (report_id) REFERENCES collections(uuid);


--
-- Name: retractions retractions_retracted_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY retractions
    ADD CONSTRAINT retractions_retracted_by_fkey FOREIGN KEY (retracted_by) REFERENCES _iw.users(id);


--
-- Name: sessions sessions_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jduren
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: site_pages site_pages_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY site_pages
    ADD CONSTRAINT site_pages_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: site_pages site_pages_parent_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY site_pages
    ADD CONSTRAINT site_pages_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES site_pages(id);


--
-- Name: site_pages site_pages_site_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY site_pages
    ADD CONSTRAINT site_pages_site_id_fkey FOREIGN KEY (site_id) REFERENCES sites(id);


--
-- Name: sites sites_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: sites sites_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: jdu
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: situation_messages situation_messages_situation_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situation_messages
    ADD CONSTRAINT situation_messages_situation_id_fkey FOREIGN KEY (situation_id) REFERENCES situations(uuid);


--
-- Name: situations situations_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations
    ADD CONSTRAINT situations_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: situations situations_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations
    ADD CONSTRAINT situations_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: situations_users situations_users_situation_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations_users
    ADD CONSTRAINT situations_users_situation_id_fkey FOREIGN KEY (situation_id) REFERENCES situations(uuid);


--
-- Name: situations_users situations_users_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY situations_users
    ADD CONSTRAINT situations_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES _iw.users(id);


--
-- Name: sms_gateways sms_gateways_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY sms_gateways
    ADD CONSTRAINT sms_gateways_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: sms_gateways sms_gateways_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY sms_gateways
    ADD CONSTRAINT sms_gateways_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: tasks tasks_assigned_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_assigned_account_id_fkey FOREIGN KEY (assigned_account_id) REFERENCES _iw.accounts(id);


--
-- Name: tasks tasks_assigned_group_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_assigned_group_id_fkey FOREIGN KEY (assigned_group_id) REFERENCES groups(id);


--
-- Name: tasks tasks_assigned_to_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_assigned_to_fkey FOREIGN KEY (assigned_to) REFERENCES _iw.users(id);


--
-- Name: tasks_v2 tasks_v2_assigned_user_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks_v2
    ADD CONSTRAINT tasks_v2_assigned_user_id_fkey FOREIGN KEY (assigned_user_id) REFERENCES _iw.users(id);


--
-- Name: tasks_v2 tasks_v2_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks_v2
    ADD CONSTRAINT tasks_v2_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: tasks_v2 tasks_v2_location_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY tasks_v2
    ADD CONSTRAINT tasks_v2_location_id_fkey FOREIGN KEY (location_id) REFERENCES locations(uuid);


--
-- Name: taxonomies taxonomies_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies
    ADD CONSTRAINT taxonomies_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: taxonomies_items taxonomies_items_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies_items
    ADD CONSTRAINT taxonomies_items_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- Name: taxonomies_items taxonomies_items_parent_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies_items
    ADD CONSTRAINT taxonomies_items_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES taxonomies_items(uuid);


--
-- Name: taxonomies_items taxonomies_items_tax_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY taxonomies_items
    ADD CONSTRAINT taxonomies_items_tax_id_fkey FOREIGN KEY (tax_id) REFERENCES taxonomies(id);


--
-- Name: templates templates_account_id_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_account_id_fkey FOREIGN KEY (account_id) REFERENCES _iw.accounts(id);


--
-- Name: templates templates_created_by_fkey; Type: FK CONSTRAINT; Schema: ewars; Owner: ewars
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_created_by_fkey FOREIGN KEY (created_by) REFERENCES _iw.users(id);


--
-- PostgreSQL database dump complete
--

