import json

from ewars.db import get_db_cur

from . import settings

from psycopg2.extensions import AsIs


async def get_settings(conf_type, user=None):
    """ Get a section of settings

    Args:
        conf_type: The settings type data to retrieve
        user: The calling user

    Returns:

    """
    if conf_type == "GENERAL":
        return await _get_general(user=user)
    else:
        return await _get_conf(conf_type, user=user)


async def _get_conf(conf_type, user=None):
    result = None

    confs = None
    if conf_type == "*":

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT * FROM %s.conf;
            """, (
                AsIs(user.get("tki")),
            ))
            confs = await cur.fetchall()
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.conf
                 WHERE grp = %s;
            """, (
                AsIs(user.get("tki")),
                conf_type,
            ))
            confs = await cur.fetchall()

    result = dict((x.get("key"), x.get("value")) for x in confs)

    return result


async def update_settings(conf_type, data, user=None):
    """ Update settings for specific context

    Args:
        conf_type: The context settings to update
        data: The data for the update
        user: The calling user

    Returns:

    """
    result = None

    if conf_type == "GENERAL":
        result = await _update_general(data, user=user)
    else:
        result = await _update_conf(conf_type, data, user=user)

    return dict()


async def _update_conf(group, data, user=None):
    result = None

    sql = []

    async with get_db_cur() as cur:
        for key, value in data.items():
            await cur.execute("""
              DELETE FROM %s.conf
              WHERE key = %s;
            """, (
                AsIs(user.get("tki")),
                key,
            ))

            await cur.execute("""
                INSERT INTO %s.conf
                (key, value, modified_by, grp)
                VALUES (%s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                key,
                json.dumps(value),
                user.get("id"),
                group,
            ))

    return True


async def _get_general(user=None):
    """ Get general settings

    Args:
        user:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts WHERE id = %s;
        """, (
            user.get("aid"),
        ))
        result = await cur.fetchone()

    result['NFO_OCTET'] = settings.VERSION
    result['NFO_HUB'] = "Disabled"
    result['NFO_TEMP_DOMAIN'] = "%s.ewars.ws" % (result.get("id"))
    result['NFO_ID'] = result.get("id")
    result["NFO_STORAGE"] = "Hosted"
    result['NFO_EMAILS_SENT'] = "0"

    return result


async def _update_general(data, user=None):
    """ Update general settings

    Args:
        data:
        user:

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.accounts
             SET name = %s,
              default_language = %s,
              domain = %s,
              last_modified = NOW(),
              description = %s,
              iso2 = %s,
              iso3 = %s,
              two_factor = %s,
              ssl = %s,
              timezone = %s,
              fmt_date = %s,
              fmt_time = %s,
              translation = %s
            WHERE id = %s;
        """, (
            data.get("name"),
            data.get("default_language", "en"),
            data.get("domain"),
            data.get("description"),
            data.get("iso2"),
            data.get("iso3"),
            data.get("two_factor"),
            data.get("ssl"),
            data.get("timezone"),
            data.get("fmt_date"),
            data.get("fmt_time"),
            data.get("translation"),
            user.get("aid"),
        ))

    return True
