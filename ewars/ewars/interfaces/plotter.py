import json

from jose import jwt, JWTError

from ewars.core.serializers.json_encoder import JSONEncoder
from ewars.core.interface import BaseRequestHandler
from ewars.plot.plotter import plot

from ewars.conf import settings

class PlotterHandler(BaseRequestHandler):
    """
        Querying Patterns:
            - Get data from a single indicator across time
            - Aggregate multiple indicator data series into a single series over time
            - Reduce multiple indicator data series into a single series using a function over time
            - Reduce a single indicator time series to a single value
            - Reduce multiple indicator time series aggregated together to a single value
            - Reduce multiple indicator data series pre-reduced across time by a function to a single reduced value
            - Generate multiple series based on a ruleset (i.e. create 1 series per location of type)

        SERIES - (time, reduce)
        COMPLEX - (time, reduce)
        AGGREGATE - (time, reduce)
        GENERATOR - (time, reduce)

        Types:
            Basic:
                SERIES: Plain Series
                SLICE: A Series aggregated to a single value
            Intermediate:
                COMPLEX: A series made up of multiple sub-series reduced by a function
                AGGREGATION: A series made up of the aggregation of multiple sub-series
            Advanced:
                COMPLEX_SLICE: Aggregation of a COMPLEX series into a SLICE
                AGGREGATION_SLICE: Aggregation series aggregated to a single value
    """

    async def get(self):
        self.write("Hello Analysis")

    async def post(self):
        token = self.request.headers.get("Authorization", None)

        user = None
        if self.current_user is not None:
            user = self.current_user
        else:
            if token is not None:
                try:
                    user = jwt.decode(token, settings.JWT_KEY, algorithms=["HS256"])
                except JWTError as e:
                    self.send_error(status_code=403)

        data = json.loads(self.request.body.decode('utf-8'))
        # results = yield self.background_task(data)

        # results = yield self.background_task(data, user)
        results = await plot(data[0], data[1], data[2], user=user)

        self.set_header('Content-type', 'application/json')
        self.write(json.dumps(results, cls=JSONEncoder))

        self.finish()

    # @run_on_executor
    # def background_task(self, data, user):
    #     results = []
    #     cur_user = self.current_user
    #
    #     results = analysis_parser(data, user=user)
    #
    #     return results

