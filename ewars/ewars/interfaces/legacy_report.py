import json
import pickle
import uuid
from base64 import b64encode
import subprocess
import re
import io
import base64

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from tornado.web import RequestHandler, asynchronous, os, HTTPError
import tornado
from tornado.process import Subprocess

from ewars.constants import CONSTANTS

from ewars.core.interface import BaseRequestHandler
from ewars.core import authentication, identity
from ewars.core import clients
from ewars.conf import settings
from ewars.models import Location
from ewars.utils.system_utils import which
from ewars.core.serializers.json_encoder import JSONEncoder

from ewars.db import async_pool


# from ewars.core.documents.retrieval import get_report, find_template, format_template


class PDFCreateInterface(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        new_uuid = str(uuid.uuid4())

        report_date = data.get("rid", None)
        template_id = data.get("tid", None)
        location_id = data.get("lid", None)
        report_uuid = data.get("cid", None)
        template_type = data.get("tt", "GENERATED")
        file_name = data.get("fileName", None)
        file_path = new_uuid + "_sep_" + file_name.replace(" ", "_") + ".pdf"

        template = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT orientation
                FROM ewars.templates
                WHERE uuid = %s
            """, (
                template_id,
            ))
            template = await cur.fetchone()

        orientation = template.get("orientation", "PROTRAIT")

        if orientation.lower() == "portrait":
            orientation = "Portrait"
        if orientation.lower() == "landscape":
            orientation = "Landscape"

        if report_date is None:
            report_date = self.request.form.get("dd", None)

        domain = None
        if settings.DEBUG == True:
            domain = "127.0.0.1:9000"
        else:
            domain = "ewars.ws"

        wkhtmltopdf_bin = which("wkhtmltopdf")

        storage_path = "%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_path)

        report_uri = "http://" + domain + "/report?"
        segments = [
            "pdf=true",
            "inline=true"
        ]

        if report_date:
            segments.append("rid=" + report_date)
        if template_id:
            segments.append("tid=" + template_id)

        if report_uuid:
            segments.append("cid=" + report_uuid)

        if location_id:
            segments.append("lid=" + location_id)

        if template_type:
            segments.append("tt=" + template_type)

        report_uri += "&".join(segments)
        report_uri += "&key=CORMORANT"

        result = Subprocess(
            [
                wkhtmltopdf_bin,
                '--window-status',
                'ALLPRESENT',
                '--debug-javascript',
                '--print-media-type',
                '--margin-top',
                '1',
                '--margin-right',
                '1',
                '--margin-bottom',
                '1',
                '--margin-left',
                '1',
                '--orientation',
                orientation,
                report_uri,
                storage_path,
            ]
        )
        self.file_path = file_path
        result.set_exit_callback(self._complete_request)

    def _complete_request(self, code):
        self.write(json.dumps(dict(
            success=True,
            name=self.file_path
        )))
        self.finish()


class PDFDownloadInterface(BaseRequestHandler):
    """
    Download a PDF generated and stored in the tmp directory
    """

    def get(self, file_name):
        content = None

        with open("%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_name), "rb") as f:
            content = f.read()

        os.remove("/tmp/%s" % (file_name))

        clean_file_name = file_name.split("_sep_", 1)[1]
        clean_file_name = clean_file_name.replace("_", " ")

        self.set_header("Content-Type", "application/pdf")
        self.set_header("Content-Disposition", "attachment; filename=%s" % clean_file_name)

        self.write(content)


class ReportViewInterface(BaseRequestHandler):
    async def get(self):
        ewars_client = clients.get_client("ewars")
        report_client = clients.get_client("report")

        template_id = self.get_argument("tid", None)
        report_date = self.get_argument("rid", None)
        location_id = self.get_argument("lid", None)
        report_uuid = self.get_argument("cid", None)
        template_type = self.get_argument("tt", None)
        is_pdf = self.get_argument("pdf", "false")
        inline = self.get_argument("inline", "false")
        is_key = self.get_argument("key", None)

        if template_id is None:
            self.send_error(status_code=404)

        if template_type is None:
            template_type = "GENERATED"

        # Evaluate as ONE_OFF
        if template_type == "GENERATED":
            if report_date is None:
                self.send_error(status_code=404)

            if location_id is None:
                self.send_error(status_code=404)

        if template_type == "PER_REPORT":
            if report_uuid is None:
                self.send_error(status_code=404)

        instance, template = find_template(template_id)

        location = None
        instance = None
        report = {}

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT a.id, i.instance_token
                FROM _iw.accounts AS a
                LEFT JOIN _iw.instances AS i ON i.uuid = a.instance_id
                WHERE a.id = %s
            """, (
                template.get("account_id"),
            ))
            instance = cur.fetchone()

            if report_uuid is not None:
                cur.execute("""
                    SELECT uuid, data_date, data
                    FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(instance.get("instance_token")),
                    report_uuid,
                ))
                report = cur.fetchone()

            cur.execute("""
                SELECT uuid, name FROM %s.locations WHERE uuid = %s
            """, (
                AsIs(instance.get("instance_token")),
                location_id,
            ))
            location = cur.fetchone()

        if template is None:
            self.send_error(status_code=404)

        if report_date is None:
            self.send_error(status_code=404)

        user = None

        if template.get("access") == CONSTANTS.PUBLIC or is_key is not None:
            user = dict(
                instance={"instance_token": instance},
                account={"id": template.get("account_id")},
                account_id=template.get("account_id"),
            )

        if template.get("access") == "PRIVATE" and is_key is None:
            user = self.get_current_user()

            if user is None:
                self.send_error(status_code=404)

        template_content = template.get("content")

        del template["content"]

        template_data = template

        template_content = await format_template(template_content,
                                                 definition=template,
                                                 report_date=report_date,
                                                 report_uuid=report_uuid,
                                                 template_type=template_type,
                                                 location=location)

        if is_pdf == "true":
            template_content = re.sub(r"(display\:(\s*)none(;?))", "", template_content)

        ewars_version = ewars_client.get("version")
        if settings.DEBUG:
            ewars_version = "dev"
        ewars_file = 'ewars-%s' % (ewars_version,)

        client_version = report_client.get("version")
        if settings.DEBUG:
            client_version = "dev"
        client_file = '%s-%s' % (report_client.get("token"), client_version)

        location_name = "null"
        if location is not None:
            location['name']['en']

        report_data = None
        if report is not None:
            report_data = report.get("data")

        self.render("document.html",
                    template=json.dumps(template, cls=JSONEncoder),
                    template_content=template_content,
                    template_id=template_id,
                    report_date=report_date,
                    location_id=location_id,
                    pdf=is_pdf,
                    inline=inline,
                    location_name=location_name,
                    debug=True,
                    orientation=template.get("orientation", "PORTRAIT"),
                    report=json.dumps(report, cls=JSONEncoder),
                    ewars_file=ewars_file,
                    client_file=client_file,
                    template_type=template_type,
                    report_data=report_data,
                    report_uuid=report_uuid)


report_uris = [
    (r"/report", ReportViewInterface),
    (r"/report/pdf/download/(.*)", PDFDownloadInterface),
    (r"/report/pdf", PDFCreateInterface)
]
