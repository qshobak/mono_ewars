import json
from ewars.core.interface import BaseRequestHandler

from ewars.core.export_forms import export_forms


class FormExportHandler(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        method = data[0]
        args = data[1] or list()
        kwargs = data[2] or dict()



        user = None
        if self.current_user is not None:
            user = self.current_user

        if user is None:
            self.send_error(status_code=403)

        kwargs['user'] = user
        args = [method] + args
        result = await _api.execute(args, kwargs)
        self.set_header('Content-type', 'application/json')
        self.write(result)
        self.finish()
