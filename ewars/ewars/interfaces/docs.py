import json
import sys
import inspect

from tornado.web import RequestHandler

from ewars.core.api.web._0 import API as WebAPI
from ewars.conf import settings


from ewars.constants import CONSTANTS
from ewars.core.api import get_api
from ewars.core.interface import BaseRequestHandler

from jose import jwt, JWTError


class APIDocsService(RequestHandler):
    def get(self):

        items = []

        for item, method in WebAPI._methods.items():
            name = "Not Implemented"
            description = "Not implemented"
            signature = ""

            if method is not None:
                name = method.__name__
                description = method.__doc__
                signature = inspect.signature(method)

            items.append([
                item,
                name,
                description,
                signature
            ])

        sort = sorted(items, key=lambda k: k[0])

        self.render("docs/api.html", items=sort)


docs_uris = [
    (r"/docs/development/api", APIDocsService),
]