import json
import uuid
import os
import base64
import tempfile
import boto3

from tornado.web import RequestHandler, asynchronous, os, RedirectHandler
from jose import jwt, JWTError

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.core.interface import BaseRequestHandler
from ewars.core.password_recovery import check_verification, reset_password, trigger_password_recovery
from ewars.core import identity
from ewars.core.serializers import JSONEncoder
from ewars.core import authentication
from ewars.core import clients
from ewars.conf import settings
from ewars.utils import _os
from ewars.utils import markdown
from .sync import SyncService
from ewars.core.notifications import handle_ses_topic
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs
import tornado
import tornado.gen

from ewars.models import Release

os.environ["AWS_ACCESS_KEY_ID"] = settings.AWS_ACCESS_KEY
os.environ["AWS_SECRET_ACCESS_KEY"] = settings.AWS_SECRET_KEY


class DownloadInterface(RequestHandler):
    async def get(self, file_name):
        file_location = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
        if not os.path.isfile(file_location):
            self.write_error(404)

        out = None
        with open(file_location, "rb") as f:
            out = f.read()

        os.remove(file_location)

        clean_file_name = file_name
        if '_' in file_name:
            clean_file_name = file_name.split("_", 1)[1]

        extension = clean_file_name.split(".")[1]

        if extension == "csv":
            self.set_header("Content-Type", "text/csv")

        if extension == 'zip':
            self.set_header('Content-Type', 'application/zip, application/octet-stream')

        self.set_header("Content-Disposition", "attachment=%s" % (clean_file_name))
        self.write(out)
        self.flush()


class UserRouter(BaseRequestHandler):
    async def get(self, user_id):
        client = clients.get_client("user")

        if client is None:
            self.write_error(404)

        kwargs = await self.render_client(client)
        if kwargs == "NO_AUTH":
            pass
        else:
            if kwargs is not None:
                self.render("template.html", **kwargs)
            else:
                self.render("template.html")


class RouterInterface(BaseRequestHandler):
    """Handles requests for client applications"""

    async def get(self, client_id):
        client = clients.get_client(client_id)

        if client is None:
            self.set_status(404)
            self.write("404 Not Found")
        else:
            kwargs = await self.render_client(client)
            if kwargs == "NO_AUTH":
                pass
            else:
                if kwargs is not None:
                    self.render("template.html", **kwargs)
                else:
                    self.render("template.html")


class SwapAccountInterface(BaseRequestHandler):

    async def get(self, target_tki, api_token):
        user_account = None
        account = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.users
                WHERE api_token = %s;
            """, (
                AsIs(target_tki),
                api_token,
            ))
            user_account = await cur.fetchone()

            await cur.execute("""
                SELECT status, domain FROM _iw.accounts
                WHERE tki = %s;
            """, (
                target_tki,
            ))
            account = await cur.fetchone()

        if user_account is None:
            self.redirect("/login", permanent=False)
            return

        if account.get('status', 'INACTIVE') != 'ACTIVE':
            self.redirect("/login", permanent=False)
            return

        if user_account.get('status', 'D') != 'ACTIVE':
            self.redirect("/login", permanent=False)
            return

        user_account['created'] = user_account['created'].strftime("%Y-%M-%d %H:%m")
        user_account['registered'] = user_account['registered'].strftime("%Y-%M-%d %H:%m")
        user_account['last_modified'] = user_account['last_modified'].strftime("%Y-%M-%d %H:%m")
        del user_account['password']

        cookie_token = str(uuid.uuid4()).replace("-", "_")
        session_data = await identity.persist_session(
            cookie_token,
            user_account,
            "ewars"
        )

        self.set_secure_cookie(
            "ewars_v4",
            cookie_token,
            expires_days=30,
            version=None,
            domain=account.get('domain')
        )

        if settings.DEBUG:
            self.redirect("http://%s:9000" % (account.get('domain')), permanent=False)
        else:
            self.redirect("http://%s" % (account.get('domain')), permanent=False)


class AdminRouterInterface(BaseRequestHandler):
    """Handles requests for client applications"""

    @tornado.gen.coroutine
    def get(self, client_id):
        client = clients.get_client(client_id)

        user = self.current_user

        if user is None:
            self.redirect("/login")
        elif client is None:
            self.set_status(404)
            self.write("404 Not Found")
        else:
            if "access" in client.keys():
                if user.get("role") not in client.get("access"):
                    self.write_error(404)
                else:
                    data = yield self.render_client(client)
                    self.render("template.html", **data)
            else:
                data = yield self.render_client(client)
                self.render("template.html", **data)


class DashboardInterface(BaseRequestHandler):
    async def get(self):
        client = clients.get_client("dashboard")
        data = await self.render_client(client)
        if data == "NO_AUTH":
            pass
        else:
            if data is not None:
                self.render("template.html", **data)
            else:
                self.render("template.html")


class DesktopInterface(BaseRequestHandler):
    @tornado.gen.coroutine
    def get(self):
        client = clients.get_client("desktop")
        data = yield self.render_client(client)

        if data is None:
            data = {}

        self.render("desktop.html", **data)


class CortexInterface(BaseRequestHandler):
    @tornado.gen.coroutine
    def get(self):
        user = self.current_user

        if user is None:
            self.render("404.html")
            return

        if user.get("user_type") != "SUPER_ADMIN":
            self.render("404.html")
            return

        client = clients.get_client("_cortex")
        data = yield self.render_client(client)

        if data == "NO_AUTH":
            pass
        else:
            if data is not None:
                self.render("cortex.html", **data)
            else:
                self.render("cortex.html", **data)


class RecoveryHandler(BaseRequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET, OPTIONS')

    @tornado.gen.coroutine
    def get(self):
        client = clients.get_client("recovery")

        data = yield self.render_client(client, public=True)
        self.render("template.html", **data)


class DesktopLoginInterface(BaseRequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with,E_C_TYPE,Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        if data is None:
            self.write(json.dumps({
                "error": "NO_PAYLOAD"
            }))

        if data.get('email', None) is None:
            self.write(json.dumps({
                "error": "NO_EMAIL"
            }))

        if data.get('password', None) is None:
            self.write(json.dumps({
                "error": "NO_PASSWORD"
            }))

        if data.get('domain', None) is None:
            self.write(json.dumps({
                "error": "NO_DOMAIN"
            }))

        if data.get('device_id', None) is None:
            self.write(json.dumps({
                "error": "NO_DEVICE_ID"
            }))

        account_domain = data.get('domain', None)

        if ":" in account_domain:
            account_domain = account_domain.split(':')[0]

        account = await authentication.get_account_by_domain(account_domain)

        if account is None:
            self.write(json.dumps(["ERROR", "NO_ACCOUNT", "Account specified does not exist"]))
            self.write(json.dumps({
                "error": "NO_ACCOUNT"
            }))

        user = await authentication.get_account_user(data.get('email'), account.get('tki'))

        if user is None:
            self.write(json.dumps({
                "error": "NO_USER_FOR_ACCOUNT"
            }))

        auth_result = await authentication.authenticate_user(
            account,
            data.get('email'),
            data.get('password')
        )

        if auth_result is False:
            self.write(json.dumps({
                "error": "AUTH_FAILED"
            }))

        token = jwt.encode(
            dict(
                user={
                    "uid": user.get('id'),
                    "name": user.get('name'),
                    "email": user.get('email'),
                    "account_name": account.get('name'),
                    "domain": account_domain,
                    "role": user.get('role'),
                    "status": user.get('status'),
                    "lid": user.get('location_id', None),
                    "tki": account.get('tki'),
                }
            ),
            settings.JWT_KEY,
            algorithm="HS256"
        )

        alternate_accounts = []

        if len(user.get('accounts', [])) > 1:
            # This user has multiple accounts, we need to pull tokens for those accounts
            # as well
            for acc in user.get('accounts', []):
                if acc != account.get('id'):
                    # Not the main account they're authing agains
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT * FROM _iw.accounts
                            WHERE id = %s;
                        """, (
                            acc,
                        ))
                        alt_account = await cur.fetchone()

                        await cur.execute("""
                            SELECT * FROM %s.users
                            WHERE id = %s;
                        """, (
                            AsIs(alt_account.get('tki')),
                            user.get('id'),
                        ))
                        alt_user = await cur.fetchone()

                    alt_token = jwt.encode(
                        dict(
                            user=dict(
                                uid=user.get('id'),
                                name=user.get('name'),
                                email=user.get('email'),
                                account_name=alt_account.get('name'),
                                domain=alt_account.get('domain'),
                                role=alt_user.get('role'),
                                status=alt_user.get('status'),
                                lid=alt_user.get('location_id', None),
                                tki=alt_account.get('tki'),
                            )
                        ),
                        settings.JWT_KEY,
                        algorithm="HS256"
                    )

                    alternate_accounts.append(dict(
                        uid=user.get('id'),
                        name=user.get('name'),
                        email=user.get('email'),
                        account_name=alt_account.get('name'),
                        domain=alt_account.get('domain'),
                        role=alt_user.get('role'),
                        status=alt_user.get('status'),
                        lid=alt_user.get('location_id', None),
                        tki=alt_account.get('tki'),
                        token=alt_token,
                    ))

        self.set_header("Content-Type", "application/json")
        self.write(json.dumps({
            "uid": user.get('id'),
            "name": user.get('name'),
            "email": user.get('email'),
            "account_name": account.get('name'),
            "domain": account_domain,
            "role": user.get('role'),
            "status": user.get('status'),
            "lid": user.get('location_id', None),
            "tki": account.get('tki'),
            "token": token,
            "alternates": alternate_accounts
        }))


class LoginInterface(BaseRequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with,E_C_TYPE,Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self):
        client = clients.get_client("login")
        # self.render_client(client, public=True)
        data = await self.render_client(client, public=True)
        self.render("template.html", **data)

    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        # Check if we're attempting to finish an auth for an account
        if "account_id" in data.keys():
            account_target = await authentication.get_account_by_id(data.get("account_id"))
            auth_result = await authentication.authenticate_user(
                account_target,
                data.get('email'),
                data.get('password')
            )

            # An error occurred authenticating the user, this
            # shouldn't happen as the user can't edit the password after the first pass
            # and this is only called from the account selection screen
            # if the user has access to multiple accounts
            if auth_result.get('err', False) is True:
                self.write(json.dumps(auth_result, cls=JSONEncoder))
                self.finish()
                return

            user = auth_result.get("user")
            user['created'] = user['created'].strftime("%Y-%M-%d %H:%m")
            user['registered'] = user['registered'].strftime("%Y-%M-%d %H:%m")
            user['last_modified'] = user['last_modified'].strftime("%Y-%M-%d %H:%m")
            del user['password']

            cookie_token = str(uuid.uuid4()).replace("-", "_")
            session_data = await identity.persist_session(cookie_token, user, "ewars")

            self.set_secure_cookie("ewars_v4", cookie_token, expires_days=30)

            for key, value in user.items():
                if isinstance(value, (uuid.UUID)):
                    user[key] = str(value)

            token = jwt.encode(dict(user=user), settings.JWT_KEY, algorithm="HS256")

            self.write(dict(
                err=False,
                token=token
            ))
            self.finish()
            return

        client_type = self.request.headers.get("E_C_TYPE", "W")

        if data.get("email", None) is None:
            self.write(json.dumps({"result": False, "code": 3}))
            self.finish()

        if data.get("password", None) is None:
            self.write(json.dumps(dict(result=False, code=4)))
            self.finish()

        # Need to check if this is a super admin
        tmp_user = await authentication.get_root_user(data.get("email"))
        if tmp_user is not None and tmp_user.get("user_type") == "SUPER_ADMIN":
            # This is a super admin user, we need to auth them,
            # then bump them to /cortex
            enc_pass, enc_salt = tmp_user.get("password").split(":")
            enc_plain = authentication.generate_password(data.get("password"), enc_salt)
            enc_plain = enc_plain.split(":")[0]
            if enc_pass == enc_plain:
                # This user checks out
                cookie_token = str(uuid.uuid4()).replace("-", "_")
                session_data = await identity.persist_session(cookie_token, tmp_user, "ewars")
                self.set_secure_cookie("ewars_v4", cookie_token, expires_days=30)
                self.write(json.dumps(dict(
                    result=True,
                    cortex=True
                )))
                self.finish()
                return
            else:
                self.write(json.dumps(dict(
                    result=False,
                    code="NO_AUTH"
                )))
                self.finish()
                return
        else:

            domain = self.request.host

            if domain in ("127.0.0.1:9000", "10.0.2.2:9000"):
                domain = "ewars.ws"

            account = None
            if domain != settings.ROOT_DOMAIN:
                account = await identity.get_token_by_domain(domain)

            auth_result = await authentication.authenticate_user(account, data.get("email"),
                                                                 data.get("password"))

            # An error occurred during authentication
            if auth_result.get("err", False):
                self.write(json.dumps(auth_result))
                self.finish()
                return
            # The user has multiple accounts and we're on the root domain, select an account to access
            elif auth_result.get("accounts", None) is not None:
                self.write(json.dumps(auth_result))
                self.finish()
                return
            # Users is logged in and ready to go
            else:
                user = auth_result.get("user")
                user['created'] = user['created'].strftime("%Y-%M-%d %H:%m")
                user['registered'] = user['registered'].strftime("%Y-%M-%d %H:%m")
                user['last_modified'] = user['last_modified'].strftime("%Y-%M-%d %H:%m")
                del user['password']

                if "accounts" in auth_result.keys() and domain:
                    user_accounts = await authentication.get_account_options(user.get("id"))
                    self.write(json.dumps(dict(
                        err=False,
                        code="MULTIPLE_ACCOUNTS",
                        accounts=user_accounts
                    ), cls=JSONEncoder))
                    self.finish()
                    return
                else:
                    if client_type in ("W", "M", None):
                        cookie_token = str(uuid.uuid4()).replace("-", "_")
                        session_data = await identity.persist_session(cookie_token, user, "ewars")

                        for key, value in user.items():
                            if isinstance(value, uuid.UUID):
                                user[key] = str(value)

                        self.set_secure_cookie("ewars_v4", cookie_token, expires_days=30)

                        token = jwt.encode(dict(user=user), settings.JWT_KEY, algorithm="HS256")

                        self.write(dict(
                            result=True,
                            uid=user.get('id'),
                            aid=user.get('aid'),
                            token=token
                        ))
                        self.finish()
                        return

                    if client_type == "D":
                        token = jwt.encode(
                            dict(
                                uid=user.get("id"),
                                aid=user.get('aid', None),
                                name=user.get("name", None),
                                email=user.get("email", None)
                            ),
                            settings.JWT_KEY,
                            algorithm="HS256",
                            headers=dict(
                                uid=user.get("id")
                            )
                        )

                        self.write(json.dumps(dict(
                            result=True,
                            aid=user.get('aid'),
                            uid=user.get('id'),
                            token=token
                        )))
                        self.finish()
                        return

        self.write(json.dumps(dict(
            err=True,
            code="UNKNOWN_ERROR"
        )))


class OrchestrationInterface(RequestHandler):
    def get(self):
        self.write("Hello orchestration")


class LogoutInterface(RequestHandler):
    async def get(self):
        session_token = self.get_secure_cookie("ewars_v4")
        if session_token is not None:
            await identity.delete_session(session_token.decode("utf-8"))

        self.clear_cookie("beaker.session.id")
        self.clear_cookie("ewars_v4")
        self.redirect("/login", permanent=False)


class LocationProfile(RequestHandler):
    def get(self):
        location_uuid = self.get_argument("uuid")

        self.render("location.html", )


class HelpRouter(RequestHandler):
    def get(self, path):
        help_path = path.split("/")

        file_name = help_path[-1] + ".md"
        actual_path = "%s/%s" % (
            "/".join(help_path[0:-1]),
            file_name,
        )

        content = None
        install_path = _os.get_install_root()
        file_path = os.path.join(install_path, "docs", actual_path)
        with open(os.path.join(_os.get_install_root(), "docs", actual_path), "rb") as f:
            content = f.read()

        md = markdown.markdown(content, extras=[
            "fenced-code-blocks",
            "footnotes",
            "numbering",
            "header-ids",
            "tables",
            'cuddled-lists'
        ])

        self.render("help.html",
                    content=md)


class PasswordRecovery(RequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        result = await trigger_password_recovery(data.get("email"))

        self.write(json.dumps(result))


class PasswordReset(RequestHandler):
    """
        Handles routing from password reset emails into system
        to verify and input a new password
    """

    async def get(self, verification_code):
        if await check_verification(verification_code):
            self.render("password_reset_input.html", error=None, verification_token=verification_code)
        else:
            self.send_error(404)

    async def post(self, verification_code):
        password = self.get_body_argument("password")
        confirm_pass = self.get_body_argument("confirm_pass")

        err = None

        if password != confirm_pass:
            err = "Passwords do not match"

        if len(password) < 6:
            err = "password must be at least 6 characters in length"

        if err:
            self.render("password_reset_input.html", error=err, verification_token=verification_code)

        result = await reset_password(verification_code, password)

        if result:
            self.render("password_reset.html")


class ReleaseDocs(RequestHandler):
    async def get(self, path):
        if path == "index":
            r_list = Release.get_release_list()
            content = Release.get_release_file(r_list[0][1])
            self.render(
                "release.html",
                releases=r_list,
                content=content
            )
        else:
            content = Release.get_release_file(path)
            r_list = Release.get_release_list()
            self.render(
                "release.html",
                releases=r_list,
                content=content
            )


def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += '=' * (4 - missing_padding)
    return base64.b64decode(data)


class UploadInterface(RequestHandler):
    async def post(self):
        fileinfo = self.request.files['file'][0]
        fname = fileinfo['filename']
        extn = os.path.splitext(fname)[1]

        origFileName = self.get_body_argument("fileName", default=None, strip=False)
        fileType = self.get_body_argument("fileType", default=None, strip=False)
        tki = self.get_body_argument("tki", default=None, strip=False)
        local = self.get_body_argument("local", default=False, strip=False)

        fileName = "account/%s/%s" % (tki, origFileName)

        if local:
            # We're storing this locally
            print(_os.get_install_root())
            end_file = os.path.join(_os.get_install_root(), "static/_data/", origFileName)
            with open(end_file, "wb") as f:
                f.write(fileinfo['body'])

            self.write(json.dumps(dict(
                fn=end_file
            )))

        else:
            fp = tempfile.NamedTemporaryFile()
            with open(fp.name, "wb") as f:
                f.write(fileinfo['body'])

            s3 = boto3.client("s3")
            s3.upload_file(
                fp.name,
                "cdn.ewars.ws",
                fileName,
                ExtraArgs={
                    "ACL": "public-read"
                })

            fp.close()

            self.write(json.dumps(dict(
                fn="http://cdn.ewars.ws/%s" % (fileName)
            )))


class PingHandler(RequestHandler):
    def get(self):
        self.write("PONG")


class OpsInterface(RequestHandler):
    """ Handles SNS topic notifications for SES

    Args:
        RequestHandler:

    Returns:

    """

    def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        result = handle_ses_topic(data)

        self.write(json.dumps({"result": True}))


class HealthRouter(RequestHandler):
    def get(self):
        self.write("Instance Health: OK")


class KafkaProduceTest(BaseRequestHandler):
    def get(self):
        for _ in range(0, 100):
            self.application.kafka.send("foobar", dict(
                uuid="12345",
                data_date="2017-01-01",
                location_id="12345-1233455-12312321",
                data=dict(
                    meas_u5=1,
                    measd_o5=2
                )
            ))

        self.application.kafka.flush()

        self.write("OK")


applications = [
    (r"/kafka", KafkaProduceTest),
    (r"/desktop", DesktopInterface),
    (r"/desktop/login", DesktopLoginInterface),
    (r"/health", HealthRouter),
    (r"/", DashboardInterface),
    (r"/cortex", CortexInterface),
    (r"/user/(.*)", UserRouter),
    (r"/ping", PingHandler),
    (r"/upload", UploadInterface),
    (r"/api/ops/sns", OpsInterface),
    (r"/releases/(.*)", ReleaseDocs),
    (r"/api/sync/android/v2", SyncService),
    (r"/api/Login\(\)", LoginInterface),
    (r"/orchestration", OrchestrationInterface),
    (r"/admin/(.*)", AdminRouterInterface),
    (r"/login", LoginInterface),
    (r"/logout", LogoutInterface),
    (r"/download/(.*)", DownloadInterface),
    (r"/help/(.*)", HelpRouter),
    (r"/recovery", RecoveryHandler),
    (r"/authentication/reset/(.*)", PasswordReset),
    (r"/authentication/recovery", PasswordRecovery),
    (r"/swap/(.*)/(.*)", SwapAccountInterface),
    (r"/(.*)", RouterInterface),
]
