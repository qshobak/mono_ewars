#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
import os
import sys
import logging
import click
import socket

from ewars import __version__
from ewars import utils
from ewars import exceptions
from ewars import config
from ewars.commands import run

log = logging.getLogger(__name__)

click.disable_unicode_literals_warning = True

class State(object):

    def __init__(self, log_name="jdu", level=logging.INFO):
        self.logger = logging.getLogger(log_name)
        self.logger.propagate = False
        stream = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)-7s - %(message)s ")
        stream.setFormatter(formatter)
        self.logger.addHandler(stream)

        self.logger.setLevel(level)


def verbose_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        if value:
            state.logger.setLevel(logging.DEBUG)
    return click.option('-v', '--verbose',
                        is_flag=True,
                        expose_value=False,
                        help='Enable verbose output',
                        callback=callback)(f)


def quiet_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        if value:
            state.logger.setLevel(logging.ERROR)

    return click.option('-q', '--quiet', is_flag=True,
		    expose_value=False,
		    help='Silence warnings',
		    callback=callback)(f)

def common_options(f):
    f = verbose_option(f)
    f = quiet_option(f)
    return f

pass_state = click.make_pass_decorator(State, ensure=True)
config_help = "Provide a specific JDU config"
debug_help = "Run the site in debug mode"

pgk_dir = os.path.dirname(os.path.abspath(__file__))

@click.group(context_settings={'help_option_names': ['-h', '--help']})
@click.version_option(
        '{0} from {1} (Python {2})'.format(__version__, pgk_dir, sys.version[:3]),
        '-V', '--version')
@common_options
def cli():
    """
    JDU - Company web site with Markdown
    """

@cli.command(name="run")
@click.option("-c", "--config-file", type=click.File('rb'), help=config_help)
@click.option('-d', '--debug', is_flag=True, help=debug_help)
@common_options
def serve_command(config_file, debug):
    logging.getLogger("tornado").setLevel(logging.WARNING)

    try:
        run.serve(
            config_file=config_file,
            debug=debug,
        )
    except (exceptions.ConfigurationError, socket.error) as e:
        raise SystemExit('\n' + str(e))

if __name__ == '__main__':
    cli()
