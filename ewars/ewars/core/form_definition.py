GRID_TYPES = ["matrix", "row", "text", "select", "numeric", "date", 'location', 'number', 'user', 'lat_long']


def merge(a, b, path=None):
    """ Merges a into b recursively

    Args:
        a: source dict
        b: target dict
        path:

    Returns:
        merged dicts
    """
    if path is None:
        path = []

    for key, item in a.items():
        if key not in b.keys():
            b[key] = item

        if isinstance(item.get("fields", None), dict) \
                and isinstance(b[key].get("fields", None), dict):
            b[key]['fields'] = merge(item['fields'], b[key]['fields'])

    return b


def _extrapolate_grid(versions):
    """ Extrapolates a grid definition

    THis is used for displaying submissions of this form type within
    a DataTable or for plucking information from the report. It essentially
    takes a list of form definitions and combnines/coalesces them into
    a single definition which can accommodate rendering submissions
    of any version in a single table.

    Args:
        versions: A list of form definition versions

    Returns:

    """
    grid = {}

    for version in versions:
        if grid is None:
            grid = version.get("definition")
        else:
            grid = merge(version.get("definition"), grid)

    return grid


def _ancestor(group, row_source, level):
    results = []

    groups = {}

    for item in group:
        try:
            groups[".".join(item.split(".")[0:-(level)])].append(item)
        except KeyError:
            groups[".".join(item.split(".")[0:-(level)])] = [item]

    for g_parent, ic_group in groups.items():

        # If all the children are same length
        if all(len(x) == len(ic_group[0]) for x in ic_group):
            # All items are same length
            # Are all items 1 length longer than the parent key?
            if all(len(x.split(".")) == (len(g_parent.split(".")) + 1) for x in ic_group):
                if sum([1 for x in row_source if x.startswith(g_parent) and len(x) > len(ic_group[0])]):
                    results = results + ic_group
                else:
                    for x in range(0, len(ic_group)):
                        results.append(g_parent)
            elif g_parent == '':
                results = results + ic_group
                # if the source row contains an item which shares
                # the same ancester but is longer than the items in this group
                # Then we the items up

    return results


def _iter(item, level, parentId):
    id_count = 1

    roots = []
    db = {}

    sorted_def = []
    for key, value in item.items():
        if value.get("type", "text") in GRID_TYPES:
            value['name'] = key
            sorted_def.append(value)
    sorted_def = sorted(sorted_def, key=lambda k: int(k.get("order", 0)))

    for field in sorted_def:
        id = '%s.%s' % (parentId, id_count)
        db[str(id)] = dict(
            name=field.get("name"),
            id=id,
            cols=0,
            rows=0,
            row=level,
            col=0,
            config=field
        )

        id_count += 1

        if field.get("fields", None) is not None:
            sub_roots, sub_db = _iter(field.get("fields"), level + 1, id)
            roots = roots + sub_roots

            for key, value in sub_db.items():
                db[key] = value

        else:
            roots.append(str(id))

    return roots, db


def _generate_table(definition):
    """Generates a table spec

    This is used for generating a list of thead element
    definition used by the application to render a nested table
    header for a complex nested form definition.

    This process builds up the definition from the bottom layer up to the top.
    Adjusting rowSpan and colSpan in order to produce and evenly distributed
    table header.

    Each table cell has it's rowStart, cellStart, with and height defined
    as well as an incremental id numbe (1, 1.1, 1.1.1, 1.2.1) which is lengthened
    based on depth of the child the id represents.

    Args:
        definition: The definition to process

    Returns:
        A list of column definitions for the table layout

    """

    db = {}
    roots = []

    last_id = 1  # We start the key at one

    sorted_def = []
    for key, value in definition.items():
        if value.get("type", "text") in GRID_TYPES:
            value['name'] = key
            sorted_def.append(value)
    sorted_def = sorted(sorted_def, key=lambda k: float(k.get('order', 0) if k.get("order") is not None else 999))

    for item in sorted_def:
        id = last_id

        last_id += 1

        if item.get("fields", None) is not None:
            sub_roots, sub_db = _iter(item.get("fields"), 1, id)
            roots = roots + sub_roots
            for sub_key, value in sub_db.items():
                db[sub_key] = value
        else:
            roots.append(str(id))

        db[str(id)] = dict(
            id=id,
            name=item.get("name"),
            cols=0,
            rows=0,
            row=None,
            col=None,
            config=item
        )

    results = [roots]

    # Use the longest path name to figure out what the row depth for
    # the table should be
    row_depth = max([len(x.split(".")) for x in roots])

    results.append([])
    results = results + [[] for x in range(0, row_depth + 1)]
    last_row = results[0]

    for i, row in enumerate(results):
        processed = []
        if i < row_depth:
            for item in row:
                if len(item.split(".")) == 1:
                    processed.append(item)
                    results[i + 1].append(item)
                else:
                    leader = tuple(item.split(".")[:-1])
                    if leader not in processed:
                        processed.append(leader)
                        count = sum([1 for x in row if tuple(x.split(".")[:-1]) == leader])
                        results[i + 1] = results[i + 1] + [".".join(leader) for y in range(0, count)]

    results = reversed(results)

    tmp_results = []
    for row in results:
        row.sort(key=lambda s: [int(u) for u in s.split(".")])
        if row not in tmp_results and len(row) > 0:
            tmp_results.append(row)

    results = tmp_results

    definition = []

    for key, item in db.items():
        for idx, row in enumerate(results):
            if key in row:
                if item.get("rows", None) is None:
                    item['rows'] = 0
                item['rows'] += 1
                if item.get("row", None) is None:
                    item['row'] = idx
                if item.get("cell", None) is None:
                    item['cell'] = row.index(key)
                item['cols'] = sum([1 for x in row if x == key])
                if item.get("col") is None:
                    item['col'] = row.index(key)

        definition.append(item)

    columns = []

    for id in results[-1]:
        columns.append(db.get(id))

    for column in columns:
        ids = str(column.get("id")).split(".")
        index = 0
        path = ids[0]

        name = []
        name_len = len(ids)
        if column['config'].get("root", False) is False:
            name.append("data")
            name_len += 1

        while True:
            name.append(db[path].get("name"))
            if len(name) >= name_len:
                break

            index += 1
            path = "%s.%s" % (path, ids[index])

        column['name'] = ".".join(name)

    return dict(
        definition=definition,
        column_ids=results[-1],
        columns=sorted(columns, key=lambda x: sorted(x.keys())),
        column_count=len(results[-1])
    )

IGNORED_FIELD_TYPES = ["header", "matrix", "row", "display"]


def _flatten_definition(definition, parent_key, parent_order, parent_name):
    """ Flatten the definition of a form
    
    Args:
        definition: 
        parent_key: 
        parent_order: 
        parent_name: 

    Returns:

    """
    results = []
    for key, value in definition.items():
        order = "%s.%s" % (parent_order, value.get("order"))
        new_key = "%s.%s" % (parent_key, key)

        label = value.get('label', None)

        if isinstance(label, (dict,)):
            label = label.get('en')

        if label is None:
            label = "Unlabelled"

        new_name = "%s \ %s" % (parent_name, label)
        if value.get("type") in IGNORED_FIELD_TYPES:
            if value.get("fields", None) is not None:
                sub_results = _flatten_definition(value.get("fields", {}), new_key, order, new_name)
                results = results + sub_results
        else:
            results.append([order, new_key, new_name, value.get("type", None), value])

    return results


def _flat_def(definition, parent_key, parent_order, parent_name, form_id):
    """ Alternate flattening logic
    
    Args:
        definition: 
        parent_key: 
        parent_order: 
        parent_name: 
        form_id: 

    Returns:

    """
    results = []
    for key, value in definition.items():
        order = "%s.%s" % (parent_order, value.get("order"))
        new_key = "%s.%s" % (parent_key, key)
        label = value.get('label', None)

        if isinstance(label, (dict,)):
            label = label.get('en')

        if label is None:
            label = "Unlabelled"

        new_name = "%s \ %s" % (parent_name, label)
        if value.get("type") in IGNORED_FIELD_TYPES:
            if value.get("fields", None) is not None:
                sub_results = _flat_def(value.get("fields", {}), new_key, order, new_name, form_id)
                results = results + sub_results
        else:
            results.append([
                form_id,
                order,
                new_key,
                value.get("type"),
                new_name,
                value
            ])

    return results