import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.utils import date_utils
from ewars.utils.isoweek import Week
from ewars.utils import form_utils

from tornado.template import Template

from ewars.core.notifications import send
from ewars.content import emails
from ewars.constants import CONSTANTS


async def get_report(template_id, report_date, user=None):
    """Retrieve a report template and information for display

    Args:
        template_id: The UUID of the template
        report_date: The report date for the template
        user: The calling user

    Returns:

    """
    template = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid, template_name, instance_name, content, status, generation, shareable, access
            FROM %s.templates
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            template_id,
        ))
        template = await cur.fetchone()


async def find_template(template_id):
    """ Search through the system for a template

    Args:
        template_id: THe UUID of the template

    Returns:

    """

    instances = ["ewars"]

    template = None

    for instance in instances:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.templates WHERE uuid = %s
            """, (
                AsIs(instance),
                template_id,
            ))
            result = await cur.fetchone()

            if result is not None:
                template = (instance, result)

    return template


def format_document_title(instance_name, definition=None, report_date=None, template_type=None, report_uuid=None,
                          location=None):
    result = ""

    if isinstance(instance_name, (dict,)):
        result = instance_name.get("en")
    else:
        result = instance_name

    result = instance_name.get("en")

    nodes = [
        "year",
        "report_date",
        "report_start_date",
        "report_end_date",
        "month",
        "year",
        "day",
        "location",
        "day_week",
        "week_no",
        "week",
        "location_name",
        "name",
        "today"
    ]

    r_date = date_utils.parse_date(report_date)

    start_date = date_utils.get_start_of_interval(r_date, definition['generation']['interval'])
    week_no = Week.withdate(r_date).week
    week = Week.withdate(r_date).day(6)

    location_name = "No associated location"
    if location is not None:
        location_name = location.get("name")['en']

    interval = None
    if template_type == "GENERATED":
        interval = definition['generation'].get("interval", "DAY")

    today = datetime.datetime.utcnow()

    result = result.replace("{year}", str(r_date.year))
    result = result.replace("{report_date}", date_utils.display(r_date, interval))
    result = result.replace("{report_start_date}", start_date.strftime("%Y-%m-%d"))
    result = result.replace("{report_end_date}", r_date.strftime("%Y-%m-%d"))
    result = result.replace("{month}", str(r_date.month))
    result = result.replace("{day}", str(r_date.day))
    result = result.replace("{location}", location_name)
    result = result.replace("{day_week}", r_date.strftime("dddd"))
    result = result.replace("{week_no}", str(week_no))
    result = result.replace("{week}", str(week_no))
    result = result.replace("{location_name}", location_name)
    result = result.replace("{today}", today.strftime("%H:%M %A, %d %B %Y"))

    # result = result.replace("Raw Value", "")

    return result


async def format_template(content, definition=None, report_date=None, location=None, template_type=None,
                          report_uuid=None):
    """ Format a template for rendering

    Args:
        content: The content of the template
        definition: The definition for the template
        report_date: The report date for the template
        location: The location of the report

    Returns:

    """
    result = content

    collected = None

    if template_type == "PER_REPORT":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.uuid,
                    c.data_date,
                    c.data,
                    f.name AS form_name,
                    v.definition AS definition
                FROM ewars.collections AS c
                    LEFT JOIN ewars.forms AS f on f.id = c.form_id
                    LEFT JOIN ewars.form_versions AS v ON v.uuid = c.form_version_id
                WHERE c.uuid = %s
            """, (
                report_uuid,
            ))
            collected = await cur.fetchone()

    def get_field(field_path):
        return collected['data'].get(field_path, "Not found")

    def get_field_display(field_path):
        field = form_utils.get_field_definition(collected.get("definition"), field_path)

        value = collected['data'].get(field_path, None)

        if value is None:
            return "Not found"

        if field is None:
            return "Not found"

        disp_value = "Not found"
        if field.get("type") == "select":
            for opt in field.get("options"):
                if opt[0] == value:
                    disp_value = opt[1]
        elif field.get("type") == "date":
            date_utils.display(value, field.get("date_type", "DAY"))

        return disp_value

    nodes = [
        "year",
        "report_date",
        "report_start_date",
        "report_end_date",
        "month",
        "year",
        "day",
        "location",
        "day_week",
        "week_no",
        "week",
        "location_name",
        "name",
        "today"
    ]

    t = Template(result)

    r_date = date_utils.parse_date(report_date)

    start_date = date_utils.get_start_of_interval(r_date, definition['generation']['interval'])
    week_no = Week.withdate(r_date).week
    week = Week.withdate(r_date).day(6)

    location_name = "No associated location"
    if location is not None:
        location_name = location.get("name")['en']

    interval = None
    if template_type == "GENERATED":
        interval = definition['generation'].get("interval", "DAY")

    today = datetime.datetime.utcnow()
    result = t.generate(
        year=str(r_date.year),
        report_date=date_utils.display(r_date, interval),
        report_start_date=start_date.strftime('%Y-%m-%d'),
        report_end_date=r_date.strftime("%Y-%m-%d"),
        month=str(r_date.month),
        day=str(r_date.day),
        location=location_name,
        day_week=r_date.strftime("dddd"),
        week_no=str(week_no),
        week=str(week_no),
        get_field=get_field,
        get_field_display=get_field_display,
        location_name=location_name,
        today=today.strftime("%H:%M %A, %d %B %Y"),
        name=""
    )

    result = result.decode("utf-8")
    result = result.replace("{year}", str(r_date.year))
    result = result.replace("{report_date}", date_utils.display(r_date, interval))
    result = result.replace("{report_start_date}", start_date.strftime("%Y-%m-%d"))
    result = result.replace("{report_end_date}", r_date.strftime("%Y-%m-%d"))
    result = result.replace("{month}", str(r_date.month))
    result = result.replace("{day}", str(r_date.day))
    result = result.replace("{location}", location_name)
    result = result.replace("{day_week}", r_date.strftime("dddd"))
    result = result.replace("{week_no}", str(week_no))
    result = result.replace("{week}", str(week_no))
    result = result.replace("{location_name}", location_name)
    result = result.replace("{today}", "%s UTC" % (today.strftime("%H:%M %A, %d %B %Y")))

    # result = result.replace("Raw Value", "")

    return result


async def share_report(reportURI, recipients, instance_name, user=None):
    """  Share a document within the system

    Args:
        reportURI (str): The URI of the report
        recipients (str): The recipients to send the report out to
        user (obj): The sharing user

    Returns:
        True if the emails were sent
   """

    recipients = recipients.replace(" ", "").split(",")

    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT domain
            FROM _iw.accounts
            WHERE id = %s;
        """, (
            user.get('aid'),
        ))
        account = await cur.fetchone()

    # Iterate through recipients and send email
    for recip in recipients:
        send(
            "SHARE",
            recip,
            dict(
                report_title=instance_name,
                uri="http://%s%s" % (
                    account.get('domain'),
                    reportURI
                ),
                sender_name=user['name'],
                sender_email=user['email']
            ),
            subject="[EWARS] A document has been shared with you",
            account=user.get("account_name", None),
            user=user
        )

    return True


async def _get_locations(generation, user=None):
    locations = []

    if generation.get("location_spec") == "SPECIFIC":
        loc_id = generation.get("location_uuid", None)
        if loc_id is None:
            loc_id = generation.get("location", None)

        if loc_id is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name FROM %s.locations
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    generation.get("location_uuid"),
                ))
                locations = await cur.fetchall()

    elif generation.get("location_spec") == "TYPE":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s
                        AND status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                [user.get("clid")],
            ))
            locations = await cur.fetchall()

    return locations


async def get_user_documents(user=None):
    """Retrieve latest documents that a user has access to

    Args:
        user:

    Returns:

    """
    results = []
    templates = []

    if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, template_name, instance_name, generation, status, template_type
                FROM %s.templates
                WHERE (status = 'ACTIVE' OR status = 'DRAFT');
            """, (
                AsIs(user.get("tki")),
            ))
            templates = await cur.fetchall()
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, template_name, instance_name, generation, template_type
                    FROM %s.templates
                    WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
            ))
            templates = await cur.fetchall()

    for template in templates:
        locations = await _get_locations(template.get("generation"), user=user)

        results.append(dict(
            template=template,
            locations=locations
        ))

    return results


async def documents_action(action, query, user=None):
    """ Retrieve information about documents

    Args:
        action: The action
        query: Any querying
        user: The calling user

    Returns:
        A list of documents made available in the last week
    """
    result = None

    if action == "RECENT":
        result = await get_recently_available(user=user)

    return result


def document_action(action, doc_id, data, user=None):
    """Perform an action against a document

    Args:
        action: The action to perform
        doc_id: The UUID of the document
        data: Data associated with the action
        user: The caling user

    Returns:
        The result of the action
    """
    result = None

    if action == "REPORTS":
        result = get_reports(doc_id, data, user=user)
    elif action == "QUERY":
        pass
    elif action == "SHARE":
        result = share_report(
            data.get("uri"),
            data.get("recipients"),
            data.get("name"),
            user=user
        )

    return result



async def get_reports(template_id, query, user=None):
    """ Retrieve reports available for a give document template

    Args:
        template_id: The UUID of the template
        user: THe calling user

    Returns:
        The spec for the template
    """

    template = None
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.templates WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            template_id,
        ))
        template = await cur.fetchone()

    t_type = template.get("template_type")

    if t_type == "GENERATED":
        total, results = _query_generated(template, query, user=user)
    elif t_type == "ONE_OFF":
        total, results = _get_one_off(template, query, user=user)
    elif t_type == "PER_REPORT":
        total, results = _query_per_report(template, query, user=user)

    return dict(
        total=total,
        documents=results
    )


def _get_one_off(template, query, user=None):
    """ Query one-off reports

    Args:
        template: The template representing the item
        query: The query to filter the date on
        user: The calling user

    Returns:

    """
    results = []

    results.append(dict(
        uuid=template.get("uuid"),
        doc_date=template['generation']['document_date'],
        doc_data_date=template['generation']['document_date'],
        instance_name=template.get("instance_name"),
        template_name=template.get("template_name"),
        status=template.get("status"),
        template_type="ONE_OFF",
        interval=CONSTANTS.DAY
    ))

    return 1, results


async def _query_generated(template, query, user=None):
    """ Query into a set of generated reports

    Args:
        template: The template being queried against
        query: The query itself
        user: THe calling user

    Returns:

    """
    results = []
    total_documents = 0

    # Figure out total reports
    start_date = date_utils.parse_date(template['generation']['start_date'])
    end_date = datetime.date.today()
    today = datetime.date.today()
    if template['generation'].get("end_date") is not None:
        end_date = date_utils.parse_date(template['generation'].get("end_date"))

        if end_date > today:
            end_date = today
    else:
        end_date = end_date - datetime.timedelta(days=1)

    location_spec = template['generation'].get("location_spec")

    if location_spec == "SPECIFIC":
        intervals = date_utils.get_date_range(start_date, end_date, template['generation']['interval'])
        total_documents = intervals
    elif location_spec == "LOCATION_TYPE":
        locations = 0
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.locations
                WHERE site_type_id = %s
                AND status = 'ACTIVE'
                AND lineage::TEXT[] @> %s::TEXT[]
            """, (
                AsIs(user.get("tki")),
                template['generation']['location_type'],
                [template['generation']['location_uuid']],
            ))
            locations = await cur.fetchone()['total']

        intervals = date_utils.get_intervals_between(start_date, end_date, template['generation']['interval'])
        total_documents = intervals * locations

    # Retrieve actual reports
    if location_spec == "SPECIFIC":
        location = None
        loc_id = template['generation'].get("location_uuid", None)
        if loc_id is None:
            loc_id = template['generation'].get("location_id", None)
        if loc_id is None:
            loc_id = template['generation'].get("location", None)


        if loc_id is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name FROM %s.locations
                    WHERE uuid =%s
                """, (
                    AsIs(user.get("tki")),
                    loc_id,
                ))
                location = await cur.fetchone()

        if location is None:
            location = dict()

        intervals = date_utils.get_range_set(start_date, end_date, template['generation']['interval'])

        for interval in intervals:
            results.append(dict(
                uuid=template.get("uuid"),
                doc_date=interval.get("y"),
                doc_data_date=interval.get("y"),
                instance_name=template.get("instance_name"),
                template_name=template.get("template_name"),
                account_id=user.get("aid"),
                tki=user.get("tki"),
                status=template.get("status"),
                template_type="GENERATED",
                interval=template['generation']['interval'],
                location_id=location.get("uuid"),
                location=location
            ))

    return total_documents, sorted(results, key=lambda k: k["doc_date"], reverse=True)


async def _query_per_report(template, query, user=None):
    """Query into a per-report template definition

    Args:
        template: The template being queries against
        query: The query filtering the results
        user: The calling user

    Returns:

    """
    results = []

    total_documents = 0

    # Get toal items
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.collections
            WHERE form_id = %s
            AND status = 'SUBMITTED'
        """, (
            AsIs(user.get("tki")),
            template['generation']['source_form_id'],
        ))
        total_documents = await cur.fetchone()['total']

        # Retrieve actual reports
        await cur.execute("""
            SELECT c.uuid,
                c.data_date,
                c.location_id,
                l.name AS location_name
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.status = 'SUBMITTED'
                AND form_id = %s
            ORDER BY c.submitted_date DESC
            LIMIT %s
            OFFSET %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            template['generation']['source_form_id'],
            query.get("limit"),
            query.get("offset"),
        ))
        results = await cur.fetchall()

    data = []

    for result in results:
        data.append(dict(
            uuid=template.get("uuid"),
            doc_date=result.get('data_date'),
            doc_data_date=result.get('data_date'),
            instance_name=template.get("instance_name"),
            template_name=template.get("template_name"),
            account_id=user.get("aid"),
            tki=user.get("tki"),
            status=template.get("status"),
            template_type="PER_REPORT",
            interval=CONSTANTS.DAY,
            report_uuid=result.get("uuid"),
            location_id=result.get("location_id"),
            location=dict(
                uuid=result.get("location_id"),
                name=result.get("location_name")
            )
        ))

    return total_documents, sorted(data, key=lambda k: k['doc_date'], reverse=True)



async def get_recently_available(user=None):
    """ Get recently available documents based on
    a users permissions

    Args:
        user: The calling user

    Returns:
        A list of reports to load
    """
    templates = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid, template_name, instance_name, generation, template_type
            FROM %s.templates
                WHERE status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
        ))
        templates = await cur.fetchall()

    reports = []

    for template in templates:
        if template.get("template_type") == "PER_REPORT":
            reports = reports + _per_report_recent(template, user=user)
        elif template.get("template_type") == "ONE_OFF":
            reports = reports + _one_off_recent(template, user=user)
        elif template.get("template_type") == "GENERATED":
            reports = reports + _generated_recent(template, user=user)

    return reports


def _calculate_recent(document, user=None):
    now = datetime.datetime.now()


def _generated_recent(document, user=None):
    """ For generated document templates
    calculate the dates that should be available
    for reports and locations

    Args:
        document: The document that shoudl be calculated for
        user: THe calling user

    Returns:

    """
    reports = []

    week_end = datetime.date.today()
    week_start = week_end - datetime.timedelta(days=30)

    gen_start_date = date_utils.parse_date(document['generation']['start_date'])

    if gen_start_date > week_start:
        week_start = gen_start_date

    loc_spec = document['generation']['location_spec']

    if loc_spec == "SPECIFIC":
        location = None
        loc_id = document['generation'].get("location_uuid", None)
        if loc_id is None:
            loc_id = document['generation'].get("location_id", None)
        if loc_id is None:
            loc_id = document['generation'].get("location", None)

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT uuid, name
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                loc_id,
            ))
            location = cur.fetchone()


        if location is None:
            location = dict()

        intervals = date_utils.get_range_set(week_start, week_end, document['generation']['interval'])
        if document.get("generation", {}).get("interval", "DAY") == "DAY":
            intervals.reverse()

        for key_date in intervals[:-1]:
            reports.append(dict(
                uuid=document.get("uuid"),
                doc_date=key_date.get("y"),
                doc_data_date=key_date.get("y"),
                location=location,
                location_id=location.get("uuid"),
                instance_name=document.get("instance_name"),
                template_name=document.get("template_name"),
                status=document.get("status"),
                template_type="GENERATED",
                interval=document['generation']['interval'] or CONSTANTS.DAY
            ))

    return reports


def _one_off_recent(document, user=None):
    results = []

    week_end = datetime.date.today()
    week_start = week_end - datetime.timedelta(days=30)

    doc_date = date_utils.parse_date(document['generation']['document_date'])

    if doc_date <= week_end and doc_date >= week_start:
        results.append(dict(
            uuid=document.get("uuid"),
            doc_date=doc_date,
            doc_data_date=doc_date,
            instance_name=document.get("instance_name"),
            template_name=document.get("template_name"),
            status=document.get("status"),
            template_type="ONE_OFF",
            interval=CONSTANTS.DAY
        ))

    return results


def _per_report_recent(document, user=None):
    """ Retrieve any PER_REPORT reports for the past week

    Args:
        document:
        user:

    Returns:

    """
    week_end = datetime.date.today()
    week_start = week_end - datetime.timedelta(days=30)

    reports = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT c.uuid, c.data_date, c.location_id, l.name->>'en' AS location_name
            FROM %s.collections AS c
            LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
            AND c.submitted_date >=  %s
            AND c.submitted_date <= %s
            AND c.status = 'SUBMITTED'
            ORDER BY c.submitted_date DESC
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            document['generation']['source_form_id'],
            week_start,
            week_end,
        ))
        reports = cur.fetchall()

    results = []

    for report in reports:
        results.append(dict(
            uuid=document.get("uuid"),
            doc_date=report.get("submitted_date"),
            doc_data_date=report.get("data_date"),
            location_name=report.get("location_name"),
            location_id=report.get("location_id"),
            instance_name=document.get("instance_name"),
            doc_id=document.get("uuid"),
            template_name=document.get("template_name"),
            status=document.get("status"),
            template_type="PER_REPORT",
            interval=CONSTANTS.DAY,
            report_uuid=report.get("uuid")
        ))

    return results