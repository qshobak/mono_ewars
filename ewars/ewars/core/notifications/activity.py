import datetime
import json

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def get_user_activity(offset, user=None):
    """ Get activity feed items which are centric to a user
    :param user_id: {int} The id of the user
    :param user: {dict} The calling user
    :return:
    """

    results = []
    total = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*, u.name
            FROM %s.activity_feed AS a
              LEFT JOIN _iw.users AS u ON u.id = a.triggered_by
            ORDER BY a.created DESC
            LIMIT 10
            OFFSET %s;
        """, (
            AsIs(user.get("tki")),
            offset,
        ))
        results = await cur.fetchall()

    for item in results:
        item_data = None
        if len(item.get("attachments")) > 0:
            item_type = item.get("attachments")[0][0]
            item_id = item.get("attachments")[0][1]

            if item_type == "REPORT":
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT f.name->>'en' AS form_name,
                            l.name->>'en' AS location_name,
                            c.data_date,
                            c.uuid,
                            f.features->'INTERVAL_REPORTING'->'interval' AS reporting_interval
                        FROM %s.collections AS c
                          LEFT JOIN %s.forms AS f ON f.id = c.form_id
                          LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                        WHERE c.uuid = %s;
                    """, (
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        item_id,
                    ))
                    item_data = await cur.fetchone()
                    if item_data is not None:
                        item_data['type'] = 'REPORT'

            if item_type == "USER":
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT u.name, u.role, o.name AS org_name
                        FROM %s.users AS u
                          LEFT JOIN %s.organizations AS o ON o.uuid = u.org_id
                        WHERE u.id = %s;
                    """, (
                        AsIs(user.get("tki")),
                        AsIs(user.get("tki")),
                        item_id,
                    ))
                    item_data = await cur.fetchone()
                    if item_data is not None:
                        item_data['type'] = 'USER'

        item['meta'] = item_data


    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total FROM %s.activity_feed;
        """, (
            AsIs(user.get("tki")),
        ))
        total = await cur.fetchone()
        total = total.get('total')

    return dict(
        activity=results,
        count=total
    )



def create(content, type=None, icon=None, location_id=None, user_id=None, lab_id=None, user=None):
    """Create an activity feed item

    Args:
        content: The content for the activity
        type: The type of activity item
        icon: An icon to use with the activity item
        location_id: An optional location id for the activity item
        user_id: An optional if of user that is the target of the feed item
        lab_id: An optional lab id which is the target fo the feed item
        user: The calling user

    Returns:
        The result of the newly created activity feed item
    """

    if type is None:
        type = "GENERAL"

    if icon is None:
        icon = "fa-bullhorn"

    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            INSERT INTO %s.activity (content, created, icon, event_type, group_id, user_id, lab_id, location_id)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
        """, (
            AsIs(user.get("tki")),
            json.dumps(content),
            datetime.datetime.now(),
            icon,
            type,
            None,
            user_id,
            lab_id,
            location_id,
        ))
