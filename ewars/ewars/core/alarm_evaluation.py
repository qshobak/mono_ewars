import datetime
import json
import uuid

from ewars.utils.six import string_types

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.utils import date_utils
from ewars.analysis import immediate, groups
from ewars.analysis import comparators
from ewars.core.alerts import add_alert_event
from ewars.core import notifications
from ewars.constants import CONSTANTS

from ewars.models import Alert

import tornado.gen

ALERT_HTML = """
<p>A <strong>{{data.form_name}}</strong> has triggered the following alert:</p>
<p>
    <strong>Name: </strong> {{data.alarm_name}}<br/>
    <strong>Location: </strong> {{data.alert_location}}<br />
    <strong>Date: </strong> {{data.trigger_period}}<br />
    <strong>EID: </strong> {{data.eid}}
</p>
<p>You can view more information and take actions on the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a></p>
"""

ALERT_PLAIN = """
A {{data.form_name}} has triggered the following alert:

Name: {{data.alarm_name}}
Location: {{data.alert_location}}
Date: {{data.alert_date}}
EID: {{data.eid}}

You can view more information and take actions on the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}

"""

ALERT_HTML_USER = """
<p>A <strong>{{data.form_name}}</strong> that you recently submitted has triggered the following alert:</p>
<p>
    <strong>Name: </strong> {{data.alarm_name}}<br/>
    <strong>Location: </strong> {{data.alert_location}}<br />
    <strong>Date: </strong> {{data.alert_date}}<br />
    <strong>EID: </strong> {{data.eid}}
    
</p>
<p>You can view more information and take actions on the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a></p>
"""

ALERT_PLAIN_USER = """
A {{data.form_name}} that you recently submitted has triggered the following alert:

Name: {{data.alarm_name}}
Location: {{data.alert_location}}
Date: {{data.alert_date}}
EID: {{data.eid}}

You can view more information and take actions on the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""


async def _evaluate_submitted_report(report, user=None):
    """ Evaluate a report against alarms

    Args:
        report: The report to evaluate
        form: The form associated with the report
        user: The reports form
        token: The instance token

    Returns:

    """
    if report.get('status') == CONSTANTS.SUBMITTED:
        alarms = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid
                FROM %s.alarms_V2
                WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
            ))
            alarms = cur.fetchall()

        for alarm in alarms:
            try:
                await evaluate_alarm(
                    alarm.get("uuid"),
                    report.get("uuid"),
                    token=user.get("tki")
                )
            except Exception as e:
                print(str(e))
                pass


async def evaluate_alarm(alarm_uuid, report_uuid, token=None):
    """ Evaluate a report against an alarm

    Evalute a specific report to see if triggers any of the alarms
    configured for the account.

    Args:
        alarm_uuid:
        report_uuid:
        instance:

    Returns:

    """
    report = None
    alarm = None
    form = None
    form_logic = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alarms_V2 WHERE uuid = %s
        """, (
            AsIs(token),
            alarm_uuid,
        ))
        alarm = await cur.fetchone()

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.*, l.lineage, l.site_type_id
              FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
              WHERE c.uuid = %s;
        """, (
            AsIs(token),
            AsIs(token),
            report_uuid,
        ))
        report = await cur.fetchone()

        if report is not None:
            await cur.execute("""
                SELECT id, name, time_interval, version_id
                FROM %s.forms
                WHERE id = %s
            """, (
                AsIs(token),
                report.get("form_id"),
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT etl FROM %s.form_versions
                WHERE uuid = %s
            """, (
                AsIs(token),
                form.get("version_id"),
            ))
            form_logic = await cur.fetchone()

    if report is None:
        return None

    # Does this report have an effect on the alarm?
    has_effect = False

    # Need to filter to figure out if this report should actually be run for the given
    # Need to know if this report affects either the source indicator for the alarm,
    # or the comparison/comparator indication for the alarm

    # Indicators which the report outputs
    output_indicators = []

    etl = form_logic.get("etl")
    if etl is None:
        etl = dict()
    output_indicators = list(set(etl.keys()))

    # Need to add in system indicators to the output_indicators array
    output_indicators.append(dict(
        uuid='e51f4ca7-1678-44e3-b5cb-64f84555696f',
        form_id=form.get("id")
    ))

    alarm_indicators = []

    # Primary alarm indicator
    ds_indicator = alarm.get("ds_indicator", None)
    ds_ind_uuid = alarm.get("ds_indicator", None)
    if "uuid" in ds_indicator:
        ds_indicator = json.loads(ds_indicator)
        alarm_indicators.append(ds_indicator)
    else:
        alarm_indicators.append(ds_ind_uuid)

    crit_indicator = None
    crit_indicator_uuid = None
    if alarm.get("crit_source", None) == "DATA":
        crit_indicator = alarm.get("crit_indicator", None)
        if "uuid" in crit_indicator:
            crit_indicator = json.loads(crit_indicator)
            crit_indicator_uuid = crit_indicator.get("uuid")

    for a_ind in alarm_indicators:
        for o_ind in output_indicators:
            if isinstance(a_ind, (string_types,)) and isinstance(o_ind, (string_types,)):
                if o_ind == a_ind:
                    has_effect = True
            elif isinstance(a_ind, (dict,)) and isinstance(o_ind, (dict,)):
                if a_ind.get("uuid", None) == o_ind.get("uuid", None):
                    if a_ind.get("uuid", None) is not None and o_ind.get("uuid", None) is not None:
                        # Check that they have matching form id so we don't allow other forms to trigger alerts
                        if a_ind.get("form_id", "NO_A") == o_ind.get("form_id", "NO_B"):
                            has_effect = True

    if has_effect == False:
        return None

    start_date = None
    end_date = None
    locations = []

    # Figure out the date range for querying for source data
    if alarm.get("ds_interval_type") == "TRIGGER_PERIOD" or alarm.get("date_spec") == "TRIGGER":
        end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get("ds_interval"))
        start_date = date_utils.get_start_of_interval(end_date, alarm.get("ds_interval"))
    elif alarm.get("ds_interval_type") == "RANGE":
        end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get('ds_interval'))
        # Get the delta to apply to the end_date
        interval_delta = date_utils.get_delta(alarm.get("ds_sd_intervals"), alarm.get("ds_interval"))
        start_date = end_date - interval_delta
        # Get the start date (start of the week) for the start date interval
        start_date = date_utils.get_start_of_interval(start_date, alarm.get("ds_interval"))

    ds_agg_lower = alarm.get("ds_agg_lower", False)
    sti = None
    loc_id = report.get("location_id")
    loc_groups = None

    # Check if the location is restricted for raising this alert
    if alarm.get("restrict_loc", False) is True:
        if alarm.get("loc_spec") == "SPECIFIC":
            loc_id = alarm.get("loc_id", None)
            if loc_id not in report.get("lineage", []):
                return None
        elif alarm.get("loc_spec") == "GROUPS":
            loc_groups = alarm.get("loc_groups")
            is_ok = False

            loc_groups_full = await groups.get_grouped_locations(loc_groups, user=dict(
                tki=token
            ))

            for loc in loc_groups_full:
                if loc.get("uuid") in report.get("lineage"):
                    is_ok = True

            if is_ok != True:
                return None

    # Check if this is restricted to a specific location type
    if alarm.get('sti_restrict', False) is True:
        sti = alarm.get("sti")
        if report.get("site_type_id") != int(sti):
            return None

    # get the data
    ds_location = report.get("location_id", None)

    constrain_report = alarm.get('monitor_type', 'AGGREGATE') == 'RECORD'
    if constrain_report:
        constrain_report = report_uuid

    ds_result = None
    if alarm.get("ds_type") == "INDICATOR":
        results = await immediate.run(
            target_interval=alarm.get("ds_interval"),
            target_location=str(loc_id),
            indicator=ds_indicator,
            options=dict(
                start_date=start_date,
                end_date=end_date,
                constrain_report=constrain_report,
                # constrain=agg_lower,
                roll_up=ds_agg_lower,
                reduction=alarm.get("ds_aggregate", "SUM")
            ),
            user=dict(
                tki=token,
                group_id=1
            )
        )
        ds_result = results.get("data", None)

    if ds_result is not None:
        ds_result = float(ds_result)

    # Apply the modifier if one has been specified
    if alarm.get('ds_modifier') is not None:
        ds_result = ds_result * float(alarm.get("ds_modifier", 1))

    # Set up the value to compare to
    comparator = alarm.get("crit_comparator")
    modifier = alarm.get("crit_modifier", 1)
    floor = alarm.get('crit_floor', None)

    result = None
    if alarm.get("crit_source") == "VALUE":
        # Create a lambda function for the comparator
        crit_value = alarm.get("crit_value", 0)
        a = None
        if comparator == "GT":
            a = lambda x: True if x > crit_value else False
        elif comparator == "GTE":
            a = lambda x: True if x >= crit_value else False
        elif comparator == "EQ":
            a = lambda x: True if x == crit_value else False
        elif alarm.get("comparator") == "LT":
            a = lambda x: True if x < crit_value else False
        elif alarm.get("comparator") == "LTE":
            a = lambda x: True if x <= crit_value else False
        elif alarm.get("comparator") == "NEQ":
            a = lambda x: True if x != crit_value else False

        # Iterate through the locations and apply the lambda to the values
        # to figure out if the value should trigger the alarm
        result = a(ds_result)

    elif alarm.get("crit_source") == "DATA":
        comparator_data = await _get_alarm_comparator(alarm,
                                                      report,
                                                      report.get("location_id"),
                                                      user=dict(tki=token))

        if comparator_data is not None:
            # Iterate through the location results and do the comparison
            crit_result = comparator_data

            if alarm.get("crit_modifier", None) is not None:
                crit_result = crit_result * float(alarm.get("crit_modifier"))

                if alarm.get('crit_floor', None) is not None:
                    if crit_result < float(alarm.get('crit_floor')):
                        crit_result = float(alarm.get("crit_floor"))

                if ds_result is None:
                    result = False
                else:
                    result = comparators.compare(comparator, ds_result, crit_result)
        else:
            result = False

    # Set up a dummy user
    account = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id FROM _iw.accounts WHERE tki = %s;
        """, (
            token,
        ))
        account = await cur.fetchone()

    dummy_user = dict(
        tki=token,
        aid=account.get("id")
    )

    if result is True:
        # Raise the alert
        await _raise_alert(
            alarm,
            report.get("location_id"),
            report,
            end_date,
            form,
            user=dummy_user
        )

    return True


async def _raise_alert(alarm, location, report, alert_date, form, user=None):
    """ Raise an alert (or update an existing alert) for the specific alarm and location
    
    Args:
        alarm: THe alarm to raise for
        location: THe location to raise for
        report: The triggering report
        alert_date: The date of the alert
        form: The form that triggered the alert
        user: The user who triggered the alert
        
    Returns:

    """
    # First check if there's already an alert for this location and alarm
    alert = None

    if alarm.get('monitor_type', 'AGGREGATE') == 'AGGREGATE':
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alerts
                WHERE location_id = %s
                    AND alarm_id = %s
                    AND trigger_end = %s;
            """, (
                AsIs(user.get("tki")),
                location,
                alarm.get("uuid"),
                alert_date,
            ))
            alert = await cur.fetchone()

    if alert is None:
        # If there's no alert or the alert is inactive
        new_uuid = str(uuid.uuid4())
        tmp_eid = new_uuid.split("-")[-1]
        eid = "-".join([tmp_eid[i:i + 4] for i in range(0, len(tmp_eid), 4)])
        eid = "0A-%s" % (eid,)

        if alarm.get("eid_prefix", None) not in ("", None):
            eid = "%s-%s" % (
                alarm.get("eid_prefix"),
                eid,
            )

        eid = eid.upper()

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alerts (uuid, alarm_id, created, state, indicator_id, location_id, trigger_start, trigger_end, source, triggering_report_id, indicator_definition, stage, stage_state, eid)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                new_uuid,
                alarm.get("uuid"),
                datetime.datetime.now(),
                'OPEN',
                alarm.get("indicator_id", None),
                location,
                alert_date,
                alert_date,
                "SYSTEM",
                report.get("uuid"),
                json.dumps(alarm.get("indicator_definition", "{}")),
                'VERIFICATION',
                'PENDING',
                eid,
            ))
            alert = await cur.fetchone()

        await add_alert_event(alert.get("uuid"), "TRIGGERED", "fa-exclamation-circle", user=user)
        await _raise_primary_actions(alarm, alert, report, form, user=user)
    else:
        # An active alert already exists for this context,
        # just send updates if notify email or sms is enabled
        pass


async def _raise_primary_actions(alarm, alert, report, form, user=None):
    """
    Raise primary actions, these are used when a new alert has been created
    :param alarm: {dict} The alarm which raised the alert
    :param alert: {dict} The newly created alert
    :param report: {report} The report which was submitted that triggered the alert
    :param user: {dict} Dummy user used for querying
    :return:
    """
    submitter = None
    alert_location = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM _iw.users
            WHERE id = %s
        """, (
            report.get("created_by"),
        ))
        submitter = await cur.fetchone()

        await cur.execute("""
            SELECT uuid, name, lineage
            FROM %s.locations
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            alert.get("location_id"),
        ))
        alert_location = await cur.fetchone()

        await cur.execute("""
                    SELECT u.id,
                        u.name,
                        u.email,
                        u.role,
                        u.profile,
                        o.name AS organization_name,
                        l.name AS location_name,
                        lt.name AS location_type
                    FROM %s.users AS u
                        LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                        LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                        LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                        LEFT JOIN _iw.laboratories AS lab ON lab.uuid = u.lab_id
                        LEFT JOIN %s.assignments AS ass ON ass.user_id = u.id
                    WHERE u.status = 'ACTIVE'
                        AND 
                          (
                            (u.role = 'REGIONAL_ADMIN' AND u.location_id IN %s) 
                            OR (u.role = 'ACCOUNT_ADMIN') 
                            OR (u.role = 'USER' AND ass.location_id = %s AND ass.status = 'ACTIVE' AND ass.form_id = %s AND ass.user_id != %s)
                            );
                """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            tuple(alert_location.get("lineage")),
            alert.get("location_id"),
            report.get("form_id"),
            submitter.get("id"),
        ))
        notifiers = await cur.fetchall()

    concrete = dict((x.get("id"), x) for x in notifiers)
    notifiers = list(concrete.values())

    # Add regional admins to the list of admins
    # Map in users with assignments at this location

    for admin in notifiers:
        send_al_not(
            admin,
            "[EWARS] New %s alert" % (alarm.get("name", "Unnamed Alarm")),
            ALERT_HTML,
            ALERT_PLAIN,
            dict(
                eid=alert.get("eid", None),
                form_name=form['name'].get("en", None),
                submitter=submitter.get("name"),
                alarm_name=alarm.get("name", "Unnamed Alarm"),
                alert_uuid=alert.get("uuid"),
                alert_location=alert_location['name'].get("en"),
                trigger_period=date_utils.display(alert.get("trigger_end"), alarm.get("date_spec_interval"))
            ),
            user=user,
            subject="[EWARS] New %s alert" % (alarm.get("name", "Unnamed Alarm"))
        )

    send_al_not(
        submitter,
        "[EWARS] New %s" % (alarm.get('name', 'Unnamed Alarm')),
        ALERT_HTML_USER,
        ALERT_PLAIN_USER,
        dict(
            form_name=form['name'].get("en", None),
            eid=alert.get("eid", None),
            submitter=submitter.get("name"),
            alarm_name=alarm.get("name", "Unnamed Alarm"),
            alert_uuid=alert.get("uuid"),
            alert_location=alert_location['name'].get("en"),
            trigger_period=date_utils.display(alert.get("trigger_end"), alarm.get("date_spec_interval")),
            report_date=date_utils.display(report.get("data_date"), form.get("time_interval"))
        ),
        account=user.get('account_name', None),
        user=user

    )


@tornado.gen.coroutine
def send_al_not(recip, subject, html, plain, data, account=None, user=None):
    notifications.send(
        "ALERT_TRIGGERED",
        recip,
        subject,
        html,
        plain,
        data,
        account=account,
        user=user
    )


async def _raise_secondary_actions(alarm, alert, report, user=None):
    """
    Raise seconday actions, these are used when an existing alert is found
    Checks to see if the newly created report had an impact on the existing alert, for
    instance increasing a previously broken threshold more
    :param alarm:
    :param alert:
    :param report:
    :param user:
    :return:
    """
    pass


async def _get_alarm_comparator(alarm, report, location, user=None):
    """
    Retrieve a data based alarm query comparator value
    :param alarm: {dict} The alarm being evaluated
    :param user: {dict} The report which triggered the evaluation
    :param locations: {list} The list of locations which are being evaluated
    :param user: {dict} The calling user
    :return: {None|dict} A dict of threshold values for each location
    --or-- None if the alarm should not evaluate at this time
    """
    reduction = alarm.get("crit_reduction", "SUM")

    start_date = report.get("data_date")
    end_date = report.get("data_date")

    if alarm.get("crit_sd_spec") == "SPECIFIC":
        start_date = date_utils.parse_date(alarm.get("start_date"))
    elif alarm.get("crit_sd_spec") == "INTERVALS_BACK":
        start_date = start_date - date_utils.get_delta(
            alarm.get("crit_sd_intervals"),
            alarm.get("ds_interval")
        )

    if alarm.get("crit_ed_spec") == "SPECIFIC":
        end_date = date_utils.parse_date(alarm.get("end_date"))
    elif alarm.get("crit_ed_spec") == "INTERVALS_BACK":
        end_date = end_date - date_utils.get_delta(
            alarm.get("crit_ed_intervals"),
            alarm.get("ds_interval")
        )

    indicator = alarm.get("crit_indicator", None)
    if indicator is not None:
        if "uuid" in indicator:
            indicator = json.loads(indicator)

    if indicator is None:
        return None

    data = await immediate.run(
        target_interval=alarm.get("ds_interval"),
        target_location=location,
        indicator=indicator,
        options=dict(
            start_date=start_date,
            end_date=end_date,
            roll_up=alarm.get("ds_agg_lower", False)
        ),
        user=user
    )

    expected_intervals = date_utils.get_date_range(start_date, end_date, alarm.get("ds_interval"))
    if len(data.get("data")) < len(expected_intervals):
        # There's not enough data to do the calculation, return None
        return None

    agg_result = 0

    data = [[x[0], x[1]] for x in data.get("data") if x[1] is not None]

    if len(data) > 0:
        if reduction == "SUM":
            agg_result = sum([x[1] for x in data])
        elif reduction == "AVG":
            agg_result = sum([x[1] for x in data]) / len(data)
        elif reduction == "MIN":
            min = data[0][1]
            for x in data:
                if x[1] > min:
                    min = x[1]
            agg_result = min
        elif reduction == "MAX":
            max = data[0][1]
            for x in data:
                if x[1] > max:
                    max = x[1]
            agg_result = max
        elif reduction == "COUNT":
            agg_result = len(data)
        elif reduction == "DISTINCT":
            items = [x[1] for x in data]
            agg_result = len(set(items))

    return agg_result
