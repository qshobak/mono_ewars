import json

from confluent_kafka import Producer, KafkaError

from ewars.core.serializers import JSONEncoder


async def produce(event_type=None, data=None, key=None, tki=None):
    p = Producer({
        "bootstrap.servers": "localhost:9092"
    })

    topic = "%s.%s" % (
        tki,
        event_type,
    )

    p.publish(
        topic=topic,
        data=json.dumps(data, cls=JSONEncoder).encode("ascii"),
        key=str(key).encode('ascii')
    )

    p.flush()

    p.close()

    return True


async def batch_produce(events=[], tki=None):
    p = Producer({
        "bootstrap.servers": "localhost:9092"
    })

    for item in events:
        topic = "%s.%s" % (
            tki,
            item.event_type,
        )

        p.publish(
            topic=topic,
            data=json.dumps(item.data, cls=JSONEncoder).encode('ascii'),
            key=str(item.key).encode('ascii')
        )

    p.flush()
    p.close()

    return True
