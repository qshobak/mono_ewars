from ewars.core.export_indicators import export_indicator_data
from ewars.core.export_alerts import export_alerts
from ewars.core.export_forms import export_forms
from ewars.core.export_locations import export_locations


async def export_data(data_type, definition, user=None):
    """ Handle different types of data export.

    Args:
        data_type: The type fo data to export
        definition: The definition of the periodicity and location of the data
        user: The calling user

    Returns:

    """
    result = None

    if data_type == "INDICATOR":
        result = await export_indicator_data(definition, user=user)
    elif data_type == "FORM":
        result = export_forms(
            definition.get("form_id"),
            definition.get("location_id"),
            definition.get("start_date"),
            definition.get("end_date"),
            user=user
        )
    elif data_type == "LOCATIONS":
        result = await export_locations(
            definition.get("location_id"),
            definition.get("site_type_id", None),
            definition.get("status", None),
            user=user
        )
    elif data_type == "ALERTS":
        result = await export_alerts(
            definition.get("alarm_id", None),
            definition.get('location_id'),
            definition.get("start_date"),
            definition.get('end_date'),
            user=user
        )

    return result
