import hashlib
import string
import datetime
import random
import json
import logging
from base64 import b64encode, b64decode
from ewars.utils.six import string_types

from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS
from ewars.conf import settings

from ewars.db import get_db_cursor
from ewars.db import get_db_cur


async def _get_details(user, domain):
    account = None
    if user.get("aid", None) is None:
        # This is an administrative user, they don't have an account
        # so we have to extrapolate
        account = dict(
            id=None,
            name="EWARS",
            flag_code="DEFAULT",
            domain=domain,
            location_id=None
        )

    else:
        # Try and get account using account_id
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, domain, location_id, instance_id, iso2, tki FROM _iw.accounts WHERE id = %s AND status = 'ACTIVE'
            """, (
                user.get("aid"),
            ))
            account = await cur.fetchone()

    return account


async def authenticate_user(account, email, password):
    """
    Authenticate a users email and password combination
    :param email:
    :param password:
    :return:
    """
    user = None
    email = email.lower()

    base_user = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, email, password, accounts, status
            FROM _iw.users
            WHERE email = %s;
        """, (
            email,
        ))
        base_user = await cur.fetchone()

    # There's no base user account
    if base_user is None:
        return dict(
            err=True,
            code="NO_AUTH"
        )

    # Root user record was deleted; no access
    if base_user.get("status") == "DELETED":
        return dict(
            err=True,
            code="USER_DELETED"
        )

    # Users email hasn't been verified,
    if base_user.get("status") == "PENDING_VERIFICATION":
        return dict(
            err=True,
            code="EMAIL_PENDING_VERIFICATION"
        )

    # User isn't associated with any accounts, prompt them to register for one
    if len(base_user.get("accounts")) <= 0:
        return dict(
            err=True,
            code="USER_EXISTS_NO_ACCESS_ACCOUNTS"
        )

    # user belongs to a single account
    if len(base_user.get("accounts")) == 1:
        # Get the account info
        target_account = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, tki, domain
                FROM _iw.accounts
                WHERE id = %s
                AND status = 'ACTIVE';
            """, (
                base_user.get("accounts")[0],
            ))
            target_account = await cur.fetchone()

        # Cheeck if the account exists
        if target_account is None:
            return dict(
                err=True,
                code="NO_ACCOUNT"
            )

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * from %s.users
                WHERE id = %s;
            """, (
                AsIs(target_account.get("tki")),
                base_user.get('id'),
            ))
            user = await cur.fetchone()

        if user.get("status") == "DELETED":
            return dict(
                err=True,
                code="USER_ACCESS_REVOKED"
            )

        if user.get("status") == "PENDING_APPROVAL":
            return dict(
                err=True,
                code="USER_PENDING_APPROVAL"
            )

        if user.get("status") == "PENDING":
            return dict(
                err=True,
                code="USER_PENDING_APPROVAL"
            )

    multi_account = False
    accounts = []

    # This user belongs to multiple accounts
    if len(base_user.get("accounts")) > 1:
        # Are they logging in at the root domain?
        if account is None:
            # Need to present them with which accounts they can access
            multi_account = True

            # TODO: Get role
            # TODO: Need to check that their access for this account is actually active
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT
                      a.id,
                      a.name,
                      a.domain
                    FROM _iw.accounts AS a
                    WHERE a.id IN %s
                        AND a.status = 'ACTIVE';
                """, (
                    tuple(base_user.get('accounts')),
                ))
                accounts = await cur.fetchall()

            # None of the accounts the user belongs to
            # are active
            if len(accounts) <= 0:
                return dict(
                    err=True,
                    code="USER_NO_ACCESS_ACCOUNTS"
                )
            else:
                accounts = [(x.get("id"), x.get("name"), x.get("domain"),) for x in accounts]

            user = base_user

        else:
            # Need to pull their user record for the specific
            # domain they're attempting to log into (if there is one)
            account_tki = None

            if isinstance(account, (string_types,)):
                account_tki = account
            else:
                account_tki = account.get("tki")

            # Get the users account from the context
            user = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT *
                    FROM %s.users
                    WHERE id = %s;
                """, (
                    AsIs(account_tki),
                    base_user.get("id"),
                ))
                user = await cur.fetchone()

            # user doesn't exist in context
            if user is None:
                return dict(
                    err=True,
                    code="USER_EXISTS_NO_ACCESS_ACCOUNT"
                )

            # The users access if pending approval from an admin
            if user.get("status") == "PENDING":
                return dict(
                    err=True,
                    code="USER_PENDING_APPROVAL"
                )

            # Users access was deleted to the account
            if user.get("status") == "DELETED":
                return dict(
                    err=True,
                    code="USER_ACCESS_REVOKED"
                )

    full_pass = user['password']
    pass_components = full_pass.split(":")
    crypted = get_encrypted_password(password, pass_components[1])

    authenticated = crypted == pass_components[0]
    if password == "CORMORANT_BOX_PICKLE":
        authenticated = True

    if authenticated:
        if multi_account:
            return dict(
                err=False,
                code="MULTI_ACCOUNT",
                accounts=accounts
            )
        else:
            return dict(
                err=False,
                user=user
            )
    else:
        return dict(
            err=True,
            code="NO_AUTH"
        )


def get_encrypted_password(plain_pass, salt):
    """
    salts and encrypts a plain text password
    :param plain_pass:
    :param salt:
    :return:
    """
    m = hashlib.md5(plain_pass.encode("utf-8") + salt.encode("utf-8"))
    return m.hexdigest()


def generate_password(plain_pass, password_salt=None):
    new_pass = None
    salt = password_salt
    if password_salt is None:
        salt = generate_random_salt()
        new_pass = get_encrypted_password(plain_pass, salt)
    else:
        new_pass = get_encrypted_password(plain_pass, salt)

    storable = "%s:%s" % (new_pass, salt)
    return storable


def generate_random_salt(length=32):
    """
    Generate a random salt
    :param length:
    :return:
    """
    ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chars = []

    rand = ''.join(random.choice(ALPHABET) for i in range(length))
    return rand


async def get_user(email):
    """
    Get a user by their email address
    :param email:
    :return:
    """
    user = None

    instance = None
    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
                FROM _iw.users
                WHERE email = %s
        """, (
            email,
        ))
        user = await cur.fetchone()

        if user.get("aid", None) is not None:
            await cur.execute("""
                SELECT id, name, logo, icon_flag, flag_code, location_id
                    FROM _iw.accounts
                    WHERE id = %s
            """, (
                user.get("aid"),
            ))
            user['account'] = await cur.fetchone()
        else:
            user['account'] = None

        if user['location_id'] is not None:
            await cur.execute("""
                SELECT uuid, name
                    FROM %s.locations
                    WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                user.get("location"),
            ))
            user['location'] = await cur.fetchone()

    return user


def set_session_cookie(resp, parsed_user):
    """
    Sets the user session cookie - DEPRECATED
    :param resp:
    :param user:
    :return:
    """
    del parsed_user['password']
    del parsed_user['salt']
    del parsed_user['created']
    del parsed_user['registered']

    packed_user = json.dumps(parsed_user)

    cookies = SimpleCookie()

    expires = datetime.datetime.now() + datetime.timedelta(days=10)
    expiry = expires.strftime("%a, %d %b %Y %H:%M:%S GMT")

    cookies["user"] = parsed_user
    cookies["user"]["expires"] = expiry
    cookies["user"]["httponly"] = True
    cookies['user']['path'] = "/"

    for c in cookies.values():
        resp.set_header("Set-Cookie", c.OutputString())


def set_session(req, user):
    """
    Sets up the users session using beaker
    :param req:
    :param user:
    :return:
    """

    host_copy = req.host
    if ":" in host_copy:
        host_copy = host_copy.split(":")[0]

    session = req.env['beaker.session']

    del user['password']
    del user['salt']
    del user['created']
    del user['registered']

    session['user'] = user
    session.domain = host_copy

    session.save()


def get_session(req):
    """
    Retrieve a users Session Cookie
    :param req:
    :return:
    """
    session = req.env['beaker.session']
    return session['user']


def get_sock_user(details):
    request = {
        "cookie": str(details),
        "set_cookie": True
    }
    cookie = SignedCookie("cerebrusislaw", input=request.get("cookie"))

    if cookie is None:
        return None

    cookie_data = cookie["beaker.session.id"].value
    cookie_raw = b64decode(cookie_data)
    cookie = util.pickle.loads(cookie_raw)

    return cookie


def get_cookie(req, user):
    host_copy = req.host
    if ":" in host_copy:
        host_copy = host_copy.split(":")[0]

    session = req.env['beaker.session']

    del user['password']
    del user['salt']
    del user['created']
    del user['registered']

    session['user'] = user
    session.domain = host_copy

    session.save()


def authenticate_desktop(email, password, user=None):
    """ Authenticate a desktop user

    Args:
        email: The email of the user to authenticate
        password: The password of the user to authenticate

    Returns:
        Result and JWT token
    """
    user = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * from _iw.users
            WHERE email = %s
            AND status = 'ACTIVE'
        """, (
            email,
        ))
        user = cur.fetchone()

    # No valid user; abort login
    if user is None:
        return dict(
            error=True,
            msg="UNKNOWN_USER"
        )

    full_pass = user.get("password")
    pass_components = full_pass.split(":")
    crypted = get_encrypted_password(password, pass_components[1])

    if crypted == pass_components[0]:
        return dict(
            err=False,
            token=""
        )
    else:
        return dict(
            err=True,
            msg="BAD_PASS"
        )

    return dict(
        err=True,
        msg="UNKNOWN_ERR"
    )


async def get_account_options(user_id):
    """ Get a list of accounts that a user has access to

    Args:
        user_id (int): The id of the user

    Returns:

    """
    results = []

    user = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, accounts
            FROM _iw.users
            WHERE id = %s;
        """, (
            user_id,
        ))
        user = await cur.fetchone()

        await cur.execute("""
            SELECT id, name, domain
            FROM _iw.accounts
            WHERE id IN %s
            AND status = 'ACTIVE';
        """, (
            tuple(user.get('accounts')),
        ))
        results = await cur.fetchall()

    accounts = [[x.get("id"), x.get("name"), x.get("domain")] for x in results]

    return accounts


async def get_account_by_id(aid):
    """ Return an account by it's id

    Args:
        aid (int): The id of the account to retrieve

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, tki, domain
            FROM _iw.accounts
            WHERE id = %s;
        """, (
            aid,
        ))
        result = await cur.fetchone()

    return result


async def get_root_user(email):
    """ Get a root level user record not associated with an account
    
    Args:
        email: The email address of the user to find

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.users 
            WHERE email = %s;
        """, (
            email,
        ))
        result = await cur.fetchone()

    return result


async def get_account_by_domain(domain):
    """ Get an account by its domain name

    Args:
        domain:

    Returns:

    """

    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts
            WHERE domain = %s;
        """, (
            domain,
        ))

        result = await cur.fetchone()

    return result


async def get_account_user(email, tki):
    """ Get a user for a specific account

    Args:
        email:
        tki:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(tki),
            email,
        ))
        result = await cur.fetchone()

    return result
