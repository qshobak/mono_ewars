from ewars.common.constants import CONSTANTS
from ewars.common.db.psql import get_db_cursor

from psycopg2.extensions import AsIs

def can_user_delete_report(report_location_id, user=None):

    if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
        return True

    if user.get("role") == CONSTANTS.SUPER_ADMIN:
        return True

    if user.get("role") == CONSTANTS.USER:
        return False

    if user.get("role") == CONSTANTS.REGIONAL_ADMIN:
        location = None
        with get_db_cursor() as cur:
            cur.execute("""
                SELECT lineage FROM %s.locations WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                report_location_id,
            ))
            location = cur.fetchone()

        if user.get("location_id") in location.get("lineage"):
            return True
        else:
            return False



def can_user_amend_report(report_uuid, user=None):
    """ Can the calling user perform an immediate amend against a report
    :param report_uuid: {str} The UUID of the report in question
    :param user: {dict} The calling user
    :return: True|False whether the user can perform an immediate amendment
    """

    if user.get("role") == CONSTANTS.ACCOUNT_ADMIN:
        return True
    elif user.get("role") == CONSTANTS.SUPER_ADMIN:
        return True
    elif user.get("role") == CONSTANTS.USER:
        return False
    elif user.get("role") == CONSTANTS.REGIONAL_ADMIN:
        location = None
        report = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT location_id FROM %s.collections WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                report_uuid,
            ))
            report = cur.fetchone()

            if report is not None:
                cur.execute("""
                    SELECT lineage FROM %s.locations WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    report.get("location_id"),
                ))
                location = cur.fetchone()

        if location is not None:
            if user.get("location_id") in location.get("lineage"):
                return True
            else:
                return False
        else:
            return False

