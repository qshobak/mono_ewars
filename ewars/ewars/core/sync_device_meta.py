import datetime
import json

from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

def sync_update_device(data=None, user=None):
    """
    Update a devices information in the database
    :param data:
    :param user:
    :return:
    """

    manifest = data.get("manifest")
    platform = manifest.get("platform", None)
    device_id = manifest.get("device_id", None)
    phone = manifest.get("phone", None)
    lat = manifest.get("lat", None)
    lng = manifest.get("lng", None)
    version_code = manifest.get("versionCode", None)
    version_name = manifest.get("versionName", None)
    android_version = manifest.get("androidVersion", None)
    device_name = manifest.get("device", None)

    device = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM %s.devices
            WHERE device_id = %s
        """, (
            AsIs(user.get("tki")),
            device_id,
        ))
        device = cur.fetchone()

    if device is not None:
        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                    UPDATE %s.devices
                    SET user_id = %s,
                        last_seen = %s,
                        device_type = %s,
                        device_lat = %s,
                        device_lng = %s,
                        device_number = %s,
                        app_version = %s,
                        app_version_name = %s,
                        android_version = %s,
                        device_name = %s
                    WHERE device_id = %s
                """, (
                AsIs(user.get("tki")),
                user['id'],
                datetime.datetime.now(),
                platform,
                lat,
                lng,
                phone,
                version_code,
                version_name,
                android_version,
                device_name,
                device_id,
            ))
    else:
        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                    INSERT INTO %s.devices (device_type, device_id, device_os_version, device_lat, device_lng, device_number, user_id, last_seen, status, app_version, app_version_name, android_version, device_name)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                AsIs(user.get("tki")),
                platform,
                device_id,
                None,
                lat,
                lng,
                phone,
                user['id'],
                datetime.datetime.now(),
                'ACTIVE',
                version_code,
                version_name,
                android_version,
                device_name,
            ))


def update_device_from_sync(device_id, platform, phone, lat, lng, user=None):
    """
    Update a devices information in the database or enter it in if it's missing
    :param device_id: {str} The IMEI of other device ID of the device
    :param platform: {str} The platform the device runs on (i.e. ANDROID)
    :param phone: {str} The devices phone number
    :param lat: {str} The last known lat of the device
    :param lng: {str} The last known lng of the device
    :param user: {dict} The user attached to the device
    :return:
    """

    device = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM %s.devices
            WHERE device_id = %s
        """, (
            AsIs(user.get("tki")),
            device_id,
        ))
        device = cur.fetchone()

    if device is not None:
        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                UPDATE %s.devices
                SET user_id = %s,
                    last_seen = %s,
                    device_type = %s,
                    device_lat = %s,
                    device_lng = %s,
                    device_number = %s
                WHERE device_id = %s
            """, (
                AsIs(user.get("tki")),
                user['id'],
                datetime.datetime.now(),
                platform,
                lat,
                lng,
                phone,
                device_id,
            ))
    else:
        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                INSERT INTO %s.devices (device_type, device_id, device_os_version, device_lat, device_lng, device_number, user_id, last_seen, status)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                platform,
                device_id,
                None,
                lat,
                lng,
                phone,
                user['id'],
                datetime.datetime.now(),
                'ACTIVE',
            ))
