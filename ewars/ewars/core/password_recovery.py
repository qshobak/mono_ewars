import uuid

from ewars.db import get_db_cur

from ewars.core import notifications
from .authentication import generate_password


async def trigger_password_recovery(email):
    """ Kick off password recovery

    Args:
        email:  THe email of the user requesting password recovery

    Returns:

    """
    user = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, api_token
            FROM _iw.users
            WHERE email = %s
        """, (
            email,
        ))
        user = await cur.fetchone()

    if user is None:
        return dict(
            err=True,
            data=dict(
                email="No account associated with this address"
            )
        )

    new_api_token = str(uuid.uuid4())

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.users
            SET api_token = %s
            WHERE id = %s
        """, (
            new_api_token,
            user.get("id"),
        ))

    await notifications.send(
        "RECOVERY",
        email,
        dict(
            email=email,
            token=new_api_token
        )

    )

    return dict(
        err=False
    )


async def reset_password(verification_code, new_password):
    """ Change users password to new password

    Args:
        email:
        password:

    Returns:

    """

    new_pass = generate_password(new_password)

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.users
            SET password = %s,
             api_token = %s
            WHERE api_token = %s
        """, (
            new_pass,
            str(uuid.uuid4()),
            verification_code,
        ))

    return True


async def check_verification(verification_code):
    """ Check the authentism of a verification code

    Args:
        verification_code:  THe code to verify

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT api_token FROM _iw.users
            WHERE api_token = %s
        """, (
            verification_code,
        ))
        result = await cur.fetchone()

    if result is None:
        return False
    else:
        return True
