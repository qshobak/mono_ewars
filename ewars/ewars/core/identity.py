import json
import hashlib
import datetime
import random
from base64 import b64decode, b64encode

from ewars.core.serializers import JSONEncoder
from ewars.utils.six import string_types
from ewars.constants import CONSTANTS
from ewars.conf import settings

from ewars.db import get_db_cur, get_db_cursor

from psycopg2.extensions import AsIs


async def get_session(token):
    ''' Get a users session from the database
    
    Args:
        token: 

    Returns:

    '''
    result = None

    async with get_db_cur() as cur:
        await cur.execute('''
            SELECT * FROM _iw.sessions WHERE token = %s;
        ''', (
            token,
        ))
        result = await cur.fetchone()

    return result


def get_session_sync(token):
    ''' Get a users session from the database

    Args:
        token:

    Returns:

    '''
    result = None

    with get_db_cursor() as cur:
        cur.execute('''
            SELECT * FROM _iw.sessions WHERE token = %s;
        ''', (
            token,
        ))
        result = cur.fetchone()

    return result


async def get_account_by_host(domain):
    """ Retrieve information about an account by its domain name
    
    Args:
        domain: The domain name of the account to retrieve

    Returns:

    """
    result = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts
            WHERE domain = %s;
        """, (
            domain,
        ))
        result = await cur.fetchone()

    return result


async def persist_session(token, data, instance_token=None):
    """ Persist a session into the database

    Args:
        token: The cookie token for the user session
        data: Any data associated with the session
        instance_token: A token representing the instance the session belongs to

    Returns:
        The persisted data
    """

    # Check if they have any sessions which have expired and remove them

    result = None
    async with get_db_cur() as cur:

        await cur.execute("""
            INSERT INTO _iw.sessions (user_id, socket_ids, token, data)
            VALUES (%s, %s, %s, %s) RETURNING *;
        """, (
            data.get("id"),
            [],
            token,
            json.dumps(data, cls=JSONEncoder),
        ))
        result = await cur.fetchone()

    return result


async def get_token_by_domain(domain):
    """ Get account token by domain

    Args:
        domain:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT tki FROM _iw.accounts
            WHERE domain = %s;
        """, (
            domain,
        ))
        result = await cur.fetchone()

    if result is None:
        return None
    else:
        return result.get("tki", None)


async def delete_session(session_id):
    async with get_db_cur() as cur:
        await cur.execute("""
            DELETE FROM _iw.sessions
            WHERE token = %s;
        """, (
            session_id,
        ))

    return True

def check_email():
    return None