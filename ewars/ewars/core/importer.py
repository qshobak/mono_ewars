import json
import datetime
import csv
import calendar
import asyncio
from enum import Enum

import tornado
from tornado import ioloop, gen

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.utils import form_utils
from ewars.core.serializers.json_encoder import JSONEncoder
from ewars.utils.six import string_types

from psycopg2.extensions import AsIs

csv.register_dialect(
    'ewars_dialect',
    delimiter=',',
    quotechar='"',
    doublequote=True,
    skipinitialspace=True,
    lineterminator='\r\n',
    quoting=csv.QUOTE_MINIMAL
)


class AutoEnum(Enum):
    def __str__(self):
        return str(self.value)


class FieldTypes:
    TEXT = "text"
    TEXTAREA = "textarea"
    NUMBER = "number"
    SELECT = "select"
    HEADER = "header"
    MATRIX = "matrix"
    ROW = "row"
    DATE = "date"


class StaticFields:
    DATA_DATE = "data_date"
    LOCATION_ID = "location_id"


class ImportErrorCodes:
    NO_MATCH = "NO_MATCH"
    MULTI_MATCH = "MULTI_MATCH"


class LocationMatchTypes:
    PCODE = "PCODE"
    UUID = "UUID"
    NAME = "NAME"


async def create_project(file_name, form_id, user=None):
    result = None
    recent = None

    mapping = dict()
    meta_map = dict()

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * 
            FROM %s.import_projects
            WHERE form_id = %s
            ORDER BY created DESC
            LIMIT 1;
        """, (
            AsIs(user.get('tki')),
            form_id,
        ))
        recent = await cur.fetchone()

        # If there is a recent chunk in the same form, load
        # up it's mapping
        if recent is not None:
            mapping = recent.get('mapping', {})
            meta_map = recent.get('meta_map', {})

        await cur.execute("""
            INSERT INTO %s.import_projects
            (name, status, target_type, version_id, form_id, data_set_id, mapping, meta_map, created_by, src_file)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
        """, (
            AsIs(user.get("tki")),
            "New Import Project",
            "COLLECTING_DATA",
            "FORM",
            None,
            form_id,
            None,
            json.dumps(mapping),
            json.dumps(meta_map),
            user.get("id"),
            file_name,
        ))
        result = await cur.fetchone()

    loop = tornado.ioloop.IOLoop.instance()
    loop.spawn_callback(_extrapolate_source, result, user=user)

    return result


def ResultIter(cursor, arraysize=250):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        for result in results:
            yield result


def _get_location_by_name(name, site_type_id=None, user=None):
    """ Get a location by it's name match

    Args:
        name:
        loc_type:
        user:

    Returns:

    """

    result = None
    with get_db_cursor() as cur:
        if site_type_id is None:
            cur.execute("""
                SELECT uuid, name
                FROM %s.locations
                WHERE lower(name->>'en') = %s
                  AND status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                name.lower(),
            ))
            result = cur.fetchall()
        else:
            cur.execute("""
                SELECT uuid, name
                FROM %s.locations
                WHERE lower(name->>'en') = %s
                  AND site_type_id = %s
                  AND status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                name.lower(),
                site_type_id,
            ))
            result = cur.fetchall()

    if len(result) <= 0:
        return ImportErrorCodes.NO_MATCH
    elif len(result) > 1:
        return ImportErrorCodes.MULTI_MATCH
    else:
        return result[0]


def _get_location_by_pcode(pcode, user=None):
    """ Get a location by it's pcode

    Args:
        pcode:
        user:

    Returns:

    """
    result = None

    with get_db_cursor() as cur:
        cur.execute("""
             SELECT uuid, name
             FROM %s.locations
             WHERE pcode = %s;
        """, (
            AsIs(user.get("tki")),
            pcode,
        ))
        result = cur.fetchall()

    if len(result) <= 0:
        return ImportErrorCodes.NO_MATCH
    elif len(result) > 1:
        return ImportErrorCodes.MULTI_MATCH
    else:
        return result[0]


def _get_location_by_uuid(loc_id, user=None):
    """ Get a location by it's UUID

    Args:
        loc_id:
        user:

    Returns:

    """
    result = None

    if loc_id is None:
        return ImportErrorCodes.NO_MATCH

    if loc_id == '':
        return ImportErrorCodes.NO_MATCH

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT uuid, name
            FROM %s.locations
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            loc_id,
        ))
        result = cur.fetchone()

    if result is None:
        return ImportErrorCodes.NO_MATCH
    else:
        return result


def _set_error(errors, r_id, prop, error):
    try:
        errors[r_id].append((prop, error,))
    except KeyError:
        errors[r_id] = [
            (prop, error,)
        ]

    return errors


def _parse_date(date_format, cur_value, interval="DAY", null_plan="NULL", allow_future=False):
    # Process formatting a date
    result = None

    today = datetime.datetime.utcnow()

    if cur_value in (None, "NULL", "null", ""):
        return null_plan

    real_date = None
    if "%W" in date_format:
        # We need to add the week day to this for python to be able to parse it
        new_value = cur_value + " 0"
        n_date_format = date_format + " %w"
        try:
            real_date = datetime.datetime.strptime(new_value, n_date_format)
        except Exception:
            real_date = -1
    else:
        try:
            real_date = datetime.datetime.strptime(cur_value, date_format)
        except Exception:
            real_date = -1

    if real_date == -1:
        return "INVALID_DATE"

    # Check that the date isn't in the future
    if not allow_future:
        if real_date > today:
            return "DATE_FUTURE"
        else:
            result = real_date.strftime("%Y-%m-%d")
    else:
        result = real_date.strftime("%Y-%m-%d")

    # Check if the date is a property valid date based on the reporting interval of the form
    if result not in ("INVALID_DATE", "DATE_FUTURE",):
        if not _validate_date(result, interval=interval):
            return "INVALID_DATE"

    return result


def _validate_select(values, field, null_plan=None):
    field_options = field.get("options", [])
    is_multi = field.get("multiple", False)

    opt_values = [x[0] for x in field_options]

    if values in ("", None, "NULL", "null"):
        return null_plan

    if is_multi:
        if "," in values:
            list_values = values.split(",")
            for value in list_values:
                if value not in opt_values:
                    return "INVALID_SELECT"

            return list_values
        else:
            if values not in opt_values:
                return "INVALID_SELECT"
            else:
                return [values]
    else:
        if values not in opt_values:
            return "INVALID_SELECT"
        else:
            return values

    return null_plan


def _has_duplicate(form_id, data_date, location_id, user=None):
    """ Check if a duplicate report exists

    Args:
        form_id: The Id of
        data_date:
        location_id:
        user:

    Returns:

    """
    result = None

    if location_id is None:
        return False

    if location_id == '':
        return False

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT uuid
            FROM %s.collections
            WHERE form_id = %s
              AND data_date = %s
              AND location_id = %s
              AND status != 'DELETED';
        """, (
            AsIs(user.get("tki")),
            form_id,
            data_date,
            location_id,
        ))
        result = cur.fetchone()

    return result is not None


def _validate_date(date_string, interval="DAY"):
    result = True

    if date_string is None:
        result = False
    else:
        if interval == "DAY":
            if date_string is None:
                result = False
        elif interval == "WEEK":
            # Need to check that it's  valid end of week ISO date
            real_date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
            weekday = real_date.isoweekday()
            if weekday != 7:
                result = False
        elif interval == "MONTH":
            year, month, day = date_string.split("-")
            m_range = calendar.monthrange(int(year), int(month))
            if int(day) != int(m_range[1]):
                result = False

        elif interval == "YEAR":
            parts = date_string.split("-")
            if int(parts[1]) != 12 and int(parts[2]) != 31:
                result = False

    return result


async def validate_project(project_id, user=None):
    """ Wraps the real validate function and initiates it in
        a background process. This allows the UI to return
        and for the user to walk away from the import

    """

    cur_loop = asyncio.get_event_loop()
    cur_loop.create_task(
        _validate_project(
            project_id,
            user=user
        )
    )

    return True


async def _validate_project(project_id, user=None):
    """ Validates the rows in an import project

    Args:
        project_id: The UUID of the project to run
        user: The calling user

    Returns:

    """
    proj = None
    errors = dict()
    form = None

    async with get_db_cur() as cur:
        # Set the improt projects state to validating
        await cur.execute("""
            UPDATE %s.import_projects
            SET status = 'VALIDATING'
            WHERE uuid = %s;
        """, (
            AsIs(user.get('tki')),
            project_id,
        ))

        await cur.execute("""
            SELECT * FROM %s.import_projects
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            project_id,
        ))
        proj = await cur.fetchone()

        await cur.execute("""
            SELECT f.id, f.name, fv.definition, f.features
            FROM %s.forms AS f
              LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
            WHERE f.id = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            proj.get('form_id'),
        ))
        form = await cur.fetchone()

        # Form features
        features = form.get("features", dict())
        has_interval = "INTERVAL_REPORTING" in features.keys()
        has_location = "LOCATION_REPORTING" in features.keys()
        interval_type = features.get("INTERVAL_REPORTING", {}).get("interval", "DAY")
        location_type = features.get("LOCATION_REPORTING", {}).get("site_type_id", None)

        fields = form_utils.flatten_definition(form.get("definition"))

        mapping = proj.get("mapping", {})
        meta_map = proj.get("meta_map", {})

        if meta_map.get("lid_nonstd", False) in ("true", True):
            # We're overriding the location type that the data is being imported into
            location_type = meta_map.get("lid_match_sti", None)

        lid_key = meta_map.get("lid_key", None)
        lid_match_type = meta_map.get("lid_match_type", None)

        data_date_key = meta_map.get("dd_key")
        data_date_format = meta_map.get("dd_format")
        allow_future = meta_map.get("allow_future", False)
        data_date_format_custom = meta_map.get("dd_custom_format", None)
        today = datetime.datetime.utcnow()

        sd_key = meta_map.get("sub_key", None)
        sd_format = meta_map.get("sub_format", None)

        # Iterate through the reports

        with get_db_cursor(name='IMPORT_DATA') as cur:
            cur.execute("""
            SELECT * FROM %s.import_project_data
            WHERE project_id = %s;
        """, (
                AsIs(user.get("tki")),
                project_id,
            ))

            for report in ResultIter(cur, arraysize=1000):
                status = report.get('status')
                status = "PENDING"

                data = report.get('data', {})

                # Sort out location
                location = None
                lid = None
                loc_match_value = data.get(lid_key, None)[0]
                if lid_match_type == LocationMatchTypes.UUID:
                    location = _get_location_by_uuid(loc_match_value, user=user)
                elif lid_match_type == LocationMatchTypes.PCODE:
                    location = _get_location_by_pcode(loc_match_value, user=user)
                elif lid_match_type == LocationMatchTypes.NAME:
                    location = _get_location_by_name(loc_match_value, site_type_id=location_type, user=user)

                if location == ImportErrorCodes.NO_MATCH:
                    lid = "LOC_NOT_FOUND"
                elif location == ImportErrorCodes.MULTI_MATCH:
                    lid = "MULTIPLE_MATCHES"
                else:
                    lid = location.get("uuid", None)

                # Sort out report date
                data_date_src = data.get(data_date_key, None)[0]
                data_date = _parse_date(
                    data_date_format,
                    data_date_src,
                    interval=interval_type,
                    null_plan="IGNORE_ROW",
                    allow_future=allow_future
                )

                sub_date = data_date

                if sd_key == "TODAY":
                    sub_date = datetime.datetime.utcnow().strftime("%Y-%m-%d")
                elif sd_key == "DATA_DATE":
                    sub_date = data_date
                else:
                    sub_date = data.get(sd_key, None)[0]
                    sub_date = _parse_date(
                        data_date_format,
                        sub_date,
                        interval="DAY",
                        null_plan="IGNORE_ROW",
                        allow_future=allow_future
                    )

                # Sort out other fields
                # We ignore these fields as we deal with them exclusively
                for key, value in mapping.items():
                    cur_value = data.get(key, None)[0]
                    mapped_value = data.get(key, None)[1]
                    target_column = value.get('target', None)
                    null_plan = value.get("null_", "NULL")
                    date_format = value.get("date_format", None)
                    date_format_custom = value.get("custom_date_format", None)

                    if target_column == "IGNORE":
                        data[key][1] = "IGNORE"
                    elif target_column not in fields.keys():
                        data[key][1] = "NO_MAP"
                    else:
                        target_field = fields.get(target_column)
                        field_type = target_field.get("type")
                        field_required = target_field.get("required", False)
                        field_label = target_field.get("label", None)
                        field_options = target_field.get("options", [])
                        field_date_type = target_field.get('date_type', None)
                        is_multi = target_field.get("multiple", False)

                        # Process the field
                        if field_type == FieldTypes.NUMBER:
                            if cur_value in (None, "null", "NULL", ""):
                                if null_plan == "NULL":
                                    data[key][1] = "NULL"
                                elif null_plan == "ZERO":
                                    data[key][1] = 0
                                else:
                                    data[key][1] = None
                            else:
                                if "," in cur_value:
                                    data[key][1] = cur_value.replace(",", "")
                                else:
                                    data[key][1] = cur_value

                        elif field_type == FieldTypes.DATE:
                            data[key][1] = _parse_date(date_format, cur_value, null_plan=null_plan)
                        elif field_type in [FieldTypes.TEXT, FieldTypes.TEXTAREA]:
                            if cur_value in (None, "NULL", "null", ""):
                                data[key][1] = null_plan
                            else:
                                data[key][1] = cur_value
                        elif field_type == FieldTypes.SELECT:
                            data[key][1] = _validate_select(cur_value, target_field, null_plan=null_plan)

                # Check for duplicate report
                if has_interval and has_location:
                    if location not in (None, "NO_MATCH", "MULTI_MATCH", "IGNORE_ROW") and \
                            data_date not in (None, "INVALID_DATE", "DATE_FUTURE", "IGNORE_ROW"):
                        if _has_duplicate(form.get('id'), data_date, location.get("uuid", None), user=user):
                            status = "CONFLICT"

                async with get_db_cur() as sub_cur:
                    await sub_cur.execute("""
                            UPDATE %s.import_project_data
                            SET data = %s,
                              status = %s,
                              location_id = %s,
                              data_date = %s
                            WHERE uuid = %s;
                        """, (
                        AsIs(user.get("tki")),
                        json.dumps(data),
                        status,
                        lid,
                        data_date,
                        report.get("uuid"),
                    ))

        # Update the import project back to an editable state
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.import_projects
                SET status = 'LOADED'
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                project_id,
            ))

    return True


async def _delete_project_records(project_id, tki=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            DELETE FROM %s.import_project_data
            WHERE project_id = %s;
        """, (
            AsIs(tki),
            project_id,
        ))

    return True


async def _delete_project_imported_records(project_id, tki=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            DELETE FROM %s.collections
            WHERE import_set = %s;
        """, (
            AsIs(tki),
            project_id,
        ))

    return True


async def _delete_project(project_id, tki=None):
    """ Background thread to destroy the project

    Args:
        project_id:
        tki:

    Returns:

    """
    res = await _delete_project_records(project_id, tki=tki)
    res = await _delete_project_imported_records(project_id, tki=tki)

    async with get_db_cur() as cur:
        await cur.execute("""
            DELETE FROM %s.import_projects
            WHERE uuid = %s;
        """, (
            AsIs(tki),
            project_id,
        ))

    return True


async def delete_project(project_id, user=None):
    """ Delete a project from the system

    Proxies to _delete_project as a ioloop callback

    Args:
        project_id:
        user:

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.import_projects
            SET status = 'DELETING'
            WHERE uuid = %s;
        """, (
            AsIs(user.get('tki')),
            project_id,
        ))

    loop = tornado.ioloop.IOLoop.current()
    loop.spawn_callback(_delete_project, project_id, tki=user.get('tki'))

    return True


async def update_project(project_id, data, user=None):
    """ Update an import projects settings

    Args:
        project_id:
        data:
        user:

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.import_projects
            SET name = %s,
              target_type = %s,
              form_id = %s,
              data_set_id = %s,
              mapping = %s,
              description = %s,
              meta_map = %s
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            data.get("name"),
            data.get('target_type', None),
            data.get("form_id", None),
            data.get("data_set_id", None),
            json.dumps(data.get("mapping", {})),
            data.get("description", "No description"),
            json.dumps(data.get("meta_map", {})),
            project_id,
        ))

    return True


async def check_progress(project_id, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT status FROM %s.import_projects
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            project_id,
        ))
        res = await cur.fetchone()
        result = res.get('status')

    return result


def _fix_val(val):
    if isinstance(val, type(None)):
        return None
    elif isinstance(val, string_types):
        return val.strip()
    else:
        return val


async def _extrapolate_source(project, user=None):
    master = []

    first_row = True
    total_rows = 0
    schema = None
    schema_row = None
    project_id = project.get('uuid')
    with get_db_cursor(commit=True) as cur:
        with open(project.get("src_file"), encoding="utf-8", mode='r') as f:
            myReader = csv.reader(f, dialect='ewars_dialect')
            for i, row in enumerate(myReader):
                if first_row:
                    schema_row = list(row)
                    schema = dict((val, dict(null_="NULL", target=None)) for val in list(row))
                    first_row = False
                else:
                    total_rows += 1
                    row_data = dict(zip(
                        schema_row,
                        list([[x, None] for x in row])
                    ))

                    cur.execute("""
                        INSERT INTO %s.import_project_data
                        (project_id, status, data)
                        VALUES (%s, %s, %s);
                    """, (
                        AsIs(user.get('tki')),
                        project_id,
                        'BASE',
                        json.dumps(row_data),
                    ))

    project['meta_map']['total_rows'] = total_rows
    project['meta_map']['imported'] = 0
    project['meta_map']['deleted'] = 0

    for key, value in schema.items():
        if key not in project.get('mapping', {}).keys():
            project['mapping'][key] = value

    project['mapping'] = dict((key, value) for key, value in project.get('mapping', {}).items() if key in schema.keys())

    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            UPDATE %s.import_projects
                SET status = 'LOADED',
                  mapping = %s,
                  meta_map = %s
                WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            json.dumps(project.get('mapping')),
            json.dumps(project.get('meta_map')),
            project.get("uuid"),
        ))

    return True


async def run_import(project_id, user=None):
    """ Wraps the normal _run_import to run it in a async
    background routine allowing the API to return and stop blocking on
    the long process

    Args:
        project_id:
        user:

    Returns:

    """

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.import_projects
            SET status = 'IMPORTING'
            WHERE uuid = %s;
        """, (
            AsIs(user.get('tki')),
            project_id,
        ))

    cur_loop = asyncio.get_event_loop()
    cur_loop.create_task(
        _run_import(
            project_id,
            user=user
        )
    )

    return True


async def _run_import(project_id, user=None):
    """ Execute incremental import of valid rows from the import project

    Args:
        import_id: The UUID of the project to run against
        user: The calling user

    Returns:
        Error or true if successful
    """
    # TODO: Check rows for validity
    # TODO: Import to

    proj = None
    errors = dict()
    form = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.import_projects
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            project_id,
        ))
        proj = await cur.fetchone()

        await cur.execute("""
            SELECT f.id, f.name, f.features, f.version_id, fv.definition
            FROM %s.forms AS f
              LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
            WHERE f.id = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            proj.get('form_id'),
        ))
        form = await cur.fetchone()

        fields = form_utils.flatten_definition(form.get("definition"))

        mapping = proj.get("mapping", {})
        meta_map = proj.get("meta_map", {})
        lid_key = None

        data_date_key = None

        for key, value in proj.get("mapping", {}).items():
            if value.get("target", None) == "location_id":
                lid_key = key

        if value.get("target", None) == "data_date":
            data_date_key = key

        # Iterate through the reports
        imported = 0
        with get_db_cursor(name='IMPORT_DATA') as cur:
            cur.execute("""
                SELECT * FROM %s.import_project_data
                WHERE project_id = %s;
            """, (
                AsIs(user.get("tki")),
                project_id,
            ))

            for report in ResultIter(cur, arraysize=1000):
                data = report.get('data', {})

                isValid = True

                # Check data_date
                if report.get("data_date") in (None, "NULL", "null", "", "INVALID_DATE", "DATE_FUTURE"):
                    isValid = False

                # Check location
                if report.get("location_id") in (
                        None, "NULL", "null", "", "NO_MATCH", "NO_MAP", "MULTIPLE_MATCHES", "LOC_NOT_FOUND"):
                    isValid = False

                values = [v[1] for x, v in data.items()]

                infractions = ('NO_MAP', None, 'IGNORE_ROW', 'INVALID_SELECT', 'INVALID_DATE')
                if any(e in infractions for e in values):
                    isValid = False

                if isValid:
                    imported = imported + 1

                    new_report = dict(
                        uuid=report.get("uuid"),
                        data_date=report.get("data_date", None),
                        location_id=report.get("location_id", None),
                        form_id=form.get("id"),
                        form_version_id=form.get("version_id"),
                        created_by=user.get("id"),
                        import_set=project_id,
                        import_data=data,
                        source="IMPORT",
                        submitted=report.get("data_date", None),
                        submitted_date=report.get("data_date", None),
                        last_modified=datetime.datetime.utcnow(),
                        history=[
                            dict(
                                event_type="IMPORTED",
                                created=datetime.datetime.utcnow(),
                                name=user.get("name"),
                                user_id=user.get("id")
                            )
                        ],
                        data=None
                    )

                    report_data = dict()

                    for key, value in mapping.items():
                        if value.get("target", None) not in (None, "IGNORE"):
                            target = value.get('target')

                            if data[key][1] in ("", None, "NULL"):
                                report_data[target] = None
                            else:
                                report_data[target] = data[key][1]

                    new_report['data'] = report_data

                    # submit_report()
                    create_res = await _create_report(new_report, user=user)
                    remove_res = await remove_row(report.get("uuid"), user=user)

        # Update the imported count in the project
        meta_map['imported'] = int(meta_map['imported']) + imported

        async with get_db_cur() as cur:
            await cur.execute("""
            UPDATE %s.import_projects
            SET meta_map = %s,
                status = 'LOADED'
            WHERE uuid = %s;
        """, (
                AsIs(user.get("tki")),
                json.dumps(meta_map),
                project_id,
            ))

    return True


async def _create_report(report, user=None):
    """ Straight show insert report into the system

    Args:
        report: The report to be inserted
        user: The calling user

    Returns:
        Void
    """
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.collections
            (
              uuid,
              submitted_date,
              form_id,
              created_by,
              created,
              last_modified,
              status,
              data,
              form_version_id,
              data_date,
              location_id,
              import_set,
              import_data,
              source,
              history,
              submitted
            )
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """, (
            AsIs(user.get("tki")),
            report.get("uuid"),
            report.get("submitted_date"),
            report.get("form_id"),
            report.get("created_by"),
            report.get("created", datetime.datetime.utcnow()),
            report.get("last_modified", datetime.datetime.utcnow()),
            "SUBMITTED",
            json.dumps(report.get('data', {})),
            report.get("form_version_id"),
            report.get("data_date"),
            report.get("location_id"),
            report.get("import_set"),
            json.dumps(report.get("import_data")),
            "IMPORT",
            json.dumps(report.get("history"), cls=JSONEncoder),
            report.get("submitted"),
        ))

    return True


async def remove_row(row_id, user=None):
    """ Delete a row from the data source for the import

    Args:
        row_id: The UUID of the row to remove
        user: The calling user

    Returns:
        Boolean true that the operation was completed
    """
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT project_id
            FROM %s.import_project_data
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            row_id,
        ))
        row = await cur.fetchone()

        await cur.execute("""
            SELECT meta_map
            FROM %s.import_projects
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            row.get("project_id"),
        ))
        project = await cur.fetchone()

        meta_map = project.get("meta_map")
        meta_map['total_rows'] = meta_map.get("total_rows", 0) - 1

        await cur.execute("""
            DELETE FROM %s.import_project_data
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            row_id,
        ))

        await cur.execute("""
            UPDATE %s.import_projects
            SET meta_map = %s
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            json.dumps(meta_map),
            row.get("project_id"),
        ))

    return True


async def update_prop(row_id, prop, value, user=None):
    """ Updates a single prop on a row

    Args:
        row_id: The UUID of the report to update
        prop: The prop to update
        value: The value to replace into the prop
        user: The calling user

    Returns:

    """
    report = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.import_project_data
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            row_id,
        ))
        report = await cur.fetchone()

        realProp = prop.replace("data.", "")

        data = report.get('data', {})

        data[realProp][0] = value

        await cur.execute("""
             UPDATE %s.import_project_data
             SET data = %s
             WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            json.dumps(data),
            row_id,
        ))

    return True


async def get_importables(user=None):
    """ Get a list of forms and datasets that can be imported to

    Args:
        user:

    Returns:

    """
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name->>'en' as name, status
            FROM %s.forms 
            WHERE status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def get_chunks(res_id, user=None):
    """ Get import chunks for a given resource

    Args:
        res_id:
        user:

    Returns:

    """
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.import_projects
            WHERE form_id = %s;
        """, (
            AsIs(user.get('tki')),
            res_id,
        ))
        results = await cur.fetchall()

    return results
