import uuid

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.utils import tmp_files, date_utils
from ewars.models import Location

from ewars.analysis import immediate

async def export_indicator_data(definition, user=None):
    """Export indicator data for location(s)

    Args:
        definition: Defines what data should be exported
        user: The calling user

    Returns:
        A filepath to download the exported data from
    """
    locations = dict()
    indicators = dict()

    start_date = date_utils.parse_date(definition.get("start_date"))
    end_date = date_utils.parse_date(definition.get("end_date"))
    interval = definition.get("interval")
    select_type = definition.get("select_type", None)
    location_type = definition.get("location_type", None)
    loc_selects = definition.get("location")
    ind_select = definition.get("indicator")
    transpose = definition.get("transpose", False)

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.indicators
            WHERE uuid::TEXT IN %s
        """, (
            AsIs(user.get("tki")),
            tuple(definition.get("indicator")),
        ))
        tmp_inds = await cur.fetchall()
        indicators = dict([(x.get("uuid"), x) for x in tmp_inds])

    # Figure out which locations need to be loaded up.
    if select_type == "SELECTED":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, lineage, pcode
                FROM %s.locations
                WHERE uuid::TEXT IN %s
            """, (
                AsIs(user.get("tki")),
                tuple(loc_selects),
            ))
            locs = await cur.fetchall()
            locations = dict([x.get("uuid"), x] for x in locs)
    elif select_type == "CHILD_TYPE":
        pre_loaded_locs = []
        async with get_db_cur() as cur:
            for location in loc_selects:
                await cur.execute("""
                    SELECT uuid, name, lineage, pcode
                    FROM %s.locations
                    WHERE lineage::TEXT[] @> %s
                    AND site_type_id = %s
                    AND status = 'ACTIVE'
                """, (
                    AsIs(user.get("tki")),
                    [location],
                    location_type,
                ))
                tmper = await cur.fetchall()
                pre_loaded_locs = pre_loaded_locs + tmper

        for loc in pre_loaded_locs:
            locations[loc.get("uuid")] = loc

    for key, location in locations.items():
        locations[key]['fname'] = await Location.get_full_name(key, user=user)

    data = dict()
    report_dates = []

    for loc_id, location in locations.items():
        for ind_uuid in indicators.keys():
            resultant = await immediate.run(
                target_interval=interval,
                target_location=loc_id,
                indicator=ind_uuid,
                options=dict(
                    start_date=start_date,
                    end_date=end_date
                ),
                user=user
            )

            resultant = dict((x[0].strftime("%Y-%m-%d"), x[1]) for x in resultant.get('data'))
            report_dates = report_dates + [key for key, value in resultant.items()]

            try:
                data[loc_id][ind_uuid] = resultant
            except KeyError:
                data[loc_id] = dict()
                data[loc_id][ind_uuid] = resultant

    completed = None
    if not transpose:
        columns = ["location", "pcode", "path", "indicator"]

        # Sort report_dates
        report_dates = sorted(list(set(report_dates)))
        columns = columns + report_dates

        rows = []

        for loc_uuid, series in data.items():
            locale = locations.get(loc_uuid)
            for ind_uuid, data in series.items():
                ind = indicators.get(ind_uuid)
                row = [locale.get("name")['en'], locale.get("pcode", ""), locale.get("fname"), ind.get("name")['en']]

                for col in report_dates:
                    row.append(data.get(col, ""))

                rows.append(row)

        completed = [columns] + rows
    else:
        columns = ["location", "pcode", "path"]

        rows = []

        for loc_uuid, series in data.items():
            locale = locations.get(loc_uuid)
            row = [locale.get("name")['en'], locale.get("pcode", ""), locale.get("fname")]
            for ind_uuid, data in series.items():
                ind = indicators.get(ind_uuid)
                if ind.get("name")['en'] not in columns:
                    columns.append(ind.get("name")['en'])

                row.append(sum([v for x, v in data.items() if v is not None]))

            rows.append(row)

        completed = [columns] + rows

    file_uuid = str(uuid.uuid4())
    file_name = "export - %s - %s - %s" % (
        user.get("name"),
        start_date.strftime("%Y-%m-%d"),
        end_date.strftime("%Y-%m-%d"),
    )

    output = tmp_files.write_tmp_file(completed, file_name, "csv")

    return dict(fn=output)
