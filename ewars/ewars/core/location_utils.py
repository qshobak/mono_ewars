def check_name(name):
    has_val = True

    if isinstance(name, (dict,)):
        items = [x for x in name.values() if x not in ("", None)]

        if len(items) <= 0:
            has_val = False

    elif name in ("", None):
        has_val = False

    return has_val