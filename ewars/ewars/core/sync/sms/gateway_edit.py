import datetime

from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

def update_gateway(gate_id, data, user=None):
    """
    Update a gateways settings
    :param gate_id: {int} THe id of the gateway
    :param data: {dict} The properties to update
    :param user: {dict} The calling user
    :return:
    """
    result = None

    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            UPDATE %s.sms_gateways
                SET status = %s,
                can_receive = %s,
                can_send = %s,
                receive_number = %s,
                send_number = %s,
                gate_type = %s,
                created_date = %s,
                created_by = %s,
                last_modified = %s,
                out_rate_limit = %s,
                out_rate_limit_interval = %s,
                in_rate_limit = %s,
                in_rate_limit_interval = %s,
                name = %s,
                gate_version = %s
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            data.get("status", None),
            data.get("can_receive", False),
            data.get("can_send", False),
            data.get("receive_number", None),
            data.get("send_number", None),
            data.get("gate_type", None),
            data.get("created_date", None),
            data.get("created_by", None),
            datetime.datetime.now(),
            data.get("out_rate_limit", None),
            data.get("out_rate_limit_interval", None),
            data.get("in_rate_limit", None),
            data.get("in_date_limit_interval", None),
            data.get("name", None),
            data.get("gate_version", None),
            gate_id,
        ))

    return data

def delete_gateway(gate_id, user=None):
    """
    Delete a gateway from the system
    :param gate_id: {int} The id of the gateway
    :param user: {dict} The calling user
    :return:
    """
    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            UPDATE %s.devices
            SET gate_id = NULL
            WHERE gate_id = %s
        """, (
            AsIs(user.get("tki")),
            gate_id,
        ))

        cur.execute("""
            DELETE FROM %s.sms_gateways
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            gate_id,
        ))