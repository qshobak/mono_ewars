import json
import datetime

from psycopg2.extensions import AsIs

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.constants import CONSTANTS

from ewars.core.locations.utils import get_location_full_name

from ewars.core.analysis import system_parser
from ewars.core.analysis.analysis_rest_parser import analysis_parser

__ALL__ = ["retrieve_alerts"]


async def query_alerts(filter, limit, offset, lt=None, user=None):
    """Retrieve alerts that a user has access to

    Args:
        filter: Any filtering applied to the alerts
        limit: The number of alerts to return
        offset: The offset for pagination
        user: The calling user

    Returns:

    """
    results = []
    alerts = []
    location_ids = []

    indicator = None
    if filter == "OPEN":
        indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_OPEN")
    else:
        indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_CLOSED")

    if user['role'] == CONSTANTS.ACCOUNT_ADMIN:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.date_spec_interval_type AS interval_type
                    FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                    WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                        AND a.state = '%s'
            """ % (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("clid"),
                filter,
            ))
            alerts = await cur.fetchall()

    elif user.get("role") == CONSTANTS.REGIONAL_ADMIN:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.date_spec_interval_type AS interval_type
                    FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                    WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                        AND a.state = '%s'
            """ % (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("clid"),
                filter,
            ))
            alerts = await cur.fetchall()

    elif user['role'] == CONSTANTS.USER:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.date_spec_interval_type AS interval_type
                    FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                        LEFT JOIN %s.alert_users AS au ON au.alert_id = a.uuid
                    WHERE au.user_id = %s
                        AND a.state = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get('id'),
                filter,
            ))
            alerts = await ur.fetchall()

    location_ids = [x['location_id'] for x in alerts]
    confinement = None
    if user['role'] not in (CONSTANTS.SUPER_ADMIN, CONSTANTS.INSTANCE_ADMIN, CONSTANTS.GLOBAL_ADMIN):
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT ST_AsGeoJSON(default_center) AS center, default_zoom
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                user.get("clid"),
            ))
            confinement = await cur.fetchone()
            if confinement.get("center", None) is not None:
                confinement['center'] = json.loads(confinement['center'])

    if len(alerts) <= 0:
        return dict(
            confinement=confinement,
            results=[]
        )

    locations = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
              uuid::TEXT,
              name,
              geometry_type,
              ST_AsGeoJSON(ST_Centroid(point)) as centroid,
              ST_AsGeoJSON(ST_Extent(point)) as extent
              FROM %s.locations
              WHERE uuid IN %s
              GROUP BY uuid
        """, (
            AsIs(user.get("tki")),
            tuple(location_ids),
        ))
        locations = await cur.fetchall()

    locs = dict((str(loc.get('uuid')), loc) for loc in locations)

    for alert in alerts:
        results.append(dict(
            alert=alert,
            location=locs[str(alert.get("location_id"))]
        ))

    return dict(
        confinement=confinement,
        results=results
    )


async def retrieve_alerts(filter, limit, offset, lt=None, user=None):
    """Retrieve alerts that a user has access to

    Args:
        filter: Any filtering applied to the alerts
        limit: The number of alerts to return
        offset: The offset for pagination
        user: The calling user

    Returns:

    """
    results = []
    alerts = []
    location_ids = []

    indicator = None
    if filter == "OPEN":
        indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_OPEN")
    else:
        indicator = dict(uuid='a18b5f1f-4775-4bc9-89cf-b0d83abab45a', dimension="ALERTS_CLOSED")

    if user['role'] == CONSTANTS.ACCOUNT_ADMIN:
        results = await analysis_parser(dict(
            indicator=indicator,
            location=dict(
                parent_id=user.get("clid"),
                site_type_id=lt or 10,
                status='ACTIVE'
            ),
            interval="DAY",
            start_date="2011-01-01",
            end_date=datetime.date.today(),
            reduction="SUM",
            type="SLICE",
            geometry=True
        ), user=user)

        results['d'] = [x for x in results.get('d') if x.get("data", 0) > 0]
    elif user['role'] == CONSTANTS.REGIONAL_ADMIN:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.date_spec_interval_type AS interval_type
                    FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                    WHERE l.lineage::TEXT[] @> '{%s}'::TEXT[]
                        AND a.state = '%s'
            """ % (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("location_id"),
                filter,
            ))
            alerts = await cur.fetchall()
        location_ids = [x['location_id'] for x in alerts]
    elif user['role'] == CONSTANTS.USER:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name AS alarm_name, al.date_spec_interval_type AS interval_type
                    FROM %s.alerts AS a
                        LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                        LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                        LEFT JOIN %s.alert_users AS au ON au.alert_id = a.uuid
                    WHERE au.user_id = %s
                        AND a.state = %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                user.get("id"),
                filter,
            ))
            alerts = await cur.fetchall()

        location_ids = [x['location_id'] for x in alerts]

    confinement = None
    if user['role'] not in (CONSTANTS.SUPER_ADMIN, CONSTANTS.INSTANCE_ADMIN, CONSTANTS.GLOBAL_ADMIN):
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT ST_AsGeoJSON(default_center) AS center, default_zoom
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                user.get("clid"),
            ))
            confinement = await cur.fetchone()
            if confinement.get("center", None) is not None:
                confinement['center'] = json.loads(confinement['center'])

    return results


async def get_alert(alert_uuid, user=None):
    """Retrieve an alert

    Args:
        alert_uuid: The UUID of the alert to retrieve
        user: The calling user

    Returns:

    """
    result = None
    alert = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*
            FROM %s.alerts_full AS a
            WHERE a.uuid = %s
        """, (
            AsIs(user.get("tki")),
            alert_uuid,
        ))
        alert = await cur.fetchone()

        if alert is not None:
            await cur.execute("""
                SELECT *
                FROM %s.alarms_V2
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert['alarm_id'],
            ))
            alarm = await cur.fetchone()

            await cur.execute("""
                SELECT type, submitted_date
                FROM %s.alert_actions
                WHERE alert_id = %s
            """, (
                AsIs(user.get("tki")),
                alert_uuid,
            ))
            actions = await cur.fetchall()
            map_actions = {}

            for item in actions:
                map_actions[item.get("type")] = item

            result = alert

            report_id = alert.get("trigger_report_id", None)
            if report_id is not None:
                await cur.execute("""
                    SELECT uuid, form_id, form_version_id, submitted_date, data_date
                    FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    report_id,
                ))
                result['report'] = await cur.fetchone()
            else:
                result['report'] = {}

            result['alarm'] = alarm
            result['actions'] = map_actions

    result['location_name_full'] = await get_location_full_name(alert.get("location_id"), user=user)

    return result
