from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.constants import CONSTANTS

from ewars.core.locations.utils import get_location_full_name

from psycopg2.extensions import AsIs



async def get_related_alerts(alert_id, user=None):
    alert = None
    alerts = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alerts WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            alert_id,
        ))
        alert = await cur.fetchone()

        await cur.execute("""
            SELECT a.uuid,
                a.trigger_end,
                a.location_id,
                a.state,
                a.stage,
                a.stage_state,
                a.created,
                al.name AS alarm_name,
                i.name AS indicator_name,
                l.name AS location_name
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                LEFT JOIN %s.indicators AS i ON i.uuid = al.indicator_id
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND a.location_id = %s
                AND a.uuid != %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert.get("alarm_id"),
            alert.get("location_id"),
            alert_id,
        ))
        alerts = await cur.fetchall()

    for alert in alerts:
        alert['location_name_full'] = await get_location_full_name(alert.get("location_id"), user=user)

    return alerts