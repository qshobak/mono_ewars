import json
import inspect
import traceback

from tornado import gen

from ewars.conf import settings
from jose import jwt, JWTError

from ewars.core.serializers.json_encoder import JSONEncoder

class BaseAPI(object):
    _methods = dict()

    @classmethod
    async def execute(cls, args, kwargs):
        method = args[0]
        args = args[1:]
        if method in cls._methods.keys():
            arg_definition = inspect.getfullargspec(cls._methods[method])
            if 'application' not in arg_definition[0]:
                if 'application' in kwargs.keys():
                    del kwargs['application']

            method_fn = cls._methods[method]
            if method_fn is None:
                return dict(
                    error=0,
                    error_desc="NOT_IMPLEMENTED"
                )
            else:
                if inspect.iscoroutinefunction(cls._methods[method]):
                    result = await cls._methods[method](*args, **kwargs)
                else:
                    result = cls._methods[method](*args, **kwargs)
                return json.dumps(dict(
                    data=result
                ), cls=JSONEncoder)
        else:
            return dict(
                error=0,
                error_desc="NO_METHOD"
            )
