from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.models import User


async def get_metrics(user=None):
    """ Get metrics for a given mobile user
    
    Args:
        user: The calling user

    Returns:

    """
    metrics = dict(
        OPEN_ALERTS=0,
        OPEN_TASKS=0,
        MONITORED_ALERTS=0
    )

    # Account Admin query across all alerts
    if user.get("role") == "ACCOUNT_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT COUNT(*) AS t
                 FROM %s.alerts AS a
                  LEFT JOIN %s.alarms_v2 AS al ON a.alarm_id = al.uuid
                 WHERE a.state = 'OPEN'
                 AND al.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            res = await cur.fetchone()
            metrics['OPEN_ALERTS'] = res.get('t')

            await cur.execute("""
                SELECT COUNT(*) AS t
                FROM %s.alerts AS a 
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.outcome = 'MONITOR'
                  AND al.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            res = await cur.fetchone()
            metrics['MONITORED_ALERTS'] = res.get('t')

    # Regional Admin, query for alerts under their location
    if user.get("role") == "REGIONAL_ADMIN":
        location_id = user.get("location_id", None)

        if location_id is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS t
                    FROM %s.alerts AS a 
                      LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                      LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                    WHERE l.lineage::TEXT[] @> %s::TEXT[]
                      AND a.state = 'OPEN'
                      AND al.status = 'ACTIVE';
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    [location_id],
                ))
                res = await cur.fetchone()
                metrics['OPEN_ALERTS'] = res.get('t')

                await cur.execute("""
                    SELECT COUNT(*) AS t
                    FROM %s.alerts AS a 
                      LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                      LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                    WHERE l.lineage::TEXT[] @> %s::TEXT[]
                      AND a.state = 'MONITOR'
                      AND al.status = 'ACTIVE';
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    [location_id],
                ))
                res = await cur.fetchone()
                metrics['MONITORED_ALERTS'] = res.get('t')

    # Reporting User, query for alerts they are explicitly added to
    if user.get("role") == "USER":
        assignments = await User.assignments(user=user)
        loc_ids = []

        for assign in assignments:
            for loc in assign.get("locations", []):
                loc_ids.append(str(loc.get("location_id")))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS t
                FROM %s.alerts AS a 
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.state = 'OPEN'
                    AND al.status = 'ACTIVE'
                    AND a.location_id::TEXT = ANY(%s);
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
            ))
            res = await cur.fetchone()
            metrics['OPEN_ALERTS'] = res.get('t')

            await cur.execute("""
                SELECT COUNT(*) AS t
                FROM %s.alerts AS a 
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                  WHERE a.state = 'MONITOR'
                  AND al.status = 'ACTIVE'
                  AND a.location_id::TEXT = ANY(%s);
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
            ))
            res = await cur.fetchone()
            metrics['MONTORED_ALERTS'] = res.get('t')

    return metrics
