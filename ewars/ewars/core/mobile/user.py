import uuid
import json
from jose import jwt, JWTError

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.conf import settings
from ewars.models import User

from psycopg2.extensions import AsIs


async def get_user(user=None):
    result = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            user.get("id"),
        ))
        result = await cur.fetchone()

    del result['password']
    del result['created']
    del result['last_modified']
    del result['registered']

    for key, value in result.items():
        if isinstance(value, uuid.UUID):
            result[key] = str(value)

    token = jwt.encode(result, settings.JWT_KEY, algorithm="HS256")

    data = dict(
        id=result['id'],
        name=result['name'],
        account_name=result['account_name'],
        email=result['email'],
        organization_name=result['org_name'],
        role=result.get("role"),
        location_id=result.get("location_id", None),
        token=token
    )

    return data

async def update_password(new_pass, conf_pass, user=None):
    pass


def _has_geom(data, geometry_type):
    """ Check if a geometry is set ofr a location, primarily checks
    a location if it has a point coordinate associated with it.

    Args:
        data:
        geometry_type:

    Returns:

    """
    has_geom = True
    if geometry_type == "POINT":
        if data is None:
            has_geom = False
        else:
            if not isinstance(data, (dict,)):
                geom = json.loads(data)

            if data.get("type", None) is None:
                has_geom = False

            if data.get("type", None) == 'Point':
                if float(data.get("coordinates", [0, 0])[0]) == float(0) and float(
                        data.get("coordinates", [0, 0])[1]) == float(0):
                    has_geom = False

    elif geometry_type is None:
        has_geom = False
    elif geometry_type == "ADMIN":
        # We don't handle admin type geometries on mobile so just mark it as having geom
        has_geom = True
    else:
        has_geom = True

    return has_geom


async def _get_assignments(user=None):
    results = dict()

    assignments = None

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT a.*,
                    l.name AS location_name,
                    l.uuid AS location_uuid,
                    l.geometry_type,
                    l.geojson,
                    f.name AS form_name,
                    f.id AS form_id,
                    f.version_id,
                    f.location_aware,
                    f.single_report_context,
                    f.features,
                    fv.definition AS form_definition
                FROM %s.assignments AS a
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                    LEFT JOIN %s.forms AS f ON f.id = a.form_id
                    LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE a.user_id = %s
                    AND a.status = 'ACTIVE'
            """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user['id'],
        ))
        assignments = await cur.fetchall()

    form_loc_dict = dict()

    for assign in assignments:
        form_loc_dict[assign.get('form_id')] = []
        if assign.get("form_id") not in results.keys():
            results[assign.get("form_id")] = dict(
                form_name=assign.get("form_name"),
                definition=assign.get("form_definition", {}),
                version_id=assign.get("version_id"),
                form_id=assign.get('form_id'),
                location_aware=assign.get("features", {}).get("LOCATION_REPORTING", None) is not None,
                interval=assign.get("features", {}).get("INTERVAL_REPORTING", {}).get("interval", None),
                features=assign.get("features"),
                single_report_context=assign.get("features", {}).get("INTERVAL_REPORTING", None) is not None,
                block_future_dates=assign.get("features", {}).get("INTERVAL_REPORTING", None) is not None,
                locations=[],
                assignments=[]
            )

        if assign.get("type", "DEFAULT") == "DEFAULT":
            # A default assignment type to a specific location

            location = dict(
                uuid=assign['uuid'],
                location_id=assign['location_uuid'],
                location_name=assign['location_name'],
                start_date=assign.get("start_date"),
                end_date=assign.get("end_date"),
                no_geom=not _has_geom(
                    assign.get('geometry_type', None),
                    assign.get("geojson", None)
                ),
                existing=[]
            )

            existing_reports = []
            if assign.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                async with get_db_cur() as cur:
                    await cur.execute("""
                            SELECT data_date
                            FROM %s.collections
                            WHERE form_id = %s
                                AND location_id = %s
                                AND status = 'SUBMITTED'
                        """, (
                        AsIs(user.get("tki")),
                        assign['form_id'],
                        assign['location_uuid'],
                    ))
                    existing_reports = await cur.fetchall()

            location['existing'] = existing_reports

            results[assign.get('form_id')]['locations'].append(location)
            results[assign.get("form_id")]['assignments'].append(location)

        elif assign.get("type", None) == "UNDER":
            site_type_id = assign.get("features", {}).get("LOCATION_REPORTING", {}).get("site_type_id", None)
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid, name
                        FROM %s.locations
                        WHERE site_type_id = %s 
                        AND lineage::TEXT[] @> %s::TEXT[] 
                        AND status = 'ACTIVE';
                    """, (
                    AsIs(user.get("tki")),
                    site_type_id,
                    [assign.get("location_uuid")],
                ))
                locations = await cur.fetchall()

                for location in locations:
                    if location.get("uuid") not in form_loc_dict[assign.get("form_id")]:
                        form_loc_dict[assign.get('form_id')].append(location.get("uuid"))

                        loc_assign = dict(
                            uuid=assign['uuid'],
                            location_id=location['uuid'],
                            location_name=location['name'],
                            start_date=assign.get("start_date"),
                            end_date=assign.get("end_date"),
                            no_geom=not _has_geom(
                                location.get('geometry_type', None),
                                location.get("geojson", None)
                            ),
                            existing=[]
                        )

                        existing_reports = []
                        if assign.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                            async with get_db_cur() as cur:
                                await cur.execute("""
                                        SELECT data_date
                                        FROM %s.collections
                                        WHERE form_id = %s
                                            AND location_id = %s
                                            AND status = 'SUBMITTED'
                                    """, (
                                    AsIs(user.get("tki")),
                                    assign['form_id'],
                                    location['uuid'],
                                ))
                                existing_reports = await cur.fetchall()

                        loc_assign['existing'] = existing_reports

                        results[assign.get('form_id')]['locations'].append(loc_assign)
                        results[assign.get("form_id")]['assignments'].append(loc_assign)

        elif assign.get("type", None) == "GROUP":
            site_type_id = assign.get("features", {}).get("LOCATION_REPORTING", {}).get("site_type_id", None)
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid, name, geojson, geometry_type
                        FROM %s.locations
                        WHERE groups::TEXT[] @> %s::TEXT[]
                        AND site_type_id = %s
                        AND status = 'ACTIVE';
                    """, (
                    AsIs(user.get("tki")),
                    [assign.get("definition", {}).get("group", None)],
                    site_type_id,
                ))
                locations = await cur.fetchall()

                for location in locations:
                    if location.get("uuid") not in form_loc_dict[assign.get("form_id")]:
                        form_loc_dict[assign.get('form_id')].append(location.get("uuid"))

                        loc_assign = dict(
                            uuid=assign['uuid'],
                            location_id=location['uuid'],
                            location_name=location['name'],
                            start_date=assign.get("start_date"),
                            end_date=assign.get("end_date"),
                            no_geom=not _has_geom(
                                location.get('geometry_type', None),
                                location.get("geojson", None)
                            ),
                            existing=[]
                        )

                        existing_reports = []
                        if assign.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                            async with get_db_cur() as cur:
                                await cur.execute("""
                                        SELECT data_date
                                        FROM %s.collections
                                        WHERE form_id = %s
                                            AND location_id = %s
                                            AND status = 'SUBMITTED'
                                    """, (
                                    AsIs(user.get("tki")),
                                    assign['form_id'],
                                    location['uuid'],
                                ))
                                existing_reports = await cur.fetchall()

                        loc_assign['existing'] = existing_reports

                        results[assign.get('form_id')]['locations'].append(loc_assign)
                        results[assign.get("form_id")]['assignments'].append(loc_assign)

    # Clean up any assignments which result in an assignment with no locations
    # for instance a group assignment that has no locations in the group or
    # an under assignment that has no location in the parent location.
    removals = []
    for key, assign in results.items():
        if len(assign.get("locations", [])) <= 0:
            removals.append(key)

    if len(removals) > 0:
        for key in removals:
            del results[key]

    return [value for key, value in results.items()]


async def get_assignments(user=None):
    results = dict()

    assignments = None

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT a.*,
                    l.name AS location_name,
                    l.uuid AS location_uuid,
                    l.geometry_type,
                    l.geojson,
                    f.name AS form_name,
                    f.id AS form_id,
                    f.version_id,
                    f.location_aware,
                    f.single_report_context,
                    f.features,
                    fv.definition AS form_definition
                FROM %s.assignments AS a
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                    LEFT JOIN %s.forms AS f ON f.id = a.form_id
                    LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE a.user_id = %s
                    AND a.status = 'ACTIVE'
            """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user['id'],
        ))
        assignments = await cur.fetchall()

    for assign in assignments:
        item = dict(
            uuid=assign['uuid'],
            location_id=assign['location_uuid'],
            location_name=assign['location_name'],
            start_date=assign.get("start_date"),
            end_date=assign.get("end_date")
        )

        no_geom = False
        if assign.get("geometry_type", None) == "POINT":
            if assign.get("geojson", None) is None:
                no_geom = True
            else:
                geom = assign.get("geojson")
                if not isinstance(geom, (dict,)):
                    geom = json.loads(geom)

                if geom.get("type", None) is None:
                    no_geom = True

                if geom.get("type", None) == 'Point':
                    if float(geom.get("coordinates", [0, 0])[0]) == float(0) and float(
                            geom.get("coordinates", [0, 0])[1]) == float(0):
                        no_geom = True



        elif assign.get("geometry_type", None) == None:
            no_geom = True
        elif assign.get("geometry_type", None) == "ADMIN":
            no_geom = False
        else:
            no_geom = True

        item["no_geom"] = no_geom

        existing_reports = []
        if assign.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT data_date
                        FROM %s.collections
                        WHERE form_id = %s
                            AND location_id = %s
                            AND status = 'SUBMITTED'
                    """, (
                    AsIs(user.get("tki")),
                    assign['form_id'],
                    assign['location_uuid'],
                ))
                existing_reports = await cur.fetchall()

        item['existing'] = existing_reports

        location_aware = False
        block_future_dates = False
        single_report_context = False
        interval = "DAY"

        if "INTERVAL_REPORTING" in assign.get("features"):
            block_future_dates = True
            single_report_context = True
            interval = assign['features']['INTERVAL_REPORTING'].get("interval")

        if "LOCATION_REPORTING" in assign.get("features"):
            location_aware = True

        try:
            results[assign['form_id']]['locations'].append(item)
        except KeyError as e:
            results[assign['form_id']] = dict(
                form_name=assign['form_name'],
                definition=assign['form_definition'],
                version_id=assign['version_id'],
                form_id=assign['form_id'],
                location_aware=location_aware,
                interval=interval,
                features=assign.get("features"),
                single_report_context=single_report_context,
                block_future_dates=block_future_dates,
                locations=[item],
                no_geom=no_geom
            )

        try:
            results[assign['form_id']]['assignments'].append(item)
        except KeyError as e:
            results[assign['form_id']]['assignments'] = [item]

    results = [value for key, value in results.items()]
    return results
