from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.models import User


async def get_alert(alert_uuid, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*, l.name AS location_name
            FROM %s.alerts AS a 
              LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
              WHERE a.uuid = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert_uuid,
        ))
        result = await cur.fetchone()

    return result


async def get_open_alerts(user=None):
    alarms = []
    alerts = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alarms_v2
            WHERE status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
        ))
        alarms = await cur.fetchall()

    if user.get("role") == "ACCOUNT_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name
                FROM %s.alerts AS a
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.state = 'OPEN'
                  AND al.status = 'ACTIVE'
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            alerts = await cur.fetchall()

    if user.get("role") == "REGIONAL_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name
                FROM %s.alerts AS a 
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.state = 'OPEN'
                  AND l.lineage::TEXT[] @> %s::TEXT[]
                  AND al.status = 'ACTIVE'
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user.get("location_id")],
            ))
            alerts = await cur.fetchall()

    if user.get("role") == "USER":
        assignments = await User.assignments(user=user)
        loc_ids = []

        for assign in assignments:
            for loc in assign.get("locations", []):
                loc_ids.append(loc.get("location_id"))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, al.name as alarm_name
                FROM %s.alerts AS a 
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id 
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.state = 'OPEN'
                  AND al.status = 'ACTIVE'
                  AND l.uuid = ANY(%s)
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
            ))
            alerts = await cur.fetchall()

    return dict(
        alerts=alerts,
        alarms=alarms
    )


async def get_monitored_alerts(user=None):
    alarms = []
    alerts = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alarms_v2
            WHERE status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
        ))
        alarms = await cur.fetchall()

    if user.get("role") == "ACCOUNT_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name
                FROM %s.alerts AS a
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.outcome = 'MONITOR'
                  AND al.status = 'ACTIVE'
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            alerts = await cur.fetchall()

    if user.get("role") == "REGIONAL_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name
                FROM %s.alerts AS a 
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.outcome = 'MONITOR'
                  AND l.lineage::TEXT[] @> %s::TEXT[]
                  AND al.status = 'ACTIVE'
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user.get("location_id")],
            ))
            alerts = await cur.fetchall()

    if user.get("role") == "USER":
        assignments = await User.assignments(user=user)
        loc_ids = []

        for assign in assignments:
            for loc in assign.get("locations", []):
                loc_ids.append(loc.get("uuid"))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name
                FROM %s.alerts AS a 
                    LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.outcome = 'MONITOR'
                  AND al.status = 'ACTIVE'
                  AND l.uuid = ANY(%s)
                ORDER BY a.trigger_end DESC;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
            ))
            alerts = await cur.fetchall()

    return dict(
        alerts=alerts,
        alarms=alarms
    )
