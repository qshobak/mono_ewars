import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from ewars.models import Form, Submission

from psycopg2.extensions import AsIs

ERR_CODES = [
    "IS_DUPLICATE",
    "MISSING_REQ_FIELD",
    "LOC_MISSING",
    "DATE_MISSING",
    "FORM_INACTIVE",
    "LOCATION_INACTIVE"
]


async def _process_records(records, user=None):
    records = []

    for record in records:
        # 1. Check for duplicate
        # 2. Check for form updates
        # 3. Check for location
        async with get_db_cur() as cur:
            uniques = await Form.get_unique_fields(record.get("form_id", None), user=user)

            if "location_id" in uniques and "data_date" in uniques:
                await cur.execute("""
                    SELECT uuid
                    FROM %s.collections
                    WHERE data_date = %s 
                      AND location_id = %s 
                      AND form_id = %s 
                      AND status = 'SUBMITTED';
                """, (
                    AsIs(user.get("tki")),
                    record.get("data_date"),
                    record.get("location_id"),
                    record.get("form_id"),
                ))
                result = await cur.fetchone()

                if result is not None:
                    record['error'] = "DUPLICATE_RECORD"
                    records.append(record)
                else:
                    result = await Submission.submit(
                        record.get("form_id"),
                        record.get("form_version_id"),
                        record,
                        source="MOBILE",
                        user=user
                    )
    return records


async def _sync_acc_admin(data, user=None):
    result = dict(
        locations=[],
        forms=[],
        user=dict()
    )

    records = data.get("records", [])
    device = data.get("device", {})
    dev_inf = await update_device(device, user=user)

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid, name, lineage, site_type_id, parent_id, pcode
            FROM %s.locations 
            WHERE status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
        ))
        locations = await cur.fetchall()

    result['locations'] = locations
    result['records'] = records

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("id")),
        ))
        result['user'] = await cur.fetchone()


    return result


async def _sync_geo_admin(data, user=None):
    result = dict(
        locations=[],
        forms=[],
        user=dict()
    )

    records = data.get("record", [])
    device = data.get("device", {})

    return result


async def _sync_user(data, user=None):
    result = dict(
        assignments=[],
        user=dict()
    )

    records = data.get("records", [])
    device = data.get("device", {})

    return result


async def sync_device(data, user=None):
    response = dict()

    if user.get("role", "USER") == "ACCOUNT_ADMIN":
        response = await _sync_acc_admin(data, user=user)
    elif user.get("role", "USER") == "GEO_ADMIN":
        response = await _sync_geo_admin(data, user=user)
    else:
        response = await _sync_user(data, user=user)

    return response


async def update_device(data, user=None):
    device = None

    device_id = data.get("device_id", None)

    if device_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id FROM %s.devices WHERE device_id = %s;
            """, (
                AsIs(user.get("tki")),
                device_id,
            ))
            device = await cur.fetchone()

        if device is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.devices 
                    SET device_type = %s,
                      device_os_version = %s,
                      device_number = %s,
                      user_id = %s,
                      last_seen = %s,
                      app_version = %s,
                      app_version_name = %s,
                      android_version = %s,
                      device_name = %s
                    WHERE device_id = %s;
                """, (
                    AsIs(user.get("tki")),
                    data.get("platform"),
                    data.get("buildNo"),
                    data.get("phone"),
                    user.get("id"),
                    datetime.datetime.now(),
                    data.get("versionCode"),
                    data.get("versionName"),
                    data.get("androidVersion"),
                    data.get("device"),
                    device_id,
                ))
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.devices (
                      device_type,
                      device_id,
                      device_os_version,
                      device_number,
                      user_id,
                      last_seen,
                      status,
                      app_version,
                      app_version_name,
                      android_version,
                      device_name
                    ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(user.get("tki")),
                    data.get("platform"),
                    data.get("device_id"),
                    data.get("buildNo"),
                    data.get("phone"),
                    user.get("id"),
                    datetime.datetime.now(),
                    'ACTIVE',
                    data.get("versionCode"),
                    data.get("versionName"),
                    data.get("androidVersion"),
                    data.get("device")
                ))

    return True
