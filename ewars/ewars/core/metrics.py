from ewars.db import get_db_cur
from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs


async def get_overall_metrics(user=None):
    alerts = 0
    forms = 0

    async with get_db_cur() as cur:
        if user.get('role') == CONSTANTS.USER:
            await cur.execute('''
                SELECT COUNT(*) AS total
                FROM %s.alerts AS a 
                  LEFT JOIN %s.alert_users AS au On au.alert_id = a.uuid
                WHERE a.state = 'OPEN'
                  AND au.user_id = %s;
            ''', (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                user.get('id'),
            ))
        elif user.get('role') == CONSTANTS.ACCOUNT_ADMIN:
            await cur.execute('''
                SELECT COUNT(*) AS total
                FROM %s.alerts AS a 
                WHERE a.state = 'OPEN';
            ''', (
                AsIs(user.get('tki')),
            ))
        elif user.get('role') == CONSTANTS.REGIONAL_ADMIN:
            await cur.execute('''
                SELECT COUNT(*) AS total
                FROM %s.alerts AS a 
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id 
                WHERE a.lineage::TEXT[] @> %s::TEXT[]
                  AND a.state = 'OPEN';
            ''', (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                [user.get('location_id')],
            ))

        alerts = await cur.fetchone()
        alerts = alerts.get('total', 0)

        await cur.execute('''
            SELECT COUNT(*) AS total
            FROM %s.forms
            WHERE status = 'ACTIVE';
        ''', (
            AsIs(user.get('tki')),
        ))
        forms = await cur.fetchone()
        forms = forms.get('total')

    return dict(
        alerts=alerts,
        forms=forms
    )


METRICS_QUERIES = dict(
    FORMS='''
        SELECT COUNT(*) FROM TKI.forms
        WHERE status = 'ACTIVE';
    ''',
    FORM_SUBMISSIONS='''
        SELECT COUNT(*) AS total
        FROM TKI.collections AS c
          LEFT JOIN TKI.forms AS f ON f.id = c.form_id
        WHERE c.status = 'SUBMITTED'
          AND f.status = 'ACTIVE';
    ''',
    SUBMISSIONS='''
        SELECT COUNT(*) AS total
        FROM TKI.collections AS c
          LEFT JOIN TKI.forms AS f ON f.id = c.form_id
        WHERE c.status = 'SUBMITTED'
          AND f.status = 'ACTIVE';
    ''',
    USERS='''
        SELECT COUNT(*) AS total
        FROM TKI.users
        WHERE status = 'ACTIVE';
    ''',
    LOCATIONS='''
        SELECT COUNT(*) AS total
        FROM TKI.locations 
        WHERE status = 'ACTIVE';
    ''',
    ALERTS_OPEN='''
        SELECT COUNT(*) AS total
        FROM TKI.alerts
        WHERE state = 'OPEN';
    ''',
    ALERTS_CLOSED='''
        SELECT COUNT(*) total
        FROM TKI.alerts
        WHERE status = 'CLOSED';
    ''',
    ALERTS_TOTAL='''
        SELECT COUNT(*) total 
        FROM TKI.alerts;
    ''',
    DEVICES='''
        SELECT COUNT(*) total
        FROM TKI.devices;
    ''',
    PARTNERS='''
        SELECT COUNT(org_id)
        FROM TKI.users
        GROUP BY org_id;
    ''',
    TASKS_OPEN='''
        SELECT COUNT(*) total
        FROM TKI.tasks_v2
        WHERE state = 'OPEN';
    ''',
    ALARMS='''
        SELECT COUNT(*) total
        FROM TKI.alarms_v2;
    ''',
    ORGANIZATIONS='''
        SELECT COUNT(org_id)
        FROM TKI.users
        WHERE status = 'ACTIVE'
        GROUP BY org_id;
    '''

)


async def get_metric(metric_name, user=None):
    """ Retrieve a metric for a given user
    
    Args:
        metric_name: 
        user: 

    Returns:

    """
    result = 0
    if metric_name in METRICS_QUERIES.keys():
        async with get_db_cur() as cur:
            sql = METRICS_QUERIES.get(metric_name)
            sql = sql.replace('TKI', user.get('tki'))
            await cur.execute(sql)
            result = await cur.fetchone()
            result = result.get('total')

    return result
