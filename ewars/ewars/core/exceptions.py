from ewars.utils import six

class ImproperlyConfigured(Exception):
    """EWARS is somehow improperly configured"""
    pass

class ExtensionsRegistryNotReady(Exception):
    pass

class SuspiciousFileOperation(Exception):
    pass

