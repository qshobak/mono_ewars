import uuid

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.conf import settings
from ewars.utils.tmp_files import write_tmp_file

def i18n(item):
    if isinstance(item, (dict,)):
        return item.get("en", "No Translation")
    else:
        return item

async def export_locations(location_id, site_type_id, status, user=None):
    """ Export locations within the system

    Args:
        definition:
        user:

    Returns:

    """
    results = []

    site_type = ""
    status_type = ""
    if status not in (None, "ANY"):
        status_type = " AND l.status = '" + status + "'"
    else:
        status_type = " AND l.status != 'DELETED'"

    location_types = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.location_types ORDER BY id;
        """, (
            AsIs(user.get('tki')),
        ))
        location_types = await cur.fetchall()

        await cur.execute("""
            SELECT l.uuid,
                l.name,
                l.pcode,
                l.groups,
                l.parent_id,
                l.lineage,
                l.status,
                l.geometry_type,
                l.groups,
                l.site_type_id,
                (SELECT json_agg(json_build_object(t.site_type_id::TEXT, t.name->>'en')) FROM %s.locations AS t WHERE t.uuid::TEXT= ANY(l.lineage::TEXT[])) AS parents,
                lt.name AS location_type
            FROM %s.locations AS l
                LEFT JOIN %s.location_types AS lt ON l.site_type_id = lt.id
            WHERE l.lineage::TEXT[] @> %s::TEXT[]
            %s
            %s
            ORDER BY array_length(lineage, 1) DESC
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [location_id],
            AsIs(site_type),
            AsIs(status_type),
        ))
        results = await cur.fetchall()

    if len(results) <= 0:
        return dict(
            err=True,
            code="NO_DATA"
        )

    loc_dict = dict((x.get("uuid"), x) for x in results)

    headers = [
        "uuid",
        "name",
        "pcode",
        "parent_id",
        "lineage",
        "status",
        "geometry_type",
        "groups",
        "location_type"
    ]
    headers = headers + [x.get('name', {}).get('en', 'Uknown') for x in location_types]
    loc_types = [str(x.get('id')) for x in location_types]

    rows = [headers]

    for location in results:
        groups = location.get('groups')
        if groups is None:
            groups = None
        else:
            groups = " + ".join(groups)

        loc_parents = {k: v for d in location.get('parents', {}) for k, v in d.items()}

        if site_type_id not in (None, ""):
            if location.get("site_type_id") == site_type_id:
                rows.append([
                                location.get("uuid"),
                                location['name']['en'],
                                location.get("pcode"),
                                location.get("parent_id"),
                                ".".join(location.get("lineage")),
                                location.get("status"),
                                location.get("geometry_type"),
                                groups,
                                location.get("location_type", {}).get('en', 'Unknown'),
                            ] + [loc_parents.get(x, '') for x in loc_types])
        else:
            rows.append([
                            location.get("uuid"),
                            location['name']['en'],
                            location.get("pcode"),
                            location.get("parent_id"),
                            ".".join(location.get("lineage")),
                            location.get("status"),
                            location.get("geometry_type"),
                            groups,
                            location.get("location_type", {}).get('en', 'Uknown'),
                        ] + [loc_parents.get(x, '') for x in loc_types])

    file_uuid = str(uuid.uuid4())
    file_name = "export - %s- %s - %s" % (
        user.get("name"),
        file_uuid,
        "locations"
    )

    output = write_tmp_file(rows, file_name, "csv")

    return dict(fn=output)

