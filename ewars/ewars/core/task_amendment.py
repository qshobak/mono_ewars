import datetime
import json

from psycopg2.extensions import AsIs

from ewars.db import get_db_cur
from ewars.constants import CONSTANTS

from ewars.core import notifications, user_lookup
from ewars.utils import date_utils
from ewars.utils import diff
from ewars.utils import form_utils

import tornado
import tornado.ioloop


def get_diff(original, amended):
    """ Get a track of what was changed

    Args:
        original: The original report in the system
        amended: The newly amended report

    Returns:
        A dict of keys containing tuples of (original value, amended value)
    """
    changes = {
        "data": {}
    }

    # Check root items
    if original.get("data_date") != amended.get("data_date"):
        changes["data_date"] = (original.get("data_date"), amended.get("data_date"))

    if original.get("location_id") != amended.get("location_id"):
        changes['location_id'] = (original.get("location_id"), amended.get("location_id"))

    for key, value in original['data'].items():
        if key in amended:
            if amended['data'][key] != value:
                changes['data'][key] = (value, amended['data'][key])
        else:
            changes['data'][key] = (original['data'][key], None)

    for key, value in amended['data'].items():
        if key not in original['data'].keys():
            changes['data'][key] = (None, amended['data'][key])

    return changes


class AmendmentTask:
    def __init__(self, id, user, task):
        self.id = id
        self.user = user
        self.action_taken = None
        self.task = task

    async def action(self, action, data):
        self.action_taken = action
        if action == CONSTANTS.APPROVE:
            await self._approve(data)
        elif action == CONSTANTS.REJECT:
            await self._reject(data)

        return True

    async def _approve(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.collections
                    SET data = %s,
                        status = 'SUBMITTED'
                    WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                json.dumps(self.task['data']['amended']),
                self.task['data']['report_uuid'],
            ))

        location_name = None
        if self.task.get("data", None) is not None:
            if self.task.get('data').get("location_name") is not None:
                location_name = self.task.get('data').get('location_name').get("en", None)


        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            "AMENDMENT_APPROVED",
            self.task['data']['user_id'],
            dict(
                form_name=self.task['data']['form_name']['en'],
                report_date=date_utils.display(self.task['data']['report_date'],
                                               self.task['data']['form_interval']),
                location_name=location_name,
                approver=self.user['name'],
                approver_email=self.user['email'],
            ),
            account=self.user.get("account_name", None),
            user=self.user
        )

        await self._close()

    async def _reject(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.collections
                    SET status = 'SUBMITTED'
                    WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['report_uuid'],
            ))

        location_name = None
        if self.task.get("data", None) is not None:
            if self.task.get('data').get("location_name") is not None:
                location_name = self.task.get('data').get('location_name').get("en", None)

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            "AMENDMENT_REJECTED",
            self.task['data']['user_id'],
            dict(
                form_name=self.task['data']['form_name']['en'],
                report_date=date_utils.display(self.task['data']['report_date'],
                                               self.task['data']['form_interval']),
                location_name=location_name,
                approver=self.user['name'],
                approver_email=self.user['email'],
                reason=data.get('reason')
            ),
            account=self.user.get('account_name', None),
            user=self.user
        )

        await self._close()

    async def _close(self):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.tasks_v2
                    SET state = 'CLOSED',
                        action_taken = %s,
                        actioned_by = %s,
                        actioned_date = %s
                    WHERE id = %s;
            """, (
                AsIs(self.user.get("tki")),
                self.action_taken,
                self.user['id'],
                datetime.datetime.now(),
                self.id,
            ))


class AmendmentRequest:
    """
    Represents an amendment within the system
    """

    def __init__(self, report_uuid, user, amended_data, reason):
        self.report_uuid = report_uuid
        self.user = user
        self.amendment = amended_data
        self.reason = reason

    async def _amend(self, amended):
        """
        Amend the report in question
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.collections
                SET data = %s,
                    last_modified = %s
                WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                json.dumps(amended),
                datetime.datetime.now(),
                self.report_uuid,
            ))

    async def process(self):
        """
        Process the amendment request
        :return:
        """
        can_amend = False
        if self.user.get('role') == 'ACCOUNT_ADMIN':
            can_amend = True

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.uuid,
                    c.location_id,
                    c.data_date,
                    c.data,
                    f.name AS form_name,
                    f.time_interval AS form_interval,
                    l.lineage AS lineage,
                    l.name AS location_name,
                    fv.definition AS definition
                FROM %s.collections AS c
                    LEFT JOIN %s.forms AS f ON f.id = c.form_id
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                    LEFT JOIN %s.form_versions AS fv ON fv.uuid = c.form_version_id
                WHERE c.uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                AsIs(self.user.get("tki")),
                AsIs(self.user.get("tki")),
                AsIs(self.user.get("tki")),
                self.report_uuid,
            ))
            self.report = await cur.fetchone()

        difference = diff.diff(self.report.get("data"), self.amendment.get("data"))
        amended = diff.update(self.report.get("data"), self.amendment.get("data"))

        if can_amend:
            self._amend(amended)
        else:
            amend_list = []

            for key, value in difference.items():
                list_item = dict()
                list_item['long_name'] = key
                list_item['long_name'] = form_utils.get_field_full_name(self.report.get("definition"), key)
                list_item['original'] = value.get("original")
                list_item['amended'] = value.get("amended")
                field_details = form_utils.get_field_details(self.report.get("definition"), key)
                if field_details is not None:
                    list_item['field_name'] = field_details.get("label", None)
                else:
                    print("NOT FOUND", key)

                amend_list.append(list_item)

            task_data = dict(
                form_name=self.report.get("form_name"),
                location_name=self.report.get("location_name", None),
                report_date=self.report.get("data_date").strftime("%Y-%m-%d"),
                form_interval=self.report.get("form_interval"),
                reason=self.reason,
                user_id=self.user['id'],
                amender_name=self.user['name'],
                amender_email=self.user['email'],
                amend_list=amend_list,
                amended=amended,
                report_uuid=self.report_uuid
            )

            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.collections
                    SET status = 'PENDING_AMENDMENT'
                    WHERE uuid = %s
                """, (
                    AsIs(self.user.get("tki")),
                    self.report_uuid,
                ))

                await cur.execute("""
                    INSERT INTO %s.tasks_v2 (task_type, assigned_user_id, assigned_user_types, data, location_id, state, priority, created_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(self.user.get("tki")),
                    'AMENDMENT_REQUEST',
                    None,
                    [CONSTANTS.ACCOUNT_ADMIN],
                    json.dumps(task_data),
                    self.report.get("location_id"),
                    'OPEN',
                    'LOW',
                    self.user['id'],
                ))

            recipients = await user_lookup.get_account_admins(user=self.user)

            loop = tornado.ioloop.IOLoop.current()
            loop.spawn_callback(
                notifications.send_bulk,
                'AMENDMENT_REQUEST',
                recipients,
                dict(
                    form_name=self.report['form_name']['en'],
                    requestor=self.user['name'],
                    requestor_email=self.user['email'],
                    location_name=self.report.get("location_name", {}).get("en", None),
                    report_date=date_utils.display(self.report['data_date'], self.report['form_interval']),
                    reason=self.reason
                ),
                user=self.user
            )
