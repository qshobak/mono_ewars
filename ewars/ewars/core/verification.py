import json
import uuid
import datetime
import asyncio

from ewars.db import get_db_cur
from ewars.core import notifications
from ewars.core.tasks import create_task

from psycopg2.extensions import AsIs


async def verify_user(verification_code):
    """ Verify a users email address and kick off approval task

    Args:
        verification_code (str): The verification code provided to the user

    Returns:

    """
    user = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.users
            WHERE api_token = %s
        """, (
            verification_code.split("_")[0],
        ))
        user = await cur.fetchone()

    if user is None:
        return False

    if user.get("status") == "PENDING_APPROVAL":
        return False

    if user.get("status") == "ACTIVE":
        return False

    if user.get("status") == "INACTIVE":
        return False

    if user is None:
        return False

    tki = "_%s" % (verification_code.split("_")[2])

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.users
            SET status = 'ACTIVE'
            WHERE id = %s;
        """, (
            user.get("id"),
        ))

        await cur.execute("""
            UPDATE %s.accounts
            SET status = 'PENDING_APPROVAL'
            WHERE user_id = %s
        """, (
            AsIs(tki),
            user.get("id"),
        ))

        await cur.execute("""
            SELECT name from _iw.accounts
            WHERE tki = %s;
        """, (
            tki,
        ))
        account = await cur.fetchone()

    # Set up verification process
    result = await create_task("REGISTRATION", user, "_%s" % (verification_code.split("_")[2],), account.get('name'))

    return True


async def send_email_verification(target, tki, acc_name):
    """ Send email verification... email

    Args:
        target (obj): The target user
        tki (str): The account tki
        acc_name (str): The name of the account

    Returns:

    """
    loop = asyncio.get_event_loop()
    loop.create_task(
        notifications.send(
            'EMAIL_VERIFICATION',
            target,
            data=dict(
                verification_code=target.get('api_token'),
                tki=tki,
            ),
            user=dict(tki=tki),
            account=acc_name,
            system=True
        )
    )
