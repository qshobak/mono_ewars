import os
import json
import functools
from ewars.conf import settings

CLIENTS = {}

BUNDLE_PATH = os.path.realpath(os.path.join(os.path.abspath(os.path.realpath(__file__)), "../../conf/apps"))


def load_clients(user_type):
    """Retrieve available client applications

    Args:
        user_type: The type of user that's requesting the clients

    Returns:
        A list of available client applications
    """
    clients = []
    ender = ".json"

    if settings.DEBUG:
        ender = "-dev.json"

    for root, dirs, files in os.walk(BUNDLE_PATH):
        for file in files:
            if file.endswith(ender):
                with open(os.path.join(root, file)) as f:
                    client = f.read()
                    client_data = json.loads(client)

                    if "access" in client_data.keys():
                        if user_type in client_data.get("access"):
                            clients.append(client_data)
                    else:
                        clients.append(client_data)

    return clients


def get_client(client_id):
    """Retrieve a client by it's id

    Args:
        client_id: The string id of the client

    Returns:
        The configuration object for the client
    """
    cl_id = client_id
    if settings.DEBUG:
        cl_id = cl_id + "-dev"

    client = None
    for root, dirs, files in os.walk(BUNDLE_PATH):
        for file in files:
            if file.startswith("%s." % cl_id):
                with open(os.path.join(root, file)) as f:
                    data = f.read()
                    client = json.loads(data)

    if client is not None:
        if "public" not in client.keys():
            client['public'] = False

    return client


def can_access(user):
    """Check whether a user can access this client application

    Args:
        user: The user to check

    Returns:
        True|False whether the user can access it
    """
    return True


def get_admin_client(client_name):
    """Return an admin client application config

    Args:
        client_name: The name of the client

    Returns:
        A dict with the clients settings
    """
    return None
