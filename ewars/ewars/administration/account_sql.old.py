ACCOUNT_SQL = """
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.9

-- Started on 2018-11-16 08:29:28 GMT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 16409)
-- Name: __SCHEMA__; Type: SCHEMA; Schema: -; Owner: ewars
--

CREATE SCHEMA __SCHEMA__;


ALTER SCHEMA __SCHEMA__ OWNER TO ewars;

--
-- TOC entry 4158 (class 1255 OID 184637)
-- Name: fn_on_collection_delete(); Type: FUNCTION; Schema: __SCHEMA__; Owner: ewars
--

CREATE FUNCTION __SCHEMA__.fn_on_collection_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
          IF (OLD.status = 'SUBMITTED')
          THEN
            IF (OLD.location_id IS NOT NULL)
            THEN
              -- Update based on full index
              UPDATE __SCHEMA__.collection_counts
              SET count = collection_counts.count - 1
              WHERE location_id = OLD.location_id
                    AND data_date = OLD.data_date
                    AND form_id = OLD.form_id;
            ELSE
              -- UPDATE based on partial index
              UPDATE __SCHEMA__.collection_counts
              SET count = collection_counts.count - 1
              WHERE data_date = OLD.data_date
                    AND form_id = OLD.form_id;
            END IF;
          END IF;
          RETURN NULL;
        END;
        $$;


ALTER FUNCTION __SCHEMA__.fn_on_collection_delete() OWNER TO ewars;

--
-- TOC entry 4157 (class 1255 OID 184635)
-- Name: fn_on_collection_insert(); Type: FUNCTION; Schema: __SCHEMA__; Owner: ewars
--

CREATE FUNCTION __SCHEMA__.fn_on_collection_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (NEW.status = 'SUBMITTED')
  THEN
    IF (NEW.location_id IS NOT NULL)
    THEN
      INSERT INTO __SCHEMA__.collection_counts (data_date, count, location_id, form_id)
      VALUES (NEW.data_date, 1, NEW.location_id, NEW.form_id)
      ON CONFLICT (data_date, location_id, form_id)
        DO UPDATE SET count = collection_counts.count + 1;
    ELSE
      INSERT INTO __SCHEMA__.collection_counts (data_date, count, form_id)
      VALUES (NEW.data_date, 1, NEW.form_id)
      ON CONFLICT (data_date, form_id) WHERE location_id IS NULL
        DO UPDATE SET count = collection_counts.count + 1;
    END IF;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION __SCHEMA__.fn_on_collection_insert() OWNER TO ewars;

--
-- TOC entry 4156 (class 1255 OID 184633)
-- Name: fn_on_collection_update(); Type: FUNCTION; Schema: __SCHEMA__; Owner: ewars
--

CREATE FUNCTION __SCHEMA__.fn_on_collection_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                    BEGIN
                      IF NEW.status = 'SUBMITTED' AND OLD.status = 'DRAFT'
                      THEN
                        IF (NEW.location_id IS NOT NULL)
                        THEN
                          -- Insert on conflict with full index
                          INSERT INTO __SCHEMA__.collection_counts (data_date, count, location_id, form_id)
                          VALUES (NEW.data_date, 1, NEW.location_id, NEW.form_id)
                          ON CONFLICT (data_date, location_id, form_id)
                            DO UPDATE
                              SET count = collection_counts.count + 1;
                        ELSE
                          -- Insert on conflict with data_date, form_id
                          INSERT INTO __SCHEMA__.collection_counts (data_date, count, form_id)
                          VALUES (NEW.data_date, 1, NEW.form_id)
                          ON CONFLICT (data_date, form_id) WHERE location_id IS NULL
                            DO UPDATE
                              SET count = collection_counts.count + 1;
                        END IF;
                      ELSIF (NEW.status = 'DELETED' AND OLD.status = 'SUBMITTED')
                        THEN
                          IF (NEW.location_id IS NOT NULL)
                          THEN
                            -- update based on full index
                            UPDATE __SCHEMA__.collection_counts
                            SET count = count - 1
                            WHERE location_id = OLD.location_id
                                  AND form_id = OLD.form_id
                                  AND data_date = OLD.data_date;
                          ELSE
                            -- update based on partial index
                            UPDATE __SCHEMA__.collection_counts
                            SET count = count - 1
                            WHERE data_date = OLD.data_date
                                  AND form_id = OLD.form_id;
                          END IF;
                      END IF;

                      IF ((NEW.location_id != OLD.location_id OR NEW.data_date != OLD.data_date) AND
                          (NEW.status = 'SUBMITTED' AND OLD.status = 'SUBMITTED'))
                      THEN
                        IF (NEW.location_id IS NOT NULL)
                        THEN
                          -- reallocate counter based on full index
                          UPDATE __SCHEMA__.collection_counts
                          SET count = collection_counts.count - 1
                          WHERE location_id = OLD.location_id
                                AND form_id = OLD.form_id
                                AND data_date = OLD.data_date;

                          INSERT INTO __SCHEMA__.collection_counts (data_date, count, location_id, form_id)
                          VALUES (NEW.data_date, 1, NEW.location_id, NEW.form_id)
                          ON CONFLICT (data_date, location_id, form_id)
                            DO UPDATE
                              SET count = collection_counts.count + 1;
                        ELSE
                          -- reallocate counter based on partial index
                          UPDATE __SCHEMA__.collection_counts
                          SET count = count - 1
                          WHERE data_date = OLD.data_date
                                AND form_id = OLD.form_id;

                          INSERT INTO __SCHEMA__.collection_counts (data_date, count, form_id)
                          VALUES (NEW.data_date, 1, NEW.form_id)
                          ON CONFLICT (data_date, form_id) WHERE location_id IS NULL
                            DO UPDATE
                              SET count = collection_counts.count + 1;
                        END IF;

                      END IF;

                      RETURN NULL;
                    END;
                    $$;


ALTER FUNCTION __SCHEMA__.fn_on_collection_update() OWNER TO ewars;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1437 (class 1259 OID 28046)
-- Name: accounts; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.accounts (
    user_id integer NOT NULL,
    aid integer NOT NULL,
    status text DEFAULT 'PENDING'::text NOT NULL,
    role text DEFAULT 'PENDING'::text NOT NULL,
    location_id uuid,
    lab_id uuid,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    registered timestamp without time zone DEFAULT now() NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE __SCHEMA__.accounts OWNER TO ewars;

--
-- TOC entry 1438 (class 1259 OID 28058)
-- Name: activity_feed; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.activity_feed (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    event_type text DEFAULT 'GENERAL'::text NOT NULL,
    triggered_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    icon text DEFAULT 'fa-bullhorn'::text NOT NULL,
    likes integer DEFAULT 0 NOT NULL,
    attachments jsonb,
    content text
);


ALTER TABLE __SCHEMA__.activity_feed OWNER TO ewars;

--
-- TOC entry 1439 (class 1259 OID 28091)
-- Name: alarms_v2; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.alarms_v2 (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text DEFAULT 'New Alarm'::text NOT NULL,
    description text,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    modified_by integer NOT NULL,
    loc_restrict boolean DEFAULT false NOT NULL,
    loc_spec text,
    loc_id uuid,
    loc_groups text[],
    sti_restrict boolean DEFAULT false NOT NULL,
    sti integer,
    ds_interval_type text DEFAULT 'INTERVAL'::text NOT NULL,
    ds_interval text DEFAULT 'DAY'::text NOT NULL,
    ds_type text DEFAULT 'INDICATOR'::text NOT NULL,
    ds_indicator text DEFAULT ''::text NOT NULL,
    ds_agg_lower boolean DEFAULT false NOT NULL,
    ds_aggregate text DEFAULT 'SUM'::text,
    ds_modifier numeric,
    crit_comparator text DEFAULT 'GTE'::text NOT NULL,
    crit_source text DEFAULT 'STATIC'::text NOT NULL,
    crit_sd_spec text,
    crit_sd_intervals numeric,
    crit_ed_spec text,
    crit_ed_intervals numeric,
    crit_indicator text,
    crit_formula text,
    crit_series jsonb,
    crit_reduction text DEFAULT 'SUM'::text NOT NULL,
    crit_floor numeric,
    crit_modifier numeric,
    crit_value numeric,
    ad_enabled boolean DEFAULT false NOT NULL,
    ad_interval text,
    ad_period numeric,
    inv_enabled boolean DEFAULT false NOT NULL,
    inv_form_ids integer[],
    eid_prefix text,
    monitor_type text DEFAULT 'AGGREGATE'::text NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    sti_agg_rollup boolean DEFAULT false NOT NULL
);


ALTER TABLE __SCHEMA__.alarms_v2 OWNER TO ewars;

--
-- TOC entry 1440 (class 1259 OID 28115)
-- Name: alert_actions; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.alert_actions (
    id integer NOT NULL,
    alert_id uuid NOT NULL,
    type text NOT NULL,
    user_id integer NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    data json DEFAULT '{}'::json,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    submitted_date timestamp without time zone
);


ALTER TABLE __SCHEMA__.alert_actions OWNER TO ewars;

--
-- TOC entry 1441 (class 1259 OID 28125)
-- Name: alert_actions_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.alert_actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.alert_actions_id_seq OWNER TO ewars;

--
-- TOC entry 14444 (class 0 OID 0)
-- Dependencies: 1441
-- Name: alert_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.alert_actions_id_seq OWNED BY __SCHEMA__.alert_actions.id;


--
-- TOC entry 1442 (class 1259 OID 28127)
-- Name: alert_events; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.alert_events (
    id integer NOT NULL,
    action_date timestamp without time zone DEFAULT now() NOT NULL,
    action_type text,
    icon text DEFAULT 'fa-exclamation-circle'::text NOT NULL,
    node_type text DEFAULT 'ACTION'::text NOT NULL,
    user_id integer,
    content text,
    alert_id uuid NOT NULL
);


ALTER TABLE __SCHEMA__.alert_events OWNER TO ewars;

--
-- TOC entry 1443 (class 1259 OID 28136)
-- Name: alert_events_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.alert_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.alert_events_id_seq OWNER TO ewars;

--
-- TOC entry 14445 (class 0 OID 0)
-- Dependencies: 1443
-- Name: alert_events_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.alert_events_id_seq OWNED BY __SCHEMA__.alert_events.id;


--
-- TOC entry 1444 (class 1259 OID 28138)
-- Name: alert_users; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.alert_users (
    id integer NOT NULL,
    alert_id uuid NOT NULL,
    user_id integer NOT NULL,
    date_added timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.alert_users OWNER TO ewars;

--
-- TOC entry 1445 (class 1259 OID 28142)
-- Name: alert_users_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.alert_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.alert_users_id_seq OWNER TO ewars;

--
-- TOC entry 14446 (class 0 OID 0)
-- Dependencies: 1445
-- Name: alert_users_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.alert_users_id_seq OWNED BY __SCHEMA__.alert_users.id;


--
-- TOC entry 1446 (class 1259 OID 28144)
-- Name: alerts; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.alerts (
    uuid uuid NOT NULL,
    alarm_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    state text DEFAULT 'OK'::text NOT NULL,
    indicator_id uuid,
    location_id uuid,
    no_trigger_interval text,
    no_trigger_period numeric,
    trigger_start date DEFAULT now() NOT NULL,
    trigger_end date DEFAULT now() NOT NULL,
    triggering_user integer,
    action_taken text,
    actioned_date date,
    actioned_by integer,
    action_reason text,
    source text DEFAULT 'SYSTEM'::text NOT NULL,
    triggering_report_id uuid,
    indicator_definition json,
    investigation_id uuid,
    stage text DEFAULT 'VERIFICATION'::text NOT NULL,
    stage_state text DEFAULT 'COMPLETE'::text,
    outcome text,
    risk text,
    eid text,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.alerts OWNER TO ewars;

--
-- TOC entry 14447 (class 0 OID 0)
-- Dependencies: 1446
-- Name: COLUMN alerts.created; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.alerts.created IS 'This is assumed to be the first date the alert was in NOT OK state';


--
-- TOC entry 1447 (class 1259 OID 28157)
-- Name: indicators; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.indicators (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    group_id integer NOT NULL,
    name json DEFAULT '{"en": "New Indicator"}'::json NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    protected boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    ranked boolean DEFAULT false,
    colour text,
    itype text DEFAULT 'DEFAULT'::text,
    definition json,
    description json DEFAULT '{}'::json,
    guidance json DEFAULT '{}'::json,
    accounts integer[],
    icode text,
    hid uuid,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.indicators OWNER TO ewars;

--
-- TOC entry 1448 (class 1259 OID 28173)
-- Name: location_types; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.location_types (
    id integer NOT NULL,
    name json DEFAULT '{}'::json,
    description text,
    status text DEFAULT 'ACTIVE'::text,
    created timestamp without time zone DEFAULT now(),
    created_by integer DEFAULT 3 NOT NULL,
    hid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.location_types OWNER TO ewars;

--
-- TOC entry 1449 (class 1259 OID 28184)
-- Name: locations; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.locations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json,
    location_type text DEFAULT 'ADMIN'::text NOT NULL,
    pcode text,
    description text DEFAULT ''::text,
    parent_id uuid,
    lineage text[] NOT NULL,
    data json,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    geometry public.geometry(MultiPolygon,3857),
    point public.geometry(Point,3857),
    geometry_type text DEFAULT 'ADMIN'::text NOT NULL,
    site_type_id integer DEFAULT 1,
    default_center public.geometry(Point,3857),
    default_zoom integer DEFAULT 10,
    groups text[],
    created_date date DEFAULT now() NOT NULL,
    geojson jsonb,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.locations OWNER TO ewars;

--
-- TOC entry 1450 (class 1259 OID 28199)
-- Name: alerts_full; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.alerts_full AS
 SELECT a.uuid,
    a.alarm_id,
    a.created,
    a.state,
    a.indicator_id,
    a.location_id,
    a.no_trigger_interval,
    a.no_trigger_period,
    a.trigger_start,
    a.trigger_end,
    a.triggering_user,
    a.action_taken,
    a.actioned_date,
    a.actioned_by,
    a.action_reason,
    a.source,
    a.triggering_report_id,
    a.indicator_definition,
    a.investigation_id,
    a.stage,
    a.stage_state,
    a.outcome,
    a.risk,
    a.eid,
    m.name AS alarm_name,
    m.ds_interval_type,
    m.ds_interval,
    m.ds_interval AS interval_type,
    l.name AS location_name,
    l.site_type_id,
    lt.name AS st_name,
    l.lineage
   FROM ((((__SCHEMA__.alerts a
     LEFT JOIN __SCHEMA__.alarms_v2 m ON ((m.uuid = a.alarm_id)))
     LEFT JOIN __SCHEMA__.indicators i ON (((i.uuid)::text = m.ds_indicator)))
     LEFT JOIN __SCHEMA__.locations l ON ((l.uuid = a.location_id)))
     LEFT JOIN __SCHEMA__.location_types lt ON ((lt.id = l.site_type_id)));


ALTER TABLE __SCHEMA__.alerts_full OWNER TO ewars;

--
-- TOC entry 1451 (class 1259 OID 28204)
-- Name: amendments; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.amendments (
    id integer NOT NULL,
    report_id uuid,
    original json,
    amended json,
    amended_date timestamp without time zone DEFAULT now() NOT NULL,
    amendment_reason text,
    status text DEFAULT 'PENDING'::text NOT NULL,
    form_id integer NOT NULL,
    amended_by integer NOT NULL
);


ALTER TABLE __SCHEMA__.amendments OWNER TO ewars;

--
-- TOC entry 1452 (class 1259 OID 28212)
-- Name: amendments_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.amendments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.amendments_id_seq OWNER TO ewars;

--
-- TOC entry 14448 (class 0 OID 0)
-- Dependencies: 1452
-- Name: amendments_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.amendments_id_seq OWNED BY __SCHEMA__.amendments.id;


--
-- TOC entry 1453 (class 1259 OID 28214)
-- Name: analysis_notebooks; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.analysis_notebooks (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json NOT NULL,
    description text NOT NULL,
    public boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.analysis_notebooks OWNER TO ewars;

--
-- TOC entry 1454 (class 1259 OID 28227)
-- Name: assignments; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.assignments (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'UNAPPROVED'::text NOT NULL,
    location_id uuid,
    form_id integer,
    start_date date,
    end_date date,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    type text DEFAULT 'DEFAULT'::text NOT NULL
);


ALTER TABLE __SCHEMA__.assignments OWNER TO ewars;

--
-- TOC entry 2777 (class 1259 OID 524752)
-- Name: channel_items; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.channel_items (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    chid uuid NOT NULL,
    item_type text DEFAULT 'M'::text NOT NULL,
    content jsonb DEFAULT '{}'::jsonb NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    ts_ms numeric NOT NULL,
    uid integer NOT NULL
);


ALTER TABLE __SCHEMA__.channel_items OWNER TO ewars;

--
-- TOC entry 2776 (class 1259 OID 524737)
-- Name: channels; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.channels (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text DEFAULT 'New Channel'::text NOT NULL,
    description text DEFAULT 'This is the new channel'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    users integer[],
    modified timestamp without time zone DEFAULT now() NOT NULL,
    private boolean DEFAULT false NOT NULL,
    admins integer[],
    permissions jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE __SCHEMA__.channels OWNER TO ewars;

--
-- TOC entry 1455 (class 1259 OID 28238)
-- Name: collection_comments; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.collection_comments (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    collection_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    edited boolean DEFAULT false NOT NULL,
    content text NOT NULL,
    meta jsonb
);


ALTER TABLE __SCHEMA__.collection_comments OWNER TO ewars;

--
-- TOC entry 2275 (class 1259 OID 184627)
-- Name: collection_counts; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.collection_counts (
    data_date date NOT NULL,
    count integer DEFAULT 0 NOT NULL,
    location_id uuid,
    form_id integer NOT NULL
);


ALTER TABLE __SCHEMA__.collection_counts OWNER TO ewars;

--
-- TOC entry 1456 (class 1259 OID 28247)
-- Name: collections; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.collections (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    submitted_date timestamp without time zone,
    form_id integer NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    data jsonb DEFAULT '{}'::json NOT NULL,
    form_version_id uuid NOT NULL,
    data_date date DEFAULT now(),
    location_id uuid,
    import_set uuid,
    import_data json DEFAULT '{}'::json,
    source text DEFAULT 'SYSTEM'::text,
    history json DEFAULT '[]'::json NOT NULL,
    attachments json,
    revisions json,
    alert_id uuid,
    submitted timestamp without time zone DEFAULT now() NOT NULL,
    eid text
);


ALTER TABLE __SCHEMA__.collections OWNER TO ewars;

--
-- TOC entry 1457 (class 1259 OID 28266)
-- Name: completeness; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.completeness AS
 SELECT c.data_date,
    count(*) AS total,
    c.location_id,
    c.form_id
   FROM __SCHEMA__.collections c
  WHERE ((c.status = 'SUBMITTED'::text) OR (c.status = 'PENDING_AMENDMENT'::text))
  GROUP BY c.data_date, c.location_id, c.form_id;


ALTER TABLE __SCHEMA__.completeness OWNER TO ewars;

--
-- TOC entry 1458 (class 1259 OID 28270)
-- Name: conf; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.conf (
    id integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    modified_by integer NOT NULL,
    grp text DEFAULT 'NONE'::text NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.conf OWNER TO ewars;

--
-- TOC entry 1459 (class 1259 OID 28278)
-- Name: conf_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.conf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.conf_id_seq OWNER TO ewars;

--
-- TOC entry 14449 (class 0 OID 0)
-- Dependencies: 1459
-- Name: conf_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.conf_id_seq OWNED BY __SCHEMA__.conf.id;


--
-- TOC entry 1460 (class 1259 OID 28280)
-- Name: data_sets; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.data_sets (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text DEFAULT 'New Data set'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    columns jsonb DEFAULT '{}'::jsonb NOT NULL,
    mappings jsonb DEFAULT '{}'::jsonb NOT NULL,
    date_field text
);


ALTER TABLE __SCHEMA__.data_sets OWNER TO ewars;

--
-- TOC entry 1461 (class 1259 OID 28292)
-- Name: data_sets_data; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.data_sets_data (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    dsid uuid NOT NULL,
    data_date timestamp without time zone DEFAULT now(),
    data jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE __SCHEMA__.data_sets_data OWNER TO ewars;

--
-- TOC entry 1462 (class 1259 OID 28301)
-- Name: data_sources; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.data_sources (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    driver text DEFAULT 'postgres'::text NOT NULL,
    db_host text,
    db_name text,
    db_user text,
    db_pass text,
    db_port integer,
    source_table text,
    schemata jsonb,
    schemata_indeces jsonb,
    last_called timestamp without time zone,
    cache text DEFAULT 'NO_CACHE'::text NOT NULL
);


ALTER TABLE __SCHEMA__.data_sources OWNER TO ewars;

--
-- TOC entry 1463 (class 1259 OID 28312)
-- Name: datasets; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.datasets (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    description text NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL
);


ALTER TABLE __SCHEMA__.datasets OWNER TO ewars;

--
-- TOC entry 1464 (class 1259 OID 28328)
-- Name: devices; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.devices (
    id integer NOT NULL,
    device_type text DEFAULT 'UNKNOWN'::text,
    device_id text NOT NULL,
    device_os_version text,
    device_lat text,
    device_lng text,
    device_number text,
    gate_id integer,
    user_id integer,
    last_seen timestamp without time zone DEFAULT now() NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    app_version text,
    app_version_name text,
    android_version text,
    device_name text,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.devices OWNER TO ewars;

--
-- TOC entry 14450 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.device_type; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.device_type IS 'The OS type of the device ANDROID|IOS|WINDOWS';


--
-- TOC entry 14451 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.device_id; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.device_id IS 'A unique device ID (i.e. the IMEI)';


--
-- TOC entry 14452 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.device_os_version; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.device_os_version IS 'The version of the phone operating system that is being run';


--
-- TOC entry 14453 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.gate_id; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.gate_id IS 'The id of the SMS gateway that this phone should use';


--
-- TOC entry 14454 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.user_id; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.user_id IS 'The is of the user that this phone is assigned to';


--
-- TOC entry 14455 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.last_seen; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.last_seen IS 'The last time that this phone connected with EWARS';


--
-- TOC entry 14456 (class 0 OID 0)
-- Dependencies: 1464
-- Name: COLUMN devices.status; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.devices.status IS 'The status of the phone ACTIVE|INACTIVE|BLOCKED';


--
-- TOC entry 1465 (class 1259 OID 28337)
-- Name: devices_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.devices_id_seq OWNER TO ewars;

--
-- TOC entry 14457 (class 0 OID 0)
-- Dependencies: 1465
-- Name: devices_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.devices_id_seq OWNED BY __SCHEMA__.devices.id;


--
-- TOC entry 1466 (class 1259 OID 28339)
-- Name: discussion_thread_messages; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.discussion_thread_messages (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id integer NOT NULL,
    discussion_id integer NOT NULL,
    content text NOT NULL,
    created timestamp without time zone DEFAULT now()
);


ALTER TABLE __SCHEMA__.discussion_thread_messages OWNER TO ewars;

--
-- TOC entry 1467 (class 1259 OID 28347)
-- Name: discussion_threads; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.discussion_threads (
    id integer NOT NULL,
    node_type text NOT NULL,
    node_id text NOT NULL,
    email boolean DEFAULT false,
    sms boolean DEFAULT false
);


ALTER TABLE __SCHEMA__.discussion_threads OWNER TO ewars;

--
-- TOC entry 1468 (class 1259 OID 28355)
-- Name: discussions_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.discussions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.discussions_id_seq OWNER TO ewars;

--
-- TOC entry 14458 (class 0 OID 0)
-- Dependencies: 1468
-- Name: discussions_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.discussions_id_seq OWNED BY __SCHEMA__.discussion_threads.id;


--
-- TOC entry 1469 (class 1259 OID 28357)
-- Name: distributions; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.distributions (
    id integer NOT NULL,
    name text NOT NULL,
    data jsonb DEFAULT '[]'::jsonb NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL
);


ALTER TABLE __SCHEMA__.distributions OWNER TO ewars;

--
-- TOC entry 1470 (class 1259 OID 28366)
-- Name: distributions_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.distributions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.distributions_id_seq OWNER TO ewars;

--
-- TOC entry 14459 (class 0 OID 0)
-- Dependencies: 1470
-- Name: distributions_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.distributions_id_seq OWNED BY __SCHEMA__.distributions.id;


--
-- TOC entry 2296 (class 1259 OID 187930)
-- Name: event_log; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.event_log (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    event_type text NOT NULL,
    ts integer DEFAULT (date_part('epoch'::text, now()))::integer NOT NULL,
    data jsonb DEFAULT '{}'::jsonb NOT NULL,
    origin_uid integer,
    origin_did text,
    metadata jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE __SCHEMA__.event_log OWNER TO ewars;

--
-- TOC entry 1471 (class 1259 OID 28368)
-- Name: event_types; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.event_types (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.event_types OWNER TO ewars;

--
-- TOC entry 1472 (class 1259 OID 28378)
-- Name: events; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.events (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    type_id uuid NOT NULL,
    title text NOT NULL,
    s_date timestamp without time zone DEFAULT now() NOT NULL,
    e_date timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    location_id uuid,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.events OWNER TO ewars;

--
-- TOC entry 1473 (class 1259 OID 28389)
-- Name: events_full; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.events_full AS
 SELECT e.uuid,
    e.type_id,
    e.title,
    e.s_date,
    e.e_date,
    e.description,
    e.location_id,
    e.created_by,
    e.created,
    e.last_modified,
    et.name AS type_name,
    u.name AS created_name,
    u.email AS created_email
   FROM ((__SCHEMA__.events e
     LEFT JOIN __SCHEMA__.event_types et ON ((et.uuid = e.type_id)))
     LEFT JOIN _iw.sso u ON ((u.id = e.created_by)));


ALTER TABLE __SCHEMA__.events_full OWNER TO ewars;

--
-- TOC entry 1474 (class 1259 OID 28394)
-- Name: feed_items; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.feed_items (
    id integer NOT NULL,
    title text NOT NULL,
    sef text NOT NULL,
    feeds integer[] NOT NULL,
    content text NOT NULL,
    content_type text DEFAULT 'MARKDOWN'::text NOT NULL,
    keys text[],
    status text DEFAULT 'DRAFT'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    publish_date timestamp without time zone DEFAULT now() NOT NULL,
    access text[]
);


ALTER TABLE __SCHEMA__.feed_items OWNER TO ewars;

--
-- TOC entry 1475 (class 1259 OID 28405)
-- Name: feed_items_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.feed_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.feed_items_id_seq OWNER TO ewars;

--
-- TOC entry 14460 (class 0 OID 0)
-- Dependencies: 1475
-- Name: feed_items_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.feed_items_id_seq OWNED BY __SCHEMA__.feed_items.id;


--
-- TOC entry 1476 (class 1259 OID 28407)
-- Name: feeds; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.feeds (
    id integer NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL
);


ALTER TABLE __SCHEMA__.feeds OWNER TO ewars;

--
-- TOC entry 1477 (class 1259 OID 28414)
-- Name: feeds_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.feeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.feeds_id_seq OWNER TO ewars;

--
-- TOC entry 14461 (class 0 OID 0)
-- Dependencies: 1477
-- Name: feeds_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.feeds_id_seq OWNED BY __SCHEMA__.feeds.id;


--
-- TOC entry 1478 (class 1259 OID 28416)
-- Name: form_versions; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.form_versions (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    form_id integer,
    version integer DEFAULT 1 NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    logic json DEFAULT '[]'::json NOT NULL,
    dia json DEFAULT '{}'::json,
    etl jsonb DEFAULT '[]'::jsonb NOT NULL,
    last_modified timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.form_versions OWNER TO ewars;

--
-- TOC entry 1479 (class 1259 OID 28429)
-- Name: forms; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.forms (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name json DEFAULT '{"en": ""}'::json NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    version_id uuid,
    location_aware boolean DEFAULT false NOT NULL,
    time_interval text DEFAULT 'NONE'::text NOT NULL,
    single_report_context boolean DEFAULT false,
    allowed_accounts text[],
    location_type text[],
    is_lab boolean DEFAULT false,
    block_future_dates boolean DEFAULT false,
    overdue_threshold numeric,
    overdue_interval text DEFAULT 'DAY'::text,
    guidance json DEFAULT '{}'::json,
    description json,
    ftype text DEFAULT 'PRIMARY'::text NOT NULL,
    site_type_id integer,
    features jsonb DEFAULT '{}'::jsonb NOT NULL,
    hid uuid,
    eid_prefix text,
    exportable boolean DEFAULT true,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.forms OWNER TO ewars;

--
-- TOC entry 1480 (class 1259 OID 28450)
-- Name: forms_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.forms_id_seq OWNER TO ewars;

--
-- TOC entry 14462 (class 0 OID 0)
-- Dependencies: 1480
-- Name: forms_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.forms_id_seq OWNED BY __SCHEMA__.forms.id;


--
-- TOC entry 1481 (class 1259 OID 28452)
-- Name: groups; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.groups (
    id integer NOT NULL,
    name text NOT NULL,
    public boolean DEFAULT true
);


ALTER TABLE __SCHEMA__.groups OWNER TO ewars;

--
-- TOC entry 1482 (class 1259 OID 28459)
-- Name: groups_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.groups_id_seq OWNER TO ewars;

--
-- TOC entry 14463 (class 0 OID 0)
-- Dependencies: 1482
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.groups_id_seq OWNED BY __SCHEMA__.groups.id;


--
-- TOC entry 1483 (class 1259 OID 28461)
-- Name: guidance; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.guidance (
    id integer NOT NULL,
    token text NOT NULL,
    title json DEFAULT '{}'::json NOT NULL,
    content json DEFAULT '{}'::json,
    active boolean DEFAULT false,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.guidance OWNER TO ewars;

--
-- TOC entry 1484 (class 1259 OID 28472)
-- Name: guidance_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.guidance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.guidance_id_seq OWNER TO ewars;

--
-- TOC entry 14464 (class 0 OID 0)
-- Dependencies: 1484
-- Name: guidance_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.guidance_id_seq OWNED BY __SCHEMA__.guidance.id;


--
-- TOC entry 2411 (class 1259 OID 404967)
-- Name: huds; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.huds (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text DEFAULT 'New HUD'::text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    settings jsonb DEFAULT '{}'::jsonb NOT NULL,
    description text DEFAULT 'No description'::text NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    lm_id integer NOT NULL,
    permissions jsonb DEFAULT '{}'::jsonb NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.huds OWNER TO ewars;

--
-- TOC entry 1485 (class 1259 OID 28474)
-- Name: import_project_data; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.import_project_data (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    project_id uuid NOT NULL,
    status text DEFAULT 'PENDING'::text NOT NULL,
    validity text DEFAULT 'BASE'::text NOT NULL,
    location_id text DEFAULT 'UNSET'::text NOT NULL,
    data_date text DEFAULT 'UNSET'::text NOT NULL,
    data jsonb DEFAULT '{}'::jsonb NOT NULL,
    collection_id uuid,
    data_set_item_id uuid,
    imported_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.import_project_data OWNER TO ewars;

--
-- TOC entry 1486 (class 1259 OID 28487)
-- Name: import_projects; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.import_projects (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    description text,
    status text DEFAULT 'DEAD'::text NOT NULL,
    target_type text DEFAULT 'FORM'::text NOT NULL,
    form_id integer,
    version_id uuid,
    data_set_id uuid,
    mapping jsonb DEFAULT '{}'::jsonb NOT NULL,
    meta_map jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    src_file text NOT NULL
);


ALTER TABLE __SCHEMA__.import_projects OWNER TO ewars;

--
-- TOC entry 1487 (class 1259 OID 28500)
-- Name: imports; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.imports (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INCOMPLETE'::text NOT NULL,
    form_id integer,
    mapping jsonb DEFAULT '{}'::jsonb NOT NULL,
    data jsonb DEFAULT '[]'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.imports OWNER TO ewars;

--
-- TOC entry 2727 (class 1259 OID 515617)
-- Name: inbound_events; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.inbound_events (
    eid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    device_id uuid,
    user_id integer,
    topic text NOT NULL,
    key text NOT NULL,
    value jsonb DEFAULT '{}'::jsonb NOT NULL,
    ts_ms integer NOT NULL,
    applied boolean DEFAULT false NOT NULL
);


ALTER TABLE __SCHEMA__.inbound_events OWNER TO ewars;

--
-- TOC entry 1488 (class 1259 OID 28512)
-- Name: indicator_groups; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.indicator_groups (
    id integer NOT NULL,
    name json NOT NULL,
    parent_id integer,
    hid uuid,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.indicator_groups OWNER TO ewars;

--
-- TOC entry 1489 (class 1259 OID 28518)
-- Name: indicator_groups_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.indicator_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.indicator_groups_id_seq OWNER TO ewars;

--
-- TOC entry 14465 (class 0 OID 0)
-- Dependencies: 1489
-- Name: indicator_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.indicator_groups_id_seq OWNED BY __SCHEMA__.indicator_groups.id;


--
-- TOC entry 1490 (class 1259 OID 28520)
-- Name: investigations; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.investigations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    opened timestamp without time zone DEFAULT now() NOT NULL,
    closed timestamp without time zone
);


ALTER TABLE __SCHEMA__.investigations OWNER TO ewars;

--
-- TOC entry 1491 (class 1259 OID 28525)
-- Name: invites; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.invites (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email text NOT NULL,
    user_id integer,
    details jsonb DEFAULT '{}'::jsonb NOT NULL,
    sent_date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.invites OWNER TO ewars;

--
-- TOC entry 1492 (class 1259 OID 28534)
-- Name: laboratories; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.laboratories (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    description text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.laboratories OWNER TO ewars;

--
-- TOC entry 1493 (class 1259 OID 28544)
-- Name: layouts; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.layouts (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    layout_type text DEFAULT 'DASHBOARD'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    definition json DEFAULT '{}'::json NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    modified_by integer NOT NULL,
    description json DEFAULT '{}'::json NOT NULL,
    access text[],
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    locked boolean DEFAULT false,
    color text,
    hid uuid,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.layouts OWNER TO ewars;

--
-- TOC entry 1494 (class 1259 OID 28566)
-- Name: location_reporting; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.location_reporting (
    id integer NOT NULL,
    location_id uuid NOT NULL,
    form_id integer NOT NULL,
    start_date date DEFAULT now() NOT NULL,
    end_date date,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    type text,
    submissions integer DEFAULT 0 NOT NULL,
    pid uuid,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.location_reporting OWNER TO ewars;

--
-- TOC entry 1495 (class 1259 OID 28576)
-- Name: location_reporting_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.location_reporting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.location_reporting_id_seq OWNER TO ewars;

--
-- TOC entry 14466 (class 0 OID 0)
-- Dependencies: 1495
-- Name: location_reporting_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.location_reporting_id_seq OWNED BY __SCHEMA__.location_reporting.id;


--
-- TOC entry 1496 (class 1259 OID 28578)
-- Name: location_types_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.location_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.location_types_id_seq OWNER TO ewars;

--
-- TOC entry 14467 (class 0 OID 0)
-- Dependencies: 1496
-- Name: location_types_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.location_types_id_seq OWNED BY __SCHEMA__.location_types.id;


--
-- TOC entry 2860 (class 1259 OID 530151)
-- Name: mapping; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.mapping (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    title text DEFAULT 'New map'::text NOT NULL,
    shared boolean DEFAULT false NOT NULL,
    description text,
    annotations jsonb DEFAULT '[]'::jsonb NOT NULL,
    layers jsonb DEFAULT '[]'::jsonb NOT NULL,
    config jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified_by integer,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.mapping OWNER TO ewars;

--
-- TOC entry 1497 (class 1259 OID 28580)
-- Name: maps; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.maps (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    locked boolean DEFAULT false,
    shared boolean DEFAULT false,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.maps OWNER TO ewars;

--
-- TOC entry 1498 (class 1259 OID 28592)
-- Name: messages; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.messages (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    recipient integer NOT NULL,
    lineage uuid[] NOT NULL,
    sender integer NOT NULL,
    subject text,
    thread json DEFAULT '[]'::json NOT NULL,
    sent timestamp without time zone DEFAULT now() NOT NULL,
    read boolean DEFAULT false NOT NULL,
    state text DEFAULT 'ON'::text NOT NULL,
    message_type text DEFAULT 'NOTIFICATION'::text NOT NULL
);


ALTER TABLE __SCHEMA__.messages OWNER TO ewars;

--
-- TOC entry 1499 (class 1259 OID 28604)
-- Name: news_categories; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.news_categories (
    id integer NOT NULL,
    name json DEFAULT '{}'::json NOT NULL
);


ALTER TABLE __SCHEMA__.news_categories OWNER TO ewars;

--
-- TOC entry 1500 (class 1259 OID 28611)
-- Name: news_categories_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.news_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.news_categories_id_seq OWNER TO ewars;

--
-- TOC entry 14468 (class 0 OID 0)
-- Dependencies: 1500
-- Name: news_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.news_categories_id_seq OWNED BY __SCHEMA__.news_categories.id;


--
-- TOC entry 1501 (class 1259 OID 28613)
-- Name: news_posts; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.news_posts (
    id integer NOT NULL,
    title text NOT NULL,
    slug text NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    excerpt text NOT NULL,
    content text NOT NULL,
    published_date timestamp without time zone DEFAULT now() NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    history json DEFAULT '{}'::json NOT NULL,
    category_id integer DEFAULT 1
);


ALTER TABLE __SCHEMA__.news_posts OWNER TO ewars;

--
-- TOC entry 1502 (class 1259 OID 28625)
-- Name: news_posts_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.news_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.news_posts_id_seq OWNER TO ewars;

--
-- TOC entry 14469 (class 0 OID 0)
-- Dependencies: 1502
-- Name: news_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.news_posts_id_seq OWNED BY __SCHEMA__.news_posts.id;


--
-- TOC entry 1503 (class 1259 OID 28627)
-- Name: notebooks; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.notebooks (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    shared boolean DEFAULT false,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.notebooks OWNER TO ewars;

--
-- TOC entry 1504 (class 1259 OID 28638)
-- Name: organizations; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.organizations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    status text DEFAULT 'ACTIVE'::text NOT NULL,
    name json DEFAULT '{}'::json,
    acronym text,
    site text,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    hid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.organizations OWNER TO ewars;

--
-- TOC entry 1505 (class 1259 OID 28650)
-- Name: orgs; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.orgs (
    uuid uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    added_by integer NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.orgs OWNER TO ewars;

--
-- TOC entry 1506 (class 1259 OID 28654)
-- Name: orgs_full; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.orgs_full AS
 SELECT o.uuid AS local_uuid,
    ro.uuid,
    ro.status,
    ro.name,
    ro.acronym,
    ro.site,
    ro.created_by,
    ro.created,
    ro.last_modified,
    ro.accounts,
    ro.hid
   FROM (__SCHEMA__.orgs o
     LEFT JOIN _iw.organizations ro ON ((ro.uuid = o.uuid)));


ALTER TABLE __SCHEMA__.orgs_full OWNER TO ewars;

--
-- TOC entry 1507 (class 1259 OID 28658)
-- Name: plots; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.plots (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    created_by integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    shared boolean DEFAULT false NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.plots OWNER TO ewars;

--
-- TOC entry 1508 (class 1259 OID 28669)
-- Name: reporting; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.reporting AS
 SELECT l.uuid,
    l.name,
    l.lineage,
    l.status AS location_status,
    l.site_type_id,
    l.parent_id,
    r.id,
    r.form_id,
    r.start_date,
    COALESCE(r.end_date, ('now'::text)::date) AS end_date,
    r.type,
    r.status
   FROM (__SCHEMA__.locations l
     LEFT JOIN __SCHEMA__.location_reporting r ON ((l.uuid = r.location_id)));


ALTER TABLE __SCHEMA__.reporting OWNER TO ewars;

--
-- TOC entry 1509 (class 1259 OID 28674)
-- Name: retractions; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.retractions (
    id integer NOT NULL,
    report_id uuid,
    retracted_by integer NOT NULL,
    retracted_date timestamp without time zone DEFAULT now() NOT NULL,
    retraction_reason text,
    status text DEFAULT 'PENDING'::text NOT NULL,
    form_id integer NOT NULL
);


ALTER TABLE __SCHEMA__.retractions OWNER TO ewars;

--
-- TOC entry 1510 (class 1259 OID 28682)
-- Name: retractions_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.retractions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.retractions_id_seq OWNER TO ewars;

--
-- TOC entry 14470 (class 0 OID 0)
-- Dependencies: 1510
-- Name: retractions_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.retractions_id_seq OWNED BY __SCHEMA__.retractions.id;


--
-- TOC entry 1511 (class 1259 OID 28684)
-- Name: site_pages; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.site_pages (
    id integer NOT NULL,
    site_id integer NOT NULL,
    title text NOT NULL,
    sef text NOT NULL,
    parent_id integer,
    definition jsonb DEFAULT '{}'::jsonb NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    access text DEFAULT 'AUTHENTICATED'::text NOT NULL,
    created_by integer NOT NULL,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL,
    page_type text DEFAULT 'MARKDOWN'::text NOT NULL,
    description text
);


ALTER TABLE __SCHEMA__.site_pages OWNER TO ewars;

--
-- TOC entry 1512 (class 1259 OID 28696)
-- Name: site_pages_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.site_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.site_pages_id_seq OWNER TO ewars;

--
-- TOC entry 14471 (class 0 OID 0)
-- Dependencies: 1512
-- Name: site_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.site_pages_id_seq OWNED BY __SCHEMA__.site_pages.id;


--
-- TOC entry 1513 (class 1259 OID 28698)
-- Name: sites; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.sites (
    id integer NOT NULL,
    title text NOT NULL,
    account_id integer NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    access text DEFAULT 'AUTHENTICATED'::text NOT NULL,
    created_by integer NOT NULL,
    created date DEFAULT now() NOT NULL,
    last_modified date DEFAULT now() NOT NULL,
    theme text DEFAULT 'DEFAULT'::text NOT NULL,
    hub_indexed text DEFAULT 'NO'::text NOT NULL,
    goog text,
    description text
);


ALTER TABLE __SCHEMA__.sites OWNER TO ewars;

--
-- TOC entry 1514 (class 1259 OID 28710)
-- Name: sites_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.sites_id_seq OWNER TO ewars;

--
-- TOC entry 14472 (class 0 OID 0)
-- Dependencies: 1514
-- Name: sites_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.sites_id_seq OWNED BY __SCHEMA__.sites.id;


--
-- TOC entry 1515 (class 1259 OID 28712)
-- Name: sms_gateways; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.sms_gateways (
    id integer NOT NULL,
    name text NOT NULL,
    status text DEFAULT 'INACTIVE'::text NOT NULL,
    can_receive boolean DEFAULT false,
    can_send boolean DEFAULT true,
    receive_number text,
    send_number text,
    gate_type text DEFAULT 'ENVAYA'::text NOT NULL,
    gate_version text,
    created_date date DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    out_rate_limit integer,
    out_rate_limit_interval text,
    in_rate_limit text,
    in_rate_limit_interval integer,
    effective_area public.geometry(Polygon,3857)
);


ALTER TABLE __SCHEMA__.sms_gateways OWNER TO ewars;

--
-- TOC entry 14473 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.status; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.status IS 'ACTIVE|INACTIVE';


--
-- TOC entry 14474 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.can_receive; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.can_receive IS 'Can this gate forward messages to EWARS';


--
-- TOC entry 14475 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.can_send; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.can_send IS 'Can this gate forward message to clients';


--
-- TOC entry 14476 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.receive_number; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.receive_number IS 'The number of that EWARS will receive messages from the relay on';


--
-- TOC entry 14477 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.send_number; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.send_number IS 'The number that will be used to send TEXTS on behalf of EWARS';


--
-- TOC entry 14478 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.gate_type; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.gate_type IS 'The type of gateway this is, translates to a driver on the server-side';


--
-- TOC entry 14479 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.out_rate_limit; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.out_rate_limit IS 'The limit that SMS can be sent through this gateway';


--
-- TOC entry 14480 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.out_rate_limit_interval; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.out_rate_limit_interval IS 'The period that out_rate_limit applies to';


--
-- TOC entry 14481 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.in_rate_limit; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.in_rate_limit IS 'The limit per hour that SMS can be received through this gateway';


--
-- TOC entry 14482 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.in_rate_limit_interval; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.in_rate_limit_interval IS 'The period that in_rate_limit applies to';


--
-- TOC entry 14483 (class 0 OID 0)
-- Dependencies: 1515
-- Name: COLUMN sms_gateways.effective_area; Type: COMMENT; Schema: __SCHEMA__; Owner: ewars
--

COMMENT ON COLUMN __SCHEMA__.sms_gateways.effective_area IS 'A geographic boundary within which, phones will be auto-configured to use this gateway';


--
-- TOC entry 1516 (class 1259 OID 28724)
-- Name: sms_gateways_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.sms_gateways_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.sms_gateways_id_seq OWNER TO ewars;

--
-- TOC entry 14484 (class 0 OID 0)
-- Dependencies: 1516
-- Name: sms_gateways_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.sms_gateways_id_seq OWNED BY __SCHEMA__.sms_gateways.id;


--
-- TOC entry 1517 (class 1259 OID 28726)
-- Name: system_notices; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.system_notices (
    id integer NOT NULL,
    content text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    active boolean DEFAULT false
);


ALTER TABLE __SCHEMA__.system_notices OWNER TO ewars;

--
-- TOC entry 1518 (class 1259 OID 28734)
-- Name: system_notices_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.system_notices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.system_notices_id_seq OWNER TO ewars;

--
-- TOC entry 14485 (class 0 OID 0)
-- Dependencies: 1518
-- Name: system_notices_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.system_notices_id_seq OWNED BY __SCHEMA__.system_notices.id;


--
-- TOC entry 1519 (class 1259 OID 28736)
-- Name: tasks_v2; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.tasks_v2 (
    id integer NOT NULL,
    task_type text NOT NULL,
    assigned_user_id integer,
    assigned_user_types text[],
    location_id uuid,
    state text DEFAULT 'OPEN'::text NOT NULL,
    priority text DEFAULT 'LOW'::text NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    claimed_date timestamp without time zone,
    action_taken text,
    actioned_date timestamp without time zone,
    actioned_by integer,
    data json DEFAULT '{}'::json,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.tasks_v2 OWNER TO ewars;

--
-- TOC entry 1520 (class 1259 OID 28746)
-- Name: tasks_v2_id_seq; Type: SEQUENCE; Schema: __SCHEMA__; Owner: ewars
--

CREATE SEQUENCE __SCHEMA__.tasks_v2_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE __SCHEMA__.tasks_v2_id_seq OWNER TO ewars;

--
-- TOC entry 14486 (class 0 OID 0)
-- Dependencies: 1520
-- Name: tasks_v2_id_seq; Type: SEQUENCE OWNED BY; Schema: __SCHEMA__; Owner: ewars
--

ALTER SEQUENCE __SCHEMA__.tasks_v2_id_seq OWNED BY __SCHEMA__.tasks_v2.id;


--
-- TOC entry 1521 (class 1259 OID 28748)
-- Name: template_cache; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.template_cache (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    doc_id text NOT NULL,
    template_id uuid NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    expires integer DEFAULT 1 NOT NULL,
    filename text
);


ALTER TABLE __SCHEMA__.template_cache OWNER TO ewars;

--
-- TOC entry 1522 (class 1259 OID 28757)
-- Name: templates; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.templates (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    template_name json DEFAULT '{}'::json NOT NULL,
    instance_name json DEFAULT '{}'::json NOT NULL,
    description json DEFAULT '{}'::json,
    created date DEFAULT now() NOT NULL,
    created_by integer NOT NULL,
    content text DEFAULT ''::text NOT NULL,
    status text DEFAULT 'DRAFT'::text NOT NULL,
    generation json DEFAULT '{}'::json NOT NULL,
    shareable boolean DEFAULT false,
    access text DEFAULT 'PRIVATE'::text NOT NULL,
    data json,
    template_type text DEFAULT 'GENERATED'::text NOT NULL,
    orientation text DEFAULT 'PORTRAIT'::text NOT NULL,
    hid uuid,
    expires integer,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE __SCHEMA__.templates OWNER TO ewars;

--
-- TOC entry 1523 (class 1259 OID 28775)
-- Name: timeliness; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.timeliness AS
 SELECT c.data_date,
    c.form_id,
    c.location_id,
    c.submitted_date,
    ((c.submitted_date)::date - c.data_date) AS delay
   FROM __SCHEMA__.collections c
  WHERE ((c.status = 'SUBMITTED'::text) OR (c.status = 'PENDING_AMENDMENT'::text));


ALTER TABLE __SCHEMA__.timeliness OWNER TO ewars;

--
-- TOC entry 2817 (class 1259 OID 527490)
-- Name: topics; Type: TABLE; Schema: __SCHEMA__; Owner: ewars
--

CREATE TABLE __SCHEMA__.topics (
    eid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    topic text DEFAULT 'NO_TOPIC'::text NOT NULL,
    key text NOT NULL,
    value jsonb,
    ts_ms integer DEFAULT (date_part('epoch'::text, (now())::timestamp without time zone))::integer NOT NULL
);


ALTER TABLE __SCHEMA__.topics OWNER TO ewars;

--
-- TOC entry 1524 (class 1259 OID 28779)
-- Name: users; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.users AS
 SELECT a.user_id AS id,
    a.aid,
    u.uuid,
    ac.tki,
    a.role,
    a.status,
    a.location_id,
    a.lab_id,
    a.created,
    a.created_by,
    a.last_modified,
    a.registered,
    ac.name AS account_name,
    ac.location_id AS clid,
    u.name,
    u.email,
    u.password,
    u.system,
    u.profile,
    u.org_id,
    u.language,
    u.api_token,
    u.notifications,
    u.timezone,
    u.date_format,
    u.time_format,
    u.pronoun,
    u.accessibility,
    u.email_format,
    u.desk_not_status,
    u.email_status,
    u.accounts,
    l.name AS location_name,
    o.name AS org_name
   FROM ((((__SCHEMA__.accounts a
     LEFT JOIN _iw.users u ON ((u.id = a.user_id)))
     LEFT JOIN __SCHEMA__.locations l ON ((l.uuid = a.location_id)))
     LEFT JOIN _iw.organizations o ON ((o.uuid = u.org_id)))
     LEFT JOIN _iw.accounts ac ON ((a.aid = ac.id)));


ALTER TABLE __SCHEMA__.users OWNER TO ewars;

--
-- TOC entry 1525 (class 1259 OID 28784)
-- Name: v_amendments; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.v_amendments AS
 SELECT a.id,
    a.report_id,
    a.original,
    a.amended,
    a.amended_date,
    a.amendment_reason,
    a.status,
    a.form_id,
    a.amended_by,
    u.name AS user_name,
    u.email,
    c.data_date,
    c.location_id,
    l.name AS location_name
   FROM (((__SCHEMA__.amendments a
     LEFT JOIN _iw.users u ON ((u.id = a.amended_by)))
     LEFT JOIN __SCHEMA__.collections c ON ((c.uuid = a.report_id)))
     LEFT JOIN __SCHEMA__.locations l ON ((l.uuid = c.location_id)));


ALTER TABLE __SCHEMA__.v_amendments OWNER TO ewars;

--
-- TOC entry 2726 (class 1259 OID 515581)
-- Name: v_assignments; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.v_assignments AS
 SELECT a.uuid,
    a.user_id,
    a.created,
    a.created_by,
    a.last_modified,
    a.status,
    a.location_id,
    a.form_id,
    a.start_date,
    a.end_date,
    l.name AS location_name,
    l.lineage,
    l.site_type_id AS sti_id,
    l.groups,
    lt.name AS sti_name,
    f.name AS form_name,
    u.name AS user_name,
    u.email AS user_email,
    u.org_id,
    o.name AS org_name
   FROM (((((__SCHEMA__.assignments a
     LEFT JOIN __SCHEMA__.locations l ON ((l.uuid = a.location_id)))
     LEFT JOIN __SCHEMA__.location_types lt ON ((lt.id = l.site_type_id)))
     LEFT JOIN __SCHEMA__.forms f ON ((f.id = a.form_id)))
     LEFT JOIN _iw.users u ON ((u.id = a.user_id)))
     LEFT JOIN _iw.organizations o ON ((o.uuid = u.org_id)));


ALTER TABLE __SCHEMA__.v_assignments OWNER TO ewars;

--
-- TOC entry 1526 (class 1259 OID 28794)
-- Name: v_collection_comments; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.v_collection_comments AS
 SELECT c.uuid,
    c.collection_id,
    c.created,
    c.user_id,
    c.edited,
    c.content,
    c.meta,
    u.name AS user_name,
    u.email,
    o.name AS org_name,
    a.role
   FROM (((__SCHEMA__.collection_comments c
     LEFT JOIN _iw.users u ON ((u.id = c.user_id)))
     LEFT JOIN _iw.organizations o ON ((u.org_id = o.uuid)))
     LEFT JOIN __SCHEMA__.accounts a ON ((a.user_id = c.user_id)));


ALTER TABLE __SCHEMA__.v_collection_comments OWNER TO ewars;

--
-- TOC entry 1527 (class 1259 OID 28799)
-- Name: v_location_profile; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.v_location_profile AS
 SELECT l.uuid,
    (l.name ->> 'en'::text) AS name,
    l.pcode,
    l.lineage,
    l.parent_id,
    l.description,
    l.groups,
    l.site_type_id,
    (lt.name ->> 'en'::text) AS sti_name,
    ( SELECT count(*) AS count
           FROM (__SCHEMA__.collections c1
             LEFT JOIN __SCHEMA__.locations l1 ON ((l.uuid = c1.location_id)))
          WHERE ((c1.status = 'SUBMITTED'::text) AND ((l.uuid)::text = ANY (l1.lineage)))) AS total_reports,
    ( SELECT count(*) AS count
           FROM __SCHEMA__.collections c2
          WHERE (c2.status = 'SUBMITTED'::text)) AS all_reports,
    ( SELECT count(*) AS count
           FROM (__SCHEMA__.assignments a1
             LEFT JOIN __SCHEMA__.locations l2 ON ((l2.uuid = a1.location_id)))
          WHERE (((l.uuid)::text = ANY (l2.lineage)) AND (a1.status = 'ACTIVE'::text))) AS assignments,
    ( SELECT count(*) AS count
           FROM (__SCHEMA__.users u1
             LEFT JOIN __SCHEMA__.locations l3 ON ((u1.location_id = l3.uuid)))
          WHERE ((u1.role = 'REGIONAL_ADMIN'::text) AND ((l.uuid)::text = ANY (l3.lineage)) AND (u1.status = 'ACTIVE'::text))) AS regional_admins,
    ( SELECT ARRAY( SELECT (l4.name ->> 'en'::text)
                   FROM __SCHEMA__.locations l4
                  WHERE ((l4.uuid)::text = ANY (l.lineage))) AS "array") AS full_name,
    ( SELECT count(*) AS count
           FROM __SCHEMA__.alerts_full al1
          WHERE (((l.uuid)::text = ANY (al1.lineage)) AND (al1.state = 'OPEN'::text))) AS open_alerts,
    ( SELECT count(*) AS count
           FROM __SCHEMA__.alerts_full al2
          WHERE ((l.uuid)::text = ANY (al2.lineage))) AS triggered_alerts,
    ( SELECT count(*) AS count
           FROM __SCHEMA__.locations ch1
          WHERE (((l.uuid)::text = ANY (ch1.lineage)) AND (ch1.status = 'ACTIVE'::text))) AS childs_active,
    ( SELECT count(*) AS count
           FROM __SCHEMA__.locations ch2
          WHERE (((l.uuid)::text = ANY (ch2.lineage)) AND (ch2.status = 'ARCHIVED'::text))) AS childs_archived
   FROM (__SCHEMA__.locations l
     LEFT JOIN __SCHEMA__.location_types lt ON ((l.site_type_id = lt.id)));


ALTER TABLE __SCHEMA__.v_location_profile OWNER TO ewars;

--
-- TOC entry 1528 (class 1259 OID 28804)
-- Name: v_sti_collections; Type: VIEW; Schema: __SCHEMA__; Owner: ewars
--

CREATE VIEW __SCHEMA__.v_sti_collections AS
 SELECT c.form_id,
    array_agg(DISTINCT l.site_type_id) AS site_types
   FROM (__SCHEMA__.collections c
     LEFT JOIN __SCHEMA__.locations l ON ((l.uuid = c.location_id)))
  GROUP BY c.form_id;


ALTER TABLE __SCHEMA__.v_sti_collections OWNER TO ewars;

--
-- TOC entry 13521 (class 2604 OID 32544)
-- Name: alert_actions id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_actions ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.alert_actions_id_seq'::regclass);


--
-- TOC entry 13526 (class 2604 OID 32545)
-- Name: alert_events id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_events ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.alert_events_id_seq'::regclass);


--
-- TOC entry 13530 (class 2604 OID 32546)
-- Name: alert_users id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_users ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.alert_users_id_seq'::regclass);


--
-- TOC entry 13568 (class 2604 OID 32547)
-- Name: amendments id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.amendments ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.amendments_id_seq'::regclass);


--
-- TOC entry 13599 (class 2604 OID 32548)
-- Name: conf id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.conf ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.conf_id_seq'::regclass);


--
-- TOC entry 13621 (class 2604 OID 32549)
-- Name: devices id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.devices ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.devices_id_seq'::regclass);


--
-- TOC entry 13628 (class 2604 OID 32550)
-- Name: discussion_threads id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.discussion_threads ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.discussions_id_seq'::regclass);


--
-- TOC entry 13631 (class 2604 OID 32551)
-- Name: distributions id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.distributions ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.distributions_id_seq'::regclass);


--
-- TOC entry 13644 (class 2604 OID 32552)
-- Name: feed_items id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.feed_items ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.feed_items_id_seq'::regclass);


--
-- TOC entry 13650 (class 2604 OID 32553)
-- Name: feeds id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.feeds ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.feeds_id_seq'::regclass);


--
-- TOC entry 13660 (class 2604 OID 32554)
-- Name: forms id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.forms ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.forms_id_seq'::regclass);


--
-- TOC entry 13677 (class 2604 OID 32555)
-- Name: groups id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.groups ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.groups_id_seq'::regclass);


--
-- TOC entry 13679 (class 2604 OID 32556)
-- Name: guidance id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.guidance ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.guidance_id_seq'::regclass);


--
-- TOC entry 13705 (class 2604 OID 32557)
-- Name: indicator_groups id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.indicator_groups ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.indicator_groups_id_seq'::regclass);


--
-- TOC entry 13725 (class 2604 OID 32559)
-- Name: location_reporting id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.location_reporting ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.location_reporting_id_seq'::regclass);


--
-- TOC entry 13551 (class 2604 OID 32560)
-- Name: location_types id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.location_types ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.location_types_id_seq'::regclass);


--
-- TOC entry 13745 (class 2604 OID 32561)
-- Name: news_categories id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.news_categories ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.news_categories_id_seq'::regclass);


--
-- TOC entry 13747 (class 2604 OID 32562)
-- Name: news_posts id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.news_posts ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.news_posts_id_seq'::regclass);


--
-- TOC entry 13775 (class 2604 OID 32563)
-- Name: retractions id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.retractions ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.retractions_id_seq'::regclass);


--
-- TOC entry 13778 (class 2604 OID 32564)
-- Name: site_pages id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.site_pages ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.site_pages_id_seq'::regclass);


--
-- TOC entry 13785 (class 2604 OID 32565)
-- Name: sites id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.sites ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.sites_id_seq'::regclass);


--
-- TOC entry 13792 (class 2604 OID 32566)
-- Name: sms_gateways id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.sms_gateways ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.sms_gateways_id_seq'::regclass);


--
-- TOC entry 13799 (class 2604 OID 32567)
-- Name: system_notices id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.system_notices ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.system_notices_id_seq'::regclass);


--
-- TOC entry 13802 (class 2604 OID 32568)
-- Name: tasks_v2 id; Type: DEFAULT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.tasks_v2 ALTER COLUMN id SET DEFAULT nextval('__SCHEMA__.tasks_v2_id_seq'::regclass);


--
-- TOC entry 13866 (class 2606 OID 51026)
-- Name: activity_feed activity_feed_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.activity_feed
    ADD CONSTRAINT activity_feed_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13868 (class 2606 OID 51030)
-- Name: alarms_v2 alarms_v2_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alarms_v2
    ADD CONSTRAINT alarms_v2_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13870 (class 2606 OID 51032)
-- Name: alert_actions alert_actions_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_actions
    ADD CONSTRAINT alert_actions_pkey PRIMARY KEY (id);


--
-- TOC entry 13872 (class 2606 OID 51034)
-- Name: alert_events alert_events_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_events
    ADD CONSTRAINT alert_events_pkey PRIMARY KEY (id);


--
-- TOC entry 13874 (class 2606 OID 51036)
-- Name: alert_users alert_users_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alert_users
    ADD CONSTRAINT alert_users_pkey PRIMARY KEY (id);


--
-- TOC entry 13879 (class 2606 OID 51038)
-- Name: alerts alerts_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.alerts
    ADD CONSTRAINT alerts_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13890 (class 2606 OID 51040)
-- Name: amendments amendments_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.amendments
    ADD CONSTRAINT amendments_pkey PRIMARY KEY (id);


--
-- TOC entry 13893 (class 2606 OID 51042)
-- Name: analysis_notebooks analysis_notebooks_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.analysis_notebooks
    ADD CONSTRAINT analysis_notebooks_pkey PRIMARY KEY (id);


--
-- TOC entry 13895 (class 2606 OID 51044)
-- Name: assignments assignments_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.assignments
    ADD CONSTRAINT assignments_pkey PRIMARY KEY (uuid);


--
-- TOC entry 14001 (class 2606 OID 524763)
-- Name: channel_items channel_items_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.channel_items
    ADD CONSTRAINT channel_items_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13999 (class 2606 OID 524751)
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13898 (class 2606 OID 51046)
-- Name: collection_comments collection_comments_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.collection_comments
    ADD CONSTRAINT collection_comments_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13902 (class 2606 OID 51048)
-- Name: collections collections_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.collections
    ADD CONSTRAINT collections_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13906 (class 2606 OID 51050)
-- Name: conf conf_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.conf
    ADD CONSTRAINT conf_pkey PRIMARY KEY (id);


--
-- TOC entry 13910 (class 2606 OID 51052)
-- Name: data_sets_data data_sets_data_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.data_sets_data
    ADD CONSTRAINT data_sets_data_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13908 (class 2606 OID 51054)
-- Name: data_sets data_sets_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.data_sets
    ADD CONSTRAINT data_sets_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13912 (class 2606 OID 51056)
-- Name: data_sources data_sources_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.data_sources
    ADD CONSTRAINT data_sources_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13914 (class 2606 OID 51058)
-- Name: datasets datasets_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.datasets
    ADD CONSTRAINT datasets_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13916 (class 2606 OID 51060)
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- TOC entry 13920 (class 2606 OID 51062)
-- Name: discussion_threads discussions_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.discussion_threads
    ADD CONSTRAINT discussions_pkey PRIMARY KEY (id);


--
-- TOC entry 13922 (class 2606 OID 51064)
-- Name: distributions distributions_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.distributions
    ADD CONSTRAINT distributions_pkey PRIMARY KEY (id);


--
-- TOC entry 13993 (class 2606 OID 187941)
-- Name: event_log event_log_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.event_log
    ADD CONSTRAINT event_log_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13924 (class 2606 OID 51066)
-- Name: event_types event_types_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.event_types
    ADD CONSTRAINT event_types_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13926 (class 2606 OID 51068)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13928 (class 2606 OID 51070)
-- Name: feed_items feed_items_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.feed_items
    ADD CONSTRAINT feed_items_pkey PRIMARY KEY (id);


--
-- TOC entry 13930 (class 2606 OID 51072)
-- Name: feeds feeds_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.feeds
    ADD CONSTRAINT feeds_pkey PRIMARY KEY (id);


--
-- TOC entry 13933 (class 2606 OID 51074)
-- Name: form_versions form_versions_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.form_versions
    ADD CONSTRAINT form_versions_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13935 (class 2606 OID 51076)
-- Name: forms forms_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- TOC entry 13937 (class 2606 OID 51078)
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- TOC entry 13939 (class 2606 OID 51080)
-- Name: guidance guidance_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.guidance
    ADD CONSTRAINT guidance_pkey PRIMARY KEY (id);


--
-- TOC entry 13995 (class 2606 OID 404983)
-- Name: huds huds_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.huds
    ADD CONSTRAINT huds_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13941 (class 2606 OID 51082)
-- Name: import_project_data import_project_data_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.import_project_data
    ADD CONSTRAINT import_project_data_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13943 (class 2606 OID 51084)
-- Name: import_projects import_projects_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.import_projects
    ADD CONSTRAINT import_projects_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13945 (class 2606 OID 51086)
-- Name: imports imports_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13997 (class 2606 OID 515627)
-- Name: inbound_events inbound_events_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.inbound_events
    ADD CONSTRAINT inbound_events_pkey PRIMARY KEY (eid);


--
-- TOC entry 13947 (class 2606 OID 51088)
-- Name: indicator_groups indicator_groups_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.indicator_groups
    ADD CONSTRAINT indicator_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 13882 (class 2606 OID 51090)
-- Name: indicators indicators_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.indicators
    ADD CONSTRAINT indicators_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13949 (class 2606 OID 51092)
-- Name: investigations investigations_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.investigations
    ADD CONSTRAINT investigations_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13951 (class 2606 OID 51094)
-- Name: invites invites_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.invites
    ADD CONSTRAINT invites_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13953 (class 2606 OID 51096)
-- Name: laboratories laboratories_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.laboratories
    ADD CONSTRAINT laboratories_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13955 (class 2606 OID 51098)
-- Name: layouts layouts_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.layouts
    ADD CONSTRAINT layouts_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13957 (class 2606 OID 51102)
-- Name: location_reporting location_reporting_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.location_reporting
    ADD CONSTRAINT location_reporting_pkey PRIMARY KEY (id);


--
-- TOC entry 13884 (class 2606 OID 51104)
-- Name: location_types location_types_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.location_types
    ADD CONSTRAINT location_types_pkey PRIMARY KEY (id);


--
-- TOC entry 13887 (class 2606 OID 51106)
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (uuid);


--
-- TOC entry 14005 (class 2606 OID 530166)
-- Name: mapping mapping_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.mapping
    ADD CONSTRAINT mapping_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13959 (class 2606 OID 51108)
-- Name: maps maps_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.maps
    ADD CONSTRAINT maps_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13918 (class 2606 OID 51110)
-- Name: discussion_thread_messages messages_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.discussion_thread_messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13961 (class 2606 OID 51112)
-- Name: messages messages_pkey1; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.messages
    ADD CONSTRAINT messages_pkey1 PRIMARY KEY (uuid);


--
-- TOC entry 13963 (class 2606 OID 51114)
-- Name: news_categories news_categories_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.news_categories
    ADD CONSTRAINT news_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 13965 (class 2606 OID 51116)
-- Name: news_posts news_posts_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.news_posts
    ADD CONSTRAINT news_posts_pkey PRIMARY KEY (id);


--
-- TOC entry 13967 (class 2606 OID 51118)
-- Name: notebooks notebooks_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.notebooks
    ADD CONSTRAINT notebooks_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13969 (class 2606 OID 51120)
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13971 (class 2606 OID 51122)
-- Name: orgs orgs_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.orgs
    ADD CONSTRAINT orgs_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13973 (class 2606 OID 51124)
-- Name: plots plots_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.plots
    ADD CONSTRAINT plots_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13975 (class 2606 OID 51126)
-- Name: retractions retractions_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.retractions
    ADD CONSTRAINT retractions_pkey PRIMARY KEY (id);


--
-- TOC entry 13977 (class 2606 OID 51128)
-- Name: site_pages site_pages_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.site_pages
    ADD CONSTRAINT site_pages_pkey PRIMARY KEY (id);


--
-- TOC entry 13979 (class 2606 OID 51130)
-- Name: sites sites_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- TOC entry 13981 (class 2606 OID 51132)
-- Name: sms_gateways sms_gateways_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.sms_gateways
    ADD CONSTRAINT sms_gateways_pkey PRIMARY KEY (id);


--
-- TOC entry 13983 (class 2606 OID 51134)
-- Name: system_notices system_notices_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.system_notices
    ADD CONSTRAINT system_notices_pkey PRIMARY KEY (id);


--
-- TOC entry 13985 (class 2606 OID 51136)
-- Name: tasks_v2 tasks_v2_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.tasks_v2
    ADD CONSTRAINT tasks_v2_pkey PRIMARY KEY (id);


--
-- TOC entry 13987 (class 2606 OID 51138)
-- Name: template_cache template_cache_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.template_cache
    ADD CONSTRAINT template_cache_pkey PRIMARY KEY (uuid);


--
-- TOC entry 13989 (class 2606 OID 51140)
-- Name: templates templates_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (uuid);


--
-- TOC entry 14003 (class 2606 OID 527500)
-- Name: topics topics_pkey; Type: CONSTRAINT; Schema: __SCHEMA__; Owner: ewars
--

ALTER TABLE ONLY __SCHEMA__.topics
    ADD CONSTRAINT topics_pkey PRIMARY KEY (eid);


--
-- TOC entry 13875 (class 1259 OID 51901)
-- Name: alerts_alarm_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX alerts_alarm_id_idx ON __SCHEMA__.alerts USING btree (alarm_id);


--
-- TOC entry 13876 (class 1259 OID 51902)
-- Name: alerts_indicator_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX alerts_indicator_id_idx ON __SCHEMA__.alerts USING btree (indicator_id);


--
-- TOC entry 13877 (class 1259 OID 51903)
-- Name: alerts_location_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX alerts_location_id_idx ON __SCHEMA__.alerts USING btree (location_id);


--
-- TOC entry 13891 (class 1259 OID 51904)
-- Name: analysis_notebooks_created_by_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX analysis_notebooks_created_by_idx ON __SCHEMA__.analysis_notebooks USING btree (created_by);


--
-- TOC entry 13896 (class 1259 OID 51905)
-- Name: assignments_user_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX assignments_user_id_idx ON __SCHEMA__.assignments USING btree (user_id);


--
-- TOC entry 13899 (class 1259 OID 51906)
-- Name: collection_date_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX collection_date_idx ON __SCHEMA__.collections USING btree (data_date);


--
-- TOC entry 13900 (class 1259 OID 51907)
-- Name: collections_location_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX collections_location_id_idx ON __SCHEMA__.collections USING btree (location_id);


--
-- TOC entry 13931 (class 1259 OID 51908)
-- Name: form_versions_form_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX form_versions_form_id_idx ON __SCHEMA__.form_versions USING btree (form_id);


--
-- TOC entry 13903 (class 1259 OID 51909)
-- Name: idx_form_id_collection; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX idx_form_id_collection ON __SCHEMA__.collections USING btree (form_id);


--
-- TOC entry 13904 (class 1259 OID 51910)
-- Name: idx_user_id_collection; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX idx_user_id_collection ON __SCHEMA__.collections USING btree (created_by);


--
-- TOC entry 13880 (class 1259 OID 51911)
-- Name: indicators_group_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX indicators_group_id_idx ON __SCHEMA__.indicators USING btree (group_id);


--
-- TOC entry 13885 (class 1259 OID 51912)
-- Name: locations_parent_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX locations_parent_id_idx ON __SCHEMA__.locations USING btree (parent_id);


--
-- TOC entry 13888 (class 1259 OID 51913)
-- Name: locations_site_type_id_idx; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE INDEX locations_site_type_id_idx ON __SCHEMA__.locations USING btree (site_type_id);


--
-- TOC entry 13990 (class 1259 OID 448693)
-- Name: unique_dd_fid_lid_when_null; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE UNIQUE INDEX unique_dd_fid_lid_when_null ON __SCHEMA__.collection_counts USING btree (data_date, form_id) WHERE (location_id IS NULL);


--
-- TOC entry 13991 (class 1259 OID 448692)
-- Name: unique_dd_lid_fid_when_not_null; Type: INDEX; Schema: __SCHEMA__; Owner: ewars
--

CREATE UNIQUE INDEX unique_dd_lid_fid_when_not_null ON __SCHEMA__.collection_counts USING btree (data_date, form_id, location_id);


--
-- TOC entry 14006 (class 2620 OID 184638)
-- Name: collections trigger_on_collection_delete; Type: TRIGGER; Schema: __SCHEMA__; Owner: ewars
--

CREATE TRIGGER trigger_on_collection_delete AFTER DELETE ON __SCHEMA__.collections FOR EACH ROW EXECUTE PROCEDURE __SCHEMA__.fn_on_collection_delete();


--
-- TOC entry 14007 (class 2620 OID 184636)
-- Name: collections trigger_on_collection_insert; Type: TRIGGER; Schema: __SCHEMA__; Owner: ewars
--

CREATE TRIGGER trigger_on_collection_insert AFTER INSERT ON __SCHEMA__.collections FOR EACH ROW EXECUTE PROCEDURE __SCHEMA__.fn_on_collection_insert();


--
-- TOC entry 14008 (class 2620 OID 184634)
-- Name: collections trigger_on_collection_update; Type: TRIGGER; Schema: __SCHEMA__; Owner: ewars
--

CREATE TRIGGER trigger_on_collection_update AFTER UPDATE ON __SCHEMA__.collections FOR EACH ROW EXECUTE PROCEDURE __SCHEMA__.fn_on_collection_update();


-- Completed on 2018-11-16 08:30:15 GMT

--
-- PostgreSQL database dump complete
--




"""
