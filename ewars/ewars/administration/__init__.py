import json
import datetime
import uuid

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs
from ewars.core.serializers.json_encoder import JSONEncoder

from .account_sql import ACCOUNT_SQL
from ewars.administration import defaults, system_indicators
from ewars.core import registration


def update_account(account_id, data, user=None):
    """ Update an accounts settings
    
    Args:
        account_id: The id of the account to update
        data: The data to update
        user: The calling user (restricted to super admin)

    Returns:

    """
    if user.get("user_type") != "SUPER_ADMIN":
        return False


async def create_account(data, user=None):
    """ Create a new account in the system
    
    Args:
        data: The data for the new account
        user: The calling user (restricted to super admin)

    Returns:

    """
    if user.get("user_type", None) != "SUPER_ADMIN":
        return False

    account = None
    acc_uuid = str(uuid.uuid4())
    tki = "_%s" % (acc_uuid.split("-")[-1])
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO _iw.accounts (
              name, 
              default_language, 
              status, 
              domain, 
              created_by, 
              last_modified, 
              description, 
              account_logo,
              account_flag,
              dashboards,
              iso2,
              iso3,
              instance_id,
              uuid,
              tki
          ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
        """, (
            data.get("name"),
            data.get("default_language"),
            data.get("status"),
            data.get("domain"),
            1,
            datetime.datetime.utcnow(),
            json.dumps(data.get("description", "")),
            data.get('account_logo'),
            data.get("account_flag"),
            '{}',
            data.get("iso2", None),
            data.get("iso3", None),
            '14e76d3b-da24-4b6f-a8be-0dab254d98b0',
            acc_uuid,
            tki,
        ))
        account = await cur.fetchone()

    create_sql = account_sql.ACCOUNT_SQL.replace("__SCHEMA__", tki)

    # Set up system indicators
    async with get_db_cur() as cur:
        await cur.execute(create_sql)

        for group in system_indicators.GROUPS:
            await cur.execute("""
                INSERT INTO %s.indicator_groups
                (id, name, parent_id) VALUES (%s, %s, %s);
            """, (
                AsIs(tki),
                group[0],
                json.dumps(group[1]),
                None,
            ))

        for indicator in system_indicators.INDICATORS:
            await cur.execute("""
                INSERT INTO %s.indicators 
                (uuid, group_id, name, status, protected, created, created_by, last_modified, ranked, colour, itype, definition, description, guidance, icode)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(tki),
                indicator[0],
                1,
                json.dumps(indicator[1]),
                'ACTIVE',
                True,
                datetime.datetime.utcnow(),
                1,
                datetime.datetime.utcnow(),
                False,
                "purple",
                indicator[2],
                json.dumps(indicator[3]),
                '{}',
                '{}',
                indicator[4],
            ))

    # Set up locations data
    async with get_db_cur() as cur:
        # Create uncategorized root location
        await cur.execute("""
            INSERT INTO %s.location_types 
            (id, name, description, status, created_by)
            VALUES (1, '{"en": "Uncategorized"}', 'Uncategorized', 'ACTIVE', 1);
        """, (
            AsIs(tki),
        ))

        # Create default root location
        root_loc_uuid = str(uuid.uuid4())
        await cur.execute("""
            INSERT INTO %s.locations 
            (uuid, name, location_type, pcode, description, parent_id, lineage, status, geometry_type, site_type_id, default_zoom)
            VALUES (%s, %s, 'ADMIN', 'ROOT', 'No description', NULL, %s, 'ACTIVE', 'ADMIN', 1, 10);
        """, (
            AsIs(tki),
            root_loc_uuid,
            json.dumps(dict(en="Root Location")),
            [root_loc_uuid],
        ))

        # Update account with location
        await cur.execute("""
            UPDATE _iw.accounts 
                SET location_id = %s 
            WHERE id = %s;
        """, (
            root_loc_uuid,
            account.get('id'),
        ))

    admin_user = await registration.create_sso_user(
        data.get('admin_email'),
        data.get('admin_password'),
        'Account Admin',
        None,
        account.get("id"),
        'ACTIVE'
    )

    await registration.create_acc_context_user(
        tki,
        admin_user.get("id"),
        account.get("id"),
        'ACTIVE',
        'ACCOUNT_ADMIN',
        None,
        1
    )

    return account


async def delete_account(account_id, user=None):
    """ Delete and account from the system
    
    Args:
        account_id: The id of the account to delete
        user: The calling user (restricted to super admin)

    Returns:

    """
    if user.get("user_type", None) != "SUPER_ADMIN":
        return False

    users = []

    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts 
            WHERE id = %s;
        """, (
            account_id,
        ))
        account = await cur.fetchone()

        await cur.execute("""
            SELECT id, accounts 
            FROM _iw.users 
            WHERE %s = ANY(accounts);
        """, (
            account_id,
        ))
        users = await cur.fetchall()

    for user in users:
        user['accounts'] = list(filter(lambda k: k != account_id, user.get("accounts", [])))

    async with get_db_cur() as cur:
        for user in users:
            await cur.execute("""
                UPDATE _iw.users 
                SET accounts = %s 
                WHERE id = %s;
            """, (
                user.get("account", []),
                user.get("id"),
            ))

        await cur.execute("""
            UPDATE _iw.users
            SET account_id = NULL 
            WHERE account_id = %s;
        """, (
            account.get("id"),
        ))

        if account.get("tki", None) is not None:
            await cur.execute("""
                DROP SCHEMA IF EXISTS %s CASCADE;
            """, (
                AsIs(account.get("tki")),
            ))
        else:
            acc_uuid = account.get("uuid")
            tki = "_%s" % (acc_uuid.split("-")[-1],)
            await cur.execute("""
                DROP SCHEMA IF EXISTS %s CASCADE;
            """, (
                AsIs(tki),
            ))

        await cur.execute("""
            DELETE FROM _iw.accounts WHERE id = %s;
        """, (
            account_id,
        ))

    return True
