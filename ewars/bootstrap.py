import signal
import ewars
import os
import tornado.ioloop

PORT = os.environ.get("EWARS_PORT", 9000)

if __name__ == "__main__":
    server = None

    try:
        server = ewars.run(port=PORT)
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
