use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use std::path::Path;
use std::borrow::BorrowMut;

use uuid::Uuid;
use chrono::Utc;
use serde_json;
use serde_json::Value;

use r2d2;
use r2d2_sqlite::SqliteConnectionManager;
use failure::Error;

use crate::sql_data::SQL_CREATE;
use crate::models::state::State;
use crate::models::config::{Config, Account, User};
use crate::codec::{Tenancy};
use crate::remote_client::{Account as InboundAccount, AccountUser};

pub(crate) trait IntoShared {
    fn into_shared(self) -> Rc<RefCell<Self>>;
}

impl<T> IntoShared for T {
    fn into_shared(self) -> Rc<RefCell<Self>> { Rc::new(RefCell::new(self)) }
}

type Tki = String;

pub struct ApplicationState {
    pub pools: HashMap<String, r2d2::Pool<SqliteConnectionManager>>,
    pub user_dir: String,
    pub active: Option<Uuid>,
    pub config: Config,
    pub peer_ts_ms: HashMap<String, usize>,
    pub account: Option<Account>,
    pub user: Option<User>,
}

impl ApplicationState {
    pub fn new(config: Config) -> Rc<RefCell<ApplicationState>> {
        let path: String = dirs::data_dir().unwrap().to_str().unwrap().to_string();
        let user_dir: String = format!("{}/EWARS", path);
        let state = ApplicationState {
            pools: HashMap::new(),
            active: Option::None,
            config: config,
            user_dir,
            peer_ts_ms: HashMap::new(),
            account: Option::None,
            user: Option::None,
        }.into_shared();

        state
    }

    // Get a list of remote domains that we need to connect to
    pub fn get_remotes(&self) -> HashMap<String, Vec<String>> {
        let mut accounts: HashMap<String, Vec<String>> = HashMap::new();
        let mut tokens: HashMap<String, Vec<String>> = HashMap::new();

        for user in &self.config.users {
            tokens.entry(user.tki.clone())
                .and_modify(|x| x.push(user.token.clone()))
                .or_insert(vec![user.token.clone()]);
        }

        for x in &self.config.accounts {
            let mut acc_tokens: Vec<String> = Vec::new();
            if let Some(c) = tokens.get(&x.tki) {
                acc_tokens = c.clone();
            }

            accounts.entry(x.domain.clone())
                .and_modify(|x| x.extend(acc_tokens.clone()))
                .or_insert(acc_tokens.clone());
        }

        accounts
    }

    pub fn add_accounts(&mut self, users: &Vec<AccountUser>, accounts: &Vec<InboundAccount>) -> Result<(), Error> {
        for user in users {
            self.config.add_user(&user);
        }

        for account in accounts {
            self.config.add_account(&account);
        }
        self.config.flush()?;

        for x in &self.config.accounts {
            if !self.pools.contains_key(&x.tki) {
                let db_name = format!("{}.db", &x.tki);
                let db_path = Path::new(&self.user_dir).join(db_name);
                let manager = SqliteConnectionManager::file(db_path);
                let pool = r2d2::Pool::builder().min_idle(Some(3)).build(manager).unwrap();
                self.pools.insert(x.tki.clone(), pool.clone());

                // Get a connection from the pool
                let conn = pool.get().unwrap();

                // Check if the database is initialized
                if !x.is_db_init(&conn) {
                    // Create the database tables
                    let result: bool = x.generate_db(&conn);
                }
            }
        }
        Ok(())
    }

    // Map a tki to a concrete domain
    pub fn get_domain_for_tki(&self, tki: &String) -> Option<String> {
        let mut domain: Option<String> = Option::None;
        for acc in &self.config.accounts {
            if &acc.tki == tki {
                domain = Some(acc.domain.clone());
                break;
            }
        }

        domain
    }

    // Set the currently active account 
    pub fn set_account(&mut self, acc: &Account) {
        self.account = Some(acc.clone());
    }

    // Set the user, and subsequently the account
    pub fn set_user(&mut self, user: &User) {
        self.user = Some(user.clone());
    }

    pub fn get_user_details(&self, uuid: &Uuid) -> Result<(Account, User), Error> {
        let mut user: Option<User> = Option::None;
        for s_user in &self.config.users {
            if uuid == &s_user.uuid {
                user = Some(s_user.clone());
            }
        }

        if let Some(c) = user {
            let mut account: Option<Account> = Option::None;
            for acc in &self.config.accounts {
                if acc.tki == c.tki {
                    account = Some(acc.clone());
                }
            }

            if let Some(d) = account {
                Ok((d, c))
            } else {
                bail!("Could not location user/account");
            }
        } else {
            bail!("Error retrieving user details");
        }
    }

    pub fn get_current_tki(&self) -> String {
        if let Some(c) = &self.account {
            c.tki.clone()
        } else {
            "".to_string()
        }
    }

    pub fn get_current_role(&self) -> String {
        if let Some(c) = &self.user {
            c.role.clone()
        } else {
            "".to_string()
        }
    }

    // get the current user
    pub fn get_current_user(&self) -> Result<User, ()> {
        if let Some(c) = &self.user {
            Ok(c.clone())
        } else {
            Err(())
        }
    }

    // Get the last ts_ms for a given tki remote
    pub fn get_tki_ts_ms(&self, tki: String) -> usize {
        unimplemented!()
    }

    pub fn get_account_by_uuid(&self, uuid: &Uuid) -> Option<Account> {
        let mut acc: Option<Account> = Option::None;

        for x in &self.config.accounts {
            if x.uuid == uuid.clone() {
                acc = Some(x.clone());
            }
        }

        acc
    }

    pub fn init(&mut self) -> Result<(), ()> {

        let mut inited: Vec<String> = Vec::new();

        for x in &self.config.accounts {
            if !inited.contains(&x.tki) {
                let db_name = format!("{}.db", &x.tki);
                let db_path = Path::new(&self.user_dir).join(db_name);
                let manager = SqliteConnectionManager::file(db_path);
                let pool = r2d2::Pool::builder().min_idle(Some(3)).build(manager).unwrap();
                self.pools.insert(x.tki.clone(), pool.clone());

                // Get a connection from the pool
                let conn = pool.get().unwrap();

                // Check if the database is initialized
                if !x.is_db_init(&conn) {
                    // Create the database tables
                    let result: bool = x.generate_db(&conn);
                }

                inited.push(x.tki.clone());
            }
        }

        Ok(())

    }

    pub fn set_account_seeded(&mut self, tki: &String) -> Result<(), Error> {
        for item in &mut self.config.accounts {
            if &item.tki == tki {
                item.seeded = true;
            }
        }

        self.config.flush()?;

        Ok(())
    }

    pub fn get_current_pool(&self) -> Result<&r2d2::Pool<SqliteConnectionManager>, String> {
        let account = self.account.clone().unwrap();

        let current_tki: Tki = account.tki;
        let window = self.pools.get(&current_tki).unwrap();
        Ok(window)
    }

    pub fn get_pool_by_tki(&self, tki: &String) -> Result<&r2d2::Pool<SqliteConnectionManager>, String> {
        let window = self.pools.get(tki).unwrap();
        Ok(window)
    }

    pub fn get_spec_pool(&self, tki: String) -> Result<&r2d2::Pool<SqliteConnectionManager>, String> {
        let window = self.pools.get(&tki).unwrap();
        Ok(window)
    }

    pub fn get_conn_by_tki(&self, tki: &String) -> Result<r2d2::PooledConnection<SqliteConnectionManager>, Error> {
        match self.pools.get(tki) {
            Some(res) => Ok(res.get().unwrap()),
            None => {
                bail!("ERR_NO_CONN");
            }
        }
    }

    pub fn get_event_pool(&self) -> Result<&r2d2::Pool<SqliteConnectionManager>, String> {
        let db_id: String = "EVENTS".to_string();
        Ok(self.pools.get(&db_id).unwrap())
    }

    /*
    pub fn get_did(&self) -> String {
        self.did.clone()
    }
    */

    pub fn get_account(&self) -> Account {
        self.account.clone().unwrap()
    }

    pub fn get_account_by_tki(&self, tki: &String) -> Result<Account, Error> {
        let mut result: Option<Account> = Option::None;
        for item in &self.config.accounts {
            if &item.tki == tki {
                result = Some(item.clone());
            }
        }

        if let Some(c) = result {
            Ok(c)
        } else {
            bail!("ACCOUNT_NOT_FOUND");
        }
    }

    pub fn get_current_uid(&self) -> Uuid {
        self.user.clone().unwrap().uid
    }

    // Get the uuid for the user of the currently active account
    pub fn get_current_uuid(&self) -> Uuid {
        self.user.clone().unwrap().uid
    }
}



