use std::collections::HashMap;
use std::io::{self, Error};

use uuid::Uuid;
use tokio::codec::{Encoder, Decoder};
use bytes::{BytesMut};
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;

use crate::codec::{Tenancy};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AccountUser {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub email: String,
    pub name: String,
    pub password: String,
    pub role: String,
    pub org_id: Option<Uuid>,
    pub tki: String,
    pub status: String,
    pub token: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Account {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub domain: String,
    pub tki: String,
}

#[derive(Debug, Message, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum RemoteReq {
    Ok,
    Ping,
    Pong,
    Command {
        cmd: String,
        args: Value,
        tki: Option<String>,
        uid: Option<Uuid>,
    },
    RegistrationRequest {

    },
    AuthRequest {
        id: Uuid,
        email: String,
        password: String,
    },
    SeedRequest {
        id: Uuid,
        tki: String
    },
    Authenticate {
        tokens: Vec<String>,
    }
}

#[derive(Debug, Message, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum RemoteResponse {
    Ok,
    Ping,
    Pong,
    Authenticated,
    AuthenticationResult {
        id: Uuid,
        success: bool,
        users: Vec<AccountUser>,
        accounts: Vec<Account>,
        error: Option<String>,
    },
    SeedData {
        id: Uuid,
        protocol: i32,
        tki: String,
        data: HashMap<String, Vec<Value>>,
        ts_ms: usize
    },
}

pub struct RemoteCodec;

impl Decoder for RemoteCodec {
    type Item = RemoteResponse;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {

        if let Some(index) = src.iter().position(|byte| *byte == b'\n') {
            let line = src.split_to(index + 1);
            let item = match json::from_slice(&line[0..line.len() - 1]) {
                Ok(res) => res,
                Err(err) => {
                    eprintln!("{:?}", err);
                    panic!()
                }
            };

            Ok(Some(item))
        } else {
            Ok(None)
        }
    }
}

impl Encoder for RemoteCodec {
    type Item  = RemoteReq;
    type Error = Error;

    fn encode(&mut self, msg: RemoteReq, dst: &mut BytesMut) -> Result<(), Self::Error> {
        let mut vec = json::to_vec(&msg)?;
        vec.push(b'\n');
        dst.extend_from_slice(&vec);
        Ok(())
    }
}
