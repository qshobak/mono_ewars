use std::io::{Error};
use std::collections::VecDeque;
use std::collections::HashMap;
use std::time::{Duration, Instant};
use std::cell::{RefCell};
use std::rc::{Rc};

use actix::prelude::*;
use actix::actors::resolver::Resolver;
use actix::actors::resolver::{Connect};
use tokio::net::{TcpStream};
use tokio::io::{WriteHalf, AsyncRead};
use tokio::codec::{FramedRead};
use actix::io as act_io;

use serde_json::Value;
use uuid::Uuid;

use crate::remote_client::{RemoteCodec, RemoteReq, RemoteResponse};
use crate::peer_server::{PeerConnection};
use crate::codec::{Codec};

use crate::sonoma::Application;
use crate::state::ApplicationState;


static DEFAULT_CANON_IP: [u8; 4] = [127, 0, 0, 1];

pub type RemoteFramed = act_io::FramedWrite<WriteHalf<TcpStream>, RemoteCodec>;

pub struct RemoteClient {
    pub framed: Option<RemoteFramed>,
    pub queue: VecDeque<Codec>,
    pub timeout: u64,
    pub endpoint: String,
    tokens: Vec<String>,
    authenticated: bool,
    requests: HashMap<Uuid, Addr<PeerConnection>>,
    state: Rc<RefCell<ApplicationState>>,
    app: Addr<Application>,
}

impl actix::io::WriteHandler<Error> for RemoteClient {
    fn error(&mut self, err: Error, _: &mut Self::Context) -> Running {
        eprintln!("Remote: Connection dropped:{} error: {}", self.endpoint, err);
        Running::Stop
    }
}

impl Actor for RemoteClient {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        eprintln!("Remote Client: Starting {}", self.endpoint);
        eprintln!("{}", self.endpoint);
        Resolver::from_registry()
            .send(Connect::host(&self.endpoint))
            .into_actor(self)
            .map(|res, act, ctx|  match res {
                Ok(stream) => {
                    eprintln!("Remote Client: Connected to remote");

                    let (r, w) = stream.split();

                    ctx.add_stream(FramedRead::new(r, RemoteCodec));
                    act.framed = Some(act_io::FramedWrite::new(w, RemoteCodec, ctx));
                    act.reset_timeout();

                    //act.authenticate(ctx);
                    //act.hb(ctx);
                },
                Err(err) => {
                    eprintln!("Remote: Error creating stream: {:?}", err);
                    act.restart_later(ctx);
                }
            })
            .map_err(|err, act, ctx| {
                eprintln!("Remote: Error opening socket: {:?}", err);
                act.restart_later(ctx);
            }).wait(ctx);
    }
}

impl Supervised for RemoteClient {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Remote: Restarting...");
        self.framed.take();
    }
}

impl RemoteClient {
    pub fn new(state: Rc<RefCell<ApplicationState>>, app: Addr<Application>, endpoint: String, tokens: Vec<String>) -> Self {
        let endpoint: String = format!("{}:6142", endpoint);
        RemoteClient {
            framed: Option::None,
            queue: VecDeque::new(),
            timeout: 0,
            endpoint,
            tokens,
            authenticated: false,
            requests: HashMap::new(),
            state: state.clone(),
            app,
        }
    }

    #[inline]
    pub fn reset_timeout(&mut self) {
        self.timeout = 0;
    }

    pub fn authenticate(&mut self, ctx: &mut Context<Self>) {
        let tokens = self.tokens.clone();
        // Only authenticate if there's a need to
        if tokens.len() > 0 {
            if let Some(ref mut x) = self.framed {
                x.write(RemoteReq::Authenticate {
                    tokens,
                });
            }
        }
    }

    pub fn restart_later(&mut self, ctx: &mut Context<Self>) {
        eprintln!("Remote Client: Restarting...");
        const TIMEOUT_INTERVAL: u64 = 10;
        const TIMEOUT_MAX: u64 = 90;

        if self.timeout < TIMEOUT_MAX {
            self.timeout += TIMEOUT_INTERVAL;
        }

        ctx.run_later(Duration::new(self.timeout, 0), |_, ctx| ctx.stop());
    }

    fn handle_command(&mut self, ctx: &mut Context<Self>, cmd: String, data: Value, tki: Option<String>, uid: Option<Uuid>) {
        eprintln!("{}", cmd);
    }

    fn start_job<'a, 'b>(&mut self, ctx: &mut Context<Self>, did: String, data: Value) {
        unimplemented!()
    }

    fn authenticated(&mut self, ctx: &mut Context<Self>) {
        self.authenticated = true;
    }

    // Send a request to authenticate a users credentials and get information about tenancies that
    // the user holds
    fn auth_request(&mut self, ctx: &mut Context<Self>, id: &Uuid, email: &String, password: &String, addr: &Addr<PeerConnection>) -> Result<Codec, ()> {
        eprintln!("{}, {}", email, password);
        if let Some(ref mut x) = self.framed {
            dbg!("HERE");
            self.requests.insert(id.clone(), addr.clone());
            x.write(RemoteReq::AuthRequest {
                id: id.clone(),
                email: email.clone(),
                password: password.clone(),
            });
        }
        Ok(Codec::Ok)
    }

    // Send a request for the seed for an account
    fn seed_request(&mut self, ctx: &mut Context<Self>, id: &Uuid, tki: &String, addr: &Addr<PeerConnection>) -> Result<Codec, ()> {
        if let Some(ref mut x) = self.framed {
            eprintln!("CANON: Sending Seed reques");
            self.requests.insert(id.clone(), addr.clone());
            x.write(RemoteReq::SeedRequest {
                id: id.clone(),
                tki: tki.clone(),
            });
        }
        Ok(Codec::Ok)
    }
}

impl Handler<Codec> for RemoteClient {
    type Result = Result<Codec, ()>;

    fn handle(&mut self, msg: Codec, ctx: &mut Context<Self>) -> Self::Result {
        eprintln!("Remote Client: HERE");
        match msg {
            Codec::AuthRequest {id, email, password, addr} => self.auth_request(ctx, &id, &email, &password, &addr),
            Codec::SeedRequest {id, tki, addr} => self.seed_request(ctx, &id, &tki, &addr),
            _ => {
                Err(())
            }
        }
    }
}

impl StreamHandler<RemoteResponse, Error> for RemoteClient {
    fn error(&mut self, error: Error, ctx: &mut Self::Context) -> actix::Running {
        eprintln!("Remote CLient: IO Error: {}", error);
        Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        eprintln!("Remote Client: Connection is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: RemoteResponse, ctx: &mut Self::Context) {
        match msg {
            RemoteResponse::Authenticated => self.authenticated(ctx),
            RemoteResponse::SeedData { id, protocol, tki, data, ts_ms} => {
                eprintln!("TS_MS: {}", ts_ms);
                eprintln!("RECV_SEED_DATA");

                if let Some(addr) = self.requests.get(&id) {
                    addr.do_send(Codec::SeedData {
                        id: id.clone(),
                        protocol,
                        tki: tki.clone(),
                        data: data.clone(),
                        ts_ms: ts_ms.clone(),
                    });
                }
            },
            RemoteResponse::AuthenticationResult { id, success, error, users, accounts} => {
                if let Some(addr) = self.requests.get(&id) {
                    addr.do_send(Codec::AuthenticationResult {
                        id: id.clone(),
                        success: success,
                        error: error,
                        users: users,
                        accounts: accounts,
                    });
                }
            },
            _ => {
                eprintln!("Remote Client: Got Response");
            }
        }
    }
}
