use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use uuid::Uuid;
use serde_json::Value;

use sonomalib::models::{Document};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;


pub fn get_by_id(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: Uuid = json::from_value(args).unwrap();

    match Document::get_by_id(&conn, "", &id) {
        Ok(res) => {
            Ok(json::to_value(res).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

