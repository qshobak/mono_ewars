use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::document::Document;

pub fn update_document(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Document = serde_json::from_value(args).unwrap();

    let sql = r#"
        UPDATE documents
            SET template_name = :template_name,
                instance_name = :instance_name,
                status = :status,
                layout = :layout,
                content = :content,
                data = :data,
                generation = :generation,
                orientation = :orientation,
                template_type = :template_type,
                permissions = :permissions,
                modified_by = :modified_by
         WHERE uuid = :uuid;
    "#.to_owned();

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":template_name", &data.template_name),
        (":instance_name", &data.instance_name),
        (":status", &data.status),
        (":layout", &data.layout),
        (":content", &data.content),
        (":data", &data.data),
        (":generation", &data.generation),
        (":orientation", &data.orientation),
        (":template_type", &data.template_type),
        (":permissions", &data.permissions),
        (":modified_by", &uid),
    ]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn delete_document(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = serde_json::from_value(args).unwrap();

    conn.execute("DELETE FROM docuements WHERE uuid = ?", &[&id]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn create_document(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Document = serde_json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO documents
        (template_name, instance_name, status, version, layout, content, data, generation, orientation, template_type, permissions, created_by, modified_by)
        VALUES (:template_name, :instance_name, :status, :version, :layout, :content, :data, :generation, :orientation, :template_type, :permissinos, :created_by, :modified_by)
        RETURNING *;
    "#.to_owned();

    let uid = app.borrow().get_current_uuid();

    let result: Document = conn.query_row_named(
        &sql,
        &[
            (":template_name", &data.template_name),
            (":instance_name", &data.instance_name),
            (":status", &data.status),
            (":version", &data.version),
            (":layout", &data.layout),
            (":content", &data.content),
            (":data", &data.data),
            (":generation", &data.generation),
            (":orientation", &data.orientation),
            (":template_type", &data.template_type),
            (":permissions", &data.permissions),
            (":created_by", &uid),
            (":modified_by", &uid),
        ],
        |row| {
            Document::from_row(row)
        }).unwrap();

    Ok(serde_json::to_value(result).unwrap())
}
