use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Task {
    uuid: String,
    status: String,
    task_type: Option<String>,
    data: Option<Value>,
    actions: Option<Value>,
    version: i32,
    form: Option<Value>,
    context: Option<Value>,
    outcome: Option<Value>,
    actioned: Option<String>,
    actioned_by: Option<String>,
    created: String,
    modified: String,
    modified_by: Option<String>,
}

pub fn seed_tasks(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<Task> = match json::from_value(args) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            return Err(format!("{:?}", err));
        }
    };

    let sql = r#"
        INSERT OR REPLACE INTO tasks
        (uuid, status, task_type, data, actions, version, form, context, outcome, actioned, actioned_by, created, modified, modified_by)
        VALUES (:uuid, :status, :task_type, :data, :actions, :version, :form, :context, :outcome, :actioned, :actioned_by, :created, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":status", &data.status),
            (":task_type", &data.task_type),
            (":data", &data.data),
            (":actions", &data.actions),
            (":version", &data.version),
            (":form", &data.form),
            (":context", &data.context),
            (":outcome", &data.outcome),
            (":actioned", &data.actioned),
            (":actioned_by", &data.actioned_by),
            (":created", &data.created),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}

