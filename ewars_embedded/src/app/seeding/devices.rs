use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Device {
    uuid: String,
    device_type: String,
    last_seed: Option<String>,
    did: Option<String>,
    uid: Option<String>,
    info: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

pub fn seed_devices(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<Device> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO devices
        (uuid, device_type, last_seed, did, uid, info, created, created_by, modified, modified_by)
        VALUES (:uuid, :device_type, :last_seed, :did, :uid, :info, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":device_type", &data.device_type),
            (":last_seed", &data.last_seed),
            (":did", &data.did),
            (":uid", &data.uid),
            (":info", &data.info),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
