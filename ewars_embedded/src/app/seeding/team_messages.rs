use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct TeamMessage {
    uuid: String,
    ts_ms: i32,
    tid: String,
    content: Option<Value>,
    pid: Option<String>,
    created: String,
    created_by: String,
    modified: String,
    modified_by: String,
}

pub fn seed_team_messages(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<TeamMessage> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO team_messages
        (uuid, ts_ms, tid, content, pid, created, created_by, modified, modified_by)
        VALUES (:uuid, :ts_ms, :tid, :content, :pid, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":ts_ms", &data.ts_ms),
            (":tid", &data.tid),
            (":content", &data.content),
            (":pid", &data.pid),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}

