use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use sonomalib::models::{Alarm};
use sonomalib::traits::{Insertable};

use crate::state::ApplicationState;

pub fn seed_alarms(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let items: Vec<Alarm> = json::from_value(args).unwrap();

    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();
    for data in items {
        data.insert(&conn, Option::None).unwrap();
    }

    Ok(true)
}
