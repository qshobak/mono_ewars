use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

static SQL_SEED_ASSIGNMENT: &'static str = r#"
    INSERT OR REPLACE INTO assignments
    (uuid, uid, status, fid, lid, assign_type, created, created_by, modified, modified_by)
    VALUES (:uuid, :uid, :status, :fid, :lid, :assign_type, :created, :created_by, :modified, :modified_by);
"#;

#[derive(Debug, Deserialize)]
struct Assignment {
    uuid: String,
    uid: String,
    status: String,
    fid: Option<String>,
    lid: Option<String>,
    assign_type: String,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}


pub fn seed_assignments(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let data: Vec<Assignment> = json::from_value(args).unwrap();

    for data in data {
        conn.execute_named(&SQL_SEED_ASSIGNMENT, &[
            (":uuid", &data.uuid),
            (":uid", &data.uid),
            (":status", &data.status),
            (":fid", &data.fid),
            (":lid", &data.lid),
            (":assign_type", &data.assign_type),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
