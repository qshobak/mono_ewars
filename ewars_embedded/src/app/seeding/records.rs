use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Record {
    uuid: String,
    status: String,
    stage: Option<String>,
    workflow: Option<Value>,
    fid: String,
    data: Option<Value>,
    submitted: String,
    submitted_by: Option<String>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
    revisions: Option<Value>,
    comments: Option<Value>,
    eid: Option<String>,
    import_id: Option<String>,
    source: Option<String>,
}


pub fn seed_records(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let records: Vec<Record> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO records
        (uuid, status, stage, workflow, fid, data, submitted, submitted_by, created, created_by, modified, modified_by, revisions, comments, eid, import_id, source)
        VALUES (:uuid, :status, :stage, :workflow, :fid, :data, :submitted, :submitted_by, :created, :created_by, :modified, :modified_by, :revisions, :comments, :eid, :import_id, :source);
    "#.to_owned();


    for data in records {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":status", &data.status),
            (":stage", &data.stage),
            (":workflow", &data.workflow),
            (":fid", &data.fid),
            (":data", &data.data),
            (":submitted", &data.submitted),
            (":submitted_by", &data.submitted_by),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
            (":revisions", &data.revisions),
            (":comments", &data.comments),
            (":eid", &data.eid),
            (":import_id", &data.import_id),
            (":source", &data.source),
        ]).unwrap();
    }


    Ok(true)
}
