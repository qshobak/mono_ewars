use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Form {
    uuid: String,
    name: Value,
    version: i32,
    status: String,
    description: Option<String>,
    guidance: Option<String>,
    eid_prefix: Option<String>,
    features: Option<Value>,
    definition: Option<Value>,
    changes: Option<Value>,
    permissions: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}


pub fn seed_forms(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Vec<Form> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO forms
        (uuid, name, version, status, description, guidance, eid_prefix, features, definition, changes, permissions, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :version, :status, :description, :guidance, :eid_prefix, :features, :definition, :changes, :permissions, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in data {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":version", &data.version),
            (":status", &data.status),
            (":description", &data.description),
            (":guidance", &data.guidance),
            (":eid_prefix", &data.eid_prefix),
            (":features", &data.features),
            (":definition", &data.definition),
            (":changes", &data.changes),
            (":permissions", &data.permissions),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
