use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Alert {
    uuid: String,
    alid: String,
    status: String,
    eid: Option<String>,
    alert_date: String,
    lid: Option<String>,
    stage: String,
    data: Option<Value>,
    workflow: Option<Value>,
    events: Option<Value>,
    raised: String,
    closed: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

pub fn seed_alerts(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let records: Vec<Alert> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO alerts
        (uuid, alid, status, eid, alert_date, lid, stage, data, workflow, events, raised, closed, modified, modified_by)
        VALUES (:uuid, :alid, :status, :eid, :alert_date, :lid, :stage, :data, :workflow, :events, :raised, :closed, :modified, :modified_by);
    "#.to_owned();

    for data in records {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":alid", &data.alid),
            (":status", &data.status),
            (":eid", &data.eid),
            (":alert_date", &data.alert_date),
            (":lid", &data.lid),
            (":stage", &data.stage),
            (":data", &data.data),
            (":workflow", &data.workflow),
            (":events", &data.events),
            (":raised", &data.raised),
            (":closed", &data.closed),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
