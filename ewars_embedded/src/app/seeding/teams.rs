use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Team {
    uuid: String,
    name: String,
    description: Option<String>,
    private: bool,
    status: String,
    permissions: Option<Value>,
    members: Option<Value>,
    created: String,
    created_by: String,
    modified: String,
    modified_by: String,
}

pub fn seed_teams(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<Team> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO teams
        (uuid, name, description, private, status, permissions, members, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :description, :private, :status, :permissions, :members, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":description", &data.description),
            (":private", &data.private),
            (":status", &data.status),
            (":permissions", &data.permissions),
            (":members", &data.members),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
