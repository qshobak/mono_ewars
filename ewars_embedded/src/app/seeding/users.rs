use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct User {
    uuid: String,
    role: String,
    permissions: Option<Value>,
    settings: Option<Value>,
    profile: Option<Value>,
    org_id: Option<String>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
    status: String,
    name: String,
    email: String,
    password: String,
    system: bool,
}

pub fn seed_users(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    println!("HERE");
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO users
        (uuid, role, permissions, settings, profile, org_id, created, created_by, modified, modified_by, status, name, email, password, system)
        VALUES (:uuid, :role, :permissions, :settings, :profile, :org_id, :created, :created_by, :modified, :modified_by, :status, :name, :email, :password, :system);
    "#.to_owned();

    let items: Vec<User> = json::from_value(args).unwrap();

    for data in &items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":role", &data.role),
            (":permissions", &data.permissions),
            (":settings", &data.settings),
            (":profile", &data.profile),
            (":org_id", &data.org_id),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
            (":status", &data.status),
            (":name", &data.name),
            (":email", &data.email),
            (":password", &data.password),
            (":system", &data.system),
        ]).unwrap();
    }

    Ok(true)
}
