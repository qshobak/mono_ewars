use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Organization {
    uuid: String,
    name: Value,
    description: Option<String>,
    acronym: Option<String>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

static SQL_INSERT_ORG: &'static str = r#"
    INSERT OR REPLACE INTO organizations
    (uuid, name, description, acronym, created, created_by, modified, modified_by)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?);
"#;


pub fn seed_organizations(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let records: Vec<Organization> = json::from_value(args).unwrap();

    for data in records {
        conn.execute(&SQL_INSERT_ORG, &[
            &data.uuid,
            &data.name,
            &data.description,
            &data.acronym,
            &data.created,
            &data.created_by,
            &data.modified,
            &data.modified_by,
        ]).unwrap();
    }

    Ok(true)
}
