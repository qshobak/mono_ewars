use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::task::Task;

pub fn get_task(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let result: Task = conn.query_row("SELECT * FROM tasks WHERE uuid = ?", &[&id], |row| {
        Task::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}

pub fn query_tasks(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn get_all_tasks(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut items: Vec<Task> = Vec::new();

    let mut stmt = conn.prepare("SELECT * FROM tasks WHERE status = 'OPEN'").unwrap();

    let rows = stmt.query_map(&[], |row| {
        Task::from_row(row)
    }).unwrap();

    for item in rows {
        items.push(item.unwrap());
    }

    let result: Value = json::to_value(items).unwrap();

    Ok(result)
}
