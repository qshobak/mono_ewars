use std::cell::RefCell;
use std::rc::Rc;
use std::error::Error;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::task::Task;

pub fn action_task(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn delete_task(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn get_task(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args.get(0).unwrap().clone()).unwrap();

    let task: Task = conn.query_row(r#"
        SELECT * FROM tasks WHERE uuid = ?;
    "#, &[&id], |row| {
        Task::from_row(row)
    }).unwrap();

    Ok(json::to_value(task).unwrap())
}

// Create a new task within the system
pub fn create_task(app: Rc<RefCell<ApplicationState>>, ti: Task) -> Result<bool, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    Ok(true)
}
