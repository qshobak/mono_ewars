use serde_derive;
use serde_json;
use serde_json::Value;

use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ResourceQuery {
    pub filters: HashMap<String, (String, String, String)>,
    pub sorting: HashMap<String, (String, String)>,
    pub limit: i32,
    pub offset: i32,
}

pub type StringNames = Vec<String>;