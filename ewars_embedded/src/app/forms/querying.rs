use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::Row;
use rusqlite::{NO_PARAMS};

use sonomalib::models::{Form};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;
use crate::models::query::{QueryResult, ResourceQuery};

#[derive(Debug, Serialize, Deserialize)]
pub struct FormRow {
    pub uuid: String,
    pub name: Value,
    pub status: String,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

impl FormRow {
    pub fn from_row(row: &Row) -> FormRow {
        FormRow {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            created: row.get(3),
            created_by: row.get(4),
            modified: row.get(5),
            modified_by: row.get(6),
        }
    }
}


// Get a full form by its uuid
pub fn get_form(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let id: Uuid = json::from_value(args).unwrap();

    match Form::get_by_id(&conn, "", &id) {
        Ok(Some(res)) => Ok(json::to_value(res).unwrap()),
        Ok(Option::None) => Ok(json::to_value(false).unwrap()),
        Err(err) => bail!(err)
    }
}


pub fn query_forms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let query: ResourceQuery = json::from_value(args).unwrap();

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "f.uuid,".to_string(),
        "f.name,".to_string(),
        "f.status,".to_string(),
        "f.created,".to_string(),
        "f.created_by,".to_string(),
        "f.modified,".to_string(),
        "f.modified_by".to_string(),
        "FROM forms AS f".to_string(),
    ];

    let mut sql_count: Vec<String> = vec![
        "SELECT".to_string(),
        "COUNT(f.uuid) AS total_records".to_string(),
        "FROM forms AS f".to_string(),
    ];

    let mut wheres: Vec<String> = Vec::new();
    let mut orders: Vec<String> = Vec::new();

    for (key, val) in query.filters.clone() {
        let (cmp, el_type, val) = query.filters.get(&key).unwrap();

        let mut flt_str: Vec<String> = Vec::new();

        flt_str.push(format!("f.{}", key));

        match cmp.as_ref() {
            "eq" => {
                flt_str.push("=".to_string());
            },
            "neq" => {
                flt_str.push("!=".to_string());
            },
            _ => {
                flt_str.push("=".to_string());
            }
        }

        flt_str.push(format!("'{}'", val));

        wheres.push(flt_str.join(" "));
    }


    for (key, val) in query.orders.clone() {
        let (col_type, dir) = query.orders.get(&key).unwrap();

        let mut ord_str: Vec<String> = Vec::new();

        ord_str.push(format!("f.{}", key));

        ord_str.push(dir.to_string());

        orders.push(ord_str.join(" "));
    }

    if wheres.len() > 0 {
        sql.push("WHERE".to_string());
        sql_count.push("WHERE".to_string());

        let mut sql_f = true;
        for item in wheres {
            if sql_f != true {
                sql.push("AND".to_string());
                sql_count.push("AND".to_string());
            }
            sql.push(item.clone());
            sql_count.push(item.clone());
            sql_f = false;
        }
    }

    if orders.len() > 0 {
        sql.push("ORDER BY".to_string());
        sql.push(orders.join(", "));
    }

    sql.push(format!("LIMIT {}", query.limit));
    sql.push(format!("OFFSET {}", query.offset));

    let c_sql = sql.join("\n");
    let c_sql_count = sql_count.join("\n");

    let mut stmt = match conn.prepare(&c_sql) {
        Ok(res) => res,
        Err(err) => bail!(err),
    };

    let rows = stmt.query_map(NO_PARAMS, |row| {
        FormRow::from_row(row)
    }).unwrap();

    let results: Vec<FormRow> = rows.map(|x| x.unwrap()).collect();

    let counter: i32 = conn.query_row(&c_sql_count, NO_PARAMS, |row| {
        row.get(0)
    }).unwrap();

    let qr = QueryResult {
        records: json::to_value(results).unwrap(),
        count: counter,
    };

    Ok(json::to_value(qr).unwrap())
}

static SQL_GET_AVAILABLE: &'static str = r#"
    SELECT * FROM forms WHERE status = 'ACTIVE';
"#;

pub fn get_available_forms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_AVAILABLE)?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Form::from(row)
    })?;
    let items: Vec<Form> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(items).unwrap();

    Ok(output)
}

static SQL_GET_ACTIVE_FORMS: &'static str = r#"
    SELECT *
    FROM forms
    WHERE (status = 'ACTIVE' OR status = 'ARCHIVED');
"#;

// Get a list of active forms within the system
pub fn get_active_forms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ACTIVE_FORMS)?;

    let rows = match stmt.query_map(NO_PARAMS, |row| {
        Form::from(row)
    }) { 
        Ok(res) => res,
        Err(err) => bail!(err)
    };

    let results: Vec<Form> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}
