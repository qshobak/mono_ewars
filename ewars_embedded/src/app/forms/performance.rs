use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use chrono::prelude::*;
use chrono::Duration;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::field::Field;

#[derive(Debug, Deserialize)]
pub struct FormInfo {
    pub uuid: String,
    pub name: Value,
    pub definition: Value,
}

#[derive(Debug, Serialize)]
pub struct DataPoint {
    pub dd: String,
    pub c: i32,
}

#[derive(Debug, Serialize)]
pub struct Output {
    pub results: HashMap<String, i32>,
    pub max: i32,
    pub total_submissions: i32,
}

#[derive(Debug, Serialize)]
pub struct Statistics {
    pub completeness: i32,
    pub timeliness: i32,
    pub overdue: i32,
    pub missing: i32,
    pub on_time: i32,
    pub locations: i32,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct FeatureDefinition {
    pub site_type_id: Option<String>,
    pub interval: Option<String>,
    pub approval: Option<bool>,
    pub block_future_dates: Option<bool>,
    pub field: Option<String>,
    pub threshold: Option<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FormDefinition {
    pub uuid: String,
    pub constraints: Vec<String>,
    pub definition: HashMap<String, Field>,
    pub features: HashMap<String, FeatureDefinition>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReportingLocation {
    pub uuid: String,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    pub status: String,
}

struct ReportingRecord {
    submitted: String,
    dd: Option<DateTime<Utc>>,
    lid: Option<String>,
}

pub fn get_form_submissions(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let form_id: String = json::from_value(args).unwrap();

    let mut sql = r#"
        SELECT COUNT(uuid), DATE(submitted)
        FROM records
        WHERE fid = ?
        GROUP BY DATE(submitted)
        ORDER BY submitted;
    "#.to_owned();

    let mut stmt = conn.prepare(&sql).unwrap();

    let mut data: HashMap<String, i32> = HashMap::new();

    let rows = stmt.query_map(&[&form_id], |row| {
        (row.get(1), row.get(0))
    }).unwrap();

    let mut max_value: i32 = 0;
    let mut total: i32 = 0;
    for item in rows {
        let point: (String, i32) = item.unwrap();
        if point.1 > max_value {
            max_value = point.1.clone();
        }
        total += point.1;
        data.insert(point.0, point.1);
    }

    let result = Output {
        results: data,
        max: max_value,
        total_submissions: total
    };
    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}

pub fn get_form_performance(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let form_id: String = json::from_value(args).unwrap();

    let sql = r#"
        SELECT uuid,
            json_extract(definition, '$.constraints') AS constraints,
            json_extract(definition, '$.definition') as definition,
            features
        FROM forms
        WHERE uuid = ?;
    "#.to_owned();

    let form: FormDefinition = conn.query_row(&sql, &[&form_id], |row| {
        let constraints: Vec<String> = json::from_value(row.get(1)).unwrap();
        let definition: HashMap<String, Field> = json::from_value(row.get(2)).unwrap();
        let features: HashMap<String, FeatureDefinition> = json::from_value(row.get(3)).unwrap();
        FormDefinition {
            uuid: row.get(0),
            constraints,
            definition,
            features,
        }
    }).unwrap();

    let mut has_date: bool = false;
    let mut has_location: bool = false;
    let mut location_type: String = String::new();
    let mut interval: String = String::new();

    // Check if the field has location reporting
    if form.features.contains_key("LOCATION_REPORTING") {
        has_location = true;

        let field: Field = form.definition.get("__lid__").unwrap().clone();

        location_type = field.lti.unwrap().clone();
    }

    // Check if the field has interval reporting
    if form.features.contains_key("INTERVAL_REPORTING") {
        has_date = true;

        let field: Field = form.definition.get("__dd__").unwrap().clone();

        interval = field.interval.unwrap().clone();
    }

    let mut sql = r#"
        SELECT l.uuid, DATE(r.start_date), IFNULL(r.end_date, DATE(CURRENT_TIMESTAMP)), r.status
        FROM locations AS l
            LEFT JOIN reporting AS r
        WHERE l.lti = ?
            AND l.status = 'ACTIVE'
            AND r.fid = ?;
    "#.to_owned();

    let mut stmt = conn.prepare(&sql).unwrap();

    let location_rows = stmt.query_map(&[&location_type, &form_id], |row| {
        ReportingLocation {
            uuid: row.get(0),
            start_date: row.get(1),
            end_date: row.get(2),
            status: row.get(3),
        }
    }).unwrap();

    let reporting_locations: Vec<ReportingLocation> = location_rows.map(|x| x.unwrap()).collect();

    let mut sql = r#"
        SELECT
            DATE(submitted),
            json_extract(data, '$.__dd__') AS dd,
            json_extract(data, '$.__lid__') as lid
        FROM records
        WHERE fid = ?;
    "#.to_owned();

    let mut rec_stmt = conn.prepare(&sql).unwrap();

    let records_rows = rec_stmt.query_map(&[&form_id], |row| {
        ReportingRecord {
            submitted: row.get(0),
            dd: row.get(1),
            lid: row.get(2),
        }
    }).unwrap();

    let records: Vec<ReportingRecord> = records_rows.map(|x| x.unwrap()).collect();

    let mut expected: HashMap<Date<Utc>, i32> = HashMap::new();
    let mut submitted: HashMap<Date<Utc>, i32> = HashMap::new();

    let mut earliest_date: DateTime<Utc> = Utc::now();
    let today: DateTime<Utc> = Utc::now();

    let mut expected_days: Vec<DateTime<Utc>> = Vec::new();

    for loc in &reporting_locations {
        if loc.start_date.date() < earliest_date.date() {
            earliest_date = loc.start_date.clone();
        }
    }

    expected_days.push(earliest_date.clone());
    let mut cur_date: DateTime<Utc> = earliest_date.clone();

    loop {
        cur_date = cur_date + Duration::days(1);
        expected_days.push(cur_date.clone());

        if cur_date.date() == today.date() {
            break;
        }
    }

    for record in records {
        if let Some(ref x) = record.dd {
            submitted.entry(x.date())
                .and_modify(|x| *x += 1)
                .or_insert(1);
        }
    }

    for loc in &reporting_locations {
        for dt in &expected_days {
            if loc.start_date.date() <= dt.date() && dt.date() <= loc.end_date.date() {
                expected.entry(dt.date())
                    .and_modify(|x| *x += 1)
                    .or_insert(1);
            }
        }
    }


    Ok(json::to_value(true).unwrap())

}
