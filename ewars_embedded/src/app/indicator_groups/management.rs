use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::indicator_group::IndicatorGroup;

pub fn update_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: IndicatorGroup = json::from_value(args).unwrap();

    let sql = r#"
        UPDATE indicator_groups
            SET name = :name,
                description = :description,
                pid = :pid,
                permissions = :permissions,
                modified_by = :uid
        WHERE uuid = :uuid;
    "#;

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":name", &data.name),
        (":description", &data.description),
        (":pid", &data.pid),
        (":permissions", &data.permissions),
        (":modified_by", &uid),
    ]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn create_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: IndicatorGroup = json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO indicator_groups
        (uuid, name, description, pid, permissions, created_by, modified_by)
        VALUES (:uuid, :name, :description, :pid, :permissions, :created_by, :modified_by)
        RETURNING *;
    "#;

    let uid = app.borrow().get_current_uuid();

    let result: IndicatorGroup = conn.query_row_named(&sql, &[
        (":uuid", &data.uuid),
        (":name", &data.name),
        (":description", &data.description),
        (":pid", &data.pid),
        (":permissions", &data.permissions),
        (":created_by", &uid),
        (":modified_by", &uid),
    ], |row| {
        IndicatorGroup::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}


pub fn delete_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM indicator_groups WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}
