use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize)]
pub struct SubmissionsResult {
    pub uuid: String,
    pub name: Value,
    pub results: Vec<(String, i32)>,
}


struct Form {
    pub uuid: String,
    pub name: Value,
    pub constraints: Vec<String>,
    pub definition: HashMap<String, Value>,
}

pub fn get_form_submissions(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let form_id: String = json::from_value(args).unwrap();

    let mut sql = r#"
        SELECT f.uuid,
                json_extract(f.definition, '$.constraints') AS constraints,
                json_extract(f.definition, '$.definition') AS definition
        FROM forms AS f
        WHERE f.uuid = ?;
    "#.to_owned();

    let form: Form = conn.query_row(&sql, &[&form_id], |row| {
        let constraints: Vec<String> = json::from_value(row.get(2)).unwrap();
        let definition: HashMap<String, Value> = json::from_value(row.get(3)).unwrap();
        Form {
            uuid: row.get(0),
            name: row.get(1),
            constraints,
            definition,
        }
    }).unwrap();

    if form.constraints.len() > 0 {
        // This form has constraints, we need to figure out if we can use them for
        // We actually don't care about this at the moment as we're going to look
        // at the submitted date rather than the other items
        eprintln!("HAS CONSTRAINTS");
    }

    let mut sql = r#"
        SELECT COUNT(r.uuid), r.submitted
        FROM records AS r
        WHERE r.fid = ?
        GROUP BY r.submitted;
    "#.to_owned();

    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(&[&form_id], |row| {
        (row.get(0), row.get(1))
    }).unwrap();

    let results: Vec<(String, i32)> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)

}

// Get the count of records submitted at a given locateion
pub fn get_form_submissions_at_location(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

// Get the count of submissions for a given form at a specific date
pub fn get_form_submissions_at_date(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}
