use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use std::rc::Rc;
use std::cell::RefCell;

use crate::models::{Role};
use crate::state::ApplicationState;


pub fn create_role(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Role = json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO roles
        (uuid, name, status, description, forms, permissions, inh, created_by, modified_by)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
    "#.to_owned();

    let new_uuid: Uuid = Uuid::new_v4();
    let uid = app.borrow().get_current_uuid();

    conn.execute(&sql, &[
        &new_uuid,
        &data.name,
        &data.status,
        &data.description,
        &data.forms,
        &data.permissions,
        &data.inh,
        &uid,
        &uid,
    ]).unwrap();

    let result: Role = Role::get_role_by_id(&conn, &new_uuid).unwrap();
    Ok(json::to_value(result).unwrap())

}

pub fn delete_role(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM roles WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn update_role(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Role = json::from_value(args).unwrap();

    let sql = r#"
        UPDATE roles
            SET name = ?,
                status = ?,
                description = ?,
                forms = ?,
                permissions = ?,
                modified_by = ?,
                modified = CURRENT_TIMESTAMP
        WHERE uuid = ?;
    "#.to_owned();

    let uid = app.borrow().get_current_uuid();

    conn.execute(&sql, &[
        &data.name,
        &data.status,
        &data.description,
        &data.forms,
        &data.permissions,
        &uid,
        &data.uuid,
    ]).unwrap();


    Ok(json::to_value(true).unwrap())
}
