use std::rc::Rc;
use std::cell::RefCell;

use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use crate::models::assignment::Assignment;
use crate::state::ApplicationState;


pub fn update_assignment(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Assignment = json::from_value(args).unwrap();

    let uid = app.borrow().get_current_uuid();

    let mut sql = r#"
        UPDATE assignments
        SET lid = ?,
            type = ?,
            status = ?,
            modified = CURRENT_TIMESTAMP,
            modified_by ?
        WHERE uuid = ?;
    "#.to_owned();

    conn.execute(&sql, &[
        &data.lid,
        &data.assign_type,
        &data.status,
        &uid,
    ]).unwrap();


    Ok(json::to_value(true).unwrap())
}

pub fn delete_assignment(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let assign_id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM assignments WHERE uuid = ?", &[&assign_id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn create_assignment(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Assignment = json::from_value(args).unwrap();
    let new_uuid: String = Uuid::new_v4().to_string();
    let ruuid = app.borrow().get_current_uuid();

    let mut sql = r#"
        INSERT INTO assignments
        (uuid, lid, type, status, created_by, modified_by)
        VALUES (:uuid, :lid, :type, :status, :created_by, :modified_by);
    "#.to_owned();

    conn.execute(&sql, &[
        &new_uuid,
        &data.lid,
        &data.assign_type,
        &data.status,
        &ruuid,
        &ruuid,
    ]).unwrap();

    let result: Assignment = conn.query_row("SELECT * FROM assignments WHERE uuid = ?", &[&new_uuid], |row| {
        Assignment::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}


