use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json as json;
use serde_json::Value;
use rusqlite::Row;

use crate::state::ApplicationState;
use crate::models::query::{ResourceQuery, QueryResult};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Assignment {
    pub uuid: String,
    pub uid: String,
    pub status: String,
    pub lid: String,
    pub assign_type: String,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
    pub name: Option<String>,
    pub email: Option<String>,
    pub location_name: Option<Value>,
}

impl Assignment {
    pub fn from_row(row: &Row) -> Assignment {
        Assignment {
            uuid: row.get(0),
            uid: row.get(1),
            status: row.get(2),
            lid: row.get(3),
            assign_type: row.get(4),
            created: row.get(5),
            created_by: row.get(6),
            modified: row.get(7),
            modified_by: row.get(8),
            name: row.get(9),
            email: row.get(10),
            location_name: row.get(11),
        }
    }
}


pub fn get_assignment(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let assign_id: String = json::from_value(args).unwrap();

    let result: Assignment = conn.query_row("SELECT * FROM assignments WHERE uuid = ?", &[&assign_id], |row| {
        Assignment::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}


pub fn query_assignments(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let query: ResourceQuery = json::from_value(args).unwrap();

    let mut sql = r#"
        SELECT r.*, u.name, u.email, l.name AS location_name
        FROM assignments AS r
            LEFT JOIN users AS u ON u.uuid = r.uid
            LEFT JOIN locations AS l ON l.uuid = r.lid
    "#.to_owned();

    let mut sql_count = r#"
        SELECT COUNT(*) AS total_records
        FROM assignments AS r
            LEFT JOIN users ON u.uuid = r.uid
            LEFT JOIN locations AS l ON l.uuid = a.lid
    "#.to_owned();

    let mut wheres: Vec<String> = Vec::new();
    let mut orders: Vec<String> = Vec::new();

    for (key, val) in query.filters.clone() {
        let (cmp, col_type, val) = query.filters.get(&key).unwrap();

        let mut flt_str: Vec<String> = Vec::new();

        match key.as_ref() {
            "name" => {
                flt_str.push("u.name".to_string());
            }
            "email" => {
                flt_str.push("u.email".to_string());
            }
            _ => {
                flt_str.push(format!("r.{}", key));
            }
        }

        let s_cmp = match cmp.as_ref() {
            "eq" => "=",
            "neq" => "!=",
            "gt" => ">",
            "gte" => ">=",
            "lt" => "<",
            "lte" => "<=",
            _ => "="
        };

        flt_str.push(s_cmp.to_string());

        flt_str.push(format!("'{}'", val));

        wheres.push(flt_str.join(" "));
    }

    for (key, val) in query.orders.clone() {
        let (col_type, dir) = query.orders.get(&key).unwrap();

        let mut ord_prop: String = "".to_string();

        match key.as_ref() {
            "name" => {
                ord_prop = "u.name".to_string();
            }
            "email" => {
                ord_prop = "u.email".to_string();
            }
            "location_name" => {
                ord_prop = format!("json_extract(l.name, '$.en')");
            }
            _ => {
                ord_prop = format!("r.{}", key);
            }
        }

        let res_ord = format!("{} {}", ord_prop, dir);
        orders.push(res_ord);
    }


    if wheres.len() > 0 {
        sql.push_str(" WHERE ");
        sql_count.push_str(" WHERE ");

        sql.push_str(&wheres.join(" AND "));
        sql_count.push_str(&wheres.join(" AND "));
    }

    if orders.len() > 0 {
        sql.push_str(" ORDER BY ");

        sql.push_str(&orders.join(", "));
    }

    sql.push_str(&format!("LIMIT {}", query.limit));
    sql.push_str(&format!("OFFSET {}", query.offset));

    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(&[], |row| {
        Assignment::from_row(row)
    }).unwrap();

    let records: Vec<Assignment> = rows.map(|x| x.unwrap()).collect();
    let output: Value = json::to_value(records).unwrap();

    let count: i32 = conn.query_row(&sql_count, &[], |row| {
        row.get(0)
    }).unwrap();

    let qr = QueryResult {
        records: output,
        count,
    };

    Ok(json::to_value(qr).unwrap())
}
