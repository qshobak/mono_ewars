use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Dashboard {
    pub uuid: String,
    pub name: String,
    pub permissions: Value,
}


pub fn get_dashboards(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let role = app.borrow().get_current_role();

    let mut results: Vec<Dashboard> = Vec::new();

    let mut stmt = conn.prepare(r#"
        SELECT uuid, name, permissions
        FROM resources
        WHERE status = 'ACTIVE' AND content_type = 'DASHBOARD';
    "#).unwrap();
    let rows = stmt.query_map(&[],|row| {
        Dashboard {
            uuid: row.get(0),
            name: row.get(1),
            permissions: row.get(2),
        }
    }).unwrap();

    results = rows.map(|x| x.unwrap()).collect();

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {}
        _ => {
            // We need to filter the results by the role
            // we return any dashboard which contains this users role as a key in it's permissions map
            // as absence of the key is taken as a sign that they don't have any kind of access.
            // more fine-grained permissions will be applied at the client-level so long as the user
            // has access to the item
            results = results.iter().filter(|dashboard| {
                let permissions: HashMap<String, Value> = json::from_value(dashboard.permissions.clone()).unwrap();
                let inner_res = permissions.contains_key(&role);
                inner_res
            }).cloned().collect();
        },
    }

    let output = json::to_value(results).unwrap();
    Ok(output)
}

