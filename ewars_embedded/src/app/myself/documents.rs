use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Document {
    pub uuid: String,
    pub template_name: String,
    pub instance_name: String,
    pub status: String,
    pub generation: Value,
    pub permissions: Value,
}

pub fn get_available_documents(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut results: Vec<Document> = Vec::new();

    let mut stmt = conn.prepare(r#"
                SELECT uuid, template_name, instance_name, status, generation, permissions
                FROM documents
                WHERE status != 'INACTIVE';
            "#).unwrap();

    let rows = stmt.query_map(&[], |row| {
        Document {
            uuid: row.get(0),
            template_name: row.get(1),
            instance_name: row.get(2),
            status: row.get(3),
            generation: row.get(4),
            permissions: row.get(5),
        }
    }).unwrap();

    results = rows.map(|x| x.unwrap()).collect();

    let role = app.borrow().get_current_role();

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {}
        _ => {
            results = results.iter().filter(|doc| {
                let permissions: HashMap<String, Value> = json::from_value(doc.permissions.clone()).unwrap();
                let inner_res = permissions.contains_key(&role);
                inner_res
            }).cloned().collect();
        }
    }

    let output = json::to_value(results).unwrap();
    Ok(output)
}
