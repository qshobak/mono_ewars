use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize)]
pub struct EventLogItem {
    pub uuid: String,
    pub ref_type: String,
    pub ref_id: String,
    pub ref_ts: i32,
    pub command: String,
    pub data: Option<Value>,
    pub source: Option<String>,
    pub ts_ms: i32,
    pub applied: Option<bool>,
    pub source_version: Option<i32>,
    pub device_id: String,
}

impl EventLogItem {
    pub fn from_row(row: &Row) -> EventLogItem {
        EventLogItem {
            uuid: row.get(0),
            ref_type: row.get(1),
            ref_id: row.get(2),
            ref_ts: row.get(3),
            command: row.get(4),
            data: row.get(5),
            source: row.get(6),
            ts_ms: row.get(7),
            applied: row.get(8),
            source_version: row.get(9),
            device_id: row.get(10),
        }
    }
}

pub fn query_events(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(r#"
        SELECT *
        FROM event_log
        ORDER BY ts_ms;
    "#).unwrap();

    let rows = stmt.query_map(&[], |row| {
        EventLogItem::from_row(row)
    }).unwrap();

    let results: Vec<EventLogItem> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

pub fn get_event(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let sql = r#"
        SELECT *
        FROM event_log
        WHERE uuid = ?;
    "#.to_owned();

    let result: EventLogItem = conn.query_row(&sql, &[&id], |row| {
        EventLogItem::from_row(row)
    }).unwrap();

    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}
