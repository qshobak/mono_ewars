use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize)]
pub struct AlertUser {
    pub uuid: String,
    pub name: String,
    pub email: String,
    pub org_name: String,
    pub org_id: String,
    pub role: String,
}

impl AlertUser {
    pub fn from_row(row: &Row) -> AlertUser {
        AlertUser {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            role: row.get(3),
            org_id: row.get(4),
            org_name: row.get(5),
        }
    }
}

#[derive(Debug)]
pub struct AlarmDef {
    pub uuid: String,
    pub permissions: Option<HashMap<String, Value>>,
}

#[derive(Debug)]
pub struct AlertDef {
    pub uuid: String,
    pub lid: Option<String>,
}

static SQL_GET_USERS: &'static str = r#"
    SELECT u.uuid, u.name, u.role, u.email, NULL AS location_id
    FROM users AS u
    WHERE u.role = 'USER";
"#;

static SQL_GET_ADMIN_USERS: &'static str = r#"
    SELECT uuid, name, role, email, a.lid AS location_id
    FROM users AS u
        LEFT JOIN assignments AS a ON a.uid = u.uuid
    WHERE u.status = 'ACTIVE' AND u.role = 'ACCOUNT_ADMIN';
"#;

static SQL_GET_GEO_ADMIN_USERS: r#"
    SELECT u.uuid, u.name, u.role, u.email, l.uuid AS location_id
    FROM users AS u
        LEFT JOIN assignments AS a ON a.uid = u.uuid
        LEFT JOIN locations AS l ON l.uuid = a.lid
    WHERE u.role = 'REGIONAL_ADMIN' AND a.assign_type = 'ADMIN'
        AND u.status = 'ACTIVE';
"#;

pub fn get_alert_users(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let lid: Uuid = json::from_value(args).unwrap();

    let location = Location::get_by_id(&conn, "", &lid).unwrap();




    let user_rows = stmt.query_row(&[], |row| {
        AlertUser::from_row(row)
    }).unwrap();

    let users: Vec<AlertUser> = user_rows.map(|x| x.unwrap()).collect();

    // If there are permissions set on the alarm, we filter to the permitted roles, then filter those remanining users to users with assignments for the location
    // of the alert
    // If there are not permissions on the alarm, we filter to users who have permissions for the location only

    let acc_admnin: String = "ACCOUNT_ADMIN".to_string();
    let reg_admin: String = "REGIONAL_ADMIN".to_string();

    let admins: Vec<AlertUser> = users.iter()
        .filter(|x| x.role == "ACCOUNT_ADMIN" || x.inh == "ACCOUNT_ADMIN")
        .clone()
        .collect();


    let admins: Vec<AlertUser> = users.iter().filter(|x| x.role == "ACCOUNT_ADMIN".to_string()).clone().collect();

}
