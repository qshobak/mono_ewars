use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use sonomalib::models::{Alert, Query, QueryResult};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;

// Get a specific alert
pub fn get_alert(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let id: Uuid = json::from_value(args).unwrap();

    match Alert::get_by_id(&conn, "", &id) {
        Ok(Some(res)) => Ok(json::to_value(res).unwrap()),
        Ok(Option::None) => bail!("ERR_NOT_FOUND"),
        Err(err) => bail!(err),
    }
}

// Query for alerts
pub fn query_alerts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let qr: Query = json::from_value(args.clone())?;

    match Alert::query(&conn, "", &qr) {
        Ok(res) => Ok(json::to_value(res).unwrap()),
        Err(err) => bail!(err),
    }
}
