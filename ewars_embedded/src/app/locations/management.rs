use serde_json as json;
use serde_json::Value;

use std::cell::RefCell;
use std::rc::Rc;

use crate::state::ApplicationState;
use crate::models::{Location, LocationUpdate, LocationCreate};



pub fn update_location(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let data: LocationUpdate = json::from_value(args.clone()).unwrap();


    match data.persist(&conn, &uid) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(_) => {
            Err("ERR_UPDATING_LOCATION".to_string())
        }
    }
}

pub fn delete_location(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM locations WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn create_location(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
    //let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    //let data: Location = json::from_value(args).unwrap();


    //let mut sql = r#"
        //INSERT INTO locations
        //(uuid, name, status, pcode, codes, groups, data, lti, pid, lineage, geometry_type, geojson, population, reporting, created_by, modified_by)
        //VALUES (:uuid, :name, :status, :pcode, :codes, :groups, :data, :lti, :pid, :lineage, :geometry_type, :geojson, :population, :reporting, :created_by, :modified_by)
        //RETURNING *;
    //"#;

    //let uid = app.borrow().get_current_uuid();

    //conn.execute_named(&sql, &[
        //(":uuid", &data.uuid),
        //(":name", &data.name),
        //(":status", &data.status),
        //(":pcode", &data.pcode),
        //(":codes", &data.codes),
        //(":groups", &data.groups),
        //(":data", &data.data),
        //(":lti", &data.lti),
        //(":pid", &data.pid),
        //(":lineage", &data.lineage),
        //(":geometry_type", &data.geometry_type),
        //(":geojson", &data.geojson),
        //(":population", &data.population),
        //(":reporting", &data.reporting),
        //(":created_by", &uid),
        //(":modified_by", &uid),
    //]).unwrap();

    //let result: Location = conn.query_row("SELECT * FROM location WHERE uuid = ?", &[&data.uuid], |row| {
        //Location::from_row(row)
    //}).unwrap();

    //Ok(json::to_value(result).unwrap())
}
