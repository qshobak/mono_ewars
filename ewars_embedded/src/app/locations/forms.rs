use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::{Row};
use rusqlite::types::{ToSql, FromSql};

use sonomalib::models::{Field, FeatureDefinition};

use crate::state::ApplicationState;

static SQL_GET_FORMS: &'static str = r#"
    SELECT f.uuid, f.name, f.features, f.definition, a.start_date, a.end_date, r.uuid AS assign_uuid
    FROM forms AS f
        LEFT JOIN reporting AS a ON a.fid = f.uuid
        LEFT JOIN assignments AS r ON r.fid = f.uuid AND r.lid = ?
    WHERE f.status = 'ACTIVE' AND a.lid = ?;
"#;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, lti, status, lineage
    FROM locations WHERE uuid = ?;
"#;


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormItem {
    pub uuid: Uuid,
    pub name: HashMap<String, Option<String>>,
    pub features: HashMap<String, Option<FeatureDefinition>>,
    pub definition: HashMap<String, Field>,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
    pub assign_uuid: Option<String>,
}

impl FormItem {
    pub fn from_row(row: &Row) -> FormItem {
        let features: HashMap<String, Option<FeatureDefinition>> = json::from_value(row.get(2)).unwrap();
        let definition: HashMap<String, Field> = json::from_value(row.get(3)).unwrap();
        let name: HashMap<String, Option<String>> = json::from_value(row.get(1)).unwrap();
        FormItem {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name,
            features,
            definition,
            start_date: row.get(4),
            end_date: row.get(5),
            assign_uuid: row.get(6),
        }
    }

    // Check if the reportable location type for this form is the
    // the same as the location being requested
    // if the form has no reporting location set up, then false
    // is returned and the form is discarded
    pub fn is_matched_lti(&self, lti: &Uuid) -> bool {
        if self.features.contains_key("LOCATION_REPORTING") {
            let location_field = self.definition.get("__lid__").clone();

            // If the location type defined in the form field matches return true
            if location_field.is_some() {
                if let Some(ref x) = location_field.unwrap().lti {
                    if x == lti {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        false
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Location {
    pub uuid: Uuid,
    pub lti: Uuid,
    pub status: String,
    pub lineage: Value,
}

// Get forms that a location could potentially report from
pub fn get_available_forms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let lid: Uuid = json::from_value(args).unwrap();

    let location = conn.query_row(&SQL_GET_LOCATION, &[&lid.to_string() as &ToSql], |row| {
        Location {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            lti: opt_uuid!(row, "lti").unwrap(),
            status: row.get(2),
            lineage: row.get(3),
        }
    }).unwrap();

    let mut stmt = conn.prepare(&SQL_GET_FORMS).unwrap();

    let rows = stmt.query_map(&[&lid.to_string() as &ToSql, &lid.to_string()], |row| {
        FormItem::from_row(row)
    }).unwrap();

    let forms: Vec<FormItem> = rows.map(|x| x.unwrap()).collect();

    // Filter to a set of forms that have the same reporting location type
    let mut actual_forms: Vec<FormItem> = forms.iter().filter(|x| x.is_matched_lti(&location.lti)).cloned().collect();
    actual_forms.dedup_by_key(|x| x.uuid.clone());

    let output: Value = json::to_value(actual_forms).unwrap();

    Ok(output)
}
