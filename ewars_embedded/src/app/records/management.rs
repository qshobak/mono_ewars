use std::rc::Rc;
use std::cell::RefCell;

use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use sonomalib::models::{RecordCreate, Record, RecordAmendment, RecordComment, NewRecordComment};

use crate::models::record::{Record, RecordAmendment, RecordComment, NewRecordComment};
use crate::state::ApplicationState;

#[derive(Debug, Deserialize, Serialize)]
pub struct NewRecord {
    pub fid: String,
    pub data: HashMap<String, Value>,
}

static INSERT_RECORD: &'static str = r#"
    INSERT OR REPLACE INTO records
    (uuid, status, fid, data, created_by, modified_by, source, submitted_by)
    VALUES (:uuid, 'SUBMITTED', :fid, :data, :created_by, :modified_by, :source, :submitted_by);
"#;

pub fn submit_record(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    // Check if record is in drafts
    // push record into record database
    // push record event onto event log
    // Announce record
    // remove record from drafts
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let record: NewRecord = json::from_value(args).unwrap();

    let new_uuid = Uuid::new_v4().to_string();

    let ruuid = app.borrow().get_current_uuid();
    let source_type = "DESKTOP".to_string();

    conn.execute_named(&INSERT_RECORD, &[
        (":uuid", &new_uuid),
        (":fid", &record.fid),
        (":data", &record.data),
        (":created_by", &ruuid),
        (":modified_by", &ruuid),
        (":source", &source_type),
        (":submitted_by", &ruuid),
    ]).unwrap();

    let result = Record::get_by_id(&conn, "", &new_uuid) {
        Ok(Some(res)) => res,
        Ok(Option::None) => bail!("ERR_NOT_FOUND"),
        Err(err) => bail!(err)
    };

    let event_value: Value = json::to_value(&result).unwrap();

    app.borrow_mut().add_event("RECORD", "INSERT", new_uuid.clone(), event_value)?;

    let output: Value = json::to_value(result).unwrap();
    Ok(output)
}

pub fn retract_record(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    // Check if form requires approval for retraction
    // YES: Create retraction task
    // NO: Delete record
    // Push record tombstone onto event log
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let record_id: String = json::from_value(args.get(0).unwrap().clone()).unwrap();

    // TODO: Check if this user requires a task to approved for retraction of a record

    conn.execute("DELETE FROM records WHERE uuid = ?", &[
        &record_id,
    ]).unwrap();

    let event_value: Value = json::from_str("{}").unwrap();
    app.borrow_mut().add_own_event("RECORD", "DELETE", record_id.clone(), event_value).unwrap();

    let output: Value = json::to_value(true).unwrap();
    Ok(output)
}

// Add a comment to record
pub fn comment_record(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: NewRecordComment = json::from_value(args).unwrap();

    let comments = conn.query_row("SELECT comments FROM records WHERE uuid = ?", &[&data.uuid], |row| {
        let val: Value = row.get(0);
        let result: Vec<RecordComment> = json::from_value(val).unwrap();
        result
    }).unwrap();

    let mut changeset: Vec<(String, Value)> = Vec::new();
    let comment_data: Value = json::to_value(&comments).unwrap();
    changeset.push(("comments".to_string(), comment_data));
    let event_data: Value = json::to_value(changeset).unwrap();
    app.borrow_mut().add_own_event("RECORD", "UPDATE", data.uuid.clone(), event_data).unwrap();

    let output: Value = json::to_value(true).unwrap();

    Ok(output)
}


pub fn amend_record(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: RecordAmendment = json::from_value(args).unwrap();

    let ruuid = app.borrow().get_current_uuid();

    let revisions: Option<Value> = conn.query_row("SELECT revisions FROM records WHERE uuid = ?", &[&data.uuid], |row| {
        match row.get(0) {
            Some(res) => {
                res
            },
            None => None
        }
    }).unwrap();


    conn.execute("UPDATE records SET data_date = ?, location_id = ?, data = ?, modified = CURRENT_TIMESTAMP, modified_by = ?, revisions = ? WHERE uuid = ?", &[
        &data.data_date,
        &data.location_id,
        &data.data,
        &ruuid,
        &data.uuid,
        &revisions,
    ]).unwrap();

    let result = conn.query_row("SELECT * FROM records WHERE uuid = ?", &[&data.uuid], |row| {
        Record::from_row(row)
    }).unwrap();


    // Create the event
    let changeset: Vec<(String, Value)> = vec![
        ("data".to_string(), json::to_value(&result.data).unwrap()),
        ("modified".to_string(), json::to_value(&result.modified).unwrap()),
        ("modified_by".to_string(), json::to_value(&result.modified_by).unwrap()),
    ];
    let event_data: Value = json::to_value(changeset).unwrap();
    app.borrow_mut().add_own_event("RECORD", "UPDATE", result.uuid.clone(), event_data.clone()).unwrap();

    // Return the updated record
    Ok(event_data)
}


