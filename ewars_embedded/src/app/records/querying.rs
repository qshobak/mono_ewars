use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use sonomalib::models::{Record, Assignment, Form, Field, RecordsQuery, QueryResult};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;

// Get a single record from the system
pub fn get_record(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: Uuid = json::from_value(args).unwrap();

    match Record::get_by_id(&conn, "", &id) {
        Ok(Some(res)) => Ok(json::to_value(res).unwrap()),
        Ok(Option::None) => bail!("ERR_NOT_FOUND"),
        Err(err) => bail!(err)
    }
}

// Query records from the system
pub fn query_records(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let query: RecordsQuery = json::from_value(args).unwrap();

    // TODO: Get a users assignmnets and filter the set based on their assigned locations for the
    // given form

    match Record::query(&conn, "", &query) {
        Ok(res) => Ok(json::to_value(res).unwrap()),
        Err(err) => bail!(err),
    }
}
