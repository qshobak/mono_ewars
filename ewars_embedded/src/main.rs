#[macro_use]
pub mod macros;
mod sonoma;
mod codec;
mod state;
mod networking;
mod sql_data;
mod models;
mod api;
mod system;
mod utils;
mod app;
mod peer_server;
mod remote_client;
mod discovery;
mod user;


extern crate sonomalib;
extern crate byteorder;
extern crate bytes;
extern crate futures;
extern crate rand;
extern crate serde;
extern crate serde_json;
extern crate tokio;
extern crate socket2;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate actix;
#[macro_use]
extern crate failure;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate rusqlite;
extern crate chrono;
extern crate uuid;
extern crate dirs;

use std::collections::HashMap;

use actix::prelude::*;

use crate::sonoma::Application;
use crate::state::ApplicationState;

use crate::models::config::{Config};

fn main() {
    let system = System::new("sonoma");

    let config = Config::load().expect("Could not load config");
    let state = ApplicationState::new(config);  
    let _ = state.borrow_mut().init().expect("Could not intialize state");

    let sonoma = Application::create(|ctx: &mut Context<Application>| Application {
        remotes: HashMap::new(),
        state: state,
        peers: HashMap::new(),
        master: Option::None,
        disco: Option::None,
    });
    system.run();
}
