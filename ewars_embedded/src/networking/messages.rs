use actix::prelude::*;
use serde_json::Value;
use tokio::net::{TcpStream};

use crate::peer_server::{PeerConnection};

#[derive(Message)]
pub struct Connect {
    pub addr: Addr<PeerConnection>,
    pub master: bool
}

#[derive(Message)]
pub struct IncomingConnection {
    pub stream: TcpStream,
    pub addr: Addr<PeerConnection>,
}


pub struct PeerConnected {
    pub addr: Addr<PeerConnection>,
    pub master: bool,
}

impl Message for PeerConnected {
    type Result = usize;
}

#[derive(Message)]
pub struct PeerDisconnected {
    pub id: usize,
}


pub struct PeerAccount {
    pub tki: String,
    pub name: String,
    pub email: String,
    pub role: String,
}

#[derive(Message)]
pub struct PeerRegistration {
    pub did: String,
    pub accounts: Vec<PeerAccount>,
}

#[derive(Message, Clone)]
pub struct AdvertisedPeer {
    pub did: String,
    pub addr: String,
    pub accounts: Vec<String>,
}

#[derive(Message)]
pub struct PeerRegistrationUpdate {
    pub did: String,
    pub accounts: Vec<PeerAccount>,
}

#[derive(Message)]
pub struct TeamMessage {
    pub tki: String,
    pub did: String,
    pub content: Value,
    pub tid: String,
    pub uid: String,
}

#[derive(Message)]
pub struct TeamMessageUpdate {
    pub tki: String,
    pub id: String,
    pub content: String,
}

#[derive(Message)]
pub struct TeamMessageDelete {
    pub tki: String,
    pub id: String,
}

#[derive(Message)]
pub struct SeedRequest {
    pub tki: String,
    pub pid: String,
    pub rid: String
}

#[derive(Message)]
pub struct InitiateSync {
    pub tki: String,
}

#[derive(Message)]
pub struct Authenticate {
    pub domain: String,
    pub email: String,
    pub password: String,
}


#[derive(Message)]
pub struct GetPackageDetails {
    pub tki: String,
    pub res_id: String
}
