use std::collections::HashMap;
use std::io;
use std::fmt;

use serde_json::Value;
use uuid::Uuid;

use actix::prelude::*;
use crate::peer_server::{PeerConnection};

use crate::remote_client::{Account, AccountUser};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Tenancy {
    pub uid: Uuid,
    pub name: String,
    pub role: String,
    pub email: String,
    pub tki: String,
    pub token: String,
    pub domain: String,
    pub status: String,
}



#[derive(Clone)]
pub enum Codec {
    // Set master
    SetMaster {
        addr: Addr<PeerConnection>,
        uuid: Uuid,
    },
    MasterSet,

    SetAccount(Uuid),

    // Authentication
    AuthRequest {
        id: Uuid,
        email: String,
        password: String,
        addr: Addr<PeerConnection>,
    },
    AuthenticationResult {
        id: Uuid,
        success: bool,
        error: Option<String>,
        users: Vec<AccountUser>,
        accounts: Vec<Account>,
    },

    // Registration
    RegistrationRequest {
        email: String,
        password: String,
        org_id: Option<Uuid>,
        org_name: Option<String>,
        name: String,
    },
    RegistrationResult {
        success: bool,
        error: Option<String>,
    },

    // Access request
    AccessRequest {
        email: String, 
        password: String,
        domain: String,
        org_id: Option<Uuid>,
        org_name: Option<String>,
    },
    AccessRequestResult {
        success: bool,
        error: Option<String>,
    },

    // Seeding
    SeedRequest {
        id: Uuid,
        tki: String,
        addr: Addr<PeerConnection>,
    },
    GetSeed {
        id: Uuid,
        tki: String,
    },
    SeedData {
        id: Uuid,
        protocol: i32,
        tki: String,
        data: HashMap<String, Vec<Value>>,
        ts_ms: usize
    },


    // Peer Connection handling
    NewPeerConnection {
        uuid: Uuid,
        addr: Addr<PeerConnection>,
        master: bool
    },
    PeerDisconnected(Uuid),
    PeerSetAccount(Uuid),


    Ping,
    Pong,
    Ok
}


impl Message for Codec {
    type Result = Result<Codec, ()>;
}
