import EN from "./en";
import FR from "./fr";
import AR from "./ar";

let lang = EN;

const langs = {
    en: EN,
    fr: FR,
    ar: AR
}

const configureLanguage(lang_code) {
    if !(lang_code) {
        lang = "en";
    } else {
        lang = langs[lang_code];
    }

    window.__ = function(code) {
        if (lang == "DEV") return code;
        if (lang[code]) {
            return lang[code];
        }

        if (EN[code]) return EN[code];
        return code;
    }

}

const setDev() {
    lang = "DEV";
    return true;
}

const getLanguage = function() {
    switch


export default configureLanguage;
export {
    configureLanguage,
    getLanguage
}
