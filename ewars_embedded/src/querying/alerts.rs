use serde_derive;
use serde_json;
use serde_json::Value;

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use application::Application;

use chrono::{DateTime, Utc, NaiveDateTime, Date, NaiveDate};

use rusqlite::{Row, Rows};

use models::alert::AlertRecord;


#[derive(Debug, Serialize)]
pub struct QueryResult {
    records: Value,
    count: i32,
}

#[derive(Debug, Deserialize, Clone)]
struct Query {
    resource: String,
    filters: HashMap<String, (String, String, String)>,
    sorters: HashMap<String, (String, String)>,
    limit: i32,
    offset: i32,
}


pub fn query_alerts(app: Rc<RefCell<Application>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let query: Query = serde_json::from_value(args).unwrap();

    let mut sql: String = "
        SELECT a.*,
                l.name AS location_name
        FROM alerts AS a
            LEFT JOIN locations AS l ON l.uuid = a.alid
    ".to_owned();

    let mut count_sql: String = "
        SELECT COUNT(*) AS total
        FROM alerts AS a
            LEFT JOIN locations AS l ON l.uuid = a.alid
    ".to_owned();

    let mut filters: Vec<String> = Vec::new();
    let mut orders: Vec<String> = Vec::new();

    for (key, value) in query.clone().filters {
        let (cmp, val, col_type) = query.filters.get(&key).unwrap();

        let mut flt_str: String = "".to_string();

        if key.contains("data.") {
            flt_str.push_str("json_extract(a.data, '$.'");
            let mut splits: Vec<String> = key.split(".").map(|s| s.to_string()).collect();

            let end_splits: Vec<String> = splits[1..].to_vec();

            let end_key: String = end_splits.join(".");

            flt_str.push_str("json_extract(a.data, '$.\"");
            flt_str.push_str(&end_key);
            flt_str.push_str("\"");
        } else {
            flt_str.push_str(&format!("a.{} ", key));
        }

        match cmp.as_ref() {
            "eq" => flt_str.push_str(" = "),
            "neq" => flt_str.push_str(" != "),
            "gt" => flt_str.push_str(" > "),
            "gte" => flt_str.push_str(" >= "),
            "lt" => flt_str.push_str(" < "),
            "lte" => flt_str.push_str(" <= "),
            "under" => flt_str.push_str(" "),
            "in" => flt_str.push_str(""),
            "like" => flt_str.push_str(""),
            "contains" => flt_str.push_str(""),
            "null" => flt_str.push_str(""),
            "nnull" => flt_str.push_str(""),
            _ => {}
        }

        match col_type.clone().as_ref() {
            "date" => {}
            "number" => {}
            "numeric" => {}
            "text" => {}
            "location" => {}
            "select" => {}
            "datetime" => {}
            "location_name" => {}
            "location_pcode" => {}
            "location_id" => {}
            "int" => {}
            "str" => {}
            _ => {
                return Err(format!("Column type filter is not supported: {}", col_type));
            }
        }

        flt_str.push_str(&format!("'{}'", val));
        filters.push(flt_str.clone());
    }

    for (key, value) in query.clone().sorters {
        let query = query.clone();
        let (direction, col_type) = query.sorters.get(&key).unwrap();

        let mut ord_str: String = "".to_string();

        if key.contains("data.") {
            ord_str.push_str("json_extract(a.data, '$.");

            let mut splits: Vec<String> = key.split(".").map(|s| s.to_string()).collect();

            let end_splits: Vec<String> = splits[1..].to_vec();
            let end_key: String = end_splits.join(".");

            ord_str.push_str("json_extract(a.data, '$.\"");
            ord_str.push_str(&end_key.clone());
            ord_str.push_str("\"')");
        } else {
            match key.as_ref() {
                "location_name" => ord_str.push_str("l.name "),
                "location_type" => ord_str.push_str("l.lti "),
                "period_start" => ord_str.push_str("a.period_start "),
                "period_end" => ord_str.push_str("a.period_end "),
                "raised" => ord_str.push_str("a.raised "),
                "closed" => ord_str.push_str("a.closed "),
                "modified" => ord_str.push_str("a.modified "),
                "stage" => ord_str.push_str("a.stage "),
                "status" => ord_str.push_str("a.status "),
                _ => {}
            }
        }

        match col_type.clone().as_ref() {
            "date" => {}
            _ => {}
        }

        orders.push(ord_str.clone());
    }

    if filters.len() > 0 {
        let filts_joined = filters.join(" AND ");
        sql.push_str(" WHERE ");
        sql.push_str(&filts_joined);
        count_sql.push_str(" WHERE ");
        count_sql.push_str(&filts_joined);
    }

    if orders.len() > 0 {
        let ords_joined = orders.join(" , ");
        sql.push_str(" ORDER BY ");
        sql.push_str(&ords_joined);
        count_sql.push_str(" ORDER BY ");
        count_sql.push_str(&ords_joined);
    }

    sql.push_str(&format!(" LIMIT {} ", query.limit));
    sql.push_str(&format!(" OFFSET {} ", query.offset));

    let mut stmt = match conn.prepare(&sql.clone()) {
        Ok(res) => res,
        Err(err) => {
            return Err(format!("{}", err));
        }
    };

    let mut rows = match stmt.query_map(&[], |row| {
        AlertRecord::from_row(row)
    }) {
        Ok(res) => res,
        Err(err) => {
            return Err(format!("Error retrieving rows: {}", err));
        }
    };

    let mut results: Vec<AlertRecord> = Vec::new();

    for record in rows {
        results.push(record.unwrap());
    }

    let output: Value = serde_json::to_value(results).unwrap();

    let mut count: i32 = conn.query_row(&count_sql, &[], |row| {
        row.get(0)
    }).unwrap();

    let query_result = QueryResult {
        records: output,
        count,
    };

    Ok(serde_json::to_value(query_result).unwrap())
}