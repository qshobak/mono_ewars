use std::io;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;

use serde_json;
use serde_json::Value;
use rusqlite::{NO_PARAMS};

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize)]
struct TaskCount {
    permissions: Value,
}

#[derive(Debug, Serialize, Deserialize)]
struct TaskPermission {
    roles: Option<Vec<String>>,
    location_id: Option<Vec<String>>,
}

impl TaskPermission {
    // Can a user of role access this task
}


// Get the total tasks a user has access to
pub fn get_total_tasks(state: Rc<RefCell<ApplicationState>>) -> Result<i32, io::Error> {
    let conn = state.borrow().get_current_pool().unwrap().get().unwrap();
    let cur_account = state.borrow().get_current_user().unwrap();

    let mut counter: i32 = 0;

    if cur_account.role == "ACCOUNT_ADMIN" {
        let mut stmt = conn.prepare("
            SELECT COUNT(*) AS total
            FROM tasks
            WHERE status = 'OPEN';
        ").unwrap();

        let rows: i32 = stmt.query_row(NO_PARAMS, |row| {
            row.get(0)
        }).unwrap();

        counter = rows;
    }

    //TODO: Iterate through the tasks and count the number that have permissions
    // that would allow this user to see them

    Ok(counter)
}

// Get the total open alerts this user has acces to
pub fn get_total_open_alerts(state: Rc<RefCell<ApplicationState>>) -> Result<i32, io::Error> {
    let conn = state.borrow().get_current_pool().unwrap().get().unwrap();
    let mut counter: i32 = 0;

    let cur_account = state.borrow().get_current_user().unwrap();

    if cur_account.role == "ACCOUNT_ADMIN" {
        let mut stmt = conn.prepare("
            SELECT COUNT(*) AS total
            FROM alerts
            WHERE status = 'OPEN';
        ").unwrap();

        let row: i32 = stmt.query_row(NO_PARAMS, |row| {
            row.get(0)
        }).unwrap();

        counter = row;
    }

    Ok(counter)
}

// Get the total news items that a user has acces to
pub fn get_total_news_items(state: Rc<RefCell<ApplicationState>>) -> Result<i32, io::Error> {
    //TODO: Count the number of news items in the database
     
    let counter: i32 = 0;

    Ok(counter)
}

