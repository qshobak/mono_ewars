use std::fs;
use std::fs::File;
use std::io::{Write, BufReader, BufRead, BufWriter};
use std::io::prelude::*;
use std::path::PathBuf;
use std::path::Path;
use std::borrow::Cow;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

pub type DeviceId = String;
pub type Tki = String;

pub type NodeId = (String, String);


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PeerAccount {
    pub tki: Tki,
    pub name: String,
    pub email: String,
    pub role: String,
    pub domain: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PeerConfig {
    pub did: String,
    pub addr: Option<String>,
    pub port: Option<usize>,
    pub version: Option<String>,
    // List of TKIs that this user holds access to
    pub accounts: Vec<String>,
    // Detailed information about the individual users for each account, that this peer holds
    pub profiles: HashMap<String, PeerAccount>,
    // MOBILE, DESKTOP, RELAY, CANON
    pub peer_type: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Config {
    pub user_dir: String,
    pub peer_ts_ms: HashMap<NodeId, usize>,
    pub own_ts_ms: HashMap<String, usize>, // Tki, ts_ms
    pub peers: HashMap<DeviceId, PeerConfig>, // String is peer device id
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct StorableConfig {
    pub user_dir: String,
    pub peer_ts_ms: HashMap<String, usize>,
    pub own_ts_ms: HashMap<String, usize>,
    pub peers: HashMap<String, PeerConfig>,
}

impl StorableConfig {
    pub fn to_config(&self) -> Config {
        let mut new_config = Config {
            user_dir: self.user_dir.clone(),
            peer_ts_ms: HashMap::new(),
            own_ts_ms: self.own_ts_ms.clone(),
            peers: self.peers.clone(),
        };

        for (key, value) in self.peer_ts_ms.clone() {
            let mut splat = key.split(",");
            let items = splat.collect::<Vec<&str>>();
            let new_id: NodeId = (items[0].to_string(), items[1].to_string());
            new_config.peer_ts_ms.insert(new_id, value);
        }

        new_config
    }
}


impl Config {
    pub fn add_peer(peer: PeerConfig) {
        unimplemented!();
    }

    pub fn remove_peer(peer_id: String) {
        unimplemented!();
    }

    pub fn update_canon_ts_ms(ts_ms: usize) {
        unimplemented!();
    }

    pub fn get_node(did: String) -> PeerConfig {
        unimplemented!();
    }

    pub fn get_peer(did: String) -> PeerConfig {
        unimplemented!();
    }

    pub fn has_peer(did: String) -> PeerConfig {
        unimplemented!();
    }

    pub fn update_peer(did: String) -> PeerConfig {
        unimplemented!();
    }

    pub fn get_manifest_for_peer(did: String) -> HashMap<DeviceId, usize> {
        unimplemented!();
    }

    pub fn initialize_account(&mut self, did: &str, tki: &str) {
        self.own_ts_ms.insert(tki.to_string(), 0);
        flush_config(self.clone());
    }

    // Update the ts_ms of a peer for a specific account
    pub fn set_peer_ts_ms(&mut self, tki: String, did: String, ts_ms: usize) {
        eprintln!("{}, {}, {}", tki, did, ts_ms);
        let node_id: NodeId = (tki, did);
        self.peer_ts_ms.insert(node_id, ts_ms);
        flush_config(self.clone());
    }

    // Set the ts_ms for an account owned by this instance
    pub fn set_own_ts_ms(&mut self, tki: String, ts_ms: usize) {
        self.own_ts_ms.insert(tki, ts_ms);
        flush_config(self.clone());
    }

    pub fn to_storage_format(&mut self) -> StorableConfig {
        let mut storable = StorableConfig {
            user_dir: self.user_dir.clone(),
            peer_ts_ms: HashMap::new(),
            own_ts_ms: self.own_ts_ms.clone(),
            peers: self.peers.clone(),
        };

        for (key, value) in self.peer_ts_ms.clone() {
            let end_key = format!("{},{}", key.0, key.1);
            storable.peer_ts_ms.insert(end_key, value);
        }

        storable
    }

    pub fn from_storage_format(stored: StorableConfig) -> Config {
        let mut new_config = Config {
            user_dir: stored.user_dir.clone(),
            peer_ts_ms: HashMap::new(),
            own_ts_ms: stored.own_ts_ms.clone(),
            peers: stored.peers.clone(),
        };

        for (key, value) in stored.peer_ts_ms.clone() {
            let mut splat = key.split(",");
            let items = splat.collect::<Vec<&str>>();
            let node_key: NodeId = (items[0].to_string(), items[1].to_string());
            new_config.peer_ts_ms.insert(node_key, value);
        }

        new_config
    }
}

// Load configuration from file
pub fn load_config(dir: &String) -> Option<Config> {
    let mut file_addr = format!("{}/sonoma.json", dir);
    eprintln!("{}", file_addr);
    let mut file_path = Path::new(&file_addr);

    match File::open(file_path.to_str().unwrap()) {
        Ok(res) => {
            let mut buffered_reader = BufReader::new(res);
            let mut contents = String::new();
            let _number_of_bytes: usize = match buffered_reader.read_to_string(&mut contents) {
                Ok(number_of_bytes) => number_of_bytes,
                Err(_err) => 0,
            };

            let config: StorableConfig = json::from_str(&contents).unwrap();


            Some(config.to_config())
        }
        Err(_err) => {
            let config = Config {
                user_dir: dir.clone(),
                peer_ts_ms: HashMap::new(),
                own_ts_ms: HashMap::new(),
                peers: HashMap::new(),
            };
            Some(config)
        }
    }
}

// Flush the config to file, overwriting the current config
pub fn flush_config(config: Config) {
    let file_name = format!("{}/sonoma.json", config.user_dir);
    let mut file_path = Path::new(&file_name);

    eprintln!("{:?}", config);

    let output: String = match json::to_value(config.clone().to_storage_format()) {
        Ok(res) => {
            res.to_string()
        }
        Err(err) => {
            eprintln!("{}", err);
            "".to_string()
        }
    };

    eprintln!("{}", file_path.to_str().unwrap());
    let mut file: File = match File::create(file_path.to_str().unwrap()) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{}", err);
            panic!();
        }
    };

    file.write(output.as_bytes()).unwrap();
}



