use chrono::prelude::*;
use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

pub struct EventLog {}

static SQLITE_INSERT_EVENT: &'static str = r#"
    INSERT INTO event_log 
    (uuid, event_type, res_type, res_id, ts_ms, data)
    VALUES (?1, ?2, ?3, ?4, ?5);
"#;

static DELETE_LOG_ITEM: &'static str = r#"
    DELETE FROM event_log WHERE res_id = ?;
"#;

impl Eventing {
    pub fn create_event(&conn: &SqliteConnection, res_type: &'static str, event_type: &'static str, id: &Uuid, data: Value) -> Result<(), Error> {
        let new_id = Uuid::new_v4();
        match event_type {
            "INSERT" => {
                match conn.execute(&SQLITE_INSERT_EVENT, &[
                    &new_id.to_string() as &ToSql,
                    &event_type,
                    &res_type,
                    &id.to_string(),
                    &ts_ms,
                    &data,
                ]) {
                    Ok(_) => Ok(()),
                    Err(err) => bail!(err)
                }
            },
            "DELETE" => {
                match conn.execute(&DELETE_LOG_ITEM, &[
                    &id.to_string() as &ToSql,
                ]) {
                    Ok(_) => Ok(()),
                    Err(err) => bail!(err)
                }

                match conn.execute(&SQLITE_INSERT_EVENT, &[
                    &new_id.to_string() as &ToSql,
                    &event_type,
                    &res_type,
                    &id.to_string(),
                    &ts_ms,
                    &Option::None,
                ]) {
                    Ok(_) => Ok(()),
                    Err(err) => bail!(err)
                }

            },
            "UPDATE" => {
                match conn.execute(&DELETE_LOG_ITEM, &[
                    &id.to_string() as &ToSql,
                ]) {
                    Ok(_) => Ok(()),
                    Err(err) => bail!(err)
                }

                match conn.execute(&SQLITE_INSERT_EVENT, &[
                    &new_id.to_string() as &ToSql,
                    &event_type,
                    &res_type,
                    &id.to_string(),
                    &ts_ms,
                    &data,
                ]) {
                    Ok(_) => Ok(()),
                    Err(err) => bail!(err)
                }

            },
            _ => {}
        }

    }
}
