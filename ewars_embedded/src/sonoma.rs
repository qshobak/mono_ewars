use std::net;
use std::str::FromStr;
use rand::{self, Rng};

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use futures::stream::SplitSink;
use futures::{Stream, Sink};
use futures::Future;

use actix::prelude::*;

use tokio::reactor;
use tokio::reactor::Handle;
use tokio::net::{TcpListener, TcpStream};
use tokio::io::AsyncRead;
use tokio::codec::{FramedRead};

use uuid::Uuid;
use chrono::prelude::*;

use serde_json::Value;

use crate::state::ApplicationState;
//use crate::clients::mcast_client::McastClient;
use crate::models::state::State; use crate::codec::{Codec};

//use crate::window::ApplicationWindow;
use crate::peer_server::{NewPeerConnection, PeerConnection, PeerCodec};
use crate::remote_client::{RemoteClient, RemoteCodec};
use crate::discovery::{Discovery};

use crate::networking::messages::*;

//use server::Server;

pub struct Config {}

pub struct Peer {
    pub uuid: Uuid,
    pub addr: Addr<PeerConnection>,
    pub master: bool,
}

pub struct Application {
    //pub config: OptionRc<Config>,
    //pub mcast: Option<Addr<McastClient>   
    pub remotes: HashMap<String, Addr<RemoteClient>>,
    pub state: Rc<RefCell<ApplicationState>>,
    pub peers: HashMap<Uuid, Peer>,
    pub master: Option<Addr<PeerConnection>>,
    pub disco: Option<Addr<Discovery>>,
}

#[derive(Message)]
struct IncomingConnection(pub TcpStream, pub net::SocketAddr);

impl Actor for Application {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let canon_app_addr = ctx.address().clone();
        let canon_state = self.state.clone();

        // Connecto to all remotes
        let state = self.state.borrow();
        let mut root_tokens: Vec<String> = Vec::new();
        for (domain, tokens) in state.get_remotes() {
            if !domain.contains("ewars.ws") {
                let canon_app_addr = ctx.address().clone();
                let canon_state = self.state.clone();
                let y = domain.clone();
                let addr = actix::Supervisor::start(move |_| RemoteClient::new(canon_state, canon_app_addr, domain, tokens));
                self.remotes.insert(y, addr);
            } else {
                root_tokens.extend(tokens);
            }
        }

        let canon_app_addr = ctx.address().clone();
        let canon_state = self.state.clone();
        let y = "52.17.203.72".to_string();
        //let y = "_val.ewars.ws".to_string();
        let addr = actix::Supervisor::start(move |_| RemoteClient::new(canon_state, canon_app_addr, y, root_tokens));
        self.remotes.insert("CANON".to_string(), addr);

        /*
        let mcast_app_addr = ctx.address().clone();
        let mcast_addr = actix::Supervisor::start(|_| Discovery::new(mcast_app_addr));
        self.disco = Some(mcast_addr);
        */

        let addr = net::SocketAddr::from_str("127.0.0.1:9009").unwrap();
        let listener = TcpListener::bind(&addr).unwrap();

        ctx.add_message_stream(listener.incoming().map_err(|_| ()).map(|st| {
            let addr = st.peer_addr().unwrap();

            IncomingConnection(st, addr)
        }));

        println!("Listening");
    }
}

// Handle an incoming connection and turn it into a PeerConnection
impl Handler<IncomingConnection> for Application {
    type Result = ();

    fn handle(&mut self, msg: IncomingConnection, ctx: &mut Context<Self>) {
        // Create a new peer session
        let cloned_self = ctx.address().clone();
        let cloned_state = self.state.clone();
        let remotes = self.remotes.clone();
        let session = PeerConnection::create(|ctx| {
            let (r, w) = msg.0.split();

            PeerConnection::add_stream(FramedRead::new(r, PeerCodec), ctx);
            PeerConnection::new(cloned_self, cloned_state, actix::io::FramedWrite::new(w, PeerCodec, ctx), remotes)
        });
    }
}

impl Handler<Codec> for Application {
    type Result = Result<Codec, ()>;

    fn handle(&mut self, msg: Codec, ctx: &mut Context<Self> ) -> Self::Result {
        let c_msg = msg.clone();
        match msg {
            Codec::SetMaster {addr, uuid} => {
                self.master = Some(addr.clone());
                if let Some(c) = &self.master {
                    c.do_send(Codec::MasterSet);
                }
                Ok(Codec::Ok)
            },
            Codec::PeerDisconnected(uuid) => {
                let _ = self.peers.remove(&uuid);
                Ok(Codec::Ok)
            },
            Codec::PeerSetAccount(id) => {
                Ok(Codec::Ok)
            },
            Codec::NewPeerConnection {uuid, addr, master} => {
                Ok(Codec::Ok)
            },
            _ => {
                Err(())
            }
        }
    }
}
