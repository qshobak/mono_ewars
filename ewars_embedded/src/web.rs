use actix::prelude::*;
use web_view::*;
use serde_json as json;

pub struct WebWindow {
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum Cmd {

}

impl WebWindow {
    pub fn new() -> Self {
        WebWindow {}
    }
}


impl Actor for WebWindow {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {

        let mut webview = web_view::builder()
                .title("EWARS")
                .content(Content::Html("<p>HELLO</p>"))
                .size(320, 480)
                .resizable(true)
                .debug(true)
                .user_data(vec![])
                .invoke_handler(|webview: &mut WebView, arg| {
                    use Cmd::*;

                    let tasks_len = {
                        let tasks = webview.user_data_mut();

                        match json::from_str(arg).unwrap() {
                            _ => {}
                        }

                        tasks.len()
                    };

                    Ok(())
                })
                .build()
                .unwrap();

        webview.set_color((156, 39, 176));

        let res = webview.run().unwrap();
    }
}

impl Supervised for WebWindow {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Restarting...");
    }
}
