use serde_json;
use serde_json::Value;

use rusqlite::{Row};

use crate::models::general_structs::CommandResult;

#[derive(Debug, Serialize, Deserialize)]
pub struct TeamSummary {
    pub uuid: String,
    pub name: String,
    pub description: String,
    pub permissions: Value,
    pub admins: Value,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Team {
    pub uuid: String,
    pub name: String,
    pub description: Option<String>,
    pub private: bool,
    pub permissions: Value,
    pub members: Value,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TeamMessage {
    pub uuid: String,
    pub ts: u32,
    pub tid: String,
    pub content_type: String,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub pid: Option<String>,
    pub created_by: Option<u32>,
}

impl Team {
    pub fn from_row(row: &Row) -> Team {
        Team {
            uuid: row.get(0),
            name: row.get(1),
            description: row.get(2),
            private: row.get(3),
            permissions: row.get(4),
            members: row.get(5),
            created: row.get(6),
            created_by: row.get(7),
            modified: row.get(8),
            modified_by: row.get(9),
        }
    }
}

impl TeamMessage {
    pub fn from_row(row: &Row) -> TeamMessage {
        TeamMessage {
            uuid: row.get(0),
            ts: row.get(1),
            tid: row.get(2),
            content_type: row.get(3),
            content: row.get(4),
            data: row.get(5),
            pid: row.get(6),
            created_by: row.get(7)
        }
    }
}
