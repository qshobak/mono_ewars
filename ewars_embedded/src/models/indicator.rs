use rusqlite::{Row};
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct Indicator {
    pub uuid: String,
    pub group_id: Option<String>,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub itype: Option<String>,
    pub icode: Option<String>,
    pub permissions: Value,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}


impl Indicator {
    pub fn from_row(row: &Row) -> Indicator {
        Indicator {
            uuid: row.get(0),
            group_id: row.get(1),
            name: row.get(2),
            status: row.get(3),
            description: row.get(4),
            itype: row.get(5),
            icode: row.get(6),
            permissions: row.get(7),
            created: row.get(8),
            created_by: row.get(9),
            modified: row.get(10),
            modified_by: row.get(11),
        }
    }
}
