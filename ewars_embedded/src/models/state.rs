use serde_json;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Message)]
pub struct State {
    pub did: String,
    pub accounts: Option<Value>,
    pub account: Option<Value>,
    pub user_dir: String,
    pub id: Option<usize>
}
