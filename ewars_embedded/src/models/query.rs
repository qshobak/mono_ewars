use serde_derive;
use serde_json as json;
use serde_json::Value;

use std::collections::HashMap;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ResourceQuery {
    pub orders: HashMap<String, (String, String)>,
    pub filters: HashMap<String, (String, String, String)>,
    pub limit: i32,
    pub offset: i32,
    pub rid: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct QueryResult {
    pub records: Value,
    pub count: i32,
}



impl QueryResult {
    pub fn to_value(&self) -> Value {
        json::to_value(self).unwrap()
    }
}

