use rusqlite::{Row, Connection};
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use std::collections::HashMap;
use crate::models::utils::*;

#[derive(Debug, Serialize)]
pub struct AlarmSummary {
    pub uuid: Uuid,
    pub name: String,
    pub definition: Value,
    pub workflow: Value,
    pub status: String,
}

pub struct Alarm {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub version: i32,
    pub definition: HashMap<String, Value>,
    pub workflow: Option<Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alarm {
    pub uuid: String,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub definition: Value,
    pub workflow: Option<Value>,
    pub permissions: Option<Value>,
    pub guidance: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<String>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<String>,
}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Alarm {
    fn from(row: &Row) -> Self {
        Alarm {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            version: row.get("version"),
            definition: row.get("definition"),
            workflow: row.get("workflow"),
            permissions: row.get("permissions"),
            guidance: row.get("guidance"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

static SQL_INSERT_ALARM: &'static str = r#"
"#;

static SQL_DELETE_ALARM: &'static str = r#"

"#;

static SQL_GET_ALARM: &'static str = r#"
    SELECT * FROM alarms WHERE uuid = ?;
"#;

impl Alarm {
    pub fn insert(conn: &Connection, data: &Alarm) -> bool {
        unimplemented!()
    }

    pub fn get_by_id(conn: &Connection, id: &String) -> Option<Self> {
        get_row!(&SQL_GET_ALARM, &conn, Alarm, &[&id])
    }

    pub fn delete(&mut self) -> bool {
        unimplemented!()
    }
}

#[derive(Debug, Serialize)]
pub struct AlarmCommandResult {
    pub result: bool,
}
