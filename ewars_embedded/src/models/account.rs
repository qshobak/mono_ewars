use serde_json as json;
use serde_json::Value;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Account {
    pub account_name: String,
    pub domain: String,
    pub email: String,
    pub lid: Option<String>,
    pub name: String,
    pub role: String,
    pub seeded: bool,
    pub status: String,
    pub tki: String,
    pub token: String,
    pub uid: u32,
    pub ruuid: String
}

#[derive(Debug, Serialize)]
pub struct AddAccountSuccess {
    pub success: bool,
    pub accounts: Vec<Account>,
}

#[derive(Debug, Deserialize)]
pub struct SwapAccount {
    pub uid: u32,
    pub tki: String,
}

#[derive(Debug, Serialize)]
pub struct CommandResult {
    pub status: bool,
}

