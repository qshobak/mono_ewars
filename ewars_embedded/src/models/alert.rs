use std::collections::HashMap;

use rusqlite::{Row};
use serde_json::Value;
use serde_json as json;
use chrono::prelude::*;
use uuid::Uuid;


pub type AlertPeriod = (DateTime<Utc>, DateTime<Utc>);

#[derive(Debug, Serialize)]
pub struct QueryResult {
    pub records: Value,
    pub count: i32,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Alert {
    pub uuid: String,
    pub alid: String,
    pub status: String,
    pub eid: Option<String>,
    pub alert_date: String,
    pub lid: Option<String>,
    pub stage: String,
    pub data: Value,
    pub workflow: Value,
    pub events: Value,
    pub raised: String,
    pub closed: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

impl Alert {
    pub fn from_row<'row>(row: &'row Row) -> Alert {
        Alert {
            uuid: row.get(0),
            alid: row.get(1),
            status: row.get(2),
            eid: row.get(3),
            alert_date: row.get(4),
            lid: row.get(5),
            stage: row.get(6),
            data: row.get(7),
            workflow: row.get(8),
            events: row.get(9),
            raised: row.get(10),
            closed: row.get(11),
            modified: row.get(11),
            modified_by: row.get(13),
        }
    }
}


#[derive(Debug, Deserialize, Serialize)]
pub struct AlertRecord {
    pub uuid: String,
    pub alid: String,
    pub status: String,
    pub eid: Option<String>,
    pub alert_date: String,
    pub lid: Option<String>,
    pub stage: String,
    pub data: Value,
    pub workflow: Value,
    pub events: Value,
    pub raised: String,
    pub closed: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
    pub location_name: Option<Value>,
    pub alarm_name: Option<String>,
    pub alarm_workflow: Option<Value>,
}


impl AlertRecord {
    pub fn from_row<'row>(row: &'row Row) -> AlertRecord {
        AlertRecord {
            uuid: row.get(0),
            alid: row.get(1),
            status: row.get(2),
            eid: row.get(3),
            alert_date: row.get(4),
            lid: row.get(5),
            stage: row.get(6),
            data: row.get(7),
            workflow: row.get(8),
            events: row.get(9),
            raised: row.get(10),
            closed: row.get(11),
            modified: row.get(12),
            modified_by: row.get(13),
            location_name: row.get(14),
            alarm_name: row.get(15),
            alarm_workflow: None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlertsQuery {
    pub filters: HashMap<String, (String, String, String)>,
    pub sorters: HashMap<String, (String, String)>,
    pub limit: i32,
    pub offset: i32
}

