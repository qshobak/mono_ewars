use serde_json::Value;
use rusqlite::{Row};


#[derive(Debug, Serialize, Deserialize)]
pub struct Task {
    pub uuid: String,
    pub status: String,
    pub task_type: String,
    pub data: Option<Value>,
    pub actions: Option<Value>,
    pub version: i32,
    pub form: Option<Value>,
    pub context: Option<Value>,
    pub outcome: Option<String>,
    pub actioned: Option<String>,
    pub actioned_by: Option<String>,
    pub created: String,
    pub modified: String,
    pub modified_by: Option<String>,
}


impl Task {
    pub fn from_row(row: &Row) -> Task {
        Task {
            uuid: row.get(0),
            status: row.get(1),
            task_type: row.get(2),
            data: row.get(3),
            actions: row.get(4),
            version: row.get(5),
            form: row.get(6),
            context: row.get(7),
            outcome: row.get(8),
            actioned: row.get(9),
            actioned_by: row.get(10),
            created: row.get(11),
            modified: row.get(12),
            modified_by: row.get(13),
        }
    }
}
