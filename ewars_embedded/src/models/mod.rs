#[macro_use]
pub mod utils;

//pub mod location_group;
//pub mod resource;
pub mod config;
//pub mod device;
pub mod query;
//pub mod form;
//pub mod alarm;
//pub mod alert;
//pub mod assignment;
//pub mod indicator;
//pub mod indicator_group;
//pub mod peer;
//pub mod peer_event;
//pub mod record;
//pub mod team;
//pub mod document;
//pub mod user;
//pub mod location_type;
//pub mod organization;
//pub mod reporting_period;
//pub mod task;
//pub mod event;
pub mod state;
//pub mod account;
//pub mod field;
//pub mod general_structs;

//mod role;
mod tenancy;
//mod location;
//mod draft;

pub use self::tenancy::{Tenancy};
//pub use self::role::{Role};
//pub use self::location::{Location, LocationUpdate, LocationCreate, LocationSummary};
//pub use self::draft::{NewDraft, Draft};
