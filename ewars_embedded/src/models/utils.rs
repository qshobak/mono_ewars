use rusqlite::types::ValueRef;

macro_rules! get_row {
    ($sql: expr, $conn: expr, $model: ident, $args: expr) => {
        {
            match $conn.query_row($sql, $args, |row| {
                $model::from(row)
            }) {
                Ok(res) => {
                    Some(res)
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}


macro_rules! execute {
    ($sql: expr, $conn: expr, $args: expr) => {
        {
            match $conn.execute($sql, $args) {
                Ok(_) => true,
                Err(err) => {
                    eprintln!("{:?}", err);
                    false
                }
            }
        }
    }
}


macro_rules! execute_named {
    ($sql: expr, $conn: expr, $args: expr) => {
        {
            match $conn.execute_named($sql, $args) {
                Ok(_) => true,
                Err(err) => {
                    eprintln!("{:?}", err);
                    false
                }
            }
        }
    }
}


macro_rules! opt_uuid {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            let val: ValueRef = $row.get_raw($name);
            match val {
                ValueRef::Text(x) => {
                    Some(Uuid::parse_str(&x).unwrap())
                },
                _ => Option::None
            }
        }
    }
}
