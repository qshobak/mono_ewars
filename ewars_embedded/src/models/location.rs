use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use rusqlite::{Row, Connection, NO_PARAMS};
use chrono::prelude::*;
use uuid::Uuid;

use sonomalib::models::{LocationTreeNode};


#[derive(Debug, Serialize, Deserialize)]
pub struct LocationSummary {
    pub uuid: String,
    pub name: Value,
    pub lti: String,
    pub pid: Option<String>,
    pub pcode: Option<String>,
    pub codes: Value,
    pub groups: Value,
    pub lineage: Value,
    pub status: String,
    pub child_count: i32,
}

impl LocationSummary {
    pub fn from_row(row: &Row) -> LocationSummary {
        LocationSummary {
            uuid: row.get(0),
            name: row.get(1),
            lti: row.get(2),
            pid: row.get(3),
            pcode: row.get(4),
            codes: row.get(5),
            groups: row.get(6),
            lineage: row.get(7),
            status: row.get(8),
            child_count: row.get(9),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Location {
    pub uuid: String,
    pub name: HashMap<String, String>,
    pub status: String,
    pub pcode: String,
    pub codes: HashMap<String, String>,
    pub groups: Vec<String>,
    pub data: Option<HashMap<String, Value>>,
    pub lti: String,
    pub pid: Option<String>,
    pub lineage: Vec<String>,
    pub organizations: Vec<String>,
    pub image: Option<String>,
    pub geometry_type: String,
    pub geojson: Option<Value>,
    pub population: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<String>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub child_count: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lti_name: Option<HashMap<String, String>>,
}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Location {
    fn from(row: &Row) -> Self {
        let lineage: Vec<String> = json::from_value(row.get("lineage")).unwrap();
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let codes: HashMap<String, String> = json::from_value(row.get("codes")).unwrap();
        let groups: Vec<String> = json::from_value(row.get("groups")).unwrap();
        let data: Option<HashMap<String, Value>> = json::from_value(row.get("data")).unwrap();
        let organizations: Vec<String> = json::from_value(row.get("organizations")).unwrap();

        let child_count: Option<i32> = match row.get_checked("child_count") {
            Ok(res) => Some(json::from_value(res).unwrap()),
            Err(_) => Option::None
        };

        let lti_name: Option<HashMap<String, String>> = match row.get_checked("lti_name") {
            Ok(res) => Some(json::from_value(res).unwrap()),
            Err(_) => Option::None
        };

        Location {
            uuid: row.get("uuid"),
            name: name,
            status: row.get("status"),
            pcode: row.get("pcode"),
            codes,
            groups,
            data,
            lti: row.get("lti"),
            pid: row.get("pid"),
            lineage,
            organizations,
            image: Option::None,
            geometry_type: row.get("geometry_type"),
            geojson: row.get("geojson"),
            population: row.get("population"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            child_count,
            lti_name,
        }
    }
}

static SQL_GET_LOCATION: &'static str = r#"
    SELECT * FROM locations WHERE uuid = ?;
"#;

static SQL_DELETE_LOCATION: &'static str = r#"

"#;

static SQL_INSERT_LOCATION: &'static str = r#"

"#;


impl Location {
    // Get a location by its id
    pub fn get_location_by_id(conn: &Connection, id: &String) -> Option<Self> {
        get_row!(&SQL_GET_LOCATION, &conn, Location, &[&id])
    }

    // Delete a location from the system
    pub fn delete_location(conn: &Connection, id: &String) -> Result<(), ()> {
        match execute!(&SQL_DELETE_LOCATION, &conn, &[&id]) {
            true => Ok(()),
            false => Err(()),
            _ => Err(())
        }
    }

    pub fn insert(conn: &Connection, data: &Location) -> Result<(), ()> {
        let groups: Value = json::to_value(&data.groups).unwrap();
        let codes: Value = json::to_value(&data.codes).unwrap();
        let real_data: Value = json::to_value(&data.data).unwrap();
        let lineage: Value = json::to_value(&data.lineage).unwrap();
        let organizations: Value = json::to_value(&data.organizations).unwrap();

        match execute!(&SQL_INSERT_LOCATION, &conn, NO_PARAMS) {
            true => Ok(()),
            false => Err(()),
            _ => Err(())
        }
    }

}


#[derive(Debug, Deserialize)]
pub struct LocationGet {
    pub uuid: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct LocationType {
    pub id: i32,
    pub name: Value,
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationUpdate {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub status: String,
    pub pcode: String,
    pub codes: HashMap<String, String>,
    pub groups: Option<Vec<String>>,
    pub data: Option<HashMap<String, Value>>,
    pub lti: Uuid,
    pub pid: Option<Uuid>,
    pub lineage: Vec<Uuid>,
    pub organizations: Vec<Uuid>,
    pub geometry_type: String,
    pub geojson: Option<Value>,
    pub population: Option<Value>,
}

static SQL_UPDATE_LOCATION: &'static str = r#"
    UPDATE locations 
        SET name = :name,
            status = :status,
            pcode = :pcode,
            codes = :codes,
            groups = :groups,
            data = :data,
            lti = :lti,
            pid = :pid,
            lineage = :lineage,
            organizations = :organizations,
            geometry_type = :geometry_type,
            geojson = :geojson,
            population = :populationm
            modified = :modified,
            modified_by = :uid
        WHERE uuid = :uuid;
"#;

impl LocationUpdate {
    pub fn persist(&self, conn: &Connection, uid: &Uuid) -> Result<(), ()> {
        let name: Value = json::to_value(&self.name).unwrap();
        let codes: Value = json::to_value(&self.codes).unwrap();
        let groups: Value = json::to_value(&self.groups).unwrap();
        let data: Option<Value> = Some(json::to_value(&self.data).unwrap());
        let lineage: Value = json::to_value(&self.lineage).unwrap();
        let organizations: Value = json::to_value(&self.organizations).unwrap();
        let population: Value = json::to_value(&self.population).unwrap();

        let pid: Option<String> = match self.pid {
            Some(res) => Some(res.to_string()),
            None => Option::None,
        };

        let cur_time = Utc::now();

        let result: bool = execute_named!(&SQL_UPDATE_LOCATION, &conn, &[
            (":name", &name),
            (":status", &self.status),
            (":pcode", &self.pcode),
            (":codes", &codes),
            (":groups", &groups),
            (":data", &data),
            (":lti", &self.lti.to_string()),
            (":pid", &pid),
            (":lineage", &lineage),
            (":organizations", &organizations),
            (":geometry_type", &self.geometry_type),
            (":geojson", &self.geojson),
            (":population", &population),
            (":modified", &cur_time),
            (":uid", &uid.to_string()),
            (":uuid", &self.uuid.to_string()),
        ]);

        if result {
            Ok(())
        } else {
            Err(())
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationCreate {}



#[derive(Debug, Deserialize)]
pub struct LocationSeedPackage {
    pub locations: Vec<Location>,
    pub location_types: Vec<LocationType>,
}

#[derive(Debug, Deserialize)]
pub struct LocationTest {
    pub uuid: String,
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct LocationsQuery {
    pub limit: i32,
    pub offset: i32,
    pub count: bool,
    pub sorting: Vec<(String, String, String)>,
    pub filters: Vec<(String, String, String)>,
}

#[derive(Debug, Serialize)]
pub struct LocationSummaryItem {
    pub uuid: String,
    pub name: Value,
    pub sti: i32,
    pub pid: String,
    pub status: String,
    pub lineage: Value,
    pub groups: Value,
    pub codes: Value,
}


