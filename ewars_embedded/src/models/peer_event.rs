use serde_json::Value;


#[derive(Debug, Serialize, Deserialize)]
pub struct PeerEvent {
    pub uuid: String,
    pub ts_ms: i64,
    pub did: String,
    pub uid: String,
    pub data: Value,
}

