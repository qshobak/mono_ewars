use std::collections::HashMap;
use std::io;

use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use serde_rusqlite::{to_params_named, from_row, columns_from_statement, from_rows_with_columns, from_row_with_columns};
use rusqlite::{Row};

use crate::models::general_structs::CommandResult;


#[derive(Debug, Deserialize)]
pub struct FormGet {
    pub id: usize
}

#[derive(Debug, Serialize)]
pub struct FormCommandResult {
    pub result: bool
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FormSummary {
    pub id: u8,
    pub uuid: String,
    pub name: Value,
    pub version: u8,
    pub definition: Value,
    pub workflow: Value,
    pub uniques: Value,
    pub features: Value,
    pub status: String,
}

impl FormSummary {
    pub fn from_row<'row>(row: &'row Row) -> FormSummary {
        FormSummary {
            id: row.get(0),
            uuid: row.get(1),
            name: row.get(2),
            version: row.get(3),
            definition: row.get(4),
            workflow: row.get(5),
            uniques: row.get(6),
            features: row.get(7),
            status: row.get(8),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Form {
    pub uuid: Option<String>,
    pub name: Value,
    pub version: i32,
    pub status: String,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: Option<Value>,
    pub definition: Value,
    pub etl: Value,
    pub changes: Value,
    pub permissions: Value,
    pub created: Option<String>,
    pub created_by: Option<String>,
    pub modified: Option<String>,
    pub modified_by: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewForm {
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub etl: Value,
    pub definition: Value,
    pub permissions: Value,
    pub public: bool,
    pub pub_submit_url: Option<String>,
    pub inline_edit: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FormField {
    pub uuid: String,
    pub name: String,
    pub field_type: String,
    pub options: Option<Vec<(String, String)>>,
    pub fields: Option<Value>,
}

impl Form {

    pub fn from_row<'row>(row: &'row Row) -> Form {
        Form {
            uuid: row.get(0),
            name: row.get(1),
            version: row.get(2),
            status: row.get(3),
            description: row.get(4),
            guidance: row.get(5),
            eid_prefix: row.get(6),
            features: row.get(7),
            definition: row.get(8),
            etl: row.get(9),
            changes: row.get(10),
            permissions: row.get(11),
            created: row.get(12),
            created_by: row.get(13),
            modified: row.get(14),
            modified_by: row.get(15),
        }
    }
}

