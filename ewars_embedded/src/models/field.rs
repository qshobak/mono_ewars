use serde_json as json;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Field {
    #[serde(rename="type")]
    pub field_type: String,
    #[serde(default="default_lti")]
    pub lti: Option<String>,
    #[serde(default="default_options")]
    pub options: Vec<(String, String)>,
    #[serde(default="default_required")]
    pub required: bool,
    #[serde(default="default_interval")]
    pub interval: Option<String>,
    #[serde(default="default_label")]
    pub label: Option<Value>,

}

fn default_lti() -> Option<String> {
    None
}

fn default_options() -> Vec<(String, String)> {
    Vec::new()
}

fn default_required() -> bool {
    false
}

fn default_interval() -> Option<String> {
    None
}

fn default_label() -> Option<Value> {
    None
}