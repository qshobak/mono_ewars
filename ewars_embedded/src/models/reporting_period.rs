
#[derive(Debug, Deserialize, Serialize)]
pub struct ReportingPeriod {
    pub uuid: String,
    pub id: i32,
    pub location_id: Option<String>,
    pub form_id: i32,
    pub start_date: String,
    pub end_date: Option<String>,
    pub status: String,
    pub submissions: i32,
    pub pid: Option<String>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}


