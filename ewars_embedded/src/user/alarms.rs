use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use rusqlite::types::ToSql;
use rusqlite::{Row, NO_PARAMS};
use sonomalib::models::{Assignment};

use crate::state::ApplicationState;

use crate::user::{get_user_assigns};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alarm {
    pub uuid: String,
    pub name: String,
    pub alert_count: i32,
}

impl Alarm {
    pub fn change_count(&mut self, count: i32) {
        self.alert_count = count;
    }
}


static SQL_GET_ALARMS: &'static str = r#"
    SELECT a.uuid, a.name,
        (SELECT COUNT(r.uuid) FROM alerts AS r WHERE r.alid = a.uuid AND r.status = 'OPEN')
    FROM alarms AS a
    WHERE a.status = 'ACTIVE';
"#;

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT a.lid, a.fid, a.assign_type
    FROM assignments AS a
    WHERE a.uid = ? AND a.status = 'ACTIVE';
"#;

static SQL_GET_ALERT_COUNT: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM alerts AS r
    WHERE r.lid IN ('{LOCS}')
        AND r.alid = ?
        AND r.status = 'OPEN';
"#;

static SQL_GET_REGIONAL_ASSIGNS: &'static str = r#"
    SELECT lid
    FROM assignments
    WHERE status = 'ACTIVE'
        AND assign_type = 'ADMIN'
        AND uid = ?;
"#;

static SQL_GET_ALERTS_UNDER: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM alerts AS r
        LEFT JOIN locations AS l ON l.uuid = r.lid
    WHERE CAST(l.lineage AS TEXT) LIKE '{LOC}'
        AND r.alid = ?
        AND r.status = 'OPEN';
"#;


pub fn get_available_alarms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let role = app.borrow().get_current_role();

    let mut results: Vec<Alarm> = Vec::new();

    match role.as_ref() {
        "USER" => {
            results = get_user_alarms(app.clone());
        },
        "ACCOUNT_ADMIN" => {
            results = get_all_alarms(app.clone());
        },
        "REGIONAL_ADMIN" => {
            results = get_regional_alarms(app.clone());
        }
        _ => {
            results = get_user_alarms(app.clone());
        }
    }

    Ok(json::to_value(results).unwrap())
}

// Get all alarms with count
fn get_all_alarms(app: Rc<RefCell<ApplicationState>>) -> Vec<Alarm> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALARMS).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Alarm {
            uuid: row.get(0),
            name: row.get(1),
            alert_count: row.get(2),
        }
    }).unwrap();

    let results: Vec<Alarm> = rows.map(|x| x.unwrap()).collect();

    results
}


// Get all alarms with a count of alerts that the active user would have access to
fn get_user_alarms(app: Rc<RefCell<ApplicationState>>) -> Vec<Alarm> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let mut assignments: Vec<Assignment> = get_user_assigns(app.clone());

    let alarms: Vec<Alarm> = get_all_alarms(app.clone());

    // Get a list of loc_ids
    // Remove assignments which don't have a location id
    // Deduplicate the assignments on the location id
    assignments = assignments.iter().filter(|x| x.lid.is_some()).cloned().collect::<Vec<Assignment>>();
    assignments.dedup_by_key(|x| x.lid.clone().unwrap());

    let mut loc_ids: Vec<String> = Vec::new();

    for item in &assignments {
        if let Some(ref x) = item.lid {
            loc_ids.push(x.to_string());
        }
    }

    let string_locs: String = loc_ids.join("','");

    let mut end_alarms: Vec<Alarm> = Vec::new();
    for alarm in alarms.iter() {

        let sql = SQL_GET_ALERT_COUNT.replace("{LOCS}", &string_locs);
        let count: i32 = conn.query_row(&sql, &[&alarm.uuid], |row| {
            row.get(0)
        }).unwrap();

        end_alarms.push(Alarm {
            uuid: alarm.uuid.clone(),
            name: alarm.name.clone(),
            alert_count: count
        })
    }

    end_alarms
}

// Get regional admin alarms with count, this is trickier as a regional admin could have multiple
// locations under their belt and we don't want to overlap the count.
fn get_regional_alarms(app: Rc<RefCell<ApplicationState>>) -> Vec<Alarm> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid().to_string();

    let mut stmt = conn.prepare(&SQL_GET_REGIONAL_ASSIGNS).unwrap();
    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        opt_uuid!(row, 0).unwrap()
    }).unwrap();
    let locations: Vec<Uuid> = rows.map(|x| x.unwrap()).collect();

    let alarms: Vec<Alarm> = get_all_alarms(app.clone());

    let mut alert_counts: HashMap<String, i32> = HashMap::new();

    for alarm in &alarms {
        alert_counts.insert(alarm.uuid.clone(), 0);
    }

    for loc_id in locations {
        for alarm in &alarms {
            let sql: String = SQL_GET_ALERTS_UNDER.replace("{LOC}", &loc_id.to_string());
            eprintln!("{}", sql);
            let count: i32 = conn.query_row(&sql, &[&alarm.uuid.to_string() as &ToSql], |row| {
                row.get(0)
            }).unwrap();

            alert_counts.entry(alarm.uuid.clone())
                .and_modify(|x| *x += count)
                .or_insert(0);
        }
    }

    let mut end_alarms = Vec::<Alarm>::new();

    for alarm in &alarms {
        let total: i32 = match alert_counts.get(&alarm.uuid) {
            Some(res) => res.clone(),
            None => 0
        };

        end_alarms.push(Alarm {
            uuid: alarm.uuid.clone(),
            name: alarm.name.clone(),
            alert_count: total,
        })
    }

    end_alarms
}
