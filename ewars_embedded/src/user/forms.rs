use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::{Row, NO_PARAMS};
use rusqlite::types::{ToSql};

use sonomalib::models::{FormFeature, Field, FeatureDefinition};

use crate::state::ApplicationState;

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT f.uuid, f.name, f.status, f.features, f.definition
    FROM forms AS f
        LEFT JOIN assignments AS a ON a.fid = f.uuid
    WHERE a.uid = ?
        AND (f.status = 'ACTIVE' OR f.status = 'ARCHIVED');
"#;

static SQL_GET_FORMS_ADMIN: &'static str = r#"
    SELECT f.uuid, f.name, f.status, f.features, f.definition
    FROM forms AS f
    WHERE (f.status = 'ACTIVE' OR f.status = 'ARCHIVED');
"#;


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Form {
    pub uuid: Uuid,
    pub name: HashMap<String, Option<String>>,
    pub status: String,
    pub features: HashMap<String, Option<FeatureDefinition>>,
    pub definition: HashMap<String, Field>,
}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Form {
    fn from(row: &Row) -> Self {
        let definition: HashMap<String, Field> = json::from_value(row.get(4)).unwrap();
        let features: HashMap<String, Option<FeatureDefinition>> = json::from_value(row.get(3)).unwrap();
        let name: HashMap<String, Option<String>> = json::from_value(row.get(1)).unwrap();

        Form {
            uuid: pull_uuid!(row, 0),
            name,
            status: row.get(2),
            features,
            definition,
        }
    }
}

pub fn get_forms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid().to_string();
    let role = app.borrow().get_current_role();

    let mut results: Vec<Form> = Vec::new();

    // If the users role is USER then we only get forms that the user has an assignment for
    if &role == "USER" {
        let mut stmt = conn.prepare(&SQL_GET_ASSIGNMENTS)?;
        let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
            Form::from(row)
        })?;
        results = rows.map(|x| x.unwrap()).collect();
    } else {
        let mut stmt = conn.prepare(&SQL_GET_FORMS_ADMIN)?;
        let rows = stmt.query_map(NO_PARAMS, |row| {
            Form::from(row)
        })?;

        results = rows.map(|x| x.unwrap()).collect();
    }

    results.sort_by_key(|x| x.uuid.clone());
    results.dedup_by_key(|x| x.uuid.clone());

    let output = json::to_value(results).unwrap();
    Ok(output)
}
