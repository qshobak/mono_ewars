use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use rusqlite::{Row, NO_PARAMS};
use rusqlite::types::{ToSql};

use serde_json as json;
use serde_json::Value;

use sonomalib::models::{Assignment};

use crate::state::ApplicationState;
use crate::user::{get_user_assigns};

#[derive(Debug, Serialize, Deserialize)]
pub struct Alert {
    pub uuid: String,
    pub alid: String,
    pub data: Value,
    pub eid: Option<String>,
    pub raised: String,
    pub alert_date: String,
    pub stage: String,
    pub status: String,
    pub lid: Option<String>,
    pub location_name: Option<Value>,
    pub alarm_name: String,
}


impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Alert {
    fn from(row: &Row) -> Alert {
        Alert {
            uuid: row.get(0),
            alid: row.get(1),
            data: row.get(2),
            eid: row.get(3),
            raised: row.get(4),
            alert_date: row.get(5),
            stage: row.get(6),
            status: row.get(7),
            lid: row.get(8),
            location_name: row.get(9),
            alarm_name: row.get(10),
        }
    }
}

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT a.lid
    FROM assignments AS a
    WHERE a.uid = ?
        AND a.status = 'ACTIVE';
"#;

static SQL_GET_OPEN_ALERTS: &'static str = r#"
    SELECT a.uuid, a.alid, a.data, a.eid, a.raised, a.alert_date, a.stage, a.status, a.lid, l.name, al.name
    FROM alerts AS a
        LEFT JOIN locations AS l ON l.uuid = a.lid
        LEFT JOIN alarms AS al ON al.uuid = a.alid
    WHERE al.status = 'ACTIVE'
        AND a.status = 'OPEN'
        AND a.lid IN ('{LOCIDS}');
"#;

static SQL_GET_ALL_OPEN_ALERTS: &'static str = r#"
    SELECT a.uuid, a.alid, a.data, a.eid, a.raised, a.alert_date, a.stage, a.status, a.lid, l.name, al.name
    FROM alerts AS a
        LEFT JOIN locations AS l ON l.uuid = l.lid
        LEFT JOIN alarms AS al ON al.uuid = a.alid
    WHERE a.status = 'OPEN';
"#;

static SQL_GET_REG_ADMIN_ASSIGNS: &'static str = r#"
    SELECT a.lid
    FROM assignments AS a
    WHERE a.uid = ? AND a.status = 'ACTIVE';
"#;

static SQL_GET_ALERTS_UNDER: &'static str = r#"

"#;

pub fn get_alerts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let uid = app.borrow().get_current_uuid();
    let role = app.borrow().get_current_role();

    let mut results: Vec<Alert> = Vec::new();

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            results = get_all_alerts(app.clone());
        }
        "REGIONAL_ADMIN" => {
            results = get_regional_admin_alerts(app.clone());
        }
        _ => {
            results = get_user_alerts(app.clone());
        }
    }

    Ok(json::to_value(results).unwrap())
}


fn get_all_alerts(app: Rc<RefCell<ApplicationState>>) -> Vec<Alert> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALL_OPEN_ALERTS).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Alert::from(row)
    }).unwrap();

    let results: Vec<Alert> = rows.map(|x| x.unwrap()).collect();

    results
}

fn get_regional_admin_alerts(app: Rc<RefCell<ApplicationState>>) -> Vec<Alert> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let mut results: Vec<Alert> = Vec::new();

    results
}

// Get a users alerts
fn get_user_alerts(app: Rc<RefCell<ApplicationState>>) -> Vec<Alert> {
    let mut assignments: Vec<Assignment> = get_user_assigns(app.clone());
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    assignments.dedup_by_key(|x| x.lid.clone().unwrap());

    let mut loc_ids: Vec<String> = Vec::new();
    for assign in assignments {
        if let Some(x) = assign.lid {
            loc_ids.push(x.to_string());
        }
    }

    let loc_id_string: String = loc_ids.join("','");
    let sql = SQL_GET_OPEN_ALERTS.replace("{LOCIDS}", &loc_id_string);
    eprintln!("{}", sql);
    let mut stmt_call = conn.prepare(&sql).unwrap();

    let rows = stmt_call.query_map(NO_PARAMS, |row| {
        Alert::from(row)
    }).unwrap();

    let alerts: Vec<Alert> = rows.map(|x| x.unwrap()).collect();

    alerts
}
