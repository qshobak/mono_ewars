use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use rusqlite::{NO_PARAMS};

use crate::state::ApplicationState;

#[derive(Debug, Serialize)]
pub struct Task {
    pub uuid: String,
    pub task_type: String,
    pub data: Option<Value>,
}

pub fn get_tasks(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let role = app.borrow().get_current_role();

    let mut results: Vec<Task> = Vec::new();

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            let mut stmt = conn.prepare("SELECT uuid, task_type, data FROM tasks WHERE status = 'OPEN'")?;
            let rows = stmt.query_map(NO_PARAMS, |row| {
                Task {
                    uuid: row.get(0),
                    task_type: row.get(1),
                    data: row.get(2),
                }
            })?;
            results = rows.map(|x| x.unwrap()).collect();
        }
        "REGIONAL_ADMIN" => {}
        "USER" => {}
        _ => {}
    }

    Ok(json::to_value(results).unwrap())
}
