use std::rc::Rc;
use std::cell::RefCell;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use failure::Error;
use serde_json;
use serde_json::Value;
use rusqlite::{NO_PARAMS};
use uuid::Uuid;

use crate::system;
use crate::app;
use crate::user;

use crate::state::ApplicationState;

pub fn default_handler(state: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    println!("{:?}", args);

    bail!("DEFUALT");
}

pub fn db_test(state: Rc<RefCell<ApplicationState>>, _args: Value) -> Result<Value, Error> {
    let conn = state.borrow().get_current_pool().unwrap().get().unwrap();

    conn.query_row("SELECT 1;", NO_PARAMS, |row| {
        let result: String = row.get(0);
    }).unwrap();

    Ok(serde_json::to_value("TEST").unwrap())
}

#[derive(Debug, Serialize, Deserialize)]
pub struct APIError {
    pub code: String,
    pub message: String,
}

macro_rules! add_method {
    ($m: expr, $name:expr, $handler:expr) => {
        {
            $m.insert($name, $handler as fn(state: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error>);
        }
    }
}

lazy_static! {
    static ref API_METHODS: HashMap<&'static str, fn(state: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error>> = {
        let mut m = HashMap::new();

        add_method!(m, "com.sonoma.conf", app::conf::get);
        // Local management commands
        //add_method!(m, "com.sonoma.add_accounts", app::accounts::management::add_accounts);
        //add_method!(m, "com.sonoma.accounts", default_handler);
        //add_method!(m, "com.sonoma.projects", default_handler);

        // Resouces
        //add_method!(m, "com.sonoma.resources", app::resources::querying::query_resources);
        //add_method!(m, "com.sonoma.resources.dashboards", app::resources::querying::query_dashboards);
        //add_method!(m, "com.sonoma.resource", app::resources::querying::get_resource);
        //add_method!(m, "com.sonoma.resource.create", app::resources::management::create_resource);
        //add_method!(m, "com.sonoma.resource.update", app::resources::management::update_resource);
        //add_method!(m, "com.sonoma.resource.delete", app::resources::management::delete_resource);

        // Tasks
        //add_method!(m, "com.sonoma.task", app::tasks::querying::get_task);
        //add_method!(m, "com.sonoma.task.action", default_handler);

        // Manifest user
        add_method!(m, "com.sonoma.self.assignments", user::get_assignments);
        //add_method!(m, "com.sonoma.self.profile", default_handler);
        //add_method!(m, "com.sonoma.self.teams", app::myself::teams::get_teams);
        add_method!(m, "com.sonoma.self.forms", user::get_forms);
        add_method!(m, "com.sonoma.self.tasks", user::get_tasks);
        //add_method!(m, "com.sonoma.self.event_log", default_handler);
        //add_method!(m, "com.sonoma.self.dashboards", app::myself::dashboards::get_dashboards);
        add_method!(m, "com.sonoma.self.documents", user::get_available_documents);
        add_method!(m, "com.sonoma.self.alarms", user::get_available_alarms);
        add_method!(m, "com.sonoma.self.alerts", user::get_alerts);
        //add_method!(m, "com.sonoma.self.resources", app::myself::resources::get_resources);
        //add_method!(m, "com.sonoma.self.overview", app::myself::overview::get_overview);
        add_method!(m, "com.sonoma.self.locations", user::get_user_locations);

        // Drafts
        add_method!(m, "com.sonoma.drafts", user::get_drafts);
        //add_method!(m, "com.sonoma.draft.delete", app::drafts::management::delete_draft);
        //add_method!(m, "com.sonoma.draft.update", app::drafts::management::update_draft);
        //add_method!(m, "com.sonoma.draft.create", app::drafts::management::create_draft);

        // Locations
        add_method!(m, "com.sonoma.locations", app::locations::querying::query_locations);
        add_method!(m, "com.sonoma.location", app::locations::querying::get_location);
        add_method!(m, "com.sonoma.locations.root", app::locations::querying::get_location_root);
        add_method!(m, "com.sonoma.location.full", app::locations::querying::get_location);
        //add_method!(m, "com.sonoma.location.create", app::locations::management::create_location);
        //add_method!(m, "com.sonoma.location.children", app::locations::querying::get_children);
        //add_method!(m, "com.sonoma.location.update", app::locations::management::update_location);
        //add_method!(m, "com.sonona.location.delete", app::locations::management::delete_location);
        add_method!(m, "com.sonoma.location.reporting", app::locations::reporting::get_context);
        //add_method!(m, "com.sonoma.location.alerts", default_handler);
        //add_method!(m, "com.sonoma.location.alerts_specific", app::locations::alerts::get_specific_alerts);
        //add_method!(m, "com.sonoma.location.submissions", app::locations::submissions::get_submissions);
        add_method!(m, "com.sonoma.location.forms.available", app::locations::forms::get_available_forms);
        add_method!(m, "com.sonoma.location.alerts.trend", app::locations::alerts::get_trend);
        add_method!(m, "com.sonoma.location.alerts", app::locations::alerts::get_location_alert_overview);
        add_method!(m, "com.sonoma.location.records.trend", app::locations::records::get_records_trend);

        //// Location performance
        add_method!(m, "com.sonoma.location.completeness", app::locations::performance::get_location_completeness);
        add_method!(m, "com.sonoma.location.timeliness", app::locations::performance::get_location_timeliness);
        add_method!(m, "com.sonoma.location.submitted", app::locations::performance::get_location_submitted);
        //add_method!(m, "com.sonoma.location.late", default_handler);
        //add_method!(m, "com.sonoma.location.on_time", default_handler);

        //// Locaiton types
        //add_method!(m, "com.sonona.location_types", app::location_types::querying::query_location_types);
        //add_method!(m, "com.sonoma.location_type.create", default_handler);
        //add_method!(m, "com.sonoma.location_type.update", default_handler);
        //add_method!(m, "com.sonoma.location_type.delete", default_handler);

        //// Performance
        //add_method!(m, "com.sonoma.performance.form.submissions", app::performance::submissions::get_form_submissions);
        //add_method!(m, "com.sonoma.performance.form.completeness", default_handler);
        //add_method!(m, "com.sonoma.performance.form.timeliness", default_handler);
        //add_method!(m, "com.sonoma.performance.form.missing", default_handler);
        //add_method!(m, "com.sonoma.performance.form.overdue", default_handler);
        //add_method!(m, "com.sonoma.performance.location.submissions", default_handler);

        //// Location groups
        //add_method!(m, "com.sonoma.location_groups", app::location_groups::querying::query_location_groups);

        //add_method!(m, "com.sonoma.locations.export", default_handler);

        //// Reporting periods
        //add_method!(m, "com.sonoma.reporting_periods", default_handler);
        //add_method!(m, "com.sonoma.reporting_period", default_handler);
        //add_method!(m, "com.sonoma.reporting_period.create", default_handler);
        //add_method!(m, "com.sonoma.reporting_period.update", default_handler);
        //add_method!(m, "com.sonoma.reporting_period.delete", default_handler);
        //add_method!(m, "com.sonoma.reporting_period.calendar", default_handler);

        //// Records
        add_method!(m, "com.sonoma.records", app::records::querying::query_records);
        add_method!(m, "com.sonoma.record", app::records::querying::get_record);
        //add_method!(m, "com.sonoma.record.submit", app::records::management::submit_record);
        //add_method!(m, "com.sonoma.record.retract", app::records::management::retract_record);
        //add_method!(m, "com.sonoma.record.amend", app::records::management::amend_record);

        //add_method!(m, "com.sonoma.record.push.sms", default_handler);

        //// Reporting Periods (location)
        //add_method!(m, "com.sonoma.reporting.check_duplicate", default_handler);
        //add_method!(m, "com.sonoma.reporting.check_overdue", default_handler);

        //// Forms
        //add_method!(m, "com.sonoma.forms", app::forms::querying::query_forms);
        add_method!(m, "com.sonoma.forms.available", app::forms::querying::get_available_forms);
        add_method!(m, "com.sonoma.forms.active", app::forms::querying::get_active_forms);
        add_method!(m, "com.sonoma.form", app::forms::querying::get_form);
        add_method!(m, "com.sonoma.form.metrics", app::forms::metrics::get_metrics);
        //add_method!(m, "com.sonoma.form.create", app::forms::management::create_form);
        //add_method!(m, "com.sonoma.form.update", app::forms::management::update_form);
        //add_method!(m, "com.sonoma.form.delete", app::forms::management::delete_form);

        //add_method!(m, "com.sonoma.form.submissions", app::forms::performance::get_form_submissions);

        //// Alarms
        //add_method!(m, "com.sonoma.alarms", app::alarms::querying::query_alarms);
        add_method!(m, "com.sonoma.alarms.available", app::alarms::querying::get_available_alarms);
        add_method!(m, "com.sonoma.alarms.active", app::alarms::querying::get_active_alarms);
        add_method!(m, "com.sonoma.alarms.all", app::alarms::querying::get_all_alarms);
        add_method!(m, "com.sonoma.alarm", app::alarms::querying::get_alarm);
        //add_method!(m, "Com.sonoma.alarm.create", app::alarms::management::create_alarm);
        //add_method!(m, "com.sonoma.alarm.update", app::alarms::management::update_alarm);
        //add_method!(m, "com.sonoma.alarm.delete", app::alarms::management::delete_alarm);
        //add_method!(m, "com.sonoma.alarm.test", app::alarms::management::test_alarm);
        //add_method!(m, "com.sonoma.alarm.guidance", app::alarms::management::get_guidance);
        //add_method!(m, "com.sonoma.alarm.raised", app::alarms::raised::get_alarm_raised);

        //// Users
        //add_method!(m, "com.sonoma.users", app::users::querying::query_users);

        //// Organizations
        //add_method!(m, "com.sonoma.organizations", app::organizations::querying::query_organizations);
        //add_method!(m, "com.sonoma.organizations.all", app::organizations::querying::get_all_organizations);
        //add_method!(m, "com.sonoma.organization", app::organizations::querying::get_organization);
        //add_method!(m, "com.sonoma.organization.create", app::organizations::management::create_organization);
        //add_method!(m, "com.sonoma.organization.delete", app::organizations::management::delete_organization);
        //add_method!(m, "com.sonoma.organization.update", app::organizations::management::update_organization);

        //// Alerts
        add_method!(m, "com.sonoma.alerts", app::alerts::querying::query_alerts);
        add_method!(m, "com.sonoma.alert", app::alerts::querying::get_alert);
        //add_method!(m, "com.sonoma.alert.action", app::alerts::actioning::action_alert);
        //add_method!(m, "com.sonoma.alert.update", app::alerts::management::update_alert);
        //add_method!(m, "com.sonoma.alert.delete", app::alerts::management::delete_alert);


        //// Documents
        add_method!(m, "com.sonoma.document", app::documents::get_by_id);
        //add_method!(m, "com.sonoma.documents", app::documents::querying::query_documents);
        //add_method!(m, "com.sonoma.document", app::documents::querying::get_document);
        //add_method!(m, "com.sonoma.document.update", app::documents::management::update_document);
        //add_method!(m, "com.sonoma.document.delete", app::documents::management::delete_document);
        //add_method!(m, "com.sonoma.document.create", app::documents::management::create_document);

        // Roles
        //add_method!(m, "com.sonoma.roles", app::roles::querying::query_roles);
        //add_method!(m, "com.sonoma.role", app::roles::querying::get_role);
        //add_method!(m, "com.sonoma.role.create", app::roles::management::create_role);
        //add_method!(m, "com.sonoma.role.delete", app::roles::management::delete_role);
        //add_method!(m, "com.sonoma.role.update", app::roles::management::update_role);

        // Assignments
        //add_method!(m, "com.sonoma.assignments", app::assignments::querying::query_assignments);
        //add_method!(m, "com.sonoma.assignment", app::assignments::querying::get_assignment);
        //add_method!(m, "com.sonoma.assignment.create", app::assignments::management::create_assignment);
        //add_method!(m, "com.sonoma.assignment.update", app::assignments::management::update_assignment);
        //add_method!(m, "com.sonoma.assignment.delete", app::assignments::management::delete_assignment);

        // Event Log
        //add_method!(m, "com.sonoma.events", app::events::querying::query_events);
        //add_method!(m, "com.sonoma.event", app::events::querying::get_event);
        //add_method!(m, "com.sonoma.event.parse", default_handler);
        //add_method!(m, "com.sonoma.event.parse_multiple", default_handler);

        // Application metrics and data

        //add_method!(m, "com.sonoma.analysis", default_handler);

        add_method!(m, "com.sonoma.system.main", system::dashboard::get_dashboard_data);
        m
    };
}

#[derive(Debug, Message)]
pub struct CommandResult {
    pub id: Uuid,
    pub d: Value,
}

#[derive(Debug, Message)]
pub struct CommandError {
    pub id: Uuid,
    pub description: String,
}


pub fn api_handler(state: Rc<RefCell<ApplicationState>>, id: Uuid, cmd: String, args: Value) -> Result<CommandResult, CommandError> {
    let args = args.clone();

    println!("{:?} command", cmd);
    println!("{:?}", args);

    let u_cmd: String = cmd.to_string();

    let s_cmd: &str = &u_cmd[..];

    if !API_METHODS.contains_key(s_cmd) {
        let result = Err(CommandError {
            description: "method not found".to_owned(),
            id: id,
        });
        result
    } else {
        let result = match API_METHODS.get(s_cmd) {
            Some(t_cmd) => {
                t_cmd(state.clone(), args)
            }
            None => {
                return Err(CommandError {
                    description: "Method not found".to_string(),
                    id: id,
                });
            }
        };

        match result {
            Ok(res) => {
                Ok(CommandResult {
                    id: id,
                    d: res,
                })
            }
            Err(err) => {
                eprintln!("{:?}", err);
                Err(CommandError {
                    id: id,
                    description: format!("{:?}", err),
                })
            }
        }
    }
}

