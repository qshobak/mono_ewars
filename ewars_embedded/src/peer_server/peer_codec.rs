use std::io::{self, Error};
use std::collections::HashMap;

use actix::prelude::*;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use tokio::codec::{Decoder, Encoder};
use bytes::BytesMut;

use crate::codec::{Tenancy};
use crate::models::config::{User, Account};

#[derive(Message, Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum PeerReq {
    Ping,
    Command {
        id: Uuid,
        cmd: String,
        args: Value
    },
    GetSeed {
        id: Uuid,
        tki: String,
    },
    SetUser {
        id: Uuid,
        uid: Uuid,
    },
    GetUsers{
        id: Uuid
    },
    AddAccount {
        accounts: Value
    },
    AuthRequest {
        email: String,
        password: String,
        domain: String,
        id: Uuid,
    },
    Authenticate {
        payload: Value,
        id: String,
    },
    SetMaster,
    GetRecordsSet {
        id: String,
        fid: Uuid,
        tki: String,
    },
    Ok,
}

#[derive(Message, Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum PeerResp {
    Ping,
    Pong,
    MasterSet,
    CommandResult {
        id: Uuid,
        error: Option<String>,
        d: Value
    },
    CommandError {
        id: Uuid,
        description: String,
    },
    SeedPackageCompleted {
        resource: String
    },
    Users {
        id: Uuid,
        users: Vec<User>,
        accounts: Vec<Account>,
    },
    UserSet {
        id: Uuid
    },
    Seeded {
        id: Uuid,
        tki: String,
        success: bool
    },
    SeedResult {
        id: Uuid,
        success: bool,
        error: Option<String>,
    },
    SeedUpdate {
        code: &'static str,
    },
    RecordSetDownloaded {
        tki: String,
        fid: Uuid,
        success: bool
    },
    Error { description: String },
    AuthResult {
        id: Uuid,
        error: Option<String>,
        success: bool,
    },
    Ok
}

pub struct PeerCodec;

impl Decoder for PeerCodec {
    type Item = PeerReq;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if let Some(index) = src.iter().position(|byte| *byte == b'\n') {
            let line = src.split_to(index + 1);
            let item = match json::from_slice(&line[0..line.len() - 1]) {
                Ok(res) => res,
                Err(err) => {
                    eprintln!("{:?}", err);
                    panic!();
                }
            };
            Ok(Some(item))
        } else {
            Ok(None)
        }
    }
}


impl Encoder for PeerCodec {
    type Item = PeerResp;
    type Error = Error;

    fn encode(&mut self, msg: PeerResp, dst: &mut BytesMut) -> Result<(), Self::Error> {
        let mut vec = json::to_vec(&msg)?;
        vec.push(b'\n');
        dst.extend_from_slice(&vec);
        Ok(())
    }
}



