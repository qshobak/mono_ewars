use actix::prelude::*;

use gtk;
use gtk::prelude::*;
use relm_attributes::widget;
use gtk::{Inhibit, Window, WindowType};
use relm::Widget;

use crate::ui::dashboard_main::DashboardMain;

pub struct Model {

}

#[derive(Msg)]
pub enum Msg {
    SwitchPage,
    Quit,
}

#[widget]
impl ::relm::Widget for Win {
    fn model() -> Model {
        Model {}
    }

    fn update(&mut self, event: Msg) {
        
    }

    view! {
        #[name="window"]
        gtk::Window {
            title: "EWARS",
            property_default_height: 768,
            property_default_width: 1024,
            gtk::Box {
                orientation: ::gtk::Orientation::Vertical,
                #[name="paned"]
                gtk::Paned {
                    child: {
                        expand: true,
                        fill: true,
                    },
                    orientation: ::gtk::Orientation::Horizontal,
                    #[name="notebook"]
                    gtk::Notebook {
                        tab_pos: ::gtk::PositionType::Top,
                        switch_page(_, _, _) => Msg::SwitchPage,
                        DashboardMain {},
                        DashboardMain {},
                        DashboardMain {},
                    }
                },
                gtk::Box {
                    orientation: ::gtk::Orientation::Horizontal,
                    gtk::Toolbar {
                    }
                }

            },
            delete_event(_, _) => (Msg::Quit, Inhibit(false)),
        }
    }
}

pub struct ApplicationWindow {}

impl Actor for ApplicationWindow {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        Win::run(()).unwrap();
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        Running::Stop
    }
}

impl ApplicationWindow {
    pub fn new() -> Self {
        ApplicationWindow {}
    }
}


impl Supervised for ApplicationWindow {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Remote: Restarting...");
    }
}
