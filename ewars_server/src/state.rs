use actix::prelude::*;

use tera;
use crate::db::db::{DbExecutor};
use crate::settings::{Settings};

use crate::services::{HookService, AlarmService, EmailService};
use crate::peer_server::{PeerServer};

pub struct AppState {
    pub template: tera::Tera,
    pub db: Addr<DbExecutor>,
    pub email_svc: Addr<EmailService>,
    pub hook_svc: Addr<HookService>,
    pub alarm_svc: Addr<AlarmService>,
    pub settings: Settings,
    pub peer_srv: Addr<PeerServer>,
}
