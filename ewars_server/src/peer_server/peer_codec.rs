use std::io::{self, Error, ErrorKind};
use std::collections::HashMap;

use actix::prelude::*;
use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;

use tokio::codec::{Decoder, Encoder};

use crate::handlers::peers::{Account, AccountUser};

use bytes::BytesMut;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum OrgId {
    Str(String),
    Id(Uuid),
}

#[derive(Debug, Message, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum PeerReq {
    Ok,
    Ping,
    SeedRequest {
        id: Uuid,
        tki: String
    },
    AuthRequest {
        id: Uuid,
        email: String,
        password: String,
    },
    RegistrationRequest {
        email: String,
        password: String,
        domain: String,
        org_id: Option<OrgId>,
        org_name: Option<String>,
    },
    Authenticate {
     tokens: Vec<String>,
    },
    Time
}

#[derive(Debug, Message, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum PeerResp {
    Ok,
    Pong,
    Ping,
    UnknownError,
    Authenticated,
    AuthenticationResult {
        id: Uuid,
        success: bool,
        error: Option<String>,
        users: Vec<AccountUser>,
        accounts: Vec<Account>,

    },
    SeedData {
        id: Uuid,
        tki: String,
        data: HashMap<String, Vec<Value>>,
        ts_ms: usize,
    },
    AuthResult {

    },
    RegistrationResult {

    },
    TimeResult {
        cur_time: DateTime<Utc>,
    }
}

pub struct PeerCodec;


impl Decoder for PeerCodec {
    type Item = PeerReq;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if let Some(index) = src.iter().position(|byte| *byte == b'\n') {
            let line = src.split_to(index + 1);
            let item = match json::from_slice(&line[0..line.len() - 1]) {
                Ok(res) => res,
                Err(err) => {
                    dbg!(err);
                    return Err(Error::new(ErrorKind::Other, "UNKNOWN_COMMAND"));
                }
            };

            Ok(Some(item))
        } else {
            Ok(None)
        }
    }
}

impl Encoder for PeerCodec {
    type Item = PeerResp;
    type Error = Error;

    fn encode(&mut self, msg: PeerResp, dst: &mut BytesMut) -> Result<(), Self::Error> {
        let mut vec = json::to_vec(&msg)?;
        vec.push(b'\n');
        dst.extend_from_slice(&vec);
        Ok(())
    }
}
