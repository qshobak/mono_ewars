use std::collections::HashMap;
use std::collections::VecDeque;
use std::time::{Instant, Duration};
use std::io::{self, Error};

use futures::Future;

use actix::prelude::*;

use tokio::net::TcpStream;
use tokio::io::WriteHalf;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::peers::{AccountUser};
use crate::peer_server::{PeerServer, PeerCodec, PeerReq, PeerResp};
use crate::handlers::{PeerCommand, PeerCommandResult};
use crate::db::db::{DbExecutor};

pub type PeerFrame = actix::io::FramedWrite<WriteHalf<TcpStream>, PeerCodec>;

#[derive(Message, Clone)]
pub struct NewPeerConnection {
    pub uuid: Uuid,
    pub addr: Addr<PeerConnection>,
}

#[derive(Debug, Message)]
pub struct PeerDisconnected(Uuid);

pub struct PeerConnection {
    pub id: Uuid,
    pub hb: Instant,
    pub server: Addr<PeerServer>,
    pub framed: actix::io::FramedWrite<WriteHalf<TcpStream>, PeerCodec>,
    pub did: Option<String>,
    pub tokens: Vec<String>,
    pub users: Vec<AccountUser>,
    pub db: Addr<DbExecutor>,
}

impl Actor for PeerConnection {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        self.server.send(NewPeerConnection {
                uuid: self.id.clone(), 
                addr: ctx.address()
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                actix::fut::ok(())
            }).spawn(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.server.do_send(PeerDisconnected(self.id));
        Running::Stop
    }
}

impl PeerConnection {

    pub fn new(server: Addr<PeerServer>, framed: PeerFrame, db: Addr<DbExecutor>) -> Self {
        PeerConnection {
            id: Uuid::new_v4(),
            hb: Instant::now(),
            server: server,
            framed: framed,
            did: Option::None,
            tokens: Vec::new(),
            users: Vec::new(),
            db: db,
        }
    }

    fn hb(&mut self, ctx: &mut Context<Self>) {
        ctx.run_later(Duration::new(10, 0), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                eprintln!("HEARTBEAT");
            }

            act.hb(ctx);
        });
    }

    pub fn authenticate(&mut self, ctx: &mut Context<Self>, tokens: Vec<String>) {
        eprintln!("{:?}", tokens);
        self.tokens = tokens.clone();
        self.db.send(PeerCommand::GetUsersFromTokens(tokens))
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Err(_) => {
                        act.framed.write(PeerResp::UnknownError);
                    },
                    Ok(Err(err)) => {
                        eprintln!("{:?}", err);
                        act.framed.write(PeerResp::UnknownError);
                    },
                    Ok(Ok(data)) => {
                        match data {
                            PeerCommandResult::Tenancies(tenancies) => {
                                act.users = tenancies.clone();
                                act.framed.write(PeerResp::Authenticated);
                            },
                            _ => {
                                act.framed.write(PeerResp::UnknownError);
                            }
                        }
                    },
                    _ => {}
                }

                actix::fut::ok(())
            }).spawn(ctx);
    }

    pub fn auth_request(&mut self, ctx: &mut Context<Self>, id: &Uuid, email: String, password: String) {
        let cid = id.clone();
        self.db.send(PeerCommand::Authenticate {
            email: email,
            password: password
        }).into_actor(self)
        .then(move |res, act, ctx| {
            match res {
                Err(_) => {
                    act.framed.write(PeerResp::UnknownError);
                },
                Ok(Err(err)) => {
                    eprintln!("{:?}", err);
                    act.framed.write(PeerResp::UnknownError);
                },
                Ok(Ok(data)) => {
                    match data {
                        PeerCommandResult::AuthenticationResult {success, error, users, accounts} => {
                            act.users = users.clone();
                            act.framed.write(PeerResp::AuthenticationResult {
                                id: cid.clone(),
                                success,
                                error,
                                users,
                                accounts,
                            });
                        },
                        _ => {}
                    }
                },
                _ => {}
            }

            actix::fut::ok(())
        }).spawn(ctx);
    }

    pub fn get_seed(&mut self, ctx: &mut Context<Self>, id: &Uuid, tki: &String) {
        let cid = id.clone();

        self.db.send(PeerCommand::GetSeed(tki.to_string()))
            .into_actor(self)
            .then(move |res, act, ctx| {
                match res {
                    Err(_) => act.framed.write(PeerResp::UnknownError),
                    Ok(Err(err)) => {
                        eprintln!("{:?}", err);
                        act.framed.write(PeerResp::UnknownError);
                    },
                    Ok(Ok(res)) => {
                        match res {
                            PeerCommandResult::SeedData {tki, data, ts_ms} => {
                                act.framed.write(PeerResp::SeedData {
                                    id: cid.clone(),
                                    tki: tki.clone(),
                                    ts_ms: ts_ms.clone(),
                                    data: data
                                });
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }

                actix::fut::ok(())
            }).spawn(ctx);
    }
}

impl actix::io::WriteHandler<Error> for PeerConnection {}

impl StreamHandler<PeerReq, Error> for PeerConnection {
    fn handle(&mut self, msg: PeerReq, ctx: &mut Self::Context) {
        eprintln!("RX: {:?}", msg);
        match msg {
            PeerReq::Authenticate { tokens } => {
                self.authenticate(ctx, tokens);
            },
            PeerReq::AuthRequest { id, email, password } => {
                self.auth_request(ctx, &id, email, password);
            },
            PeerReq::SeedRequest {id, tki} => {
                self.get_seed(ctx, &id, &tki);
            },
            _ => {}
        }
        eprintln!("HERE");
    }
}

