#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![allow(dead_code)]
#![allow(unused_imports)]

mod app;
mod services;
mod utils;
mod analysis;
mod codecs;
mod models;
mod db;
mod api;
mod state;
mod share;
mod handlers;
mod endpoints;
mod settings;
mod data;
mod peer_server;

extern crate env_logger;
extern crate config;
extern crate byteorder;
extern crate bytes;
extern crate futures;
extern crate rand;
extern crate serde;
extern crate serde_json;
extern crate base64;
extern crate csv;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate actix;
extern crate actix_web;
extern crate r2d2;
extern crate postgres;
extern crate r2d2_postgres;
extern crate postgres_binary_copy;
extern crate streaming_iterator;
extern crate chrono;
extern crate uuid;
#[macro_use]
extern crate tera;
extern crate num_cpus;
#[macro_use]
extern crate failure;
extern crate dotenv;
extern crate md5;
extern crate jsonwebtoken as jwt;
extern crate openssl;
extern crate cookie;
extern crate time;
extern crate rusoto_core;
extern crate rusoto_ses;
extern crate eval;
extern crate tokio;

use std::collections::HashMap;
use std::io;
use std::io::prelude::*;
use std::env;
use std::path::{PathBuf};
use std::fs::{File, remove_file};
use std::process::{Command};

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use bytes::{BytesMut, Bytes};
use futures::{Stream, Future};
use futures::{future, future::{result, Either}};
use futures::stream::{once};

use crate::settings::Settings;

use actix::prelude::*;
use r2d2_postgres::{TlsMode, PostgresConnectionManager};
use actix_web::{Body, Path, Error, server, App, HttpRequest, Responder, middleware, http, Query, State, error, HttpResponse,
                fs, Json, HttpMessage, AsyncResponder, FutureResponse};
use actix_web::http::{header, StatusCode, ContentEncoding};
use actix_web::middleware::identity::RequestIdentity;
use actix_web::middleware::identity::{CookieIdentityPolicy, IdentityService};
use r2d2::Pool;
use openssl::ssl::{SslMethod, SslAcceptor, SslFiletype};

use crate::peer_server::PeerServer;
use crate::handlers::api_handler::{ApiMethod, ApiResult, ApiError};
use crate::handlers::command_handler::{DbCommand, CommandResult, Organization, Invite, AccountInfo, DocumentInfo};
use crate::db::db::DbExecutor;
use crate::state::AppState;
use crate::models::{UserSession};

use crate::services::{EventBuilder, AlarmService, EmailService, HookService};

static VERSION: &'static str = r#"6.0.0"#;

fn not_found(
    (state, query): (State<AppState>, Query<HashMap<String, String>>),
) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    let s = state.template
        .render("404.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn login(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    let r = req.clone();
    req.clone().state().db.send(DbCommand::GetAccountInfoFromDomain{domain: host.clone()}) 
        .from_err()
        .and_then(move |res| {
            let account: Option<AccountInfo> = match res.unwrap() {
                CommandResult::AccountInfoResult(c) => {
                    Some(c)
                },
                _ => Option::None
            };

            if account.is_none() {
                Ok(HttpResponse::NotFound().into())
            } else {
                let mut ctx = tera::Context::new();

                let s =r.state().template
                    .render("login.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

                Ok(HttpResponse::Ok()
                   .content_type("text/html")
                   .body(s).into())
            }
        }).responder()
}

pub fn home_route(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let mut ctx = tera::Context::new();

    let s = req.state().template.render("home.html", &ctx)
        .map_err(|e| {
            error::ErrorInternalServerError("TEMPLATE_ERROR")
        }).unwrap();

    Box::new(future::ok(HttpResponse::Ok().content_type("text/html").body(s).into()))
}

fn recovery_view(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    let r = req.clone();

    req.clone().state().db.send(DbCommand::GetAccountInfoFromDomain{domain: host.clone()})
        .from_err()
        .and_then(move |res| {
            let account: Option<AccountInfo> = match res.unwrap() {
                CommandResult::AccountInfoResult(c) => {
                    Some(c)
                },
                _ => Option::None
            };

            if account.is_none() {
                Ok(HttpResponse::NotFound().into())
            } else {
                let mut ctx = tera::Context::new();

                let s = r.state().template
                    .render("recovery.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

                Ok(HttpResponse::Ok()
                   .content_type("text/html")
                   .body(s).into())
            }
        }).responder()
}

fn logout(req: &HttpRequest<AppState>) -> Result<HttpResponse, Error> {
    req.forget();

    Ok(HttpResponse::Found().header(header::LOCATION, "/login").finish())
}

fn dashboard_route((state, query, req): (State<AppState>, Query<HashMap<String, String>>, HttpRequest<AppState>)) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();

    let mut ident: Option<UserSession> = match req.identity() {
        Some(res) => {
            Some(json::from_str::<UserSession>(&res).unwrap())
        }
        None => None
    };

    // There's no actively logged in user
    if ident.is_none() {
        return Ok(HttpResponse::Found().header(header::LOCATION, "/login").finish());
    }

    let user: UserSession = ident.unwrap();

    let is_admin: bool = match user.role.as_ref() {
        "ACCOUNT_ADMIN" => true,
        _ => false
    };

    let menu = endpoints::component::get_menu_map(user.role.clone());
    ctx.insert("app_name", "dashboard");
    ctx.insert("page_title", "Dashboard");
    ctx.insert("menu", &menu);
    ctx.insert("is_admin", &is_admin);
    ctx.insert("user", &user);
    ctx.insert("theme", "flex");

    let s = state.template
        .render("app.html", &ctx)
        .map_err(|err| {
            eprintln!("{:?}", err);
            error::ErrorInternalServerError("Template error")
        })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}


fn _dashboard_route((state, query, req): (State<AppState>, Query<HashMap<String, String>>, HttpRequest<AppState>)) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();

    let mut ident: Option<UserSession> = match req.identity() {
        Some(res) => {
            Some(json::from_str::<UserSession>(&res).unwrap())
        }
        None => None
    };

    // There's no actively logged in user
    if ident.is_none() {
        return Ok(HttpResponse::Found().header(header::LOCATION, "/login").finish());
    }

    let user: UserSession = ident.unwrap();

    let is_admin: bool = match user.role.as_ref() {
        "ACCOUNT_ADMIN" => true,
        _ => false
    };


    let menu = endpoints::component::get_menu_map(user.role.clone());
    ctx.insert("app_name", "dashboard");
    ctx.insert("page_title", "Dashboard");
    ctx.insert("menu", &menu);
    ctx.insert("is_admin", &is_admin);
    ctx.insert("user", &user);
    ctx.insert("theme", "flex");

    let s = state.template
        .render("_app.html", &ctx)
        .map_err(|err| {
            eprintln!("{:?}", err);
            error::ErrorInternalServerError("Template error")
        })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn register_view(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    let ident: Option<UserSession> = match req.identity() {
        Some(res) => {
            Some(json::from_str::<UserSession>(&res).unwrap())
        },
        None => Option::None
    };

    let r = req.clone();
    req.state().db.send(DbCommand::GetAccountInfoFromDomain{domain: host.clone()})
        .from_err()
        .and_then(move |res| {
            let account: Option<AccountInfo> = match res.unwrap() {
                CommandResult::AccountInfoResult(c) => {
                    Some(c)
                },
                _ => Option::None
            };

            if account.is_none() {
                return Ok(HttpResponse::NotFound().into());
            }

            let account = account.unwrap();

            if account.account.is_none() {
                return Ok(HttpResponse::NotFound().into());
            }

            let mut ctx = tera::Context::new();

            ctx.insert("account", &account.account.unwrap());
            ctx.insert("orgs", &account.orgs);
            if let Some(ref c) = ident {
                ctx.insert("user", &c);
            }

            eprintln!("HERE");
            let s = r.state().template
                .render("register.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

            Ok(HttpResponse::Ok()
               .content_type("text/html")
               .body(s).into())
        }).responder()
}

fn invitation_view(info: Path<Invite>, req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    let r = req.clone();
    req.state().db.send(DbCommand::GetAccountInfoFromDomain{domain: host.clone()})
        .from_err()
        .and_then(move |res| {
            let account: Option<AccountInfo> = match res.unwrap() {
                CommandResult::AccountInfoResult(c) => {
                    Some(c)
                },
                _ => Option::None
            };

            if account.is_none() {
                Ok(HttpResponse::NotFound().into())
            } else {
                let mut ctx = tera::Context::new();

                let s = r.state().template
                    .render("invite.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

                Ok(HttpResponse::Ok()
                   .content_type("text/html")
                   .body(s).into())
            }
        }).responder()
}

fn document_handler(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();
    let code: String = req.match_info().query("code").unwrap_or("".to_string());

    let r = req.clone();
    req.state().db.send(DbCommand::GetDocumentInfo{code: code.clone(), domain: host.clone()})
        .from_err()
        .and_then(move |res| {
            let data: Option<DocumentInfo> = match res.unwrap() {
                CommandResult::Document(c) => {
                    Some(c)
                },
                _ => Option::None
            };

            if data.is_none() {
                Ok(HttpResponse::NotFound().into())
            } else {
                let rt: DocumentInfo = data.unwrap();
                if rt.account.is_none() {
                    Ok(HttpResponse::NotFound().into())
                } else if rt.template.is_none() {
                    Ok(HttpResponse::NotFound().into())
                } else {
                    let mut ctx = tera::Context::new();

                    ctx.insert("account", &rt.account);
                    ctx.insert("record", &rt.record);

                    if let Some(c) = rt.template {
                        // Temp fix for bad non-https external loads in templates
                        let mut content = c.content.replace("http://intel.ewars.ws", "https://intel.ewars.ws.s3.amazonaws.com");
                        content = content.replace("http://cdn.ewars.ws", "https://cdn.ewars.ws.s3.amazonaws.com");

                        ctx.insert("template", &c);
                        ctx.insert("template_id", &c.uuid);
                        ctx.insert("orientation", &c.orientation);
                        ctx.insert("template_content", &content);
                    } else {
                        ctx.insert("template", &"");
                        ctx.insert("template_id", &"");
                        ctx.insert("orientation", &"");
                        ctx.insert("template_content", &"");
                    }

                    if let Some(c) = rt.location_name {
                        ctx.insert("location_name", &c);
                        ctx.insert("location_id", &rt.location_id);
                    } else {
                        ctx.insert("location_id", &"");
                        ctx.insert("location_name", &"");
                    }

                    ctx.insert("report_date", &rt.report_date);
                    ctx.insert("pdf", &false);
                    ctx.insert("inline", &true);
                    ctx.insert("token", &"".to_string());


                    let s = r.state().template
                        .render("document.html", &ctx)
                        .map_err(|err| {
                            eprintln!("{:?}", err);
                            error::ErrorInternalServerError("TEMPLATE_ERROR")
                        }).unwrap();


                    Ok(HttpResponse::Ok()
                       .content_type("text/html")
                       .body(s).into())
                }
            }
        }).responder()
}


fn system_route(route: &'static str, req: &HttpRequest<AppState>) -> HttpResponse {
    let mut ctx = tera::Context::new();

    let ident: Option<UserSession> = match req.identity() {
        Some(res) => {
            Some(json::from_str::<UserSession>(&res).unwrap())
        }
        None => None
    };

    if ident.is_none() {
        return HttpResponse::Found().header(header::LOCATION, "/login").finish();
    }

    let user: UserSession = ident.unwrap();

    let is_admin: bool = match user.role.as_ref() {
        "ACCOUNT_ADMIN" => true,
        _ => false
    };

    let menu = endpoints::component::get_menu_map(user.role.clone());
    ctx.insert("app_name", &route);
    ctx.insert("page_title", "Settings");
    ctx.insert("menu", &menu);
    ctx.insert("is_admin", &is_admin);
    ctx.insert("user", &user);
    ctx.insert("theme", "admin");

    let s = req.state().template
        .render("_app.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

    HttpResponse::Ok()
        .content_type("text/html")
        .body(s)
}


pub fn api_endpoint((body, req): (String, HttpRequest<AppState>)) -> FutureResponse<HttpResponse> {
    let payload: Vec<Value> = json::from_str(&body).unwrap();

    let method_name: String = json::from_value(payload.get(0).unwrap().to_owned()).unwrap();
    let args: Value = json::from_value(payload.get(1).unwrap().to_owned()).unwrap();

    let mut user_session: UserSession;
    if let Some(ref session_user) = req.identity() {
        let session_user: UserSession = json::from_str(&session_user).unwrap();
        user_session = UserSession {
            uuid: session_user.uuid.clone(),
            aid: session_user.aid.clone(),
            name: session_user.name.clone(),
            email: session_user.email,
            role: session_user.role,
            tki: session_user.tki.clone(),
            account_name: session_user.account_name.clone(),
        }
    } else {
        return Box::new(future::ok(HttpResponse::new(StatusCode::from_u16(400).unwrap())));
    }

    req.state()
        .db
        .send(ApiMethod {
            cmd: method_name.clone(),
            args: args.clone(),
            kwargs: Option::None,
            user: user_session,
            alarm_svc: req.state().alarm_svc.clone(),
            hook_svc: req.state().hook_svc.clone(),
            email_svc: req.state().email_svc.clone(),
            settings: req.state().settings.clone(),
        })
        .from_err()
        .and_then(|res| {
            match res {
                Ok(dt) => {
                    Ok(HttpResponse::Ok().content_encoding(ContentEncoding::Auto).json(dt))
                }
                Err(et) => {
                    Ok(HttpResponse::Ok().json(et))
                }
            }
        }).responder()
}

pub fn download_handler(req: &HttpRequest<AppState>) -> HttpResponse {
    let mut file_path: PathBuf = PathBuf::from(&req.state().settings.server.files_path);
    let file_name: String = req.match_info().query("file_name").unwrap_or("".to_string());
    file_path.push(file_name);
    let end_file = file_path.to_str().unwrap();
    let mut file = File::open(end_file).unwrap();
    let mut contents = String::new();
    let _ = file.read_to_string(&mut contents).unwrap();
    let _ = remove_file(end_file).unwrap();
    HttpResponse::Ok()
        .chunked()
        .body(Body::Streaming(Box::new(once(Ok(Bytes::from(contents))))))
}

pub fn generate_pdf(req: &HttpRequest<AppState>) -> HttpResponse {
    let mut cmd = Command::new(req.state().settings.server.chromium_path.clone());
    cmd.arg("--headless");
    cmd.arg("--disable-gpu");
    cmd.arg("--print-to-pdf=output.pdf");

    cmd.arg("http://ewars.ws");

    let result = cmd.output().expect("HELLO");
    eprintln!("{:?}", result);

    HttpResponse::Ok().content_type("text/html").body("Hello")
}


fn main() {
    ::std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let system = System::new("sonoma");

    let settings = Settings::new().unwrap();
    
    eprintln!("{:?}", settings);

    let manager = PostgresConnectionManager::new(settings.database.db_url.clone(), TlsMode::None).unwrap();
    let pool = Pool::new(manager).unwrap();

    eprintln!("{}", num_cpus::get());
    let addr = SyncArbiter::start(num_cpus::get(), move || DbExecutor(pool.clone()));

    std::env::set_var("AWS_ACCESS_KEY_ID", settings.amazon_aws.aws_access_key.clone());
    std::env::set_var("AWS_SECRET_ACCESS_KEY", settings.amazon_aws.aws_secret_key.clone());

    //let email_addr = SyncArbiter::start(1, move || EmailService::new());
    let email_addr = Supervisor::start(|_| EmailService::new());
    let alarm_addr = Supervisor::start(|_| AlarmService::new());
    let event_service = Supervisor::start(|_| EventBuilder::new());
    let hook_addr = SyncArbiter::start(1, move || HookService::new());
    let srv_db = addr.clone();
    let peer_srv = Supervisor::start(move |_| PeerServer::new(srv_db.clone()));

    // Start alarm evaluator
    // Start notification manager
    // Start tcp notifier

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder.set_private_key_file("key.pem", SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();

    server::new(move || {
        let tera = compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*"));
        App::with_state(AppState { 
                template: tera, 
                db: addr.clone(),
                hook_svc: hook_addr.clone(),
                email_svc: email_addr.clone(),
                alarm_svc: alarm_addr.clone(),
                settings: settings.clone(),
                peer_srv: peer_srv.clone(),
            })
            .default_encoding(ContentEncoding::Gzip)
            .middleware(
                middleware::DefaultHeaders::new()
                        .header("Access-Control-Allow-Origin", "*")
                        .header("Access-Control-Allow-Credentials", "true")
            )
            .middleware(middleware::Logger::default())
            .middleware(
                IdentityService::new(
                    CookieIdentityPolicy::new(&[0; 32])
                        .name("sonoma_v6")
                        .secure(true)
                        .path("/"),
                )
            )
            .handler("/static", fs::StaticFiles::new("./static").unwrap().show_files_listing())
            .resource("/", |r| r.method(http::Method::GET).with(dashboard_route))
            .resource("/logout", |r| r.method(http::Method::GET).f(logout))
            .resource("/login", |r| {
                r.get().f(login);
                r.post().with(api::authentication::web_auth);
            })
            .resource("/register", |r| {
                r.get().f(register_view);
                r.post().with(endpoints::handle_registration);
            })
            .resource("/access", |r| {
                r.post().with(endpoints::handle_access_request);
            })
            .resource("/api", |r| {
                r.post().with(api_endpoint);
            })
            .resource("/api/_w", |r| {
                r.post().with(api_endpoint);
            })
            .resource("/api/_m", |r| {
                r.post().with(api_endpoint);
            })
            .resource("/dummy", |r| r.method(http::Method::GET).f(home_route))
            .resource("/alert/{uuid}", |r| r.method(http::Method::GET).with(not_found))
            .resource("/record/{uuid}", |r| r.method(http::Method::GET).with(not_found))
            .resource("/invitation/{uuid}", |r| r.method(http::Method::GET).with(not_found))
            .resource("/verify/{ver_code}", |r| r.method(http::Method::GET).f(endpoints::verify_email))
            .resource("/recover", |r| {
                r.get().with(not_found);
                r.post().with(not_found);
            })
            .resource("/document/{code}", |r| r.method(http::Method::GET).f(document_handler))
            .resource("/settings", |r| r.method(http::Method::GET).f(|req| {
                system_route("settings", req)
            }))
            .resource("/profile/{uuid}", |r| r.method(http::Method::GET).f(|req| {
                system_route("profile", req)
            }))
            .resource("/task/{task_id}", |r| r.method(http::Method::GET).with(not_found))
            .resource("/recovery", |r| {
                r.get().f(recovery_view);
                r.post().with(not_found);
            })
            .resource("/recovery/{recover_code}", |r| {
                r.get().with(not_found);
                r.post().with(not_found);
            })
            .resource("/pdf", |r| r.method(http::Method::GET).f(generate_pdf))
            .resource("/download/{file_name}", |r| r.method(http::Method::GET).f(download_handler))
            .resource("/print/record/{record_id}", |r| r.method(http::Method::GET).f(endpoints::print_record))
            .resource("/{app_name}", |r| r.method(http::Method::GET).f(endpoints::component::app_route))
    })
        .bind_ssl("127.0.0.1:8000", builder)
        //.bind("127.0.0.1:8000")
        .unwrap()
        .start();


    system.run();
}
