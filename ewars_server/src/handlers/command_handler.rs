use std::str;
use std::io;

use postgres::rows::{Row};

use actix::prelude::*;
use failure::Error;
use jwt::{encode, Header, Algorithm};
use uuid::Uuid;
use base64;
use chrono::prelude::*;

use serde_json as json;
use serde_json::Value;

use crate::db::db::DbExecutor;
use crate::utils::auth::verify_pass;
use crate::share::common::Claims;

use crate::app::{Record as PrintRecord, get_print_record};
use crate::app::{RegistrationRequest, attempt_registration};
use crate::app::{AccessRequest, attempt_access_request};
use crate::app::{verify_user};

use crate::services::{EmailService};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Organization {
    pub uuid: Uuid,
    pub name: String,
}

impl<'a> From<Row<'a>> for Organization {
    fn from(row: Row) -> Self {
        Organization {
            uuid: row.get(0),
            name: row.get(1),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AccountData {
    pub uuid: Uuid,
    pub name: String,
    pub tki: String,
    pub domain: String,
    pub status: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct AccountInfo {
    pub account: Option<AccountData>,
    pub orgs: Vec<Organization>,
}


impl<'a> From<Row<'a>> for AccountData {
    fn from(row: Row) -> Self {
        AccountData {
            uuid: row.get("uuid"),
            name: row.get("name"),
            tki: row.get("tki"),
            domain: row.get("domain"),
            status: row.get("status"),
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct Invite {
    uuid: Uuid,
    email: String,
    name: String,
}

impl<'a> From<Row<'a>> for Invite {
    fn from(row: Row) -> Self {
        Invite {
            uuid: row.get("uuid"),
            email: row.get("email"),
            name: row.get("name"),
        }
    }
}


static SQL_GET_TEMPLATE: &'static str = r#"
    SELECT uuid, template_name, instance_name, content, data, generation, orientation, status, template_type
    FROM {SCHEMA}.documents
    WHERE uuid = $1;
"#;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DocumentRecord {
    pub uuid: Uuid,
    pub data: Value,
    pub submitted: DateTime<Utc>,
    pub submitted_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub form_name: Value,
    pub loc_name: Option<Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Template {
    pub uuid: Uuid,
    pub template_name: Value,
    pub instance_name: Value,
    pub content: String,
    pub definition: Value,
    pub generation: Value,
    pub orientation: String,
    pub status: String,
    pub template_type: String,
}

impl<'a> From<Row<'a>> for Template {
    fn from(row: Row) -> Self {
        Template {
            uuid: row.get("uuid"),
            template_name: row.get("template_name"),
            instance_name: row.get("instance_name"),
            content: row.get("content"),
            definition: row.get("data"),
            generation: row.get("generation"),
            orientation: row.get("orientation"),
            status: row.get("status"),
            template_type: row.get("template_type"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DocumentInfo {
    pub account: Option<AccountData>,
    pub template: Option<Template>,
    pub report_date: Option<NaiveDate>,
    pub location_name: Option<Value>,
    pub record: Option<DocumentRecord>,
    pub location_id: Option<Uuid>,
}

pub enum CommandResult {
    OrgsResult(Vec<Organization>),
    InviteResult(Invite),
    AccountInfoResult(AccountInfo),
    Document(DocumentInfo),
    PrintRecord(PrintRecord),
    RegistrationResult(bool, String),
    AccessResult(bool, String),
    VerificationResult(bool, Option<String>),
    Error(),
}

pub enum DbCommand {
    GetAccountFromDomain {
        domain: String,
    },
    GetAccountInfoFromDomain {
        domain: String,
    },
    GetPrintRecord(String, Uuid),
    GetAccountOrganizations {
        tki: String,
    },
    RegRequest(RegistrationRequest),
    AccRequest(AccessRequest),
    VerificationRequest(String, Addr<EmailService>),
    GetDocumentInfo {
        code: String,
        domain: String,
    },
    GetInvite {
        id: Uuid,
        tki: String,
    }
}

impl Message for DbCommand {
    type Result = Result<CommandResult, ()>;
}

static SQL_GET_ACCOUNT: &'static str = r#"
    SELECT uuid, name, tki, domain, status
    FROM core.accounts WHERE domain =$1;
"#;

static SQL_GET_ORGS: &'static str = r#"
    SELECT uuid, name->>'en' AS name
    FROM {SCHEMA}.organizations
    ORDER BY name->>'en';
"#;

static SQL_GET_LOC_NAME: &'static str = r#"
    SELECT name FROM {SCHEMA}.locations WHERE uuid = $1;
"#;

impl Handler<DbCommand> for DbExecutor {
    type Result = Result<CommandResult, ()>;

    fn handle(&mut self, msg: DbCommand, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();
        match msg {
            DbCommand::GetAccountInfoFromDomain {domain} => {
                let mut tmp_domain: String = domain.split(":").collect::<Vec<&str>>()[0].to_string();
                let account: Option<AccountData> = match &conn.query(&SQL_GET_ACCOUNT, &[&tmp_domain]) {
                    Ok(rows) => {
                        if let Some(row) = rows.iter().next() {
                            Some(AccountData::from(row))
                        } else {
                            Option::None
                        }
                    },
                    Err(err) => {
                        eprintln!("{:?}", err);
                        Option::None
                    }
                };

                if let Some(acc) = account {
                    let orgs: Vec<Organization> = match &conn.query(&SQL_GET_ORGS.replace("{SCHEMA}", &acc.tki), &[]) {
                        Ok(rows) => {
                            rows.iter().map(|x| Organization::from(x)).collect()
                        },
                        Err(err) => {
                            eprintln!("{:?}", err);
                            Vec::new()
                        }
                    };

                    Ok(CommandResult::AccountInfoResult(AccountInfo {
                        account: Some(acc.clone()),
                        orgs: orgs.clone(),
                    }))
                } else {
                    Ok(CommandResult::AccountInfoResult(AccountInfo {
                        account: Option::None,
                        orgs: Vec::new(),
                    }))
                }

            },
            DbCommand::GetPrintRecord(tki, id) => get_print_record(&tki, &id, &conn),
            DbCommand::RegRequest(req) => {
                Ok(attempt_registration(&conn, &req))
            },
            DbCommand::VerificationRequest(code, email_svc) => verify_user(&conn, &code, email_svc),
            DbCommand::GetDocumentInfo {code, domain} => {
                let byteline: Vec<u8> = base64::decode(&code).unwrap();
                let data: String = String::from_utf8_lossy(&byteline).to_string();


                let mut template_id: Option<Uuid> = Option::None;
                let mut location_id: Option<Uuid> = Option::None;
                let mut report_date: Option<NaiveDate> = Option::None;

                let parts: Vec<&str> = data.split(";").collect();
                eprintln!("{:?}", parts);

                for part in &parts {
                    let define: Vec<&str> = part.split(":").collect();
                    match define[0].as_ref() {
                        "l" => {
                            location_id = Some(Uuid::parse_str(&define[1]).unwrap());
                        },
                        "t" => {
                            template_id = Some(Uuid::parse_str(&define[1]).unwrap());
                        },
                        "d" => {
                            report_date = Some(NaiveDate::parse_from_str(&define[1], "%Y-%m-%d").unwrap());
                        },
                        _ => {}
                    }
                }

                if template_id.is_none() {
                    return Err(());
                }

                let mut tmp_domain: String = domain.split(":").collect::<Vec<&str>>()[0].to_string();
                let account: Option<AccountData> = match &conn.query(&SQL_GET_ACCOUNT, &[&tmp_domain]) {
                    Ok(rows) => {
                        if let Some(row) = rows.iter().next() {
                            Some(AccountData::from(row))
                        } else {
                            Option::None
                        }
                    },
                    Err(err) => {
                        eprintln!("{:?}", err);
                        Option::None
                    }
                };

                if account.is_none() {
                    return Err(())
                }

                let account = account.unwrap();

                let mut template: Option<Template> = Option::None;
                match &conn.query(&SQL_GET_TEMPLATE.replace("{SCHEMA}", &account.tki), &[&template_id.unwrap()]) {
                    Ok(rows) => {
                        if let Some(row) = rows.iter().next() {
                            template = Some(Template::from(row));
                        } else {
                            template = Option::None;
                        }
                    },
                    Err(err) => {
                        eprintln!("{:?}", err);
                        template = Option::None;
                    }
                }


                let mut location_name: Option<Value> = Option::None;
                if let Some(loc) = location_id {
                    location_name = match &conn.query(&SQL_GET_LOC_NAME.replace("{SCHEMA}", &account.tki), &[&loc]) {
                        Ok(rows) => {
                            if let Some(row) = rows.iter().next() {
                                Some(row.get(0))
                            } else {
                                Option::None
                            }
                        },
                        Err(err) => {
                            eprintln!("{:?}", err);
                            Option::None
                        }
                    }
                }

                let result = DocumentInfo {
                    account: Some(account),
                    template,
                    report_date,
                    location_name,
                    location_id,
                    record: Option::None,
                };

                Ok(CommandResult::Document(result))
            },
            _ => {
                Err(())
            }
        }
    }
}

