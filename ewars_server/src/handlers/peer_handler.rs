use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};
use std::io;

use actix::prelude::*;
use failure::Error;
use jwt::{encode, decode, Header, Algorithm, Validation};
use uuid::Uuid;
use base64;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};

use crate::db::db::DbExecutor;
use crate::utils::auth::verify_pass;
use crate::share::common::Claims;
use crate::handlers::api_handler::{PoolConnection};
use crate::handlers::peers::{authenticate_user, Account, AccountUser};

use crate::app::verify_user;
use sonomalib::models::*;

pub enum PeerCommand {
    Authenticate {
        email: String,
        password: String
    },
    GetSeed(String),
    GetUsersFromTokens(Vec<String>)
}

pub enum PeerCommandResult {
    AuthenticationResult {
        success: bool,
        error: Option<String>,
        users: Vec<AccountUser>,
        accounts: Vec<Account>,
    },
    SeedData {
        data: HashMap<String, Vec<Value>>,
        tki: String,
        ts_ms: usize,
    },
    Tenancies(Vec<AccountUser>),
    UnknownError,
}

impl Message for PeerCommand {
    type Result = Result<PeerCommandResult, Error>;
}

impl Handler<PeerCommand> for DbExecutor {
    type Result = Result<PeerCommandResult, Error>;

    fn handle(&mut self, msg: PeerCommand, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();

        match msg {
            PeerCommand::Authenticate { email, password } => authenticate_user(&conn, &email, &password),
            PeerCommand::GetSeed(tki) => get_seed(&conn, &tki),
            _ => {
                bail!("UNK_COMMAND");
            }
        }
    }
}

lazy_static! {
    static ref RESOURCES: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();

        m.insert("locations", "SELECT * FROM {SCHEMA}.locations;");
        m.insert("resources", "SELECT * FROM {SCHEMA}.resources;");
        m.insert("location_types", "SELECT * FROM {SCHEMA}.location_types");
        m.insert("forms", "SELECT * FROM {SCHEMA}.forms;");
        m.insert("users", "SELECT * FROM {SCHEMA}.users;");
        m.insert("alarms", "SELECT * FROM {SCHEMA}.alarms;");
        m.insert("alerts", "SELECT * FROM {SCHEMA}.alerts;");
        m.insert("documents", "SELECT * FROM {SCHEMA}.documents;");
        m.insert("reporting", "SELECT * FROM {SCHEMA}.reporting;");
        m.insert("organizations", "SELECT * FROM {SCHEMA}.organizations;");
        m.insert("conf", "SELECT * FROM {SCHEMA}.conf;");
        m.insert("assignments", "SELECT * FROM {SCHEMA}.assignments;");
        m.insert("indicators", "SELECT * FROM {SCHEMA}.indicators;");
        m.insert("indicator_groups", "SELECT * FROM {SCHEMA}.indicator_groups;");
        m.insert("roles", "SELECT * FROM {SCHEMA}.roles;");
        m.insert("tasks", "SELECT * FROM {SCHEMA}.tasks;");
        m.insert("records", "SELECT * FROM {SCHEMA}.records;");
        m.insert("outbreaks", "SELECT * FROM {SCHEMA}.outbreaks;");
        m.insert("news", "SELECT * FROM {SCHEMA}.news;");
        m.insert("intervals", "SELECT * FROM {SCHEMA}.intervals;");
        m.insert("users", "SELECT * FROM {SCHEMA}.users;");

        m
    };
}

// get the seed for an account
fn get_seed(conn: &PoolConnection, tki: &String) -> Result<PeerCommandResult, Error> {
    let mut results: HashMap<String, Vec<Value>> = HashMap::new();

    for (key, query) in RESOURCES.iter() {
        let items: Vec<Value> = match &conn.query(&query.replace("{SCHEMA}", &tki), &[]) {
            Ok(rows) => {
                match key.as_ref() {
                    "locations" => rows.iter().map(|x| json::to_value(Location::from(x)).unwrap()).collect(),
                    "resources" => rows.iter().map(|x| json::to_value(Resource::from(x)).unwrap()).collect(),
                    "location_types" => rows.iter().map(|x| json::to_value(LocationType::from(x)).unwrap()).collect(),
                    "forms" => rows.iter().map(|x| json::to_value(Form::from(x)).unwrap()).collect(),
                    "users" => rows.iter().map(|x| json::to_value(User::from(x)).unwrap()).collect(),
                    "alarms" => rows.iter().map(|x| json::to_value(Alarm::from(x)).unwrap()).collect(),
                    "alerts" => rows.iter().map(|x| json::to_value(Alert::from(x)).unwrap()).collect(),
                    "documents" => rows.iter().map(|x| json::to_value(Document::from(x)).unwrap()).collect(),
                    "reporting" => rows.iter().map(|x| json::to_value(ReportingPeriod::from(x)).unwrap()).collect(),
                    "organizations" => rows.iter().map(|x| json::to_value(Organization::from(x)).unwrap()).collect(),
                    "conf" => rows.iter().map(|x| json::to_value(Conf::from(x)).unwrap()).collect(),
                    "assignments" => rows.iter().map(|x| json::to_value(Assignment::from(x)).unwrap()).collect(),
                    "indicators" => rows.iter().map(|x| json::to_value(Indicator::from(x)).unwrap()).collect(),
                    "indicator_groups" => rows.iter().map(|x| json::to_value(IndicatorGroup::from(x)).unwrap()).collect(),
                    "roles" => rows.iter().map(|x| json::to_value(Role::from(x)).unwrap()).collect(),
                    "outbreaks" => rows.iter().map(|x| json::to_value(Outbreak::from(x)).unwrap()).collect(),
                    "news" => rows.iter().map(|x| json::to_value(NewsItem::from(x)).unwrap()).collect(),
                    "intervals" => rows.iter().map(|x| json::to_value(Interval::from(x)).unwrap()).collect(),
                    "tasks" => rows.iter().map(|x| json::to_value(Task::from(x)).unwrap()).collect(),
                    "records" => rows.iter().map(|x| json::to_value(Record::from(x)).unwrap()).collect(),
                    "users" => rows.iter().map(|x| json::to_value(User::from(x)).unwrap()).collect(),
                    _ => Vec::new()
                }
            }
            Err(err) => {
                eprintln!("{:?}", err);
                bail!("ERR_LOADING_SEED");
            }
        };

        eprintln!("DONE_BUILDING_SEED");

        results.insert(key.to_string(), items);
    }

    let duration_since = SystemTime::now().duration_since(UNIX_EPOCH).expect("Ooops, back to the future");
    let in_ms = duration_since.as_secs() * 1000 + duration_since.subsec_millis() as u64 / 1_000_000;

    Ok(PeerCommandResult::SeedData {
        data: results,
        tki: tki.clone(),
        ts_ms: in_ms as usize,
    })
}
