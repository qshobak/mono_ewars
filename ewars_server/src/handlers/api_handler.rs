use std::io;
use std::collections::HashMap;

use futures::{Future};

use failure::Error;
use serde_json as json;
use serde_json::Value;
use actix::prelude::*;
use actix_web::{FutureResponse};
use jwt::{encode, Header, Algorithm};
use uuid::Uuid;

use r2d2::{Pool, PooledConnection};
use r2d2_postgres::{PostgresConnectionManager};

use crate::utils::auth::verify_pass;
use crate::share::common::Claims;
use crate::db::db::{DbExecutor};
use crate::api;
use crate::analysis;
use crate::models::{UserSession};
use crate::state::AppState;
use crate::services::{HookService, AlarmService, EmailService};
use crate::settings::{Settings};

use postgres::{Connection};

pub type PoolConnection = PooledConnection<PostgresConnectionManager>;

#[derive(Debug, Serialize, Deserialize)]
pub struct ApiError {
    pub success: bool,
    pub code: String,
}

pub struct ApiMethod {
    pub cmd: String,
    pub args: Value,
    pub kwargs: Option<Value>,
    pub user: UserSession,
    pub alarm_svc: Addr<AlarmService>,
    pub email_svc: Addr<EmailService>,
    pub hook_svc: Addr<HookService>,
    pub settings: Settings,
}

impl Message for ApiMethod {
    type Result = Result<ApiResult, ApiError>;
}


fn default_handler(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    Ok(json::to_value(true).unwrap())
}

macro_rules! add_method {
    ($m: expr, $name: expr, $handler:expr) => {
        {
            $m.insert($name, $handler as fn(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error>);
        }
    }
}

lazy_static! {
    static ref API_METHODS: HashMap<&'static str, fn(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error>> = {
        let mut m = HashMap::new();

        add_method!(m, "com.sonoma.analysis", analysis::handle_query);
        add_method!(m, "com.sonoma.table.locations", analysis::handle_table_loc_query);

        add_method!(m, "com.ewars.user.reinstate", default_handler);
        add_method!(m, "com.ewars.activity", api::myself::activity::get_activity);

        // Export
        add_method!(m, "com.sonoma.export.records", api::export::export_records);
        add_method!(m, "com.sonoma.export.alerts", api::export::export_alerts);

        add_method!(m, "com.sonoma.config", api::myself::get_config);

        // Metrics

        add_method!(m, "com.sonoma.resources", api::resources::query_resources);
        add_method!(m, "com.sonoma.resource", api::resources::get_resource);
        add_method!(m, "com.sonoma.resource.create", api::resources::create_resource);
        add_method!(m, "com.sonoma.resource.delete", api::resources::delete_resource);
        add_method!(m, "com.sonoma.resource.update", api::resources::update_resource);
        add_method!(m, "com.sonoma.resource.export", api::resources::export_resource);
        add_method!(m, "com.sonoma.resource.import", api::resources::import_resource);

        add_method!(m, "com.sonoma.resources.layer_options", api::resources::get_map_layer_options);

        // User
        add_method!(m, "com.ewars.user.get", default_handler);
        add_method!(m, "com.ewars.user.profile.activity", default_handler);
        add_method!(m, "com.ewars.user.assignments", default_handler);
        add_method!(m, "com.ewars.user.tasks", default_handler);
        add_method!(m, "com.ewars.user.notifications", default_handler);
        add_method!(m, "com.ewars.user.missing", default_handler);
        add_method!(m, "com.ewars.user.upcoming", default_handler);
        add_method!(m, "com.ewars.user.overdue", default_handler);
        add_method!(m, "com.ewars.user.alerts", default_handler);
        add_method!(m, "com.ewars.user.register", default_handler);
        add_method!(m, "com.ewars.user.grant", default_handler);
        add_method!(m, "com.sonoma.user.revoke", api::users::revoke_access);

        add_method!(m, "com.sonoma.self", api::myself::get);
        add_method!(m, "com.sonoma.self.update_password", api::myself::update_password);
        add_method!(m, "com.sonoma.self.update", api::myself::update_user);
        add_method!(m, "com.sonoma.self.profile", api::myself::get_own_profile);
        add_method!(m, "com.sonoma.self.dashboards", api::myself::get_dashboards);
        add_method!(m, "com.sonoma.self.documents", api::myself::documents::get_documents);
        add_method!(m, "com.sonoma.self.activity", default_handler);
        add_method!(m, "com.sonoma.self.metrics", default_handler);
        add_method!(m, "com.sonoma.self.metric", api::myself::get_user_metric);
        add_method!(m, "com.sonoma.self.forms", api::myself::get_user_forms);
        add_method!(m, "com.sonoma.self.assignments", api::myself::get_derived_assignments);

        add_method!(m, "com.ewars.user.create.check_email", default_handler);

        // Invites
        add_method!(m, "com.ewars.invite", default_handler);
        add_method!(m, "com.ewars.invites", default_handler);
        add_method!(m, "com.sonoma.invite.delete", api::users::delete_invite);
        add_method!(m, "com.ewars.invite.resend", default_handler);

        // Forms
        add_method!(m, "com.sonoma.forms.active", api::forms::get_active_forms);
        add_method!(m, "com.sonoma.form", api::forms::get_form);
        add_method!(m, "com.sonoma.forms", api::forms::query_forms);
        add_method!(m, "com.sonoma.form.create", api::forms::create_form);
        add_method!(m, "com.sonoma.form.update", api::forms::update_form);
        add_method!(m, "com.sonoma.form.delete", api::forms::delete_form);
        add_method!(m, "com.sonoma.form.import", api::forms::import_form);
        add_method!(m, "com.sonoma.form.export", api::forms::export_form);
        add_method!(m, "com.sonoma.form.duplicate", api::forms::duplicate_form);

        // Records
        add_method!(m, "com.sonoma.records", api::records::query_records);
        add_method!(m, "com.sonoma.record", api::records::get_record);
        add_method!(m, "com.sonoma.record.amend", api::records::amend_record);
        add_method!(m, "com.sonoma.record.delete", api::records::delete_record);
        add_method!(m, "com.sonoma.record.submit", api::records::submit_record);

        // Indicators
        add_method!(m, "com.sonoma.indicators", api::indicators::query_indicators);
        add_method!(m, "com.sonoma.indicators.tree", api::indicators::tree_query);
        add_method!(m, "com.sonoma.indicator", api::indicators::get_indicator);
        add_method!(m, "com.sonoma.indicator.inbound", api::indicators::get_inbound);
        add_method!(m, "com.sonoma.indicator.create", api::indicators::create_indicator);
        add_method!(m, "com.sonoma.indicator.update", api::indicators::update_indicator);
        add_method!(m, "com.sonoma.indicator.delete", api::indicators::delete_indicator);

        // Indicator Groups
        add_method!(m, "com.sonoma.indicator_groups", api::indicator_groups::query_indicator_groups);
        add_method!(m, "com.sonoma.indicator_group", api::indicator_groups::get_indicator_group);
        add_method!(m, "com.sonoma.indicator_group.create", api::indicator_groups::create_indicator_group);
        add_method!(m, "com.sonoma.indicator_group.update", api::indicator_groups::update_indicator_group);
        add_method!(m, "com.sonoma.indicator_group.delete", api::indicator_groups::delete_indicator_group);
        add_method!(m, "com.sonoma.indicator_groups.root", api::indicator_groups::get_root_indicator_groups);
        add_method!(m, "com.sonoma.indicator_group.children", api::indicator_groups::get_indicator_group_childs);

        // Alarms
        add_method!(m, "com.sonoma.alarms", api::alarms::query_alarms);
        add_method!(m, "com.sonoma.alarms.active", api::alarms::get_active_alarms);
        add_method!(m, "com.sonoma.alarm", api::alarms::get_alarm);
        add_method!(m, "com.sonoma.alarm.create", api::alarms::create_alarm);
        add_method!(m, "com.sonoma.alarm.update", api::alarms::update_alarm);
        add_method!(m, "com.sonoma.alarm.delete", api::alarms::delete_alarm);

        // Locations
        add_method!(m, "com.sonoma.locations.root", api::locations::get_root_locations);
        add_method!(m, "com.sonoma.location", api::locations::get_location);
        add_method!(m, "com.sonoma.location.digest", api::locations::get_location_digest);
        add_method!(m, "com.sonoma.location.create", api::locations::create_location);
        add_method!(m, "com.sonoma.location.update", api::locations::update_location);
        add_method!(m, "com.sonoma.location.delete", api::locations::delete_location);
        add_method!(m, "com.sonoma.locations.import", default_handler);
        add_method!(m, "com.sonoma.locations.export", default_handler);
        add_method!(m, "com.sonoma.location.completeness", api::locations::get_location_completeness);
        add_method!(m, "com.sonoma.location.timeliness", api::locations::get_location_timeliness);
        add_method!(m, "com.sonoma.locations.tree", api::locations::query_locations_tree);
        add_method!(m, "com.sonoma.location.reporting", api::locations::get_reporting_periods);
        add_method!(m, "com.sonoma.location.disable_children", api::locations::disable_child_locations);
        add_method!(m, "com.sonoma.location.enable_children", api::locations::enable_child_locations);

        add_method!(m, "com.sonoma.location_groups", api::locations::get_location_groups);


        // Assignments
        add_method!(m, "com.sonoma.assignments", api::assignments::query_assignments);
        add_method!(m, "com.sonoma.assignment", api::assignments::get_assignment);
        add_method!(m, "com.sonoma.assignment.create", api::assignments::create_assignment);
        add_method!(m, "com.sonoma.assignment.update", api::assignments::update_assignment);
        add_method!(m, "com.sonoma.assignment.delete", api::assignments::delete_assignment);
        add_method!(m, "com.sonoma.assignment.request", api::assignments::request_assignment);
        add_method!(m, "com.sonoma.assignment.reject", api::assignments::reject_assignment);
        add_method!(m, "com.sonoma.assignment.approve", api::assignments::approve_assignment);

        // Registration Requests
        add_method!(m, "com.sonoma.registration.approve", default_handler);
        add_method!(m, "com.sonoma.registration.reject", default_handler);

        // Amendments
        add_method!(m, "com.sonoma.amendment.approve", api::records::approve_amendment);
        add_method!(m, "com.sonoma.amendment.reject", api::records::reject_amendment);
        
        // Retractions
        add_method!(m, "com.sonoma.retraction.approve", api::records::approve_retraction);
        add_method!(m, "com.sonoma.retraction.reject", api::records::reject_retraction);

        // Access request
        add_method!(m, "com.sonoma.access_request.approve", api::user::approve_access_request);
        add_method!(m, "com.sonoma.access_request.reject", api::user::reject_access_request);

        // Invites
        add_method!(m, "com.sonoma.invites", api::invites::query_invites);
        add_method!(m, "com.sonoma.invite", default_handler);

        // Users
        add_method!(m, "com.sonoma.users", api::users::query_users);
        add_method!(m, "com.sonoma.user", api::users::get_user);
        add_method!(m, "com.sonoma.user.create", api::users::create_system_user);
        add_method!(m, "com.sonoma.user.invite", api::users::invite_user);
        add_method!(m, "com.sonoma.user.update", api::users::update_user);
        add_method!(m, "com.sonoma.user.assignments", api::users::get_assignments);

        // Documents
        add_method!(m, "com.sonoma.documents", api::documents::query_documents);
        add_method!(m, "com.sonoma.documents.active", api::documents::get_active_documents);
        add_method!(m, "com.sonoma.documents.available", api::documents::get_available_documents);
        add_method!(m, "com.sonoma.document", api::documents::get_document);
        add_method!(m, "com.sonoma.document.create", api::documents::create_document);
        add_method!(m, "com.sonoma.document.update", api::documents::update_document);
        add_method!(m, "com.sonoma.document.delete", api::documents::delete_document);
        add_method!(m, "com.sonoma.document.export", default_handler);
        add_method!(m, "com.sonoma.document.import", default_handler);
        add_method!(m, "com.sonoma.document.data", default_handler);

        // Location Types
        add_method!(m, "com.sonoma.location_types", api::location_types::query_location_types);
        add_method!(m, "com.sonoma.location_types.active", api::location_types::get_location_types_as_options);
        add_method!(m, "com.sonoma.location_type", api::location_types::get_location_type);
        add_method!(m, "com.sonoma.location_type.geoms", api::location_types::get_type_geoms);
        add_method!(m, "com.sonoma.location_type.create", api::location_types::create_location_type);
        add_method!(m, "com.sonoma.location_type.update", api::location_types::update_location_type);
        add_method!(m, "com.sonoma.location_type.delete", api::location_types::delete_location_type);

        // Dashboards
        add_method!(m, "com.sonoma.dashboards", api::dashboards::query_dashboards);
        add_method!(m, "com.sonoma.dashboard", api::dashboards::get_dashboard);

        // Location Reporting
        add_method!(m, "com.sonoma.reporting.create", api::reporting::create_period);
        add_method!(m, "com.sonoma.reporting.update", api::reporting::update_period);
        add_method!(m, "com.sonoma.reporting.delete", api::reporting::delete_period);

        // Organizations
        add_method!(m, "com.sonoma.organizations", api::organizations::query_organizations);
        add_method!(m, "com.sonoma.organization", api::organizations::get_organization);
        add_method!(m, "com.sonoma.organization.create", api::organizations::create_organization);
        add_method!(m, "com.sonoma.organization.delete", api::organizations::delete_organization);
        add_method!(m, "com.sonoma.organization.update", api::organizations::update_organization);
        add_method!(m, "com.sonoma.organization.users", api::organizations::get_org_users);

        // Tasks
        add_method!(m, "com.sonoma.tasks", api::tasks::get_tasks);
        add_method!(m, "com.sonoma.task", api::tasks::get_task);
        add_method!(m, "com.sonoma.task.action", api::tasks::action_task);

        // import Projects
        add_method!(m, "com.sonoma.imports", api::import_projects::query_import_projects);
        add_method!(m, "com.sonoma.import_project", api::import_projects::get_import_project);
        add_method!(m, "com.sonoma.import_project.status", default_handler);
        add_method!(m, "com.sonoma.import_project.create", default_handler);
        add_method!(m, "com.sonoma.import_project.delete", default_handler);
        add_method!(m, "com.sonoma.import_project.update", default_handler);

        // import Project data
        add_method!(m, "com.sonoma.import_data", default_handler);

        // Devices
        add_method!(m, "com.sonoma.devices", api::devices::query_devices);
        add_method!(m, "com.sonoma.device", api::devices::get_device);
        add_method!(m, "com.sonoma.device.create", api::devices::create_device);
        add_method!(m, "com.sonoma.device.update", api::devices::update_device);
        add_method!(m, "com.sonoma.device.revoke", api::devices::revoke_device);
        add_method!(m, "com.sonoma.devices.import", default_handler);

        // Drafts
        add_method!(m, "com.sonoma.drafts", api::drafts::query_drafts);
        add_method!(m, "com.sonoma.draft", api::drafts::get_draft);
        add_method!(m, "com.sonoma.draft.update", api::drafts::update_draft);
        add_method!(m, "com.sonoma.draft.delete", api::drafts::delete_draft);
        add_method!(m, "com.sonoma.draft.create", api::drafts::create_draft);

        // Alerts
        add_method!(m, "com.sonoma.alerts", api::alerts::query_alerts);
        add_method!(m, "com.sonoma.alerts.map", api::alerts::query_alerts_map);
        add_method!(m, "com.sonoma.alert", api::alerts::get_alert);
        add_method!(m, "com.sonoma.alert.records", default_handler);
        add_method!(m, "com.sonoma.alert.print", default_handler);
        add_method!(m, "com.sonoma.alert.users", api::alerts::get_alert_users);
        add_method!(m, "com.sonoma.alert.action", api::alerts::action_alert);
        add_method!(m, "com.sonoma.alert.comment", api::alerts::comment);
        add_method!(m, "com.sonoma.alert.export", default_handler);
        add_method!(m, "com.sonoma.alert.related", default_handler);
        add_method!(m, "com.sonoma.alert.reopen", api::alerts::reopen_alert);
        add_method!(m, "com.sonoma.alert.reevaluate", default_handler);

        // Account
        add_method!(m, "com.sonoma.account", api::account::get_account);
        add_method!(m, "com.sonoma.conf.update", api::account::update_conf);

        // TEsts
        add_method!(m, "com.sonoma.test.email", api::tests::test_email_send);

        m
    };
}


#[derive(Debug, Deserialize, Serialize)]
pub struct ApiCommand {
    pub method: String,
    pub args: Vec<Value>,
    pub user: Option<Value>,
}

impl Message for ApiCommand {
    type Result = Result<ApiResult, io::Error>;
}

#[derive(Debug, Serialize, Deserialize, Message)]
pub struct ApiResult {
    pub success: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub data: Option<Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub code: Option<Value>,
}

impl Handler<ApiMethod> for DbExecutor {
    type Result = Result<ApiResult, ApiError>;

    fn handle(&mut self, msg: ApiMethod, _: &mut Self::Context) -> Self::Result {
        let conn: PooledConnection<PostgresConnectionManager> = self.0.get().unwrap();

        let u_cmd: String = msg.cmd.to_string();
        let s_cmd: &str = &u_cmd[..];
        let tki = msg.user.tki.clone();

        if !API_METHODS.contains_key(s_cmd) {
            return Ok(ApiResult {
                success: false,
                data: None,
                code: Some(json::to_value("UNKNOWN_COMMAND").unwrap()),
            });
        } else {
            let method = API_METHODS.get(s_cmd);

            if method.is_none() {
                return Ok(ApiResult {
                    success: false,
                    data: None,
                    code: Some(json::to_value("UNKNOWN_COMMAND").unwrap()),
                });
            }

            let method = method.unwrap();
            match method(conn, tki, &msg) {
                Ok(res) => {
                    Ok(ApiResult {
                        success: true,
                        data: Some(res),
                        code: None
                    })
                },
                Err(err) => {
                    let v_err: Value = json::to_value(format!("{:?}", err)).unwrap();
                    Ok(ApiResult {
                        success: false,
                        data: None,
                        code: Some(v_err),
                    })
                }
            }

        }
    }
}

