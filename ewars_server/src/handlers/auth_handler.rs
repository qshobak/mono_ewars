use std::io;

use postgres::rows::{Row};

use actix::prelude::*;
use failure::Error;
use jwt::{encode, Header, Algorithm};
use uuid::Uuid;

use crate::db::db::DbExecutor;
use crate::utils::auth::verify_pass;
use crate::share::common::Claims;


#[derive(Debug, Deserialize, Serialize)]
pub struct GetAuthUser {
    pub email: String,
    pub password: String,
    pub domain: String,
}

impl Message for GetAuthUser {
    type Result = Result<AuthResult, io::Error>;
}

#[derive(Debug, Message, Serialize, Deserialize, Clone)]
pub struct AuthUser {
    pub uuid: Uuid,
    pub name: String,
    pub email: String,
    #[serde(skip_serializing)]
    pub password: String,
    pub role: String,
    pub status: String,
    pub tki: String,
    pub account_name: String,
    pub aid: Option<Uuid>,
}

impl AuthUser {
    pub fn from_row(row: &Row) -> Self {
        Self {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            password: row.get(3),
            role: row.get(4),
            status: row.get(5),
            tki: "".to_string(),
            account_name: "".to_string(),
            aid: Option::None,
        }
    }
}

#[derive(Debug, Message, Serialize, Deserialize)]
pub struct AuthResult {
    pub success: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub code: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<AuthUser>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub accounts: Option<Vec<String>>,
}

#[derive(Debug, Deserialize, Clone)]
struct Account {
    uuid: Uuid,
    name: String,
    tki: String,
}

static SQL_GET_AUTH_USER: &'static str = r#"
    SELECT uuid, name, email, password, role, status
    FROM {SCHEMA}.users
    WHERE email = $1;
"#;

static SQL_GET_ACCOUNT_FROM_DOMAIN: &'static str = r#"
    SELECT uuid, name, tki
    FROM core.accounts
    WHERE domain = $1;
"#;

impl Handler<GetAuthUser> for DbExecutor {
    type Result = Result<AuthResult, io::Error>;

    fn handle(&mut self, msg: GetAuthUser, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();

        let mut end_domain: String = "".to_string();

        if msg.domain.contains(":") {
            // This domain contains a port
            let tmp_domain: String = msg.domain.split(":").collect::<Vec<&str>>()[0].to_string();

            match tmp_domain.as_ref() {
                "localhost" => {
                    end_domain = "_val.ewars.ws".to_string();
                }
                "127.0.0.1" => {
                    end_domain = "_val.ewars.ws".to_string();
                },
                _ => {
                    end_domain = tmp_domain.clone();
                }
            }
        } else {
            end_domain = msg.domain.clone();
        }

        let account: Option<Account> = match conn.query(&SQL_GET_ACCOUNT_FROM_DOMAIN, &[&end_domain]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(Account {
                        uuid: row.get(0),
                        name: row.get(1),
                        tki: row.get(2),
                    })
                } else {
                    Option::None
                }
            }
            Err(err) => {
                Option::None
            }
        };

        // Couldn't find an account with the domain that we're trying to authenticate against
        if account.is_none() {
            return Ok(AuthResult {
                success: false,
                code: Some("UNK_ACCOUNT".to_string()),
                user: None,
                token: None,
                accounts: None,
            });
        }

        let account = account.unwrap();


        let sql = SQL_GET_AUTH_USER.replace("{SCHEMA}", &account.tki);
        let result: Option<AuthUser> = match conn.query(&sql, &[&msg.email]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(AuthUser::from_row(&row))
                } else {
                    Option::None
                }
            }
            Err(err) => {
                Option::None
            }
        };

        if result.is_none() {
            return Ok(AuthResult {
                success: false,
                code: Some("NO_AUTH".to_string()),
                user: None,
                token: None,
                accounts: None,
            });
        }

        let result: AuthUser = result.unwrap();


        // Check the users status
        match result.status.as_ref() {
            "PENDING_VERIFICATION" => {
                return Ok(AuthResult {
                    success: false,
                    code: Some("EMAIL_PENDING_VERIFICATION".to_string()),
                    user: None,
                    token: None,
                    accounts: None,
                });
            }
            "PENDING" => {
                return Ok(AuthResult {
                    success: false,
                    code: Some("USER_PENDING_APPROVAL".to_string()),
                    user: None,
                    token: None,
                    accounts: None,
                });
            },
            "PENDING_APPROVAL" => {
                return Ok(AuthResult {
                    success: false,
                    code: Some("USER_PENDING_APPROVAL".to_string()),
                    user: None,
                    token: None,
                    accounts: None,
                });
            },
            _ => {}
        }


        match verify_pass(&msg.password, &result.password) {
            Ok(valid) => {
                let key = "secret";
                let claims = Claims {
                    user_id: result.uuid.to_string(),
                    role: result.role.clone(),
                    tki: account.tki.clone(),
                };
                let token = match encode(&Header::default(), &claims, key.as_ref()) {
                    Ok(t) => t,
                    Err(_) => panic!()
                };

                let mut user = result.clone();
                user.account_name = account.name.clone();
                user.tki = account.tki.clone();
                user.aid = Some(account.uuid.clone());

                Ok(AuthResult {
                    success: true,
                    code: None,
                    user: Some(user),
                    token: Some(token),
                    accounts: None,
                })
            }
            Err(_) => {
                Ok(AuthResult {
                    success: false,
                    code: Some("NO_AUTH".to_string()),
                    user: None,
                    token: None,
                    accounts: None,
                })
            }
        }
    }
}

