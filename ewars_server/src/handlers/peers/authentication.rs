use std::collections::HashMap;

use failure::Error;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use jwt::{encode, decode, Header, Algorithm, Validation};
use base64;

use postgres::rows::{Row};

use crate::utils::auth::verify_pass;
use crate::share::common::Claims;
use crate::handlers::api_handler::{PoolConnection};
use crate::handlers::peer_handler::{PeerCommandResult, PeerCommand};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AccountUser {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub email: String,
    pub name: String,
    pub password: String,
    pub role: String,
    pub org_id: Option<Uuid>,
    pub tki: String,
    pub status: String,
    pub token: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Account {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub domain: String,
    pub tki: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct AuthenticationResult {
    pub success: bool,
    pub error: Option<String>,
    pub accounts: Vec<Account>,
    pub users: Vec<AccountUser>,
}

struct TargetUser {
    uuid: Uuid,
    name: String,
    email: String,
    password: String,
    accounts: Vec<Uuid>,
    status: String,
}

struct TargetAccount {
    uuid: Uuid,
    name: String,
    domain: String,
    tki: String,
    status: String,
}

struct TargetAccountUser {
    uuid: Uuid,
    uid: Uuid,
    role: String,
    status: String,
    org_id: Option<Uuid>,
}

static SQL_GET_USER: &'static str = r#"
    SELECT uuid, name, email, password, accounts, status
    FROM core.users WHERE email = $1;
"#;

static SQL_GET_USER_BY_UID: &'static str = r#"
    SELECT uuid, email, password, accounts, status
    FROM core.users WHERE email = $1;
"#;

static SQL_GET_ACCOUNTS: &'static str = r#"
    SELECT name, domain, tki, status, uuid
    FROM core.accounts WHERE uuid = ANY($1);
"#;

static SQL_GET_ACCOUNT_USER: &'static str = r#"
    SELECT ruuid, uuid, role, status, org_id
    FROM {SCHEMA}.users
    WHERE uuid = $1;
"#;

pub fn authenticate_user(conn: &PoolConnection, email: &String, password: &String) -> Result<PeerCommandResult, Error> {
    let user: Option<TargetUser> = match conn.query(&SQL_GET_USER, &[&email]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(TargetUser {
                    uuid: row.get("uuid"),
                    name: row.get("name"),
                    email: row.get("email"),
                    password: row.get("password"),
                    accounts: row.get("accounts"),
                    status: row.get("status"),
                })
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if user.is_none() {
        return Ok(PeerCommandResult::AuthenticationResult {
            success: false,
            error: Some("UNK_USER".to_string()),
            users: Vec::new(),
            accounts: Vec::new(),
        });
    }
    let user: TargetUser = user.unwrap();

    // authenticate
    match verify_pass(&password, &user.password) {
        Ok(valid) => {},
        Err(_) => {
            return Ok(PeerCommandResult::AuthenticationResult {
                success: false,
                error: Some("AUTH_ERROR".to_string()),
                users: Vec::new(),
                accounts: Vec::new(),
            });
        }
    }

    // User is pending verification
    if &user.status == "PENDING_VERIFICATION" {
        return Ok(PeerCommandResult::AuthenticationResult {
            success: false,
            error: Some("USER_PENDING_VERIFICATION".to_string()),
            users: Vec::new(),
            accounts: Vec::new()
        });
    }

    // User is inactive
    if &user.status != "ACTIVE" {
        return Ok(PeerCommandResult::AuthenticationResult {
            success: false,
            error: Some("USER_INACTIVE".to_string()),
            users: Vec::new(),
            accounts: Vec::new(),
        });
    }

    if user.accounts.len() <= 0 {
        return Ok(PeerCommandResult::AuthenticationResult {
            success: false,
            error: Some("NO_ACCOUNTS".to_string()),
            users: Vec::new(),
            accounts: Vec::new()
        });
    }

    // We can now pull tenancies
    let accounts: Vec<TargetAccount> = match &conn.query(&SQL_GET_ACCOUNTS, &[&user.accounts]) {
        Ok(rows) => rows.iter().map(|row| {
            TargetAccount {
                uuid: row.get("uuid"),
                name: row.get("name"),
                domain: row.get("domain"),
                tki: row.get("tki"),
                status: row.get("status"),
            }
        }).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            return Ok(PeerCommandResult::AuthenticationResult {
                success: false,
                error: Some("UNK_ERROR".to_string()),
                users: Vec::new(),
                accounts: Vec::new(),
            });
        }
    };

    let mut tenancies: Vec<TargetAccountUser> = Vec::new();
    let claim_key = "secret";

    let mut end_accounts: Vec<Account> = Vec::new();
    let mut users: Vec<AccountUser> = Vec::new();

    // Get the user accounts
    for account in &accounts {

        let acc_user: Option<TargetAccountUser> = match &conn.query(&SQL_GET_ACCOUNT_USER.replace("{SCHEMA}", &account.tki), &[&user.uuid]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(TargetAccountUser {
                        uuid: row.get("ruuid"),
                        uid: row.get("uuid"),
                        role: row.get("role"),
                        status: row.get("status"),
                        org_id: row.get("org_id"),
                    })
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        if let Some(c) = acc_user {
            end_accounts.push(Account {
                uuid: account.uuid.clone(),
                name: account.name.clone(),
                status: account.status.clone(),
                domain: account.domain.clone(),
                tki: account.tki.clone(),
            });

            if &c.status == "ACTIVE" {
                let claims = Claims {
                    user_id: c.uid.clone().to_string(),
                    role: c.role.clone().to_string(),
                    tki: account.tki.clone(),
                };
                let token = match encode(&Header::default(), &claims, claim_key.as_ref()) {
                    Ok(r) => r,
                    Err(_) => panic!(),
                };

                users.push(AccountUser {
                    uuid: c.uuid.clone(),
                    uid: c.uid.clone(),
                    email: user.email.clone(),
                    name: user.name.clone(),
                    password: user.password.clone(),
                    role: c.role.clone(),
                    org_id: c.org_id.clone(),
                    tki: account.tki.clone(),
                    status: c.status.clone(),
                    token: token,
                });
            }
        }
    }

    Ok(PeerCommandResult::AuthenticationResult {
        success: true,
        error: Option::None,
        users: users,
        accounts: end_accounts,
    })
}

