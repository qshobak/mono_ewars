use std::collections::VecDeque;
use std::time::{Duration, Instant};
use std::fs;

use actix::prelude::*;
use crate::settings::Settings;
use postgres::{Connection, TlsMode};

use serde_json as json;

use uuid::Uuid;

use crate::db::db::DbExecutor;

static CACHE_LOCATION: &'static str = r#""#;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct AlarmContext {
    pub record_id: Option<Uuid>,
    pub version: i32,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
    pub locations: Option<Vec<Uuid>>,
    pub tki: String,
}

pub struct AlarmService {
    pub db: Connection,
    pub queue: VecDeque<AlarmContext>,
    pub hb: Instant,
    pub processing: bool
}

impl AlarmService {
    pub fn new() -> Self {
        let settings = Settings::new().unwrap();
        let conn = Connection::connect(settings.database.db_url, TlsMode::None).unwrap();
        Self {
            db: conn,
            queue: VecDeque::new(),
            hb: Instant::now(),
            processing: false
        }
    }

    pub fn process_context(&self, al_ctx: &AlarmContext) -> Result<(), ()> {
        Ok(())
    }


    fn hb(&mut self, ctx: &mut Context<Self>) {
        ctx.run_later(Duration::new(10, 0), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                if !act.processing {
                    // Drain the swamp
                    act.processing = true;
    
                    // We only process alarm contexts one at a atime
                    // as it's pretty intensive
                    if act.queue.len() > 0 {
                        let a_context: AlarmContext = act.queue.pop_front().unwrap();

                        match act.process_context(&a_context) {
                            Ok(_) => {

                            },
                            Err(err) => {
                                eprintln!("{:?}", err);
                                // Add the context back onto the queue
                                act.queue.push_back(a_context);
                            }
                        };
                    }

                    act.hb = Instant::now();
                    act.processing = false;
                }
            }
            act.hb(ctx);
        });
    }

}

impl Actor for AlarmService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        eprintln!("Alarm Service: Started");
        self.hb(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        println!("Alarm Service: Shutting down...");
        // TODO flush cache of VecDequeu to disk
        Running::Stop
    }
}

impl Supervised for AlarmService {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Alarm Service: Restarting...");
    }
}

impl Drop for AlarmService {
    fn drop(&mut self) {
        // Write the queued items to disk and pick them back up when restarting the application
        if self.queue.len() > 0 {
            let data: Vec<AlarmContext> = self.queue.drain(0..).collect::<Vec<AlarmContext>>();
            let json_str: String = json::to_string(&data).unwrap();
            fs::write("./.cache/alarms.json", json_str).expect("Could not write cache file");
        }
    }
}

impl Message for AlarmContext {
    type Result = Result<(), ()>;
}

impl Handler<AlarmContext> for AlarmService {
    type Result = Result<(), ()>;

    fn handle(&mut self, msg: AlarmContext, ctx: &mut Context<Self>) -> Self::Result {
        self.queue.push_back(msg.clone());

        Ok(())
    }
}

