use actix::prelude::*;

use crate::db::db::DbExecutor;

pub struct SMSService {

}

impl SMSService {
    pub fn new(conn: Addr<DbExecutor>) -> Self {
        Self {
            db: conn
        }
    }

    pub fn default_threads(threads: usize) -> Self {
        let db: Addr<_> = DbExecutor::start_threaded(threads);
        Self {
            db
        }
    }
}

impl Actor for SMSService {
    type Context = Context<Self>;
}



