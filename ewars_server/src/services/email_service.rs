use std::collections::VecDeque;
use std::time::{Duration, Instant};
use std::fs;

use serde_json as json;

use actix::prelude::*;

use crate::db::db::DbExecutor;
use rusoto_ses::{SesClient, Ses, SendEmailRequest, Body, Content, Message, Destination};
use rusoto_core::{Region, DefaultCredentialsProvider};


static SQL_CACHE_LOCATION: &'static str = "./email_queue.toml";

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Email {
    pub recipients: Vec<String>,
    pub account_name: Option<String>,
    pub subject: String,
    pub plain_content: Option<String>,
    pub html_content: String,
    pub system: bool,
}

impl actix::Message for Email {
    type Result = Result<(), ()>;
}

pub struct EmailService {
    ses_client: SesClient,
    queue: VecDeque<Email>,
    hb: Instant,
    creds: DefaultCredentialsProvider,
    processing: bool
}

impl EmailService {
    pub fn new() -> Self {
        Self {
            ses_client: SesClient::new(Region::EuWest1),
            queue: VecDeque::new(),
            hb: Instant::now(),
            creds: DefaultCredentialsProvider::new().unwrap(),
            processing: false
        }
    }

    pub fn default_threads(threads: usize) -> Self {
        Self {
            ses_client: SesClient::new(Region::EuWest1),
            queue: VecDeque::new(),
            hb: Instant::now(),
            creds: DefaultCredentialsProvider::new().unwrap(),
            processing: false
        }
    }

    pub fn send_email(&self, email: &Email) -> Result<(), ()> {
        let emails: Vec<String> = vec!["jduren@protonmail.com".to_string()];
        let destination = Destination {
            bcc_addresses: Option::None,
            //to_addresses: Some(email.recipients.clone()),
            to_addresses: Some(emails),
            cc_addresses: Option::None,
        };

        let html_body = Content {
            charset: Option::None,
            data: email.html_content.clone(),
        };

        let message = Message {
            body: Body {
                html: Some(html_body),
                text: match email.plain_content {
                    Some(ref res) => {
                        Some(Content {
                            charset: Option::None,
                            data: res.clone()
                        })
                    },
                    None => Option::None
                }
            },
            subject: Content {
                charset: Option::None,
                data: email.subject.clone(),
            }
        };

        let reply_to: Vec<String> = vec!["support@ewars.ws".to_string()];

        let email = SendEmailRequest {
            configuration_set_name: Option::None,
            destination,
            message,
            reply_to_addresses: Some(reply_to),
            return_path: Option::None,
            return_path_arn: Option::None,
            source: "support@ewars.ws".to_string(),
            source_arn: Option::None,
            tags: Option::None,
        };

        match self.ses_client.send_email(email).sync() {
            Ok(res) => {
                eprintln!("{:?}", res);
                eprintln!("SENT");
            },
            Err(err) => {
                eprintln!("{:?}", err);
            }
        };


        Ok(())
    }

    fn hb(&mut self, ctx: &mut Context<Self>) {
        println!("Heartbeat Run");
        ctx.run_later(Duration::new(10, 0), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                if !act.processing {
                    act.processing = true;
                    eprintln!("Heartbeat called");
                    let mut total = act.queue.len();
                    if total > 10 {
                        total = 10
                    }
                    let drained = act.queue.drain(0..total).collect::<VecDeque<_>>();

                    for item in drained.iter() {
                        act.send_email(item);
                    }
                    act.hb = Instant::now();
                    act.processing = false;
                }
            }
            act.hb(ctx);
        });
    }

    // Queue up an email intot he queue
    pub fn queue_email(&mut self, email: Email) -> Result<(), ()> {
        Ok(())
    }
}

impl Actor for EmailService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        eprintln!("Email Service: Started");
        // TODO check the cache file, read it in, delete it
        // let data = fs::read_to_string("/etc/hosts").expect("Unable to read file");
        self.hb(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        eprintln!("Email Service: Shutting down...");
        Running::Stop
    }
}

impl Supervised for EmailService {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Email Services: Restarting...");
    }
}

impl Drop for EmailService {
    fn drop(&mut self) {
        // Dump the queued emails to disk, pick them back up once the application restarts
        if self.queue.len() > 0 {
            let data: Vec<Email> = self.queue.drain(0..).collect::<Vec<Email>>();
            let json_str: String = json::to_string(&data).unwrap();
            fs::write("./.cache/emails.json", json_str).expect("Could not write cache file");
        }
    }
}

impl Handler<Email> for EmailService {
    type Result = Result<(), ()>;

    fn handle(&mut self, msg: Email, ctx: &mut Context<Self>) -> Self::Result {
        eprintln!("{:?}", msg);
        self.queue.push_back(msg.clone());
        Ok(())
    }
}
