/**
 * Handle denormalizing record data into an in-memory cache
 */

use std::collections::VecDeque;
use std::time::{Duration};

use serde_json as json;
use serde_json::Value;
use actix::prelude::*;

pub struct DenormService {
    queue: VecDequeue<Record>,
    hb: Instant,
}

impl Actor for DenormService {

}

impl Supervised for DenormService {

}

impl Drop for DenormService {

}
