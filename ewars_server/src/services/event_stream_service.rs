use std::time::{Duration};
use std::collections::{VecDeque};
use std::fs;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use actix::prelude::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum EventType {
    Created,
    Updated,
    Deleted,
    Rollback,
    Rebuild,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct InboundMessage {
    pub did: Uuid,
    pub ts_ms: i32,
    pub event_type: EventType,
    pub resource_type: ResourceType,
    pub resource_id: Uuid,
    pub payload: Option<Value>,
    pub processed: bool,
}

pub struct EventStreamService {
    queue: VecDeque<InboundMessage>,
    hb: Instant,
}

impl Actor for EventStreamService {

}

impl Supervised for EventStreamService {

}

impl Drop for EventStreamService {

}

impl Handler<InboundMessage> for EventStreamService {

}
