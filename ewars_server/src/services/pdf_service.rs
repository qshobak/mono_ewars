use std::collections::VecDeque;
use std::time::{Instant, Duration};
use std::fs;

use serde_json as json;
use serde_json::Value;

use actix::prelude::*;

#[derive(Debug, Clone)]
pub struct PDFJob {
    pub tid: Uuid,
    pub report_date: String,
    pub lid: Uuid,
    pub tki: String,
}

pub struct PDFService {
    queue: VecDeque<PDFJob>,
    hb: Instant,
}


impl Actor for PDFService {

}

impl Supervised for PDFService {

}


impl Drop for PDFService {

}

impl Handler<PDFJob> for PDFService {

}
