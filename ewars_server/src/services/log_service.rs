use actix::prelude::*;

use crate::db::db::DbExecutor;

pub struct LogCompactionService {

}

impl LogCompactionService {
    pub fn new(conn: Addr<DbExecutor>) -> Self {
        Self {
            db: conn
        }
    }

    pub fn default_threads(threads: usize) -> Self {
        let db: Addr<_> = DbExecutor::start_threaded(threads);
        Self {
            db
        }
    }
}

impl Actor for LogCompactionService {
    type Context = Context<Self>;
}


