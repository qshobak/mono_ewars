use serde_json as json;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthenticationRequest {
    pub email: String,
    pub password: String,
}

// Authenticate a users password
pub fn authenticate_password(plaintext: &String, enrypted: &String) -> Result<bool, Result::Error> {
    Ok(true)
}

pub fn encrypt_password(plaintext: &String) -> Result<String, Result::Error> {
    Ok("".to_string())
}

