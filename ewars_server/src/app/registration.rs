use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use actix::prelude::*;
use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;
use base64::{encode};

use tera;
use tera::{Tera};

use crate::api::forms::{Field, FormFeature};
#[macro_use]
use crate::api::utils::*;

use crate::handlers::api_handler::{PoolConnection};
use crate::handlers::command_handler::{CommandResult};
use crate::services::{EmailService, Email};

use crate::models::{Account, CoreUser};

use crate::utils::auth::*;


lazy_static! {
    pub static ref TERA: Tera = {
        let tera = compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*"));
        tera
    };
}

static SQL_GET_EXISTING_USER: &'static str = r#"
    SELECT uuid, status, accounts, status
    FROM core.users
    WHERE email = $1;
"#;

static SQL_GET_ACCOUNT: &'static str = r#"
    SELECT uuid, name, tki, status
    FROM core.accounts
    WHERE domain = $1;
"#;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Org {
    Str(String),
    Id(Uuid),
}

#[derive(Clone)]
pub struct RegistrationRequest {
    pub email: String,
    pub name: String,
    pub password: String,
    pub org_id: Option<Org>,
    pub org_name: Option<String>,
    pub domain: String,
    pub email_svc: Addr<EmailService>,
}

// Attempt to register
pub fn attempt_registration(conn: &PoolConnection, data: &RegistrationRequest) -> CommandResult {
    // Check that a user doesn;t already ex

    let account: Account = match Account::from_domain(&conn, &data.domain) {
        Some(res) => res,
        None => {
            return CommandResult::RegistrationResult(false, "UNK_ACCOUNT".to_string());
        }
    };

    let user: Option<CoreUser> = match CoreUser::from_email(&conn, &data.email) {
        Some(res) => Some(res),
        None => {
            Option::None
        }
    };

    if user.is_some() {
        // There's a user already in the system with this email
        // Check the user isn't pending verificaiont
        let user: CoreUser = user.unwrap();

        let status: String = user.status.clone();

        match status.as_ref() {
            "PENDING_VERIFICATION" => {
                return CommandResult::RegistrationResult(false, "PENDING_EMAIL_VERIFICATION".to_string());
            },
            "ACTIVE" => {
                return CommandResult::RegistrationResult(false, "ERR_EXISTING_USER".to_string());
            },
            _ => {
                return CommandResult::RegistrationResult(false, "ERR_UNKNOWN".to_string());
            }
        }

        return CommandResult::RegistrationResult(false, "ERR_UNKNOWN".to_string());
    } else {
        // Create core user
        // send email verification, verification code contains account specific information
        // (org_id, account_id) for triggering creation of the task
    
        let new_user: Option<CoreUser> = match CoreUser::create(&conn, &data.email, &data.name, &data.password) {
            Some(res) => Some(res),
            None => {
                Option::None
            }
        };

        if new_user.is_none() {
            return CommandResult::RegistrationResult(false, "UNK_ERR".to_string());
        }
        let new_user: CoreUser = new_user.unwrap();

        // Build the verification URL
        let org_id: String = match &data.org_id {
            Some(res) => {
                match res {
                    Org::Str(c) => c.clone(),
                    Org::Id(c) => c.to_string(),
                    _ => "".to_string()
                }
            }
            None => "".to_string()
        };
        let org_name: String = match &data.org_name {
            Some(res) => res.clone(),
            None => "".to_string()
        };
        let mut params: String = format!("u:{};a:{};o:{};on:{};", 
                                         new_user.uuid.to_string(),
                                         account.tki.to_string(),
                                         org_id,
                                         org_name);
        let code: String = encode(&params);

        // send email

        let mut ctx = tera::Context::new();
        ctx.insert("domain", &data.domain);
        ctx.insert("code", &code);
        ctx.insert("subject", &"Please verify your email address");

        let mut email_content: Option<String> = Option::None;
        match TERA.render("emails/verification.html", &ctx) {
            Ok(res) => {
                email_content = Some(res);
            },
            Err(err) => {
                eprintln!("{:?}", err);
            }
        }

        if let Some(c) = email_content {
            data.email_svc.do_send(Email {
                recipients: vec![data.email.clone()],
                account_name: Some(account.name.clone()),
                subject: format!("[EWARS] Please verify your email address - {}", account.name.clone()),
                plain_content: Some("Please verify your email address".to_string()),
                html_content: c.clone(),
                system: true,
            });
        }

        return CommandResult::RegistrationResult(true, "".to_string());
    }
}

