use std::fmt;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use actix::prelude::*;

use tera;
use tera::{Tera};
use postgres::rows::{Row};

use uuid::Uuid;

use crate::services::{EmailService, Email};
use crate::handlers::command_handler::{DbCommand, CommandResult};
use crate::handlers::api_handler::{PoolConnection};

use crate::models::{Account, CoreUser};

lazy_static! {
    pub static ref TERA: Tera = {
        let mut tera = compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/emails/**/*"));
        tera
    };
}

#[derive(Clone)]
pub struct AccessRequest {
    pub domain: String,
    pub email: String,
    pub password: String,
    pub org_id: Option<Uuid>,
    pub org_name: Option<String>,
    pub email_svc: Addr<EmailService>,
}

impl fmt::Debug for AccessRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("AccessRequest")
            .field("domain", &self.domain)
            .field("email", &self.email)
            .finish()
    }
}


pub fn attempt_access_request(conn: &PoolConnection, data: &AccessRequest) -> CommandResult {
    // Check if the user already has acces
    // Check if the user is pending verification
    // Check if the user has a pending access request for this account

    let user: CoreUser = match CoreUser::from_email(&conn, &data.email) {
        Some(res) => res,
        None => {
            return CommandResult::AccessResult(false, "UNK_USER".to_string());
        }
    };

    let account: Account = match Account::from_domain(&conn, &data.domain) {
        Some(res) => res,
        None => {
            return CommandResult::AccessResult(false, "UNK_ACCOUNT".to_string());
        }
    };


    if &user.status == "PENDING_VERIFICATION" {
        return CommandResult::AccessResult(false, "USER_PENDING_VERIFICATION".to_string());
    }

    // TODO: Authenticate user

    let mut accounts: Vec<Uuid> = Vec::new();
    if let Some(accs) = &user.accounts {
        accounts = accs.clone();
    }
    if accounts.contains(&account.uuid) {
        // The user has this account listed in its accounts already
        // Check that the status of that account user
        let account_user: Option<AccountUser> = get_account_user(&conn, &user.uuid, &account.tki);

        if let Some(r) = account_user {
            let u_status: String = r.status.clone();

            match u_status.as_ref() {
                "ACTIVE" => {
                    return CommandResult::AccessResult(false, "ERR_EXISTING".to_string());
                },
                "REVOKED" => {
                    return CommandResult::AccessResult(false, "ERR_ACCESS_REVOKED".to_string());
                },
                _ => {
                    return CommandResult::AccessResult(false, "ERR_UNKNOWN".to_string());
                }
            }
        }

        return CommandResult::AccessResult(false, "UNK_ERROR".to_string());

    } else {
        // Check if there's a pending task to register this user already
        let task: Option<Task> = get_account_task(&conn, &user.uuid, &account.tki);

        if let Some(t) = task {

            let status: String = t.status.clone();

            match status.as_ref() {
                "OPEN" => {
                    return CommandResult::AccessResult(false, "ERR_PENDING_REQUEST".to_string());
                },
                "CLOSED" => {
                    return CommandResult::AccessResult(false, "ACCESS_REJECTED".to_string());
                },
                _ => {
                    return CommandResult::AccessResult(false, "UNK_ERROR".to_string());
                }
            }

            return CommandResult::AccessResult(false, "UNK_ERROR".to_string());
        } else {
            // Create the task
            let result: Option<Uuid> = create_task(&conn, &account.tki, &user, &data);

            if let Some(res) = result {
                // Send task email
                let mut ctx = tera::Context::new();
                ctx.insert("name", &user.name);
                ctx.insert("email", &user.email);
                ctx.insert("account_name", &account.name);
                ctx.insert("domain", &data.domain);
                ctx.insert("task_id", &res);

                let s: String = TERA.render("access_request.html", &ctx)
                    .map_err(|err| {
                        eprintln!("{:?}", err);
                    }).unwrap();

                let admins: Vec<(String, String)> = get_account_admins(&conn, &account.tki);
                let recps: Vec<String> = admins.iter().map(|x| x.0.clone()).clone().collect();

                &data.email_svc.do_send(Email {
                    recipients: recps,
                    account_name: Some(account.name.clone()),
                    subject: format!("[EWARS] New Task - User Access Request [{}]", &account.name),
                    plain_content: Option::None,
                    html_content: s,
                    system: false,
                });


                return CommandResult::AccessResult(true, "".to_string());
                
            } else {
                // An error occurred creating the task
                return CommandResult::AccessResult(false, "UNK_ERR".to_string());
            }
        }
    }
}

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO {SCHEMA}.tasks
    (status, task_type, data, context, roles)
    VALUES ('OPEN', 'ACCESS_REQUEST', $1, $2, $3) RETURNING uuid;
"#;

#[derive(Debug, Serialize)]
struct TaskData {
    uid: Uuid,
    email: String,
    org_id: Option<Uuid>,
    org_name: Option<String>,
    name: String,
}

fn create_task(conn: &PoolConnection, tki: &String, user: &CoreUser, data: &AccessRequest) -> Option<Uuid> {
    let t_data = TaskData {
        uid: user.uuid.clone(),
        email: user.email.clone(),
        org_id: data.org_id.clone(),
        org_name: data.org_name.clone(),
        name: user.name.clone(),
    };
    let t_data_val: Value = json::to_value(t_data).unwrap();

    let t_context: HashMap<String, String> = HashMap::new();
    let t_context_val: Value = json::to_value(t_context).unwrap();

    let roles: Vec<String> = vec!["ACCOUNT_ADMIN".to_string()];

    match &conn.query(&SQL_CREATE_TASK.replace("{SCHEMA}", &tki), &[
        &t_data_val,
        &t_context_val,
        &roles,
    ]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(c.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    }
}

#[derive(Debug, Clone)]
struct AccountUser {
    uuid: Uuid,
    status: String,
    role: String,
}

impl<'a> From<Row<'a>> for AccountUser {
    fn from(row: Row) -> Self {
        AccountUser {
            uuid: row.get(0),
            status: row.get(1),
            role: row.get(2),
        }
    }
}

static SQL_GET_ACCOUNT_USER: &'static str = r#"
    SELECT uuid, status, role
    FROM {SCHEMA}.users 
    WHERE uuid = $1;
"#;

// Get the account user if it looks like ther is one
fn get_account_user(conn: &PoolConnection, uid: &Uuid, tki: &String) -> Option<AccountUser> {
    match &conn.query(&SQL_GET_ACCOUNT_USER.replace("{SCHEMA}", &tki), &[&uid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(AccountUser::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    }
}

static SQL_GET_TASK: &'static str = r#"
    SELECT uuid, status, outcome
    FROM {SCHEMA}.tasks
    WHERE data->>'uid' = $1::TEXT;
"#;

#[derive(Debug, Clone)]
pub struct Task {
    pub uuid: Uuid,
    pub status: String,
    pub outcome: Option<HashMap<String, String>>,
}

impl<'a> From<Row<'a>> for Task {
    fn from(row: Row) -> Self {
        let outcome: Option<HashMap<String, String>> = json::from_value(row.get(2)).unwrap();
        Task {
            uuid: row.get(0),
            status: row.get(1),
            outcome,
        }
    }
}

// check for a task in the account for approving access request
fn get_account_task(conn: &PoolConnection, uid: &Uuid, tki: &String) -> Option<Task> {
    match &conn.query(&SQL_GET_TASK.replace("{SCHEMA}", &tki), &[&uid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Task::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    }
}

static SQL_GET_ACC_ADMINS: &'static str = r#"
    SELECT email, name
    FROM {SCHEMA}.users
    WHERE status = 'ACTIVE' AND role = 'ACCOUNT_ADMIN';
"#;

// Get admins for the account
fn get_account_admins(conn: &PoolConnection, tki: &String) -> Vec<(String, String)> {
    match &conn.query(&SQL_GET_ACC_ADMINS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| (x.get(0), x.get(1))).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    }
}
