use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct AlarmDetails {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub definition: Option<Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alarm_description: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
pub struct LocationGeometry {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub loc_geom: Option<Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub loc_geom_type: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct AlertEvent {
    pub uuid: Uuid,
    pub uid: Option<Uuid>,
    pub event_type: String,
    pub transition: Option<Vec<String>>,
    pub reason: Option<String>,
    pub created: DateTime<Utc>,
    pub comment: Option<String>,
}


#[derive(Debug, Serialize, Clone)]
pub struct Alert {
    pub uuid: Uuid,
    pub alid: Uuid,
    pub status: String,
    pub eid: Option<String>,
    pub alert_date: Option<NaiveDate>,
    pub lid: Option<Uuid>,
    pub stage: String,
    pub data: Option<Value>,
    pub workflow: Option<Value>,
    pub events: Option<Vec<AlertEvent>>,
    pub raised: DateTime<Utc>,
    pub closed: Option<DateTime<Utc>>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub alarm_name: Option<String>,
    pub location_name: Option<Value>,
    pub location_full_name: Option<Vec<String>>,
    pub lti_name: Option<Value>,
    pub modifier_name: Option<String>,
    pub modifier_email: Option<String>,
    #[serde(flatten)]
    pub alarm_details: AlarmDetails,
    #[serde(flatten)]
    pub location_geom: LocationGeometry,
}

impl<'a> From<Row<'a>> for Alert {
    fn from(row: Row) -> Self {
        let events: Vec<AlertEvent> = json::from_value(row.get("events")).unwrap();
        Alert {
            uuid: row.get("uuid"),
            alid: row.get("alid"),
            status: row.get("status"),
            eid: row.get("eid"),
            alert_date: row.get("alert_date"),
            lid: row.get("lid"),
            stage: row.get("stage"),
            data: row.get("data"),
            workflow: row.get("workflow"),
            events: Some(events),
            raised: row.get("raised"),
            closed: row.get("closed"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            alarm_name: match row.get_opt("alarm_name") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            location_name: row.get("location_name"),
            location_full_name: row.get("location_full_name"),
            lti_name: row.get("lti_name"),
            modifier_name: row.get("modifier_name"),
            modifier_email: row.get("modifier_email"),
            alarm_details: AlarmDetails {
                definition: match row.get_opt("alarm_definition") {
                    Some(res) => res.unwrap(),
                    None => Option::None,
                },
                alarm_description: match row.get_opt("alarm_description") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                }
            },
            location_geom: LocationGeometry {
                loc_geom: match row.get_opt("loc_geom") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                },
                loc_geom_type: match row.get_opt("loc_geom_type") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                }
            }
        }
    }
}


