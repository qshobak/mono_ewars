use serde_json as json;
use serde_json::Value;

use failure::Error;
use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::alerts::{get_alert_int, Alert};

use crate::api::utils::*;


#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct AlertUser {
    pub uuid: Uuid,
    pub name: String,
    pub email: String,
    pub role: String,
}

impl<'a> From<Row<'a>> for AlertUser {
    fn from(row: Row) -> Self {
        AlertUser {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            role: row.get(3),
        }
    }
}

static SQL_GET_USERS: &'static str = r#"
    SELECT u.uuid, u.name, u.email, u.role
    FROM {SCHEMA}.assignments AS a
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid= a.uid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = a.lid
    WHERE (
        l.uuid = $1 
        OR (l.groups::TEXT[] @> ARRAY[a.assign_group]::TEXT[] AND a.assign_type = 'GROUP')
        OR (a.assign_type = 'UNDER' AND a.lid = ANY($2::UUID[]))
    ) AND u.status = 'ACTIVE';

"#;

static SQL_GET_REGIONAL_ADMINS: &'static str = r#"
    SELECT u.uuid, u.name, u.email, u.role
    FROM {SCHEMA}.assignments AS a
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid = a.uid
"#;

static SQL_GET_ACCOUNT_ADMINS: &'static str = r#"
    SELECT u.uuid, u.name, u.email, u.role
    FROM {SCHEMA}.users AS u
    WHERE u.role = 'ACCOUNT_ADMIN' AND u.status = 'ACTIVE';
"#;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name, lineage
    FROM {SCHEMA}.locations
    WHERE uuid = $1;
"#;

static SQL_GET_USERS_BASE: &'static str = r#"
    SELECT u.uuid, u.name, u.email, u.role
    FROM {SCHEMA}.assignments AS a
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid= a.uid
    WHERE a.fid = $1 AND u.status = 'ACTIVE';
"#;


pub fn get_alert_users(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let alert: Alert = match get_alert_int(&conn, &tki, &id) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            bail!("ERR_NO_ALERT");
        }
    };

    eprintln!("{:?}", alert.lid);
    if alert.lid.is_none() {
        bail!("ERR_NO_LOCAITON");
    }

    let location_id: Uuid = alert.lid.clone().unwrap();

    let location: (Uuid, Value, Vec<Uuid>) = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&location_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                (row.get(0), row.get(1), row.get(2))
            } else {
                bail!("ERR_NO_LOCATION");
            }
        }, 
        Err(err) => {
            eprintln!("{:?}", err);
            bail!("ERR_NO_LOCATION");
        }
    };


    let mut users: Vec<AlertUser> = Vec::new();
    if alert.lid.is_some() {
        users = get_vec_from_args!(conn, SQL_GET_USERS, AlertUser, tki, &[
           &location_id,
           &location.2,
        ]);
    } 
    // TODO: get users with permissions for the form that triggered this alert
    let regional_admins: Vec<AlertUser> = get_vec_from_args!(conn, SQL_GET_REGIONAL_ADMINS, AlertUser, tki, &[]);
    let account_admins: Vec<AlertUser>= get_vec_from_args!(conn, SQL_GET_ACCOUNT_ADMINS, AlertUser, tki, &[]);
    
    let mut all_users: Vec<AlertUser> = Vec::new();
    all_users.extend(users.iter().cloned());
    all_users.extend(regional_admins.iter().cloned());
    all_users.extend(account_admins.iter().cloned());

    all_users.sort_by_key(|x| x.uuid);
    all_users.dedup_by_key(|x| x.uuid);

    Ok(json::to_value(all_users).unwrap())
}
