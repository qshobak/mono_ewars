use uuid::Uuid;

use failure::Error;
use crate::api::alerts::{Alert};

use crate::handlers::api_handler::{PoolConnection};

static SQL_GET_ALERT: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
                          (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email,
        al.definition as alarm_definition,
        al.description AS alarm_description
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn get_alert_int(conn: &PoolConnection, tki: &String, aid: &Uuid) -> Result<Alert, Error> {
    match conn.query(&SQL_GET_ALERT.replace("{SCHEMA}", &tki), &[&aid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(Alert::from(row))
            } else {
                bail!("ERR_NOT_FOUND");
            }
        },
        Err(err) => {
            bail!(err);
        }
    }
}
