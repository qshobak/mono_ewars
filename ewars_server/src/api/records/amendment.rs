use std::collections::HashMap;

use failure::Error;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::models::{Task, Record, Form, RecordRevision, Field, Location};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::services::{EventBuilder, Job};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordAmendment {
    pub uuid: Uuid,
    pub amendments: HashMap<String, Value>,
    pub reason: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TaskData {
    pub record_id: Uuid,
    pub form_name: Value,
    pub original_data: HashMap<String, Value>,
    pub amendments: HashMap<String, Value>,
    pub reason: String,
    pub creator: String,
    pub creator_id: Uuid,
    pub definition: HashMap<String, Field>,
    pub report_date: Option<String>,
    pub location_name: Option<(String, String)>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct TaskContext {
    roles: Vec<String>,
    lid: Option<Uuid>,
}

static SQL_UPDATE_RECORD: &'static str = r#"
    UPDATE {SCHEMA}.records
    SET data = $1,
        revisions = $2,
        modified = NOW(),
        modified_by = $3
    WHERE uuid = $4;
"#;

static SQL_UPDATE_RECORD_STATUS: &'static str = r#"
    UPDATE {SCHEMA}.records
    SET status = 'PENDING_AMENDMENT'
    WHERE uuid = $1;
"#;


pub fn amend_record(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    // Block if the record already has an amendment pending
    let data: RecordAmendment = json::from_value(msg.args.clone()).unwrap();

    let context: Option<Record> = Record::get_record_by_id(&conn, &tki, &data.uuid);
    if context.is_none() {
        bail!("ERR_NO_RECORD");
    }

    let context: Record = context.unwrap();

    let form: Option<Form> = match Form::get_form_by_id(&conn, &tki, &context.fid) {
        Ok(res) => Some(res),
        Err(_) => Option::None
    };
    if form.is_none() {
        bail!("ERR_NO_FORM");
    }
    let form: Form = form.unwrap();

    let role: String = msg.user.role.clone();
    if &role == &"USER".to_string() {
        // Update the records status to pending amendment, blocking further amendments until this
        // one is approved
        let _ = &conn.execute(&SQL_UPDATE_RECORD_STATUS.replace("{SCHEMA}", &tki), &[&data.uuid]).unwrap();

        let mut lid: Option<Uuid> = Option::None;
        let mut location_name: Option<(String, String)> = Option::None;
        if let Some(c) = context.data.get("__lid__") {
            lid = Some(json::from_value(c.clone()).unwrap());
            if let Some(l) = lid {
                location_name = match Location::get_location_name(&conn, &tki, &l) {
                    Ok(res) => Some(res),
                    Err(_) => Option::None
                };
            }
        }

        // Get the record date as a formatted string
        let dd: Date<Utc> = context.get_record_date().unwrap();
        let formatted_date: String = form.format_date(&dd);

        let task_data: Value = json::to_value(TaskData {
            record_id: data.uuid.clone(),
            form_name: form.name.clone(),
            original_data: context.data.clone(),
            amendments: data.amendments.clone(),
            reason: data.reason.clone(),
            creator: format!("{} ({})", msg.user.name, msg.user.email),
            creator_id: msg.user.uuid.clone(),
            report_date: Some(formatted_date.clone()),
            location_name: location_name,
            definition: form.definition.clone(),
        }).unwrap();

        let task_context: Value = json::to_value(TaskContext {
            roles: vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
            lid: lid,
        }).unwrap();

        let roles: Vec<String> = vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()];
        let result: bool = match Task::create_task(&conn, &tki, &task_context, &task_data, &"AMENDMENT_REQUEST") {
            Ok(_) => true,
            Err(err) => false
        };

        if result {
            let record: Option<Record> = Record::get_record_by_id(&conn, &tki, &data.uuid);
            Ok(json::to_value(record).unwrap())
        } else {
            bail!("ERR_CREATING_AMEND_TASK");
        }

    } else {
        // AMendment can just be applied to the data
        let mut new_data: HashMap<String, Value> = context.data.clone();
        // Map the amended data into the original data
        for (key, value) in &data.amendments {
            new_data.entry(key.clone())
                .and_modify(|x| *x = value.clone())
                .or_insert(value.clone());
        }

        // Create the revision information
        let revision_data = RecordRevision {
            uuid: Uuid::new_v4(),
            data: data.amendments.clone(),
            uid: msg.user.uuid.clone(),
            ts: Utc::now(),
            reason: data.reason.clone(),
        };

        let mut revisions: Vec<RecordRevision> = Vec::new();
        if let Some(c) = context.revisions {
            revisions = c.clone();
        }
        revisions.push(revision_data);

        // Creat the revision
        let result: bool = match &conn.execute(&SQL_UPDATE_RECORD.replace("{SCHEMA}", &tki), &[
            &json::to_value(new_data).unwrap(),
            &json::to_value(revisions).unwrap(),
            &msg.user.uuid,
            &data.uuid,
        ]) { 
            Ok(_) => true,
            Err(err) => {
                eprintln!("{:?}", err);
                false
            }
        };

        if result {
            let record: Option<Record> = Record::get_record_by_id(&conn, &tki, &data.uuid);
            Ok(json::to_value(record).unwrap())
        } else {
            bail!("ERR_AMENDING_RECORD");
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AmendmentRejection {
    pub uuid: Uuid,
    pub reason: String,
}

pub fn reject_amendment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AmendmentRejection = json::from_value(msg.args.clone()).unwrap();

    let task: Option<Task> = Task::get_task_by_id(&conn, &tki, &data.uuid);

    if task.is_none() {
        bail!("ERR_NO_TASK");
    }
    let task: Task = task.unwrap();


    let task_data: TaskData = json::from_value(task.data).unwrap();
    let result: bool = Record::update_status(&conn, &tki, &task_data.record_id, "SUBMITTED");

    if !result {
        bail!("ERR_UPDATING_RECORD");
    }

    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "REJECTED");
    outcome.insert("reason", &task_data.reason);
    let outcome: Value = json::to_value(outcome).unwrap();

    let result: bool = Task::close_task(&conn, &tki, &task.uuid, &outcome, &msg.user.uuid);

    if !result {
        bail!("ERR_UPDATING_TASK");
    }

    // TODO: Send email notification to original user to let them know it was rejected

    Ok(json::to_value(true).unwrap())
}


static SQL_UPDATE_RECORD_APPROVE: &'static str = r#"
    UPDATE {SCHEMA}.records
    SET status = 'SUBMITTED',
        data = $1,
        modified = NOW(),
        modified_by = $2,
        revisions = $3
    WHERE uuid = $4;
"#;

pub fn approve_amendment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let task: Task = match Task::get_task_by_id(&conn, &tki, &id) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_TASK");
        }
    };

    let task_data: TaskData = json::from_value(task.data).unwrap();
    let record: Record = match Record::get_record_by_id(&conn, &tki, &task_data.record_id) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_RECORD");
        }
    };

    let mut data: HashMap<String, Value> = record.data.clone();
    for (key, value) in &task_data.amendments {
        data.entry(key.clone())
            .and_modify(|x| *x = value.clone())
            .or_insert(value.clone());
    }
    
    let mut revisions: Vec<RecordRevision> = record.revisions.clone().unwrap_or(Vec::new());
    // Create the revision information
    revisions.push(RecordRevision {
        uuid: Uuid::new_v4(),
        data: task_data.amendments.clone(),
        uid: msg.user.uuid.clone(),
        ts: Utc::now(),
        reason: task_data.reason.clone(),
    });

    let result: bool = match &conn.execute(&SQL_UPDATE_RECORD_APPROVE.replace("{SCHEMA}", &tki), &[
        &json::to_value(data).unwrap(),
        &task_data.creator_id,
        &json::to_value(revisions).unwrap(),
        &task_data.record_id,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if !result {
        bail!("ERR_UPDATING_RECORD");
    }

    // Update the task
    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "APPROVED");
    let outcome: Value = json::to_value(outcome).unwrap();
    let result: bool = Task::close_task(&conn, &tki, &task.uuid, &outcome, &msg.user.uuid);

    if !result {
        // TODO: Rollback changes
        bail!("ERR_UPDATING_TASK");
    }

    // TODO: Send email notification to the user

    Ok(json::to_value(true).unwrap())
}


