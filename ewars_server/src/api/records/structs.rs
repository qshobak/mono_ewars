use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use postgres::rows::Row;
use uuid::Uuid;

#[derive(Debug, Serialize, Clone)]
pub struct AuditData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub submitter: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Record {
    pub uuid: Uuid,
    pub status: String,
    pub stage: Option<String>,
    pub workflow: Option<Value>,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
    pub submitted: DateTime<Utc>,
    pub submitted_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub revisions: Option<Value>,
    pub comments: Option<Value>,
    pub eid: Option<String>,
    pub import_id: Option<Uuid>,
    pub source: Option<String>,
    #[serde(flatten)]
    pub audit_data: AuditData,
    pub metadata: Option<HashMap<String, Value>>,
}

impl<'a> From<Row<'a>> for Record {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        Record {
            uuid: row.get("uuid"),
            status: row.get("status"),
            stage: row.get("stage"),
            workflow: row.get("workflow"),
            fid: row.get("fid"),
            data: data,
            submitted: row.get("submitted"),
            submitted_by: row.get("submitted_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            revisions: row.get("revisions"),
            comments: row.get("comments"),
            eid: row.get("eid"),
            import_id: row.get("import_id"),
            source: row.get("source"),
            audit_data: AuditData {
                submitter: match row.get_opt("submitter") {
                    Some(res) => res.unwrap(),
                    None => Option::None,
                },
                modifier: match row.get_opt("modifier") {
                    Some(res) => res.unwrap(),
                    None => Option::None,
                }
            },
            metadata: match row.get_opt("metadata") {
                Some(res) => {
                    json::from_value(res.unwrap()).unwrap()
                },
                None => Option::None
            }
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct RecordSubmission {
    pub uuid: Option<Uuid>,
    pub fid: Uuid,
    pub data: Value,
}

#[derive(Debug, Deserialize)]
pub struct RecordAmendment {
    pub uuid: Uuid,
    pub data: HashMap<String, Value>,
    pub reason: String,
}
