use std::collections::HashMap;

use failure::Error;

use postgres::rows::{Row};

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use crate::api::forms::{FormFeature};
use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::services::{AlarmContext};

use crate::api::records::{Record, RecordSubmission, RecordAmendment};

static SQL_GET_RECORD: &'static str = r#"
    SELECT * FROM {SCHEMA}.records WHERE uuid = $1;
"#;

static SQL_INSERT_RECORD: &'static str = r#"
    INSERT INTO {SCHEMA}.records
    (uuid, status, stage, fid, data, submitted_by, modified_by, eid, source)
    VALUES ($1, 'SUBMITTED', NULL, $2, $3, $4, $5, $6, $7) RETURNING *;
"#;

static SQL_DELETE_DRAFT: &'static str = r#"
    DELETE FROM {SCHEMA}.drafts WHERE uuid = $1;
"#;

pub fn submit_record(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    eprintln!("{:?}", msg.args.clone());
    let data: RecordSubmission = json::from_value(msg.args.clone()).unwrap();
    eprintln!("{:?}", data);

    let eid: String = String::new();
    let source: String = "SYSTEM".to_string();

    if let Some(exist) = data.uuid {
        let _ = &conn.execute(&SQL_DELETE_DRAFT.replace("{SCHEMA}", &tki), &[&exist]).unwrap();
    }

    let new_uuid = Uuid::new_v4();

    let result: Option<Record> = match &conn.query(&SQL_INSERT_RECORD.replace("{SCHEMA}", &tki), &[
        &new_uuid,
        &data.fid,
        &data.data,
        &msg.user.uuid,
        &msg.user.uuid,
        &eid,
        &source,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Record::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if result.is_none() {
        bail!("ERR_UNK");
    }

    let result = result.unwrap();
    msg.alarm_svc.do_send(AlarmContext {
        record_id: Some(result.uuid.clone()),
        version: 1,
        start_date: Option::None,
        end_date: Option::None,
        locations: Option::None,
        tki: tki.clone(),
    });

    // TODO: do disagg service


    Ok(json::to_value(result).unwrap())
}

