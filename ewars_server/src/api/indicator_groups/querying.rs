use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::share::{Query, QueryResult};

use crate::api::utils::*;
use crate::api::indicator_groups::{IndicatorGroup};
use crate::api::indicators::{Indicator};

static SQL_QUERY_IND_GROUPS: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.indicator_groups AS r
"#;

static SQL_COUNT_IND_GROUPS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.indicator_groups AS r
"#;

pub fn query_indicator_groups(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_IND_GROUPS, SQL_COUNT_IND_GROUPS, tki, IndicatorGroup)
}

static SQL_GET_ROOT_IGS: &'static str = r#"
    SELECT r.*,
        uu.name || ' (' || uu.email || ')' AS creator,
        uc.name || ' (' || uu.email || ')' AS modifier
    FROM {SCHEMA}.indicator_groups AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.pid IS NULL;
"#;

pub fn get_root_indicator_groups(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let groups: Vec<IndicatorGroup> = match &conn.query(&SQL_GET_ROOT_IGS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| IndicatorGroup::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(groups).unwrap())
}

static SQL_GET_IND_CHILDS: &'static str = r#"
    SELECT r.*,
        uu.name || ' (' || uu.email || ')' AS creator,
        uc.name || ' (' || uc.email || ')' AS modifier
    FROM {SCHEMA}.indicators AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.group_id = $1;
"#;

static SQL_GET_IND_GROUP_CHILDS: &'static str = r#"
     SELECT r.*,
        uu.name || ' (' || uu.email || ')' AS creator,
        uc.name || ' (' || uc.email || ')' AS modifier
    FROM {SCHEMA}.indicator_groups AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.pid = $1;)
"#;

#[derive(Debug, Serialize, Clone)]
#[serde(untagged)]
pub enum TreeNode {
    Ind(Indicator),
    IndGroup(IndicatorGroup),
}

pub fn get_indicator_group_childs(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();
    let mut results: Vec<TreeNode> = Vec::new();

    let groups: Vec<IndicatorGroup> = match &conn.query(&SQL_GET_IND_GROUP_CHILDS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| IndicatorGroup::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let inds: Vec<Indicator> = match &conn.query(&SQL_GET_IND_CHILDS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| Indicator::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    for item in groups {
        results.push(TreeNode::IndGroup(item.clone()));
    }

    for item in inds {
        results.push(TreeNode::Ind(item.clone()));
    }

    Ok(json::to_value(results).unwrap())
}

static SQL_GET_IND_GROUP: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.indicator_groups AS r
    WHERE r.uuid = $1;
"#;

// Get an indicator group from the database
pub fn get_indicator_group(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let group_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<IndicatorGroup> = match &conn.query(&SQL_GET_IND_GROUP.replace("{SCHEMA}", &tki), &[&group_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(IndicatorGroup::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = result {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_UNK_GROUP");
    }
}
