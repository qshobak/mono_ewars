use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use postgres::rows::Row;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::indicator_groups::{IndicatorGroup};
use crate::api::utils::*;

static SQL_CREATE_GROUP: &'static str = r#"
    INSERT INTO {SCHEMA}.indicator_groups
    (name, pid, created_by, modified_by)
    VALUES ($1, $2, $3, $4) RETURNING *;
"#;



#[derive(Debug, Deserialize)]
pub struct NewGroup {
    pub name: String,
    pub pid: Option<Uuid>,
}


pub fn create_indicator_group(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let create: NewGroup = json::from_value(msg.args.clone()).unwrap();

    let result: Option<IndicatorGroup> = match conn.query(&SQL_CREATE_GROUP.replace("{SCHEMA}", &tki), &[
        &create.name,
        &create.pid,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(IndicatorGroup::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    match result {
        Some(res) => {
            Ok(json::to_value(res).unwrap())
        },
        None => {
            Ok(json::to_value(false).unwrap())
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct UpdateGroup {
    pub uuid: Uuid,
    pub name: String,
    pub pid: Option<Uuid>
}

static SQL_UPDATE_GROUP: &'static str = r#"
    UPDATE {SCHEMA}.indicator_groups
    SET name = $1,
        pid = $2,
        modified_by = $3
    WHERE uuid = $4;
"#;

static SQL_GET_GROUP: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.indicator_groups AS r
    WHERE r.uuid = $1;
"#;

// Update an indicator group
pub fn update_indicator_group(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: UpdateGroup = json::from_value(msg.args.clone()).unwrap();

    let result: bool = match &conn.execute(&SQL_UPDATE_GROUP.replace("{SCHEMA}", &tki), &[
        &data.name,
        &data.pid,
        &msg.user.uuid,
        &data.uuid,
    ]) {
        Ok(res) => true,
        Err(err) => false
    };

    if result {
        let output: Option<IndicatorGroup> = get_single_from_args!(conn, SQL_GET_GROUP, IndicatorGroup, tki, &[&data.uuid]);
        if let Some(c) = output {
            Ok(json::to_value(c).unwrap())
        } else {
            bail!("ERR_UNK_GROUP");
        }
    } else {
        bail!("ERR_UNK_GROUP");
    }
}


static SQL_GET_CHILD_GROUPS: &'static str = r#"
    WITH RECURSIVE sources AS (
        SELECT id, ARRAY[]::UUID[] AS ancestors
        FROM {SCHEMA}.indicator_groups WHERE parent_id IS NULL

        UNION ALL

        SELECT {SCHEMA}.indicator_groups, source.ancestors || S{CHEMA}.indicator_group.parent_id
        FROM {SCHEMA}.indicator_groups, sources
        WHERE {SCHEMA}.idnicator_groups.parent_id = sources.id
    ) SELECT uuid FROM sources WHERE $1 = ANY(sources.ancestors);
"#;


static SQL_GET_CHILD_INDICATORS: &'static str = r#"
    SELECT uuid
    FROM {SCHEMA}.indicators
    WHERE pid = ANY($1);
"#;

static SQL_DELETE_IND_CHILDS: &'static str = r#"
    DELETE FROM {SCHEMA}.indicators WHERE uuid = ANY($1);
"#;

static SQL_DELETE_IND_CHILD_GROUPS: &'static str = r#"
    DELETE FROM {SCHEMA}.indicator_groups WHERE uuid = ANY($1);
"#;

static SQL_DELETE_GROUP: &'static str = r#"
    DELETE FROM {SCHEMA}.indicator_groups WHERE uuid = $1;
"#;

pub fn delete_indicator_group(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let children: Vec<Uuid> = match &conn.query(&SQL_GET_CHILD_GROUPS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let indicators: Vec<Uuid> = match &conn.query(&SQL_GET_CHILD_INDICATORS.replace("{SCHEMA}", &tki), &[&children]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let _ = &conn.execute(&SQL_DELETE_IND_CHILDS.replace("{SCHEMA}", &tki), &[&indicators]).unwrap();
    let _ = &conn.execute(&SQL_DELETE_IND_CHILD_GROUPS.replace("{SCHEMA}", &tki), &[&children]).unwrap();
    let _ = &conn.execute(&SQL_DELETE_GROUP.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}
