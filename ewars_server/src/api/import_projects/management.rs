use serde_json as json;
use serde_json::Value;

use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};


pub fn create_project(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}

pub fn delete_project(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}


pub fn update_project(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
