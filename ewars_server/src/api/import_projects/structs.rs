use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use postgres::rows::{Row};
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ImportProject {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub status: String,
    pub form_id: Uuid,
    pub mapping: Option<Value>,
    pub meta_map: Option<Value>,
    pub src_file: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<String>,
    pub form_name: Option<Value>,
}

impl<'a> From<Row<'a>> for ImportProject {
    fn from(row: Row) -> Self {
        ImportProject {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            status: row.get("status"),
            form_id: row.get("form_id"),
            mapping: row.get("mapping"),
            meta_map: row.get("meta_map"),
            src_file: row.get("src_file"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            form_name: row.get("form_name"),
        }
    }
}
