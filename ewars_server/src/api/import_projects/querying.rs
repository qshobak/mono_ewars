use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::import_projects::{ImportProject};

static SQL_GET_IMPORTS: &'static str = r#"
    SELECT i.*, f.name
    FROM {SCHEMA}.import_projects AS i
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = i.form_id
    WHERE i.form_id = $1;
"#;

pub fn query_import_projects(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let form_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let imports: Vec<ImportProject> = match &conn.query(&SQL_GET_IMPORTS.replace("{SCHEMA}", &tki), &[&tki]) {
        Ok(rows) => {
            rows.iter().map(|x| ImportProject::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(imports).unwrap())
}

static SQL_GET_PROJECT: &'static str = r#"
    SELECT *
    FROM {SCHEMA}.import_projects
    WHERE uuid = $1;
"#;

pub fn get_import_project(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let project: Option<ImportProject> = match &conn.query(&SQL_GET_PROJECT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(ImportProject::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = project {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_UNK_PROJECT");
    }
}

