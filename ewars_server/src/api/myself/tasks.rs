use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::models::{UserSession};
use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::tasks::{Task};


pub fn get_open_task_count(conn: &PoolConnection, user: &UserSession) -> i64 {
    0
}

static SQL_GET_OWN_TASKS_ACC_ADMIN: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.tasks AS r
        WHERE r.status = 'OPEN'
        AND r.context->>'roles' = ANY(ARRAY['ACCOUNT_ADMIN']);
"#;

static SQL_GET_OWN_TASKS_REG_ADMIN: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.tasks AS r
        WHERE r.satus = 'OPEN'
        AND r.context->>'roles' = ANY(ARRAY['REGIONAL_ADMIN'])
        AND r.context->>'lid' = ANY($1::TEXT[]);
"#;

static SQL_GET_OWN_TASKS_USER: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.tasks AS r
    WHERE r.context->>'uid' = $1
        AND r.status = 'OPEN';
"#;

// Get tasks for the given user
pub fn get_open_tasks(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let mut sql = String::new();
    let results: Vec<Task> = match msg.user.role.as_ref() {
        "ACCOUNT_ADMIN" => {
            sql = SQL_GET_OWN_TASKS_ACC_ADMIN.to_string();
            match &conn.query(&sql.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    rows.iter().map(|x| Task::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        },
        "REGIONAL_ADMIN" => {
            sql = SQL_GET_OWN_TASKS_REG_ADMIN.to_string();
            match &conn.query(&sql.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    rows.iter().map(|x| Task::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        },
        _ => {
            sql = SQL_GET_OWN_TASKS_USER.to_string();
            match &conn.query(&sql.replace("{SCHEMA}", &tki), &[&msg.user.uuid]) {
                Ok(rows) => {
                    rows.iter().map(|x| Task::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        }
    };

    Ok(json::to_value(results).unwrap())
}
