use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::utils::auth::*;

static SQL_UPDATE_PASSWORD: &'static str = r#"
    UPDATE core.users
    SET password = $1
    WHERE uuid = $2;
"#;

pub fn update_password(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let new_pass: String = json::from_value(msg.args.clone()).unwrap();

    let coded_pass: String = generate_password(&new_pass).unwrap();


    match conn.execute(&SQL_UPDATE_PASSWORD.replace("{SCHEMA}", &tki), &[&coded_pass, &msg.user.uuid]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}
