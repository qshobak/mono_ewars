use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
struct TemplateGeneration {
    pub location_type: Option<i32>,
    pub location_spec: Option<String>,
    pub notify: Option<bool>,
    pub interval: Option<String>,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
    pub location_uuid: Option<String>,
    pub notifiers: Option<Vec<String>>,
    pub document_date: Option<String>,
    pub location: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Template {
    pub uuid: Uuid,
    pub template_name: Value,
    pub instance_name: Value,
    pub generation: TemplateGeneration,
    pub status: String,
    pub template_type: String,
}

impl<'a> From<Row<'a>> for Template {
    fn from(row: Row) -> Self {
        let gen: TemplateGeneration = json::from_value(row.get(3)).unwrap();
        Template {
            uuid: row.get(0),
            template_name: row.get(1),
            instance_name: row.get(2),
            generation: gen,
            status: row.get(4),
            template_type: row.get(5),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct TemplateLocation {
    uuid: Uuid,
    name: Value,
}

impl TemplateLocation {
    pub fn from_row(row: &Row) -> Self {
        Self {
            uuid: row.get(0),
            name: row.get(1),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct TemplateResult {
    template: Template,
    locations: Vec<TemplateLocation>,
}


static SQL_GET_DOCUMENTS_ADMIN: &'static str = r#"
    SELECT uuid, template_name, instance_name, generation, status, template_type
    FROM {SCHEMA}.documents
    WHERE (status = 'ACTIVE' OR status = 'DRAFT');
"#;

static SQL_GET_DOCUMENTS: &'static str = r#"
    SELECT uuid, template_name, instance_name, generation, status, template_type
    FROM {SCHEMA}.documents
    WHERE status = 'ACTIVE';
"#;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.locations
    WHERE uuid::TEXT = $1;
"#;

static SQL_GET_LOCATIONS_OF_TYPE: &'static str = r#"
    SELECT uuid, name,
    FROM {SCHEMA}.locations
    WHERE site_type_id = $1
        AND status = 'ACTIVE';
"#;

pub fn get_documents(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let templates: Vec<Template> = match msg.user.role.as_ref() {
        "USER" => {
            match &conn.query(&SQL_GET_DOCUMENTS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => rows.iter().map(|x| Template::from(x)).collect(),
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        },
        _ => {
            match &conn.query(&SQL_GET_DOCUMENTS_ADMIN.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => rows.iter().map(|x| Template::from(x)).collect(),
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        }
    };

    let mut results: Vec<TemplateResult> = Vec::new();

    for temp in &templates {
        if let Some(ref loc_spec) = temp.generation.location_spec {
            let locations: Vec<TemplateLocation> = match loc_spec.as_ref() {
                "SPECIFIC" => {
                    // Need to normalize this setting, it can exist in location_uuid, or location in the generation
                    let mut loc_id: Option<String> = Option::None;
                    if let Some(ref g_loc) = temp.generation.location_uuid {
                        loc_id = Some(g_loc.clone());
                    } else {
                        if let Some(ref gloc) = temp.generation.location {
                            loc_id = Some(gloc.clone());
                        }
                    }

                    if let Some(lid) = loc_id {
                        let locations: Vec<TemplateLocation> = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&lid]) {
                            Ok(rows) => {
                                rows.iter().map(|x| TemplateLocation::from_row(&x)).collect()
                            },
                            Err(_) => {
                                Vec::new()
                            }
                        };
                        locations
                    } else {
                        Vec::new()
                    }
                },
                "TYPE" => {
                    let location_type: i32 = temp.generation.location_type.clone().unwrap();

                    let locations: Vec<TemplateLocation> = match &conn.query(&SQL_GET_LOCATIONS_OF_TYPE.replace("{SCHEMA}", &tki), &[&tki, &location_type]) {
                        Ok(rows) => {
                            rows.iter().map(|x| TemplateLocation::from_row(&x)).collect()
                        },
                        Err(_) => {
                            Vec::new()
                        }
                    };

                    locations
                },
                _ => Vec::new()
            };

            results.push(TemplateResult {
                template: temp.clone(),
                locations: locations,
            })
        } else {
            results.push(TemplateResult {
                template: temp.clone(),
                locations: Vec::new(),
            })
        }
    }

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

