use serde_json as json;
use serde_json::Value;
use failure::Error;

use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};


#[derive(Debug, Deserialize)]
pub struct UserUpdate {
    pub name: String,
    pub org_id: Option<Uuid>,
    pub profile: Option<Value>,
}

static SQL_UPDATE_CORE_USER: &'static str = r#"
    UPDATE core.users
    SET name = $1
    WHERE uuid = $2;
"#;

static SQL_UPDATE_ACC_USER: &'static str = r#"
    UPDATE {SCHEMA}.accounts
    SET org_id = $1,
        profile = $2
    WHERE uuid = $3;
"#;

pub fn update_user(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: UserUpdate = json::from_value(msg.args.clone()).unwrap();


    let uid: Uuid = msg.user.uuid.clone();

    let result: bool = match &conn.execute(&SQL_UPDATE_CORE_USER, &[&data.name, &uid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        let acc_result: bool = match &conn.execute(&SQL_UPDATE_ACC_USER.replace("{SCHEMA}", &tki), &[&data.org_id, &data.profile, &uid]) {
            Ok(_) => true,
            Err(err) => {
                eprintln!("{:?}", err);
                false
            }
        };
        
        if acc_result {
            Ok(json::to_value(true).unwrap())
        } else {
            bail!("ERR_UPDATING_ACC_USER");
        }
    } else {
        bail!("ERR_UPDATING_CORE_USER");
    }
}
