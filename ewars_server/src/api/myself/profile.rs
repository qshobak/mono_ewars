use serde_json as json;
use serde_json::Value;
use failure::Error;

use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Serialize, Clone)]
pub struct UserProfile {
    pub uuid: Uuid,
    pub role: String,
    pub permissions: Value,
    pub settings: Value,
    pub profile: Value,
    pub org_id: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub status: String,
    pub tki: String,
    pub account_name: String,
    pub aid: Uuid,
    pub name: String,
    pub email: String,
    pub accounts: Vec<Uuid>,
    pub org_name: Option<Value>,
    pub system: bool,
}

impl<'a> From<Row<'a>> for UserProfile {
    fn from (row: Row) -> Self {
        UserProfile {
            uuid: row.get("uuid"),
            role: row.get("role"),
            permissions: row.get("permissions"),
            settings: row.get("settings"),
            profile: row.get("profile"),
            org_id: row.get("org_id"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            status: row.get("status"),
            tki: row.get("tki"),
            account_name: row.get("account_name"),
            aid: row.get("aid"),
            name: row.get("name"),
            email: row.get("email"),
            accounts: row.get("accounts"),
            org_name: row.get("org_name"),
            system: row.get("system"),
        }
    }
}



static SQL_GET_PROFILE: &'static str = r#"
    SELECT * FROM {SCHEMA}.users WHERE uuid = $1;
"#;

// Get the users profile for the profile page
pub fn get_own_profile(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = msg.user.uuid.clone();

    let result: Option<UserProfile> = match conn.query(&SQL_GET_PROFILE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(UserProfile::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(user) = result {
        Ok(json::to_value(user).unwrap())
    } else {
        bail!("ERR_NO_USER");
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct ProfileUpdate {
    pub prop: String,
    pub value: Value,
}

static SQL_UPDATE_OCCUPATION: &'static str = r#"
    UPDATE {SCHEMA}.users
    SET profile = JSONB_SET(profile, 'occupation', $1, TRUE)
    WHERE uuid = $2;
"#;

static SQL_UPDATE_BIO: &'static str = r#"
    UPDATE {SCHEMA}.users
    SET profile = JSONB_SET(profile, 'bio', $1, TRUE)
    WHERE uuid = $2;)
"#;

static SQL_UPDATE_PHONE: &'static str = r#"
    UPDATE {SCHEMA}.users
    SET profile = JSONB_SET(profile, 'phone', $1, TRUE)
    WHERE uuid = $2;
"#;

static SQL_UPDATE_ORG_ID: &'static str = r#"
    UPDATE {SCHEMA}.accounts
    SET org_id = $1
    WHERE uuid = $2;
"#;

pub fn update_profile(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = msg.user.uuid.clone();
    let data: ProfileUpdate = json::from_value(msg.args.clone()).unwrap();

    let prop: String = data.prop.clone();
    let value: Value = data.value.clone();

    if !value.is_null() {
        match prop.as_ref() {
            "name" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let _ = conn.execute("UPDATE core.users SET name = $1 WHERE uuid = $2", &[&value, &id]).unwrap();
            },
            "email" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let _ = conn.execute("UPDATE core.users SET email = $1 WHERE uuid = $2", &[&value, &id]).unwrap();
            },
            "occupation" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let _ = conn.execute(&SQL_UPDATE_OCCUPATION.replace("{SCHEMA}", &tki), &[&value, &id]).unwrap();
            },
            "bio" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let _ = conn.execute(&SQL_UPDATE_BIO.replace("{SCHEMA}", &tki), &[&value, &id]).unwrap();
            },
            "phone" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let _ = conn.execute(&SQL_UPDATE_PHONE.replace("{SCHEMA}", &tki), &[&value, &id]).unwrap();
            },
            "org_id" => {
                let value: String = data.value.as_str().unwrap().to_string();
                let end_id: Uuid = Uuid::parse_str(&value).unwrap();
                let _ = conn.execute(&SQL_UPDATE_ORG_ID.replace("{SCHEMA}", &tki), &[&end_id, &id]).unwrap();
            },
            _ => {}
        }
    }

    Ok(json::to_value(true).unwrap())
}


// Allow a user to request deletion of their account, this will anonymize any data associated
// with the user and wipe them from the account altogether, show a warning that if they want to be 
// completely removed from the whole EWARS ecosystem then they need to go to a differentn URL
// though
pub fn request_delete_account(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
