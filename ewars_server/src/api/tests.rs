use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::models::{UserSession};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::services::{Email};

pub fn test_email_send(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {

    let new_email = Email {
        recipients: vec!["jduren@protonmail.com".to_string()],
        account_name: Some("TEST".to_string()),
        subject: "[EWARS] Email Cycling Text".to_string(),
        plain_content: Some("[EWARS] Email send test".to_string()),
        html_content: "<p>[EWARS] Email Send test</p>".to_string(),
        system: true
    };

    msg.email_svc.do_send(new_email);


    Ok(json::to_value(true).unwrap())
}
