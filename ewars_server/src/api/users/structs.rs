use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct User {
    pub uuid: Uuid,
    pub role: String,
    pub permissions: Option<Value>,
    pub settings: Option<Value>,
    pub profile: Option<Value>,
    pub org_id: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub status: String,
    pub tki: String,
    pub account_name: String,
    pub aid: Option<Uuid>,
    pub name: String,
    pub email: String,
    pub accounts: Option<Vec<Uuid>>,
    pub org_name: Option<Value>,
    pub system: bool,
    pub assignments: Option<i64>,
}

impl<'a> From<Row<'a>> for User {
    fn from(row: Row) -> Self {
        User {
            uuid: row.get("uuid"),
            role: row.get("role"),
            permissions: row.get("permissions"),
            settings: row.get("settings"),
            profile: row.get("profile"),
            org_id: row.get("org_id"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            status: row.get("status"),
            tki: row.get("tki"),
            account_name: row.get("account_name"),
            aid: row.get("aid"),
            name: row.get("name"),
            email: row.get("email"),
            accounts: row.get("accounts"),
            org_name: row.get("org_name"),
            system: row.get("system"),
            assignments: match row.get_opt("assignments") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}
