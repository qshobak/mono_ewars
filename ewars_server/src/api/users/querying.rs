use serde_json as json;
use serde_json::Value;

use failure::Error;

use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::share::{Query, QueryResult};

use crate::api::utils::*;
use crate::api::users::{User};

static SQL_QUERY_USERS: &'static str = r#"
    WITH act_users AS (
        SELECT * FROM {SCHEMA}.users AS c
        WHERE c.status = ANY(ARRAY['ACTIVE', 'INACTIVE'])
    )
    SELECT r.*,
        (SELECT COUNT(a.uuid) FROM {SCHEMA}.assignments AS a WHERE a.uid = r.uuid) AS assignments
    FROM act_users AS r
"#;

static SQL_COUNT_USERS: &'static str = r#"
    WITH act_users AS (
        SELECT * FROM {SCHEMA}.users AS c
        WHERE c.status = ANY(ARRAY['ACTIVE', 'INACTIVE'])
    )
    SELECT COUNT(r.uuid)
    FROM act_users AS r
"#;

static SQL_GET_USER: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.users AS r
    WHERE r.uuid = $1;
"#;

pub fn query_users(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_USERS, SQL_COUNT_USERS, tki, User)
}

pub fn get_user(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();
    let result: Option<User> = match &conn.query(&SQL_GET_USER.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(User::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}
