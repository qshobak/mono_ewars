use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Draft {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub data: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Uuid,
    pub modified: DateTime<Utc>,
    pub modified_by: Uuid,
}

impl<'a> From<Row<'a>> for Draft {
    fn from(row: Row) -> Self {
        Draft {
            uuid: row.get("uuid"),
            fid: row.get("fid"),
            data: row.get("data"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by")
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct DraftUpdate {
    pub uuid: Uuid,
    pub data: Option<Value>,
}


#[derive(Debug, Deserialize, Clone)]
pub struct DraftCreate {
    pub data: Option<Value>,
}
