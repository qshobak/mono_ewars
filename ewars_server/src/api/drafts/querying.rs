use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::models::{UserSession};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::share::{Query, QueryResult};

use crate::api::utils::*;

use crate::models::{Draft};

static SQL_QUERY_DRAFTS: &'static str = r#"
    SELECT r.*,
        f.name AS form_name,
        l.name AS location_name
    FROM {SCHEMA}.drafts AS r
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
"#;

static SQL_COUNT_DRAFTS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.drafts AS r
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
"#;

static SQL_GET_DRAFT: &'static str = r#"
    SELECT r.*,
        f.name AS form_name
    FROM {SCHEMA}.drafts AS r
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
    WHERE r.uuid = $1;
"#;

pub fn query_drafts(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let mut query: Query = json::from_value(msg.args.clone()).unwrap();

    let id: Value = json::to_value(msg.user.uuid.to_string()).unwrap();
    query.filters.entry("created_by".to_string())
        .and_modify(|x| *x = ("eq".to_string(), id.clone()))
        .or_insert(("eq".to_string(), id.clone()));

    query_fn!(conn, query, SQL_QUERY_DRAFTS, SQL_COUNT_DRAFTS, tki, Draft)
}

pub fn get_draft(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Draft> = match &conn.query(&SQL_GET_DRAFT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Draft::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}
