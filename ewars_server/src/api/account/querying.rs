use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use failure::Error;

use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::account::{Account};

static SQL_GET_ACCOUNT: &'static str = r#"
    SELECT * FROM core.accounts WHERE tki = $1;
"#;

static SQL_GET_CONFS: &'static str = r#"
    SELECT key, value
    FROM {SCHEMA}.conf;
"#;

#[derive(Debug, Serialize)]
pub struct AccountConf {
    pub key: String,
    pub value: Value,
}

impl<'a> From<Row<'a>> for AccountConf {
    fn from(row: Row) -> Self {
        AccountConf {
            key: row.get(0),
            value: row.get(1),
        }
    }
}

pub fn get_account(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let account: Option<Account> = match &conn.query(&SQL_GET_ACCOUNT, &[&msg.user.tki]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Account::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    let confs: Vec<AccountConf> = match &conn.query(&SQL_GET_CONFS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| AccountConf::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    if let Some(c) = account {
        let result: (Account, Vec<AccountConf>) = (c, confs);
        Ok(json::to_value(result).unwrap())
    } else {
        bail!("ERR_LOADING_ACCOUNT");
    }
}
