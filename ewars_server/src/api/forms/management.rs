use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::api::forms::{Form, FormUpdate, FormCreate};

use postgres::Error as PGError;
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_DELETE_RECORDS: &'static str = r#"
    DELETE FROM {SCHEMA}.records WHERE fid = $1;
"#;

static SQL_DELETE_ASSIGNMENTS: &'static str = r#"
    DELETE FROM {SCHEMA}.assignments WHERE fid = $1;
"#;

static SQL_DELETE_REPORTING: &'static str = r#"
    DELETE FROM {SCHEMA}.reporting WHERE fid = $1;
"#;

static SQL_DELETE_FORM: &'static str = r#"
    DELETE FROM {SCHEMA}.forms WHERE uuid = $1;
"#;


pub fn delete_form(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();
    eprintln!("{}", id);

    // Delete all the sub-items firwt
    //
    let trans = conn.transaction().unwrap();

    trans.execute(&SQL_DELETE_RECORDS.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    trans.execute(&SQL_DELETE_ASSIGNMENTS.replace("{SCHEMA}", &tki), &[&id]).unwrap();
    trans.execute(&SQL_DELETE_REPORTING.replace("{SCHEMA}", &tki), &[&id]).unwrap();
    trans.execute(&SQL_DELETE_FORM.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    match trans.commit() {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_UPDATE_FORM: &'static str = r#"
    UPDATE {SCHEMA}.forms
        SET name = $1,
            status = $2,
            description = $3,
            guidance = $4,
            eid_prefix = $5,
            features = $6,
            definition = $7,
            stages = $8,
            etl = $9,
            changes = $10,
            permissions = $11,
            modified_by = $12
    WHERE uuid = $13;
"#;

pub fn update_form(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let update: FormUpdate = json::from_value(msg.args.clone()).unwrap();

    match &conn.execute(&SQL_UPDATE_FORM.replace("{SCHEMA}", &tki), &[
        &update.name,
        &update.status,
        &update.description,
        &update.guidance,
        &update.eid_prefix,
        &update.features,
        &update.definition,
        &update.stages,
        &update.etl,
        &update.changes,
        &update.permissions,
        &msg.user.uuid,
        &update.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Ok(json::to_value(false).unwrap())
        }
    }
}

static SQL_CREATE_FORM: &'static str = r#"
    INSERT INTO {SCHEMA}.forms
    (name, version, status, description, guidance, eid_prefix, features, definition, stages, constraints, etl, changes, permissions, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING *;
"#;

pub fn create_form(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let new: FormCreate = json::from_value(msg.args.clone()).unwrap();

    let result: Value = match &conn.query(&SQL_CREATE_FORM.replace("{SCHEMA}", &tki), &[
        &new.name,
        &new.version,
        &new.status,
        &new.description,
        &new.guidance,
        &new.eid_prefix,
        &new.features,
        &new.definition,
        &new.stages,
        &new.constraints,
        &new.etl,
        &new.changes,
        &new.permissions,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                json::to_value(Form::from(row)).unwrap()
            } else {
                json::to_value(false).unwrap()
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            json::to_value(false).unwrap()
        }
    };

    Ok(result)
}

static SQL_DUPE_FORM: &'static str = r#"
    WITH rows AS (
        SELECT f.*, 'DRAFT' AS r_status, $1::UUID AS r_created_by, $2::UUID AS r_modified_by
        FROM {SCHEMA}.forms AS f
        WHERE f.uuid = $3
    )
    INSERT INTO {SCHEMA}.forms
    (name, version, status, description, guidance, eid_prefix, features, definition, constraints, stages, etl, permissions, created_by, modified_by)
    SELECT name, version, r_status, description, guidance, eid_prefix, features, definition, constraints, stages, etl, permissions, r_created_by, r_modified_by FROM rows;
"#;

pub fn duplicate_form(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_DUPE_FORM.replace("{SCHEMA}", &tki), &[
        &msg.user.uuid,
        &msg.user.uuid,
        &id,
    ]) {
        Ok(row) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}
