use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::Row;

use crate::api::utils::*;
use crate::models::{Form};
use crate::share::{Query, QueryResult};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};



#[derive(Debug, Serialize, Deserialize)]
pub struct SummaryForm {
    uuid: Uuid,
    name: Value,
    status: String,
    features: Value
}

impl<'a> From<Row<'a>> for SummaryForm {
    fn from(row: Row) -> Self {
        SummaryForm {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            features: row.get("features"),
        }
    }
}


static SQL_GET_ACTIVE_FORMS: &'static str = r#"
    SELECT * FROM {SCHEMA}.forms
    WHERE status = 'ACTIVE';
"#;

static SQL_GET_FULL_FORM: &'static str = r#"
    SELECT r.*,
        uc.name AS creator_name,
        uc.email AS creator_email,
        uu.name AS updater_name,
        uc.email AS updater_email
    FROM {SCHEMA}.forms AS r
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn get_active_forms(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<SummaryForm> = match &conn.query(&SQL_GET_ACTIVE_FORMS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| SummaryForm::from(x)).collect()
        }
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}

// Get a form definition
pub fn get_form(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let form_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<FormRow> = match &conn.query(&SQL_GET_FULL_FORM.replace("{SCHEMA}", &tki), &[&form_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(FormRow::from(row))
            } else {
                Option::None
            }
        }
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };
    if let Some(data) = result {
        Ok(json::to_value(data).unwrap())
    } else {
        Ok(json::to_value(false).unwrap())
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormRow {
    pub uuid: Uuid,
    pub id: i32,
    pub name: Value,
    pub version: i32,
    pub status: String,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: Value,
    pub definition: Value,
    pub etl: Value,
    pub changes: Value,
    pub permissions: Value,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator_name: Option<String>,
    pub creator_email: Option<String>,
    pub modifier_name: Option<String>,
    pub modifier_email: Option<String>,
}

impl<'a> From<Row<'a>> for FormRow {
    fn from(row: Row) -> Self {
        FormRow {
            uuid: row.get("uuid"),
            id: row.get("id"),
            name: row.get("name"),
            version: row.get("version"),
            status: row.get("status"),
            description: row.get("description"),
            guidance: row.get("guidance"),
            eid_prefix: row.get("eid_prefix"),
            features: row.get("features"),
            definition: row.get("definition"),
            etl: row.get("etl"),
            changes: row.get("changes"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator_name: row.get("creator_name"),
            creator_email: row.get("creator_email"),
            modifier_name: row.get("updater_name"),
            modifier_email: row.get("updater_email"),
        }
    }
}

impl FormRow {
    pub fn from_row(row: &Row) -> FormRow {
        FormRow {
            uuid: row.get(0),
            id: row.get(1),
            name: row.get(2),
            version: row.get(3),
            status: row.get(4),
            description: row.get(5),
            guidance: row.get(6),
            eid_prefix: row.get(7),
            features: row.get(8),
            definition: row.get(9),
            etl: row.get(10),
            changes: row.get(11),
            permissions: row.get(12),
            created: row.get(13),
            created_by: row.get(14),
            modified: row.get(15),
            modified_by: row.get(16),
            creator_name: row.get(17),
            creator_email: row.get(18),
            modifier_name: row.get(19),
            modifier_email: row.get(20),
        }
    }
}

pub fn query_forms(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let qr: Query = json::from_value(msg.args.clone()).unwrap();

    let mut sql = r#"
        SELECT r.*,
            uc.name AS creator_name,
            uc.email AS creator_email,
            uu.name AS updater_name,
            uc.email AS updater_email
        FROM {SCHEMA}.forms AS r
            LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.created_by
            LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    "#.to_string();

    let mut sql_count: String = r#"
        SELECT COUNT(r.*) AS total
        FROM {SCHEMA}.forms As r
            LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.created_by
            LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    "#.to_string();

    let mut wheres: Vec<String> = Vec::new();
    let mut orderings: Vec<String> = Vec::new();

    for (key, filter) in qr.clone().filters {
        let (cmp,val) = qr.filters.get(&key).unwrap();
        let mut flt_str = String::new();

        flt_str.push_str("r.");
        flt_str.push_str(&key);

        let s_cmp = match cmp.as_ref() {
            "eq" => " = ",
            "neq" => " != ",
            "gt" => " > ",
            "gte" => " >= ",
            "lt" => " < ",
            "lte" => " <= ",
            _ => " = "
        }.to_string();
        flt_str.push_str(&s_cmp);

        flt_str.push_str(&format!(" '{}'", val));
        wheres.push(flt_str.to_owned());
    }

    for (key, order) in qr.clone().orders {
        let (el_type, dir) = qr.orders.get(&key).unwrap();

        let mut ord_str: String = String::new();

        ord_str.push_str("r.");
        ord_str.push_str(&key);
        ord_str.push_str(" ");
        ord_str.push_str(dir);
        orderings.push(ord_str.to_owned());
    }

    if wheres.len() > 0 {
        sql.push_str("WHERE ");
        sql_count.push_str("WHERE");

        let mut sql_f = true;
        for item in wheres {
            if sql_f != true {
                sql.push_str("AND");
                sql_count.push_str("AND");
            }
            sql.push_str(&item);
            sql_count.push_str(&item);
            sql_f = false;
        }
    }

    if orderings.len() > 0 {
        sql.push_str("ORDER BY");

        sql.push_str(&orderings.join(", "));
    }

    sql.push_str(&format!("LIMIT {} ", qr.limit));
    sql.push_str(&format!("OFFSET {} ", qr.offset));


    eprintln!("{}", sql);

    let records: Vec<FormRow> = match &conn.query(&sql.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| FormRow::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let count: i64 = get_scalar!(&conn, &sql_count, &tki);

    let result = QueryResult {
        results: json::to_value(records).unwrap(),
        count,
    };

    Ok(json::to_value(result).unwrap())
}
