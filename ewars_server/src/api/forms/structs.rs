use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use postgres::rows::{Row};
use chrono::prelude::*;

pub type Rule = (String, String, String);
pub type RuleGroup = Vec<(String, String, String)>;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FieldCondition {
    Rule((String, String, String)),
    RuleGroup(Vec<(String, String, String)>),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormFeature {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lti: Option<Uuid>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interval: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub threshold: Option<NumericValue>,
}

impl FormFeature {
    pub fn threshold(&self) -> Option<i64> {
        if let Some(c) = self.threshold.clone() {
            match c {
                NumericValue::Int(v) => {
                    Some(v as i64)
                },
                NumericValue::Float(v) => {
                    let a_int: i64 = v as i64;
                    Some(a_int)
                },
                NumericValue::Str(c) => {
                    let a_int: i64 = c.parse().unwrap();
                    Some(a_int)
                },
                _ => Option::None
            }
        } else {
            Option::None
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum NumericValue {
    Int(i32),
    Float(f64),
    Str(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum LabelValue {
    I18N(HashMap<String, String>),
    Standard(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Field {
    pub uuid: Option<Uuid>,
    pub name: Option<String>,
    pub label: LabelValue,
    pub options: Option<Vec<(String, String)>>,
    pub required: Option<bool>,
    #[serde(rename="type")]
    pub field_type: String,
    pub allow_future_dates: Option<bool>,
    pub show_mobile: Option<bool>,
    pub order: NumericValue,
    pub help: Option<HashMap<String, String>>,
    pub conditions: Option<Value>,
    pub conditional_bool: Option<bool>,
    pub interval: Option<String>,
    pub date_type: Option<String>,
    pub min: Option<NumericValue>,
    pub max: Option<NumericValue>,
    pub multi_select: Option<bool>,
    pub header_style: Option<String>,
    pub fields: Option<HashMap<String, Field>>,
}

impl Field {
    // get the display value for a given field and value
    // // only used for select-based fields
    pub fn get_select_display_value(&self, val: &String) -> Option<String> {
        let mut dis_val: String = val.clone();

        if let Some(options) = self.options.clone() {
            for item in options.iter() {
                if &item.0 == val {
                    dis_val = item.1.clone();
                }
            }
        }
        Some(dis_val)
    }

    // Get the language dependent label for the field
    pub fn get_label(&self, lang: &str) -> String {
        match self.label.clone() {
            LabelValue::Standard(c) => {
                c.to_string()
            },
            LabelValue::I18N(c) => {
                if let Some(d) = c.get(lang) {
                    d.to_string()
                } else {
                    "".to_string()
                }
            },
            _ => {
                "".to_string()
            }
        }
    }

    // Get the order from the field as an i32
    pub fn order(&self) -> i32 {
        match self.order.clone() {
            NumericValue::Int(c) => {
                c as i32
            },
            NumericValue::Float(c) => {
                let a_int: i32 = c as i32;
                a_int
            },
            NumericValue::Str(c) => {
                let o_int: i32 = c.parse().unwrap();
                o_int
            },
            _ => {
                0
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Form {
    pub uuid: Uuid,
    pub id: i32,
    pub name: Value,
    pub version: i32,
    pub status: String,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: Value,
    pub definition: HashMap<String, Field>,
    pub stages: Option<Value>,
    pub constraints: Option<Value>,
    pub etl: Value,
    pub changes: Option<Value>,
    pub permissions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Form {
    fn from(row: Row) -> Self {
        let definition: HashMap<String, Field> = json::from_value(row.get("definition")).unwrap();
        Form {
            uuid: row.get("uuid"),
            id: row.get("id"),
            name: row.get("name"),
            version: row.get("version"),
            status: row.get("status"),
            description: row.get("description"),
            guidance: row.get("guidance"),
            eid_prefix: row.get("eid_prefix"),
            features: row.get("features"),
            definition,
            stages: row.get("stages"),
            constraints: row.get("constraints"),
            etl: row.get("etl"),
            changes: row.get("changes"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct FormUpdate {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
    pub version: i32,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: Option<Value>,
    pub definition: Option<Value>,
    pub stages: Option<Value>,
    pub constraints: Option<Value>,
    pub etl: Option<Value>,
    pub changes: Option<Value>,
    pub permissions: Option<Value>,
}

impl Default for FormUpdate {
    fn default() -> Self {
        let features: HashMap<String, Value> = HashMap::new();
        let definition: HashMap<String, String> = HashMap::new();
        let stages: Vec<String> = Vec::new();
        let constraints: Vec<String> = Vec::new();
        FormUpdate {
            uuid: Uuid::new_v4(),
            name: json::to_value(false).unwrap(),
            status: "ACTIVE".to_string(),
            version: 1,
            description: Option::None,
            guidance: Option::None,
            eid_prefix: Option::None,
            features: Some(json::to_value(features.clone()).unwrap()),
            definition: Some(json::to_value(definition.clone()).unwrap()),
            stages: Some(json::to_value(stages.clone()).unwrap()),
            constraints: Some(json::to_value(constraints.clone()).unwrap()),
            etl: Some(json::to_value(features.clone()).unwrap()),
            changes: Some(json::to_value(stages.clone()).unwrap()),
            permissions: Some(json::to_value(features.clone()).unwrap()),
        }
    }
}


#[derive(Debug, Deserialize)]
pub struct FormCreate {
    pub name: Value,
    pub status: String,
    pub version: i32,
    pub description: Option<String>,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: Option<Value>,
    pub definition: Option<Value>,
    pub stages: Option<Value>,
    pub constraints: Option<Value>,
    pub etl: Option<Value>,
    pub changes: Option<Value>,
    pub permissions: Option<Value>,
}

#[derive(Debug, Serialize)]
pub struct FormSummary {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
}   

impl<'a> From<Row<'a>> for FormSummary {
    fn from(row: Row) -> Self {
        FormSummary {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
        }
    }
}
