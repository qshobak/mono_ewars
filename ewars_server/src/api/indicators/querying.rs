use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::api::utils::*;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::share::{Query, QueryResult};

use crate::api::indicators::{Indicator};
use crate::api::indicator_groups::{IndicatorGroup};

static SQL_QUERY_INDICATORS: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.indicators AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_INDICATORS: &'static str = r#"
    SELECT COUNT(r.uuid) AS total
    FROM {SCHEMA}.indicators AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;


static SQL_GET_INDICATOR: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.indicators AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;


// Get a single indicator
pub fn get_indicator(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let ind_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Indicator> = match &conn.query(&SQL_GET_INDICATOR.replace("{SCHEMA}", &tki), &[&ind_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Indicator::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(indicator) = result {
        Ok(json::to_value(indicator).unwrap())
    } else {
        Ok(json::to_value(false).unwrap())
    }
}

// Query Indicators from the system
pub fn query_indicators(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_INDICATORS, SQL_COUNT_INDICATORS, tki, Indicator)
}


static SQL_GET_IND_GROUPS_ROOT: &'static str = r#"
    SELECT * FROM {SCHEMA}.indicator_groups WHERE pid IS NULL;
"#;

static SQL_GET_GROUP_CHILDS: &'static str = r#"
    SELECT * FROM {SCHEMA}.indicator_groups WHERE pid = $1;
"#;

static SQL_GET_INDS_ROOT: &'static str = r#"
    SELECT * FROM {SCHEMA}.indicators WHERE group_id IS NULL;
"#;

static SQL_GET_IND_CHILDS: &'static str = r#"
    SELECT * FROM {SCHEMA}.indicators WHERE group_id = $1;
"#;

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub enum TreeResults {
    IndicatorItem(Indicator),
    Group(IndicatorGroup),
}

pub fn tree_query(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let pid: Option<Uuid> = json::from_value(msg.args.clone()).unwrap();

    let mut groups: Vec<IndicatorGroup> = Vec::new();
    let mut indicators: Vec<Indicator> = Vec::new();

    if let Some(c) = pid {
        groups = match &conn.query(&SQL_GET_GROUP_CHILDS.replace("{SCHEMA}", &tki), &[&c]) {
            Ok(rows) => {
                rows.iter().map(|x| IndicatorGroup::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

        indicators = match &conn.query(&SQL_GET_IND_CHILDS.replace("{SCHEMA}", &tki), &[&c]) {
            Ok(rows) => {
                rows.iter().map(|x| Indicator::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

    } else {
        groups = match &conn.query(&SQL_GET_IND_GROUPS_ROOT.replace("{SCHEMA}", &tki), &[]) {
            Ok(rows) => {
                rows.iter().map(|x| IndicatorGroup::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };


        indicators = match &conn.query(&SQL_GET_INDS_ROOT.replace("{SCHEMA}", &tki), &[]) {
            Ok(rows) => {
                rows.iter().map(|x| Indicator::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };
    }

    let mut items: Vec<TreeResults> = Vec::new();

    for item in groups {
        items.push(TreeResults::Group(item));
    }

    for item in indicators {
        items.push(TreeResults::IndicatorItem(item));
    }

    let result: Value = json::to_value(items).unwrap();

    Ok(result)
}
