use std::io;

use serde_json as json;
use serde_json::Value;

use futures::Future;

use crate::db::db::DbExecutor;
use crate::handlers::auth_handler::GetAuthUser;

use actix_web::{Error, HttpRequest, HttpResponse, Json, State, FutureResponse, Responder, AsyncResponder};
use actix_web::middleware::identity::RequestIdentity;
use crate::state::AppState;

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthRequest {
    pub email: String,
    pub password: String,
}

pub fn web_auth((item, req): (Json<AuthRequest>, HttpRequest<AppState>)) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    req.state()
        .db
        .send(GetAuthUser {
            email: item.email.clone(),
            password: item.password.clone(),
            domain: host.clone(),
        })
        .from_err()
        .and_then(move |res| {
            match res {
                Ok(auth_result) => {
                    if let Some(ref user) = auth_result.user {
                        eprintln!("{:?}", user);
                        req.remember(json::to_string(user).unwrap());
                    }

                    Ok(HttpResponse::Ok().json(auth_result))
                },
                Err(_) => Ok(HttpResponse::InternalServerError().into())
            }
        })
        .responder()
}


