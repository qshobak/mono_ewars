use serde_json as json;
use serde_json::Value;
use failure::Error;

use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::api::tasks::{Task};

static SQL_GET_TASK: &'static str = r#"
    SELECT * FROM {SCHEMA}.tasks WHERE uuid = $1;
"#;


pub fn get_task(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Task> = match &conn.query(&SQL_GET_TASK.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Task::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if result.is_none() {
        bail!("ERR_NOT_FOUND");
    }


    let result = result.unwrap();

    Ok(json::to_value(result).unwrap())
}


static SQL_GET_ADMIN_TASKS: &'static str = r#"
    SELECT * FROM {SCHEMA}.tasks
    WHERE status = 'OPEN';
"#;

pub fn get_tasks(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let role: String = msg.user.role.clone();

    let results: Vec<Task> = match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            match &conn.query(&SQL_GET_ADMIN_TASKS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    rows.iter().map(|x| Task::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        },
        _ => {
            Vec::new()
        }
    };

    let result = json::to_value(results).unwrap();

    Ok(result)
}
