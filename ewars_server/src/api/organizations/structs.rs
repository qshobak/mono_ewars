use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Organization {
    pub uuid: Uuid,
    pub name: Value,
    pub description: Option<String>,
    pub acronym: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator: Option<String>,
    pub modifier: Option<String>,
}

impl<'a> From<Row<'a>> for Organization {
    fn from(row: Row) -> Self {
        Organization {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            acronym: row.get("acronym"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: match row.get_opt("creator") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            modifier: match row.get_opt("modifier") {
                Some(res) => res.unwrap(),
                None => Option::None
            }

        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct OrganizationUpdate {
    pub uuid: Uuid,
    pub name: Value,
    pub description: Option<String>,
    pub acronym: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct OrganizationCreate {
    pub name: Value,
    pub description: Option<String>,
    pub acronym: Option<String>,
}

