use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use failure::Error;

use crate::api::utils::*;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::share::{Query, QueryResult};
use crate::api::alarms::{Alarm};

static SQL_QUERY_ALARMS: &'static str  = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.alarms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_ALARMS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.alarms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_GET_ALARM: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.alarms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn query_alarms(conn: PoolConnection,tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone())?;

    query_fn!(conn, query, SQL_QUERY_ALARMS, SQL_COUNT_ALARMS, tki, Alarm)
}

// Get a specific alarm
pub fn get_alarm(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone())?;

    let result: Option<Alarm> = match &conn.query(&SQL_GET_ALARM.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Alarm::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}

static SQL_GET_ACTIVE_ALARMS: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.alarms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.status = 'ACTIVE';
"#;

pub fn get_active_alarms(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<Alarm> = match &conn.query(&SQL_GET_ACTIVE_ALARMS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Alarm::from(x)).collect()
        },
        Err(err) => {
            bail!("{:?}", err);
            Vec::new()
        }
    };


    Ok(json::to_value(results).unwrap())
}
