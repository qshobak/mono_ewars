use serde_json as json;
use serde_json::Value;

use failure::Error;
use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::share::{Query, QueryResult};

use crate::api::utils::*;

use crate::api::invites::{Invite};

static SQL_QUERY_INVITES: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.invites AS r
"#;

static SQL_COUNT_INVITES: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.invites as r
"#;


pub fn query_invites(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_INVITES, SQL_COUNT_INVITES, tki, Invite)
}
