use serde_json::Value;

use chrono::prelude::*;
use postgres::rows::Row;
use uuid::Uuid;

#[derive(Debug, Serialize, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
    pub pcode: String,
    pub codes: Option<Value>,
    pub groups: Option<Vec<String>>,
    pub data: Option<Value>,
    pub lti: Uuid,
    pub pid: Option<Uuid>,
    pub lineage: Vec<Uuid>,
    pub organizations: Option<Vec<Uuid>>,
    pub image: Option<Vec<u8>>,
    pub geometry_type: Option<String>,
    pub geojson: Option<Value>,
    pub population: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator_name: Option<String>,
    pub creator_email: Option<String>,
    pub modifier_name: Option<String>,
    pub modifier_email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lti_name: Option<Value>,
}

impl<'a> From<Row<'a>> for Location {
    fn from(row: Row) -> Self {
        Location {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            pcode: row.get("pcode"),
            codes: row.get("codes"),
            groups: row.get("groups"),
            data: row.get("data"),
            lti: row.get("lti"),
            pid: row.get("pid"),
            lineage: row.get("lineage"),
            organizations: row.get("organizations"),
            image: row.get("image"),
            geometry_type: row.get("geometry_type"),
            geojson: row.get("geojson"),
            population: row.get("population"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator_name: row.get("creator_name"),
            creator_email: row.get("creator_email"),
            modifier_name: row.get("modifier_name"),
            modifier_email: row.get("modifier_email"),
            lti_name: match row.get_opt("lti_name") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationUpdate {
    pub uuid: Option<Uuid>,
    pub name: Value,
    pub status: String,
    pub pcode: String,
    pub codes: Option<Value>,
    pub groups: Option<Vec<String>>,
    pub data: Option<Value>,
    pub lti: Uuid,
    pub pid: Option<Uuid>,
    pub organizations: Option<Vec<Uuid>>,
    pub image: Option<Vec<u8>>,
    pub geometry_type: Option<String>,
    pub geojson: Option<Value>,
    pub population: Option<Value>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationMerge {
    pub source: Uuid,
    pub target: Uuid,
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationMove {
    pub uuid: Uuid,
    pub new_parent: Uuid,
}
