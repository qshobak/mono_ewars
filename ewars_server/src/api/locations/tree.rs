use serde_json as json;
use serde_json::Value;

use failure::Error;
use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

static SQL_GET_WITH_PARENT: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.geometry_type, l.status, l.pid, l.lineage,
    lt.name AS lti_name,
    (SELECT COUNT(ll.uuid) FROM {SCHEMA}.locations AS ll WHERE ll.pid = l.uuid AND ll.status != 'DELETED') AS children
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.location_types AS lt ON lt.uuid = l.lti
    WHERE l.status != 'DELETED'
    AND l.pid = $1;
"#;

static SQL_GET_ROOTS: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.geometry_type, l.status, l.pid, l.lineage,
    lt.name AS lti_name,
    (SELECT COUNT(ll.uuid) FROM {SCHEMA}.locations AS ll WHERE ll.pid = l.uuid AND ll.status != 'DELETED') AS children
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.location_types AS lt ON lt.uuid = l.lti
    WHERE l.status != 'DELETED'
        AND l.pid IS NULL;
"#;

#[derive(Debug, Deserialize)]
struct LocationTreeQuery {
    pub parent_id: Option<Uuid>,
    pub show_disabled: Option<bool>,
    pub search: Option<String>,
    pub lti: Option<Uuid>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Location {
    uuid: Uuid,
    name: Value,
    lti: Uuid,
    geometry_type: String,
    status: String,
    pid: Option<Uuid>,
    lineage: Vec<Uuid>,
    lti_name: Option<Value>,
    child_count: i64,
}

impl<'a> From<Row<'a>> for Location {
    fn from(row: Row) -> Self {
        Location {
            uuid: row.get("uuid"),
            name: row.get("name"),
            lti: row.get("lti"),
            geometry_type: row.get("geometry_type"),
            status: row.get("status"),
            pid: row.get("pid"),
            lineage: row.get("lineage"),
            lti_name: row.get("lti_name"),
            child_count: row.get("children"),
        }
    }
}

// Proxies to one of the other functions based on whether a parent id is specified or not
pub fn query_locations_tree(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let qr: LocationTreeQuery = json::from_value(msg.args.clone()).unwrap();

    if qr.parent_id.is_none() {
        get_root_locations(conn, tki)
    } else {
        get_child_locations(conn, tki, qr.parent_id.unwrap())
    }
}

fn get_root_locations(conn: PoolConnection, tki: String) -> Result<Value, Error> {
    let results: Vec<Location> = match &conn.query(&SQL_GET_ROOTS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}

fn get_child_locations(conn: PoolConnection, tki: String, locid: Uuid) -> Result<Value, Error> {
    let results: Vec<Location> = match &conn.query(&SQL_GET_WITH_PARENT.replace("{SCHEMA}", &tki), &[&locid]) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}
