use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::share::{Query, QueryResult};
use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::locations::structs::{Location};
use crate::models::{LocationDigest};

use crate::api::utils::*;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT l.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        lti.name AS ltu_name
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = l.created_by
        LEFT JOIN {SCHEMA}.users AS uc on uc.uuid = l.modified_by
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
    WHERE l.uuid = $1;
"#;

static SQL_GET_ROOT_LOCATIONS: &'static str = r#"
    SELECT l.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        lti.name AS lti_name
    FROM {SCHEMAS}.locations AS l
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = l.created_by
        LEFT JOIN {SCHENA}.users AS uc ON uc.uuid = l.modified_by
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.uuid
    WHERE l.pid IS NULL;
"#;

static SQL_GET_CHILD_LOCATIONS: &'static str = r#"
    SELECT l.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        lti.name AS lti_name
    FROM {SCHEMAS}.locations AS l
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = l.created_by
        LEFT JOIN {SCHENA}.users AS uc ON uc.uuid = l.modified_by
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.uuid
    WHERE l.pid = $1;
"#;

pub fn get_location(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let locid: Uuid = json::from_value(msg.args.clone()).unwrap();
    let result: Option<Location> = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&locid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Location::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}

pub fn get_root_locations(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
   let locations: Vec<Location> = match &conn.query(&SQL_GET_ROOT_LOCATIONS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(locations).unwrap())
}

pub fn get_location_children(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let loc_id: Uuid = json::from_value(msg.args.clone()).unwrap();
    let locations: Vec<Location> = match &conn.query(&SQL_GET_CHILD_LOCATIONS.replace("{SCHEMA}", &tki), &[&loc_id]) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(locations).unwrap())
}

static SQL_QUERY_LOCATIONS: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.locations AS r
"#;

static SQL_COUNT_LOCATIONS: &'static str = r#"
    SELECT COUNT(r.uuid) AS total
    FROM {SCHEMA}.locations AS r
"#;

pub fn query_locations(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_LOCATIONS, SQL_COUNT_LOCATIONS, tki, Location)
}

// Get a smaller sized bit of info about a location
pub fn get_location_digest(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match LocationDigest::from_id(&conn, &tki, &id) {
        Some(res) => {
            Ok(json::to_value(res).unwrap())
        },
        None => {
            bail!("ERR_LOC_DIGEST");
        }
    }
}
