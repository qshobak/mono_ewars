use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};


static SQL_GET_GROUPS: &'static str = r#"
    SELECT groups 
    FROM {SCHEMA}.locations
    WHERE array_length(groups, 1) > 0;
"#;


pub fn get_location_groups(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<Vec<String>> = match &conn.query(&SQL_GET_GROUPS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => rows.iter().map(|x| x.get(0)).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    let mut items: Vec<String> = Vec::new();

    for item in results {
        for node in item {
            if !items.contains(&node) {
                items.push(node.clone());
            }
        }
    }

    Ok(json::to_value(items).unwrap())
}
