mod structs;

pub use self::structs::{Outbreak, OutbreakUpdate, OutbreakCreate};
