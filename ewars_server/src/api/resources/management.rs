use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use chrono::prelude::*;
use postgres::rows::{Row};

use uuid::Uuid;

use crate::api::resources::{Resource};

static SQL_DELETE_RESOURCE: &'static str = r#"
    DELETE FROM {SCHEMA}.resources WHERE uuid = $1;
"#;

pub fn delete_resource(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: bool = match &conn.execute(&SQL_DELETE_RESOURCE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_DELETING_RESOURCE");
    }
}

static SQL_UPDATE_RESOURCE: &'static str = r#"
    UPDATE {SCHEMA}.resources
        SET name = $1,
            status = $2,
            description = $3,
            shared = $4,
            layout = $5,
            data = $6,
            style = $7,
            variables = $8,
            permissions = $9,
            modified = NOW(),
            modified_by = $10
    WHERE uuid = $11;
"#;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ResourceUpdate {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub shared: Option<bool>,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<Value>,
}

pub fn update_resource(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: ResourceUpdate = json::from_value(msg.args.clone()).unwrap();

    let result: bool = match &conn.execute(&SQL_UPDATE_RESOURCE.replace("{SCHEMA}", &tki), &[
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };


    if result {
        Ok(json::to_value(true).unwrap())
    } else { 
        bail!("ERR_UPDATING_RESOURCE");
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewResource {
    pub name: String,
    pub description: Option<String>,
    pub status: Option<String>,
    pub shared: Option<bool>,
    pub content_type: String,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<Value>,
}

static SQL_CREATE_RESOURCE: &'static str = r#"
    INSERT INTO {SCHEMA}.resources
    (name, status, version, description, content_type, shared, layout, data, style, variables, permissions, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7,  $8, $9, $10, $11, $12, $13) RETURNING *;
"#;

pub fn create_resource(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: NewResource = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Resource> = get_single_from_args!(conn, SQL_CREATE_RESOURCE, Resource, tki, &[
        &data.name,
        &data.status, 
        &1,
        &data.description,
        &data.content_type,
        &data.shared,
        &data.layout,
        &data.data,
        &data.style,
        &data.variables,
        &data.permissions,
        &msg.user.uuid,
        &msg.user.uuid,
    ]);

    if let Some(c) = result {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_CREATING_RESOURCE");
    }
}
