use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Deserialize)]
pub struct LayerOptions {
    pub groups: Vec<String>,
    pub location_types: Vec<LocationType>,
    pub forms: Vec<Form>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationType {
    pub uuid: Uuid,
    pub name: Value,
}

impl<'a> From<Row<'a>> for LocationType {
    fn from(row: Row) -> Self {
        LocationType {
            uuid: row.get(0),
            name: row.get(1),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Form {
    pub uuid: Uuid,
    pub name: Value,
    pub definition: Value,
}

impl<'a> From<Row<'a>> for Form {
    fn from(row: Row) -> Self {
        Form {
            uuid: row.get(0),
            name: row.get(1),
            definition: row.get(2),
        }
    }
}

static SQL_GET_GROUPS: &'static str = r#"
    SELECT groups
    FROM {SCHEMA}.locations
    WHERE groups != NULL
        AND array_length(groups, 1) > 0;
"#;

static SQL_GET_LOC_TYPES: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.location_types;
"#;

static SQL_GET_FORMS: &'static str = r#"
    SELECT uuid, name, definition
    FROM {SCHEMA}.forms
    WHERE status = 'ACTIVE';
"#;

pub fn get_map_layer_options(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let groups: Vec<Vec<String>> = match &conn.query(&SQL_GET_GROUPS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };
    let mut end_groups: Vec<String> = Vec::new();

    for item in groups {
        for group in item {
            if !end_groups.contains(&group) {
                end_groups.push(group.clone());
            }
        }
    }

    let location_types: Vec<LocationType> = match &conn.query(&SQL_GET_LOC_TYPES.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| LocationType::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let forms: Vec<Form> = match &conn.query(&SQL_GET_FORMS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Form::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    let result = LayerOptions {
        groups: end_groups,
        location_types,
        forms,
    };

    Ok(json::to_value(result).unwrap())
}
