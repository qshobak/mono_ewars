use serde_json as json;
use serde_json::Value;

use failure::Error;
use crate::postgres::rows::{Row};
use crate::uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_GET_GEOMS: &'static str = r#"
    SELECT uuid, name, geom_type, geom
    FROM {SCHEMA}.locations
    WHERE lti = $1
"#;

#[derive(Debug, Serialize, Clone)]
pub struct GeomData {
    pub uuid: Uuid,
    pub name: Value,
    pub geom_type: String,
    pub geom: Option<Value>,
}

impl<'a> From<Row<'a>> for GeomData {
    fn from(row: Row) -> Self {
        GeomData {
            uuid: row.get(0),
            name: row.get(1),
            geom_type: row.get(2),
            geom: row.get(3),
        }
    }
}

pub fn get_type_geoms(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();


    let results: Vec<GeomData> = match &conn.query(&SQL_GET_GEOMS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| GeomData::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    Ok(json::to_value(results).unwrap())
}

