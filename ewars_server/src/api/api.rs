use std::collections::HashMap;
use serde_json as json;
use serde_json::Value;

use failure::Error;

use std::rc::Rc;
use std::cell::RefCell;

pub fn default_handler(req: &HttpRequest<State>) -> Result<Value, String> {
    Ok(json::to_value(true).unwrap())
}

macro_rules! add_method {
    ($m: expr, $name: expr, $handler: expr) => {
        {
            $m.insert($name, $handler as fn(req: &HttpRequest<State>) -> Result<Value, String>);
        }
    }
}

lazy_static! {
    static ref API_METHODS: HashMap<&'static str, fn(req: &HttpRequest<State>) -> Result<Value, String>> = {
        let mut m = HashMap::new();

        add_method!(m, "com.sonoma.add_accounts", default_handler);

        m
    }
}

#[derive(Debug, Message)]
pub struct CommandResult {
    pub id: String,
    pub d: Value,
}

#[derive(Debug, Message)]
pub struct CommandError {
    pub id: String,
    pub description: String,
}

pub fn handler(state: &HttpRequest<State>, id: String, cmd: String, args: Value) -> Result<CommandResult, CommandError> {
    let args = args.clone();

    let u_cmd: String = cmd.to_string();

    let s_cmd: &str = &u_cmd[..];

    if !API_METHODS.contains_key(s_cmd) {
        let result = Err(CommandError {
            description: "method not found".to_owned(),
            id: id,
        });
        result
    } else {
        let result = match API_METHODS.get(s_cmd) {
            Some(t_cmd) => {
                t_cmd(state.clone(), args)
            }
            None => {
                return Err(CommandError {
                    description: "Method not found".to_string(),
                    id: id,
                });
            }
        };

        match result {
            Ok(res) => {
                Ok(CommandResult {
                    id: id,
                    d: res,
                })
            }
            Err(err) => {
                eprintln!("{:?}", err);
                Err(CommandError {
                    id: id,
                    description: format!("{:?}", err),
                })
            }
        }
    }
}
