use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};


#[derive(Debug, Deserialize, Serialize)]
pub struct Assignment {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Option<Uuid>,
    pub assign_type: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub user_name: Option<String>,
    pub user_email: Option<String>,
    pub form_name: Option<Value>,
    pub location_name: Option<Value>,
    pub creator_name: Option<String>,
    pub creator_email: Option<String>,
    pub modifier_name: Option<String>,
    pub modifier_email: Option<String>,

}

impl<'a> From<Row<'a>> for Assignment {
    fn from(row: Row) -> Self {
        Assignment {
            uuid: row.get("uuid"),
            uid: row.get("uid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            assign_type: row.get("assign_type"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            user_name: row.get("user_name"),
            user_email: row.get("user_email"),
            form_name: row.get("form_name"),
            location_name: row.get("location_name"),
            creator_name: row.get("creator_name"),
            creator_email: row.get("creator_email"),
            modifier_name: row.get("modifier_name"),
            modifier_email: row.get("modifier_email"),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct AssignmentUpdate {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Option<Uuid>,
    pub assign_type: String,
}


#[derive(Debug, Deserialize)]
pub struct AssignmentCreate {
    pub uid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Option<Uuid>,
    pub assign_type: String,
}
