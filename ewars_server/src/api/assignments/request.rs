use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use failure::Error;
use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::models::{Task};

#[derive(Debug, Deserialize)]
struct AssignmentRequest {
    pub form_id: Uuid,
    pub location_id: Option<Uuid>,
}



static SQL_CHECK_ASSIGNS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.assignments AS r
    WHERE r.uid = $1
    AND r.fid = $2;
"#;


#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct AssignmentRequestData {
    pub form_id: Uuid,
    pub uid: Uuid,
    pub form_name: Value,
    pub user_name: String,
    pub user_email: String,
    pub location_id: Option<Uuid>,
    pub location_name: Option<Value>,
}


#[derive(Debug, Serialize)]
pub struct AssignmentContext {
    pub lid: Option<Uuid>,
    pub fid: Uuid,
    pub roles: Vec<String>,
}

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO {SCHEMA}.tasks
    (status, task_type, data, version, form, context, roles, location, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING uuid;
"#;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name 
    FROM {SCHEMA}.locations
    WHERE uuid = $1;
"#;

static SQL_GET_FORM: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.forms
    WHERE uuid = $1;
"#;

pub fn request_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let req: AssignmentRequest = json::from_value(msg.args.clone()).unwrap();

    // TODO: Check if the new assignment overlaps with an existing one
    // TODO: Create the assignment approval task
    // TODO: Send out the notification email that a user has requested an assignment
    //
    //

    let uid: Uuid = msg.user.uuid.clone();

    let mut count_sql: String = SQL_CHECK_ASSIGNS.to_string();
    let mut counter: u32 = 0;
    if req.location_id.is_some() {
        count_sql += " AND r.lid = $3";

        let location_id: Uuid = req.location_id.unwrap();
        counter = match &conn.query(&count_sql.replace("{SCHEMA}", &tki), &[&uid, &req.form_id, &location_id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    row.get(0)
                } else {
                    0
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                0
            }
        };
    } else {
        counter = match &conn.query(&SQL_CHECK_ASSIGNS.replace("{SCHEMA}", &tki), &[&uid, &req.form_id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    row.get(0)
                } else {
                    0
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                0
            }
        };
    }

    if counter > 0 {
        bail!("ERR_EXISTING_ASSIGN");
    }

    let form: Option<(Uuid, Value)> = match &conn.query(&SQL_GET_FORM.replace("{SCHEMA}", &tki), &[&req.form_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some((row.get(0), row.get(1)))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if form.is_none() {
        bail!("ERR_RETR_FORM");
    }
    let form: (Uuid, Value) = form.unwrap();

    let mut location: Option<(Uuid, Value)> = Option::None;
    let mut location_name: Option<Value> = Option::None;
    if let Some(c) = req.location_id {
        location = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&req.location_id.unwrap()]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some((row.get(0), row.get(1)))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        if let Some(c) = &location {
            location_name = Some(c.1.clone());
        } else {
            bail!("ERR_UNK_LOCATION");
        }
    }

    let task_data: Value = json::to_value(AssignmentRequestData {
        form_id: req.form_id,
        uid: uid.clone(),
        form_name: form.1.clone(),
        user_name: msg.user.name.clone(),
        user_email: msg.user.email.clone(),
        location_name: location_name,
        location_id: req.location_id.clone(),
    }).unwrap();

    let task_context = json::to_value(AssignmentContext {
        lid: req.location_id.clone(),
        fid: req.form_id.clone(),
        roles: vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
    }).unwrap();

    let result: bool = match Task::create_task(&conn, &tki, &task_context, &task_data, &"ASSIGNMENT_REQUEST") {
        Ok(_) => true,
        Err(_) => false
    };
    // Create the task in the system

    // There was an error creating the task
    if !result {
        bail!("ERR_CREATING_TASK");
    }

    Ok(json::to_value(true).unwrap())
}

static SQL_CREATE_ASSIGNMENT: &'static str = r#"
    INSERT INTO {SCHEMA}.assignments
    (uid, status, fid, lid, assign_type, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING uuid;
"#;

static SQL_GET_TASK: &'static str = r#"
    SELECT uuid, data
    FROM {SCHEMA}.tasks
    WHERE uuid = $1;
"#;

static SQL_CLOSE_TASK: &'static str = r#"
    UPDATE {SCHEMA}.tasks
    SET status = 'CLOSED',
        outcome = $1
    WHERE uuid = $2;
"#;


pub fn approve_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();


    let task: Option<(Uuid, AssignmentRequestData)> = match &conn.query(&SQL_GET_TASK.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                let task_data: AssignmentRequestData = json::from_value(row.get(1)).unwrap();
                Some((row.get(0), task_data))
            } else {
                Option::None
            } 
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if task.is_none() {
        bail!("ERR_UNK_TASK");
    }


    let _ = &conn.execute(&SQL_CLOSE_TASK.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    // Create the actual assignment
    let data: AssignmentRequestData = task.unwrap().1.clone();
    let mut outcome: HashMap<String, String> = HashMap::new();
    outcome.insert("action".to_string(), "APPROVED".to_string());
    let outcome_val: Value = json::to_value(outcome).unwrap();
    let result: bool = match &conn.query(&SQL_CREATE_ASSIGNMENT.replace("{SCHEMA}", &tki), &[
        &data.uid,
        &"ACTIVE",
        &data.form_id,
        &data.location_id,
        &"DEFAULT",
        &msg.user.uuid,
        &msg.user.uuid,
        &outcome_val,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_CREATING_ASSIGNMENT");
    }

}


#[derive(Debug, Deserialize)]
pub struct AssignmentRejection {
    pub uuid: Uuid,
    pub reason: Option<String>,
}

static SQL_UPDATE_TASK_REJECT: &'static str = r#"
    UPDATE {SCHEMA}.tasks
    SET status = 'CLOSED',
        outcome = $1
    WHERE uuid = $2;
"#;

pub fn reject_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AssignmentRejection = json::from_value(msg.args.clone()).unwrap();


    let mut outcome: HashMap<String, String> = HashMap::new();
    outcome.insert("ACTION".to_string(), "REJECTED".to_string());
    if let Some(c) = data.reason {
        outcome.insert("REASON".to_string(), c.clone());
    }
    let outcome_val: Value = json::to_value(outcome).unwrap();

    let result: bool = match &conn.execute(&SQL_UPDATE_TASK_REJECT.replace("{SCHEMA}", &tki), &[
        &outcome_val,
        &data.uuid,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };


    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_UPDATING_TASK");
    }
}

