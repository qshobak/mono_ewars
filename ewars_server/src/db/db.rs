use uuid;
use actix::prelude::*;
use r2d2;
use r2d2_postgres;

use r2d2_postgres::{PostgresConnectionManager};

pub type PgPool = r2d2::Pool<PostgresConnectionManager>;
pub type Connection = r2d2::PooledConnection<PostgresConnectionManager>;

pub struct DbExecutor(pub PgPool);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        eprintln!("DB Service: Stopping");
        Running::Stop
    }

    fn started(&mut self, ctx: &mut Self::Context) {
        eprintln!("DB Service: Starting...");
    }
}

impl Supervised for DbExecutor {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("DB Service: Restarting...");
    }
}
