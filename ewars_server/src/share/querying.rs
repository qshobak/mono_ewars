use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Query {
    pub filters: HashMap<String, (String, Value)>,
    pub orders: HashMap<String, (String, String)>,
    pub limit: i32,
    pub offset: i32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordQuery {
    pub filters: HashMap<String, (String, Value)>,
    pub orders: HashMap<String, (String, String)>,
    pub limit: i32,
    pub offset: i32,
    pub fid: Uuid,
    pub param: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct QueryResult {
    pub results: Value,
    pub count: i64
}
