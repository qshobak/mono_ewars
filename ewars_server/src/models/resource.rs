use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;

pub struct Resource {
    pub uuid: String,
    pub name: LanguageString,
    pub status: String,
    pub shared: bool,
    pub description: LanguageString,
    pub content_type: String,
    pub layout: Value,
    pub data: Value,
    pub style: Value,
    pub veriables: Value,
    pub permissions: Value,
    pub created: DateTime<Utc>,
    pub created_by: String,
    pub modified: DateTime<Utc>,
    pub modified_by: String,
}