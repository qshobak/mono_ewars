use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserSession {
    pub uuid: Uuid,
    pub name: String,
    pub email: String,
    pub role: String,
    pub tki: String,
    pub account_name: String,
    pub aid: Uuid,
}
