use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection};

pub struct Organization {
    pub uuid: Uuid,
    pub name: Value,
    pub description: Option<String>,
    pub acronym: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Organization {
    fn from(row: Row) -> Self {
        Organization {
            uuid: row.get(0),
            name: row.get(1),
            description: row.get(2),
            acronym: row.get(3),
            created: row.get(4),
            created_by: row.get(5),
            modified: row.get(6),
            modified_by: row.get(7),
        }
    }
}

static SQL_CREATE_ORG: &'static str = r#"
    INSERT INTO {SCHEMA}.organizations 
    (name, created_by, modified_by)
    VALUES ($1, $2, $3) RETURNING *;
"#;

impl Organization {
    pub fn create_new(conn: &PoolConnection, tki: &String, name: &String, acronym: &String, uid: &Uuid) -> Option<Organization> {
        let mut s_name: HashMap<&str, &str> = HashMap::new();
        s_name.insert("en", &name);
        let s_name: Value = json::to_value(s_name).unwrap();

        match &conn.query(&SQL_CREATE_ORG.replace("{SCHEMA}", &tki), &[
           &name,
           &uid,
           &uid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(Organization::from(row))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("Error creating organization: {:?}", err);
                Option::None
            }
        }
    }
}
