use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

pub struct WorkflowItem {
    pub uuid: String,
    pub created: DateTime<Utc>,
    pub stage: String,
    pub data: Value,
    pub actioned_by: String,
}

pub struct AlertRevision {
    pub uuid: String,
    pub revised: DateTime<Utc>,
    pub revisor: String,
    pub changes: Value,
    pub reason: Option<String>,
}

pub struct Alert {
    pub uuid: String,
    pub eid: String,
    pub alid: String,
    pub period_start: String,
    pub period_end: String,
    pub lid: Option<String>,
    pub status: String,
    pub stage: String,

    pub workflow: Vec<WorkflowItem>,
    pub data: HashMap<&'static str, Value>,
    pub revisions: Vec<AlertRevision>,

    pub raised: DateTime<Utc>,
    pub closed: DateTime<Utc>,
    pub closed_by: String,

    pub modified: DateTime<Utc>,
    pub modified_by: String,
}

