use serde_json::Value;

use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthUser {
    pub id: i32,
    pub uuid: String,
    pub name: String,
    pub email: String,
    pub password: String,
    pub accounts: Vec<i32>,
    pub registered: DateTime<Utc>,
    pub last_modified: DateTime<Utc>,
}