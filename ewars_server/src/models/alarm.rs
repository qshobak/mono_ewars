use serde_json as json;
use serde_json::Value;

use crate::models::form::{Field, Stage};

pub type Permission = HashMap<&'static str, bool>;

pub struct Alarm {
    pub uuid: String,
    pub name: LanguageString,
    pub status: String,
    pub description: LanguageString,
    pub guidance: LanguageString,
    pub definition: HashMap<&'static str, Field>,
    pub stages: HashMap<&'static str, Stage>,
    pub permissions: HashMap<&'static str, Permission>,
    pub created: DateTime<Utc>,
    pub created_by: String,
    pub modified: DateTime<Utc>,
    pub modified_by: String,
}
