macro_rules! query_single {
    ($conn: expr, $sql: expr, $model: ident, $args: expr) => {
        { 
            match &$conn.query(&$sql, $args) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        Some($model::from(row))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}
