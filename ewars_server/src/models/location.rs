use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::models::language_string::LanguageString;
use crate::handlers::api_handler::{PoolConnection};

pub struct Location {
    pub uuid: Uuid,
    pub name: LanguageString,
    pub location_type: Option<String>,
    pub pcode: Option<String>,
    pub description: Option<String>,
    pub status: String,

}

#[derive(Debug, Serialize, Clone)]
pub struct LocationDigest {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
    pub groups: Option<Vec<String>>,
    pub lti_name: Option<Value>,
}


impl<'a> From<Row<'a>> for LocationDigest {
    fn from(row: Row) -> Self {
        LocationDigest {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            groups: row.get(3),
            lti_name: row.get(4),
        }
    }
}

static SQL_GET_LOC_DIGEST: &'static str = r#"
    SELECT l.uuid, l.name, l.status, l.groups, lt.name
    FROM {SCHEMA}.locations AS l 
    LEFT JOIN {SCHEMA}.location_types AS lt ON lt.uuid = l.lti
    WHERE l.uuid = $1;
"#;

impl LocationDigest {
    pub fn from_id(conn: &PoolConnection, tki: &String, id: &Uuid) -> Option<Self> {
        match &conn.query(&SQL_GET_LOC_DIGEST.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(LocationDigest::from(row))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        }
    }
}


static SQL_GET_LOC_NAME: &'static str = r#"
    SELECT l.name->>'en',
        (SELECT array(
            SELECT name->>'en' FROM {SCHEMA}.locations AS t
            WHERE t.uuid::TEXT = ANY(l.lineage::TEXT[])
            ORDER BY array_length(t.lineage, 1)
        )) AS location_lineage
    FROM {SCHEMA}.locations AS l
    WHERE l.uuid = $1;

"#;

impl Location {
    pub fn get_location_name(conn: &PoolConnection, tki: &String, id: &Uuid) -> Result<(String, String), ()> {
        match &conn.query(&SQL_GET_LOC_NAME.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    let lineage: Vec<String> = row.get(1);
                    let lineage_name: String = lineage.join(" \\ ").to_string();
                    Ok((row.get(0), lineage_name))
                } else {
                    Err(())
                }
            },
            Err(err) => {
                Err(())
            }
        }
    }
}
