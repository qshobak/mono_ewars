use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection};

use crate::models::Form;

static SQL_GET_UNDERS: &'static str = r#"
    SELECT l.uuid, l.name
    FROM {SCHEMA}.locations AS l
    WHERE l.lineage @> ARRAY[$1]::UUID[]
        AND l.lti = $2;
"#;


static SQL_GET_GROUP_LOCATIONS: &'static str = r#"
    SELECT l.uuid, l.name
    FROM {SCHEMA}.locations AS l
    WHERE l.groups @> ARRAY[$1]::TEXT[]
        AND l.lti = $2;
"#;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Assignment {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub status: String,
    pub fid: Option<Uuid>,
    pub lid: Option<Uuid>,
    pub assign_group: Option<String>,
    pub assign_type: String, pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub location_name: Option<Value>,
    pub form_name: Option<Value>,
    pub creator: Option<String>,
    pub modifier: Option<String>,
}

macro_rules! extractor {
    ($field_name: expr, $row: expr) => {
        {
            match $row.get_opt($field_name) {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

impl<'a> From<Row<'a>> for Assignment {
    fn from(row: Row) -> Self {
        Assignment {
            uuid: row.get("uuid"),
            uid: row.get("uid"),
            status: row.get("status"),
            fid: row.get("fid"),
            lid: row.get("lid"),
            assign_group: row.get("assign_group"),
            assign_type: row.get("assign_type"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            location_name: extractor!("location_name", row),
            form_name: extractor!("form_name", row),
            creator: extractor!("creator", row),
            modifier: extractor!("modifier", row),
        }
    }
}

impl Assignment {

    // Whether this location is assignment-based
    pub fn has_location(&self) -> bool {
        if self.lid.is_some() {
            return true;
        }

        if &self.assign_type == "GROUP" {
            return true;
        }

        return false;
    }

    pub fn fid(&self) -> Uuid {
        if let Some(c) = self.fid {
            c.clone()
        } else {
            panic!("Could not retrieve form_id");
        }
    }

    // Get the derived locations from an assignment
    pub fn get_derived_locations(&self, conn: &PoolConnection, tki: &String) -> Vec<(Uuid, Value)> {
        let assign_type: String = self.assign_type.clone();

        let form: Form = Form::get_form_by_id(&conn, &tki, &self.fid()).unwrap();

        // It's assumed we've already checked that this form is location reporting 
        // so we can just unwrap
        let location_type: Option<Uuid> = form.get_reporting_location_type();

        if location_type.is_none() {
            return Vec::new();
        }

        let mut locations: Vec<(Uuid, Value)> = Vec::new();
        match assign_type.as_ref() {
            "DEFAULT" => {
                locations.push((self.lid.clone().unwrap(), self.location_name.clone().unwrap()));
            },
            "UNDER" => {
                let lid: Uuid = self.lid.unwrap();
                locations = match &conn.query(&SQL_GET_UNDERS.replace("{SCHEMA}", &tki), &[&vec![&lid], &location_type]) {
                    Ok(rows) => {
                        rows.iter().map(|x| (x.get(0), x.get(1))).collect()
                    },
                    Err(err) => {
                        eprintln!("{:?}", err);
                        Vec::new()
                    }
                };
            },
            "GROUP" => {
                let groups: String = self.assign_group.clone().unwrap();
                locations = match &conn.query(&SQL_GET_GROUP_LOCATIONS.replace("{SCHEMA}", &tki), &[&vec![&groups], &location_type]) {
                    Ok(rows) => {
                        rows.iter().map(|x| (x.get(0), x.get(1))).collect()
                    },
                    Err(err) => {
                        eprintln!("{:?}", err);
                        Vec::new()
                    }
                };
            },
            _ => {}
        }

        locations.sort_by_key(|x| x.0.clone());
        locations.dedup_by_key(|x| x.0.clone());

        locations
    }   

    pub fn get_derived_locations_admin(&self, conn: &PoolConnection) -> Vec<(Uuid, Value)> {
        unimplemented!()
    }
}
