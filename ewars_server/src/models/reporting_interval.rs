use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;

pub struct ReportingInterval {
    pub uuid: String,
    pub name: LanguageString,
    pub status: String,
    pub description: LanguageString,
    pub interval_type: String,
    pub formatting: Option<String>,
    pub data: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: String,
    pub modified: DateTime<Utc>,
    pub modified_by: String,
}

impl ReportingInterval {
    pub fn extrapolate_dates(&self, year: &i32) -> Vec<DateTime<Utc>> {
        let items: Vec<DateTime<Utc>> = Vec::new();

        items
    }
}