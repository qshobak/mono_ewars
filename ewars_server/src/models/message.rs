use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;

pub struct Message {
    pub uuid: String,
    pub uid: String,
    pub created: DateTime<Utc>,
    pub modified: Option<DateTime<Utc>>,
    pub content: HashMap<String, Value>,
    pub ref_type: String,
    pub ref_id: String,
    pub parent_id: Option<String>,
}

