use std::collections::HashMap;
use futures::future;
use futures::Future;

use serde_json as json;
use serde_json::Value;
use tera;
use uuid::Uuid;

use actix::prelude::*;
use actix_web::{Json, AsyncResponder, HttpResponse, Error, FutureResponse, error, HttpRequest};

use crate::services::{EmailService};
use crate::state::AppState;
use crate::app::{RegistrationRequest};
use crate::handlers::command_handler::{DbCommand, CommandResult};
use crate::app::{Org};


#[derive(Debug, Deserialize, Clone)]
pub struct Payload {
    pub name: String,
    pub email: String,
    pub password: String,
    pub org_id: Option<Org>,
    pub org_name: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct RegistrationResult {
    pub success: bool,
    pub error: Option<String>,
}

// User registration handler
pub fn handle_registration((body, req): (String, HttpRequest<AppState>)) -> FutureResponse<HttpResponse> {
    let payload: Value = json::from_str(&body).unwrap();
    let mut domain = req.connection_info().host().to_string();
    if domain.contains(":") {
        let parts: Vec<String> = domain.split(":").map(|x| x.to_string()).collect();
        domain = parts[0].clone();
    }

    let data: Payload = match json::from_value(payload) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            // TODO: Udpate this to a JSON response
            return Box::new(future::ok(HttpResponse::NotFound().into()));
        }
    };

    let r = req.clone();
    let email_svc: Addr<EmailService> = r.state().email_svc.clone();
    req.state().db.send(DbCommand::RegRequest(RegistrationRequest {
        email: data.email,
        password: data.password,
        domain: domain.clone(),
        name: data.name.clone(),
        org_id: data.org_id,
        org_name: data.org_name,
        email_svc,
    })).from_err()
    .and_then(move |res| {
        let mut result: bool = false;
        let mut error_code: Option<String> = Option::None;
        match res.unwrap() {
            CommandResult::RegistrationResult(reg_res, code) => {
                if reg_res {
                    result = true;
                } else {
                    result = false;
                    error_code = Some(code.clone());
                }
            },
            _ => {
                result = false;
                error_code = Some("UNK_ERROR".to_string());
            }
        }

        if result {
            let resp: Value = json::to_value(RegistrationResult {
                success: true,
                error: Option::None,
            }).unwrap();

            Ok(HttpResponse::Ok().json(resp).into())
        } else {
            let resp: Value = json::to_value(RegistrationResult {
                success: false,
                error: Option::None,
            }).unwrap();

            Ok(HttpResponse::Ok().json(resp).into())
        }

    }).responder()
}

