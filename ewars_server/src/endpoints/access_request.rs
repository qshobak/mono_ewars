use std::collections::HashMap;
use futures::future;
use futures::Future;

use serde_json as json;
use serde_json::Value;
use tera;
use uuid::Uuid;

use actix::prelude::*;
use actix_web::{Json, AsyncResponder, HttpResponse, Error, FutureResponse, error, HttpRequest};

use crate::handlers::command_handler::{DbCommand, CommandResult};
use crate::services::{EmailService};
use crate::state::AppState;
use crate::app::{AccessRequest};

#[derive(Debug, Serialize, Clone)]
pub struct AccessResult {
    pub success: bool,
    pub error: Option<String>,
}

#[derive(Debug, Deserialize)]
struct Payload {
    pub email: String,
    pub password: String,
    pub org_id: Option<Uuid>,
    pub org_name: Option<String>,
}

// User registration handler
pub fn handle_access_request((body, req): (String, HttpRequest<AppState>)) -> FutureResponse<HttpResponse> {
    let payload: Value = json::from_str(&body).unwrap();
    let mut domain = req.connection_info().host().to_string();
    if domain.contains(":") {
        let parts: Vec<String> = domain.split(":").map(|x| x.to_string()).collect();
        domain = parts[0].clone();
    }

    let data: Payload = match json::from_value(payload) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            // Return a JSON response
            return Box::new(
                future::ok(HttpResponse::Ok()
                            .json(AccessResult {
                                success: false,
                                error: Some("ERR_BAD_PAYLOAD".to_string()),
                            }).into()
                            )
                );

        }
    };

    let r = req.clone();
    let email_svc: Addr<EmailService> = r.state().email_svc.clone();
    req.state().db.send(DbCommand::AccRequest(AccessRequest {
        email: data.email.clone(),
        password: data.password.clone(),
        domain: domain.clone(),
        org_id: data.org_id.clone(),
        org_name: data.org_name.clone(),
        email_svc,
    })).from_err()
    .and_then(move |res| {
        let mut result: bool = false;
        let mut error_code: Option<String> = Option::None;
        match res.unwrap() {
            CommandResult::AccessResult(acc_res, code) => {
                if acc_res {
                    result = true;
                } else {
                    result = false;
                    error_code = Some(code.clone());
                }
            },
            _ => {
                result = false;
                error_code = Some("UNK_ERROR".to_string());
            }
        }

        if result {
            let resp: Value = json::to_value(AccessResult {
                success: true,
                error: Option::None,
            }).unwrap();

            Ok(HttpResponse::Ok().json(resp).into())
        } else {
            let resp: Value = json::to_value(AccessResult {
                success: false,
                error: Option::None,
            }).unwrap();

            Ok(HttpResponse::Ok().json(resp).into())
        }

    }).responder()
}

