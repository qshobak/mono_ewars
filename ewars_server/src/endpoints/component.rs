use std::collections::HashMap;

use tera;
use serde_json as json;
use serde_json::Value;

use actix_web::{Error, HttpRequest, HttpResponse, error};
use actix_web::http::{header, StatusCode};
use actix_web::middleware::identity::RequestIdentity;

use crate::models::{UserSession};
use crate::state::AppState;

#[derive(Debug, Clone, Serialize)]
pub struct ComponentItem {
    pub id: &'static str,
    pub name: &'static str,
    pub description: &'static str,
    pub icon: &'static str,
    pub cat: Option<&'static str>,
    pub admin: bool,
}

impl ComponentItem {
    pub fn is_analysis(&self) -> bool {
        if let Some(ref cat) = self.cat {
            let result = match cat.as_ref() {
                "ANALYSIS" => true,
                _ => false
            };
            result
        } else {
            false
        }
    }

    pub fn is_reporting(&self) -> bool {
        if let Some(ref cat) = self.cat {
            let result = match cat.as_ref() {
                "REPORTING" => true,
                _ => false
            };
            result
        } else {
            false
        }
    }

    pub fn is_admin(&self) -> bool {
        if let Some(ref cat) = self.cat {
            let result = match cat.as_ref() {
                "ADMIN" => true,
                _ => false
            };
            result
        } else {
            false
        }
    }

    pub fn is_auditing(&self) -> bool {
        if let Some(ref cat) = self.cat {
            let result = match cat.as_ref() {
                "AUDITING" => true,
                _ => false
            };
            result
        } else {
            false
        }
    }
}

lazy_static! {
    static ref APPS: HashMap<&'static str, ComponentItem> = {
        let mut m = HashMap::new();

        // System Apps
        m.insert("dashboard", ComponentItem {
            id: "dashboard",
            name: "Dashboard",
            description: "",
            icon: "tachometer",
            cat: None,
            admin: false,
        });

        m.insert("reporting", ComponentItem {
            id: "reporting",
            name: "Report Manager",
            description: "Perform routine reporting",
            icon: "clipboard",
            cat: Some("REPORTING"),
            admin: false,
        });

        m.insert("alerts", ComponentItem {
            id: "alerts",
            name: "Alert Log",
            description: "View and track alerts occurring within the system",
            icon: "bell",
            cat: Some("REPORTING"),
            admin: false
        });

        m.insert("mapping", ComponentItem {
            id: "mapping",
            name: "Mapping (Beta)",
            description: "Explore data through a map interface",
            icon: "map",
            cat: Some("ANALYSIS"),
            admin: false,
        });

        m.insert("documents", ComponentItem {
            id: "documents",
            name: "Documents",
            description: "Access reportins and information",
            icon: "book",
            cat: Some("ANALYSIS"),
            admin: false,
        });

        m.insert("export", ComponentItem {
            id: "export",
            name: "Export",
            description: "Export data for use in other systems",
            icon: "download",
            cat: Some("ANALYSIS"),
            admin: false,
        });

        m.insert("plot", ComponentItem {
            id: "plot",
            name: "Plot",
            description: "Create interactive visualizations",
            icon: "chart-line",
            cat: Some("ANALYSIS"),
            admin: false,
        });

        m.insert("analysis", ComponentItem {
            id: "analysis",
            name: "Notebooks",
            description: "Organize complex analysis",
            icon: "book",
            cat: Some("ANALYSIS"),
            admin: false,
        });

        m.insert("outbreaks", ComponentItem {
            id: "outbreaks",
            name: "Outbreaks",
            description: "Access and manage outbreaks",
            icon: "bell",
            cat: Some("ADMIN"),
            admin: true,
        });


        m.insert("auditor", ComponentItem {
            id: "auditor",
            name: "M&E Auditor",
            description: "Review key indicators of system performance",
            icon: "briefcase",
            cat: Some("AUDITING"),
            admin: false,
        });

        m.insert("import", ComponentItem {
            id: "import",
            name: "Data Import",
            description: "Import data into forms",
            icon: "upload",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("users", ComponentItem {
            id: "users",
            name: "Users",
            description: "Manage users and permissions",
            icon: "users",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("locations", ComponentItem {
            id: "locations",
            name: "Locations",
            description: "Manage locations, types and more",
            icon: "map-marker",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("templates", ComponentItem {
            id: "templates",
            name: "Document Templates",
            description: "Manage/design documents",
            icon: "book",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("report_manager", ComponentItem {
            id: "report_manager",
            name: "Submissions",
            description: "Browse and manage form records",
            icon: "clipboard",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("forms", ComponentItem {
            id: "forms",
            name: "Forms",
            description: "Manage/Configure your data collection forms",
            icon: "clipboard",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("dashboards", ComponentItem {
            id: "dashboards",
            name: "Dashboards",
            description: "Edit/Manage dashboards displayed on main screen",
            icon: "tachometer",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("alarms", ComponentItem {
            id: "alarms",
            name: "Alarms",
            description: "Configure/Manage alert criteria and workflows",
            icon: "bell",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("indicators", ComponentItem {
            id: "indicators",
            name: "Indicators",
            description: "Define indicators for analysis",
            icon: "code-branch",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("forms_", ComponentItem {
            id: "forms",
            name: "Forms",
            description: "Forms manager",
            icon: "clipboard",
            cat: Some("ADMIN"),
            admin: true,
        });

        m.insert("inbox", ComponentItem {
            id: "inbox",
            name: "Inbox",
            description: "Manage tasks and notifications",
            icon: "inbox",
            cat: Option::None,
            admin: false,
        });

        m.insert("profile", ComponentItem {
            id: "profile",
            name: "Profile",
            description: "Manage your settings and configuration",
            icon: "fa-user",
            cat: Option::None,
            admin: false,
        });

        m
    };
}


// Get the menu organized
pub fn get_menu_map(role: String) -> HashMap<&'static str, Vec<ComponentItem>> {
    let mut results: HashMap<&'static str, Vec<ComponentItem>> = HashMap::new();

    results.insert("REPORTING", APPS.values().filter(|x| x.is_reporting()).cloned().collect::<Vec<ComponentItem>>());
    results.insert("ANALYSIS", APPS.values().filter(|x| x.is_analysis()).cloned().collect::<Vec<ComponentItem>>());
    results.insert("AUDITING", APPS.values().filter(|x| x.is_auditing()).cloned().collect::<Vec<ComponentItem>>());

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            results.insert("ADMIN", APPS.values().filter(|x| x.is_admin()).cloned().collect::<Vec<ComponentItem>>());
        },
        _ => {}
    };

    results
}

pub fn get_menu(role: String) -> Vec<(&'static str, Vec<ComponentItem>)> {
    let mut results: Vec<(&'static str, Vec<ComponentItem>)> = vec![
        ("REPORTING", APPS.values().filter(|x| x.is_reporting()).cloned().collect::<Vec<ComponentItem>>()),
        ("ANALYSIS", APPS.values().filter(|x| x.is_analysis()).cloned().collect::<Vec<ComponentItem>>()),
        ("AUDITING", APPS.values().filter(|x| x.is_auditing()).cloned().collect::<Vec<ComponentItem>>())
    ];

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            results.push(("ADMIN", APPS.values().filter(|x| x.is_admin()).cloned().collect::<Vec<ComponentItem>>()))
        }
        _ => {}
    };

    results
}


pub fn app_route(req: &HttpRequest<AppState>) -> HttpResponse {
    let app_name = req.match_info().get("app_name").unwrap_or("none");
    let page_title = "";

    let mut user_session: Option<UserSession> = None;
    let mut end_role: UserSession;
    let mut tki: String = String::new();
    if let Some(ref session_user) = req.identity() {
        let session_user: UserSession = json::from_str(&session_user).unwrap();
        user_session = Some(UserSession {
            uuid: session_user.uuid.clone(),
            aid: session_user.aid.clone(),
            name: session_user.name.clone(),
            email: session_user.email.clone(),
            role: session_user.role,
            tki: session_user.tki.clone(),
            account_name: session_user.account_name.clone(),
        });
    }
    let mut ctx = tera::Context::new();

    // If there's no user session, disallow access
    if user_session.is_none() {
        let s = req.state().template
            .render("404.html", &ctx)
            .map_err(|err| {
                eprintln!("{:?}",err);
                error::ErrorInternalServerError("TEMPLATE_ERROR")
            }).unwrap();

        return HttpResponse::Ok()
            .content_type("text/html")
            .body(s);
    } else {
        end_role = user_session.unwrap();
    }

    let mut app: Option<ComponentItem> = None;
    let mut end_app: ComponentItem;
    if let Some(cmp) = APPS.get(&app_name) {
        app = Some(cmp.clone());
    }

    if app.is_none() {
        let s = req.state().template
            .render("404.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

        return HttpResponse::Ok()
            .content_type("text/html")
            .body(s);
    } else {
        end_app = app.unwrap();
    }

    let menu: HashMap<&'static str, Vec<ComponentItem>> = get_menu_map(end_role.role.clone());
    let is_admin: bool = match end_role.role.as_ref() {
        "ACCOUNT_ADMIN" => true,
        _ => false
    };


    // Add context items
    ctx.insert("app_name", &app_name.to_owned());
    ctx.insert("page_title", &end_app.name.to_owned());
    ctx.insert("menu", &menu);
    ctx.insert("is_admin", &is_admin);
    ctx.insert("user", &end_role);

    if end_app.admin {
        ctx.insert("theme", "admin");
    } else {
        ctx.insert("theme", "flex");
    }

    // Check if the user has access to this component
    // If they don't have access to the component then we need to make sure we
    // don't leak information about what components we host
    let mut can_see: bool = true;
    match end_app.cat {
        Some(res) => {
            match res.as_ref() {
                "ADMIN" => {
                    if end_role.role != "ACCOUNT_ADMIN" {
                        can_see = false;
                    }
                }
                _ => {}
            }
        }
        None => {}
    };

    // The active user can't see this component so we need to 404 them
    // Make sure that we don't even let on that a component exists
    eprintln!("{:?}", ctx);
    if !can_see {
        let s = req.state().template
            .render("404.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

        return HttpResponse::Ok()
            .content_type("text/html")
            .body(s);
    }

    let s = req.state().template
        .render("_app.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("TEMPLATE_ERROR")).unwrap();

    HttpResponse::Ok()
        .content_type("text/html")
        .body(s)
}
