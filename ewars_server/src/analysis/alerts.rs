use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::api_handler::{PoolConnection};
use crate::analysis::structs::{AnalysisQuery, IndicatorDefinition, Indicator};

pub type QueryResult = HashMap<NaiveDate, f64>;

static SQL_GET_ALERTS: &'static str = r#"
    SELECT r.alert_date
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
"#;

pub fn query_alerts(conn: &PoolConnection, locid: &Option<Uuid>, tki: &String, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let ind_def: Indicator = match query.indicator.clone().unwrap() {
        IndicatorDefinition::SystemIndicator(c) => c,
        _ => {
            return Err(json::to_value("NON_SYS_IND").unwrap());
        }
    };
    let metric: String = ind_def.dimension.unwrap_or("ALERTS_RAISED".to_string());

    let mut wheres: Vec<String> = Vec::new();

    // Id an alarm id is specific
    if let Some(alid) = ind_def.alarm_id {
        wheres.push(format!("r.alid::TEXT = '{}'", alid.to_string()));
    }

    if let Some(lid) = locid {
        wheres.push(format!("l.lineage::TEXT[] @> ARRAY['{}']::TEXT[]", lid.to_string()));
    }

    match metric.as_ref() {
        "ALERTS_OPEN" => {
            wheres.push("r.status = 'OPEN'".to_string());
        },
        "ALERTS_CLOSED" => {
            wheres.push("r.status = ANY(ARRAY['CLOSED', 'AUTODISCARDED'])".to_string());
        },
        "ALERTS_CLOSED_NON_AUTO" => {
            wheres.push("r.status = 'CLOSED'".to_string());
        },
        "ALERTS_AUTO_DISCARDED" => {
            wheres.push("r.status = 'AUTODISCARDED'".to_string());
        },
        "ALERTS_DISCARDED" => {
            wheres.push("(r.data->>'outcome' = 'DISCARDED' OR r.data->>'verification_outcome' = 'DISCARDED')".to_string());
        },
        "ALERTS_MONITORED" => {
            wheres.push("r.data->>'outcome' = 'MONITOR'".to_string());
        },
        "ALERTS_RESPOND" => {
            wheres.push("r.data->>'outcome' = 'RESPOND'".to_string());
        },
        "ALERTS_IN_VERIFICATION" => {
            wheres.push("r.stage = 'VERIFICATION'".to_string());
            wheres.push("r.status = 'OPEN'".to_string());
        },
        "ALERTS_AWAIT_VERIFICATION" => {
            wheres.push("r.stage = 'VERIFICATION'".to_string());
            wheres.push("r.status = 'OPEN'".to_string());
        },
        "ALERTS_VERIFIED" => {
            wheres.push("r.stage = ANY(ARRAY['RISK_ASSESS', 'RISK_CHAR', 'OUTCOME'])".to_string());
        },
        "ALERTS_DISCARDED_VERIFICATION" => {
            wheres.push("r.data->>'verification_outcome' = 'DISCARD'".to_string());
        },
        "ALERTS_MONITORED_OUTCOME" => {
            wheres.push("r.data->>'outcome' = 'MONITOR'".to_string());
        },
        "ALERTS_IN_RISK_ASSESS" => {
            wheres.push("r.stage = 'RISK_ASSESS'".to_string());
        },
        "ALERTS_AWAIT_RISK_ASSESS" => {
            wheres.push("r.stage = 'RISK_ASSESS'".to_string());
        },
        "ALERTS_RISK_ASSESSED" => {
            wheres.push("r.stage = ANY(ARRAY['RISK_CHAR', 'OUTCOME'])".to_string());
        },
        "ALERTS_IN_RISK_CHAR" => {
            wheres.push("r.stage = 'RISK_CHAR'".to_string());
        },
        "ALERTS_AWAIT_RISK_CHAR" => {
            wheres.push("r.stage = 'RISK_CHAR'".to_string());
        },
        "ALERTS_RISK_CHAR" => {
            wheres.push("r.stage = ANY(ARRAY['OUTCOME'])".to_string());
        },
        "ALERTS_IN_OUTCOME" => {
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        "ALERTS_AWAIT_OUTCOME" => {
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        "ALERTS_DISCARD_OUTCOME" => {
            wheres.push("r.data->>'outcome' = ANY(ARRAY['DISCARDED', 'DISCARD'])".to_string());
        },
        "ALERTS_RESPOND_OUTCOME" => {
            wheres.push("r.data->>'outcome' = ANY(ARRAY['RESPONSE', 'RESPOND'])".to_string());
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        "ALERTS_RISK_LOW" => {
            wheres.push("r.data->>'risk' ILIKE '%LOW%'".to_string());
            wheres.push("r.stage = ANY(ARRAY['OUTCOME'])".to_string());
        },
        "ALERTS_RISK_MODERATE" => {
            wheres.push("r.data->>'risk' ILIKE '%MODERATE%'".to_string());
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        "ALERTS_RISK_HIGH" => {
            wheres.push("r.data->>'risk' ILIKE '%HIGH%'".to_string());
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        "ALERTS_RISK_SEVERE" => {
            wheres.push("r.data->>'risk' ILIKE '%SEVERE%'".to_string());
            wheres.push("r.stage = 'OUTCOME'".to_string());
        },
        _ => {}
    }

    let mut sql: String = SQL_GET_ALERTS.replace("{SCHEMA}", &tki);

    if wheres.len() > 0 {
        let joined: String = wheres.join(" AND ");
        sql.push_str(" WHERE ");
        sql.push_str(&joined);
    }

    let records: Vec<NaiveDate> = match conn.query(&sql, &[]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut data: HashMap<NaiveDate, f64> = HashMap::new();

    for rec in &records {
        data.entry(rec.clone())
            .and_modify(|x| *x += 1.0)
            .or_insert(1.0);
    }


    Ok(data)
}
