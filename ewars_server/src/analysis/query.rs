use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use eval::{Expr, to_value};
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::analysis::structs::{AnalysisQuery, IndicatorDefinition, LocationDefinition, SeriesDefinition};
use crate::analysis::{get_data_series, DataPoint};
use crate::analysis::{query_alerts, query_submissions, query_locations, query_forms};
use crate::analysis::{get_map_ind, get_root_geom, get_map_location};

lazy_static! {
    static ref ALERTS_IND: Uuid = Uuid::parse_str("a18b5f1f-4775-4bc9-89cf-b0d83abab45a").unwrap();
    static ref LOCATIONS_IND: Uuid = Uuid::parse_str("470cc1fc-cfb3-49d9-8ac8-b1788b9c28f6").unwrap();
    static ref ASSIGNMENTS_IND: Uuid = Uuid::parse_str("59deb036-9857-401d-bec6-86b991071912").unwrap();
    static ref SUBMISSIONS_IND: Uuid = Uuid::parse_str("e51f4ca7-1678-44e3-b5cb-64f84555696f").unwrap();
    static ref DEVICES_IND: Uuid = Uuid::parse_str("ddfea0c3-a047-450f-9681-d5a992f2cdee").unwrap();
    static ref TASKS_IND: Uuid = Uuid::parse_str("b59c5be2-5e7a-4e74-9319-c2ce7d6adbf2").unwrap();
    static ref FORMS_IND: Uuid = Uuid::parse_str("08f844e7-62d2-4c8d-81dd-41b10808783c").unwrap();
    static ref USERS_IND: Uuid = Uuid::parse_str("ffec4451-b00f-466e-9935-2d9c060576f1").unwrap();
}

#[derive(Debug, Serialize, Clone)]
pub struct Resultant {
    pub data: f64,
}

// This function just proxies to the other functions below
pub fn handle_query(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: AnalysisQuery = match json::from_value(msg.args.clone()) {
        Ok(res) => res,
        Err(err) => {
            bail!(err);
        }
    };

    match query.query_type.as_ref() {
        "SLICE" => handle_slice(&conn, &tki, &query),
        "SLICE_COMPLEX" => handle_slice_complex(&conn, &tki, &query),
        "SERIES" => handle_series(&conn, &tki, &query),
        "SERIES_COMPLEX" => handle_series_complex(&conn, &tki, &query),
        "MAP_SLICE" => handle_map_slice(&conn, &tki, &query),
        "MAP_SLICE_COMPLEX" => default(),
        _ => default(),
    }
}

fn default() -> Result<Value, Error> {
    bail!("ERR");
}

#[derive(Debug, Serialize, Clone)]
pub struct MapNode {
    pub indicator: Option<MapIndicatorResult>,
    pub location: Option<MapLocationResult>,
    pub data: f64,
}

#[derive(Debug, Serialize, Clone)]
pub struct MapIndicatorResult {
    pub uuid: Uuid,
    pub name: String,
}

impl<'a> From<Row<'a>> for MapIndicatorResult {
    fn from(row: Row) -> Self {
        MapIndicatorResult {
            uuid: row.get(0),
            name: row.get(1),
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct MapLocationResult {
    pub uuid: Uuid,
    pub name: Value,
    pub pcode: Option<String>,
    pub geometry: Option<Value>,
    pub geometry_type: Option<String>,
    pub lineage: Option<Vec<Uuid>>,
}

#[derive(Debug, Serialize, Clone)]
pub struct MapResult {
    pub g: Option<Value>,
    pub d: Vec<MapNode>,
}

fn handle_map_slice(conn: &PoolConnection, tki: &String, query: &AnalysisQuery) -> Result<Value, Error> {
    let mut location: LocationDefinition;
    let mut indicator: IndicatorDefinition;

    if let Some(loc) = query.location.clone() {
        location = loc;
    } else {
        bail!("ERR_LOC_SPEC");
    }

    if let Some(ind) = query.indicator.clone() {
        indicator = ind;
    } else {
        bail!("ERR_IND");
    }

    let mut result: Value = json::to_value(false).unwrap();

    match location {
        LocationDefinition::LocationUuid(c) => {
            let result_location: MapLocationResult = get_map_location(&conn, &tki, &c).unwrap();

            let root_geom: Option<Value> = match get_root_geom(&conn, &tki, &c) {
                Ok(res) => Some(res),
                Err(err) => Option::None
            };

            let dp: HashMap<NaiveDate, f64> = match get_single_series(&conn, &tki, &c, &query) {
                Ok(res) => res,
                Err(err) => {
                    bail!(err);
                }
            };

            let reduction: String = query.reduction.clone().unwrap_or("SUM".to_string());
            let mut data: f64 = 0.0;

            let mut ind_info: Option<MapIndicatorResult> = Option::None;

            match indicator {
                IndicatorDefinition::IndicatorUuid(c) => {
                    ind_info = match get_map_ind(&conn, &tki, &c) {
                        Ok(res) => Some(res),
                        Err(_) => Option::None,
                    };
                },
                IndicatorDefinition::SystemIndicator(c) => {
                    ind_info = Some(MapIndicatorResult {
                        uuid: c.uuid.clone(),
                        name: "".to_string(),
                    });
                },
                _ => {}
            }

            match reduction.as_ref() {
                "SUM" => {
                    data = dp.values().sum();
                },
                _ => {
                    data = dp.values().sum();
                }
            }

            result = json::to_value(MapResult {
                g: root_geom,
                d: vec![
                    MapNode {
                        indicator: ind_info,
                        location: Some(result_location.clone()),
                        data,
                    }
                ],
            }).unwrap();


        },
        _ => {}
    }


    Ok(result)
}

fn get_single_series(conn: &PoolConnection, tki: &String, lid: &Uuid, query: &AnalysisQuery) -> Result<HashMap<NaiveDate, f64>, Error> {
    let indicator: IndicatorDefinition = query.indicator.clone().unwrap();

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    let mut start_date: NaiveDate;
    if let Some(dt) = query.start_date.clone() {
        start_date = NaiveDate::parse_from_str(&dt, "%Y-%m-%d").unwrap();
    } else {
        bail!("ERR_START_DATE");
    }

    let mut end_date: NaiveDate;
    if let Some(dt) = query.end_date.clone() {
        end_date = NaiveDate::parse_from_str(&dt, "%Y-%m-%d").unwrap();
    } else {
        bail!("ERR_END_DATE");
    }

    match indicator {
        IndicatorDefinition::IndicatorUuid(c) => {
            let dp: HashMap<NaiveDate, f64> = match get_data_series(&conn, &c, &lid, &tki, &query, &start_date, &end_date) {
                Ok(data) => data,
                Err(err) => {
                    bail!("ERR_UNK");
                }
            };

            result = dp.clone();
        },
        IndicatorDefinition::SystemIndicator(c) => {
            let ind_id: Uuid = c.uuid.clone();

            let mut dp: HashMap<NaiveDate, f64> = HashMap::new();

            if &ind_id == &*ALERTS_IND {
                result = query_alerts(&conn, &Some(lid.clone()), &tki, &query).expect("FAIL");
            } else if &ind_id == &*SUBMISSIONS_IND {
                result = match query_submissions(&conn, &lid, &tki, &query) {
                    Ok(res) => res,
                    Err(err) => {
                        bail!(err);
                    }
                };
            } else if &ind_id == &*LOCATIONS_IND {
                result = match query_locations(&conn, &lid, &tki, &query) {
                    Ok(res) => res,
                    Err(err) => {
                        bail!(err);
                    }
                };
            } else if &ind_id == &*FORMS_IND {
                result = match query_forms(&conn, &lid, &tki, &query) {
                    Ok(res) => res,
                    Err(err) => {
                        bail!(err);
                    }
                };
            } else {
                bail!("UNK_SYS_IND");
            }
            
        }
        _ => {}
    }

    Ok(result)
}

fn handle_slice(conn: &PoolConnection, tki: &String, query: &AnalysisQuery) -> Result<Value, Error> {

    let mut result: f64 = 0.0;

    let dp: HashMap<NaiveDate, f64> = match query.location.clone().unwrap() {
        LocationDefinition::LocationUuid(c) => {
            match get_single_series(&conn, &tki, &c, &query) {
                Ok(res) => res,
                Err(err) => {
                    bail!(err);
                }
            }
        },
        _ => {
            bail!("ERR_UNK_LOC_SPEC");
        }
    };


    let reduction: String = query.reduction.clone().unwrap_or("SUM".to_string());
    match reduction.as_ref() {
        "SUM" => {
            result = dp.values().sum();
        },
        _ => {}
    }

    let resultant = Resultant {
        data: result,
    };

    Ok(json::to_value(resultant).unwrap())
}

fn handle_slice_complex(conn: &PoolConnection, tki: &String, query: &AnalysisQuery) -> Result<Value, Error> {
    let series: Vec<SeriesDefinition> = query.series.clone().unwrap_or(Vec::new());

    // There are no variables specified
    if series.len() <= 0 {
        bail!("ERR_NO_VARS");
    }

    let mut formula: String = String::new();
    if let Some(fx) = query.formula.clone() {
        formula = fx.clone();
    } else {
        bail!("ERR_NO_FORMULA");
    }

    let mut data_series: HashMap<String, HashMap<NaiveDate, f64>> = HashMap::new();
    let mut data_series: HashMap<String, f64> = HashMap::new();

    for sii in &series {
        let mut new_query = query.clone();

        new_query.series = Option::None;
        new_query.indicator = sii.indicator.clone();
        new_query.reduction = sii.reduction.clone();
        new_query.query_type = "SLICE".to_string();

        let lid: Uuid = match new_query.location.clone().unwrap() {
            LocationDefinition::LocationUuid(c) => {
                c
            },
            _ => {
                bail!("ERR_UNK_LOCATION_SPEC");
            }
        };

        let dp: HashMap<NaiveDate, f64> = match get_single_series(&conn, &tki, &lid, &new_query) {
            Ok(res) => res,
            Err(err) => {
                bail!(err);
            }
        };
        let reduction: String = sii.reduction.clone().unwrap_or("SUM".to_string());
        let var_name: String = sii.variable_name.clone().unwrap();

        let mut var_result: f64 = 0.0;

        match reduction.as_ref() {
            "SUM" => {
                var_result = dp.values().sum();
            },
            _ => {
                var_result = dp.values().sum();
            }
        }


        data_series.insert(var_name.clone(), var_result);
    }

    let mut expr = Expr::new(formula.clone());

    for (key, val) in &data_series {
        expr = expr.value(key.clone(), to_value(&val));
    }

    let result: Value = match expr.exec() {
        Ok(res) => res,
        Err(err) => {
            bail!("ERR_PARSING_FX");
        }
    };

    let dp: f64 = match result.as_f64() {
        Some(res) => res,
        None => 0.0,
    };

    Ok(json::to_value(Resultant {
        data: dp,
    }).unwrap())
}

#[derive(Debug, Serialize, Clone)]
pub struct IndicatorResult {
    pub uuid: Uuid,
    pub name: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
pub struct LocationResult {
    pub uuid: Uuid,
    pub name: Option<Value>,
}

#[derive(Debug, Serialize, Clone)]
pub struct SeriesResult {
    pub data: Vec<(NaiveDate, f64)>,
    pub indicator: Option<IndicatorResult>,
    pub location: Option<LocationResult>,
}

fn handle_series(conn: &PoolConnection, tki: &String, query: &AnalysisQuery) -> Result<Value, Error> {
    let indicator: IndicatorDefinition = query.indicator.clone().unwrap();
    let location: LocationDefinition = query.location.clone().unwrap();

    let mut start_date: NaiveDate;
    if let Some(dt) = query.start_date.clone() {
        start_date = NaiveDate::parse_from_str(&dt, "%Y-%m-%d").unwrap();
    } else {
        bail!("ERR_START_DATE");
    }

    let mut end_date: NaiveDate;
    if let Some(dt) = query.end_date.clone() {
        end_date = NaiveDate::parse_from_str(&dt, "%Y-%m-%d").unwrap();
    } else {
        bail!("ERR_END_DATE");
    }

    // Will this query return multiple series?
    let mut multi_result: bool = false;

    let mut result: Value = json::to_value(false).unwrap();

    if query.is_single_ind() {
        if query.is_specific_location() {
            let mut series: Vec<(NaiveDate, f64)> = Vec::new();
            let ind: Uuid = query.get_ind_uuid().unwrap();
            let loc: Uuid = query.get_loc_uuid().unwrap();
            let dp: HashMap<NaiveDate, f64> = match get_data_series(&conn, &ind, &loc, &tki, &query, &start_date, &end_date) {
                Ok(data) => data,
                Err(err) => {
                    bail!(err);
                }
            };

            for (key, value) in &dp {
                series.push((key.clone(), value.clone()));
            }

            let ind = get_indicator(&conn, &tki, &ind).unwrap();
            let loc_def = get_location(&conn, &tki, &loc).unwrap();

            let series_result = SeriesResult {
                data: series.clone(),
                indicator: Some(ind.clone()),
                location: Some(loc_def.clone()),
            };

            result = json::to_value(series_result).unwrap();
        }
    }


    Ok(result)
}

fn handle_series_complex(conn: &PoolConnection, tki: &String, query: &AnalysisQuery) -> Result<Value, Error> {
    Ok(json::to_value(false).unwrap())
}

static SQL_GET_IND: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.indicators 
    WHERE uuid = $1;
"#;

fn get_indicator(conn: &PoolConnection, tki: &String, ind: &Uuid) -> Result<IndicatorResult, Error> {
    match conn.query(&SQL_GET_IND.replace("{SCHEMA}", &tki), &[&ind]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(IndicatorResult {
                    uuid: row.get(0),
                    name: row.get(1),
                })
            } else {
                bail!("ERR_UNK_IND");
            }
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.locations 
    WHERE uuid = $1;
"#;

fn get_location(conn: &PoolConnection, tki: &String, loc: &Uuid) -> Result<LocationResult, ()> {
    match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&loc]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(LocationResult {
                    uuid: row.get(0),
                    name: row.get(1),
                })
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

