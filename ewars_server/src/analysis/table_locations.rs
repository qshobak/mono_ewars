use serde_json as json;
use failure::Error;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use postgres::rows::{Row};
use uuid::Uuid;

#[derive(Debug, Deserialize, Clone)]
pub struct TableLocationQuery {
    pub parent: Option<Uuid>,
    pub status: Option<String>,
    pub sti: Option<Uuid>,
    pub tki: Option<String>,
    #[serde(rename="type")]
    pub query_type: String,
    pub lid: Option<Uuid>,
    pub groups: Option<Vec<String>>,
}

pub fn handle_table_loc_query(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let q: TableLocationQuery = json::from_value(msg.args.clone()).unwrap();

    match q.query_type.as_ref() {
        "GROUPS" => {
            get_grouped_locations(&conn, &tki, &q)
        },
        "SPECIFIC" => {
            get_specific_location(&conn, &tki, &q)
        },
        "GENERATOR" => {
            get_lti_locations(&conn, &tki, &q)
        },
        _ => {
            Ok(json::to_value("ERR_UNK_SPECIFICATION").unwrap())
        }
    }
}

static SQL_GET_GROUPED_LOCATIONS: &'static str = r#"
    SELECT l.uuid, 
        l.name->>'en' AS name,
        l.lineage,
        p.name->>'en' AS parent_name,
        l.pcode
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.locations As p ON p.uuid = l.pid
    WHERE l.status = 'ACTIVE'
        AND l.groups::TEXT[] @> $1::TEXT[];

"#;

#[derive(Debug, Serialize, Clone)]
pub struct TableLocation {
    pub uuid: Uuid,
    pub name: Option<String>,
    pub parent_name: Option<String>,
    pub lineage: Option<Vec<Uuid>>,
    pub pcode: Option<String>,
}

impl<'a> From<Row<'a>> for TableLocation {
    fn from(row: Row) -> Self {
        TableLocation {
            uuid: row.get("uuid"),
            name: row.get("name"),
            parent_name: row.get("parent_name"),
            lineage: match row.get_opt("lineage") {
                Some(res) => Some(res.unwrap()),
                None => Option::None,
            },
            pcode: row.get("pcode"),
        }
    }
}

fn get_grouped_locations(conn: &PoolConnection, tki: &String, query: &TableLocationQuery) -> Result<Value, Error> {
    let groups: Vec<String> = query.groups.clone().unwrap_or(Vec::new());
    let mut grouped: Vec<TableLocation> = match conn.query(&SQL_GET_GROUPED_LOCATIONS.replace("{SCHEMA}", &tki), &[&groups]) {
        Ok(rows) => {
            rows.iter().map(|x| TableLocation::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    grouped.dedup_by_key(|x| x.uuid);

    Ok(json::to_value(grouped).unwrap())
}

static SQL_GET_SPEC_LOCATIONS: &'static str = r#"
    SELECT 
        l.uuid,
        l.name->>'en' AS name,
        p.name->>'en' AS parent_name,
        l.pcode
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.locations AS p ON p.uuid = l.pid
    WHERE l.lti = $1
        AND l.status = ANY($2)
        AND l.lineage @> $3::UUID[];
"#;


fn get_lti_locations(conn: &PoolConnection, tki: &String, query: &TableLocationQuery) -> Result<Value, Error> {
    let status = query.status.clone().unwrap_or("ACTIVE".to_string());

    // Map out the statuses we want to retrieve
    let statuses: Vec<String> = match status.as_ref() {
        "ACTIVE" => vec!["ACTIVE".to_string()],
        "INACTIVE" => vec!["INACTIVE".to_string(), "DISABLED".to_string()],
        "DISABLED" => vec!["INACTIVE".to_string(), "DISABLED".to_string()],
        "ANY" => vec!["DISABLED".to_string(), "INACTIVE".to_string(), "ACTIVE".to_string()],
        _ => vec!["ACTIVE".to_string()]
    };

    let loc_h: Vec<Uuid> = vec![query.parent.unwrap()];
    let locs: Vec<TableLocation> = match conn.query(&SQL_GET_SPEC_LOCATIONS.replace("{SCHEMA}", &tki), &[
        &query.sti.unwrap(),
        &statuses,
        &loc_h,
    ]) {
        Ok(rows) => {
            rows.iter().map(|x| TableLocation::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    Ok(json::to_value(locs).unwrap())
}

static GET_SPEC_LOCATION: &'static str = r#"
    SELECT l.uuid,
        l.name->>'en' AS name,
        p.name->>'en' AS parent_name,
        l.pcode
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.locations AS p ON p.uuid = l.pid
    WHERE l.uuid = $1;
"#;

// Retrieve a specific location from the system
fn get_specific_location(conn: &PoolConnection, tki: &String, query: &TableLocationQuery) -> Result<Value, Error> {
    let results: Option<TableLocation> = match conn.query(&GET_SPEC_LOCATION.replace("{SCHEMA}", &tki), &[&query.lid.unwrap()]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(TableLocation::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    match results {
        Some(res) => {
            Ok(json::to_value(res).unwrap())
        },
        None => {
            bail!("ERR_UNK_LOCATION");
        }
    }
}
