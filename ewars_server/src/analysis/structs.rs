use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Indicator {
    pub uuid: Uuid,
    pub metric: Option<String>,
    pub dimension: Option<String>,
    pub site_type_id: Option<Uuid>,
    pub form_id: Option<Uuid>,
    pub alarm_id: Option<Uuid>,
    pub definition: Option<Value>,
    pub state: Option<String>,
    pub status: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum IndicatorDefinition {
    IndicatorUuid(Uuid),
    SystemIndicator(Indicator),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub metric: Option<String>,
    pub site_type_id: Option<i32>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationSpec {
    pub parent_id: Option<Uuid>,
    pub site_type_id: Option<i32>,
    pub status: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum LocationDefinition {
    LocationUuid(Uuid),
    Spec(LocationSpec),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SeriesDefinition {
    pub alarm_id: Option<IndicatorDefinition>,
    pub reduction: Option<String>,
    pub variable_name: Option<String>,
    pub colour: Option<String>,
    pub title: Option<Value>,
    #[serde(rename = "dataType")]
    pub data_type: Option<String>,
    pub indicator: Option<IndicatorDefinition>,
    pub location: Option<LocationDefinition>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AnalysisQuery {
    pub end_date: Option<String>,
    pub start_date: Option<String>,
    pub indicator: Option<IndicatorDefinition>,
    pub location: Option<LocationDefinition>,
    pub meta: Option<bool>,
    pub reduction: Option<String>,
    #[serde(rename="type")]
    pub query_type: String,
    pub interval: Option<String>,
    pub series: Option<Vec<SeriesDefinition>>,
    pub fill: Option<bool>,
    pub geometry: Option<bool>,
    pub formula: Option<String>,
    pub centroid: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IndicatorRow {
    pub uuid: Uuid,
    pub name: Value,
}

impl AnalysisQuery {
    pub fn should_meta(&self) -> bool {
        match self.meta {
            Some(res) => res,
            None => false
        }
    }

    pub fn is_single_ind(&self) -> bool {
        match self.indicator.clone() {
            Some(ind) => {
                match ind.clone() {
                    IndicatorDefinition::IndicatorUuid(c) => {
                        true
                    },
                    _ => false
                }
            },
            None => false
        }
    }

    pub fn is_specific_location(&self) -> bool {
        match self.location.clone() {
            Some(loc) => {
                match loc {
                    LocationDefinition::LocationUuid(c) => {
                        true
                    },
                    _ => false
                }
            },
            None => false
        }
    }

    pub fn get_ind_uuid(&self) -> Result<Uuid, ()> {
        match self.indicator.clone() {
            Some(ind) => {
                match ind {
                    IndicatorDefinition::IndicatorUuid(c) => {
                        Ok(c.clone())
                    },
                    _ => Err(())
                }
            },
            None => Err(())
        }
    }

    pub fn get_loc_uuid(&self) -> Result<Uuid, ()> {
        match self.location.clone() {
            Some(loc) => {
                match loc {
                    LocationDefinition::LocationUuid(c) => {
                        Ok(c.clone())
                    },
                    _ => Err(())
                }
            },
            None => Err(())
        }
    }

}
