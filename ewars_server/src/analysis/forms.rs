use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::analysis::structs::{AnalysisQuery, IndicatorDefinition, LocationDefinition, Indicator};
use crate::utils::{date_utils};
use crate::analysis::{query_locations};

pub type QueryResult = HashMap<NaiveDate, f64>;

pub fn query_forms(conn: &PoolConnection, locid: &Uuid, tki: &String, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let ind_def: Indicator = match query.indicator.clone().unwrap() {
        IndicatorDefinition::SystemIndicator(c) => c,
        _ => {
            return Err(json::to_value("NON_SYS_IND").unwrap());
        }
    };
    let metric: String = ind_def.metric.clone().unwrap_or("SUBMITTED".to_string());


    eprintln!("{}", metric);
    let result: Result<QueryResult, Value> = match metric.as_ref() {
        "REPORTING_LOCATIONS" => query_locations(&conn, &locid, &tki, &query),
        _ => {
            let err_string = format!("UNK_SUB_DIM: {}", metric);
            Err(json::to_value(err_string).unwrap())
        }
    };

    result
}

