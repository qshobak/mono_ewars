use std::env;
use config::{ConfigError, Config, File, Environment};

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Database {
    pub db_url: String,
    pub tls_mode: bool,
    pub echo: bool,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Theme {
    pub app_name: String
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Server {
    pub root_domain: String,
    pub port: i32,
    pub files_path: String,
    pub chromium_path: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Authentication {
    pub session_secret: String,
    pub token_secret: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Tls {
    pub enabled: bool,
    pub tls_certificate_file: String,
    pub tls_key_file: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Amazon {
    pub aws_secret_key: String,
    pub aws_access_key: String,
    pub aws_region: String,
}


#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Settings {
    pub debug: bool,
    pub database: Database,
    pub theme: Theme,
    pub server: Server,
    pub tls: Tls,
    pub amazon_aws: Amazon
}


impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();

        s.merge(File::with_name("config/default"))?;

        let env = env::var("RUN_MODE").unwrap_or("development".into());
        s.merge(File::with_name(&format!("config/{}", env)).required(false))?;

        s.merge(File::with_name("config/local").required(false))?;

        s.merge(Environment::with_prefix("app"))?;

        println!("debug: {:?}", s.get_bool("debug"));
        println!("database: {:?}", s.get::<String>("database.db_url"));

        s.try_into()
    }
}
