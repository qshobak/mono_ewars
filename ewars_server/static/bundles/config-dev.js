window.__ENAME__ = "config"; window.__EVERSION__ = "0.0.2";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 48);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(50), __esModule: true };

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(54);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(40);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(82);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(86);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(40);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 5 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 6 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 7 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(15);
var IE8_DOM_DEFINE = __webpack_require__(38);
var toPrimitive = __webpack_require__(23);
var dP = Object.defineProperty;

exports.f = __webpack_require__(9) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(17)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(6);
var core = __webpack_require__(5);
var ctx = __webpack_require__(37);
var hide = __webpack_require__(11);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(8);
var createDesc = __webpack_require__(19);
module.exports = __webpack_require__(9) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(63);
var defined = __webpack_require__(20);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(22)('wks');
var uid = __webpack_require__(18);
var Symbol = __webpack_require__(6).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    TEXT: 0,
    NUMERIC: 1,
    DATE: 2,
    LOCATION: 3,
    ALARM: 4,
    DATETIME: 5,
    TIME: 6,
    LOCATION_GROUP: 7,
    LOCATION_TYPE: 8,
    ROLE: 9,
    SECTION: 10,
    SELECT: 11,
    SWITCH: 12,
    USER: 13,
    DISPLAY: 14,
    ROW: 15,
    LNG_LAT: 16,
    GEOM_EDITOR: 17,
    COLOR: 18,
    CONDITIONAL: 19,
    FORM: 20,
    FORMS: 21,
    WORKFLOW: 22,
    IMAGE: 23,
    FILE: 24,
    OPTIONS: 25,
    CONDITIONS: 26,
    VALIDATIONS: 27,
    INDICATOR_GROUP: 28
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 18 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(22)('keys');
var uid = __webpack_require__(18);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(6);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(16);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(15);
var dPs = __webpack_require__(62);
var enumBugKeys = __webpack_require__(29);
var IE_PROTO = __webpack_require__(21)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(39)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(67).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(43);
var enumBugKeys = __webpack_require__(29);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 29 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(8).f;
var has = __webpack_require__(7);
var TAG = __webpack_require__(13)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(13);


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(6);
var core = __webpack_require__(5);
var LIBRARY = __webpack_require__(25);
var wksExt = __webpack_require__(31);
var defineProperty = __webpack_require__(8).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 33 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ComponentState = exports.state = undefined;

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TabInstance = function TabInstance(props) {
    (0, _classCallCheck3.default)(this, TabInstance);

    this.props = props;
};

var state = void 0;

var ComponentState = function ComponentState() {
    var _this = this;

    (0, _classCallCheck3.default)(this, ComponentState);

    this.addTab = function (tab) {
        var newId = ewars.utils.uuid();
        _this.tabs.push({
            _: newId,
            cmp: tab.n,
            n: tab.n,
            i: tab.i || null,
            _id: null
        });
        _this.activeTab = newId;

        ewars.emit('VIEW_UPDATED');
    };

    this.removeTab = function (tid) {
        _this.activeTab = 'DEFAULT';
        var rIndex = void 0;
        _this.tabs.forEach(function (item, index) {
            if (item._ == tid) rIndex = index;
        });
        _this.tabs.splice(rIndex, 1);
        ewars.emit('VIEW_UPDATED');
    };

    this.getActiveTab = function () {
        var tab = _this.tabs.filter(function (item) {
            return item._ == _this.activeTab;
        })[0] || null;

        return tab;
    };

    this.setActiveTab = function (tid) {
        _this.activeTab = tid;
        ewars.emit('VIEW_UPDATED');
    };

    this.setView = function (view) {
        _this.view = view;
        ewars.emit('VIEW_UPDATED');
    };

    exports.state = state = this;
    this.tabs = [];
    this.view = 'DEFAULT';
    this.dirty = false;
    this.activeTab = 'DEFAULT';
};

exports.default = ComponentState;
exports.state = state;
exports.ComponentState = ComponentState;

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(20);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(7);
var toObject = __webpack_require__(35);
var IE_PROTO = __webpack_require__(21)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(53);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(9) && !__webpack_require__(17)(function () {
  return Object.defineProperty(__webpack_require__(39)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(16);
var document = __webpack_require__(6).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(57);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(72);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(25);
var $export = __webpack_require__(10);
var redefine = __webpack_require__(42);
var hide = __webpack_require__(11);
var has = __webpack_require__(7);
var Iterators = __webpack_require__(26);
var $iterCreate = __webpack_require__(61);
var setToStringTag = __webpack_require__(30);
var getPrototypeOf = __webpack_require__(36);
var ITERATOR = __webpack_require__(13)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(7);
var toIObject = __webpack_require__(12);
var arrayIndexOf = __webpack_require__(64)(false);
var IE_PROTO = __webpack_require__(21)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 44 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 45 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(43);
var hiddenKeys = __webpack_require__(29).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(33);
var createDesc = __webpack_require__(19);
var toIObject = __webpack_require__(12);
var toPrimitive = __webpack_require__(23);
var has = __webpack_require__(7);
var IE8_DOM_DEFINE = __webpack_require__(38);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(9) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _view = __webpack_require__(49);

var _view2 = _interopRequireDefault(_view);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

ReactDOM.render(React.createElement(_view2.default, null), document.getElementById("application"));

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _cmp_state = __webpack_require__(34);

var _view = __webpack_require__(89);

var _view2 = _interopRequireDefault(_view);

var _tab = __webpack_require__(111);

var _tab2 = _interopRequireDefault(_tab);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TABS = {
    'Locations': _tab2.default
};

var ViewMain = function (_React$Component) {
    (0, _inherits3.default)(ViewMain, _React$Component);

    function ViewMain(props) {
        (0, _classCallCheck3.default)(this, ViewMain);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewMain.__proto__ || (0, _getPrototypeOf2.default)(ViewMain)).call(this, props));

        _this._onChange = function () {
            _this.setState(_this.state);
        };

        _this._selectView = function (view) {
            _cmp_state.state.setView(view.n);
        };

        _this.state = new _cmp_state.ComponentState();

        ewars.subscribe('VIEW_UPDATED', _this._onChange);
        return _this;
    }

    (0, _createClass3.default)(ViewMain, [{
        key: 'render',
        value: function render() {
            var view = void 0;

            if (_cmp_state.state.activeTab != 'DEFAULT') {
                var tab = _cmp_state.state.getActiveTab();
                if (TABS[tab.n]) {
                    var Cmp = TABS[tab.n];
                    view = React.createElement(Cmp, null);
                }
            } else {
                view = React.createElement(_view2.default, null);
            }

            var tabs = this.state.tabs.map(function (item) {
                return React.createElement(ewars.d.Button, {
                    highlight: item._ == _cmp_state.state.activeTab,
                    label: item.name || item.n,
                    icon: item.i || undefined,
                    closable: true,
                    onClose: function onClose() {
                        _cmp_state.state.removeTab(item._);
                    },
                    onClick: function onClick() {
                        _cmp_state.state.setActiveTab(item._);
                    } });
            });

            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    null,
                    React.createElement(
                        'div',
                        { className: 'btn-group btn-tabs' },
                        React.createElement(ewars.d.Button, {
                            highlight: _cmp_state.state.activeTab == 'DEFAULT',
                            onClick: function onClick() {
                                _cmp_state.state.setActiveTab('DEFAULT');
                            },
                            label: 'Config',
                            icon: 'fa-cogs' }),
                        tabs
                    )
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        view
                    )
                )
            );
        }
    }]);
    return ViewMain;
}(React.Component);

exports.default = ViewMain;

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(51);
module.exports = __webpack_require__(5).Object.getPrototypeOf;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(35);
var $getPrototypeOf = __webpack_require__(36);

__webpack_require__(52)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(10);
var core = __webpack_require__(5);
var fails = __webpack_require__(17);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(55), __esModule: true };

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(56);
var $Object = __webpack_require__(5).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(10);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(9), 'Object', { defineProperty: __webpack_require__(8).f });


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(58), __esModule: true };

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(59);
__webpack_require__(68);
module.exports = __webpack_require__(31).f('iterator');


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(60)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(41)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(24);
var defined = __webpack_require__(20);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(27);
var descriptor = __webpack_require__(19);
var setToStringTag = __webpack_require__(30);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(11)(IteratorPrototype, __webpack_require__(13)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(8);
var anObject = __webpack_require__(15);
var getKeys = __webpack_require__(28);

module.exports = __webpack_require__(9) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(44);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(12);
var toLength = __webpack_require__(65);
var toAbsoluteIndex = __webpack_require__(66);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(24);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(24);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(6).document;
module.exports = document && document.documentElement;


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(69);
var global = __webpack_require__(6);
var hide = __webpack_require__(11);
var Iterators = __webpack_require__(26);
var TO_STRING_TAG = __webpack_require__(13)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(70);
var step = __webpack_require__(71);
var Iterators = __webpack_require__(26);
var toIObject = __webpack_require__(12);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(41)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(73), __esModule: true };

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(74);
__webpack_require__(79);
__webpack_require__(80);
__webpack_require__(81);
module.exports = __webpack_require__(5).Symbol;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(6);
var has = __webpack_require__(7);
var DESCRIPTORS = __webpack_require__(9);
var $export = __webpack_require__(10);
var redefine = __webpack_require__(42);
var META = __webpack_require__(75).KEY;
var $fails = __webpack_require__(17);
var shared = __webpack_require__(22);
var setToStringTag = __webpack_require__(30);
var uid = __webpack_require__(18);
var wks = __webpack_require__(13);
var wksExt = __webpack_require__(31);
var wksDefine = __webpack_require__(32);
var enumKeys = __webpack_require__(76);
var isArray = __webpack_require__(77);
var anObject = __webpack_require__(15);
var toIObject = __webpack_require__(12);
var toPrimitive = __webpack_require__(23);
var createDesc = __webpack_require__(19);
var _create = __webpack_require__(27);
var gOPNExt = __webpack_require__(78);
var $GOPD = __webpack_require__(47);
var $DP = __webpack_require__(8);
var $keys = __webpack_require__(28);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(46).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(33).f = $propertyIsEnumerable;
  __webpack_require__(45).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(25)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    if (it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    replacer = args[1];
    if (typeof replacer == 'function') $replacer = replacer;
    if ($replacer || !isArray(replacer)) replacer = function (key, value) {
      if ($replacer) value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(11)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(18)('meta');
var isObject = __webpack_require__(16);
var has = __webpack_require__(7);
var setDesc = __webpack_require__(8).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(17)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(28);
var gOPS = __webpack_require__(45);
var pIE = __webpack_require__(33);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(44);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(12);
var gOPN = __webpack_require__(46).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 79 */
/***/ (function(module, exports) {



/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(32)('asyncIterator');


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(32)('observable');


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(83), __esModule: true };

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(84);
module.exports = __webpack_require__(5).Object.setPrototypeOf;


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(10);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(85).set });


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(16);
var anObject = __webpack_require__(15);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(37)(Function.call, __webpack_require__(47).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(87), __esModule: true };

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(88);
var $Object = __webpack_require__(5).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(10);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(27) });


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _cmp_state = __webpack_require__(34);

var _control = __webpack_require__(90);

var _control2 = _interopRequireDefault(_control);

var _view = __webpack_require__(92);

var _view2 = _interopRequireDefault(_view);

var _viewExport = __webpack_require__(105);

var _viewExport2 = _interopRequireDefault(_viewExport);

var _view3 = __webpack_require__(106);

var _view4 = _interopRequireDefault(_view3);

var _view5 = __webpack_require__(107);

var _view6 = _interopRequireDefault(_view5);

var _view7 = __webpack_require__(108);

var _view8 = _interopRequireDefault(_view7);

var _view9 = __webpack_require__(109);

var _view10 = _interopRequireDefault(_view9);

var _view11 = __webpack_require__(110);

var _view12 = _interopRequireDefault(_view11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VIEWS = {
    EXPORT_ALERTS: _viewExport2.default,
    ACCOUNT: _view6.default,
    THEME: _view4.default,
    NOTIFICATIONS: _view8.default,
    SECURITY: _view10.default,
    USAGE: _view12.default
};

var TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};

var ViewRoot = function (_React$Component) {
    (0, _inherits3.default)(ViewRoot, _React$Component);

    function ViewRoot(props) {
        (0, _classCallCheck3.default)(this, ViewRoot);
        return (0, _possibleConstructorReturn3.default)(this, (ViewRoot.__proto__ || (0, _getPrototypeOf2.default)(ViewRoot)).call(this, props));
    }

    (0, _createClass3.default)(ViewRoot, [{
        key: 'render',
        value: function render() {
            var view = void 0;

            if (_cmp_state.state.view.t == TYPES.VIEW) {
                if (VIEWS[_cmp_state.state.view._]) {
                    var Cmp = VIEWS[_cmp_state.state.view._];
                    view = React.createElement(Cmp, null);
                }
            } else if (_cmp_state.state.view.t == TYPES.TABLE) {
                // this is a table view, need to render it
                view = React.createElement(_view2.default, { data: _cmp_state.state.view });
            }

            return React.createElement(
                ewars.d.Row,
                null,
                React.createElement(
                    ewars.d.Cell,
                    { width: '250px', borderRight: true },
                    React.createElement(_control2.default, {
                        onSelect: this._selectView })
                ),
                React.createElement(
                    ewars.d.Cell,
                    null,
                    React.createElement(
                        ewars.d.Row,
                        null,
                        React.createElement(
                            ewars.d.Cell,
                            null,
                            view
                        )
                    )
                )
            );
        }
    }]);
    return ViewRoot;
}(React.Component);

exports.default = ViewRoot;

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _const = __webpack_require__(91);

var _const2 = _interopRequireDefault(_const);

var _cmp_state = __webpack_require__(34);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};

var TreeNode = function (_React$Component) {
    (0, _inherits3.default)(TreeNode, _React$Component);

    function TreeNode(props) {
        (0, _classCallCheck3.default)(this, TreeNode);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TreeNode.__proto__ || (0, _getPrototypeOf2.default)(TreeNode)).call(this, props));

        _this._toggleShow = function () {
            _this.setState({ show: !_this.state.show });
        };

        _this._onClick = function () {
            if (_this.props.data.t == TYPES.TAB) {
                _cmp_state.state.addTab(_this.props.data);
            } else {
                _cmp_state.state.setView(_this.props.data);
            }
        };

        _this.state = { show: false };
        return _this;
    }

    (0, _createClass3.default)(TreeNode, [{
        key: 'render',
        value: function render() {
            var icon = void 0;

            if (this.props.data.i) icon = React.createElement('i', { className: 'fal ' + this.props.data.i });

            var hasChilds = this.props.data.c ? true : false;
            var caretIcon = 'fal fa-caret-right';
            if (this.state.show) caretIcon = 'fal fa-caret-down';

            return React.createElement(
                'li',
                null,
                React.createElement(
                    ewars.d.Row,
                    null,
                    hasChilds ? React.createElement(
                        ewars.d.Cell,
                        {
                            onClick: this._toggleShow,
                            width: '20px' },
                        React.createElement('i', { className: caretIcon })
                    ) : null,
                    !hasChilds ? React.createElement(
                        ewars.d.Cell,
                        { width: '20px' },
                        '\xA0'
                    ) : null,
                    icon ? React.createElement(
                        ewars.d.Cell,
                        {
                            onClick: this._onClick,
                            width: '18' },
                        icon
                    ) : null,
                    React.createElement(
                        ewars.d.Cell,
                        {
                            onClick: this._onClick,
                            style: { cursor: 'pointer' } },
                        this.props.data.n
                    )
                ),
                this.props.data.c && this.state.show ? React.createElement(
                    'ul',
                    null,
                    this.props.data.c.map(function (item) {
                        return React.createElement(TreeNode, { key: item._, data: item });
                    })
                ) : null
            );
        }
    }]);
    return TreeNode;
}(React.Component);

var ControlTreeMenu = function (_React$Component2) {
    (0, _inherits3.default)(ControlTreeMenu, _React$Component2);

    function ControlTreeMenu() {
        (0, _classCallCheck3.default)(this, ControlTreeMenu);
        return (0, _possibleConstructorReturn3.default)(this, (ControlTreeMenu.__proto__ || (0, _getPrototypeOf2.default)(ControlTreeMenu)).apply(this, arguments));
    }

    (0, _createClass3.default)(ControlTreeMenu, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                ewars.d.Panel,
                null,
                React.createElement(
                    'ul',
                    { className: 'config-menu', style: { marginTop: '8px', marginLeft: '8px' } },
                    _const2.default.map(function (item) {
                        return React.createElement(TreeNode, { key: item._, data: item });
                    })
                )
            );
        }
    }]);
    return ControlTreeMenu;
}(React.Component);

exports.default = ControlTreeMenu;

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
};

exports.default = [{
    n: 'Appearance & Behaviour',
    i: 'fa-cog',
    _: 'APPEARANCE',
    t: TYPES.VIEW,
    c: [{ n: 'Account', i: 'fa-life-ring', _: 'ACCOUNT', t: TYPES.VIEW }, { n: 'Theme', i: 'fa-paint-brush', _: 'THEME', t: TYPES.VIEW }, { n: 'Email & Notifications', i: 'fa-envelope', _: 'EMAIL', t: TYPES.VIEW }, { n: 'Security & Authentication', i: 'fa-key', _: 'SECURITY', t: TYPES.VIEW }, { n: 'Registration', i: 'fa-list', _: 'REGISTRATION', t: TYPES.VIEW }, { n: 'Menu', i: 'fa-globe', _: 'MENU', t: TYPES.VIEW }, {
        n: 'Legal',
        i: 'fa-balance-scale',
        _: 'LEGAL',
        t: null,
        c: [{ n: 'Terms of use', i: 'fa-file', _: 'TERMS', t: TYPES.VIEW }, { n: 'Privacy policy', i: 'fa-file', _: 'PRIVACY', t: TYPES.VIEW }]
    }]
}, {
    n: 'Alarms',
    i: 'fa-bell',
    _: 'ALARMS',
    t: TYPES.TABLE,
    c: [{ n: 'Settings', i: 'fa-cog', _: 'ALARM_SETTINGS', t: TYPES.VIEW }, { n: 'Notifications', i: 'fa-envelope', _: 'ALERT_NOTIFICATIONS', t: TYPES.VIEW }, { n: 'Export', i: 'fa-download', _: 'ALERT_EXPORT', t: TYPES.VIEW }, { n: 'Integrations', i: 'fa-cubes', _: 'ALERT_INTEGRATIONS', t: TYPES.VIEW }, { n: 'Admin actions', i: 'fa-bold', _: 'ALARM_ADMIN_ACTIONS', t: TYPES.VIEW }]
}, {
    n: 'Locations',
    i: 'fa-map-marker',
    t: TYPES.TAB,
    _: 'LOCATIONS',
    c: [{ n: 'Location types', i: 'fa-tags', _: 'LOCATION_TYPES', t: TYPES.TABLE }, { n: 'Location groups', i: 'fa-tags', _: 'LOCATION_GROUPS', t: TYPES.TABLE }, { n: 'Identification codes', i: 'fa-barcode', _: 'LOCATION_ID_CODES', t: TYPES.TABLE }, { n: 'Export', i: 'fa-download', _: 'LOCATIONS_EXPORT', t: TYPES.VIEW }, { n: 'Import', i: 'fa-upload', _: 'LOCATIONS_IMPORT', t: TYPES.VIEW }, { n: 'Profiles', i: 'fa-map-signs', _: 'LOCATION_PROFILE', t: TYPES.VIEW }, { n: 'Integrations', i: 'fa-cubes', _: 'LOCATION_INTEGRATIONS', t: TYPES.VIEW }, { n: 'Reporting', i: 'fa-clipboard', _: 'LOCATION_REPORTING', t: TYPES.TABLE }, { n: 'Location statuses', _: 'LOCATION_STATUSES', t: TYPES.TABLE }]
}, {
    n: 'Users',
    i: 'fa-user',
    _: 'USERS',
    t: TYPES.TABLE,
    c: [{ n: 'Assignments', i: 'fa-clipboard', _: 'ASSIGNMENTS', t: TYPES.TABLE }, { n: 'Roles', i: 'fa-users', _: 'ROLES', t: TYPES.TABLE }, { n: 'Organizations', i: 'fa-building', _: 'ORGANIZATIONS', t: TYPES.TABLE }, { n: 'Profile extensions', i: 'fa-id-card', _: 'USER_PROFILES', t: TYPES.TABLE }, { n: 'Teams', i: 'fa-users', _: 'TEAMS', t: TYPES.TABLE }, { n: 'Distribution lists', i: 'fa-envelope', _: 'DISTRIBUTIONS', t: TYPES.TABLE }, { n: 'Invitations', i: 'fa-envelope', _: 'INVITATIONS', t: TYPES.TABLE }]
}, {
    n: 'Forms',
    i: 'fa-list-alt',
    _: 'FORMS',
    t: TYPES.TABLE,
    c: [{ n: 'Data import', i: 'fa-upload', _: 'FORMS_IMPORT', t: TYPES.TAB }, { n: 'Data export', i: 'fa-download', _: 'FORMS_EXPORT', t: TYPES.TAB }, { n: 'Integrations', i: 'fa-plug', _: 'FORM_INTEGRATIONS', t: TYPES.TABLE }, { n: 'Push API', i: 'fa-plug', _: 'FORMS_PUSH_API', t: TYPES.TABLE }, { n: 'Submissions', i: 'fa-clipboard', _: 'SUBMISSIONS', t: TYPES.TAB }]
}, {
    n: 'Workflows',
    i: 'fa-code-branch',
    _: 'WORKFLOWS',
    t: TYPES.TABLE,
    c: [{ n: 'Alert', i: 'fa-bell', _: 'WORKFLOW_ALERTS', t: TYPES.TABLE }, { n: 'Form', i: 'fa-list-alt', _: 'WORKFLOW_FORMS', t: TYPES.TABLE }, { n: 'System', i: 'fa-cube', _: 'WORKFLOW_SYSTEM', t: TYPES.TABLE }, { n: 'Active processes', i: 'fa-wrench', _: 'PROCESSES', t: TYPES.TABLE }, { n: 'Task auditing', i: 'fa-tasks', _: 'TASK_AUDITOR', t: TYPES.TABLE }]
}, {
    n: 'Data',
    i: 'fa-database',
    _: 'DATA',
    t: TYPES.VIEW,
    c: [{ n: 'Custom datasets', i: 'fa-database', _: 'RELATIONAL', t: TYPES.TABLE }, { n: 'External data', i: 'fa-server', _: 'CONNECTORS', t: TYPES.TABLE }]
}, {
    n: 'Analysis',
    i: 'fa-chart-line',
    _: 'ANALYSIS',
    c: [{ n: 'Notebooks', i: 'fa-book', _: 'NOTEBOOKS', t: TYPES.TABLE }, { n: 'HUDs', i: 'fa-browser', _: 'HUDS', t: TYPES.TABLE }, { n: 'Plots', i: 'fa-chart-pie', _: 'PLOTS', t: TYPES.TABLE }, { n: 'Maps', i: 'fa-map', _: 'MAPS', t: TYPES.TABLE }, { n: 'Measures', i: 'fa-tag', _: 'MEASURES', t: TYPES.TAB }, { n: 'Dimensions', i: 'fa-cog', _: 'DIMENSIONS', t: TYPES.TAB }, { n: 'Indicators', i: 'fa-tag', _: 'INDICATORS', t: TYPES.TAB }]

}, {
    n: 'Content',
    i: 'fa-eraser',
    _: 'CONTENT',
    c: [{ n: 'Feeds', i: 'fa-file-alt', _: 'FEEDS', t: TYPES.TABLE }, { n: 'Wikis', i: 'fa-graduation-cap', _: 'WIKIS', t: TYPES.TABLE }, { n: 'Digest emails', i: 'fa-envelope', _: 'DIGESTS', t: TYPES.TABLE }, { n: 'Dashboards', i: 'fa-tachometer', _: 'DASHBOARDS', t: TYPES.TABLE }, { n: 'Dashboards (legacy)', i: 'fa-tachometer', _: 'DASHBOARDS_LEGACY', t: TYPES.TABLE }, { n: 'Documents', i: 'fa-file', _: 'DOCUMENTS', t: TYPES.TABLE }, { n: 'Documents (legacy)', i: 'fa-file', _: 'DOCUMENTS_LEGACY', t: TYPES.TABLE }]
}, {
    n: 'Devices',
    i: 'fa-mobile',
    _: 'DEVICES',
    t: TYPES.VIEW,
    c: [{ n: 'Inventory', i: 'fa-barcode', _: 'DEVICE_INVENTORY', t: TYPES.TABLE }, { n: 'SMS gateways', i: 'fa-server', _: 'GATEWAYS_SMS', t: TYPES.TABLE }, { n: 'WAP gateway', i: 'fa-server', _: 'GATEWAYS_WAP', t: TYPES.TABLE }, { n: 'Relay nodes', i: 'fa-server', _: 'RELAY_NODES', t: TYPES.TABLE }, { n: 'Event log', i: 'fa-sync', _: 'EVENT_LOG', t: TYPES.TABLE }, { n: 'Import inventory', i: 'fa-upload', _: 'IMPORT_DEVICES', t: TYPES.TABLE }]
}, {
    n: 'Hub',
    i: 'fa-dot-circle',
    _: 'HUB',
    t: TYPES.CUSTOM,
    c: [{ n: 'Subscriptions', i: 'fa-arrow-to-right', _: 'SUBSCRIPTIONS', t: TYPES.TABLE }, { n: 'Shared data', i: 'fa-exchange', _: 'SHARED_DATA', t: TYPES.TABLE }, { n: 'Subscribers', i: 'fa-arrow-from-right', _: 'SUBSCRIBERS', t: TYPES.TABLE }, { n: 'Advertised sets', i: 'fa-eye', _: 'ADVERTISED_SETS', t: TYPES.TABLE }]
}, {
    n: '3rd party integrations',
    i: 'fa-cubes',
    _: 'INTEGRATIONS',
    t: TYPES.VIEW,
    c: [{ n: 'Weather API', i: 'fa-cloud', _: 'INTEGRATION_WEATHER', t: TYPES.VIEW }, { n: 'Maps', i: 'fa-map', _: 'INTEGRATION_MAPS', t: TYPES.VIEW }, { n: 'Amazon S3', i: 'fa-database', _: 'INTEGRATION_S3', t: TYPES.VIEW }, { n: 'Slack', i: 'fa-slack-hash', _: 'INTEGRATION_SLACK', t: TYPES.VIEW }, { n: 'Skype', i: 'fa-skype', _: 'INTEGRATION_SLACK', t: TYPES.VIEW }, { n: 'Telegram', i: 'fa-telegram', _: 'INTEGRATION_TELEGRAM', t: TYPES.VIEW }, { n: 'Open ID', i: 'fa-openid', _: 'INTEGRATION_OPENID', t: TYPES.VIEW }, { n: 'Dropbox', i: 'fa-dropbox', _: 'INTEGRATION_DROPBOX', t: TYPES.VIEW }, { n: 'Line', i: 'fa-line', _: 'INTEGRATION_LINE', t: TYPES.VIEW }]
}, {
    n: 'Custom components',
    i: 'fa-cubes',
    _: 'COMPONENTS',
    t: TYPES.TABLE
}, {
    n: 'Sites',
    i: 'fa-sitemap',
    _: 'SITES',
    t: TYPES.TABLE
}, {
    n: 'API',
    i: 'fa-leaf',
    _: 'API',
    t: TYPES.VIEW
}, {
    n: 'System administration',
    i: 'fa-cogs',
    _: 'ADMIN',
    t: TYPES.VIEW,
    c: [{ n: 'Usage', i: 'fa-fire', _: 'USAGE', t: TYPES.VIEW }, { n: 'Backup(s)', i: 'fa-database', _: 'BACKUPS', t: TYPES.VIEW }, { n: 'GSM Modems', i: 'fa-server', _: 'GSM', t: TYPES.VIEW }, { n: 'Performance', i: 'fa-heartbeat', _: 'PERFORMANCE', t: TYPES.VIEW }, { n: 'System notices', i: 'fa-exclamation-triangle', _: 'NOTICES', t: TYPES.TABLE }, { n: 'EWARS releases', i: 'fa-thumbs-up', _: 'RELEASES', t: TYPES.TABLE }, { n: 'Translation(s)', i: 'fa-language', _: 'TRANSLATIONS', t: TYPES.TABLE }, { n: 'PDF cache', i: 'fa-file-archive', _: 'PDF_CACHE', t: TYPES.TABLE }, { n: 'Indexing', i: 'fa-list-alt', _: 'INDEXING', t: TYPES.VIEW }, { n: 'Audit log(s)', i: 'fa-clock', _: 'AUDIT_LOGS', t: TYPES.TABLE }, { n: 'Authentication log', i: 'fa-shield', _: 'AUTH_LOG', t: TYPES.TABLE }, { n: 'Admin actions', i: 'fa-bolt', _: 'ADMIN_ACTIONS', t: TYPES.VIEW }, { n: 'Jobs', i: 'fa-clock', _: 'JOBS', t: TYPES.VIEW }]
}];

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _tables = __webpack_require__(93);

var TABLES = _interopRequireWildcard(_tables);

var _utils = __webpack_require__(102);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEFAULT_ACTIONS = [['fa-sync', 'RELOAD'], ['fa-filter', 'FILTER'], ['fa-ellipsis-v', 'NOOPA'], ['fa-fast-backward', 'BB'], ['fa-backward', 'B'], ['fa-forward', 'F'], ['fa-fast-forward', 'FF']];

var TableHeaderCell = function (_React$Component) {
    (0, _inherits3.default)(TableHeaderCell, _React$Component);

    function TableHeaderCell(props) {
        (0, _classCallCheck3.default)(this, TableHeaderCell);
        return (0, _possibleConstructorReturn3.default)(this, (TableHeaderCell.__proto__ || (0, _getPrototypeOf2.default)(TableHeaderCell)).call(this, props));
    }

    (0, _createClass3.default)(TableHeaderCell, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'th',
                null,
                this.props.data.l
            );
        }
    }]);
    return TableHeaderCell;
}(React.Component);

var TableHeader = function (_React$Component2) {
    (0, _inherits3.default)(TableHeader, _React$Component2);

    function TableHeader(props) {
        (0, _classCallCheck3.default)(this, TableHeader);
        return (0, _possibleConstructorReturn3.default)(this, (TableHeader.__proto__ || (0, _getPrototypeOf2.default)(TableHeader)).call(this, props));
    }

    (0, _createClass3.default)(TableHeader, [{
        key: 'render',
        value: function render() {
            var _this3 = this;

            var headers = [];
            if (TABLES[this.props.table]) {
                headers = TABLES[this.props.table].columns.map(function (col) {
                    return React.createElement(TableHeaderCell, {
                        key: col.n,
                        sorts: _this3.props.sorts,
                        filters: _this3.props.filters,
                        data: col });
                });
            }

            return React.createElement(
                'thead',
                null,
                React.createElement(
                    'tr',
                    null,
                    headers
                )
            );
        }
    }]);
    return TableHeader;
}(React.Component);

var TableRow = function (_React$Component3) {
    (0, _inherits3.default)(TableRow, _React$Component3);

    function TableRow(props) {
        (0, _classCallCheck3.default)(this, TableRow);
        return (0, _possibleConstructorReturn3.default)(this, (TableRow.__proto__ || (0, _getPrototypeOf2.default)(TableRow)).call(this, props));
    }

    (0, _createClass3.default)(TableRow, [{
        key: 'render',
        value: function render() {
            var _this5 = this;

            return React.createElement(
                'tr',
                null,
                this.props.columns.map(function (col) {
                    return React.createElement(
                        'td',
                        { key: col.n },
                        _this5.props.data[col.n] || ''
                    );
                })
            );
        }
    }]);
    return TableRow;
}(React.Component);

var FilterPanel = function (_React$Component4) {
    (0, _inherits3.default)(FilterPanel, _React$Component4);

    function FilterPanel(props) {
        (0, _classCallCheck3.default)(this, FilterPanel);
        return (0, _possibleConstructorReturn3.default)(this, (FilterPanel.__proto__ || (0, _getPrototypeOf2.default)(FilterPanel)).call(this, props));
    }

    (0, _createClass3.default)(FilterPanel, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'filter-panel' },
                React.createElement(ewars.d.Panel, null)
            );
        }
    }]);
    return FilterPanel;
}(React.Component);

var ViewTable = function (_React$Component5) {
    (0, _inherits3.default)(ViewTable, _React$Component5);

    function ViewTable(props) {
        (0, _classCallCheck3.default)(this, ViewTable);

        var _this7 = (0, _possibleConstructorReturn3.default)(this, (ViewTable.__proto__ || (0, _getPrototypeOf2.default)(ViewTable)).call(this, props));

        _this7._onAction = function (action, data) {};

        var table_def = TABLES[_this7.props.data._];

        _this7.state = {
            data: [],
            limit: 50,
            total: 'unknown',
            offset: 0,
            sorts: table_def.initialSort || {},
            filters: table_def.initialFilter || {},
            search: table_def.search || false,
            searchTerm: '',
            def: table_def,
            columns: table_def.columns,
            map: table_def.map || {},
            actions: table_def.actions || null,
            showFilters: false
        };

        if (table_def) {
            table_def.query(_this7.state.limit, _this7.state.offset, _this7.state.sorts, _this7.state.filters).then(function (resp) {
                _this7.setState({
                    data: resp.results,
                    total: resp.total,
                    limit: _this7.state.limit,
                    offset: _this7.state.offset,
                    sorts: _this7.state.sorts,
                    filters: _this7.state.filters
                });
            });
        }
        return _this7;
    }

    (0, _createClass3.default)(ViewTable, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(props) {
            var _this8 = this;

            var table_def = TABLES[props.data._];

            this.state = {
                data: [],
                limit: 50,
                total: 'unknown',
                offset: 0,
                sorts: table_def.initialSort || {},
                filters: table_def.initialFilter || {},
                search: table_def.search || false,
                searchTerm: '',
                def: table_def,
                columns: table_def.columns,
                map: table_def.map || {},
                actions: table_def.actions || null,
                showFilters: false
            };

            if (table_def) {
                table_def.query(this.state.limit, this.state.offset, this.state.sorts, this.state.filters).then(function (resp) {
                    _this8.setState({
                        data: resp.results,
                        total: resp.total,
                        limit: _this8.state.limit,
                        offset: _this8.state.offset,
                        sorts: _this8.state.sorts,
                        filters: _this8.state.filters
                    });
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this9 = this;

            var actions = _utils2.default.copy(DEFAULT_ACTIONS);

            if (this.state.actions) {
                actions.push(['fa-ellipsis-v', 'NOOPB']);
                this.state.actions.forEach(function (item) {
                    actions.push(item);
                });
            }

            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: this.props.data.n, icon: this.props.data.i || null },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: actions })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            ewars.d.Panel,
                            null,
                            React.createElement(
                                'div',
                                { className: 'data-table' },
                                React.createElement(
                                    'table',
                                    { className: 'data-table' },
                                    React.createElement(TableHeader, {
                                        table: this.props.data._,
                                        filters: this.state.filters,
                                        sorts: this.state.sorts }),
                                    React.createElement(
                                        'tbody',
                                        null,
                                        this.state.data.map(function (row) {
                                            return React.createElement(TableRow, {
                                                key: row.id || row.uuid,
                                                map: _this9.state.map,
                                                data: row,
                                                columns: _this9.state.columns });
                                        })
                                    )
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    ewars.d.Toolbar,
                    null,
                    React.createElement(
                        'span',
                        null,
                        'Showing __ to ___ of ',
                        this.state.total
                    )
                ),
                this.state.showFilters ? React.createElement(FilterPanel, {
                    filters: this.state.filters,
                    table: this.props.data._ }) : null
            );
        }
    }]);
    return ViewTable;
}(React.Component);

exports.default = ViewTable;

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DEVICES = exports.LOCATIONS = exports.USERS = exports.TEAMS = exports.ROLES = exports.LOCATION_TYPES = exports.ASSIGNMENTS = exports.ALARMS = undefined;

var _table = __webpack_require__(94);

var _table2 = _interopRequireDefault(_table);

var _table3 = __webpack_require__(95);

var _table4 = _interopRequireDefault(_table3);

var _table5 = __webpack_require__(96);

var _table6 = _interopRequireDefault(_table5);

var _table7 = __webpack_require__(97);

var _table8 = _interopRequireDefault(_table7);

var _table9 = __webpack_require__(98);

var _table10 = _interopRequireDefault(_table9);

var _table11 = __webpack_require__(99);

var _table12 = _interopRequireDefault(_table11);

var _table13 = __webpack_require__(100);

var _table14 = _interopRequireDefault(_table13);

var _table15 = __webpack_require__(101);

var _table16 = _interopRequireDefault(_table15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.ALARMS = _table2.default;
exports.ASSIGNMENTS = _table4.default;
exports.LOCATION_TYPES = _table6.default;
exports.ROLES = _table8.default;
exports.TEAMS = _table10.default;
exports.USERS = _table12.default;
exports.LOCATIONS = _table14.default;
exports.DEVICES = _table16.default;

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    query: function query(limit, offset, sorts, filters) {
        return ewars.tx('com.ewars.alarms', [limit, offset, sorts, filters]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-eye', 'VIEW_ALERTS'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'name', l: 'Name', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'created', l: 'Created', t: 'DATE', sort: true }, { n: 'created_by', l: 'Created by' }, { n: 'modified', l: 'Modified', t: 'DATE', sort: true }, { n: 'modified_by', l: 'Modified by' }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: 'SELECT',
            o: ['ACTIVE', 'INACTIVE']
        }
    },
    map: {
        ACTIVE: 'Active',
        INACTIVE: 'Inactive'
    }
};

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.assignments', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'user_name', l: 'Name', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'form_name', l: 'Form', sort: true }, { n: 'location_name', l: 'Location', sort: true }, { n: 'start_date', l: 'Start date', sort: true, t: _const2.default.DATE }, { n: 'end_date', l: 'End date', sort: true, t: _const2.default.DATE }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        },
        'location': {
            t: _const2.default.LOCATION
        },
        'user': {
            t: _const2.default.USER
        },
        'form': {
            t: _const2.default.FORM
        }

    }
};

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.users', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-clipboard', 'ASSIGNMENTS'], ['fa-ellipsis-v', 'NOOP'], ['fa-ban', 'DELETE']],
    columns: [{ n: 'name', l: 'Name', sort: true }, { n: 'email', l: 'Email', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'role', l: 'Role', sort: true }, { n: 'location_name', l: 'Location', sort: true }, { n: 'org_name', l: 'Organization', sort: true }, { n: 'registered', l: 'Registered', sort: true, t: _const2.default.DATE }, { n: 'system', l: 'System user', sort: true, t: _const2.default.SWITCH }],
    initialSort: {
        'registered': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        },
        'role': {
            t: _const2.default.ROLE
        }
    }
};

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.roles', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'name', l: 'Name', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'created', l: 'Created', t: _const2.default.DATE, sort: true }, { n: 'created_by', l: 'Created by' }, { n: 'modified', l: 'Modified', t: _const2.default.DATE, sort: true }, { n: 'modified_by', l: 'Modified by' }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        }
    }
};

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.teams', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-eye', 'VIEW'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'name', l: 'Name', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'created', l: 'Created', t: _const2.default.DATE, sort: true }, { n: 'created_by', l: 'Created by' }, { n: 'modified', l: 'Modified', t: _const2.default.DATE, sort: true }, { n: 'modified_by', l: 'Modified by' }, { n: 'members', l: 'Members' }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        }
    }
};

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {};

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.teams', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-eye', 'VIEW'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'name', l: 'Name', sort: true }, { n: 'sti_name', l: 'Location type', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'created', l: 'Created', t: _const2.default.DATE, sort: true }, { n: 'created_by', l: 'Created by' }, { n: 'modified', l: 'Modified', t: _const2.default.DATE, sort: true }, { n: 'modified_by', l: 'Modified by' }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'INACTIVE']
        }
    }
};

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _const = __webpack_require__(14);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    query: function query(limit, offset, filters, sorts) {
        return ewars.tx('com.ewars.devices', [limit, offset, filters, sort]);
    },
    actions: [['fa-plus', 'CREATE']],
    rowActions: [['fa-pencil', 'EDIT'], ['fa-ellipsis-v', 'NOOP'], ['fa-trash', 'DELETE']],
    columns: [{ n: 'device_id', l: 'Device ID', sort: true }, { n: 'status', l: 'Status', sort: true }, { n: 'created', l: 'Created', t: _const2.default.DATE, sort: true }, { n: 'created_by', l: 'Created by' }, { n: 'modified', l: 'Modified', t: _const2.default.DATE, sort: true }, { n: 'modified_by', l: 'Modified by' }],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: _const2.default.SELECT,
            o: ['ACTIVE', 'BLOCKED']
        }
    }
};

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(103);

var _stringify2 = _interopRequireDefault(_stringify);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Utils = function Utils() {
    (0, _classCallCheck3.default)(this, Utils);
};

Utils.copy = function (data) {
    return JSON.parse((0, _stringify2.default)(data));
};

exports.default = Utils;

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(104), __esModule: true };

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(5);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewExportAlarms = function (_React$Component) {
    (0, _inherits3.default)(ViewExportAlarms, _React$Component);

    function ViewExportAlarms(props) {
        (0, _classCallCheck3.default)(this, ViewExportAlarms);
        return (0, _possibleConstructorReturn3.default)(this, (ViewExportAlarms.__proto__ || (0, _getPrototypeOf2.default)(ViewExportAlarms)).call(this, props));
    }

    (0, _createClass3.default)(ViewExportAlarms, [{
        key: "render",
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(ewars.d.Toolbar, { title: "Alerts export" }),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(ewars.d.Panel, null)
                    )
                )
            );
        }
    }]);
    return ViewExportAlarms;
}(React.Component);

exports.default = ViewExportAlarms;

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewTheme = function (_React$Component) {
    (0, _inherits3.default)(ViewTheme, _React$Component);

    function ViewTheme(props) {
        (0, _classCallCheck3.default)(this, ViewTheme);
        return (0, _possibleConstructorReturn3.default)(this, (ViewTheme.__proto__ || (0, _getPrototypeOf2.default)(ViewTheme)).call(this, props));
    }

    (0, _createClass3.default)(ViewTheme, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: 'Theme Settings' },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(ewars.d.Panel, null)
                    )
                )
            );
        }
    }]);
    return ViewTheme;
}(React.Component);

exports.default = ViewTheme;

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewAccount = function (_React$Component) {
    (0, _inherits3.default)(ViewAccount, _React$Component);

    function ViewAccount(props) {
        (0, _classCallCheck3.default)(this, ViewAccount);
        return (0, _possibleConstructorReturn3.default)(this, (ViewAccount.__proto__ || (0, _getPrototypeOf2.default)(ViewAccount)).call(this, props));
    }

    (0, _createClass3.default)(ViewAccount, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: 'Account Settings' },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            ewars.d.Panel,
                            null,
                            React.createElement(ewars.d.SystemForm, {
                                definition: [],
                                data: {} })
                        )
                    )
                )
            );
        }
    }]);
    return ViewAccount;
}(React.Component);

exports.default = ViewAccount;

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewNotifications = function (_React$Component) {
    (0, _inherits3.default)(ViewNotifications, _React$Component);

    function ViewNotifications(props) {
        (0, _classCallCheck3.default)(this, ViewNotifications);
        return (0, _possibleConstructorReturn3.default)(this, (ViewNotifications.__proto__ || (0, _getPrototypeOf2.default)(ViewNotifications)).call(this, props));
    }

    (0, _createClass3.default)(ViewNotifications, [{
        key: "render",
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: "Email & Notifications" },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(ewars.d.Panel, null)
                    )
                )
            );
        }
    }]);
    return ViewNotifications;
}(React.Component);

exports.default = ViewNotifications;

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-save', 'SAVE']];

var ViewSecurity = function (_React$Component) {
    (0, _inherits3.default)(ViewSecurity, _React$Component);

    function ViewSecurity(props) {
        (0, _classCallCheck3.default)(this, ViewSecurity);
        return (0, _possibleConstructorReturn3.default)(this, (ViewSecurity.__proto__ || (0, _getPrototypeOf2.default)(ViewSecurity)).call(this, props));
    }

    (0, _createClass3.default)(ViewSecurity, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: 'Security & Authentication' },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(ewars.d.Panel, null)
                    )
                )
            );
        }
    }]);
    return ViewSecurity;
}(React.Component);

exports.default = ViewSecurity;

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewUsage = function (_React$Component) {
    (0, _inherits3.default)(ViewUsage, _React$Component);

    function ViewUsage(props) {
        (0, _classCallCheck3.default)(this, ViewUsage);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ViewUsage.__proto__ || (0, _getPrototypeOf2.default)(ViewUsage)).call(this, props));

        _this.state = { data: {} };

        ewars.tx('com.ewars.account.usage', []).then(function (resp) {
            _this.setState({ data: resp });
        });
        return _this;
    }

    (0, _createClass3.default)(ViewUsage, [{
        key: "render",
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(ewars.d.Toolbar, { label: "Usage" }),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "table",
                            { className: "data-table" },
                            React.createElement(
                                "thead",
                                null,
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Metric"
                                    ),
                                    React.createElement(
                                        "th",
                                        null,
                                        "Value"
                                    )
                                )
                            ),
                            React.createElement(
                                "tbody",
                                null,
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Database size"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.db_size || '0 MB'
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Emails sent"
                                    ),
                                    React.createElement("td", null)
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Users"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.users || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Locations"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.locations || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Forms"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.forms || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Devices"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.devices || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Dataset records"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.records_ds || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Form records"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.records || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Alerts"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.alerts || 0
                                    )
                                ),
                                React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "th",
                                        null,
                                        "Event logs"
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        this.state.data.events || 0
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ViewUsage;
}(React.Component);

exports.default = ViewUsage;

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _controlLocations = __webpack_require__(112);

var _controlLocations2 = _interopRequireDefault(_controlLocations);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTIONS = [['fa-plus', 'ADD']];

var TabLocations = function (_React$Component) {
    (0, _inherits3.default)(TabLocations, _React$Component);

    function TabLocations(props) {
        (0, _classCallCheck3.default)(this, TabLocations);
        return (0, _possibleConstructorReturn3.default)(this, (TabLocations.__proto__ || (0, _getPrototypeOf2.default)(TabLocations)).call(this, props));
    }

    (0, _createClass3.default)(TabLocations, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Toolbar,
                    { label: 'Locations' },
                    React.createElement(ewars.d.ActionGroup, {
                        right: true,
                        actions: ACTIONS })
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        { width: '33.33%', borderRight: true },
                        React.createElement(_controlLocations2.default, null)
                    ),
                    React.createElement(ewars.d.Cell, null)
                )
            );
        }
    }]);
    return TabLocations;
}(React.Component);

exports.default = TabLocations;

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

var _ContextMenu = __webpack_require__(113);

var _ContextMenu2 = _interopRequireDefault(_ContextMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright (c) 2015 JDU Software & Consulting Limited.
 * ALl Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of JDU Software &
 * Consulting Limited and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to JDU Software & Consulting Limited and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from JDU Software & Consulting Limited.
 */
var CONSTANTS = __webpack_require__(114);

var SELECTS = ["uuid", "name", "location_type", "site_type_id", "geometry_type", "status", "@children", "parent_id", "geometry", "point"];
var JOINS = ["location_type:site_type_id:id"];

var TreeNodeComponent = function (_React$Component) {
    (0, _inherits3.default)(TreeNodeComponent, _React$Component);

    function TreeNodeComponent(props) {
        (0, _classCallCheck3.default)(this, TreeNodeComponent);

        var _this = (0, _possibleConstructorReturn3.default)(this, (TreeNodeComponent.__proto__ || (0, _getPrototypeOf2.default)(TreeNodeComponent)).call(this, props));

        _this.isLoaded = false;

        _this._onBodyClick = function (evt) {
            if (!_this.refs.item.contains(evt.target)) {
                _this.setState({
                    showContext: false
                });
            }
        };

        _this._load = function (location_id, show) {
            _this.refs.iconHandle.setAttribute("class", "fa fa-spin fa-gear");
            var query = {
                parent_id: { eq: _this.props.data.uuid },
                status: _this.props.hideInactive ? { eq: "ACTIVE" } : { neq: "DELETED" }
            };

            ewars.tx("com.ewars.query", ["location", SELECTS, query, { "name.en": "ASC" }, null, null, JOINS]).then(function (resp) {
                this._hasLoaded = true;
                this._isLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: show ? show : this.state.showChildren
                });
            }.bind(_this));
        };

        _this._changeReload = function (change_locs) {
            if (change_locs.indexOf(_this.props.data.uuid) >= 0) {
                var query = {
                    parent_id: { eq: _this.props.data.uuid },
                    status: _this.props.hideInactive ? { eq: "ACTIVE" } : { neq: "DELETED" }
                };

                ewars.tx("com.ewars.query", ["location", SELECTS, query, { "name.en": "ASC" }, null, null, JOINS]).then(function (resp) {
                    this.setState({
                        children: resp
                    });
                }.bind(_this));
            }
        };

        _this._reload = function (location_id, show) {
            if (!_this._isLoaded && _this.props.data.children > 0) {
                _this._load(location_id, show);
            }
        };

        _this._onCaretClick = function () {
            // Children are already loaded
            if (_this.state.children) {
                _this.setState({
                    showChildren: !_this.state.showChildren
                });
                return;
            }

            // No Children loaded, load them
            _this._reload(_this.props.data.uuid, true);
        };

        _this._onLabelClick = function () {
            if (_this.props.restrict_selection && _this.props.data.site_type_id == _this.props.restrict_selection) return;

            _this.props.onAction("EDIT", _this.props.data);
        };

        _this._hasChildren = function () {
            return true;
        };

        _this._onCheck = function () {
            _this.props.onAction("CHECK", _this.props.data);
        };

        _this._onContext = function (e) {
            e.preventDefault();
            _this.setState({
                showContext: true
            });
        };

        _this._onContextAction = function (action) {
            _this.setState({
                showContext: false
            });

            _this.props.onAction(action, _this.props.data);
        };

        _this.state = ewars.g["LOC_" + _this.props.data.uuid] || {
            children: null,
            showChildren: false,
            isChecked: false
        };
        return _this;
    }

    (0, _createClass3.default)(TreeNodeComponent, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            if (this.props.checked) this.state.isChecked = this.props.checked.indexOf(this.props.data.uuid) >= 0;
            ewars.subscribe("LOCATIONS_CHANGE", this._changeReload);
            window.__hack__.addEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            ewars.g["LOC_" + this.props.data.uuid] = ewars.copy(this.state);
            window.__hack__.removeEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (nextProps.checked) this.state.isChecked = nextProps.checked.indexOf(this.props.data.uuid) >= 0;
        }
    }, {
        key: "render",
        value: function render() {
            var childs = void 0;
            if (this.state.children) {
                childs = this.state.children.map(function (child, i) {
                    return React.createElement(TreeNodeComponent, {
                        actions: this.props.actions,
                        key: child.uuid,
                        hideInactive: this.props.hideInactive,
                        checked: this.props.checked,
                        allowCheck: this.props.allowCheck,
                        onAction: this.props.onAction,
                        index: i,
                        selectTypeId: this.props.selectTypeId,
                        data: child });
                }.bind(this));
            }

            var icon = "fa-folder";
            if (this.state.showChildren) icon = "fa-folder-open";
            if (this.props.data.children <= 0) icon = "fa-map-marker";
            var iconClass = "fal " + icon;

            var name = ewars.I18N(this.props.data.name) + " (" + ewars.I18N(this.props.data.location_type.name) + ")";

            var checkClass = "fal fa-square";
            if (this.state.isChecked) checkClass = "fal fa-check-square";

            var blockClass = "block-content";
            if (this.props.data.status == CONSTANTS.ACTIVE) blockClass += " status-green";
            if (this.props.data.status == CONSTANTS.DISABLED) blockClass += " status-red";

            return React.createElement(
                "div",
                { className: "block" },
                React.createElement(
                    "div",
                    { className: blockClass, style: { padding: 0 }, ref: "item", onContextMenu: this._onContext },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col", onClick: this._onCaretClick,
                                style: { maxWidth: 20, padding: 8 } },
                            React.createElement(
                                "div",
                                { className: "node-control" },
                                React.createElement("i", { ref: "iconHandle", className: iconClass })
                            )
                        ),
                        this.props.allowCheck ? React.createElement(
                            "div",
                            { className: "ide-col", onClick: this._onCheck, style: { maxWidth: 20, padding: 8 } },
                            React.createElement(
                                "div",
                                { className: "node-check" },
                                React.createElement("i", { className: checkClass })
                            )
                        ) : null,
                        React.createElement(
                            "div",
                            { className: "ide-col", onClick: this._onLabelClick, style: { padding: 8 } },
                            name
                        )
                    ),
                    this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                        absolute: true,
                        onClick: this._onContextAction,
                        actions: this.props.actions }) : null
                ),
                this.state.showChildren ? React.createElement(
                    "div",
                    { className: "block-children" },
                    childs
                ) : null
            );
        }
    }]);
    return TreeNodeComponent;
}(React.Component);

var LocationTree = function (_React$Component2) {
    (0, _inherits3.default)(LocationTree, _React$Component2);

    function LocationTree(props) {
        (0, _classCallCheck3.default)(this, LocationTree);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (LocationTree.__proto__ || (0, _getPrototypeOf2.default)(LocationTree)).call(this, props));

        _this2._init = function () {
            var query = {};

            if (!_this2.props.parentId) query.parent_id = { eq: "NULL" };
            if (_this2.props.parentId) query.uuid = { eq: _this2.props.parentId };

            if (_this2.props.hideInactive) {
                query.status = { eq: "ACTIVE" };
            } else {
                query.status = { neq: "DELETED" };
            }

            ewars.tx("com.ewars.query", ["location", SELECTS, query, { "name.en": "ASC" }, null, null, JOINS]).then(function (resp) {
                this.setState({ rootElements: resp });
            }.bind(_this2));
        };

        _this2._handleSearchChange = function (e) {
            var val = e.target.value;

            _this2.setState({
                search: val,
                rootElements: []
            });

            var args = [val, null, null];

            if (_this2.props.hideInactive) args[2] = { eq: "ACTIVE" };

            if (val.length >= 3) {
                var query = {
                    "name.en": { like: val }
                };

                if (_this2.props.hideInactive) {
                    query.status = { eq: "ACTIVE" };
                } else {
                    query.status = { neq: "DELETED" };
                }

                ewars.tx("com.ewars.query", ["location", SELECTS, query, { "name.en": "ASC" }, null, null, JOINS]).then(function (resp) {
                    this.setState({
                        rootElements: resp
                    });
                }.bind(_this2));
            } else {
                _this2._init();
            }
        };

        _this2._onCheck = function (node) {
            var isChecked = true;
            var checked = _this2.props.checked || [];

            if (checked.indexOf(node.uuid) < 0) {
                checked.push(node.uuid);
            } else {
                checked = _.without(checked, node.uuid);
                isChecked = false;
            }

            // Store location data
            var checkedNodes = _this2.state.checkedNodes || [];

            if (isChecked) {
                checkedNodes.push(node);
                var existing = [];
                checkedNodes = _.reject(checkedNodes, function (node) {
                    if (existing.indexOf(node.uuid) < 0) {
                        existing.push(node.uuid);
                        return false;
                    } else {
                        return true;
                    }
                });
            } else {
                checkedNodes = _.reject(checkedNodes, function (item) {
                    return item.uuid == node.uuid;
                }, _this2);
            }

            _this2.setState({
                checked: checked,
                checkedNodes: checkedNodes
            });

            _this2.props.onSelectChange(checked, checkedNodes);
        };

        _this2._clearSearch = function () {
            _this2.setState({
                search: ""
            });
            _this2._init();
        };

        _this2.state = {
            steps: [],
            rootElements: [],
            checked: [],
            nodesChecked: [],
            search: ""
        };

        ewars.subscribe('LOCATIONS_CHANGE', _this2._init);
        _this2._init();
        return _this2;
    }

    (0, _createClass3.default)(LocationTree, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (nextProps.allowCheck) {
                this.state.checked = this.props.checked;
                // coalesce changes in selections into nodesChecked
                this.state.checkedNodes = [];
                this.state.checkedNodes.forEach(function (item) {
                    var index = this.state.checked.indexOf(item.uuid);
                    if (index >= 0) this.state.checkedNodes.push(item);
                }.bind(this));
            }
        }
    }, {
        key: "render",
        value: function render() {
            var rootNodes = this.state.rootElements.map(function (element) {
                var id = element.uuid;
                if (this.state.search != "") id = "SRCH_" + id;
                return React.createElement(TreeNodeComponent, {
                    key: id,
                    onAction: this.props.onAction,
                    hideInactive: this.props.hideInactive,
                    checked: this.props.checked,
                    allowCheck: this.props.allowCheck,
                    data: element,
                    actions: this.props.actions,
                    selectTypeId: this.props.selectTypeId });
            }.bind(this));

            return React.createElement(
                "div",
                { className: "ide-layout" },
                React.createElement(
                    "div",
                    { className: "ide-row", style: { maxHeight: 45 } },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(
                            "div",
                            { className: "search" },
                            React.createElement(
                                "div",
                                { className: "search-inner" },
                                React.createElement(
                                    "div",
                                    { className: "ide-row" },
                                    React.createElement(
                                        "div",
                                        { className: "ide-col search-left", style: { maxWidth: 25 } },
                                        React.createElement("i", { className: "fal fa-search" })
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "ide-col search-mid" },
                                        React.createElement("input", { type: "text", value: this.state.search,
                                            onChange: this._handleSearchChange })
                                    ),
                                    this.state.search != "" ? React.createElement(
                                        "div",
                                        { className: "ide-col search-right", style: { maxWidth: 25 },
                                            onClick: this._clearSearch },
                                        React.createElement("i", { className: "fal fa-times" })
                                    ) : null
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree" },
                                rootNodes
                            )
                        )
                    )
                )
            );
        }
    }]);
    return LocationTree;
}(React.Component);

LocationTree.defaultProps = {
    allowCheck: false,
    parentId: null,
    hideInactive: false,
    onAction: function onAction() {},
    actions: [],
    selectTypeId: null
};
exports.default = LocationTree;

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(1);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(2);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(3);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(4);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ContextMenuItem = function (_React$Component) {
    (0, _inherits3.default)(ContextMenuItem, _React$Component);

    function ContextMenuItem(props) {
        (0, _classCallCheck3.default)(this, ContextMenuItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ContextMenuItem.__proto__ || (0, _getPrototypeOf2.default)(ContextMenuItem)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.props.onClick(_this.props.data.action);
        };

        return _this;
    }

    (0, _createClass3.default)(ContextMenuItem, [{
        key: "render",
        value: function render() {
            var iconClass = "fal " + this.props.data.icon;

            return React.createElement(
                "div",
                { onClick: this._onClick, className: "context-menu-item" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement("i", { className: iconClass })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        this.props.data.label
                    )
                )
            );
        }
    }]);
    return ContextMenuItem;
}(React.Component);

var ContextMenu = function (_React$Component2) {
    (0, _inherits3.default)(ContextMenu, _React$Component2);

    function ContextMenu(props) {
        (0, _classCallCheck3.default)(this, ContextMenu);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (ContextMenu.__proto__ || (0, _getPrototypeOf2.default)(ContextMenu)).call(this, props));

        _this2._onClick = function (action) {
            _this2.props.onClick(action);
        };

        return _this2;
    }

    (0, _createClass3.default)(ContextMenu, [{
        key: "render",
        value: function render() {
            var style = {};
            if (!this.props.absolute) style.position = "relative";

            var actions = this.props.actions.map(function (action) {
                return React.createElement(ContextMenuItem, {
                    key: action.action,
                    data: action,
                    onClick: this._onClick });
            }.bind(this));

            return React.createElement(
                "div",
                { className: "context-menu", style: style },
                actions
            );
        }
    }]);
    return ContextMenu;
}(React.Component);

exports.default = ContextMenu;

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
    // User Types
    SUPER_ADMIN: "SUPER_ADMIN",
    INSTANCE_ADMIN: "INSTANCE_ADMIN",
    GLOBAL_ADMIN: "GLOBAL_ADMIN",
    ACCOUNT_ADMIN: "ACCOUNT_ADMIN",
    USER: "USER",
    ORG_ADMIN: "ORG_ADMIN",
    LAB_USER: "LAB_USER",
    REGIONAL_ADMIN: "REGIONAL_ADMIN",
    COUNTRY_SUPERVISOR: "ACCOUNT_ADMIN",
    BOT: "BOT",
    API_USER: "API_USER",

    // General states
    DISABLED: "DISABLED",

    // Reporting intervals
    NONE: "NONE",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR",

    // Filterable time periods
    ONEM: "1M",
    THREEM: "3M",
    SIXM: "6M",
    ONEY: "1Y",
    YTD: "YTD",
    ALL: "ALL",

    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    DRAFT: "DRAFT",
    DELETED: "DELETED",

    DEFAULT: "DEFAULT",

    DESC: "DESC",
    ASC: "ASC",

    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD",

    // Icons
    ICO_DASHBOARD: "fal fa-tachometer",
    ICO_USERS: "fal fa-users",
    ICO_USER: "fal fa-user",
    ICO_REPORT: "fal fa-file",
    ICO_REPORTS: "fal fa-clipboard",
    ICO_GRAPH: "fal fa-chart-bar",
    ICO_INVESTIGATION: "fal fa-search",
    ICO_RELATED: "fal fa-link",
    ICO_GEAR: "fal fa-gear",
    ICO_CARET_DOWN: "fal fa-caret-down",
    ICO_SEND: "fal fa-envelope",
    ICO_SUBMIT: "fal fa-senf",
    ICO_SAVE: "fal fa-save",
    ICO_CANCEL: "fal fa-times",

    // Chart Types
    SERIES: "SERIES",
    BAR: "BAR",
    MAP: "MAP",
    PIE: "PIE",
    SCATTER: "SCATTER",

    // Indicator types
    CUSTOM: "CUSTOM",

    OPEN: "OPEN",
    CLOSED: "CLOSED",

    // Form types
    PRIMARY: "PRIMARY",
    INVESTIGATIVE: "INVESTIGATIVE",
    LAB: "LAB",
    SURVEY: "SURVEY",

    // Alert stages
    VERIFICATION: "VERIFICATION",
    RISK_ASSESS: "RISK_ASSESS",
    RISK_CHAR: "RISK_CHAR",
    OUTCOME: "OUTCOME",

    // OUtcomes

    // Stage states
    DISCARD: "DISCARD",
    MONITOR: "MONITOR",
    ADVANCE: "ADVANCE",
    COMPLETED: "COMPLETED",
    PENDING: "PENDING",
    RESPOND: "RESPOND",
    DISCARDED: "DISCARDED",
    AUTODISCARDED: "AUTODISCARDED",

    // Risk levels
    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",

    // Colors
    RED: "red",
    GREEN: "green",
    ORANGE: "orange",
    YELLOW: "yellow",

    // Default Formats
    DEFAULT_DATE: "YYYY-MM-DD",

    // Dates
    MONTHS_LONG: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    MONTHS_SHORT: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    CAL_DAYS_LABELS: ["Sun", "Mon", "Tue", "Wed", "Thurs", "Fri", "Sat"],
    CAL_DAYS_IN_MONTH: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
};

/***/ })
/******/ ]);
//# sourceMappingURL=config-dev.map?[contentHash]