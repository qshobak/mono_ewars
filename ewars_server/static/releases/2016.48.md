# v2016.48

## v2016.48.12

- #185 Assignments for a disabled location are now disabled at the same time [Core]
- #177 Missing show row label field in form definition [Forms]
- #176 New Location does not wipe Parent Location field [Locations]
- #178 Fixes an issue where duplicating fields did not update UUID of new field or it's children [forms]
- #186 Root location parent can no longer be changed [Locations]
- BUGFIX: Fix to document generation [Documents]
- BUGFIX: Fixed layout/styling issues in table data editor [Templates]
- #163 Data Manager for table widget issues [Templates]

## v2016.48.11

- BUGFIX: Search in indicator selector [All]
- BUGFIX: "Search Locations..." showing up in indicator selector [All]
- BUGFIX: Notifications now marked as read when opened [Inbox]
- Added delete button to inbox notifications [Inbox]
- Added new Delete All button to remove all inbox notifications [Inbox]
- BUGFIX: Geographic Admins are now restricted to only seeing alerts for their area [Alerts]


## v2016.48.10

- BUGFIX: Scrolling in location editor on smaller screens

## v2016.48.9

- #136 Restrict form export to only active sites

## v2016.48.8

- BUGFIX: Form definition conditions returned
- BUGFIX: Date Field type available again in form definitions
- BUGFIX: Fixes an issue with form feature for alerts not operating correctly

## v2016.48.7

- BUGFIX: Fixes an issue with the edit action in the user management DataTable

## v2016.48.6

- BUGFIX: Fixes an issue where new accounts could only be associated with a country
- BUGFIX: email collision with system bot accounts and support@ewars.ws

## v2016.48.5

- BUGFIX: Fixes an issue with displaying account information on sub-domain mappings

## v2016.48.4

- Fixes an issue with saving location with no geometry

## v2016.48.3

- BUGFIX: Fixes an issue with email sending for certain sub-processes

## v2016.48.2

- BUGFIX: Fixes an issue with saving new users from account admin
- BUGFIX: Fixes and issue saving new forms in Forms Manager

## v2016.48.1

- New Gauge chart available
- Updated styling of releases pages.
- BUGFIX: Fixed an issue with expansion of locations in the tree