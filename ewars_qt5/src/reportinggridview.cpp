#include "reportinggridview.h"
#include "ui_reportinggridview.h"

ReportingGridView::ReportingGridView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReportingGridView)
{
    ui->setupUi(this);

    QToolBar* toolbar = new QToolBar();
    toolbar->setProperty("gridtbar", true);

    ui->mainLayout->addWidget(toolbar);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/jdu/Projects/qt/ewars/_d4cd548c5d97.db");
    if (!db.open()) {
        QMessageBox::critical(nullptr, QObject::tr("Cannot open database"),
                              QObject::tr("Unable to establish a database connection."), QMessageBox::Cancel);
    }

    QSqlTableModel *model = new QSqlTableModel(this, db);
    model->setTable("sqlite_master");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();

    QTableView *table = new QTableView;
    table->setModel(model);
    table->show();
    table->resizeColumnsToContents();

    ui->mainLayout->addWidget(table);

    QToolBar* footbar = new QToolBar();

    QIcon startIcon(":/icos/icons/fast-backward.svg");
    QPushButton startButton(this);
    startButton.setIcon(startIcon);
    footbar->addWidget(&startButton);

    QIcon prevIcon(":/icos/icons/caret-left.svg");
    QPushButton prevButton(this);
    prevButton.setIcon(prevIcon);
    footbar->addWidget(&prevButton);

    QIcon nextIcon(":/icos/icons/caret-right.svg");
    QPushButton nextButton(this);

    footbar->addWidget(&nextButton);

    QIcon lastIcon(":/icos/icons/fast-forward.svg");
    QPushButton *lastButton = new QPushButton(this);
    lastButton->setIcon(lastIcon);
    footbar->addWidget(lastButton);

    ui->mainLayout->addWidget(footbar);

}

ReportingGridView::~ReportingGridView()
{
    delete ui;
}
