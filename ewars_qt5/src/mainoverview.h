#ifndef MAINOVERVIEW_H
#define MAINOVERVIEW_H

#include <QWidget>
#include <QListWidgetItem>

#include "reportinggridview.h"

namespace Ui {
class MainOverview;
}

class MainOverview : public QWidget
{
    Q_OBJECT

public:
    explicit MainOverview(QWidget *parent = nullptr);
    ~MainOverview();

    QListWidgetItem formsMenuItem;
    QListWidgetItem locationsMenuItem;

private:
    Ui::MainOverview *ui;
};

#endif // MAINOVERVIEW_H
